# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
#---------------------------------1.实体类---------------------------------
-keep class com.jrdz.zhyb_android.ui.catalogue.model.** { *; }
-keep class com.jrdz.zhyb_android.ui.home.model.** { *; }
-keep class com.jrdz.zhyb_android.ui.index.model.** { *; }
-keep class com.jrdz.zhyb_android.ui.login.model.** { *; }
-keep class com.jrdz.zhyb_android.ui.settlement.model.** { *; }
-keep class com.jrdz.zhyb_android.ui.mine.model.** { *; }
-keep class com.jrdz.zhyb_android.ui.insured.model.** { *; }
-keep class com.jrdz.zhyb_android.database.** { *; }
-keep class com.jrdz.zhyb_android.ui.zhyf_manage.model.** { *; }
-keep class com.jrdz.zhyb_android.ui.zhyf_user.model.** { *; }


-keep class com.jrdz.zhyb_android.base.BaseModel { *; }
#webview
-keep class com.jrdz.zhyb_android.base.MyWebViewActivity { *; }

#华为扫码
-ignorewarnings
-keepattributes *Annotation*
-keepattributes Exceptions
-keepattributes InnerClasses
-keepattributes Signature
-keepattributes SourceFile,LineNumberTable
-keep class com.huawei.hianalytics.**{*;}
-keep class com.huawei.updatesdk.**{*;}
-keep class com.huawei.hms.**{*;}
#百度身份证识别
-keep class com.baidu.ocr.sdk.**{*;}
-dontwarn com.baidu.ocr.**
#pos机模块打印
-keep class com.centerm.cpay.ai.lib.**{*;}
-keep class com.centerm.smartpos.**{*;}
#支付宝人脸认证
-keep class com.alibaba.fastjson.** { *; }
-keep class fvv.** { *; }
-keepclassmembers class ** {
     @com.squareup.otto.Subscribe public *;
     @com.squareup.otto.Produce public *;
}
-keep class com.alipay.mobile.android.verify.** { *; }
#pdf文档浏览
-keep class com.shockwave.**
#全局弹框
-keep class com.hjq.window.** {*;}

# 快钱聚合支付混淆过滤
-dontwarn com.kuaiqian.**
-keep class com.kuaiqian.** {*;}
# 微信混淆过滤
-dontwarn com.tencent.**
-keep class com.tencent.** {*;}
# 内部WebView 混淆过滤
-keepclassmembers class * {
@android.webkit.JavascriptInterface <methods>;
}
#银联混淆
-dontwarn com.unionpay.**
-keep class com.huawei.nfc.sdk.service.** {*;}
-keep class com.unionpay.** {*;}
-keep class cn.gov.pbc.tsm.client.mobile.android.bank.service.** {*;}
-keep class org.simalliance.openmobileapi.** {*;}
#七牛sdk
-keep class com.qiniu.**{*;}
-keep class com.qiniu.**{public <init>();}
-ignorewarnings