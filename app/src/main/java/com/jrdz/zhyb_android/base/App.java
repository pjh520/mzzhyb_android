package com.jrdz.zhyb_android.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.alipay.mobile.android.verify.sdk.MPVerifyService;
import com.alipay.mobile.android.verify.sdk.ServiceFactory;
import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.common.BaiduMapSDKException;
import com.frame.bugly_library.BuglyUtils;
import com.frame.compiler.base.BaseApplication;
import com.frame.compiler.net.Custom_PersistentCookieStore;
import com.frame.compiler.net.CustomerLoggerInterceptor;
import com.frame.compiler.utils.BroadCastReceiveUtils;
import com.frame.compiler.utils.LogUtils;
import com.frame.compiler.utils.RxCrashTool;
import com.jrdz.zhyb_android.BuildConfig;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.HttpDns;
import com.jrdz.zhyb_android.pos.CPayDevice;
import com.jrdz.zhyb_android.ui.home.activity.ScanPayGoodDetailActivity;
import com.jrdz.zhyb_android.ui.index.activity.WelcomeActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.GoodDetailActivity;
import com.jrdz.zhyb_android.utils.AppManager_Acivity;
import com.jrdz.zhyb_android.utils.AppManager_GoodsDetailsAcivity;
import com.jrdz.zhyb_android.utils.MMKVUtils;
import com.library.constantStorage.ConstantStorage;
import com.library.jpush_library.JPushUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.cookie.CookieJarImpl;

import org.litepal.LitePal;

import java.net.Proxy;
import java.util.concurrent.TimeUnit;

import okhttp3.Dns;
import okhttp3.OkHttpClient;
import xyz.doikki.videoplayer.ijk.IjkPlayerFactory;
import xyz.doikki.videoplayer.player.VideoViewConfig;
import xyz.doikki.videoplayer.player.VideoViewManager;

/**
 * ================================================
 * 项目名称：My Application
 * 包    名：com.example.myapplication.base
 * 版    本：1.0
 * 创建日期：2021/9/27 0027
 * 描    述：
 * ================================================
 */
public class App extends BaseApplication{

    //app在后台被杀掉 需要重启app
    private BroadCastReceiveUtils mReSetAppReceive = new BroadCastReceiveUtils() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //ToastUtils.show("app在后台已被系统回收，即将重启app。。。");
            Intent resetIntent = new Intent(context, WelcomeActivity.class);
            resetIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(resetIntent);
            AppManager_Acivity.getInstance().finishAllActivity();
        }
    };
    //用户同意隐私协议 该类需要做一些处理
    private BroadCastReceiveUtils mAgreePrivacyAppReceive=new BroadCastReceiveUtils() {
        @Override
        public void onReceive(Context context, Intent intent) {
            initHttp();
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
//        new DoKit.Builder(this).productId("").build();
        if (Constants.Configure.IS_POS){
            CPayDevice.initDevice(this);
        }
        //支付宝人脸认证 无隐私相关调用，需在 application 中初始化
        MPVerifyService.setup(this);
        if (MMKVUtils.getBoolean("privacyAgreemen")){
            JPushUtils.init(getApplicationContext());
        }
    }

    @Override
    public void mainThreadInit() {
        BroadCastReceiveUtils.registerLocalReceiver(this, Constants.Action.SEND_RESET_APP, mReSetAppReceive);
        BroadCastReceiveUtils.registerLocalReceiver(this, Constants.Action.SEND_AGREE_PRIVACY, mAgreePrivacyAppReceive);
        LogUtils.init(BuildConfig.LOG_DEBUG);
        super.mainThreadInit();
        //初始化网络请求
        initHttp();
        //配置litepal的加密秘钥
        LitePal.aesKey(ConstantStorage.getLiteAesKey(this));
        // 2022-05-21 在用户同意隐私政策后 存储同意状态，这边下次启动才开始初始化
        if (MMKVUtils.getBoolean("privacyAgreemen")){
            BuglyUtils.getInstance().initBugly(this,Constants.Configure.BUGLY_APPID,Constants.Configure.BUGLY_LOG_ENABLE);
        }
        //初始化百度地图
        initLBS();
        //监听app的生命周期
        registerActivityLife();
    }

    //初始化网络请求库
    private void initHttp() {
        // 网络请求框架初始化
        CookieJarImpl cookieJar = new CookieJarImpl(new Custom_PersistentCookieStore(getApplicationContext()));
//        HttpsUtils.SSLParams sslParams = HttpsUtils.getSslSocketFactory(null, null, null);
//        .sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager)
        OkHttpClient okHttpClient;
        if (MMKVUtils.getBoolean("privacyAgreemen")){
            okHttpClient = new OkHttpClient.Builder()
                    .proxy(Proxy.NO_PROXY)
                    .addInterceptor(new CustomerLoggerInterceptor())
                    .dns(new HttpDns())
                    .connectTimeout(60000L, TimeUnit.MILLISECONDS)
                    .readTimeout(60000L, TimeUnit.MILLISECONDS)
                    //其他配置
                    .cookieJar(cookieJar)
                    .build();
        }else {
            okHttpClient = new OkHttpClient.Builder()
                    .proxy(Proxy.NO_PROXY)
                    .addInterceptor(new CustomerLoggerInterceptor())
                    .connectTimeout(60000L, TimeUnit.MILLISECONDS)
                    .readTimeout(60000L, TimeUnit.MILLISECONDS)
                    //其他配置
                    .cookieJar(cookieJar)
                    .build();
        }
//        EasyConfig.with(okHttpClient)
//                // 是否打印日志
//                //.setLogEnabled(BuildConfig.DEBUG)
//                // 设置服务器配置
//                .setServer(new ReleaseServer())
//                // 设置请求处理策略
//                .setHandler(new RequestHandler(this))
//                // 设置请求参数拦截器
//                .setInterceptor(new IRequestInterceptor() {
//                    @Override
//                    public void interceptArguments(HttpRequest<?> httpRequest, HttpParams params, HttpHeaders headers) {}
//                })
//                // 设置请求重试次数
//                .setRetryCount(1)
//                // 设置请求重试时间
//                .setRetryTime(2000)
//                // 添加全局请求参数
////                .addParam("token", "6666666")
//                // 添加全局请求头
//                //.addHeader("date", "20191030")
//                .into();

        //初始化okhttputils
        OkHttpUtils.initClient(okHttpClient);
    }

    @Override
    public void asyncMethod() {
        //初始化播放器配置
        initVideoPlayer();

        //初始化崩溃打印日志
        initCrashHandler();
    }

    private void initVideoPlayer() {
        //播放器配置，注意：此为全局配置，按需开启
        VideoViewManager.setConfig(VideoViewConfig.newBuilder()
                .setLogEnabled(BuildConfig.DEBUG) //调试的时候请打开日志，方便排错
                /** 软解，支持格式较多，可通过自编译so扩展格式，结合 {@link xyz.doikki.dkplayer.widget.videoview.IjkVideoView} 使用更佳 */
                .setPlayerFactory(IjkPlayerFactory.create())
//                .setPlayerFactory(AndroidMediaPlayerFactory.create()) //不推荐使用，兼容性较差
                /** 硬解，支持格式看手机，请使用CpuInfoActivity检查手机支持的格式，结合 {@link xyz.doikki.dkplayer.widget.videoview.ExoVideoView} 使用更佳 */
//                .setPlayerFactory(ExoMediaPlayerFactory.create())
                // 设置自己的渲染view，内部默认TextureView实现
//                .setRenderViewFactory(SurfaceRenderViewFactory.create())
                // 根据手机重力感应自动切换横竖屏，默认false
//                .setEnableOrientation(true)
                // 监听系统中其他播放器是否获取音频焦点，实现不与其他播放器同时播放的效果，默认true
//                .setEnableAudioFocus(false)
                // 视频画面缩放模式，默认按视频宽高比居中显示在VideoView中
//                .setScreenScaleType(VideoView.SCREEN_SCALE_MATCH_PARENT)
                // 适配刘海屏，默认true
//                .setAdaptCutout(false)
                // 移动网络下提示用户会产生流量费用，默认不提示，
                // 如果要提示则设置成false并在控制器中监听STATE_START_ABORT状态，实现相关界面，具体可以参考PrepareView的实现
//                .setPlayOnMobileNetwork(false)
                // 进度管理器，继承ProgressManager，实现自己的管理逻辑
//                .setProgressManager(new ProgressManagerImpl())
                .build());
    }

    //初始化崩溃打印日志
    private void initCrashHandler() {
        RxCrashTool.init(this);
    }

    //初始化地图
    private void initLBS() {
        if (MMKVUtils.getBoolean("privacyAgreemen")){
            // 在使用 SDK 各组间之前初始化 context 信息，传入 ApplicationContext
            // 默认本地个性化地图初始化方法
            SDKInitializer.setAgreePrivacy(this, true);
            try {
                SDKInitializer.initialize(this);
                //自4.3.0起，百度地图SDK所有接口均支持百度坐标和国测局坐标，用此方法设置您使用的坐标类型.
                //包括BD09LL和GCJ02两种坐标，默认是BD09LL坐标。
                SDKInitializer.setCoordType(CoordType.BD09LL);
            } catch (BaiduMapSDKException e) {

            }
        }
    }

    //注册监听activity的生命周期
    private void registerActivityLife() {
        //app 全局灰白化
//        Paint mPaint = new Paint();
//        ColorMatrix mColorMatrix = new ColorMatrix();
//        mColorMatrix.setSaturation(0);
//        mPaint.setColorFilter(new ColorMatrixColorFilter(mColorMatrix));
//        registerActivityLifecycleCallbacks(new AppGrayManager(mPaint));
        //Hook WindowManagerGlobal 中 mViews 实现 App 黑白化
//        GlobalGray.hook();
        //Activity管理
        registerActivityLifecycleCallbacks(AppManager_Acivity.getInstance());
        //商品详情页面管理
        registerActivityLifecycleCallbacks(mGoodsDetailsAcivityListener);
    }

    //商品详情页面管理
    private ActivityLifecycleCallbacks mGoodsDetailsAcivityListener=new ActivityLifecycleCallbacks() {
        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            if(activity instanceof GoodDetailActivity) {
                if(AppManager_GoodsDetailsAcivity.getInstance().getSize() >= AppManager_GoodsDetailsAcivity.PRODUCT_DETAIL_ACTIVITY_MAX_NUM){
                    AppManager_GoodsDetailsAcivity.getInstance().finishFirstActivity(); //移除栈顶的详情页并finish,保证商品详情页个数最大为3
                }
                AppManager_GoodsDetailsAcivity.getInstance().addActivity((GoodDetailActivity) activity);
                Log.e("activityNum","GoodsDetailsAcivity+activityNum="+AppManager_GoodsDetailsAcivity.getInstance().getSize());
            }

            if(activity instanceof ScanPayGoodDetailActivity) {
                if(AppManager_GoodsDetailsAcivity.getInstance().getSize2() >= AppManager_GoodsDetailsAcivity.PRODUCT_DETAIL_ACTIVITY_MAX_NUM){
                    AppManager_GoodsDetailsAcivity.getInstance().finishFirstActivity2(); //移除栈顶的详情页并finish,保证商品详情页个数最大为3
                }
                AppManager_GoodsDetailsAcivity.getInstance().addActivity((ScanPayGoodDetailActivity) activity);
                Log.e("activityNum","ScanPayGoodDetailActivity+activityNum="+AppManager_GoodsDetailsAcivity.getInstance().getSize2());
            }
        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            if(activity instanceof GoodDetailActivity) {
                AppManager_GoodsDetailsAcivity.getInstance().finishActivity(activity);
                Log.e("activityNum","GoodsDetailsAcivity+activityNum="+AppManager_GoodsDetailsAcivity.getInstance().getSize());
            }

            if(activity instanceof ScanPayGoodDetailActivity) {
                AppManager_GoodsDetailsAcivity.getInstance().finishActivity2(activity);
                Log.e("activityNum","ScanPayGoodDetailActivity+activityNum="+AppManager_GoodsDetailsAcivity.getInstance().getSize2());
            }
        }
    };
}
