package com.jrdz.zhyb_android.base;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.toast.ToastUtil;
import com.frame.compiler.widget.loadingView.KProgressHUD;
import com.frame.compiler.widget.title.OnTitleBarListener;
import com.frame.compiler.widget.title.TitleBar;
import com.jrdz.zhyb_android.R;
import com.zhy.http.okhttp.OkHttpUtils;

import java.util.Random;

/**
 * project:PJHAndroidFrame
 * package:com.frame.pjhandroidframe.base
 * Created by 彭俊鸿 on 2018/5/23.
 * e-mail : 1031028399@qq.com
 * fragment基类
 * https://blog.csdn.net/c6E5UlI1N/article/details/90307961
 * 在 Androidx 模式下，goole推荐使用 FragmentTransaction.setMaxLifecycle() 的方式来处理 Fragment 的懒加载
 */

public abstract class BaseFragmentX extends Fragment {
    public final String TAG = this.getClass().getSimpleName();
    public KProgressHUD hud;
    protected TitleBar mTitleBar;

    public boolean isDataInitialized;//是否初始化过数据
    private boolean isLazyLoadEnabled = false;//是否开启懒加载 默认关闭

    /*
     *是否开启懒加载 调用该方法开启
     */
    public void enableLazyLoad() {
        isLazyLoadEnabled = true;
    }

    /* enableDataInitialized 是否开启数据已初始化的功能 当viewpager中fragment未被回收掉时
     *是否重新加载数据isDataInitialized：true直接使用数据 false 重新加载数据
     *false：一般使用在不管什么情况 在切换fragment时都需要重新请求数据。
     * 默认为true
     */
    public void enableDataInitialized() {
        isDataInitialized = true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTitle(view);
        initView(view);

        //不需要懒加载的情况
        if (!isLazyLoadEnabled) {
            initData();
            initEvent();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //需要懒加载的情况
        if (isLazyLoadEnabled && !isDataInitialized) {
            enableDataInitialized();
            initData();
            initEvent();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    //设置页面布局
    public abstract int getLayoutId();

    //初始化页面控件
    public void initView(View view) {
    }

    //初始化头部title
    public void initTitle(View view) {
        mTitleBar = (TitleBar) view.findViewById(R.id.title_bar);
        if (mTitleBar == null) {
            return;
        }
        mTitleBar.setOnTitleBarListener(new OnTitleBarListener() {
            @Override
            public void onLeftClick(TitleBar titleBar) {
                leftTitleViewClick();
            }

            @Override
            public void onTitleClick(TitleBar titleBar) {

            }

            @Override
            public void onRightClick(TitleBar titleBar) {
                if (ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    rightTitleViewClick();
                }
            }
        });
    }

    //设置标题
    public void setTitle(String text) {
        if (mTitleBar != null) {
            mTitleBar.setTitle(text);
        }
    }

    //title左侧点击事件
    public void leftTitleViewClick() {
        KeyboardUtils.hideSoftInput(mTitleBar);
        getActivity().onBackPressed();
    }

    //title左侧控件显示
    public void showLeftView() {
        if (mTitleBar != null) {
            mTitleBar.getLeftView().setVisibility(View.VISIBLE);
        } else {
            showShortToast("控件还未初始化!");
        }
    }

    //title左侧控件隐藏
    public void hideLeftView() {
        if (mTitleBar != null) {
            mTitleBar.getLeftView().setVisibility(View.GONE);
        } else {
            showShortToast("控件还未初始化!");
        }
    }

    //title右侧点击事件
    public void rightTitleViewClick() {
        showShortToast("title右侧点击事件");
    }

    //title右侧控件显示
    public void showRightView() {
        if (mTitleBar != null) {
            mTitleBar.getRightView().setVisibility(View.VISIBLE);
        } else {
            showShortToast("控件还未初始化!");
        }
    }

    //title右侧控件隐藏
    public void hideRightView() {
        if (mTitleBar != null) {
            mTitleBar.getRightView().setVisibility(View.GONE);
        } else {
            showShortToast("控件还未初始化!");
        }
    }

    //设置title右侧控件文字
    public void setRightTitleView(String text) {
        if (mTitleBar != null) {
            mTitleBar.setRightTitle(text);
        }
    }

    //设置title右侧控件文字
    public void setRightIcon(Drawable drawable) {
        if (mTitleBar != null) {
            mTitleBar.setRightIcon(drawable);
        }
    }

    //初始化页面数据
    public void initData() {
    }

    //初始化页面交互事件
    public void initEvent() {
    }

    //显示toast
    public void showShortToast(String tip) {
        if ("Canceled".equals(tip)) {
            return;
        }
        ToastUtil.show(tip);
    }

    //显示数据加载框
    public void showWaitDialog() {
        showWaitDialog(null);
    }

    //显示数据加载框
    protected void showWaitDialog(String tip) {
        if (getContext() == null) {
            return;
        }
        if (hud == null) {
            hud = KProgressHUD.create(getContext())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        }

        if (!TextUtils.isEmpty(tip)) {
            hud.setLabel(tip);
        }
        hud.show();
    }

    //判断数据加载框是否显示
    public boolean isWaitShow() {
        return hud.isShowing();
    }

    //隐藏数据加载框
    public void hideWaitDialog() {
        if (hud != null && hud.isShowing()) {
            hud.dismiss();
            hud = null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        OkHttpUtils.getInstance().cancelTag(TAG);//取消以Activity.this作为tag的请求
    }

    @Override
    public void onDestroy() {
        hideWaitDialog();
        super.onDestroy();
        isDataInitialized = false;
    }

    //关闭页面
    public void goFinish() {
        getActivity().finish();
        KeyboardUtils.hideSoftInput(getActivity());
    }

    /**
     * startActivityForResult 方法优化
     */

    private OnActivityCallback mActivityCallback;
    private int mActivityRequestCode;

    public void startActivityForResult(Intent intent, OnActivityCallback callback) {
        startActivityForResult(intent, null, callback);
    }

    public void startActivityForResult(Intent intent, Bundle options, OnActivityCallback callback) {
        // 回调还没有结束，所以不能再次调用此方法，这个方法只适合一对一回调，其他需求请使用原生的方法实现
        if (mActivityCallback == null) {
            mActivityCallback = callback;
            // 随机生成请求码，这个请求码在 0 - 255 之间
            mActivityRequestCode = new Random().nextInt(255);
            startActivityForResult(intent, mActivityRequestCode, options);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (mActivityCallback != null && mActivityRequestCode == requestCode) {
            mActivityCallback.onActivityResult(resultCode, data);
            mActivityCallback = null;
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public interface OnActivityCallback {

        /**
         * 结果回调
         *
         * @param resultCode 结果码
         * @param data       数据
         */
        void onActivityResult(int resultCode, @Nullable Intent data);
    }

}
