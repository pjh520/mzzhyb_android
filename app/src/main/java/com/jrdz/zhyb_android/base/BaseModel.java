package com.jrdz.zhyb_android.base;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.frame.compiler.utils.DecimalFormatUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.MD5Util;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;
import com.jrdz.zhyb_android.net.RequestData;
import com.jrdz.zhyb_android.ui.insured.model.SpecialDrugDetailModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.zhy.http.okhttp.OkHttpUtils;

import java.io.File;
import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.base
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/10
 * 描    述：
 * ================================================
 */
public class BaseModel {
    private String code;
    private String msg;
    private String server_time;
    private Object data;

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public Object getData() {
        return data;
    }

    //新增科室
    public static void sendAddDeptRequest(final String TAG, String hosp_dept_codg, String caty, String hosp_dept_name, String begntime,
                                          String endtime, String itro, String dept_resper_name, String dept_resper_tel, String dept_med_serv_scp,
                                          String dept_estbdat, String aprv_bed_cnt, String hi_crtf_bed_cnt, String poolarea_no, String dr_psncnt,
                                          String phar_psncnt, String nurs_psncnt, String tecn_psncnt, String memo,
                                          final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("hosp_dept_codg", hosp_dept_codg);
        jsonObject.put("caty", caty);
        jsonObject.put("hosp_dept_name", hosp_dept_name);
        jsonObject.put("begntime", begntime);
        jsonObject.put("endtime", endtime);
        jsonObject.put("itro", itro);
        jsonObject.put("dept_resper_name", dept_resper_name);
        jsonObject.put("dept_resper_tel", dept_resper_tel);
        jsonObject.put("dept_med_serv_scp", dept_med_serv_scp);
        jsonObject.put("dept_estbdat", dept_estbdat);
        jsonObject.put("aprv_bed_cnt", aprv_bed_cnt);
        jsonObject.put("hi_crtf_bed_cnt", hi_crtf_bed_cnt);
        jsonObject.put("poolarea_no", poolarea_no);
        jsonObject.put("dr_psncnt", dr_psncnt);
        jsonObject.put("phar_psncnt", phar_psncnt);
        jsonObject.put("nurs_psncnt", nurs_psncnt);
        jsonObject.put("tecn_psncnt", tecn_psncnt);
        jsonObject.put("memo", memo);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_ADDDEPT_URL, jsonObject.toJSONString(), callback);
    }

    //更新科室
    public static void sendUpdateDeptRequest(final String TAG, String deptId, String Origin_Code, String hosp_dept_codg, String caty, String hosp_dept_name,
                                             String begntime, String endtime, String itro, String dept_resper_name, String dept_resper_tel, String dept_med_serv_scp,
                                             String dept_estbdat, String aprv_bed_cnt, String hi_crtf_bed_cnt, String poolarea_no, String dr_psncnt,
                                             String phar_psncnt, String nurs_psncnt, String tecn_psncnt, String memo,
                                             final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("deptId", deptId);
        jsonObject.put("Origin_Code", Origin_Code);
        jsonObject.put("hosp_dept_codg", hosp_dept_codg);
        jsonObject.put("caty", caty);
        jsonObject.put("hosp_dept_name", hosp_dept_name);
        jsonObject.put("begntime", begntime);
        jsonObject.put("endtime", endtime);
        jsonObject.put("itro", itro);
        jsonObject.put("dept_resper_name", dept_resper_name);
        jsonObject.put("dept_resper_tel", dept_resper_tel);
        jsonObject.put("dept_med_serv_scp", dept_med_serv_scp);
        jsonObject.put("dept_estbdat", dept_estbdat);
        jsonObject.put("aprv_bed_cnt", aprv_bed_cnt);
        jsonObject.put("hi_crtf_bed_cnt", hi_crtf_bed_cnt);
        jsonObject.put("poolarea_no", poolarea_no);
        jsonObject.put("dr_psncnt", dr_psncnt);
        jsonObject.put("phar_psncnt", phar_psncnt);
        jsonObject.put("nurs_psncnt", nurs_psncnt);
        jsonObject.put("tecn_psncnt", tecn_psncnt);
        jsonObject.put("memo", memo);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_UPDATEDEPT_URL, jsonObject.toJSONString(), callback);
    }

    //删除科室
    public static void sendDeletDeptRequest(final String TAG, String deptId, String hosp_dept_codg, String hosp_dept_name, String begntime,
                                            final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("deptId", deptId);
        jsonObject.put("hosp_dept_codg", hosp_dept_codg);
        jsonObject.put("hosp_dept_name", hosp_dept_name);
        jsonObject.put("begntime", begntime);
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_DELETDEPT_URL, jsonObject.toJSONString(), callback);
    }

    //新增医师
    public static void sendAddDoctorRequest(final String TAG, String hosp_dept_codg, String hosp_dept_name, String dr_code, String dr_name,
                                            String dr_pwd, String dr_type, String dr_type_name, String dr_phone, String accessoryId,
                                            final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("hosp_dept_codg", hosp_dept_codg);
        jsonObject.put("hosp_dept_name", hosp_dept_name);
        jsonObject.put("dr_code", dr_code);
        jsonObject.put("dr_name", dr_name);
        jsonObject.put("dr_pwd", dr_pwd);
        jsonObject.put("dr_type", dr_type);
        jsonObject.put("dr_type_name", dr_type_name);
        jsonObject.put("dr_phone", dr_phone);
        jsonObject.put("accessoryId", accessoryId);
//        jsonObject.put("verificationCode", verificationCode);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_ADDDOCTOR_URL, jsonObject.toJSONString(), callback);
    }

    //更新医师
    public static void sendUpdateDoctorRequest(final String TAG, String doctorId, String hosp_dept_codg, String hosp_dept_name, String dr_code, String dr_name,
                                               String dr_pwd, String dr_type, String dr_type_name, String dr_phone, String accessoryId,
                                               final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("doctorId", doctorId);
        jsonObject.put("hosp_dept_codg", hosp_dept_codg);
        jsonObject.put("hosp_dept_name", hosp_dept_name);
        jsonObject.put("dr_code", dr_code);
        jsonObject.put("dr_name", dr_name);
        jsonObject.put("dr_pwd", dr_pwd);
        jsonObject.put("dr_type", dr_type);
        jsonObject.put("dr_type_name", dr_type_name);
        jsonObject.put("dr_phone", dr_phone);
        jsonObject.put("accessoryId", accessoryId);
//        jsonObject.put("verificationCode", verificationCode);
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_EDITDOCTOR_URL, jsonObject.toJSONString(), callback);
    }

    //删除医师
    public static void sendDelDoctorRequest(final String TAG, String doctorId, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("doctorId", doctorId);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_DELDOCTOR_URL, jsonObject.toJSONString(), callback);
    }

    //医师启用禁用
    public static void sendEditdoctorIsEnableRequest(final String TAG, String doctorId, String IsEnable, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("doctorId", doctorId);
        jsonObject.put("IsEnable", IsEnable);
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_EDITDOCTORISENABLE_URL, jsonObject.toJSONString(), callback);
    }

    //注销
    public static void sendLogoffdoctorRequest(final String TAG, String doctorId, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("doctorId", doctorId);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_LOGOFFDOCTOR_URL, jsonObject.toJSONString(), callback);
    }

    //上传目录
    public static void sendContentsMatchRequest(final String TAG, String ItemType, String ItemCode, String ItemName, String MIC_Code,
                                                String Price, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ItemType", ItemType);
        jsonObject.put("ItemCode", ItemCode);
        jsonObject.put("ItemName", ItemName);
        jsonObject.put("MIC_Code", MIC_Code);
        jsonObject.put("Price", Price);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_CONTENTSMATCH_URL, jsonObject.toJSONString(), callback);
    }

    //修改目录价格（不上传省平台）
    public static void sendContentsEditRequest(final String TAG, String ItemType, String ItemCode, String ItemName, String MIC_Code,
                                               String Price, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ItemType", ItemType);
        jsonObject.put("ItemCode", ItemCode);
        jsonObject.put("ItemName", ItemName);
        jsonObject.put("MIC_Code", MIC_Code);
        jsonObject.put("Price", Price);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_EDIT_MICCOMPARISON_URL, jsonObject.toJSONString(), callback);
    }

    //目录对照撤销
    public static void sendDelMatchRequest(final String TAG, String fixmedins_hilist_id, String list_type,
                                           String med_list_codg, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fixmedins_hilist_id", fixmedins_hilist_id);
        jsonObject.put("list_type", list_type);
        jsonObject.put("med_list_codg", med_list_codg);
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_CONTENTSMATCHDELETE_URL, jsonObject.toJSONString(), callback);
    }

    //上传图片
    public static void sendUploadFileRequest(final String TAG, String setlId, File stFile, final CustomerJsonCallBack<BaseModel> callback) {
        OkHttpUtils.post()
                .url(Constants.BASE_URL + Constants.Api.GET_UPLOADFILE_URL)
                .tag(TAG)
                .addFile("file", stFile.getName(), stFile)//
                .addParams("accessoryId", EmptyUtils.strEmpty(setlId))
                .build()
                .execute(callback);
    }

    //门诊信息上传
    public static void sendOutpatMdtrtUpARequest(final String TAG, String dataParams, String insuplc_admdvs, final CustomerJsonCallBack<BaseModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_OUTPATIENTMDTRTINFOUPA_URL, dataParams, insuplc_admdvs, callback);
    }

    //门诊费用明细信息撤销
    public static void sendOutpatFeeListUpCancelRequest(final String TAG, String psn_no, String mdtrt_id, String chrg_bchno,
                                                        String settlementClassification, String insuplc_admdvs,
                                                        final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("psn_no", psn_no);
        jsonObject.put("chrg_bchno", chrg_bchno);
        jsonObject.put("mdtrt_id", mdtrt_id);
        jsonObject.put("settlementClassification", settlementClassification);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_OUTPATIENTFEELISTUPCANCEL_URL, jsonObject.toJSONString(), insuplc_admdvs, callback);
    }

    //门诊挂号撤销
    public static void sendoutpatRegCancelRequest(final String TAG, String psn_no, String ipt_otp_no,
                                                  String mdtrt_id, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("psn_no", psn_no);
        jsonObject.put("ipt_otp_no", ipt_otp_no);
        jsonObject.put("mdtrt_id", mdtrt_id);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_OUTPAREGCANCEL_URL, jsonObject.toJSONString(), callback);
    }

    //药店结算撤销
    public static void sendFixmedinSettleDeleteRequest(final String TAG, String psn_no, String setl_id,
                                                       String mdtrt_id, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("psn_no", psn_no);
        jsonObject.put("setl_id", setl_id);
        jsonObject.put("mdtrt_id", mdtrt_id);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_FIXMEDINSETTLEDELETE_URL, jsonObject.toJSONString(), callback);
    }

    //门诊结算撤销
    public static void sendCancleSettletmentRequest(final String TAG, String ipt_otp_no, String psn_no, String setl_id,
                                                    String mdtrt_id, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ipt_otp_no", ipt_otp_no);
        jsonObject.put("psn_no", psn_no);
        jsonObject.put("setl_id", setl_id);
        jsonObject.put("mdtrt_id", mdtrt_id);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_CANCLESETTLETMENT_URL, jsonObject.toJSONString(), callback);
    }

    //管理员修改密码
    public static void sendUpdatePwdRequest(final String TAG, String oldPassword, String newPassword, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("oldPassword", MD5Util.up32(oldPassword));
        jsonObject.put("newPassword", MD5Util.up32(newPassword));

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_UPDATEADMINPWD_URL, jsonObject.toJSONString(), callback);
    }

    //结算对明细账
    public static void sendStmtDetailRequest(final String TAG, String StmtTotalId, String setl_optins, String stmt_begndate, String stmt_enddate, String medfee_sumamt,
                                             String fund_pay_sumamt, String cash_payamt, String fixmedins_setl_cnt,
                                             final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("StmtTotalId", StmtTotalId);
        jsonObject.put("setl_optins", setl_optins);
        jsonObject.put("stmt_begndate", stmt_begndate);
        jsonObject.put("stmt_enddate", stmt_enddate);
        jsonObject.put("medfee_sumamt", medfee_sumamt);
        jsonObject.put("fund_pay_sumamt", fund_pay_sumamt);
        jsonObject.put("cash_payamt", cash_payamt);
        jsonObject.put("fixmedins_setl_cnt", fixmedins_setl_cnt);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STMTDETAIL_URL, jsonObject.toJSONString(), callback);
    }

    //申请解绑
    public static void sendApplyUnbindingRequest(final String TAG, final CustomerJsonCallBack<BaseModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_APPLYUNBINDING_URL, "", callback);
    }

    //门诊记录提交
    public static void sendFeelistCommitRequest(final String TAG, String ipt_otp_no, String mdtrt_id, String psn_name, String psn_no, String settlementClassification,
                                                final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ipt_otp_no", ipt_otp_no);
        jsonObject.put("mdtrt_id", mdtrt_id);
        jsonObject.put("psn_name", psn_name);
        jsonObject.put("psn_no", psn_no);
        jsonObject.put("settlementClassification", settlementClassification);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_SAVEOUTPATIENTRECORDS_URL, jsonObject.toJSONString(), callback);
    }

    //系统设置
    public static void sendSetSystemRequest(final String TAG, String ElectronicPrescription, String OutpatientMdtrtinfo,
                                            final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ElectronicPrescription", ElectronicPrescription);
        jsonObject.put("OutpatientMdtrtinfo", OutpatientMdtrtinfo);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_SETSYSTEM_URL, jsonObject.toJSONString(), callback);
    }

    //提交问卷
    public static void sendAddQuestionnaireRequest(final String TAG, String data, final CustomerJsonCallBack<BaseModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_ADDQUESTIONNAIRE_URL, data, callback);
    }

    //月结申报
    public static void sendMonthSettDeclarRequest(final String TAG, String begndate, String enddate, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("clr_type", "");
        jsonObject.put("clr_way", "");
        jsonObject.put("clr_type_lv2", "");
        jsonObject.put("begndate", begndate);
        jsonObject.put("enddate", enddate);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MEDINSMONSETLAPPY_URL, jsonObject.toJSONString(), callback);
    }

    //忘记密码
    public static void sendForgetPasswordRequest(final String TAG, String username, String newPassword, String verificationCode, String dr_phone,
                                                 final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", username);
        jsonObject.put("newPassword", MD5Util.up32(newPassword));
        jsonObject.put("verificationCode", verificationCode);
        jsonObject.put("dr_phone", dr_phone);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_FORGETPASSWORD_URL, jsonObject.toJSONString(), callback);
    }

    //商户端退出登录
    public static void sendLogoutRequest(final String TAG, final CustomerJsonCallBack<BaseModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_LOGOUT_URL, "", callback);
    }

    //医师设置上下线接口
    public static void sendEditdoctorIsOnlineRequest(final String TAG, String doctorId, String IsOnline,
                                                     final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("doctorId", doctorId);
        jsonObject.put("IsOnline", IsOnline);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MERCHANT_EDITDOCTORISONLINE_URL, jsonObject.toJSONString(), callback);
    }

    //===========================参保人===============================================================
    //参保人注册
    public static void sendInsuredRegistRequest(final String TAG, String phone, String verificationCode, String pwd,
                                                String confirm_pwd, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("phone", phone);
        jsonObject.put("verificationCode", verificationCode);
        jsonObject.put("pwd", pwd);
        jsonObject.put("confirm_pwd", confirm_pwd);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSUREDREG_URL, jsonObject.toJSONString(), callback);
    }

    //参保人手机号码更换
    public static void sendPhoneUpdateRequest(final String TAG, String phone, String verificationCode,
                                              final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("phone", phone);
        jsonObject.put("verificationCode", verificationCode);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_CHANGEPHONE_URL, jsonObject.toJSONString(), callback);
    }

    //参保人密码修改
    public static void sendInsuredUpdatePwdRequest(final String TAG, String original_pwd, String pwd, String confirm_pwd,
                                                   final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("original_pwd", MD5Util.up32(original_pwd));
        jsonObject.put("pwd", MD5Util.up32(pwd));
        jsonObject.put("confirm_pwd", MD5Util.up32(confirm_pwd));

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_CHANGEPWD_URL, jsonObject.toJSONString(), callback);
    }

    //参保人密码修改
    public static void sendInsuredFindPwdRequest(final String TAG, String phone, String verificationCode, String pwd, String confirm_pwd,
                                                 final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("phone", phone);
        jsonObject.put("verificationCode", verificationCode);
        jsonObject.put("pwd", MD5Util.up32(pwd));
        jsonObject.put("confirm_pwd", MD5Util.up32(confirm_pwd));

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_FORGOTPASSWORD_URL, jsonObject.toJSONString(), callback);
    }

    //参保人上传头像
    public static void sendInsuredUploadRequest(final String TAG, String accessoryId,
                                                final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessoryId", accessoryId);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_UPLOAD_URL, jsonObject.toJSONString(), callback);
    }

    //参保人发送短信
    public static void sendSendsmsRequest(final String TAG, String phone, String type,
                                          final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("phone", phone);
        jsonObject.put("type", type);//type:类型（1、参保人注册/商户端注册激活、2修改手机号、3、忘记密码 4、管理员认证5、医师认证6、其他人员认证 7、人员添加/修改手机号 8、互联网药店医保结算9商户端忘记密码 10修改支付密码）

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_SENDSMS_URL, jsonObject.toJSONString(), callback);
    }

    //参保人信息修改
    public static void sendUpdatePersonInfoRequest(final String TAG, String name, String id_card_no,
                                                   final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", name);
        jsonObject.put("id_card_no", id_card_no);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_EDITINSURED_URL, jsonObject.toJSONString(), callback);
    }

    //参保人退出登录
    public static void sendLogoutRequest_user(final String TAG, final CustomerJsonCallBack<BaseModel> callback) {
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_LOGOUT_URL, "", callback);
    }

    //参保人注销功能
    public static void sendLogOffRequest_user(final String TAG, final CustomerJsonCallBack<BaseModel> callback) {
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_LOGOFF_URL, "", callback);
    }

    //参保人企业基金交易密码设置
    public static void sendSetEnterprisePwdRequest(final String TAG, String id_card_no, String phone, String verificationCode, String PaymentPwd,
                                                   final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id_card_no", id_card_no);
        jsonObject.put("phone", phone);
        jsonObject.put("verificationCode", verificationCode);
        jsonObject.put("PaymentPwd", PaymentPwd);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_SETPAYMENTPWD_URL, jsonObject.toJSONString(), callback);
    }

    //参保人企业基金交易密码修改
    public static void sendUpdateEnterprisePwdRequest(final String TAG, String id_card_no, String phone, String verificationCode, String PaymentPwd, String OriginalPaymentPwd,
                                                      final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id_card_no", id_card_no);
        jsonObject.put("phone", phone);
        jsonObject.put("verificationCode", verificationCode);
        jsonObject.put("PaymentPwd", PaymentPwd);
        jsonObject.put("OriginalPaymentPwd", OriginalPaymentPwd);
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_CHANGEPAYMENTPWD_URL, jsonObject.toJSONString(), callback);
    }

    //参保人企业基金交易密码重置
    public static void sendResetEnterprisePwdRequest(final String TAG, String certify_id, String PaymentPwd,
                                                     final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("certify_id", certify_id);
        jsonObject.put("PaymentPwd", PaymentPwd);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_RESETPAYMENTPWD_URL, jsonObject.toJSONString(), callback);
    }

    // 特药备案申请
    public static void sendAddSpecialDrugRequest(final String TAG, String name, String id_card_no, String phone,
                                                 String ApplicationFormAccessoryId1, String PrescriptionAccessoryId1, String DiagnosisAccessoryId1,
                                                 String CaseAccessoryId1, String CommitmentLetterAccessoryId, ArrayList<SpecialDrugDetailModel.DataBean.MedInsuDirListBean> basicDataModels,
                                                 final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", name);//备案人姓名
        jsonObject.put("id_card_no", id_card_no);//备案人身份证号
        jsonObject.put("phone", phone);//备案人手机号
        jsonObject.put("ApplicationFormAccessoryId1", ApplicationFormAccessoryId1);//申请表附件1
        jsonObject.put("PrescriptionAccessoryId1", PrescriptionAccessoryId1);//处方附件1
        jsonObject.put("DiagnosisAccessoryId1", DiagnosisAccessoryId1);//诊断附件1
        jsonObject.put("CaseAccessoryId1", CaseAccessoryId1);//病例附件1
        jsonObject.put("CommitmentLetterAccessoryId", CommitmentLetterAccessoryId);//承诺书附件

        JSONArray jsonArray = new JSONArray();
        for (SpecialDrugDetailModel.DataBean.MedInsuDirListBean basicDataModel : basicDataModels) {
            JSONObject jsonObjectChild = new JSONObject();
            jsonObjectChild.put("fixmedins_code", EmptyUtils.strEmpty(basicDataModel.getFixmedins_code()));//机构编码
            jsonObjectChild.put("fixmedins_name", EmptyUtils.strEmpty(basicDataModel.getFixmedins_name()));//机构名称
            jsonObjectChild.put("hosp_dept_name", EmptyUtils.strEmpty(basicDataModel.getHosp_dept_name()));//科室名称
            jsonObjectChild.put("dr_code", EmptyUtils.strEmpty(basicDataModel.getDr_code()));//医师编码
            jsonObjectChild.put("dr_name", EmptyUtils.strEmpty(basicDataModel.getDr_name()));//医师名字
            jsonObjectChild.put("ProfessionalTitle", EmptyUtils.strEmpty(basicDataModel.getProfessionalTitle()));//职称
            jsonObjectChild.put("MIC_Code", EmptyUtils.strEmpty(basicDataModel.getMIC_Code()));//药品编码
            jsonObjectChild.put("ItemName", EmptyUtils.strEmpty(basicDataModel.getItemName()));//药品名称
            jsonObjectChild.put("cnt", EmptyUtils.strEmpty(basicDataModel.getCnt()));//药品数量
            jsonObjectChild.put("cnt_prcunt", EmptyUtils.strEmpty(basicDataModel.getCnt_prcunt()));//药品单位
            jsonObjectChild.put("begintime", EmptyUtils.strEmpty(basicDataModel.getBegintime()));//开始时间
            jsonObjectChild.put("endtime", EmptyUtils.strEmpty(basicDataModel.getEndtime()));//结束时间
            jsonArray.add(jsonObjectChild);
        }
        jsonObject.put("med_insu_dir_list", jsonArray);//医师和药品
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_ADDSPECIALDRUG_URL, jsonObject.toJSONString(), callback);
    }

    // 特药备案申请--重新备案
    public static void sendAgainAddSpecialDrugRequest(final String TAG, String SpecialDrugFilingId, String name, String id_card_no, String phone,
                                                      String ApplicationFormAccessoryId1, String PrescriptionAccessoryId1, String DiagnosisAccessoryId1,
                                                      String CaseAccessoryId1, String CommitmentLetterAccessoryId, ArrayList<SpecialDrugDetailModel.DataBean.MedInsuDirListBean> basicDataModels,
                                                      final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("SpecialDrugFilingId", SpecialDrugFilingId);//备案人姓名
        jsonObject.put("name", name);//备案人姓名
        jsonObject.put("id_card_no", id_card_no);//备案人身份证号
        jsonObject.put("phone", phone);//备案人手机号
        jsonObject.put("ApplicationFormAccessoryId1", ApplicationFormAccessoryId1);//申请表附件1
        jsonObject.put("PrescriptionAccessoryId1", PrescriptionAccessoryId1);//处方附件1
        jsonObject.put("DiagnosisAccessoryId1", DiagnosisAccessoryId1);//诊断附件1
        jsonObject.put("CaseAccessoryId1", CaseAccessoryId1);//病例附件1
        jsonObject.put("CommitmentLetterAccessoryId", CommitmentLetterAccessoryId);//承诺书附件

        JSONArray jsonArray = new JSONArray();
        for (SpecialDrugDetailModel.DataBean.MedInsuDirListBean basicDataModel : basicDataModels) {
            JSONObject jsonObjectChild = new JSONObject();
            jsonObjectChild.put("fixmedins_code", EmptyUtils.strEmpty(basicDataModel.getFixmedins_code()));//机构编码
            jsonObjectChild.put("fixmedins_name", EmptyUtils.strEmpty(basicDataModel.getFixmedins_name()));//机构名称
            jsonObjectChild.put("hosp_dept_name", EmptyUtils.strEmpty(basicDataModel.getHosp_dept_name()));//科室名称
            jsonObjectChild.put("dr_code", EmptyUtils.strEmpty(basicDataModel.getDr_code()));//医师编码
            jsonObjectChild.put("dr_name", EmptyUtils.strEmpty(basicDataModel.getDr_name()));//医师名字
            jsonObjectChild.put("ProfessionalTitle", EmptyUtils.strEmpty(basicDataModel.getProfessionalTitle()));//职称
            jsonObjectChild.put("MIC_Code", EmptyUtils.strEmpty(basicDataModel.getMIC_Code()));//药品编码
            jsonObjectChild.put("ItemName", EmptyUtils.strEmpty(basicDataModel.getItemName()));//药品名称
            jsonObjectChild.put("cnt", EmptyUtils.strEmpty(basicDataModel.getCnt()));//药品数量
            jsonObjectChild.put("cnt_prcunt", EmptyUtils.strEmpty(basicDataModel.getCnt_prcunt()));//药品单位
            jsonObjectChild.put("begintime", EmptyUtils.strEmpty(basicDataModel.getBegintime()));//开始时间
            jsonObjectChild.put("endtime", EmptyUtils.strEmpty(basicDataModel.getEndtime()));//结束时间

            jsonArray.add(jsonObjectChild);
        }
        jsonObject.put("med_insu_dir_list", jsonArray);//医师和药品

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_EDITSPECIALDRUG_URL, jsonObject.toJSONString(), callback);
    }

    //删除图片
    public static void sendDelAttachmentUploadRequest(final String TAG, String AccessoryId,
                                                      final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("AccessoryId", AccessoryId);//图片标识

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_DELATTACHMENTUPLOAD_URL, jsonObject.toJSONString(), callback);
    }

    //撤销特药备案申请
    public static void sendRevokeSpecialDrugRequest(final String TAG, String id_card_no,String SpecialDrugFilingId,
                                                    final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id_card_no", id_card_no);//身份证号
        jsonObject.put("SpecialDrugFilingId", SpecialDrugFilingId);//特药备案唯一标识

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_REVOKESPECIALDRUG_URL, jsonObject.toJSONString(), callback);
    }

    //addComplaint
    public static void sendAddComplaintRequest(final String TAG, String FullName,String Phone,String ComplaintContent,String ComplaintAccessoryId,
                                                    final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("FullName", FullName);//姓名
        jsonObject.put("Phone", Phone);//手机号
        jsonObject.put("ComplaintContent", ComplaintContent);//反馈内容
        jsonObject.put("ComplaintAccessoryId", ComplaintAccessoryId);//投诉附件标识
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_ADDCOMPLAINT_URL, jsonObject.toJSONString(), callback);
    }
    //===========================参保人===============================================================

    //===========================智慧药房-商户端===============================================================
    //新增店铺药品库
    public static void sendAddShopDrugDataRequest(final String TAG, String DrugsClassification, String DrugsClassificationName, String ItemName,
                                                  String Spec, String EnterpriseName, String StorageConditions, String ExpiryDateCount, String InventoryQuantity,
                                                  String Price, Object InstructionsAccessoryId, Object DetailAccessoryId1, Object DetailAccessoryId2,
                                                  Object DetailAccessoryId3, Object DetailAccessoryId4, Object DetailAccessoryId5, Object ItemType,
                                                  String ApprovalNumber, String RegDosform, String EfccAtd, String UsualWay, String AssociatedDiseases,
                                                  String Brand, String RegistrationCertificateNo, String ProductionLicenseNo, String Packaging, String ApplicableScope,
                                                  String UsageDosage, String Precautions, String Usagemethod,
                                                  final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        //公共参数
        jsonObject.put("DrugsClassification", DrugsClassification);//药品分类标识（1、西药中成药 2、医疗器械3、成人用品4、个人用品）
        jsonObject.put("DrugsClassificationName", DrugsClassificationName);//药品分类名称
        jsonObject.put("ItemName", ItemName);//本地目录名称
        jsonObject.put("Spec", Spec);//规格
        jsonObject.put("EnterpriseName", EnterpriseName);//生产企业名称
        jsonObject.put("StorageConditions", StorageConditions);//贮存条件
        jsonObject.put("ExpiryDateCount", ExpiryDateCount);//有效期数
        jsonObject.put("InventoryQuantity", InventoryQuantity);//库存数量
        jsonObject.put("Price", Price);//价格
        jsonObject.put("InstructionsAccessoryId", null == InstructionsAccessoryId ? "" : String.valueOf(InstructionsAccessoryId));//说明书附件标识
        jsonObject.put("DetailAccessoryId1", null == DetailAccessoryId1 ? "" : String.valueOf(DetailAccessoryId1));//详情图附件标识1
        jsonObject.put("DetailAccessoryId2", null == DetailAccessoryId2 ? "" : String.valueOf(DetailAccessoryId2));//详情图附件标识2
        jsonObject.put("DetailAccessoryId3", null == DetailAccessoryId3 ? "" : String.valueOf(DetailAccessoryId3));//详情图附件标识3
        jsonObject.put("DetailAccessoryId4", null == DetailAccessoryId4 ? "" : String.valueOf(DetailAccessoryId4));//详情图附件标识4
        jsonObject.put("DetailAccessoryId5", null == DetailAccessoryId5 ? "" : String.valueOf(DetailAccessoryId5));//详情图附件标识5
        //西药中成药特有
        jsonObject.put("ItemType", null == ItemType ? "" : String.valueOf(ItemType));//目录类型（1、OTC 2、处方药）
        jsonObject.put("ApprovalNumber", ApprovalNumber);//批准文号
        jsonObject.put("RegDosform", RegDosform);//注册剂型
        jsonObject.put("EfccAtd", EfccAtd);//功能主治
        jsonObject.put("UsualWay", UsualWay);//常见用法
        jsonObject.put("AssociatedDiseases", AssociatedDiseases);//关联疾病  使用，隔开
        //医疗器械
        jsonObject.put("Brand", Brand);//品牌
        jsonObject.put("RegistrationCertificateNo", RegistrationCertificateNo);//注册证编号/备案凭证编号
        jsonObject.put("ProductionLicenseNo", ProductionLicenseNo);//医疗器械生产许可证编号/备案编号
        jsonObject.put("Packaging", Packaging);//包装
        jsonObject.put("ApplicableScope", ApplicableScope);//适用范围
        jsonObject.put("UsageDosage", UsageDosage);//用法用量
        jsonObject.put("Precautions", Precautions);//注意事项
        //成人用品
        jsonObject.put("Usagemethod", Usagemethod);//使用方法

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_PSTOREDRUGDATA_ADDDRUGDATA_URL, jsonObject.toJSONString(), callback);
    }

    //修改店铺药品库
    public static void sendUpdateShopDrugDataRequest(final String TAG, String DrugDataId, String DrugsClassification, String DrugsClassificationName, String ItemName,
                                                     String Spec, String EnterpriseName, String StorageConditions, String ExpiryDateCount, String InventoryQuantity,
                                                     String Price, Object InstructionsAccessoryId, Object DetailAccessoryId1, Object DetailAccessoryId2,
                                                     Object DetailAccessoryId3, Object DetailAccessoryId4, Object DetailAccessoryId5, Object ItemType,
                                                     String ApprovalNumber, String RegDosform, String EfccAtd, String UsualWay, String AssociatedDiseases,
                                                     String Brand, String RegistrationCertificateNo, String ProductionLicenseNo, String Packaging, String ApplicableScope,
                                                     String UsageDosage, String Precautions, String Usagemethod,
                                                     final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        //公共参数
        jsonObject.put("DrugDataId", DrugDataId);//唯一标识
        jsonObject.put("DrugsClassification", DrugsClassification);//药品分类标识（1、西药中成药 2、医疗器械3、成人用品4、个人用品）
        jsonObject.put("DrugsClassificationName", DrugsClassificationName);//药品分类名称
        jsonObject.put("ItemName", ItemName);//本地目录名称
        jsonObject.put("Spec", Spec);//规格
        jsonObject.put("EnterpriseName", EnterpriseName);//生产企业名称
        jsonObject.put("StorageConditions", StorageConditions);//贮存条件
        jsonObject.put("ExpiryDateCount", ExpiryDateCount);//有效期数
        jsonObject.put("InventoryQuantity", InventoryQuantity);//库存数量
        jsonObject.put("Price", Price);//价格
        jsonObject.put("InstructionsAccessoryId", null == InstructionsAccessoryId ? "" : String.valueOf(InstructionsAccessoryId));//说明书附件标识
        jsonObject.put("DetailAccessoryId1", null == DetailAccessoryId1 ? "" : String.valueOf(DetailAccessoryId1));//详情图附件标识1
        jsonObject.put("DetailAccessoryId2", null == DetailAccessoryId2 ? "" : String.valueOf(DetailAccessoryId2));//详情图附件标识2
        jsonObject.put("DetailAccessoryId3", null == DetailAccessoryId3 ? "" : String.valueOf(DetailAccessoryId3));//详情图附件标识3
        jsonObject.put("DetailAccessoryId4", null == DetailAccessoryId4 ? "" : String.valueOf(DetailAccessoryId4));//详情图附件标识4
        jsonObject.put("DetailAccessoryId5", null == DetailAccessoryId5 ? "" : String.valueOf(DetailAccessoryId5));//详情图附件标识5
        //西药中成药特有
        jsonObject.put("ItemType", null == ItemType ? "" : String.valueOf(ItemType));//目录类型（1、OTC 2、处方药）
        jsonObject.put("ApprovalNumber", ApprovalNumber);//批准文号
        jsonObject.put("RegDosform", RegDosform);//注册剂型
        jsonObject.put("EfccAtd", EfccAtd);//功能主治
        jsonObject.put("UsualWay", UsualWay);//常见用法
        jsonObject.put("AssociatedDiseases", AssociatedDiseases);//关联疾病  使用，隔开
        //医疗器械
        jsonObject.put("Brand", Brand);//品牌
        jsonObject.put("RegistrationCertificateNo", RegistrationCertificateNo);//注册证编号/备案凭证编号
        jsonObject.put("ProductionLicenseNo", ProductionLicenseNo);//医疗器械生产许可证编号/备案编号
        jsonObject.put("Packaging", Packaging);//包装
        jsonObject.put("ApplicableScope", ApplicableScope);//适用范围
        jsonObject.put("UsageDosage", UsageDosage);//用法用量
        jsonObject.put("Precautions", Precautions);//注意事项
        //成人用品
        jsonObject.put("Usagemethod", Usagemethod);//使用方法

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_PSTOREDRUGDATA_EDITDRUGDATA_URL, jsonObject.toJSONString(), callback);
    }

    //删除店铺药品库
    public static void sendDelShopDrugDataRequest(final String TAG, String DrugDataId, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        //公共参数
        jsonObject.put("DrugDataId", DrugDataId);//唯一标识

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_PSTOREDRUGDATA_DELDRUGDATA_URL, jsonObject.toJSONString(), callback);
    }

    //提交机构认证（管理员）
    public static void sendAuthManaRequest(final String TAG, String fixmedins_code, String fixmedins_name, String fixmedins_type, String Director, String fixmedins_phone,
                                           String SettlementBankCard, String Bank, Object FrontAccessoryId, Object BackAccessoryId, Object BusinessLicenseAccessoryId,
                                           Object DrugBLAccessoryId, Object MedicalDeviceBLAccessoryId, Object GSCertificateAccessoryId, Object AccountLicenceAccessoryId,
                                           Object DoorHeaderAccessoryId, Object InteriorOneAccessoryId, Object InteriorTwoAccessoryId,
                                           final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fixmedins_code", fixmedins_code);//机构编码
        jsonObject.put("fixmedins_name", fixmedins_name);//机构名称
        jsonObject.put("fixmedins_type", fixmedins_type);//医疗机构类型
        jsonObject.put("Director", Director);//法人姓名
        jsonObject.put("fixmedins_phone", fixmedins_phone);//法人联系方式:
        jsonObject.put("SettlementBankCard", SettlementBankCard);//结算银行卡
        jsonObject.put("Bank", Bank);//开户行
        jsonObject.put("FrontAccessoryId", null == FrontAccessoryId ? "" : String.valueOf(FrontAccessoryId));//身份证正面照附件唯一标识
        jsonObject.put("BackAccessoryId", null == BackAccessoryId ? "" : String.valueOf(BackAccessoryId));//身份证反面照附件唯一标识
        jsonObject.put("BusinessLicenseAccessoryId", null == BusinessLicenseAccessoryId ? "" : String.valueOf(BusinessLicenseAccessoryId));//营业执照附件唯一标识
        jsonObject.put("DrugBLAccessoryId", null == DrugBLAccessoryId ? "" : String.valueOf(DrugBLAccessoryId));//药品经营许可证
        jsonObject.put("MedicalDeviceBLAccessoryId", null == MedicalDeviceBLAccessoryId ? "" : String.valueOf(MedicalDeviceBLAccessoryId));//医疗器械经营许可证
        jsonObject.put("GSCertificateAccessoryId", null == GSCertificateAccessoryId ? "" : String.valueOf(GSCertificateAccessoryId));//GS认证证书
        jsonObject.put("AccountLicenceAccessoryId", null == AccountLicenceAccessoryId ? "" : String.valueOf(AccountLicenceAccessoryId));//开户许可证
        jsonObject.put("DoorHeaderAccessoryId", null == DoorHeaderAccessoryId ? "" : String.valueOf(DoorHeaderAccessoryId));//门头照片
        jsonObject.put("InteriorOneAccessoryId", null == InteriorOneAccessoryId ? "" : String.valueOf(InteriorOneAccessoryId));//内景照片1
        jsonObject.put("InteriorTwoAccessoryId", null == InteriorTwoAccessoryId ? "" : String.valueOf(InteriorTwoAccessoryId));//内景照片2

//        jsonObject.put("verificationCode", verificationCode);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_AUTH_MEDINSINFOACTIVATION_URL, jsonObject.toJSONString(), callback);
    }

    //提交机构认证（管理员）--立即登录
    public static void sendAuthLoginRequest(final String TAG, String ActivationType, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ActivationType", ActivationType);//认证类型（1、管理员认证2、医师认证3、其他人员认证）

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_AUTH_LOGINACTIVATION_URL, jsonObject.toJSONString(), callback);
    }


    //提交认证--医师或其他人员认证
    public static void sendAuthDoctorRequest(final String TAG, String doctorId, String fixmedins_code, String fixmedins_name, String dr_name, String dr_phone,
                                             Object DoctorAccessoryId, String QualificationCertificateNo, String IDNumber, Object FrontAccessoryId,
                                             Object BackAccessoryId, String ActivationType, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("doctorId", doctorId);//机构编码
        jsonObject.put("fixmedins_code", fixmedins_code);//机构编码
        jsonObject.put("fixmedins_name", fixmedins_name);//机构名称
        jsonObject.put("dr_name", dr_name);//姓名
        jsonObject.put("dr_phone", dr_phone);//联系电话
        jsonObject.put("DoctorAccessoryId", null == DoctorAccessoryId ? "" : String.valueOf(DoctorAccessoryId));//医师职业资格证附件唯一标识
        jsonObject.put("QualificationCertificateNo", QualificationCertificateNo);//资格证编号
        jsonObject.put("IDNumber", IDNumber);//身份证号
        jsonObject.put("FrontAccessoryId", null == FrontAccessoryId ? "" : String.valueOf(FrontAccessoryId));//身份证正面照附件唯一标识
        jsonObject.put("BackAccessoryId", null == BackAccessoryId ? "" : String.valueOf(BackAccessoryId));//身份证反面照附件唯一标识
        jsonObject.put("ActivationType", ActivationType);//认证类型（1、医师认证2、其他人员认证）

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_AUTH_DOCTORACTIVATIO_URL, jsonObject.toJSONString(), callback);
    }


    //删除目录
    public static void sendDelCataRequest(final String TAG, String CatalogueId, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CatalogueId", CatalogueId);//机构编码

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_DELCATALOGUE_URL, jsonObject.toJSONString(), callback);
    }

    //保存店铺风格
    public static void sendSaveStoreStyleRequest(final String TAG, String dataParams, final CustomerJsonCallBack<BaseModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_SAVESTORESTYLE_URL, dataParams, callback);
    }

    //保存店铺信息
    public static void sendSaveStoreInfoRequest(final String TAG, Object StoreAccessoryId, String StoreName, String IsAutomatic, String StartTime, String EndTime,
                                                String IsShowSold, String IsShowGoodsSold, String IsShowMargin, String StartingPrice, String StoreAdress,
                                                String DetailedAddress, String Telephone, String Wechat, String OpeningStartTime, String OpeningEndTime,
                                                String DistributionScope, String BusinessScope, String FeaturedServices, String Notice,
                                                final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("StoreAccessoryId", null == StoreAccessoryId ? "" : String.valueOf(StoreAccessoryId));//店铺缩略图唯一标识
        jsonObject.put("StoreName", StoreName);//店铺名称
        jsonObject.put("IsAutomatic", IsAutomatic);//是否自动上线下线(1是2否）
        jsonObject.put("StartTime", StartTime);//开店时间
        jsonObject.put("EndTime", EndTime);//闭店时间
        jsonObject.put("IsShowSold", IsShowSold);//是否显示已售数量(1是2否）
        jsonObject.put("IsShowGoodsSold", IsShowGoodsSold);//是否展示商品月售数量(1是2否）
        jsonObject.put("IsShowMargin", IsShowMargin);//当库存小于等于5是否展示剩余数(1是2否）
        jsonObject.put("StartingPrice", StartingPrice);//药品起送价
        jsonObject.put("StoreAdress", StoreAdress);//药店地址
        jsonObject.put("DetailedAddress", DetailedAddress);//详细地址
        jsonObject.put("Telephone", Telephone);//联系电话
        jsonObject.put("Wechat", Wechat);//商家微信
        jsonObject.put("OpeningStartTime", OpeningStartTime);//营业开始时间
        jsonObject.put("OpeningEndTime", OpeningEndTime);//营业结束时间
        jsonObject.put("DistributionScope", DistributionScope);//配送范围
        jsonObject.put("BusinessScope", BusinessScope);//经营范围
        jsonObject.put("FeaturedServices", FeaturedServices);//特色服务
        jsonObject.put("Notice", Notice);//公告
//        jsonObject.put("BusinessLicenseAccessoryId", null == BusinessLicenseAccessoryId ? "" : String.valueOf(BusinessLicenseAccessoryId));//营业执照
//        jsonObject.put("DrugBLAccessoryId", null == DrugBLAccessoryId ? "" : String.valueOf(DrugBLAccessoryId));//药品经营许可证
//        jsonObject.put("MedicalDeviceBLAccessoryId", null == MedicalDeviceBLAccessoryId ? "" : String.valueOf(MedicalDeviceBLAccessoryId));//医疗器械经营许可证
//        jsonObject.put("GSCertificateAccessoryId", null == GSCertificateAccessoryId ? "" : String.valueOf(GSCertificateAccessoryId));//GS认证证书
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_SAVESTOREINFORMATION_URL, jsonObject.toJSONString(), callback);
    }

    //保存物流配送--各个大区 配送城市具体信息
    public static void sendSaveLogisticsSetRequest(final String TAG, String dataParams, final CustomerJsonCallBack<BaseModel> callback) {


        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_SAVELOGISTICSSET_URL, dataParams, callback);
    }


    //保存物流配送
    public static void sendSaveLogisCofigRequest(final String TAG, String IsSelfDelivery, String SelfDeliveryName, String SelfDeliveryPhone,
                                                 String SelfDeliveryArea, String IsExpress, String ExpressCompany,
                                                 final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("IsSelfDelivery", IsSelfDelivery);//是否自配
        jsonObject.put("SelfDeliveryName", SelfDeliveryName);//自配姓名
        jsonObject.put("SelfDeliveryPhone", SelfDeliveryPhone);//自配手机
//        jsonObject.put("fixmedins_code", fixmedins_code);//机构编码
        jsonObject.put("SelfDeliveryArea", SelfDeliveryArea);//自配区域
        jsonObject.put("IsExpress", IsExpress);//是否快递
        jsonObject.put("ExpressCompany", ExpressCompany);//快递公司

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_SAVELOGISTICSDISTRIBUTION_URL, jsonObject.toJSONString(), callback);
    }

    //新增商品
    public static void sendAddGoodsRequest(final String TAG, String GoodsLocation, String CatalogueId, String CatalogueName, Object BannerAccessoryId1, Object BannerAccessoryId2,
                                           Object BannerAccessoryId3, Object Intervaltime, Object IsPromotion, String PromotionDiscount,
                                           String PreferentialPrice, String StoreSimilarity, String IsPlatformDrug, String DrugsClassification, String DrugsClassificationName,
                                           String MIC_Code, String ItemName, String ItemCode, String GoodsName, String Spec, String EnterpriseName,
                                           String StorageConditions, String ExpiryDateCount, String InventoryQuantity, String Price,
                                           Object InstructionsAccessoryId, Object DetailAccessoryId1, Object DetailAccessoryId2, Object DetailAccessoryId3,
                                           Object DetailAccessoryId4, Object DetailAccessoryId5, String IsOnSale, Object ItemType,
                                           String ApprovalNumber, String RegDosform, String EfccAtd, String UsualWay,
                                           String AssociatedDiagnostics, String AssociatedDiseases, String Brand, String RegistrationCertificateNo,
                                           String ProductionLicenseNo, String Packaging, String ApplicableScope, String UsageDosage,
                                           String Precautions, String Usagemethod, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        //公共参数
        jsonObject.put("GoodsLocation", GoodsLocation);//商品出现位置
        jsonObject.put("CatalogueId", CatalogueId);//关联目录唯一标识
        jsonObject.put("CatalogueName", CatalogueName);//关联目录唯一标识
        jsonObject.put("BannerAccessoryId1", null == BannerAccessoryId1 ? "" : String.valueOf(BannerAccessoryId1));//banner附件标识1
        jsonObject.put("BannerAccessoryId2", null == BannerAccessoryId2 ? "" : String.valueOf(BannerAccessoryId2));//banner附件标识2
        jsonObject.put("BannerAccessoryId3", null == BannerAccessoryId3 ? "" : String.valueOf(BannerAccessoryId3));//banner附件标识3
        jsonObject.put("Intervaltime", null == Intervaltime ? "3" : String.valueOf(Intervaltime));//轮播间隔时间
        jsonObject.put("IsPromotion", null == IsPromotion ? "1" : String.valueOf(IsPromotion));//是否促销
        jsonObject.put("PromotionDiscount", DecimalFormatUtils.noZero(PromotionDiscount));//促销折扣
        jsonObject.put("PreferentialPrice", DecimalFormatUtils.noZero(PreferentialPrice));//预估到手价
        jsonObject.put("StoreSimilarity", StoreSimilarity);//店内相似
        jsonObject.put("IsPlatformDrug", IsPlatformDrug);//是否平台药品库（1是2否）
        jsonObject.put("DrugsClassification", DrugsClassification);//药品分类标识（1、西药中成药 2、医疗器械3、成人用品4、个人用品）
        jsonObject.put("DrugsClassificationName", DrugsClassificationName);//药品分类名称
        jsonObject.put("MIC_Code", MIC_Code);//医保目录编码
        jsonObject.put("ItemName", ItemName);//本地目录名称
        jsonObject.put("ItemCode", ItemCode);//本地目录编码
        jsonObject.put("GoodsName", GoodsName);//商品名称
        jsonObject.put("Spec", Spec);//规格
        jsonObject.put("EnterpriseName", EnterpriseName);//生产企业名称
        jsonObject.put("StorageConditions", StorageConditions);//贮存条件
        jsonObject.put("ExpiryDateCount", ExpiryDateCount);//有效期数
        jsonObject.put("InventoryQuantity", InventoryQuantity);//库存数量
        jsonObject.put("Price", DecimalFormatUtils.noZero(Price));//价格
        jsonObject.put("InstructionsAccessoryId", null == InstructionsAccessoryId ? "" : String.valueOf(InstructionsAccessoryId));//说明书附件标识
        jsonObject.put("DetailAccessoryId1", null == DetailAccessoryId1 ? "" : String.valueOf(DetailAccessoryId1));//详情图附件标识1
        jsonObject.put("DetailAccessoryId2", null == DetailAccessoryId2 ? "" : String.valueOf(DetailAccessoryId2));//详情图附件标识2
        jsonObject.put("DetailAccessoryId3", null == DetailAccessoryId3 ? "" : String.valueOf(DetailAccessoryId3));//详情图附件标识3
        jsonObject.put("DetailAccessoryId4", null == DetailAccessoryId4 ? "" : String.valueOf(DetailAccessoryId4));//详情图附件标识4
        jsonObject.put("DetailAccessoryId5", null == DetailAccessoryId5 ? "" : String.valueOf(DetailAccessoryId5));//详情图附件标识5
        jsonObject.put("IsOnSale", IsOnSale);//是否上架（0新建1上架2下架）
        //西药中成药特有
        jsonObject.put("ItemType", null == ItemType ? "" : String.valueOf(ItemType));//目录类型（1、OTC 2、处方药）
        jsonObject.put("ApprovalNumber", ApprovalNumber);//批准文号
        jsonObject.put("RegDosform", RegDosform);//注册剂型
        jsonObject.put("EfccAtd", EfccAtd);//功能主治
        jsonObject.put("UsualWay", UsualWay);//常见用法
        jsonObject.put("AssociatedDiagnostics", AssociatedDiagnostics);//关联诊断
        jsonObject.put("AssociatedDiseases", AssociatedDiseases);//关联疾病  使用，隔开
        //医疗器械
        jsonObject.put("Brand", Brand);//品牌
        jsonObject.put("RegistrationCertificateNo", RegistrationCertificateNo);//注册证编号/备案凭证编号
        jsonObject.put("ProductionLicenseNo", ProductionLicenseNo);//医疗器械生产许可证编号/备案编号
        jsonObject.put("Packaging", Packaging);//包装
        jsonObject.put("ApplicableScope", ApplicableScope);//适用范围
        jsonObject.put("UsageDosage", UsageDosage);//用法用量
        jsonObject.put("Precautions", Precautions);//注意事项
        //成人用品
        jsonObject.put("Usagemethod", Usagemethod);//使用方法

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_ADDGOODS_URL, jsonObject.toJSONString(), callback);
    }

    //更新商品
    public static void sendUpdateGoodsRequest(final String TAG, String GoodsNo, String GoodsLocation, String CatalogueId, String CatalogueName, Object BannerAccessoryId1, Object BannerAccessoryId2,
                                              Object BannerAccessoryId3, Object Intervaltime, Object IsPromotion, String PromotionDiscount,
                                              String PreferentialPrice, String StoreSimilarity, String IsPlatformDrug, String DrugsClassification, String DrugsClassificationName,
                                              String MIC_Code, String ItemName, String ItemCode, String GoodsName, String Spec, String EnterpriseName,
                                              String StorageConditions, String ExpiryDateCount, String InventoryQuantity, String Price,
                                              Object InstructionsAccessoryId, Object DetailAccessoryId1, Object DetailAccessoryId2, Object DetailAccessoryId3,
                                              Object DetailAccessoryId4, Object DetailAccessoryId5, String IsOnSale, Object ItemType,
                                              String ApprovalNumber, String RegDosform, String EfccAtd, String UsualWay,
                                              String AssociatedDiagnostics, String AssociatedDiseases, String Brand, String RegistrationCertificateNo,
                                              String ProductionLicenseNo, String Packaging, String ApplicableScope, String UsageDosage,
                                              String Precautions, String Usagemethod, String from, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        //公共参数
        jsonObject.put("GoodsNo", GoodsNo);//商品出现位置
        jsonObject.put("GoodsLocation", GoodsLocation);//商品出现位置
        jsonObject.put("CatalogueId", CatalogueId);//关联目录唯一标识
        jsonObject.put("CatalogueName", CatalogueName);//关联目录唯一标识
        jsonObject.put("BannerAccessoryId1", null == BannerAccessoryId1 ? "" : String.valueOf(BannerAccessoryId1));//banner附件标识1
        jsonObject.put("BannerAccessoryId2", null == BannerAccessoryId2 ? "" : String.valueOf(BannerAccessoryId2));//banner附件标识2
        jsonObject.put("BannerAccessoryId3", null == BannerAccessoryId3 ? "" : String.valueOf(BannerAccessoryId3));//banner附件标识3
        jsonObject.put("Intervaltime", null == Intervaltime ? "3" : String.valueOf(Intervaltime));//轮播间隔时间
        jsonObject.put("IsPromotion", null == IsPromotion ? "1" : String.valueOf(IsPromotion));//是否促销
        jsonObject.put("PromotionDiscount", DecimalFormatUtils.noZero(PromotionDiscount));//促销折扣
        jsonObject.put("PreferentialPrice", DecimalFormatUtils.noZero(PreferentialPrice));//预估到手价
        jsonObject.put("StoreSimilarity", StoreSimilarity);//店内相似
        jsonObject.put("IsPlatformDrug", IsPlatformDrug);//是否平台药品库（1是2否）
        jsonObject.put("DrugsClassification", DrugsClassification);//药品分类标识（1、西药中成药 2、医疗器械3、成人用品4、个人用品）
        jsonObject.put("DrugsClassificationName", DrugsClassificationName);//药品分类名称
        jsonObject.put("MIC_Code", MIC_Code);//医保目录编码
        jsonObject.put("ItemName", ItemName);//本地目录名称
        jsonObject.put("ItemCode", ItemCode);//本地目录编码
        jsonObject.put("GoodsName", GoodsName);//商品名称
        jsonObject.put("Spec", Spec);//规格
        jsonObject.put("EnterpriseName", EnterpriseName);//生产企业名称
        jsonObject.put("StorageConditions", StorageConditions);//贮存条件
        jsonObject.put("ExpiryDateCount", ExpiryDateCount);//有效期数
        jsonObject.put("InventoryQuantity", InventoryQuantity);//库存数量
        jsonObject.put("Price", DecimalFormatUtils.noZero(Price));//价格
        jsonObject.put("InstructionsAccessoryId", null == InstructionsAccessoryId ? "" : String.valueOf(InstructionsAccessoryId));//说明书附件标识
        jsonObject.put("DetailAccessoryId1", null == DetailAccessoryId1 ? "" : String.valueOf(DetailAccessoryId1));//详情图附件标识1
        jsonObject.put("DetailAccessoryId2", null == DetailAccessoryId2 ? "" : String.valueOf(DetailAccessoryId2));//详情图附件标识2
        jsonObject.put("DetailAccessoryId3", null == DetailAccessoryId3 ? "" : String.valueOf(DetailAccessoryId3));//详情图附件标识3
        jsonObject.put("DetailAccessoryId4", null == DetailAccessoryId4 ? "" : String.valueOf(DetailAccessoryId4));//详情图附件标识4
        jsonObject.put("DetailAccessoryId5", null == DetailAccessoryId5 ? "" : String.valueOf(DetailAccessoryId5));//详情图附件标识5
        jsonObject.put("IsOnSale", IsOnSale);//是否上架（0新建1上架2下架）
        //西药中成药特有
        jsonObject.put("ItemType", null == ItemType ? "" : String.valueOf(ItemType));//目录类型（1、OTC 2、处方药）
        jsonObject.put("ApprovalNumber", ApprovalNumber);//批准文号
        jsonObject.put("RegDosform", RegDosform);//注册剂型
        jsonObject.put("EfccAtd", EfccAtd);//功能主治
        jsonObject.put("UsualWay", UsualWay);//常见用法
        jsonObject.put("AssociatedDiagnostics", AssociatedDiagnostics);//关联诊断
        jsonObject.put("AssociatedDiseases", AssociatedDiseases);//关联疾病  使用，隔开
        //医疗器械
        jsonObject.put("Brand", Brand);//品牌
        jsonObject.put("RegistrationCertificateNo", RegistrationCertificateNo);//注册证编号/备案凭证编号
        jsonObject.put("ProductionLicenseNo", ProductionLicenseNo);//医疗器械生产许可证编号/备案编号
        jsonObject.put("Packaging", Packaging);//包装
        jsonObject.put("ApplicableScope", ApplicableScope);//适用范围
        jsonObject.put("UsageDosage", UsageDosage);//用法用量
        jsonObject.put("Precautions", Precautions);//注意事项
        //成人用品
        jsonObject.put("Usagemethod", Usagemethod);//使用方法

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + ("1".equals(from) ? Constants.Api.GET_MANA_EDITGOODS_URL :
                Constants.Api.GET_MANA_DRAFTS_EDITGOODS_URL), jsonObject.toJSONString(), callback);
    }

    //删除产品池商品
    public static void sendDelGoodsRequest(final String TAG, String GoodsNo, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("GoodsNo", GoodsNo);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_DELETE_GOODS_URL, jsonObject.toJSONString(), callback);
    }

    //删除草稿箱商品
    public static void sendDraftsDelGoodsRequest(final String TAG, String GoodsNo, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("GoodsNo", GoodsNo);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_PDRAFTS_DELETE_GOODS_URL, jsonObject.toJSONString(), callback);
    }

    //上架下架商品
    public static void sendOnOffGoodsRequest(final String TAG, String GoodsNo, String IsOnSale, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("GoodsNo", GoodsNo);//是否上架（1上架2下架
        jsonObject.put("IsOnSale", IsOnSale);//是否上架（1上架2下架

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_ONOFFGOODS_URL, jsonObject.toJSONString(), callback);
    }

    //新增提现
    public static void sendApplyWithdrawalRequest(final String TAG, String ReceiveMethod, String ReceiveAccount, String FullName,
                                                  String Phone, String TakeAmount, String AccountBalance, String TakeType,
                                                  final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ReceiveMethod", ReceiveMethod);//收款方式（1、微信2支付宝）
        jsonObject.put("ReceiveAccount", ReceiveAccount);//收款账号
        jsonObject.put("FullName", FullName);//收款人姓名
        jsonObject.put("Phone", Phone);//收款人手机号
        jsonObject.put("TakeAmount", TakeAmount);//提现金额
        jsonObject.put("AccountBalance", AccountBalance);//账户余额
        jsonObject.put("TakeType", TakeType);//提现类型（1普通提现2保证金提现）

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_TAKECASH_URL, jsonObject.toJSONString(), callback);
    }

    //新增提现-医师
    public static void sendApplyWithdrawalRequest_doctor(final String TAG, String ReceiveMethod, String ReceiveAccount, String FullName,
                                                         String Phone, String TakeAmount, String AccountBalance, String TakeType,
                                                         final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ReceiveMethod", ReceiveMethod);//收款方式（1、微信2支付宝）
        jsonObject.put("ReceiveAccount", ReceiveAccount);//收款账号
        jsonObject.put("FullName", FullName);//收款人姓名
        jsonObject.put("Phone", Phone);//收款人手机号
        jsonObject.put("TakeAmount", TakeAmount);//提现金额
        jsonObject.put("AccountBalance", AccountBalance);//账户余额
        jsonObject.put("TakeType", TakeType);//提现类型（1普通提现2保证金提现）

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_DOCTORTAKECASH_URL, jsonObject.toJSONString(), callback);
    }

    //设置消息已读
    public static void sendIsReadMessageRequest(final String TAG, String MessageId, String IsAll, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("MessageId", MessageId);
        jsonObject.put("IsAll", IsAll);//是否全部设为已读（0 没有 1全部）

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_ISREADMESSAGE_URL, jsonObject.toJSONString(), callback);
    }

    //设置消息已读--用户端
    public static void sendUserIsReadMessageRequest(final String TAG, String MessageId, String IsAll, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("MessageId", MessageId);
        jsonObject.put("IsAll", IsAll);//是否全部设为已读（0 没有 1全部）

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_USERISREADMESSAGE_URL, jsonObject.toJSONString(), callback);
    }

    //更新店铺资质
    public static void sendUpdateStoreCredentialsRequest(final String TAG, String BusinessLicenseAccessoryId, String DrugBLAccessoryId,
                                                         Object MedicalDeviceBLAccessoryId, Object GSCertificateAccessoryId,
                                                         final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("BusinessLicenseAccessoryId", BusinessLicenseAccessoryId);//营业执照
        jsonObject.put("DrugBLAccessoryId", DrugBLAccessoryId);//药品经营许可证
        jsonObject.put("MedicalDeviceBLAccessoryId", null == MedicalDeviceBLAccessoryId ? "" : String.valueOf(MedicalDeviceBLAccessoryId));//医疗器械经营许可证
        jsonObject.put("GSCertificateAccessoryId", null == GSCertificateAccessoryId ? "" : String.valueOf(GSCertificateAccessoryId));//GS认证证书

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_UPDATESTORECREDENTIALS_URL, jsonObject.toJSONString(), callback);
    }

    //修改订单
    public static void sendUpdateOrderRequest(final String TAG, String OrderNo, String ShippingMethod, String ShippingMethodName, String FullName,
                                              String Phone, String Address, String Location, String distance, String AreaName, String CityName,
                                              final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);//订单号
        jsonObject.put("ShippingMethod", ShippingMethod);//配送方式（1、自提2、商家配送）
        jsonObject.put("ShippingMethodName", ShippingMethodName);//配送方式名称
        jsonObject.put("FullName", FullName);//姓名
        jsonObject.put("Phone", Phone);//手机号
        jsonObject.put("Address", Address);//地址
        jsonObject.put("Location", Location);//所在地区
        jsonObject.put("distance", distance);//距离
        jsonObject.put("AreaName", AreaName);//省份
        jsonObject.put("CityName", CityName);//市

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_UPDATEORDER_URL, jsonObject.toJSONString(), callback);
    }

    //商户取消订单
    public static void sendStoreCancelOrderRequest(final String TAG, String OrderNo, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);//订单号

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STORECANCELORDER_URL, jsonObject.toJSONString(), callback);
    }

    //商户删除订单
    public static void sendStoreDelOrderRequest(final String TAG, String OrderNo, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);//订单号

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STOREDELORDER_URL, jsonObject.toJSONString(), callback);
    }

    //新增订单处方
    public static void sendAddElectronicPrescriptionRequest(final String TAG, String OrderNo, String wmcpmDiagResults, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);//订单号
        jsonObject.put("wmcpmDiagResults", wmcpmDiagResults);//电子处方附件标识
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_ADDELECTRONICPRESCRIPTION_URL, jsonObject.toJSONString(), callback);
    }

    //订单立即发货
    public static void sendImmediateDeliveryRequest(final String TAG, String OrderNo, String ShippingMethod, String ShippingMethodName, String FullName,
                                                    String Phone, String Address, String SelfDeliveryName, String SelfDeliveryPhone, String ExpressCompany,
                                                    String ExpressNo, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);//订单号
        jsonObject.put("ShippingMethod", ShippingMethod);//配送方式（1、自提2、商家配送）
        jsonObject.put("ShippingMethodName", ShippingMethodName);//配送方式名称
        jsonObject.put("FullName", FullName);//姓名
        jsonObject.put("Phone", Phone);//手机号
        jsonObject.put("Address", Address);//地址
        jsonObject.put("SelfDeliveryName", SelfDeliveryName);//配送人姓名
        jsonObject.put("SelfDeliveryPhone", SelfDeliveryPhone);//配送人手机
        jsonObject.put("ExpressCompany", ExpressCompany);//快递公司
        jsonObject.put("ExpressNo", ExpressNo);//快递单号
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STORE_ADDLOGISTICSPROGRESS_URL, jsonObject.toJSONString(), callback);
    }

    //订单语音提醒
    public static void sendVoiceNotificationRequest(final String TAG, String VoiceNotification, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("VoiceNotification", VoiceNotification);//（1开启0关闭）
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STORE_SETVOICENOTIFICATION_URL, jsonObject.toJSONString(), callback);
    }

    //药品目录同步
    public static void sendSyncMedListMapRequest(final String TAG, final CustomerJsonCallBack<BaseModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_SYNCMEDLISTMAP_URL, "", callback);
    }

    //添加购物车商品
    public static void sendAddShopCarRequest_mana(final String TAG, String fixmedins_code, String GoodsNo, int GoodsNum, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fixmedins_code", fixmedins_code);//机构编码
        jsonObject.put("GoodsNo", GoodsNo);//商品编码
        jsonObject.put("ShoppingCartNum", GoodsNum);//商品数量

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MERCHANT_ADDSHOPPINGCARTGOODS_URL, jsonObject.toJSONString(), callback);
    }

    //清空购物车商品
    public static void sendDelAllShopCarRequest_mana(final String TAG, String fixmedins_code, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fixmedins_code", fixmedins_code);//商品编码

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MERCHANT_DELALLSHOPPINGCARTGOODS_URL, jsonObject.toJSONString(), callback);
    }

    //添加购物车商品-订单
    public static void sendAddOrderShopCarRequest_mana(final String TAG, String GoodsNo, int GoodsNum, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", GoodsNo);//商品编码
        jsonObject.put("ShoppingCartNum", GoodsNum);//商品数量

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MERCHANT_ADDORDERSHOPPINGCARTGOODS_URL, jsonObject.toJSONString(), callback);
    }

    //修改处方问诊状态
    public static void sendUpdatePresStatusRequest_mana(final String TAG, String GoodsNo, String dr_code, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", GoodsNo);//商品编码
        jsonObject.put("dr_code", dr_code);//医师编码

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MERCHANT_UPDATEPRESSTATUS_URL, jsonObject.toJSONString(), callback);
    }

    //===========================智慧药房-商户端===============================================================
    //===========================智慧药房-用户端===============================================================
    //添加购物车商品
    public static void sendAddShopCarRequest(final String TAG, String fixmedins_code, String GoodsNo, int GoodsNum, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fixmedins_code", fixmedins_code);//机构编码
        jsonObject.put("GoodsNo", GoodsNo);//商品编码
        jsonObject.put("ShoppingCartNum", GoodsNum);//商品数量

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ADDSHOPPINGCARTGOODS_URL, jsonObject.toJSONString(), callback);
    }

    //添加购物车商品-订单
    public static void sendAddOrderShopCarRequest(final String TAG, String GoodsNo, int GoodsNum, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", GoodsNo);//商品编码
        jsonObject.put("ShoppingCartNum", GoodsNum);//商品数量

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ADDORDERSHOPPINGCARTGOODS_URL, jsonObject.toJSONString(), callback);
    }

    //删除购物车商品
    public static void sendDelShopCarRequest(final String TAG, String GoodsNo, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("GoodsNo", GoodsNo);//商品编码

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_DELSHOPPINGCARTGOODS_URL, jsonObject.toJSONString(), callback);
    }

    //清空购物车商品
    public static void sendDelAllShopCarRequest(final String TAG, String fixmedins_code, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fixmedins_code", fixmedins_code);//商品编码

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_DELALLSHOPPINGCARTGOODS_URL, jsonObject.toJSONString(), callback);
    }

    //新增收货地址-用户端
    public static void sendAddAddressRequest_user(final String TAG, String FullName, String Phone, String Location, String Address,
                                                  String Latitude, String Longitude, String AreaName, String City, String IsDefault,
                                                  final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("FullName", FullName);//姓名
        jsonObject.put("Phone", Phone);//手机号
        jsonObject.put("Location", Location);//省市区
        jsonObject.put("Address", Address);//地址
        jsonObject.put("Latitude", Latitude);//纬度
        jsonObject.put("Longitude", Longitude);//经度
        jsonObject.put("AreaName", AreaName);//省份
        jsonObject.put("City", City);//城市
        jsonObject.put("IsDefault", IsDefault);//设置默认 0不默认1默认

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ADDSHIPTOADDRESS_URL, jsonObject.toJSONString(), callback);
    }

    //修改收货地址-用户端
    public static void sendUpdateAddressRequest_user(final String TAG, String ShipToAddressId, String FullName, String Phone, String Location, String Address,
                                                     String Latitude, String Longitude, String AreaName, String City, String IsDefault,
                                                     final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ShipToAddressId", ShipToAddressId);//唯一标识
        jsonObject.put("FullName", FullName);//姓名
        jsonObject.put("Phone", Phone);//手机号
        jsonObject.put("Location", Location);//省市区
        jsonObject.put("Address", Address);//地址
        jsonObject.put("Latitude", Latitude);//纬度
        jsonObject.put("Longitude", Longitude);//经度
        jsonObject.put("AreaName", AreaName);//省份
        jsonObject.put("City", City);//省份
        jsonObject.put("IsDefault", IsDefault);//设置默认 0不默认1默认

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_UPDATEADDRESS_URL, jsonObject.toJSONString(), callback);
    }

    //删除收货地址-用户端
    public static void sendDelAddressRequest_user(final String TAG, String ShipToAddressId, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ShipToAddressId", ShipToAddressId);//唯一标识

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_DELADDRESS_URL, jsonObject.toJSONString(), callback);
    }

    //取消订单-用户端
    public static void sendCancleOrderRequest_user(final String TAG, String OrderNo, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);//订单号

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_CANCELORDER_URL, jsonObject.toJSONString(), callback);
    }

    //修改昵称
    public static void sendUpdateNickNameRequest(final String TAG, String nickname, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("nickname", nickname);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_CHANGENICKNAME_URL, jsonObject.toJSONString(), callback);
    }

    //添加意见反馈
    public static void sendAddFeedbackRequest(final String TAG, String Type, String ProblemDescription, String Phone, String accessoryId,
                                              final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Type", Type);//类型（1,功能2、其他）
        jsonObject.put("ProblemDescription", ProblemDescription);//问题描述
        jsonObject.put("Phone", Phone);//联系方式
        jsonObject.put("accessoryId", accessoryId);//附件标识

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ADDFEEDBACK_URL, jsonObject.toJSONString(), callback);
    }

    //添加足迹
    public static void sendAddTrackRequest(final String TAG, String GoodsNo, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("GoodsNo", GoodsNo);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ADDTRACK_URL, jsonObject.toJSONString(), callback);
    }

    //删除足迹
    public static void sendDelTrackRequest(final String TAG, String TrackId, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("IsAll", "0");
        jsonObject.put("TrackId", TrackId);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_DELTRACK_URL, jsonObject.toJSONString(), callback);
    }

    //添加店铺收藏
    public static void sendAddStoreCollectionRequest(final String TAG, String fixmedins_code, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fixmedins_code", fixmedins_code);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ADDSTORECOLLECTION_URL, jsonObject.toJSONString(), callback);
    }

    //删除店铺收藏
    public static void sendDelStoreCollectionRequest(final String TAG, String fixmedins_code, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fixmedins_code", fixmedins_code);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_DELSTORECOLLECTION_URL, jsonObject.toJSONString(), callback);
    }

    //添加商品收藏
    public static void sendAddGoodCollectionRequest(final String TAG, String GoodsNo, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("GoodsNo", GoodsNo);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ADDGOODSCOLLECTION_URL, jsonObject.toJSONString(), callback);
    }

    //删除商品收藏
    public static void sendDelGoodCollectionRequest(final String TAG, String GoodsNo, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("GoodsNo", GoodsNo);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_DELGOODSCOLLECTION_URL, jsonObject.toJSONString(), callback);
    }

    //删除订单
    public static void sendDelOrderRequest(final String TAG, String OrderNo, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_DELORDER_URL, jsonObject.toJSONString(), callback);
    }

    //订单确认收货
    public static void sendConfirmReceiptOrderRequest_user(final String TAG, String OrderNo, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_QUERYLOGISTICSPROGRESS_URL, jsonObject.toJSONString(), callback);
    }

    //填写问诊信息
    public static void sendAddOnlineInquiryRequest_user(final String TAG, String OrderNo, String psn_name, String mdtrt_cert_no, String sex, String age,
                                                        String AssociatedDiseases, String IsTake, String IsAllergy, String IsUntowardReaction, String Symptom,
                                                        String MedicalRecordAccessory1, String MedicalRecordAccessory2, String MedicalRecordAccessory3,
                                                        String MedicalRecordAccessory4, String MedicalRecordAccessory5,
                                                        String IsDoctor, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);
        jsonObject.put("psn_name", psn_name);
        jsonObject.put("mdtrt_cert_no", mdtrt_cert_no);
        jsonObject.put("sex", sex);
        jsonObject.put("age", age);
        jsonObject.put("AssociatedDiseases", AssociatedDiseases);
        jsonObject.put("IsTake", IsTake);
        jsonObject.put("IsAllergy", IsAllergy);
        jsonObject.put("IsUntowardReaction", IsUntowardReaction);
        jsonObject.put("Symptom", Symptom);
        jsonObject.put("MedicalRecordAccessory1", MedicalRecordAccessory1);
        jsonObject.put("MedicalRecordAccessory2", MedicalRecordAccessory2);
        jsonObject.put("MedicalRecordAccessory3", MedicalRecordAccessory3);
        jsonObject.put("MedicalRecordAccessory4", MedicalRecordAccessory4);
        jsonObject.put("MedicalRecordAccessory5", MedicalRecordAccessory5);
        jsonObject.put("IsDoctor", IsDoctor);
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ADDONLINEINQUIRY_URL, jsonObject.toJSONString(), callback);
    }

    //修改参保人疾病史
    public static void sendEditInsuredAssociatedDiseasesRequest(final String TAG, String AssociatedDiseases, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("AssociatedDiseases", AssociatedDiseases);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_EDITINSUREDASSOCIATEDDISEASES_URL, jsonObject.toJSONString(), callback);
    }

    //投诉商家
    public static void sendAddComplaintRequest(final String TAG, String ComplaintType, String Status, String Phone, String ComplaintContent,
                                               String ComplaintAccessoryId, String fixmedins_code, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ComplaintType", ComplaintType);//投诉类型
        jsonObject.put("Status", Status);//下单状态
        jsonObject.put("Phone", Phone); //联系电话
        jsonObject.put("ComplaintContent", ComplaintContent);  //投诉内容
        jsonObject.put("ComplaintAccessoryId", ComplaintAccessoryId);//投诉附件标识
        jsonObject.put("fixmedins_code", fixmedins_code); //机构编码

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ADDCOMPLAINT_URL, jsonObject.toJSONString(), callback);
    }

    //修改性别
    public static void sendChangeSexRequest(final String TAG, String sex, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sex", sex);//投诉类型

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_CHANGESEX_URL, jsonObject.toJSONString(), callback);
    }

    //提醒发货
    public static void sendRemindShipmentRequest(final String TAG, String OrderNo, final CustomerJsonCallBack<BaseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);//投诉类型

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_REMINDSHIPMENT_URL, jsonObject.toJSONString(), callback);
    }
    //===========================智慧药房-用户端===============================================================
}
