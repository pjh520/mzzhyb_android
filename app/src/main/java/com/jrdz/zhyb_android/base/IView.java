/*
 * ************************************************************
 * 文件：IView.java  模块：lib-core  项目：component
 * 当前修改时间：2019年04月23日 18:23:20
 * 上次修改时间：2019年04月13日 08:43:55
 * 作者：Cody.yi   https://github.com/codyer
 *
 * 描述：lib-core
 * Copyright (c) 2019
 * ************************************************************
 */

package com.jrdz.zhyb_android.base;

/**
 * ================================================
 * 项目名称：My Application
 * 包    名：com.example.myapplication.mvvm
 * 版    本：1.0
 * 创建日期：2021/11/16 0016
 * 描    述：baseactivity and basefragment 需要实现的接口
 * ================================================
 */
public interface IView {
    //弹出提示框
    void showShortToast(String tip);

    //显示数据加载框
    void showWaitDialog();

    //显示数据加载框
    void showWaitDialog(String tip);

    //判断数据加载框是否显示
    boolean isWaitShow();

    //隐藏数据加载框
    void hideWaitDialog();
}
