package com.jrdz.zhyb_android.base;

import android.content.Context;
import android.content.Intent;
import com.frame.compiler.utils.StringUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.base.baseWebview.BaseWebviewActivity;

/**
 * ================================================
 * 项目名称：dgonline-android
 * 包    名：com.aten.compiler.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2018/10/26
 * 描    述：webview的实现类
 * ================================================
 */
public class MyWebViewActivity extends BaseWebviewActivity {

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .statusBarColor(android.R.color.white)
                .titleBarMarginTop(mTitleBar);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        super.initData();
        x5Webview.setDesktopMode(true);
//        x5Webview.addJavascriptInterface(this, "ShareElec");//代理名
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
        super.onDownloadRequested(url, suggestedFilename, mimeType, contentLength, contentDisposition, userAgent);
        String suffix = StringUtils.generateFileSuffix(url);
//        VideoDownLoadUtils.downLoadPdf(url, "down_doc", BaseGlobal.getPdfDir(), "直播行为规范"+ suffix, new VideoDownLoadUtils.IVideoDownLoad() {
//            @Override
//            public void onDownLoadSuccess(File response, String tag) {
//                hideWaitDialog();
//                QbSdk.openFileReader(MyWebViewActivity.this, BaseGlobal.getPdfDir() + "直播行为规范"+suffix, null, new ValueCallback<String>() {
//                    @Override
//                    public void onReceiveValue(String s) {
//
//                    }
//                });
//            }
//
//            @Override
//            public void onDownLoadFail(String errorText) {
//                hideWaitDialog();
//                showShortToast("文件下载失败");
//            }
//        });
    }

    public static void newIntance(Context context, String title, String url, boolean hasNeedTitleBar, boolean hasNeedRightView) {
        Intent intent = new Intent(context, MyWebViewActivity.class);
        intent.putExtra("title",title);
        intent.putExtra("url",url);
        intent.putExtra("hasNeedTitleBar",hasNeedTitleBar);
        intent.putExtra("hasNeedRightView",hasNeedRightView);
        context.startActivity(intent);
    }
}
