package com.jrdz.zhyb_android.base;

import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.view.View;
import android.widget.FrameLayout;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customWebview.AdvancedWebView;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseWebview.BaseWebviewActivity;

/**
 * ================================================
 * 项目名称：dgonline-android
 * 包    名：com.aten.compiler.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2018/10/26
 * 描    述：webview的实现类
 * ================================================
 */
public class MyWebViewActivity_html extends BaseWebviewActivity {

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .statusBarColor(android.R.color.white)
                .titleBarMarginTop(mTitleBar);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        //添加webview
        mFlWebviewContain.removeAllViews();
        x5Webview = new AdvancedWebView(this);
        mFlWebviewContain.addView(x5Webview, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));
        //网页中的视频，上屏幕的时候，可能出现闪烁的情况，需要如下设置：Activity在onCreate时需要设置
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        x5Webview.setGeolocationEnabled(false);
        x5Webview.setMixedContentAllowed(false);
        x5Webview.setCookiesEnabled(true);
        x5Webview.setThirdPartyCookiesEnabled(true);
        x5Webview.setDesktopMode(true);
        //先阻塞加载图片
        x5Webview.getSettings().setBlockNetworkImage(true);
        mTitle = getIntent().getStringExtra("title");
        String url = getIntent().getStringExtra("url");
        boolean hasNeedTitleBar = getIntent().getBooleanExtra("hasNeedTitleBar", false);
        boolean hasNeedRightView = getIntent().getBooleanExtra("hasNeedRightView", false);
        setTitle(EmptyUtils.strEmpty(mTitle));
        if (hasNeedTitleBar) {
            mTitleBar.setVisibility(View.VISIBLE);
        } else {
            mTitleBar.setVisibility(View.GONE);
        }

        if (hasNeedRightView) {
            setRightIcon(getResources().getDrawable(R.mipmap.ic_launcher));
        }
        x5Webview.loadHtml(url);
    }

    public static void newIntance(Context context, String title, String url, boolean hasNeedTitleBar, boolean hasNeedRightView) {
        Intent intent = new Intent(context, MyWebViewActivity_html.class);
        intent.putExtra("title",title);
        intent.putExtra("url",url);
        intent.putExtra("hasNeedTitleBar",hasNeedTitleBar);
        intent.putExtra("hasNeedRightView",hasNeedRightView);
        context.startActivity(intent);
    }
}
