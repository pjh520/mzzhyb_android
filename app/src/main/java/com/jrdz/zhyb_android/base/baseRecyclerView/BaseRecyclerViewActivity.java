package com.jrdz.zhyb_android.base.baseRecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.widget.CostomLoadMoreView;
import com.frame.compiler.widget.CustRefreshLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.constant.RefreshState;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

/**
 * project:PJHAndroidFrame
 * package:com.frame.pjh_core.base
 * Created by 彭俊鸿 on 2018/6/4.
 * e-mail : 103102activity_base_recyclerview8399@qq.com
 * recyclerview activity 基类
 */

public class BaseRecyclerViewActivity extends BaseActivity implements OnRefreshListener,
        BaseQuickAdapter.RequestLoadMoreListener, BaseQuickAdapter.OnItemChildClickListener, BaseQuickAdapter.OnItemClickListener {
    private LinearLayout mLlBaseRecycler;
    protected RecyclerView mRecyclerView;
    protected CustRefreshLayout mRefreshLayout;
    
    protected BaseQuickAdapter mAdapter;
    public int mPageNum = 0;
    protected RecyclerView.LayoutManager layoutManager;

    @Override
    public int getLayoutId() {
        return R.layout.activity_base_recyclerview;
    }

    @Override
    public void initView() {
        super.initView();
        mLlBaseRecycler= findViewById(R.id.ll_baserecycler);
        mRecyclerView = findViewById(R.id.recyclerView);
        mRefreshLayout = findViewById(R.id.refreshLayout);
    }

    @Override
    public void initData() {
        super.initData();
        initAdapter();
        mAdapter.setLoadMoreView(new CostomLoadMoreView());
        mRefreshLayout.setEnableRefresh(false);
        mRefreshLayout.setEnableLoadMore(false);
        mAdapter.setEnableLoadMore(false);
        if (isOpenAnim()) {
            mAdapter.openLoadAnimation(BaseQuickAdapter.SLIDEIN_RIGHT);
            mAdapter.isFirstOnly(false);
        }
        mRecyclerView.setHasFixedSize(true);
        //设置recyclerview 的LayoutManager
        layoutManager = getLayoutManager();
        if (layoutManager == null) {
            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        }
        mRecyclerView.setLayoutManager(layoutManager);
        setEmptyView();
        isShowEmptyView();
        mRecyclerView.setAdapter(mAdapter);
        setRefreshInfo();
    }

    //RecyclerView的LayoutManager
    public RecyclerView.LayoutManager getLayoutManager() {
        return null;
    }

    //是否一进入页面就加载空布局 默认为true
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(true);
    }

    //设置recyclervie的空页面
    public void setEmptyView() {
        mAdapter.setEmptyView(R.layout.layout_empty_view, mRecyclerView);
    }

    //初始话适配器
    public void initAdapter() {
        mAdapter = new BaseAdapter_recyclerview();
    }

    //是否开启item加载动画
    public boolean isOpenAnim() {
        return false;
    }

    //设置SmartRefreshLayout的刷新 加载样式
    public void setRefreshInfo() {
        mRefreshLayout.setPrimaryColorsId(R.color.windowbackground, R.color.txt_color_666);
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(this));
        mRefreshLayout.setHeaderMaxDragRate(5);//最大显示下拉高度/Header标准高度  头部下拉高度的比例
    }

    //加载列表数据
    public void getData() {}

    @Override
    public void initEvent() {
        super.initEvent();

        mAdapter.setOnItemClickListener(this);
        mAdapter.setOnItemChildClickListener(this);
    }

    //设置smartrefresh是否需要下拉刷新以及加载更多
    public void setSmartHasRefreshOrLoadMore() {
        mRefreshLayout.setEnableRefresh(true);
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setEnableLoadMore(false);
    }

    //设置smartrefresh是否需要下拉刷新以及加载更多
    public void setLoadMore() {
        mAdapter.setEnableLoadMore(true);
        mAdapter.setOnLoadMoreListener(BaseRecyclerViewActivity.this, mRecyclerView);
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        mPageNum = 0;
        getData();
    }

    @Override
    public void onLoadMoreRequested() {
        mPageNum++;
        getData();
    }

    //加载更多时 遇到外围因素 失败时调用
    protected void setLoadMoreFail() {
        if (mAdapter!=null){
            mAdapter.loadMoreFail();
        }

        if (mPageNum > 1) {
            mPageNum -= 1;
        }
    }

    //关闭刷新的view
    public void hideRefreshView() {
        if (mRefreshLayout!=null&&mRefreshLayout.getState() == RefreshState.Refreshing) {
            mRefreshLayout.finishRefresh();
        } else {
            hideWaitDialog();
        }
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        if (ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            customItemClick(adapter,view,position);
        }
    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        if (ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            customItemChildClick(adapter,view,position);
        }
    }


    //自定义item点击事件
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {

    }

    //自定义item子view点击事件
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mLlBaseRecycler!=null){
            mLlBaseRecycler.removeAllViews();
            mLlBaseRecycler=null;
        }

        if (mAdapter!=null){
            mAdapter.getData().clear();
            mAdapter.setOnItemClickListener(null);
            mAdapter.setOnItemChildClickListener(null);
            mAdapter.setOnLoadMoreListener(null,null);
            mAdapter=null;
        }
        layoutManager=null;
        if (mRecyclerView!=null){
            mRecyclerView.removeAllViews();
            mRecyclerView.setAdapter(null);
            mRecyclerView=null;
        }
    }
}
