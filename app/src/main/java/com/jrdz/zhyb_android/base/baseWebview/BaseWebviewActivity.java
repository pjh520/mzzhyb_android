package com.jrdz.zhyb_android.base.baseWebview;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customWebview.AdvancedWebView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;

/**
 * ================================================
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2018/10/26
 * 描    述：Webview基类
 * ================================================
 */
public class BaseWebviewActivity extends BaseActivity implements AdvancedWebView.Listener {
    public AdvancedWebView x5Webview;
    public String mTitle;
    public FrameLayout mFlWebviewContain;

    @Override
    public int getLayoutId() {
        return R.layout.activity_base_webview;
    }

    @Override
    public void initView() {
        super.initView();
        mFlWebviewContain = findViewById(R.id.fl_webview_contain);
    }

    @Override
    public void initData() {
        //添加webview
        mFlWebviewContain.removeAllViews();
        x5Webview = new AdvancedWebView(this);
        mFlWebviewContain.addView(x5Webview, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));
        //网页中的视频，上屏幕的时候，可能出现闪烁的情况，需要如下设置：Activity在onCreate时需要设置
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        x5Webview.setGeolocationEnabled(false);
        x5Webview.setMixedContentAllowed(true);
        x5Webview.setCookiesEnabled(true);
        x5Webview.setThirdPartyCookiesEnabled(true);
        //先阻塞加载图片
        x5Webview.getSettings().setBlockNetworkImage(true);
        mTitle = getIntent().getStringExtra("title");
        String url = getIntent().getStringExtra("url");
        boolean hasNeedTitleBar = getIntent().getBooleanExtra("hasNeedTitleBar", false);
        boolean hasNeedRightView = getIntent().getBooleanExtra("hasNeedRightView", false);
        setTitle(EmptyUtils.strEmpty(mTitle));
        if (hasNeedTitleBar) {
            mTitleBar.setVisibility(View.VISIBLE);
        } else {
            mTitleBar.setVisibility(View.GONE);
        }

        if (hasNeedRightView) {
            setRightIcon(getResources().getDrawable(R.mipmap.ic_launcher));
        }
        x5Webview.loadUrl(url);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        x5Webview.setListener(this, this);
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        x5Webview.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        x5Webview.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (x5Webview != null) {
            //退出时调用此方法，移除绑定的服务，否则某些特定系统会报错
            x5Webview.getSettings().setJavaScriptEnabled(false);
            x5Webview.stopLoading();
            x5Webview.removeAllViewsInLayout();
            x5Webview.removeAllViews();
            x5Webview.setWebViewClient(null);

            x5Webview.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
            x5Webview.clearHistory();
            x5Webview.onDestroy();
            x5Webview = null;
        }

        super.onDestroy();

        hideWaitDialog();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        x5Webview.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onBackPressed() {
        if (!x5Webview.onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //如果按下的是回退键且历史记录里确实还有页面
        if ((keyCode == KeyEvent.KEYCODE_BACK) && x5Webview.canGoBack()) {
            x5Webview.goBack();
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        showWaitDialog();
    }

    @Override
    public void onPageFinished(String url) {
        hideWaitDialog();
        x5Webview.getSettings().setBlockNetworkImage(false);
        //判断webview是否加载了，图片资源
        if (!x5Webview.getSettings().getLoadsImagesAutomatically()) {
            //设置wenView加载图片资源
            x5Webview.getSettings().setLoadsImagesAutomatically(true);
        }
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {}

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
    }

    @Override
    public void onExternalPageRequest(String url) {

    }

    @Override
    public void onReceivedTitle(WebView view, String title) {
        if (TextUtils.isEmpty(mTitle)) {
            setTitle(title);
        }
    }

}
