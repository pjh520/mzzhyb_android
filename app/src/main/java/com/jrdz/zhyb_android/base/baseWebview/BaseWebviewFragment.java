package com.jrdz.zhyb_android.base.baseWebview;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customWebview.AdvancedWebView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;

/**
 * ================================================
 * 项目名称：PJHAndroidFrame
 * 包    名：com.frame.pjh_core.base
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2018/10/26
 * 描    述：
 * ================================================
 */
public class BaseWebviewFragment extends BaseFragment implements AdvancedWebView.Listener {
    private FrameLayout mFlWebviewContain;

    public AdvancedWebView x5Webview;
    private String title;

    @Override
    public int getLayoutId() {
        return R.layout.activity_base_webview;
    }

    @Override
    public void initView(View view) {
        super.initView(view);

        mFlWebviewContain = view.findViewById(R.id.fl_webview_contain);
    }

    @Override
    public void initData() {
        //添加webview
        mFlWebviewContain.removeAllViews();
        x5Webview = new AdvancedWebView(getContext());
        mFlWebviewContain.addView(x5Webview, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));
        //网页中的视频，上屏幕的时候，可能出现闪烁的情况，需要如下设置：Activity在onCreate时需要设置
        getActivity().getWindow().setFormat(PixelFormat.TRANSLUCENT);
        x5Webview.setGeolocationEnabled(false);
        x5Webview.setMixedContentAllowed(true);
        x5Webview.setCookiesEnabled(true);
        x5Webview.setThirdPartyCookiesEnabled(true);
        //先阻塞加载图片
        x5Webview.getSettings().setBlockNetworkImage(true);

        title = getArguments().getString("title");
        String url = getArguments().getString("url");
        boolean hasNeedTitleBar = getArguments().getBoolean("hasNeedTitleBar", false);
        boolean hasNeedRightView = getArguments().getBoolean("hasNeedRightView", false);

        setTitle(EmptyUtils.strEmpty(title));
        if (hasNeedTitleBar) {
            mTitleBar.setVisibility(View.VISIBLE);
        } else {
            mTitleBar.setVisibility(View.GONE);
        }

        if (hasNeedRightView) {
            setRightIcon(getActivity().getResources().getDrawable(R.mipmap.ic_launcher));
        }
        x5Webview.loadUrl(url);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        x5Webview.setListener(getActivity(), this);
    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();
        x5Webview.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        x5Webview.onPause();
        // ...
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (x5Webview != null) {
            ViewParent parent = x5Webview.getParent();
            if (parent != null) {
                ((ViewGroup) parent).removeView(x5Webview);
            }
            // 退出时调用此方法，移除绑定的服务，否则某些特定系统会报错
            x5Webview.getSettings().setJavaScriptEnabled(false);
            x5Webview.stopLoading();
            x5Webview.removeAllViewsInLayout();
            x5Webview.removeAllViews();
            x5Webview.setWebViewClient(null);
            try {
                CookieSyncManager.getInstance().stopSync();
            } catch (Exception e) {
                e.printStackTrace();
            }

            x5Webview.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
            x5Webview.clearHistory();
            x5Webview.destroy();
            x5Webview = null;
        }

        if (hud != null && hud.isShowing()) {
            hud.dismiss();
            hud = null;
        }
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        x5Webview.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        showWaitDialog();
    }

    @Override
    public void onPageFinished(String url) {
        hideWaitDialog();
        x5Webview.getSettings().setBlockNetworkImage(false);
        //判断webview是否加载了，图片资源
        if (!x5Webview.getSettings().getLoadsImagesAutomatically()) {
            //设置wenView加载图片资源
            x5Webview.getSettings().setLoadsImagesAutomatically(true);
        }
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength,
                                    String contentDisposition, String userAgent) {
    }

    @Override
    public void onExternalPageRequest(String url) {
    }

    @Override
    public void onReceivedTitle(WebView view, String title) {
        if (TextUtils.isEmpty(title)) {
            setTitle(title);
        }
    }
}
