package com.jrdz.zhyb_android.constant;

/**
 * ================================================
 * 项目名称：My Application
 * 包    名：com.example.myapplication.base
 * 版    本：1.0
 * 创建日期：2021/9/30 0030
 * 描    述：
 * ================================================
 */
public class Constants {
    /*获取baseurl */
    public static final String BASE_URL = "http://113.135.194.23:3080/";//113.135.194.23:3080 3080测试端口 3081正式端口 https://apitest.sxzhyb.com/  测试    https://api.sxzhyb.com/  正式
    public static final String API = "api/";//接口前缀

    //app请求api
    public static class Api {
        public static final String GET_ACTIVATEDEVICE_URL = API + "reg/register";// 激活设备
        public static final String GET_LOGIN_URL = API + "user/loginV2";// 用户登录
        public static final String GET_QUERYMEDINSINFO_URL = API + "pMedinsinfo/queryMedinsinfo";// 获取机构信息
        public static final String GET_PHONEBYACCOUNT_URL = API + "user/getPhoneByAccount";// 根据账户获取绑定的手机号
        //-----------------------------字典表--------------------------------------------
        public static final String GET_DATADICLIST_URL = API + "datadic/queryDataDic";// 字典表下载
        //-----------------------------科室--------------------------------------------
        public static final String GET_GETDEPTLIST_URL = API + "dept/deptlist";// 获取科室列表
        public static final String GET_ADDDEPT_URL = API + "dept/adddept";// 新增科室
        public static final String GET_UPDATEDEPT_URL = API + "dept/editdept";// 更新科室
        public static final String GET_DELETDEPT_URL = API + "pauseDepartmentInfo/deldept";// 删除科室
        //-----------------------------医师--------------------------------------------
        public static final String GET_DOCTORLIST_URL = API + "doctor/doctorlist";// 医师列表
        public static final String GET_ADDDOCTOR_URL = API + "doctor/adddoctor";// 医师添加
        public static final String GET_EDITDOCTOR_URL = API + "doctor/editdoctor";// 更新医师
        public static final String GET_DELDOCTOR_URL = API + "doctor/deldoctor";// 删除医师
        public static final String GET_EDITDOCTORISENABLE_URL = API + "doctor/editdoctorIsEnable";// 医师启用禁用
        public static final String GET_LOGOFFDOCTOR_URL = API + "doctor/logoffdoctor";// 注销
        //-----------------------------目录管理--------------------------------------------
        public static final String GET_WESTERNMEDICINELIST_URL = API + "WMCPMCatalogue/list";// 获取西药中成药目录
        public static final String GET_CHINESEMEDICINELIST_URL = API + "CMDPCatalogue/list";// app获取中药饮片目录
        public static final String GET_MEDICALSUPPLIESLIST_URL = API + "MedicalSupplies/list";// app获取医用耗材目录
        public static final String GET_PHAPRELIST_URL = API + "SelfPreparation/list";//app获取医疗机构自制剂目录
        public static final String GET_MEDICALSERVICELIST_URL = API + "MedicalServiceItems/list";// app获取医疗服务项目目录
        public static final String GET_CONTENTSMATCH_URL = API + "MICComparison/comparison";// 上传目录
        public static final String GET_EDIT_MICCOMPARISON_URL = API + "MICComparison/edit";// 修改目录价格（不上传省平台）
        public static final String GET_CONTENTSMATCHDELETE_URL = API + "deleteCatalogCompAudited/deleteCatalogCompAudited";// 目录对照撤销
        public static final String GET_CONTENTSMATCHLIST_URL = API + "MICComparison/list";// 目录对照列表
        public static final String GET_COMPA_SCANCODELIST_URL = API + "MICComparison/scancodelist";// 扫码查询已对照目录表
        public static final String GET_SCANCODELIST_URL = API + "WMCPMCatalogue/scancodelist";// 扫码查询西药中成药
        //-----------------------------病种目录--------------------------------------------
        public static final String GET_DIAGCATALOGUE_URL = API + "DiagCatalogue/list";// 疾病与诊断目录
        public static final String GET_OPSPDISECATALOGUE_URL = API + "OpspDiseCatalogue/list";// 门诊慢特病病种目录
        public static final String GET_TCMDISECATALOGUE_URL = API + "TCMDiseCatalogue/list";// 中医疾病目录
        public static final String GET_TCMSYNDROMECATALOGUE_URL = API + "TCMSyndromeCatalogue/list";// 中医症候目录
        //-----------------------------获取人员信息--------------------------------------------
        public static final String GET_PATIENTINFO_URL = API + "personnelInfo/query";// 获取人员信息
        public static final String GET_GETTREATMENT_URL = API + "personnelTreatmentInspection/query";// 待遇检查
        //-----------------------------药店结算--------------------------------------------
        public static final String GET_FIXMEDINSETTLE_URL = API + "settlement/pharmacySettlement";// 药店结算
        public static final String GET_FIXMEDINSETTLEDELETE_URL = API + "drugstoreSettleRevoke/drugstoreSettleRevoke";// 药店结算撤销
        public static final String GET_FIXMEDINSETTLEPRE_URL = API + "settlement/preSettlement";// 药店预结算
        //-----------------------------门诊结算--------------------------------------------
        public static final String GET_OUTPATIENTRREGISTRATION_URL = API + "outpatient/outpatientRegistrationV2";// 门诊挂号
        public static final String GET_OUTPATIENTMDTRTINFOUPA_URL = API + "outpatientMdtrtinfoUpA/outpatientMdtrtinfoUpA";// 门诊信息上传
        public static final String GET_OUTPATIENTFEELISTUP_URL = API + "outpatientFeeListUp/outpatientFeeListUp";// 门诊明细上传
        public static final String GET_SAVESETTLETMENT_URL = API + "saveSettletmentA/saveSettletmentA";// 门诊结算
        public static final String GET_UNSETTLEDOUTPATIENTINFO_URL = API + "unsettledOutpatientInfo/unsettledOutpatientInfo";// 未结算门诊信息
        public static final String GET_SETTLERECEIPT_URL = API + "TicketContent/TicketContent";// 小票
        public static final String GET_OUTPATIENTFEELISTUPCANCEL_URL = API + "outpatientFeeListUpCancel/outpatientFeeListUpCancel";// 门诊费用明细信息撤销
        public static final String GET_REGISTRATIONINFO_URL = API + "outpatient/outpatientRegistrationList";// 门诊挂号列表
        public static final String GET_OUTPATIENTREGISTRATIONDETAIL_URL = API + "outpatientRegistrationDetail/outpatientRegistrationDetail";// 门诊记录详情
        public static final String GET_OUTPAREGCANCEL_URL = API + "outpatientRegistrationCancel/outpatientRegistrationCancel";// 门诊挂号撤销
        public static final String GET_OUTPATIENTMDTRTINFOLIST_URL = API + "outpatientMdtrtinfoUpA/outpatientMdtrtinfoList";// 门诊日志列表
        public static final String GET_OUTPATIENTMDTRTINFODETAIL_URL = API + "outpatientMdtrtinfoUpA/outpatientMdtrtinfoDetail";// 门诊日志详情
        public static final String GET_SETTLEINFOLIST_URL = API + "settlement/settlementList";// 门诊结算列表
        public static final String GET_PHARMACYSETTLEMENTLIST_URL = API + "settlement/PharmacySettlementList";// 药店结算列表
        public static final String GET_CANCLESETTLETMENT_URL = API + "cancleSettletment/cancleSettletment";// 门诊结算撤销
        public static final String GET_SAVESETTLETMENTPRE_URL = API + "preSettletmentA/preSettletmentA";// 门诊预结算
        public static final String GET_SAVEOUTPATIENTRECORDS_URL = API + "saveOutpatientRecords/saveOutpatientRecords";// 门诊记录提交
        public static final String GET_ELECTRONICPRESCRIPTIONLIST_URL = API + "ElectronicPrescription/ElectronicPrescriptionList";// 电子处方列表
        public static final String GET_ELECTRONICPRESCRIPTIONDETAIL_URL = API + "electronicPrescription/electronicPrescriptionDetail";// 电子处方详情

        public static final String GET_MEDINSMONSETLAPPY_URL = API + "medinsMonSetlAppy/medinsMonSetlAppy";// 月结申报
        public static final String GET_QUERYMONSETLAPPYINFO_URL = API + "queryMonSetlAppyInfo/queryMonSetlAppyInfo";// 月结核定结果查询
        public static final String GET_FORGETPASSWORD_URL = API + "user/forgetPassword";// 忘记密码
        //-----------------------------图片上传--------------------------------------------
        public static final String GET_UPLOADFILE_URL = API + "TicketUpload/TicketUpload";// 文件上传（表单）
        public static final String GET_ATTACHMENTUPLOAD_URL = API + "attachmentUpload/attachmentUpload";// 文件上传（表单）
        public static final String GET_DELATTACHMENTUPLOAD_URL = API + "attachmentUpload/delAttachmentUpload";// 删除图片
        //-----------------------------图片上传--------------------------------------------
        //-----------------------------对总账--------------------------------------------
        public static final String GET_STMTTOTALLIST_URL = API + "stmtTotalList/stmtTotalList";// 对账查询列表
        public static final String GET_STMTINFOLIST_URL = API + "stmtTotalList/stmtTotalResultsList";// 对账结果记录列表
        public static final String GET_STMTTOTAL_URL = API + "stmtTotal/stmtTotal";// 结算对总账
        public static final String GET_STMTDETAIL_URL = API + "stmtDetail/stmtDetail";// 对明细帐
        public static final String GET_STMTDETAILRECORDLIST_URL = API + "stmtDetail/StmtDetailRecordList";// 对明细帐列表
        //-----------------------------通知公告--------------------------------------------
        public static final String GET_NOTIFY_URL = API + "medicalInsuranceInfo/list";// 通知公告
        //-----------------------------其他目录查询--------------------------------------------
        public static final String GET_QUERYHILIST_URL = API + "queryHilist/queryHilist";// 医保目录信息查询
        public static final String GET_QUERYMEDINSHILISTMAPBYPAGE_URL = API + "queryMedinsHilistMapByPage/queryMedinsHilistMapByPage";// 医疗目录与医保目录匹配信息查询
        //        public static final String GET_QUERYMEDINSHILISTMAPBYPAGE_URL =  API+"queryMedinsHilistMapByPage/queryMedinsHilistMapByPage";// 医保目录信息查询
        public static final String GET_QUERYLMTPRCBYPAGE_URL = API + "queryLmtprcByPage/queryLmtprcByPage";// 医保目录限价信息查询
        public static final String GET_QUERYSELFPAYBYPAGE_URL = API + "querySelfpayByPage/querySelfpayByPage";// 医保目录先自付比例信息查询
        //-----------------------------首页--------------------------------------------
        public static final String GET_ROTATEPICTURES_URL = API + "RotatePictures/list";// 首页轮播图
        public static final String GET_APPUPDATE_URL = API + "APPUpdate/MiZhiAPPStoreUpdate";// app检查更新
        public static final String GET_URLPREFIXSET_URL = API + "URLPrefixSet/URLPrefixSet";// 获取app配置
        public static final String GET_UPDATEADMINPWD_URL = API + "user/changePassword";// 修改密码
        public static final String GET_APPLYUNBINDING_URL = API + "ApplyUnbinding/ApplyUnbinding";// 申请解绑
        public static final String GET_QUERYMONSETL_URL = API + "queryMonSetl/queryMonSetl";// 月结查询
        public static final String GET_NEWDETAIL_URL = API + "medicalInsuranceInfo/getcontent";// 资讯内容获取
        public static final String GET_HELPCENTER_URL = API + "HelpCenter/list";// 获取帮助中心列表
        public static final String GET_HELPCENTER_DETAIL_URL = API + "HelpCenter/getcontent";// 获取帮助中心详情
        public static final String GET_SYSTEMSET_URL = API + "systemSet/getSystemSet";// 系统设置获取接口
        public static final String GET_SETSYSTEM_URL = API + "systemSet/systemSet";// 系统设置接口
        public static final String GET_APPUNIONSUPDATE_URL = API + "APPUpdate/APPUnionsUpdate";// 修改打印联数
        public static final String GET_LOGOUT_URL = API + "user/logout";// 商户端登出接口
        public static final String GET_QUERYQUESTIONNAIRETEMPLATE_URL = API + "questionnaireTemplate/queryQuestionnaireTemplate";// 获取调查问卷模板
        public static final String GET_ADDQUESTIONNAIRE_URL = API + "questionnaire/addQuestionnaire";// 提交问卷
        //-----------------------------参保人--------------------------------------------
        public static final String GET_INSUREDREG_URL = API + "insuredReg/insuredReg";// 参保人注册接口
        public static final String GET_INSUREDLOGIN_URL = API + "insuredLogin/insuredLogin";// 参保人登录接口
        public static final String GET_INSUREDSMSCODELOGIN_URL = API + "insuredLogin/insuredSmscodeLogin";// 参保人验证码登录接口
//        public static final String GET_INSURED_ROTATEPICTURES_URL = API + "RotatePictures/insuredList";// 参保人轮播图查询
        public static final String GET_INSURED_ROTATEPICTURES_URL = API + "mzhi/rotatePictureList ";// 参保人轮播图查询
        public static final String GET_INSURED_NOTIFY_URL = API + "medicalInsuranceInfo/insuredList";// 参保人资讯列表查询
        public static final String GET_INSURED_DIAGCATALOGUE_URL = API + "DiagCatalogue/insuredList";// 参保人疾病与诊断目录查询
        public static final String GET_INSURED_OPSPDISECATALOGUE_URL = API + "OpspDiseCatalogue/insuredList";// 参保人门诊慢特病种目录查询
        public static final String GET_INSURED_TCMDISECATALOGUE_URL = API + "TCMDiseCatalogue/insuredList";// 参保人中医疾病目录查询
        public static final String GET_INSURED_TCMSYNDROMECATALOGUE_URL = API + "TCMSyndromeCatalogue/insuredList";// 参保人中医证候目录查询
        public static final String GET_INSURED_PERSONNELINFO_URL = API + "personnelInfo/insuredQuery";// 参保人人员信息查询
        public static final String GET_INSURED_CHANGEPHONE_URL = API + "insuredSet/changePhone";// 参保人手机号码更换
        public static final String GET_INSURED_CHANGEPWD_URL = API + "insuredSet/changePwd";// 参保人密码修改
        public static final String GET_INSURED_FORGOTPASSWORD_URL = API + "insuredForgotPassword/insuredForgotPassword";// 参保人忘记密码接口
        public static final String GET_INSURED_UPLOAD_URL = API + "attachmentUpload/insuredUpload";// 参保人上传头像
        public static final String GET_INSURED_SENDSMS_URL = API + "sendSMS/sendSMS";// 参保人发送短信
        public static final String GET_INSURED_EDITINSURED_URL = API + "insuredReg/editInsured";// 参保人信息修改

//        public static final String GET_INSURED_FACECERTIFY_URL = API + "faceCertify/faceCertify";// 参保人人脸识别验证
//        public static final String GET_INSURED_QUERYFACECERTIFY_URL = API + "faceCertify/queryFaceCertify";// 参保人人脸识别验证查询
        public static final String GET_INSURED_FACECERTIFY_URL = API + "faceCertify/newFaceCertify";// 参保人人脸识别验证
        public static final String GET_INSURED_QUERYFACECERTIFY_URL = API + "faceCertify/newQueryFaceCertify";// 参保人人脸识别验证查询

        public static final String GET_INSURED_QUERYMEDINSINFOLIST_URL = API + "pMedinsinfo/queryAllMedinsList";// 获取所有医疗机构接口
        public static final String GET_INSURED_QUERYALLPHARMACYLIST_URL = API + "pMedinsinfo/queryAllPharmacyList";// 获取所有药店接口
        public static final String GET_INSURED_QUERYAGENCYLIST_URL = API + "pMedinsinfo/queryAgencyList";// 经办机构信息列表
        public static final String GET_INSURED_LOGOUT_URL = API + "insuredLogin/logout";// 参保人登出接口
        public static final String GET_INSURED_GETAPPVER_URL = API + "APPUpdate/GetAPPVer";// 获取app审核状态
        public static final String GET_INSURED_LOGOFF_URL = API + "insuredSet/logoffinsured";// 参保人注销功能
        public static final String GET_INSURED_SETPAYMENTPWD_URL = API + "insuredSet/setPaymentPwd";// 参保人设置企业基金交易密码
        public static final String GET_INSURED_CHANGEPAYMENTPWD_URL = API + "insuredSet/changePaymentPwd";// 参保人修改企业基金交易密码
        public static final String GET_INSURED_RESETPAYMENTPWD_URL = API + "faceCertify/ResetPaymentPwd";// 参保人重置企业基金交易密码
        public static final String GET_INSURED_GETENTERPRISEFUND_URL = API + "pEnterpriseFund/getEnterpriseFund";// 获取企业基金余额
        public static final String GET_INSURED_SPECIALDRUGLIST_URL = API + "specialDrug/specialDrugList";// 特药药品库列表
        public static final String GET_INSURED_DOCTORLIST_URL = API + "specialDrug/doctorlist";// 特药备案责任医师列表
        public static final String GET_INSURED_ADDSPECIALDRUG_URL = API + "specialDrug/addSpecialDrug";// 特药备案申请
        public static final String GET_INSURED_QUERYSPECIALDRUG_URL = API + "specialDrug/querySpecialDrug";// 特药备案查询和撤销备案查询
        public static final String GET_INSURED_SPECIALDRUGDETAIL_URL = API + "specialDrug/specialDrugDetail";// 特药备案查询和撤销备案查询
        public static final String GET_INSURED_REVOKESPECIALDRUG_URL = API + "specialDrug/revokeSpecialDrug";// 撤销特药备案申请
        public static final String GET_INSURED_EDITSPECIALDRUG_URL = API + "specialDrug/editSpecialDrug";// 特药重新备案
        public static final String GET_INSURED_QUERYOPSPDISEPHARMACYLIST_URL = API + "pMedinsinfo/queryOpspDisePharmacyList";// 慢保药店查询接口
        public static final String GET_INSURED_QUERYCOORDINATIONMEDINSLIST_URL = API + "pMedinsinfo/queryCoordinationMedinsList";// 门诊统筹定点机构查询接口
        public static final String GET_INSURED_QUERYCOORDINATIONPHARMACYLIST_URL = API + "pMedinsinfo/queryCoordinationPharmacyList";// 门诊统筹定点药店查询
        public static final String GET_INSURED_ADDCOMPLAINT_URL = API + "mzhi/addComplaint";// 米脂投诉建议新增接口
        public static final String GET_INSURED_NEWSLIST_URL = API + "mzhi/newsList";// 米脂资讯列表
        public static final String GET_INSURED_NEWSDETAIL_URL = API + "mzhi/newsDetail";// 米脂资讯详情接口
        public static final String GET_USER_GETEPAYMENTURL_URL = API + "icbc/getEpaymentUrl";// 获取E缴费医保缴费页面地址
        //-----------------------------参保人--------------------------------------------

        //-----------------------------商户端--------------------------------------------
        public static final String GET_MANA_PDRUGDATA_LIST_URL = API + "pDrugData/list";// 平台药品库列表
        public static final String GET_MANA_PSTOREDRUGDATA_LIST_URL = API + "pStoreDrugData/list";// 店铺药品库列表
        public static final String GET_MANA_DETAILDRUGDATA_URL = API + "pDrugData/detailDrugData";// 药品库详情
        public static final String GET_MANA_PSTOREDRUGDATA_ADDDRUGDATA_URL = API + "pStoreDrugData/addDrugData";// 新增店铺药品库
        public static final String GET_MANA_PSTOREDRUGDATA_EDITDRUGDATA_URL = API + "pStoreDrugData/editDrugData";// 修改店铺药品库
        public static final String GET_MANA_PSTOREDRUGDATA_DELDRUGDATA_URL = API + "pStoreDrugData/delDrugData";// 删除店铺药品库
        public static final String GET_MANA_AUTH_MEDINSINFOACTIVATION_URL = API + "pAuthenticationActivation/medinsinfoActivation";// 提交机构认证（管理员）
        public static final String GET_MANA_AUTH_QUERYMEDINSINFOACTIVATION_URL = API + "pAuthenticationActivation/queryMedinsinfoActivation";// 查询机构认证（管理员）
        public static final String GET_MANA_AUTH_LOGINACTIVATION_URL = API + "pAuthenticationActivation/loginActivation";// 立即登录
        public static final String GET_MANA_AUTH_DOCTORACTIVATIO_URL = API + "pAuthenticationActivation/doctorActivation";// 提交医师或其他人员认证
        public static final String GET_MANA_AUTH_QUERYDOCTORACTIVATION_URL = API + "pAuthenticationActivation/queryDoctorActivation";// 查看医师或其他人员认证
        public static final String GET_MANA_GETDEFAULTICON_URL = API + "pCatalogue/getDefaultIcon";// 获取默认图标
        public static final String GET_MANA_HOMEPAGELIST_URL = API + "pCatalogue/homePageList";// 获取首页目录
        public static final String GET_MANA_CLASSIFICATIONLIST_URL = API + "pCatalogue/classificationList";// 获取分类目录
        public static final String GET_MANA_ADDCATALOGUE_URL = API + "pCatalogue/addCatalogue";// 新增目录
        public static final String GET_MANA_DELCATALOGUE_URL = API + "pCatalogue/delCatalogue";// 删除目录
        public static final String GET_MANA_EDITCATALOGUE_URL = API + "pCatalogue/editCatalogue";// 修改目录
        public static final String GET_MANA_QUERYSTORESTYLE_URL = API + "pStoreInformation/queryStoreStyle";// 查询店铺风格
        public static final String GET_MANA_SAVESTORESTYLE_URL = API + "pStoreInformation/saveStoreStyle";// 保存店铺风格
        public static final String GET_MANA_GETSTORESTATUS_URL = API + "pStoreInformation/getStoreStatus";// 获取店铺状态
        public static final String GET_MANA_UPDATESTORESTATUS_URL = API + "pStoreInformation/updateStoreStatus";// 更新店铺状态
        public static final String GET_MANA_SAVESTOREINFORMATION_URL = API + "pStoreInformation/saveStoreInformation";// 保存店铺信息
        public static final String GET_MANA_QUERYSTOREINFORMATION_URL = API + "pStoreInformation/queryStoreInformation";// 查询店铺信息
        public static final String GET_MANA_QUERYLOGISTICSDISTRIBUTION_URL = API + "pStoreInformation/queryLogisticsDistribution";// 查询物流配送
        public static final String GET_MANA_QUERYLOGISTICSSET_URL = API + "pStoreInformation/queryLogisticsSet";// 查询物流设置
        public static final String GET_MANA_SAVELOGISTICSSET_URL = API + "pStoreInformation/saveLogisticsSet";// 保存物流配送--各个大区 配送城市具体信息
        public static final String GET_MANA_SAVELOGISTICSDISTRIBUTION_URL = API + "pStoreInformation/saveLogisticsDistribution";// 保存物流配送
        public static final String GET_MANA_QUERYDISTRIBUTIONSCOPE_URL = API + "pStoreInformation/queryDistributionScope";// 查询店铺配送范围
        public static final String GET_MANA_GETSTORECREDENTIALS_URL = API + "pStoreInformation/getStoreCredentials";// 我的店铺资质
        public static final String GET_MANA_UPDATESTORECREDENTIALS_URL = API + "pStoreInformation/updateStoreCredentials";// 更新店铺资质
        public static final String GET_MANA_ADDGOODS_URL = API + "pDrafts/addGoods";// 新增商品
        public static final String GET_MANA_DRAFTS_GOODS_LIST_URL = API + "pDrafts/list";// 草稿箱商品列表
        public static final String GET_MANA_DRAFTS_EDITGOODS_URL = API + "pDrafts/editGoods";// 修改草稿箱商品
        public static final String GET_MANA_PDRAFTS_DELETE_GOODS_URL = API + "pDrafts/delGoods";// 删除草稿箱商品
        public static final String GET_MANA_EDITGOODS_URL = API + "pGoods/editGoods";// 修改产品池商品
        public static final String GET_MANA_GOODS_LIST_URL = API + "pGoods/list";// 产品池商品列表
        public static final String GET_MANA_DELETE_GOODS_URL = API + "pGoods/delGoods";// 删除产品池商品
        public static final String GET_MANA_DETAILGOODS_URL = API + "pGoods/detailGoods";// 获取商品详情
        public static final String GET_MANA_ONOFFGOODS_URL = API + "pGoods/onOffGoods";// 上架下架商品
        public static final String GET_MANA_SEARCHLIST_URL = API + "pGoods/searchlist";// 用户搜索接口
        public static final String GET_MANA_CASHACCOUNT_URL = API + "pAccount/cashAccount";// 我的账户
        public static final String GET_MANA_DOCTORCASHACCOUNT_URL = API + "pAccount/doctorCashAccount";// 我的账户-医师
        public static final String GET_MANA_RECHARGECASH_URL = API + "pAccount/rechargeCash";// 新增充值
        public static final String GET_MANA_QUERYRECHARGECASH_URL = API + "pAccount/queryRechargeCash";// 充值状态查询
        public static final String GET_MANA_RECHARGERECORDLIST_URL = API + "pAccount/rechargeRecordList";// 充值记录列表
        public static final String GET_MANA_TAKECASH_URL = API + "pAccount/takeCash";// 新增提现
        public static final String GET_MANA_DOCTORTAKECASH_URL = API + "pAccount/doctorTakeCash";// 新增提现-医师
        public static final String GET_MANA_TAKERECORDLIST_URL = API + "pAccount/takeRecordList";// 提现记录列表
        public static final String GET_MANA_DEDUCTIONRECORDLIST_URL = API + "pAccount/deductionRecordList";// 扣款记录列表
        public static final String GET_MANA_MESSAGELIST_URL = API + "pMessage/messageList";// 消息列表
        public static final String GET_MANA_ISREADMESSAGE_URL = API + "pMessage/isReadMessage";// 设置消息已读
        public static final String GET_STORE_ELECTRONICPRESCRIPTIONORDERCOUNT_URL = API + "pOrder/getElectronicPrescriptionOrderCount";// 获取管理端-首页 未读消息
        public static final String GET_MANA_SYNCMEDLISTMAP_URL = API + "syncMedListMap/syncMedListMap";// 同步机构目录
        public static final String GET_STORE_ORDERLIST_URL = API + "pOrder/storeOrderlist";// 商户订单列表
        public static final String GET_STOREORDERDETAIL_URL = API + "pOrder/storeOrderDetail";// 商户订单详情
        public static final String GET_UPDATEORDER_URL = API + "pOrder/updateOrder";// 修改订单
        public static final String GET_STORECANCELORDER_URL = API + "pOrder/storeCancelOrder";// 商户取消订单
        public static final String GET_STOREDELORDER_URL = API + "pOrder/storeDelOrder";// 商户删除订单
        public static final String GET_QUERYONLINEINQUIRY_URL = API + "pOnlineInquiry/queryOnlineInquiry";// 查询问诊信息
        public static final String GET_ADDELECTRONICPRESCRIPTION_URL = API + "pOrder/addElectronicPrescription";// 新增订单处方
        public static final String GET_REMINDSHIPMENT_URL = API + "pOrder/remindShipment";// 提醒发货
        public static final String GET_STORE_ORDERSEARCHLIST_URL = API + "pOrder/storeOrderSearchlist";// 商户订单搜索
        public static final String GET_ORDERELECTRONICPRESCRIPTIONDET_URL = API + "electronicPrescription/orderElectronicPrescriptionDetail";// 查看订单处方
        public static final String GET_STORE_ORDERWITHPRESCRIPTIONLIST_URL = API + "pOrder/orderWithPrescriptionlist";// 带处方订单列表
        public static final String GET_STORE_ADDLOGISTICSPROGRESS_URL = API + "pLogisticsProgress/addLogisticsProgress";// 订单立即发货
        public static final String GET_STORE_QUERYLOGISTICSPROGRESS_URL = API + "pLogisticsProgress/queryLogisticsProgress";// 查询物流
        public static final String GET_STORE_ORDERREFUNDPROGRESS_URL = API + "pOrder/storeOrderRefundProgress";// 查看退款进度详情
        public static final String GET_STORE_SETVOICENOTIFICATION_URL = API + "systemSet/setVoiceNotification";// 设置订单语音提醒
        public static final String GET_STORE_GETDOCTORELECTRONICPRESCRIPTIONCOUNT_URL = API + "electronicPrescription/getDoctorElectronicPrescriptionCount";// 获取处方数量
        public static final String GET_STORE_ELECTRONICPRESCRIPTIONLIST_URL = API + "electronicPrescription/storeElectronicPrescriptionList";// 店铺处方
        public static final String GET_STORE_ONLINEELECTRONICPRESCRIPTIONLIST_URL = API + "electronicPrescription/onlineElectronicPrescriptionList";// 在线处方
        public static final String GET_STORE_QUERYDOCTORACTIVATIONSTATUS_URL = API + "pAuthenticationActivation/queryDoctorActivationStatus";// 获查询人员认证状态
        public static final String GET_STORE_GETSHARINGRECORD_URL = API + "pSharingRecord/getSharingRecord";// 商户端交易明细
        public static final String GET_STORE_ISQUESTIONNAIRE_URL = API + "questionnaire/IsQuestionnaire";// 是否强制开启调查问卷
        public static final String GET_MERCHANT_SHOPPINGCARTLIST_URL = API + "pShoppingCart/merchantList";// 商户端购物车商品列表
        public static final String GET_MERCHANT_ADDSHOPPINGCARTGOODS_URL = API + "pShoppingCart/addMerchantShoppingCartGoods";// 商户端新增购物车商品
        public static final String GET_MERCHANT_DELALLSHOPPINGCARTGOODS_URL = API + "pShoppingCart/delMerchantAllShoppingCartGoods";// 商户端清空购物车商品
        public static final String GET_MERCHANT_MERCHANTGOODSLIST_URL = API + "pOTO/merchantGoodslist";// 商户端商品列表
        public static final String GET_MERCHANT_SCAN_ORDER_URL = API + "pOTO/order";// 新增O2O订单
        public static final String GET_MERCHANT_SCAN_QUERYORDERSTATUS_URL = API + "pOTO/queryOrderStatus";// 查询O2O订单状态
        public static final String GET_MERCHANT_ADDORDERSHOPPINGCARTGOODS_URL = API + "pShoppingCart/addMerchantOrderShoppingCartGoods";// 商户端新增订单购物车商品-订单
        public static final String GET_MERCHANT_UPDATEORDERDISCOUNTAMOUNT_URL = API + "pOTO/updateOrderDiscountAmount";// 订单修改优惠价格接口
        public static final String GET_MERCHANT_QUERYDOCTORISONLINE_URL = API + "doctor/querydoctorIsOnline";// 查询医师在线状态接口
        public static final String GET_MERCHANT_EDITDOCTORISONLINE_URL = API + "doctor/editdoctorIsOnline";// 医师设置上下线接口
        public static final String GET_MERCHANT_UPDATEPRESSTATUS_URL = API + "pOnlineInquiry/updatePresStatus";// 修改处方问诊状态
        //-----------------------------商户端--------------------------------------------

        //-----------------------------用户端--------------------------------------------
        public static final String GET_USER_HOMEPAGELIST_URL = API + "pCatalogue/userHomePageList";// 获取首页目录
        public static final String GET_USER_DETAILCATALOGUE_URL = API + "pCatalogue/detailCatalogue";// 获取目录详情
        public static final String GET_USER_CLASSIFICATIONLIST_URL = API + "pCatalogue/userClassificationList";// 获取分类目录
        public static final String GET_USER_STORERECOMMENDLIST_URL = API + "pGoods/storeRecommendList";// 获取首页商品推荐
        public static final String GET_USER_NEARBYSTORELIST_URL = API + "pStoreInformation/nearbyStoreList";// 获取附近药房
        public static final String GET_USER_GOODSLIST_URL = API + "pGoods/goodslist";// 商品列表（全部）
        public static final String GET_USER_ZONEGOODSLIST_URL = API + "pGoods/zoneGoodsList";// 商品列表（全部）
        public static final String GET_USER_STOREGOODSDETAIL_URL = API + "pGoods/storeGoodsDetail";// 获取商品详情
        public static final String GET_USER_USERGOODSDETAIL_URL = API + "pGoods/userGoodsDetail";// 获取商品详情（带商家药品列表）
        public static final String GET_USER_STORESIMILARITYLIST_URL = API + "pGoods/storeSimilarityList";// 获取店内相似
        public static final String GET_USER_USERSEARCHLIST_URL = API + "pGoods/usersearchlist";// 获取用户搜索接口
        public static final String GET_USER_ADDSHOPPINGCARTGOODS_URL = API + "pShoppingCart/addShoppingCartGoods";// 添加购物车商品
        public static final String GET_USER_DELSHOPPINGCARTGOODS_URL = API + "pShoppingCart/delShoppingCartGoods";// 删除购物车商品
        public static final String GET_USER_DELALLSHOPPINGCARTGOODS_URL = API + "pShoppingCart/delAllShoppingCartGoods";// 清空购物车商品
        public static final String GET_USER_ADDORDERSHOPPINGCARTGOODS_URL = API + "pShoppingCart/addOrderShoppingCartGoods";// 添加购物车商品-订单
        public static final String GET_USER_SHOPPINGCARTLIST_URL = API + "pShoppingCart/list";// 购物车商品列表
        public static final String GET_USER_ADDSHIPTOADDRESS_URL = API + "pShipToAddress/addShipToAddress";// 新增收货地址
        public static final String GET_USER_ADDRESS_LIST_URL = API + "pShipToAddress/list";// 收货地址列表
        public static final String GET_USER_UPDATEADDRESS_URL = API + "pShipToAddress/editShipToAddress";// 修改收货地址
        public static final String GET_USER_DELADDRESS_URL = API + "pShipToAddress/delShipToAddress";// 删除收货地址
        public static final String GET_USER_DEFAULTADDRESS_URL = API + "pShipToAddress/defaultAddress";// 获取默认收货地址
        public static final String GET_USER_QUERYPERSONNELINFO_URL = API + "personnelInfo/queryPersonnelInfo";// 人员信息查询
        public static final String GET_USER_ORDER_URL = API + "pOrder/order";// 新增订单
        public static final String GET_USER_ORDERLIST_URL = API + "pOrder/userOrderlist";// 用户订单列表
        public static final String GET_USER_ORDERDETAIL_URL = API + "pOrder/orderDetail";// 订单详情
        public static final String GET_USER_CANCELORDER_URL = API + "pOrder/cancelOrder";// 取消订单
        public static final String GET_USER_ORDERPAYONLINE_URL = API + "pOrder/orderPayOnline";// 订单在线支付（现金支付）
        public static final String GET_USER_QUERYORDERPAYONLINE_URL = API + "pOrder/queryOrderPayOnline";// 订单在线支付状态查询
        public static final String GET_USER_ORDERPAY_URL = API + "pOrder/orderPay";// 订单立即支付（医保结算）
        public static final String GET_USER_DELORDER_URL = API + "pOrder/delOrder";// 删除订单
        public static final String GET_USER_QUERYLOGISTICSPROGRESS_URL = API + "pLogisticsProgress/confirmReceipt";// 订单确认收货
        public static final String GET_USER_ORDERREFUNDPROGRESS_URL = API + "pOrder/orderRefundProgress";// 查看退款进度详情
        public static final String GET_USER_ORDERSEARCHLIS_URL = API + "pOrder/userOrderSearchlist";// 订单搜索
        public static final String GET_USER_ISDOCTOR_URL = API + "doctor/isDoctor";// 判断机构是否有医师执业执照
        public static final String GET_USER_ADDONLINEINQUIRY_URL = API + "pOnlineInquiry/addOnlineInquiry";// 填写问诊信息
        public static final String GET_USER_EDITINSUREDASSOCIATEDDISEASES_URL = API + "insuredReg/editInsuredAssociatedDiseases";// 修改疾病史
        public static final String GET_USER_QUERYSTOREINFO_URL = API + "pStoreInformation/queryStoreInfo";// 查询店铺详情
        public static final String GET_USER_ADDCOMPLAINT_URL = API + "pComplaint/addComplaint";// 投诉商家
        public static final String GET_USER_CHANGENICKNAME_URL = API + "insuredSet/changeNickname";// 修改昵称
        public static final String GET_USER_CHANGESEX_URL = API + "insuredSet/changeSex";// 修改性别
        public static final String GET_USER_ADDFEEDBACK_URL = API + "pFeedback/addFeedback";// 添加意见反馈
        public static final String GET_USER_ADDTRACK_URL = API + "pTrack/addTrack";// 添加足迹
        public static final String GET_USER_TRACKLIST_URL = API + "pTrack/trackList";// 足迹列表
        public static final String GET_USER_DELTRACK_URL = API + "pTrack/delTrack";// 删除足迹
        public static final String GET_USER_ADDSTORECOLLECTION_URL = API + "pCollection/addStoreCollection";// 添加店铺收藏
        public static final String GET_USER_DELSTORECOLLECTION_URL = API + "pCollection/delStoreCollection";// 删除店铺收藏
        public static final String GET_USER_STORECOLLECTIONLIST_URL = API + "pCollection/storeCollectionList";// 店铺收藏列表
        public static final String GET_USER_ADDGOODSCOLLECTION_URL = API + "pCollection/addGoodsCollection";// 添加商品收藏
        public static final String GET_USER_DELGOODSCOLLECTION_URL = API + "pCollection/delGoodsCollection";// 删除商品收藏
        public static final String GET_USER_GOODSCOLLECTIONLIST_URL = API + "pCollection/goodsCollectionList";// 商品收藏列表
        public static final String GET_USER_ORDERENTERPRISEFUNDPAY_URL = API + "pOrder/orderEnterpriseFundPay";// 企业基金支付
        public static final String GET_USER_GETUSERSHARINGRECORD_URL = API + "pSharingRecord/getUserSharingRecord";// 用户端交易明细
        public static final String GET_USER_STOREINFORMATIONGOODSLIST_URL = API + "pGoods/storeInformationGoodsList";// 用户端专区药店商品显示
        public static final String GET_MANA_USERISREADMESSAGECOUNT_URL = API + "pMessage/userIsReadMessageCount";// 获取未读消息数
        public static final String GET_USER_USERMESSAGELIST_URL = API + "pMessage/userMessageList";// 用户端消息列表
        public static final String GET_USER_USERISREADMESSAGE_URL = API + "pMessage/userIsReadMessage";// 设置消息已读(用户端）
        public static final String GET_USER_USERORDERELECTRONICPRESCRIPTIONDETAIL_URL = API + "electronicPrescription/userOrderElectronicPrescriptionDetail";// 查看订单处方
        public static final String GET_USER_SCAN_ORDERENTERPRISEFUNDPAY_URL = API + "pOTO/orderEnterpriseFundPay";// 订单O2O企业基金支付
        public static final String GET_USER_QUERYSHOPPINGCARTNUM_URL = API + "pShoppingCart/queryShoppingCartNum";// 用户端获取购物车商品数
        public static final String GET_USER_QUERYPRESSTATUS_URL = API + "pOnlineInquiry/queryPresStatus";// 查询处方问诊状态
        public static final String GET_USER_QUERYONLINEINQUIRY_URL = API + "pOnlineInquiry/queryOnlineInquiry";// 查询问诊信息
        public static final String GET_USER_USERORDERELECPRESDETAIL_URL = API + "electronicPrescription/userOrderElecPresDetail";// 查询内部处方详情接口
        //-----------------------------用户端--------------------------------------------
    }

    public static class WebUrl {
        public static final String USER_AGREEMENT_URL = "MiZhiUserAgreement.html";//用户协议
        public static final String PRIVACY_POLICY_URL = "MiZhiPrivacyPolicy.html";//隐私政策
        public static final String ALIPAYS_SXSBJF_URL = "alipays://platformapi/startapp?appId=2021002131614967";//支付宝小程序--陕西社保缴费
        public static final String SXYBGF_SXSBJF_URL = "https://snsjsb.govpay.ccb.com/online/sbsm?fee=610000000001";//陕西医保-社保缴费
        public static final String SXYBGF_SXSBJF_URL2 = "https://mp.weixin.qq.com/intp/out_jump_cs?params=7chtGSsgDEQUwAs1oKKte3p6xiKx91imREEM3y%2BJrtTXZ6IwmzwLKp%2B7qIXT7e4GxltOOk8RTffsA623%2F0CGKwl9kUfqg%2B3JWhACYNhnDJAIXStGC%2ByulDvSudL7GRApxNDNOtiHm9%2FNH5rY4wFaKm8JoXpS5O3J1h0Rrm3Ca4OrV49peer4DXRKpYl4ulc0%2F9Osg2ml4ahZ9dCYe%2FgIxIMEFjOpbwM0G0ok4lxh5%2BKuvbM8u9uffBC3Iip9KzTftuUImwGf54NAKAslO5b4r%2Bebw%2FmjkpBUNaVMaCUkCA5iH7MBxkQqhiH1FvCt2RFw1qLvexrreBI1UpKczBGxtY3egObaIXGAfABOMdJIIRMOA82L%2BScXXbdH6G%2B5TihL&jump_ts=1719225651#wechat_redirect";//陕西医保-社保缴费
        public static final String GH_SXSBJF_URL = "https://fee.icbc.com.cn/servlet/H5OnlinePaymentServlet?f=ICBCqr&t=2&p=33&x=0&z=&i=UEoxNzIwMTcyMjFBU1NQMDM3MDA=&n=UEoxNzIwMTcyMjFBU1NQMDM3MDA=&l=";//工行-社保缴费

        public static final String STORESERVICEINSTRUCTIONS_URL = "StoreServiceInstructions.html";//服务使用须知
        public static final String STORESENSITIVERULES_URL = "StoreSensitiveRules.html";//敏感个人信息处理规则
        public static final String STOREUSERAGREEMENT_URL = "StoreUserAgreement.html";//用户协议-用户端
        public static final String STOREPRIVACYPOLICY_URL = "StorePrivacyPolicy.html";//隐私政策
        public static final String PLATFORMSERVICEAGREEMENT_URL = "PlatformServiceAgreement.html";//用户协议-商户端
        public static final String CASHWITHDRAWALINSTRUCTIONS_URL = "CashWithdrawalInstructions.html";//用户协议-商户端
        public static final String ICP_BEIAN_URL = "https://beian.miit.gov.cn/";//icp备案链接
    }

    public static class Configure {
        public static final Boolean IS_POS = false;//是否运行在POS机上面
        public static final Boolean IS_APP_MARKET = false;//是否是应用市场版本
        public static final String VERSIONNAME_TAG = "(20241204测试版)";//版本标记

        public static final String PHONE_TYPE = Configure.IS_POS?"3":"7";//榆林app（Android传1 ios传2 pos传3 后续可能会有其他的） 7米脂app
        public static final boolean BUGLY_LOG_ENABLE = false;//bugly 日志控制
        public static final String BUGLY_APPID = "7fc1358273";//bugly appid
        public static final String ENTERPRISE_PWD_TAG = "dkjk142s5drt";//企业基金交易密码 加密盐值
        public static final String WX_APPID = "wx8029d1996fbcf524";//微信appid
        public static final String WEZ_APPID = "gh_7635c4e2620b";//微问诊小程序原始id
        public static final String WEZ_PATH = "pages/navigate/index/index?ftAppId=f76ac86da9c13609d02addf2d1e144ec";//微问诊微信小程序路径以及ftAppId
        public static final String EBCX_APPID = "gh_53598a1163ca";//人保财险小程序原始id
    }

    public static class Action {
        public static final String SEND_RESET_APP = "send.reset.app";//重启app
        public static final String SEND_AGREE_PRIVACY = "send.agree.privacy";//通知Application这个类 用户已经同意隐私协议
        public static final String SEND_START_MAINACTIVITY_0 = "start.mainactivity_0";//跳转首页

        public static String SEND_REFRESH_DAILY_LOTTERY = "send.refresh.order_list";//刷新我的订单列表
        public static String SEND_REFRESH_ORDER_LIST_USER = "send.refresh.order_list_user";//刷新我的订单列表
    }

    //RequestCode不能为负值,也不能大于16位bit值65536
    public static class RequestCode {
        public static final int DEL_ADD_DEPART_CODE = 8001;//新增科室
        public static final int DEL_UPDATE_DEPART_CODE = 8002;//更新或者删除科室
        public static final int SELECT_DEPART_CODE = 8003;//选择科室
        public static final int ADD_DOCTOR_CODE = 8004;//新增医师
        public static final int SELECT_CATAENALIST_CODE = 8005;//选择药品
        public static final int GET_HANDWRITE_CODE = 8006;//获取手写签名数据
        public static final int GET_DOCTOR_CODE = 8007;//获取医师数据
        public static final int GET_SETLECT_DISE_CODE = 8008;//获取诊断信息数据
        public static final int GET_SETLECT_CHRONIC_DISE_CODE = 8009;//获取慢性病列表数据
        public static final int DEL_UPDATE_DOCTOR_CODE = 8010;//获取慢性病列表数据
        public static final int REQUEST_IDCARD_CAMERA_CODE = 8011;//获取身份证信息
        public static final int REQUEST_SCANCATALIST_CODE = 8012;//获取扫条形码返回数据
        public static final int GET_SETLECT_CHRONIC_DISE_CODE2 = 8013;//获取普通疾病列表数据
        public static final int GET_SMALLTICKET_CODE2 = 8014;//小票上传结果（药店，门诊结算记录页面上传小票）
        public static final int SELECT_WESTPRESCR_CATA_CODE = 8015;//西药中成药处方选择药品
        public static final int SELECT_WESTPRESCR_CODE = 8016;//选择西药中成药处方
        public static final int SELECT_CHINAPRESCR_CATA_CODE = 8017;//中药处方选择药品
        public static final int SELECT_CHINAPRESCR_CODE = 8018;//选择中药处方
        public static final int PRESCR_UPLOAD_CODE = 8019;//处方上传
        public static final int ADD_ADDRESS_USER_CODE = 8020;//添加地址--用户端
        public static final int EDIT_ADDRESS_USER_CODE = 8021;//编辑地址--用户端
        public static final int OUTPATSETTLEMENT_INCOMPLETE_CODE = 8022;//智慧医保-门诊结算
        public static final int OWNEXPENSESETTLE_INCOMPLETE_CODE = 8023;//智慧医保-自费结算
    }

    public static class AppStorage{
        public static String APP_STORE_STATUS = "1";//智慧药房-商户-店铺状态记录
        public static String QUERY_PERSONAL_CERTNO = "";//智慧医保-查询页面身份证号码记录
        public static double APP_LONGITUDE = 0;//经度
        public static double APP_LATITUDE = 0;//纬度
    }

    public static class H5URL{
        public static String H5_BASE_URL="https://ggfw.xaybjj.cn/front/#/";
        public static String H5_WECHATFORYULINLOGIN_URL = "https://jszx.ybj.shaanxi.gov.cn/hsa-pss-pw/api/wechat/wechatForYuLinLogin";//陕西医保-H5 获取token
//        public static String H5_WECHATFORYULINLOGIN_URL = "https://zwfw.shaanxi.gov.cn/ggfw/hsa-pss-pw/api/wechat/wechatForYuLinLogin";//陕西医保-H5 获取token

//        public static String H5_BASE_URL="https://zwfw.shaanxi.gov.cn/test/web/app/#/";//测试
//        public static String H5_WECHATFORYULINLOGIN_URL = "https://zwfw.shaanxi.gov.cn/test/hsa-pss-pw/api/wechat/wechatForYuLinLogin";//陕西医保-H5 获取token 测试

        public static String H5_PERSONALACCOUNT_URL = H5_BASE_URL+"personalMedicalInsuranceQuery/personalAccount";//个人账户信息查询
        public static String H5_PERSONACCOUNTDETAIL_URL = H5_BASE_URL+"personalMedicalInsuranceQuery/personAccountDetail";//个人账户明细查询
        public static String H5_TRANSFERPROOF_URL = H5_BASE_URL+"mySearch/TransferProof";//个人参保信息
        public static String H5_PERSONALMEDICALACCOUNTRETURNQUERY_URL = H5_BASE_URL+"personalMedicalInsuranceQuery/personalMedicalAccountReturnQuery";//个人医疗账户返还查询
        public static String H5_PAYMENTINFORMATIONQUERY_URL = H5_BASE_URL+"personalMedicalInsuranceQuery/paymentInformationQuery";//缴费信息查询
        public static String H5_FAMILYAUTHORIZATIONRECORD_URL = H5_BASE_URL+"familyAuthorizationRecord";//家庭共济查询
        public static String H5_ANOTHERPLACETREATREACORDAPPLY_URL = H5_BASE_URL+"myTransaction/anotherPlaceTreatReacordApply";//异地就医备案
        public static String H5_INSUREDDATAREPORT_URL = H5_BASE_URL+"insuredDataReport";//数据上报
        public static String H5_ANOTHERPLACETREATREACORDSEARCHBASE_URL = H5_BASE_URL+"myTransaction/anotherPlaceTreatReacordSearchBase";//异地就医备案查询
        public static String H5_TRANSFERREACORDSEARCH_URL = H5_BASE_URL+"mySearch/TransferReacordSearch";//医保转移查询
        public static String H5_CROSS_PROVINCE_URL = H5_BASE_URL+"myTransaction/transferMedicalInsurance/cross-province";//跨省医保转移申请
        public static String H5_PROVINCIAL_URL = H5_BASE_URL+"myTransaction/transferMedicalInsurance/provincial";//省内医保转移申请
        public static String H5_OUTPATIENTCONSUMPTIONINFORMATIONQUERY_URL = H5_BASE_URL+"personalMedicalInsuranceQuery/outpatientConsumptionInformationQuery";//消费信息查询
        public static String H5_PERSONALMEDICALRECORDSQUERY_URL = H5_BASE_URL+"personalMedicalInsuranceQuery/personalMedicalRecordsQuery";//个人就医记录查询
        public static String H5_CONSUMERECORDQUERY_URL = H5_BASE_URL+"ConsumeRecordQuery";//跨省异地就医结算信息查询
        public static String H5_ANOTHERPLACETREATREACORDQUERY_URL = H5_BASE_URL+"personalMedicalInsuranceQuery/anotherPlaceTreatReacordQuery";//跨省异地就医备案信息查询
        public static String H5_OUTPATIENTFAMILYCONSUMPTIONINFORMATIONQUERY_URL = H5_BASE_URL+"personalMedicalInsuranceQuery/outpatientFamilyConsumptionInformationQuery";//家庭共济消费查询
        public static String H5_RESIDENTINSUREGISTRATION_URL = H5_BASE_URL+"myTransaction/residentInsuRegistration";//城乡居民参保登记
        public static String H5_RESIDENTSUSPENDINSURANCE_URL = H5_BASE_URL+"myTransaction/residentSuspendInsurance";//城乡居民停保申请
        public static String H5_FAMILYBINDINGAPPLY_URL = H5_BASE_URL+"familyBindingApply";//家庭共济绑定
        public static String H5_DESIGNATEDMEDICALINSTITUTIONQUERY_URL = H5_BASE_URL+"publicQuery/designatedMedicalInstitutionQuery";//定点医疗机构查询
        public static String H5_AGENCYINSTITUTIONS_URL = H5_BASE_URL+"publicQuery/agencyInstitutions";//经办机构查询
        public static String H5_OUTPROVINCEADMDVSQUERY_URL = H5_BASE_URL+"publicQuery/outProvinceAdmdvsQuery";//跨省就医统筹区开通信息查询
        public static String H5_OUTPROVINCEORGQUERY_URL = H5_BASE_URL+"publicQuery/outProvinceOrgQuery";//跨省异地就医定点医疗机构查询

        public static String H5_TRAUMAINFOREG_URL = "https://ylws.cpic-sx.com.cn/h5/button.html";//外伤登记调查
        public static String H5_CHRONICDISEASESHANDLE_URL = "https://chronic-7g5lea1cb1edd584-1316984915.tcloudbaseapp.com/jumpTo.html?sign=ccce3bbc68d0d3264ef8cd6587717a1b&t=1693796872";//米脂慢病办理链接
    }
}
