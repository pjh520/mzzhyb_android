package com.jrdz.zhyb_android.database;

import android.os.Parcel;
import android.os.Parcelable;

import org.litepal.annotation.Column;
import org.litepal.crud.LitePalSupport;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.database
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/14
 * 描    述：
 * ================================================
 */
public class CatalogueModel implements Parcelable {
    private int id;

    private String ItemType;//目录类别                    1
    private String MIC_Code;//医疗目录编码                1
    private String ItemCode;//医药机构目录编码          1
    private String ItemName;//医药机构目录名字          1
    //西药目录
    private String reg_dosform;//注册剂型               1
    private String drug_type_name;//药品规格
    private String EnterpriseName;//生产企业名称       1
    private String ApprovalNumber;//批住文号          1
    //中药目录
    private String med_part;//药用部位          1
    private String cnvl_used;//常规用法         1
    private String natfla;//性味               1
    //医用耗材目录
    private String spec;//规格                    1
    private String spec_mol;//规格型号            1
    private String mcs_matl;//耗材材质              1
    private String reg_fil_no;//注册备案号           1
    private String prodentp_name;//生产企业名称
    //医疗机构自制剂目录
    private String dosforom;//剂型                1
    private String user_frqu;//使用频次             1
    private String each_dos;//每次用量              1
    //医疗服务项目目录
    private String prcunt;//计价单位            1
    //价格
    private String Price;//药品价格             1
    //自用字段
    @Column(ignore = true)
    public boolean choose;//是否被选中
    @Column(ignore = true)
    public int num = 1;//选择的数量
    private String Remark;//备注             1

    public CatalogueModel() {}

    public int getId() {
        return id;
    }

    public String getFixmedins_hilist_id() {
        return ItemCode;
    }

    public void setFixmedins_hilist_id(String fixmedins_hilist_id) {
        this.ItemCode = fixmedins_hilist_id;
    }

    public String getFixmedins_hilist_name() {
        return ItemName;
    }

    public void setFixmedins_hilist_name(String fixmedins_hilist_name) {
        this.ItemName = fixmedins_hilist_name;
    }

    public String getList_type() {
        return ItemType;
    }

    public void setList_type(String list_type) {
        this.ItemType = list_type;
    }

    public String getMed_list_codg() {
        return MIC_Code;
    }

    public void setMed_list_codg(String med_list_codg) {
        this.MIC_Code = med_list_codg;
    }

    public String getProdentp_name() {
        return prodentp_name;
    }

    public void setProdentp_name(String prodentp_name) {
        this.prodentp_name = prodentp_name;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        this.Price = price;
    }

    public boolean isChoose() {
        return choose;
    }

    public void setChoose(boolean choose) {
        this.choose = choose;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getDrug_spec_code() {
        return reg_dosform;
    }

    public void setDrug_spec_code(String drug_spec_code) {
        this.reg_dosform = drug_spec_code;
    }

    public String getDrug_type_name() {
        return drug_type_name;
    }

    public void setDrug_type_name(String drug_type_name) {
        this.drug_type_name = drug_type_name;
    }

    public String getEnterpriseName() {
        return EnterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        EnterpriseName = enterpriseName;
    }

    public String getAprvno_begndate() {
        return ApprovalNumber;
    }

    public void setAprvno_begndate(String aprvno_begndate) {
        this.ApprovalNumber = aprvno_begndate;
    }

    public String getMed_part() {
        return med_part;
    }

    public void setMed_part(String med_part) {
        this.med_part = med_part;
    }

    public String getCnvl_used() {
        return cnvl_used;
    }

    public void setCnvl_used(String cnvl_used) {
        this.cnvl_used = cnvl_used;
    }

    public String getNatfla() {
        return natfla;
    }

    public void setNatfla(String natfla) {
        this.natfla = natfla;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getSpec_mol() {
        return spec_mol;
    }

    public void setSpec_mol(String spec_mol) {
        this.spec_mol = spec_mol;
    }

    public String getMcs_matl() {
        return mcs_matl;
    }

    public void setMcs_matl(String mcs_matl) {
        this.mcs_matl = mcs_matl;
    }

    public String getReg_fil_no() {
        return reg_fil_no;
    }

    public void setReg_fil_no(String reg_fil_no) {
        this.reg_fil_no = reg_fil_no;
    }

    public String getDosforom() {
        return dosforom;
    }

    public void setDosforom(String dosforom) {
        this.dosforom = dosforom;
    }

    public String getUser_frqu() {
        return user_frqu;
    }

    public void setUser_frqu(String user_frqu) {
        this.user_frqu = user_frqu;
    }

    public String getEach_dos() {
        return each_dos;
    }

    public void setEach_dos(String each_dos) {
        this.each_dos = each_dos;
    }

    public String getPrcunt() {
        return prcunt;
    }

    public void setPrcunt(String prcunt) {
        this.prcunt = prcunt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.ItemType);
        dest.writeString(this.MIC_Code);
        dest.writeString(this.ItemCode);
        dest.writeString(this.ItemName);
        dest.writeString(this.reg_dosform);
        dest.writeString(this.drug_type_name);
        dest.writeString(this.EnterpriseName);
        dest.writeString(this.ApprovalNumber);
        dest.writeString(this.med_part);
        dest.writeString(this.cnvl_used);
        dest.writeString(this.natfla);
        dest.writeString(this.spec);
        dest.writeString(this.spec_mol);
        dest.writeString(this.mcs_matl);
        dest.writeString(this.reg_fil_no);
        dest.writeString(this.prodentp_name);
        dest.writeString(this.dosforom);
        dest.writeString(this.user_frqu);
        dest.writeString(this.each_dos);
        dest.writeString(this.prcunt);
        dest.writeString(this.Price);
        dest.writeByte(this.choose ? (byte) 1 : (byte) 0);
        dest.writeInt(this.num);
        dest.writeString(this.Remark);
    }

    protected CatalogueModel(Parcel in) {
        this.id = in.readInt();
        this.ItemType = in.readString();
        this.MIC_Code = in.readString();
        this.ItemCode = in.readString();
        this.ItemName = in.readString();
        this.reg_dosform = in.readString();
        this.drug_type_name = in.readString();
        this.EnterpriseName = in.readString();
        this.ApprovalNumber = in.readString();
        this.med_part = in.readString();
        this.cnvl_used = in.readString();
        this.natfla = in.readString();
        this.spec = in.readString();
        this.spec_mol = in.readString();
        this.mcs_matl = in.readString();
        this.reg_fil_no = in.readString();
        this.prodentp_name = in.readString();
        this.dosforom = in.readString();
        this.user_frqu = in.readString();
        this.each_dos = in.readString();
        this.prcunt = in.readString();
        this.Price = in.readString();
        this.choose = in.readByte() != 0;
        this.num = in.readInt();
        this.Remark= in.readString();
    }

    public static final Parcelable.Creator<CatalogueModel> CREATOR = new Parcelable.Creator<CatalogueModel>() {
        @Override
        public CatalogueModel createFromParcel(Parcel source) {
            return new CatalogueModel(source);
        }

        @Override
        public CatalogueModel[] newArray(int size) {
            return new CatalogueModel[size];
        }
    };
}
