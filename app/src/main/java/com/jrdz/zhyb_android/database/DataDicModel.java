package com.jrdz.zhyb_android.database;

import com.frame.compiler.widget.customPop.model.BottomChooseBean;
import com.frame.compiler.widget.wheel.entity.IWheelEntity;

import org.litepal.crud.LitePalSupport;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.database
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：数据字典表
 * ================================================
 */
public class DataDicModel extends LitePalSupport implements BottomChooseBean , IWheelEntity {
    private int id;

    private String label;
    private int sort;
    private String type;
    private String value;
    private String isEnable;

    public DataDicModel() {}

    public DataDicModel(String label,int sort,String type,String value,String isEnable) {
        this.label = label;
        this.sort = sort;
        this.type = type;
        this.value = value;
        this.isEnable=isEnable;
    }

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public int getSort() {
        return sort;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public String getIsEnable() {
        return isEnable;
    }

    @Override
    public String getText() {
        return label;
    }

    @Override
    public String getWheelText() {
        return label;
    }
//
//    @Override
//    public String toString() {
//        return "DataDicModel{" +
//                "id=" + id +
//                ", label='" + label + '\'' +
//                ", sort=" + sort +
//                ", type='" + type + '\'' +
//                ", value='" + value + '\'' +
//                ", isEnable='" + isEnable + '\'' +
//                '}';
//    }


    @Override
    public String toString() {
        return "DataDicModel{" +
                "label='" + label + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
