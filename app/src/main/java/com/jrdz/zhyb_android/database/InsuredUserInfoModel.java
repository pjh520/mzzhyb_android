package com.jrdz.zhyb_android.database;

import org.litepal.annotation.Encrypt;
import org.litepal.crud.LitePalSupport;

/**
 * ================================================
 * 项目名称：Pumeng_master
 * 包    名：com.languo.pumeng_master.ui.login.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/6/2 0002
 * 描    述：用户信息
 * ================================================
 */
public class InsuredUserInfoModel extends LitePalSupport {
    private int id;

    //公共数据
    @Encrypt(algorithm = AES)
    private String ssdrfrth;
    @Encrypt(algorithm = AES)
    private String mncbghdfje;
    @Encrypt(algorithm = AES)
    private String qqdsdezdgfhy;
    @Encrypt(algorithm = AES)
    private String oolputyghb;
    @Encrypt(algorithm = AES)
    private String mbmnbhfkeee;
    @Encrypt(algorithm = AES)
    private String ppolerlerkrr;
    @Encrypt(algorithm = AES)
    private String zcxbcnjfkr;
    @Encrypt(algorithm = AES)
    private String opprlkrjktjkt;
    @Encrypt(algorithm = AES)
    private String uritotooptt;
    @Encrypt(algorithm = AES)
    private String hfjfkklrr;
    @Encrypt(algorithm = AES)
    private String ududkkekehfhygujg;
    @Encrypt(algorithm = AES)
    private String jgkeuuriiotot;
    @Encrypt(algorithm = AES)
    private String hfhjkritioto;

    public InsuredUserInfoModel() {
    }

    public InsuredUserInfoModel(String phone, String name, String isFace, String id_card_no, String accessoryId, String accessoryUrl, String pwd
            , String nickname,String AssociatedDiseases,String sex,String IsSetPaymentPwd,String IsEnterpriseFundPay,String userID) {
        this.ssdrfrth = phone;
        this.mncbghdfje = name;
        this.qqdsdezdgfhy = isFace;
        this.oolputyghb = id_card_no;
        this.mbmnbhfkeee = accessoryId;
        this.ppolerlerkrr = accessoryUrl;
        this.zcxbcnjfkr = pwd;
        this.opprlkrjktjkt = nickname;
        this.uritotooptt = AssociatedDiseases;
        this.hfjfkklrr = sex;
        this.ududkkekehfhygujg = IsSetPaymentPwd;
        this.jgkeuuriiotot=IsEnterpriseFundPay;
        this.hfhjkritioto=userID;
    }

    public int getId() {
        return id;
    }

    public String getPhone() {
        return ssdrfrth;
    }

    public void setPhone(String phone) {
        this.ssdrfrth = phone;
    }

    public String getName() {
        return mncbghdfje;
    }

    public void setName(String name) {
        this.mncbghdfje = name;
    }

    public String getIsFace() {
        return qqdsdezdgfhy;
    }

    public void setIsFace(String isFace) {
        this.qqdsdezdgfhy = isFace;
    }

    public String getId_card_no() {
        return oolputyghb;
    }

    public void setId_card_no(String id_card_no) {
        this.oolputyghb = id_card_no;
    }

    public String getAccessoryId() {
        return mbmnbhfkeee;
    }

    public void setAccessoryId(String accessoryId) {
        this.mbmnbhfkeee = accessoryId;
    }

    public String getAccessoryUrl() {
        return ppolerlerkrr;
    }

    public void setAccessoryUrl(String accessoryUrl) {
        this.ppolerlerkrr = accessoryUrl;
    }

    public String getPwd() {
        return zcxbcnjfkr;
    }

    public void setPwd(String pwd) {
        this.zcxbcnjfkr = pwd;
    }

    public String getNickname() {
        return opprlkrjktjkt;
    }

    public void setNickname(String nickname) {
        this.opprlkrjktjkt = nickname;
    }

    public String getAssociatedDiseases() {
        return uritotooptt;
    }

    public void setAssociatedDiseases(String associatedDiseases) {
        uritotooptt = associatedDiseases;
    }

    public String getSex() {
        return hfjfkklrr;
    }

    public void setSex(String sex) {
        this.hfjfkklrr = sex;
    }

    public String getIsSetPaymentPwd() {
        return ududkkekehfhygujg;
    }

    public void setIsSetPaymentPwd(String isSetPaymentPwd) {
        ududkkekehfhygujg = isSetPaymentPwd;
    }

    public String getIsEnterpriseFundPay() {
        return jgkeuuriiotot;
    }

    public void setIsEnterpriseFundPay(String isEnterpriseFundPay) {
        jgkeuuriiotot = isEnterpriseFundPay;
    }

    public String getUserID() {
        return hfhjkritioto;
    }

    public void setUserID(String userID) {
        hfhjkritioto = userID;
    }
}
