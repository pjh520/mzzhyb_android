package com.jrdz.zhyb_android.database;

import org.litepal.annotation.Encrypt;
import org.litepal.crud.LitePalSupport;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.database
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/1/16
 * 描    述： 机构信息表
 * ================================================
 */
public class MechanismInfoModel extends LitePalSupport {
    private int id;

    @Encrypt(algorithm = AES)
    private String qwesaxcd;//定点医药机构编号
    @Encrypt(algorithm = AES)
    private String hnmjkoiuyty;//定点医药机构名称
    @Encrypt(algorithm = AES)
    private String azxdddfv;//就医地医保区划
    @Encrypt(algorithm = AES)
    private String vvbbnhhjjk;//定点医药机构类型
    @Encrypt(algorithm = AES)
    private String sssfffeaaa;//医院等级
    @Encrypt(algorithm = AES)
    private String nmqdfgjkkll;//药店等级
    @Encrypt(algorithm = AES)
    private String qqdfrrrtyhn;//统一社会信用代码
    @Encrypt(algorithm = AES)
    private String mkkdlfdl;//打印联数

    @Encrypt(algorithm = AES)
    private String nvmfjjfke;//手机号
    @Encrypt(algorithm = AES)
    private String eefgjtjktkl;//是否开启电子处方
    @Encrypt(algorithm = AES)
    private String kkloikejee;//是否开启门诊日志
    @Encrypt(algorithm = AES)
    private String bnvnvmfmueei;//营业执照id
    @Encrypt(algorithm = AES)
    private String pplkjdhfhf;//营业执照url
    //智慧药房版本-新增店铺审核状态
    @Encrypt(algorithm = AES)
    private String ooepeeljnvjf;//（0、从未提交审核1、审核中2、审核未通过3、审核通过4、禁止审核）
    //智慧药房版本-新增店铺入驻模式
    @Encrypt(algorithm = AES)
    private String acxcfcgtyeueieir;//(0、无模式1、固定抽佣模式2、管理服务模式)
    //智慧药房版本-订单语音提醒
    @Encrypt(algorithm = AES)
    private String gitiorororoe;//(1开启0关闭)
    //是否允许企业基金支付（0不允许 1允许）
    @Encrypt(algorithm = AES)
    private String gfhfjekekew;
    //（1是0不是）是否是O2Ｏ商家
    @Encrypt(algorithm = AES)
    private String hjgjktoyyrr;

    public MechanismInfoModel() {
    }

    //存储机构信息
    public MechanismInfoModel(String fixmedins_code, String fixmedins_name, String admdvs, String fixmedins_type, String hosp_lv,
                              String medinsLv, String uscc, String Unions, String phone, String ElectronicPrescription, String OutpatientMdtrtinfo,
                              String accessoryId, String AccessoryUrl, String ApproveStatus, String EntryMode, String VoiceNotification,
                              String IsEnterpriseFundPay,String IsOTO) {
        this.qwesaxcd = fixmedins_code;
        this.hnmjkoiuyty = fixmedins_name;
        this.azxdddfv = admdvs;
        this.vvbbnhhjjk = fixmedins_type;
        this.sssfffeaaa = hosp_lv;
        this.nmqdfgjkkll = medinsLv;
        this.qqdfrrrtyhn = uscc;
        this.mkkdlfdl = Unions;
        this.nvmfjjfke = phone;
        this.eefgjtjktkl = ElectronicPrescription;
        this.kkloikejee = OutpatientMdtrtinfo;
        this.bnvnvmfmueei = accessoryId;
        this.pplkjdhfhf = AccessoryUrl;
        this.ooepeeljnvjf = ApproveStatus;
        this.acxcfcgtyeueieir = EntryMode;
        this.gitiorororoe = VoiceNotification;
        this.gfhfjekekew = IsEnterpriseFundPay;
        this.hjgjktoyyrr=IsOTO;
    }

    public int getId() {
        return id;
    }

    public String getFixmedins_code() {
        return qwesaxcd;
    }

    public void setFixmedins_code(String fixmedins_code) {
        this.qwesaxcd = fixmedins_code;
    }

    public String getFixmedins_name() {
        return hnmjkoiuyty;
    }

    public void setFixmedins_name(String fixmedins_name) {
        this.hnmjkoiuyty = fixmedins_name;
    }

    public String getAdmdvs() {
        return azxdddfv;
    }

    public void setAdmdvs(String admdvs) {
        this.azxdddfv = admdvs;
    }

    public String getFixmedins_type() {
        return vvbbnhhjjk;
    }

    public void setFixmedins_type(String fixmedins_type) {
        this.vvbbnhhjjk = fixmedins_type;
    }

    public String getHosp_lv() {
        return sssfffeaaa;
    }

    public void setHosp_lv(String hosp_lv) {
        this.sssfffeaaa = hosp_lv;
    }

    public String getMedinsLv() {
        return nmqdfgjkkll;
    }

    public void setMedinsLv(String medinsLv) {
        this.nmqdfgjkkll = medinsLv;
    }

    public String getUscc() {
        return qqdfrrrtyhn;
    }

    public void setUscc(String uscc) {
        this.qqdfrrrtyhn = uscc;
    }

    public String getUnions() {
        return mkkdlfdl;
    }

    public void setUnions(String unions) {
        mkkdlfdl = unions;
    }

    public String getPhone() {
        return nvmfjjfke;
    }

    public void setPhone(String phone) {
        this.nvmfjjfke = phone;
    }

    public String getElectronicPrescription() {
        return eefgjtjktkl;
    }

    public void setElectronicPrescription(String electronicPrescription) {
        eefgjtjktkl = electronicPrescription;
    }

    public String getOutpatientMdtrtinfo() {
        return kkloikejee;
    }

    public void setOutpatientMdtrtinfo(String outpatientMdtrtinfo) {
        kkloikejee = outpatientMdtrtinfo;
    }

    public String getAccessoryId() {
        return bnvnvmfmueei;
    }

    public void setAccessoryId(String accessoryId) {
        this.bnvnvmfmueei = accessoryId;
    }

    public String getAccessoryUrl() {
        return pplkjdhfhf;
    }

    public void setAccessoryUrl(String accessoryUrl) {
        pplkjdhfhf = accessoryUrl;
    }

    public String getApproveStatus() {
        return ooepeeljnvjf;
    }

    public void setApproveStatus(String approveStatus) {
        ooepeeljnvjf = approveStatus;
    }

    public String getEntryMode() {
        return acxcfcgtyeueieir;
    }

    public void setEntryMode(String entryMode) {
        acxcfcgtyeueieir = entryMode;
    }

    public String getVoiceNotification() {
        return gitiorororoe;
    }

    public void setVoiceNotification(String voiceNotification) {
        gitiorororoe = voiceNotification;
    }

    public String getIsEnterpriseFundPay() {
        return gfhfjekekew;
    }

    public void setIsEnterpriseFundPay(String isEnterpriseFundPay) {
        gfhfjekekew = isEnterpriseFundPay;
    }

    public String getIsOTO() {
        return hjgjktoyyrr;
    }

    public void setIsOTO(String isOTO) {
        hjgjktoyyrr = isOTO;
    }
}
