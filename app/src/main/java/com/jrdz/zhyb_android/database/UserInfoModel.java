package com.jrdz.zhyb_android.database;

import org.litepal.annotation.Encrypt;
import org.litepal.crud.LitePalSupport;

/**
 * ================================================
 * 项目名称：Pumeng_master
 * 包    名：com.languo.pumeng_master.ui.login.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/6/2 0002
 * 描    述：用户信息
 * ================================================
 */
public class UserInfoModel extends LitePalSupport {
    private int id;

    //公共数据
    @Encrypt(algorithm = AES)
    private String cccqqqqq;//登录人类型标记 0是普通用户，1代表管理员
    @Encrypt(algorithm = AES)
    private String wwffghhhy;//医师类型
    @Encrypt(algorithm = AES)
    private String aaaasrfgvghh;//医师类型名称
    @Encrypt(algorithm = AES)
    private String bnmhjhhh;//所属科室id
    @Encrypt(algorithm = AES)
    private String wwwaaaqqf;//所属科室名称
    @Encrypt(algorithm = AES)
    private String cvcdfgg;//用户编号
    @Encrypt(algorithm = AES)
    private String afbhttftg;//用户名
    @Encrypt(algorithm = AES)
    private String qqfggbgbgb;//密码
    @Encrypt(algorithm = AES)
    private String dr_phone;//医师电话
    @Encrypt(algorithm = AES)
    private String aadfgvggb;//科别
    @Encrypt(algorithm = AES)
    private String lkkiiiijjffj;//医师资格证 图片对应的id
    @Encrypt(algorithm = AES)
    private String jjkeellwo;//医师资格证 图片
    @Encrypt(algorithm = AES)
    private String uueiojfjfjf;//医师签名 图片对应的id
    @Encrypt(algorithm = AES)
    private String oollrrkrkeeewq;//医师签名 图片
    @Encrypt(algorithm = AES)
    private String dhfhowerenf;//身份证号码
    @Encrypt(algorithm = AES)
    private String oorlfkejewjqwf;//医师申请入驻 审核状态
    @Encrypt(algorithm = AES)
    private String qqaassffgg;//医师头像 对应的url
    @Encrypt(algorithm = AES)
    private String hjkiuuyttrrr;//医师头像 图片对应的id

    public UserInfoModel() {}

    //存储医师信息
    public UserInfoModel(String type, String drType, String drTypeName, String deptId, String deptIdName,
                         String userId, String userName, String pwd,String dr_phone, String caty,String accessoryId,String accessoryUrl,
                         String subAccessoryId, String accessoryUrl1,String IDNumber,String ApproveStatus,String thrAccessoryUrl,String thrAccessoryId) {
        this.cccqqqqq = type;
        this.wwffghhhy = drType;
        this.aaaasrfgvghh = drTypeName;
        this.bnmhjhhh = deptId;
        this.wwwaaaqqf = deptIdName;
        this.cvcdfgg = userId;
        this.afbhttftg = userName;
        this.qqfggbgbgb = pwd;
        this.dr_phone = dr_phone;
        this.aadfgvggb = caty;
        this.lkkiiiijjffj = accessoryId;
        this.jjkeellwo = accessoryUrl;
        this.uueiojfjfjf = subAccessoryId;
        this.oollrrkrkeeewq = accessoryUrl1;
        this.dhfhowerenf = IDNumber;
        this.oorlfkejewjqwf = ApproveStatus;
        this.qqaassffgg = thrAccessoryUrl;
        this.hjkiuuyttrrr = thrAccessoryId;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return cccqqqqq;
    }

    public void setType(String type) {
        this.cccqqqqq = type;
    }

    public String getDrType() {
        return wwffghhhy;
    }

    public void setDrType(String drType) {
        this.wwffghhhy = drType;
    }

    public String getDrTypeName() {
        return aaaasrfgvghh;
    }

    public void setDrTypeName(String drTypeName) {
        this.aaaasrfgvghh = drTypeName;
    }

    public String getDeptId() {
        return bnmhjhhh;
    }

    public void setDeptId(String deptId) {
        this.bnmhjhhh = deptId;
    }

    public String getDeptIdName() {
        return wwwaaaqqf;
    }

    public void setDeptIdName(String deptIdName) {
        this.wwwaaaqqf = deptIdName;
    }

    public String getUserId() {
        return cvcdfgg;
    }

    public void setUserId(String userId) {
        this.cvcdfgg = userId;
    }

    public String getUserName() {
        return afbhttftg;
    }

    public void setUserName(String userName) {
        this.afbhttftg = userName;
    }

    public String getPwd() {
        return qqfggbgbgb;
    }

    public void setPwd(String pwd) {
        this.qqfggbgbgb = pwd;
    }

    public String getCaty() {
        return aadfgvggb;
    }

    public void setCaty(String caty) {
        this.aadfgvggb = caty;
    }


    public String getAccessoryId() {
        return lkkiiiijjffj;
    }

    public void setAccessoryId(String accessoryId) {
        this.lkkiiiijjffj = accessoryId;
    }

    public String getAccessoryUrl() {
        return jjkeellwo;
    }

    public void setAccessoryUrl(String accessoryUrl) {
        this.jjkeellwo = accessoryUrl;
    }

    public String getSubAccessoryId() {
        return uueiojfjfjf;
    }

    public void setSubAccessoryId(String subAccessoryId) {
        this.uueiojfjfjf = subAccessoryId;
    }

    public String getAccessoryUrl1() {
        return oollrrkrkeeewq;
    }

    public void setAccessoryUrl1(String accessoryUrl1) {
        this.oollrrkrkeeewq = accessoryUrl1;
    }

    public String getDr_phone() {
        return dr_phone;
    }

    public void setDr_phone(String dr_phone) {
        this.dr_phone = dr_phone;
    }

    public String getIDNumber() {
        return dhfhowerenf;
    }

    public void setIDNumber(String IDNumber) {
        this.dhfhowerenf = IDNumber;
    }

    public String getApproveStatus() {
        return oorlfkejewjqwf;
    }

    public void setApproveStatus(String approveStatus) {
        oorlfkejewjqwf = approveStatus;
    }

    public String getThrAccessoryUrl() {
        return qqaassffgg;
    }

    public void setThrAccessoryUrl(String thrAccessoryUrl) {
        this.qqaassffgg = thrAccessoryUrl;
    }

    public String getThrAccessoryId() {
        return hjkiuuyttrrr;
    }

    public void setThrAccessoryId(String thrAccessoryId) {
        this.hjkiuuyttrrr = thrAccessoryId;
    }
}
