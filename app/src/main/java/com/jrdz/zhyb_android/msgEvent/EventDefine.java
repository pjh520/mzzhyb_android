package com.jrdz.zhyb_android.msgEvent;

import com.jrdz.zhyb_android.ui.home.model.ApplyResultModel;
import com.jrdz.zhyb_android.ui.home.model.ScanCataRefreshModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarRefreshModel;

import cody.bus.annotation.Event;
import cody.bus.annotation.EventGroup;

/**
 * ================================================
 * 项目名称：My Application
 * 包    名：com.example.myapplication.msgEvent
 * 版    本：1.0
 * 创建日期：2021/11/2 0002
 * 描    述：
 * ================================================
 */
@EventGroup(value = "MsgBus",active = true)
public class EventDefine {
    @Event(description = "登录成功或者退出登录", multiProcess = false, active = true)
    Integer sendLoginStatus;//0 退出成功 1:登录成功

    @Event(description = "参保人端登录成功或者退出登录", multiProcess = false, active = true)
    Integer sendInsuredLoginStatus;//0 退出成功 1:登录成功

    @Event(description = "药品目录更新", multiProcess = false, active = true)
    String sendCataRefresh;

    @Event(description = "已启用药品目录更新", multiProcess = false, active = true)
    String sendCataEnableRefresh;

    @Event(description = "通知已对照目录药品信息修改", multiProcess = false, active = true)
    String sendCataEnableUpdate;

    @Event(description = "药品目录更新", multiProcess = false, active = true)
    String sendDiseRefresh;

    @Event(description = "扫码药品目录更新", multiProcess = false, active = true)
    ScanCataRefreshModel sendScanCataRefresh;

    @Event(description = "应用是否从后台进入前台", multiProcess = false, active = true)
    Boolean sendIsBackground;

    @Event(description = "对总账成功之后返回对账列表刷新", multiProcess = false, active = true)
    String sendStmlListRefresh;

    @Event(description = "更新参保人的信息", multiProcess = false, active = true)
    String updateInsuredInfo;

    @Event(description = "添加目录成功通知", multiProcess = false, active = true)
    PhaSortModel.DataBean sendHomePhaSortRefresh;

    @Event(description = "医师或者其他人员入驻智慧药房结果", multiProcess = false, active = true)
    ApplyResultModel sendApplyResult;

    @Event(description = "更新智慧医保人员的信息", multiProcess = false, active = true)
    String updateManaInfo;

    //===============================智慧药房--商户端 ================================================
    @Event(description = "药品库搜索通知", multiProcess = false, active = true)
    String sendDrugStoreHouseSearch;
    @Event(description = "添加商品成功通知", multiProcess = false, active = true)
    String sendAddGoodsRefresh;
    @Event(description = "店铺状态通知", multiProcess = false, active = true)
    String sendStoreStatus;
    @Event(description = "购物车刷新", multiProcess = false, active = true)
    ShopCarRefreshModel sendShopCarRefresh;
    @Event(description = "我开具的处方刷新", multiProcess = false, active = true)
    String sendMyIssuedPrescrRefresh;
    @Event(description = "店铺装修修改分类数据", multiProcess = false, active = true)
    String sendShopDecoSortRefresh;
    //===============================智慧药房--商户端 ================================================
    //===============================智慧药房--用户端 ================================================
    @Event(description = "用户支付结果", multiProcess = false, active = true)
    String sendPayStatusRefresh_user;
    @Event(description = "用户商品收藏或者取消收藏", multiProcess = false, active = true)
    String sendCollectStatus_user;
    @Event(description = "用户端小程序返回数据", multiProcess = false, active = true)
    String sendAppletData_user;
    @Event(description = "参保人实名认证结果", multiProcess = false, active = true)
    String sendSmrzResult_user;
    //===============================智慧药房--用户端 ================================================
}
