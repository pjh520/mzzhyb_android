package com.jrdz.zhyb_android.net;

import android.content.Intent;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.frame.compiler.net.gsonFactory.GsonFactory;
import com.frame.compiler.utils.BroadCastReceiveUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.LogUtils;
import com.frame.compiler.utils.NetworkUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.toast.ToastUtil;
import com.jrdz.zhyb_android.constant.Constants;
import com.zhy.http.okhttp.callback.StringCallback;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import okhttp3.Call;
import okhttp3.Response;

/**
 * ================================================
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2018/5/13
 * 描    述：网络请求 回调解析类
 * ================================================
 */

public abstract class CustomerJsonCallBack<T> extends StringCallback {

    @Override
    public String parseNetworkResponse(Response response, int id) throws IOException {
        String str = response.body().string();
        response.close();
        //1.获取到加密数据 进行解密
        return str;
    }

    /**
     * 该方法是子线程处理，不能做ui相关的工作
     * 主要作用是解析网络返回的 response 对象,生产onSuccess回调中需要的数据对象
     * 这里的解析工作不同的业务逻辑基本都不一样,所以需要自己实现,以下给出的时模板代码,实际使用根据需要修改
     */
    @Override
    public void onResponse(String response, int id) {
        //2.解密完之后 可以取code值进行回调操作
        if (TextUtils.isEmpty(response)) {
            ToastUtil.show("服务器开小差，请联系客服!");
            return;
        }

        JSONObject jsonObject = null;
        try {
            jsonObject = JSON.parseObject(response);
            String message = jsonObject.getString("msg");
            String code = jsonObject.getString("code");

            // 如果没有通过构造函数传进来，就自动解析父类泛型的真实类型（有局限性，继承后就无法解析到）
            T t = null;
            if ("1".equals(code)) {//成功
                try {
                    Type type = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
                    t = GsonFactory.getSingletonGson().fromJson(response, type);
                } catch (Exception e) {
                    e.printStackTrace();
                    onRequestError(t, message);
                    return;
                }
                onRequestSuccess(t);
            }else if ("2".equals(code)){//未激活的状态
                //跳转激活注册页面
                Intent intent02=new Intent(Constants.Action.SEND_START_MAINACTIVITY_0);
                intent02.putExtra("type","0");
                BroadCastReceiveUtils.sendLocalBroadCast(RxTool.getContext(),intent02);
                ToastUtil.show(EmptyUtils.strEmpty(message));
            }else if ("3".equals(code)){//被抢登的情况--参保人
                //跳转登录页面
                Intent intent02=new Intent(Constants.Action.SEND_START_MAINACTIVITY_0);
                intent02.putExtra("type","1");
                BroadCastReceiveUtils.sendLocalBroadCast(RxTool.getContext(),intent02);
                onRequestError(t, "");
            }else if ("4".equals(code)){//管理员注销
                //跳转登录页面
                Intent intent02=new Intent(Constants.Action.SEND_START_MAINACTIVITY_0);
                intent02.putExtra("type","2");
                BroadCastReceiveUtils.sendLocalBroadCast(RxTool.getContext(),intent02);
                onRequestError(t, "");
            }else if ("5".equals(code)){//其他人员注销
                //跳转登录页面
                Intent intent02=new Intent(Constants.Action.SEND_START_MAINACTIVITY_0);
                intent02.putExtra("type","3");
                BroadCastReceiveUtils.sendLocalBroadCast(RxTool.getContext(),intent02);
                onRequestError(t, message);
            }else if ("6".equals(code)){//密码被修改的情况--参保人
                //跳转登录页面
                Intent intent02=new Intent(Constants.Action.SEND_START_MAINACTIVITY_0);
                intent02.putExtra("type","1");
                BroadCastReceiveUtils.sendLocalBroadCast(RxTool.getContext(),intent02);
                onRequestError(t, "");
            }else {
                try {
                    Type type = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
                    t = GsonFactory.getSingletonGson().fromJson(response, type);
                } catch (Exception e) {
                    LogUtils.e(e.getMessage());
                    onRequestError(t, message);
                    return;
                }
                onRequestError(t, message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Call call, Exception e, int id) {
        onRequestError(null, NetworkUtils.isConnected()?e.getMessage():"请开启网络!");
    }

    //后台错误code的回调 若有用到returnData,需要判断returnData是否为空
    public abstract void onRequestError(T returnData, String msg);

    //后台成功code的回调
    public abstract void onRequestSuccess(T returnData);
}
