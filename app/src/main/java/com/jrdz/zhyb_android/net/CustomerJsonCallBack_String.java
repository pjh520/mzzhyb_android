package com.jrdz.zhyb_android.net;

import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.frame.compiler.utils.NetworkUtils;
import com.frame.compiler.widget.toast.ToastUtil;
import com.zhy.http.okhttp.callback.StringCallback;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Response;

/**
 * ================================================
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2018/5/13
 * 描    述：网络请求 回调解析类
 * ================================================
 */

public abstract class CustomerJsonCallBack_String extends StringCallback {
    @Override
    public String parseNetworkResponse(Response response, int id) throws IOException {
        String str = response.body().string();
        response.close();
        //1.获取到加密数据 进行解密
        return str;
    }

    /**
     * 该方法是子线程处理，不能做ui相关的工作
     * 主要作用是解析网络返回的 response 对象,生产onSuccess回调中需要的数据对象
     * 这里的解析工作不同的业务逻辑基本都不一样,所以需要自己实现,以下给出的时模板代码,实际使用根据需要修改
     */
    @Override
    public void onResponse(String response, int id) {
        //2.解密完之后 可以取code值进行回调操作
        if (TextUtils.isEmpty(response)) {
            ToastUtil.show("服务器开小差，请联系客服!");
            return;
        }

        JSONObject jsonObject = null;
        try {
            jsonObject = JSON.parseObject(response);
            String message = jsonObject.getString("msg");
            int code = jsonObject.getInteger("code");

            if (200 == code) {//成功
                onRequestSuccess(response);
            }else if (401 == code){//登录超时
                //跳转登录页面
//                Intent intent02=new Intent(Constants.Action.SEND_START_MAINACTIVITY_0);
//                intent02.putExtra("type","0");
//                BroadCastReceiveUtils.sendLocalBroadCast(RxTool.getContext(),intent02);
//                onRequestError(response, message);
            } else {
                onRequestError(response, message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Call call, Exception e, int id) {
        if (!NetworkUtils.isConnected()) {
            ToastUtil.show("请开启网络!");
        } else {
            onRequestError(null, e.getMessage());
        }
    }

    //后台错误code的回调 若有用到returnData,需要判断returnData是否为空
    public abstract void onRequestError(String returnData, String msg);

    //后台成功code的回调
    public abstract void onRequestSuccess(String returnData);
}
