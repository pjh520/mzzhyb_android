package com.jrdz.zhyb_android.net;

import android.util.Log;

import com.frame.compiler.utils.RxTool;
import com.qiniu.android.dns.DnsManager;
import com.qiniu.android.dns.IResolver;
import com.qiniu.android.dns.NetworkInfo;
import com.qiniu.android.dns.Record;
import com.qiniu.android.dns.dns.DnsUdpResolver;
import com.qiniu.android.dns.local.AndroidDnsServer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.Dns;

import static java.net.InetAddress.getAllByName;

/**
 * ================================================
 * 项目名称：My Application
 * 包    名：com.example.myapplication.net
 * 版    本：1.0
 * 创建日期：2021/11/8 0008
 * 描    述：
 *
 * 腾讯
 * https://www.dnspod.cn/Products/Public.DNS
 * 119.29.29.29
 * 182.254.118.118
 *
 *阿里
 * https://alidns.com/
 * 223.5.5.5
 * 223.6.6.6
 *
 * 114
 *https://www.114dns.com/
 * 114.114.114.114
 * 114.114.115.115
 *
 *国外优秀的 DNS 服务器
 *
 *Cloudflare DNS
 * DNS： 1.1.1.1
 *
 * 谷歌
 * 主 DNS：8.8.8.8
 * 辅 DNS：8.8.4.4
 *
 * Norton ConnectSafe
 * 主 DNS： 199.85.126.10
 * 辅 DNS： 199.85.127.10
 *
 * 诺顿以其出色的防病毒，互联网安全服务和产品而闻名。他们的 DNS 服务器也不会令人失望。
 *
 * OpenDNS
 * 主 DNS： 208.67.222.222
 * 辅 DNS： 208.67.220.220
 *
 * DNS Watch
 * 主 DNS： 84.200.69.80
 * 辅 DNS： 84.200.70.40
 *
 * Comodo 安全 DNS
 * 主 DNS： 8.26.56.26
 * 辅 DNS： 8.20.247.20
 *
 * 威瑞信
 * 主 DNS： 64.6.64.6
 * 辅 DNS： 64.6.65.6
 *
 * Verisign 的服务基于两个主题：作为匿名 DNS 服务器，并提供恶意软件和恶意网站的保护。他们不会向第三方出售他们的信息，也不会向用户提供任何广告。
 *
 * OpenNIC
 * 主 DNS： 192.95.54.3
 * 辅 DNS： 192.95.54.1
 *
 * GreenTeamDNS
 * 主 DNS： 81.218.119.11
 * 辅 DNS： 209.88.198.133
 * ================================================
 */
public class HttpDns implements Dns {
    private DnsManager dnsManager;

    public HttpDns() {
        IResolver[] resolvers = new IResolver[2];
        try {
            resolvers[0] = new DnsUdpResolver("119.29.29.29"); //自定义 DNS 服务器地址
            resolvers[1] = AndroidDnsServer.defaultResolver(RxTool.getContext()); //系统默认 DNS 服务器
//            resolvers[2] = new DohResolver("https://dns.alidns.com/dns-query");//阿里云
            dnsManager = new DnsManager(NetworkInfo.normal, resolvers);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<InetAddress> lookup(String hostname) throws UnknownHostException {
        if (dnsManager == null) //当构造失败时使用默认解析方式
            return Dns.SYSTEM.lookup(hostname);
        try {
            Record[] ips = dnsManager.queryRecords(hostname);//获取HttpDNS解析结果
            if (ips == null || ips.length == 0) {
                return Dns.SYSTEM.lookup(hostname);
            }

            List<InetAddress> result = new ArrayList<>();
            for (Record ip : ips) {//将ip地址数组转换成所需要的对象列表
                Log.e("66666", "ips===" + ip.value);
                result.addAll(Arrays.asList(getAllByName(ip.value)));
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        //当有异常发生时，使用默认解析
        return Dns.SYSTEM.lookup(hostname);
    }
}
