package com.jrdz.zhyb_android.net;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.utils.DeviceID;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import java.util.HashMap;

import okhttp3.MediaType;

/**
 * ================================================
 * 项目名称：dgonline-android
 * 包    名：com.aten.compiler.net
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2018/12/14
 * 描    述：请求基础类（okhttputils版本）
 * ================================================
 */
public class InsuredRequestData {
    //post请求
    public static void requesNetWork(String TAG, String url, HashMap<String, String> dataParams, Callback callback) {
        if (dataParams == null) {
            OkHttpUtils
                    .post()
                    .url(url)
                    .tag(TAG)
                    .addHeader("username", InsuredLoginUtils.getPhone())
                    .addHeader("password", InsuredLoginUtils.getPwd())
                    .addHeader("uni_code", DeviceID.getDeviceIDByMMKV())
                    .addHeader("phoneType", Constants.Configure.PHONE_TYPE)//Android传1 ios传2 后续可能会有其他的
                    .build()
                    .execute(callback);
        } else {
            OkHttpUtils
                    .post()
                    .url(url)
                    .tag(TAG)
                    .params(dataParams)
                    .addHeader("username", InsuredLoginUtils.getPhone())
                    .addHeader("password", InsuredLoginUtils.getPwd())
                    .addHeader("uni_code", DeviceID.getDeviceIDByMMKV())
                    .addHeader("phoneType", Constants.Configure.PHONE_TYPE)//Android传1 ios传2 后续可能会有其他的
                    .build()
                    .execute(callback);
        }
    }

    //get请求
    public static void requesNetWork_Get(String TAG, String url, HashMap<String, String> dataParams, Callback callback) {
        if (dataParams == null) {
            OkHttpUtils
                    .get()
                    .url(url)
                    .tag(TAG)
                    .addHeader("username", InsuredLoginUtils.getPhone())
                    .addHeader("password", InsuredLoginUtils.getPwd())
                    .addHeader("uni_code", DeviceID.getDeviceIDByMMKV())
                    .addHeader("phoneType", Constants.Configure.PHONE_TYPE)//Android传1 ios传2 后续可能会有其他的
                    .build()
                    .execute(callback);
        } else {
            OkHttpUtils
                    .get()
                    .url(url)
                    .tag(TAG)
                    .params(dataParams)
                    .addHeader("username", InsuredLoginUtils.getPhone())
                    .addHeader("password", InsuredLoginUtils.getPwd())
                    .addHeader("uni_code", DeviceID.getDeviceIDByMMKV())
                    .addHeader("phoneType", Constants.Configure.PHONE_TYPE)//Android传1 ios传2 后续可能会有其他的
                    .build()
                    .execute(callback);
        }
    }

    //post请求-json
    public static void requesNetWork_Json(String TAG, String url, String dataParams, Callback callback) {
        OkHttpUtils
                .postString()
                .url(url)
                .tag(TAG)
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .content(dataParams)
                .addHeader("username", InsuredLoginUtils.getPhone())
                .addHeader("password", InsuredLoginUtils.getPwd())
                .addHeader("uni_code", DeviceID.getDeviceIDByMMKV())
                .addHeader("phoneType", Constants.Configure.PHONE_TYPE)//Android传1 ios传2  pos传3 后续可能会有其他的
                .build()
                .execute(callback);
    }
}
