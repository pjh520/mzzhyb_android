package com.jrdz.zhyb_android.net;

import com.frame.compiler.utils.StringUtils;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.utils.DeviceID;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

import okhttp3.MediaType;

/**
 * ================================================
 * 项目名称：dgonline-android
 * 包    名：com.aten.compiler.net
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2018/12/14
 * 描    述：请求基础类（okhttputils版本）
 * ================================================
 */
public class RequestData {
    //post请求
    public static void requesNetWork(String TAG, String url, HashMap<String, String> dataParams, Callback callback) {
        try {
            if (dataParams == null) {
                OkHttpUtils
                        .post()
                        .url(url)
                        .tag(TAG)
                        .addHeader("fixmedins_code", MechanismInfoUtils.getFixmedinsCode())
                        .addHeader("fixmedins_name", URLEncoder.encode(MechanismInfoUtils.getFixmedinsName(), "UTF-8"))
                        .addHeader("username", LoginUtils.getUserId())
                        .addHeader("password", LoginUtils.getPwd())
                        .addHeader("uni_code", DeviceID.getDeviceIDByMMKV())
                        .addHeader("phoneType", Constants.Configure.PHONE_TYPE)//Android传1 ios传2 后续可能会有其他的
                        .build()
                        .execute(callback);
            } else {
                OkHttpUtils
                        .post()
                        .url(url)
                        .tag(TAG)
                        .params(dataParams)
                        .addHeader("fixmedins_code", MechanismInfoUtils.getFixmedinsCode())
                        .addHeader("fixmedins_name", URLEncoder.encode(MechanismInfoUtils.getFixmedinsName(), "UTF-8"))
                        .addHeader("username", LoginUtils.getUserId())
                        .addHeader("password", LoginUtils.getPwd())
                        .addHeader("uni_code", DeviceID.getDeviceIDByMMKV())
                        .addHeader("phoneType",Constants.Configure.PHONE_TYPE)//Android传1 ios传2 后续可能会有其他的
                        .build()
                        .execute(callback);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    //get请求
    public static void requesNetWork_Get(String TAG, String url, HashMap<String, String> dataParams, Callback callback) {
        try {
            if (dataParams == null) {
                OkHttpUtils
                        .get()
                        .url(url)
                        .tag(TAG)
                        .addHeader("fixmedins_code", MechanismInfoUtils.getFixmedinsCode())
                        .addHeader("fixmedins_name", URLEncoder.encode(MechanismInfoUtils.getFixmedinsName(), "UTF-8"))
                        .addHeader("username", LoginUtils.getUserId())
                        .addHeader("password", LoginUtils.getPwd())
                        .addHeader("uni_code", DeviceID.getDeviceIDByMMKV())
                        .addHeader("phoneType",Constants.Configure.PHONE_TYPE)//Android传1 ios传2 后续可能会有其他的
                        .build()
                        .execute(callback);
            } else {
                OkHttpUtils
                        .get()
                        .url(url)
                        .tag(TAG)
                        .params(dataParams)
                        .addHeader("fixmedins_code", MechanismInfoUtils.getFixmedinsCode())
                        .addHeader("fixmedins_name", URLEncoder.encode(MechanismInfoUtils.getFixmedinsName(), "UTF-8"))
                        .addHeader("username", LoginUtils.getUserId())
                        .addHeader("password", LoginUtils.getPwd())
                        .addHeader("uni_code", DeviceID.getDeviceIDByMMKV())
                        .addHeader("phoneType",Constants.Configure.PHONE_TYPE)//Android传1 ios传2 后续可能会有其他的
                        .build()
                        .execute(callback);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    //post请求-json
    public static void requesNetWork_Json(String TAG, String url, String dataParams,Callback callback) {
        requesNetWork_Json(TAG,url,dataParams,"",callback);
    }

    public static void requesNetWork_Json(String TAG, String url, String dataParams,String insuplc_admdvs, Callback callback) {
        try {
            OkHttpUtils
                    .postString()
                    .url(url)
                    .tag(TAG)
                    .mediaType(MediaType.parse("application/json; charset=utf-8"))
                    .content(dataParams)
                    .addHeader("fixmedins_code", MechanismInfoUtils.getFixmedinsCode())
                    .addHeader("fixmedins_name", URLEncoder.encode(MechanismInfoUtils.getFixmedinsName(), "UTF-8"))
                    .addHeader("username", LoginUtils.getUserId())
                    .addHeader("password", LoginUtils.getPwd())
                    .addHeader("uni_code", DeviceID.getDeviceIDByMMKV())
                    .addHeader("insuplc_admdvs",insuplc_admdvs)
                    .addHeader("phoneType",Constants.Configure.PHONE_TYPE)//Android传1 ios传2  pos传3 后续可能会有其他的
                    .build()
                    .execute(callback);
//                  URLDecoder.decode(token,"utf-8")//解码
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    //特殊接口---使用post请求-json 注册激活
    public static void requesNetWork_Json2(String TAG, String fixmedins_code, String fixmedins_name, String guid, String url, String dataParams, Callback callback) {
        try {
            OkHttpUtils
                    .postString()
                    .url(url)
                    .tag(TAG)
                    .mediaType(MediaType.parse("application/json; charset=utf-8"))
                    .content(dataParams)
                    .addHeader("fixmedins_code", StringUtils.replaceBlank(fixmedins_code))
                    .addHeader("fixmedins_name", URLEncoder.encode(StringUtils.replaceBlank(fixmedins_name), "UTF-8"))
                    .addHeader("username", "")
                    .addHeader("password", "")
                    .addHeader("uni_code", guid)
                    .addHeader("insuplc_admdvs","")
                    .addHeader("phoneType",Constants.Configure.PHONE_TYPE)//Android传1 ios传2 后续可能会有其他的
                    .build()
                    .execute(callback);
//                  URLDecoder.decode(token,"utf-8")//解码
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    //特殊接口---使用post请求-json  登录
    public static void requesNetWork_Json3(String TAG, String username, String password, String url, String dataParams, Callback callback) {
        try {
            OkHttpUtils
                    .postString()
                    .url(url)
                    .tag(TAG)
                    .mediaType(MediaType.parse("application/json; charset=utf-8"))
                    .content(dataParams)
                    .addHeader("fixmedins_code", "")
                    .addHeader("fixmedins_name", "")
                    .addHeader("username", StringUtils.replaceBlank(username))
                    .addHeader("password", password)
                    .addHeader("uni_code", DeviceID.getDeviceIDByMMKV())
                    .addHeader("insuplc_admdvs","")
                    .addHeader("phoneType",Constants.Configure.PHONE_TYPE)//Android传1 ios传2 后续可能会有其他的
                    .build()
                    .execute(callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //特殊接口---使用post请求-json  获取机构信息
    public static void requesNetWork_Json4(String TAG, String fixmedins_code, String fixmedins_name, String url, String dataParams, Callback callback) {
        try {
            OkHttpUtils
                    .postString()
                    .url(url)
                    .tag(TAG)
                    .mediaType(MediaType.parse("application/json; charset=utf-8"))
                    .content(dataParams)
                    .addHeader("fixmedins_code", StringUtils.replaceBlank(fixmedins_code))
                    .addHeader("fixmedins_name", URLEncoder.encode(StringUtils.replaceBlank(fixmedins_name), "UTF-8"))
                    .addHeader("username", LoginUtils.getUserId())
                    .addHeader("password", LoginUtils.getPwd())
                    .addHeader("uni_code", DeviceID.getDeviceIDByMMKV())
                    .addHeader("insuplc_admdvs","")
                    .addHeader("phoneType",Constants.Configure.PHONE_TYPE)//Android传1 ios传2 后续可能会有其他的
                    .build()
                    .execute(callback);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
