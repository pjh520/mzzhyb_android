package com.jrdz.zhyb_android.net.easyhttp;

import androidx.annotation.NonNull;

import com.frame.compiler.net.gsonFactory.GsonFactory;
import com.hjq.http.config.IRequestApi;
import com.hjq.http.request.HttpRequest;
import com.tencent.mmkv.MMKV;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.net.easyhttp
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-27
 * 描    述： Http 缓存管理器
 * ================================================
 */
public class HttpCacheManager {
    private volatile static MMKV sMmkv;

    /**
     * 获取单例的 MMKV 实例
     */
    public static MMKV getMmkv() {
        if(sMmkv == null) {
            synchronized (RequestHandler.class) {
                if (sMmkv == null) {
                    sMmkv = MMKV.mmkvWithID("http_cache_id");
                }
            }
        }
        return sMmkv;
    }

    /**
     * 生成缓存的 key
     */
    public static String generateCacheKey(@NonNull HttpRequest<?> httpRequest) {
        IRequestApi requestApi = httpRequest.getRequestApi();
        return "用户 id" + "\n" + requestApi.getApi() + "\n" + GsonFactory.getSingletonGson().toJson(requestApi);
    }
}
