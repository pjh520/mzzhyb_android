package com.jrdz.zhyb_android.net.easyhttp;
import com.hjq.http.exception.HttpException;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.net.easyhttp
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-27
 * 描    述：Token 失效异常
 * ================================================
 */
public class TokenException extends HttpException {

    public TokenException(String message) {
        super(message);
    }

    public TokenException(String message, Throwable cause) {
        super(message, cause);
    }
}
