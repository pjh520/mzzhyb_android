//package com.jrdz.zhyb_android.pos;
//
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Handler;
//import android.os.Looper;
//import android.util.Log;
//
//
///**
// * ================================================
// * 项目名称：zhyb_android
// * 包    名：com.jrdz.zhyb_android.pos
// * 作    者：彭俊鸿
// * 邮    箱：1031028399@qq.com
// * 版    本：1.0
// * 创建日期：2022-04-28
// * 描    述：监听开机广播实现应用开机自启动
// * ================================================
// */
//public class BootReceiver extends BroadcastReceiver {
//    Handler handler=new Handler(Looper.getMainLooper());
//
//    @Override
//    public void onReceive(Context context, Intent intent) {
//        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    Intent toIntent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
//                    context.startActivity(toIntent);
//                }
//            }, 2000);
//        }
//    }
//}
