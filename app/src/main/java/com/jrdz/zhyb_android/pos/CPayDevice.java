package com.jrdz.zhyb_android.pos;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import com.centerm.smartpos.aidl.sys.AidlDeviceManager;

/**
 * @author 拜雨
 * @date 2020-03
 * @desc 底层硬件调用
 */
public class CPayDevice {

    private final static String DEVICE_PACKAGE = "com.centerm.smartposservice";
    private final static String DEVICE_ACTION = "com.centerm.smartpos.service.MANAGER_SERVICE";

    private static AidlDeviceManager manager = null;
    private static CPayID cPayID;
    private static CPayPrint cPayPrint;
    private static CPaySystem cPaySystem;
//    private static CPayScan cPayScan;

//    public static CPayScan getCPayScan() {
//        if (cPayScan == null) {
//            throw new NullPointerException("扫码模块为空");
//        }
//        return cPayScan;
//    }

    public static CPayID getCPayID() {
        if (cPayID == null) {
            throw new NullPointerException("身份证模块为空");
        }
        return cPayID;
    }

    public static CPayPrint getCPayPrint() {
        if (cPayPrint == null) {
            throw new NullPointerException("打印模块为空");
        }
        return cPayPrint;
    }

    public static CPaySystem getCPaySystem() {
        if (cPaySystem == null) {
            throw new NullPointerException("打印模块为空");
        }
        return cPaySystem;
    }

    public static void initDevice(Context context) {
        cPayID = new CPayID(context);
        Intent intent = new Intent();
        intent.setPackage(DEVICE_PACKAGE);
        intent.setAction(DEVICE_ACTION);
        context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
    }

    /**
     * 服务连接桥
     */
    private static ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            manager = null;
            Log.d("CPayDevice","^_^  设备服务已关闭");
            cPayPrint = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            manager = AidlDeviceManager.Stub.asInterface(service);
            Log.d("CPayDevice","^_^  设备服务已绑定");
            cPayPrint = new CPayPrint(manager);
            cPaySystem = new CPaySystem(manager);
//            cPayScan= new CPayScan(manager);
        }
    };
}
