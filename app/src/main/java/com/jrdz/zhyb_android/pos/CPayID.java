package com.jrdz.zhyb_android.pos;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;

import com.centerm.cpay.ai.lib.CpayAiService;
import com.centerm.cpay.ai.lib.OnResultCallbackListener;
import com.frame.compiler.utils.LogUtils;

/**
 * @author 拜雨
 * @date 2020-03
 * @desc 身份证识别
 */
public class CPayID {
    private static final String AI_ACTION = "com.centerm.cpay.ai.service";
    private static final String AI_PACKAGE = "com.centerm.cpay.ai";
    /*服务商提供的id*/
    private static final String APP_ID_VALUE = "4D21A2E0F938BEB7F89694C40BDD56741FE781D035161B6929861B49CB1F2AA2";
    private static final String APP_ID_KEY = "appId";
    /*服务商提供的密钥*/
    private static final String APP_SECRET_VALUE = "F558787080EA2AF66DED0E245B9BE37993A46C195087B25D5B0B59D5ED473C34";
    private static final String APP_SECRET_KEY = "appSecret";

    /*超时时间，单位秒*/
    private static final int TIME_OUT_VALUE = 30;
    private static final String TIME_OUT_KEY = "timeout";
    /*身份证图片压缩类型
     *“0”=不需要身份证图片
     *“1”=压缩身份证图片
     *“2”=不压缩身份证图片
     */
    private static final String TYPE_VALUE = "1";
    private static final String TYPE_KEY = "type";

    private CpayAiService cpayaiService;

    private CPayID() {}

    CPayID(Context context) {
        Intent intent = new Intent();
        intent.setPackage(AI_PACKAGE);
        intent.setAction(AI_ACTION);
        ServiceConnection aiConn = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
                cpayaiService = null;
                LogUtils.e("^_^  身份证服务已关闭");
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                LogUtils.e("^_^  身份证服务已绑定");
                cpayaiService = CpayAiService.Stub.asInterface(service);
                Bundle bundle = new Bundle();
                bundle.putString(APP_ID_KEY, APP_ID_VALUE);
                bundle.putString(APP_SECRET_KEY, APP_SECRET_VALUE);
                try {
                    boolean result = cpayaiService.login(bundle);
                    if (!result) {
                        LogUtils.e("-_- 身份证认证登陆失败");
                    } else {
                        LogUtils.e("^_^ 身份证认证登陆成功");
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                    LogUtils.e("-_- 身份证认证登陆失败");
                }
            }
        };
        context.bindService(intent, aiConn, Context.BIND_AUTO_CREATE);
    }

    public void startDetect(OnResultCallbackListener listener) {
        try {
            Bundle data = new Bundle();
            data.putInt(TIME_OUT_KEY, TIME_OUT_VALUE);
            data.putString(TYPE_KEY, TYPE_VALUE);
            cpayaiService.detectIDCard(data, listener);
        } catch (RemoteException e) {
            LogUtils.e("-_-  身份证识别异常:" + e);
            try {
                listener.onResult(-1, null);
            } catch (RemoteException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void stopDetect() {
        try {
            cpayaiService.stopDetectIDCard();
        } catch (Exception e) {
            LogUtils.e("-_-  身份证识别异常:" + e);
        }
    }

    /**
     * @param code
     * @return 2    参数异常
     * 3    库中无数据
     * 4    系统异常
     * 5    读卡超时
     * 6    网络错误
     * 7    手动取消
     * 11    超时错误
     * 12    非法终端
     * 13    次数用尽
     * 14    查询异常
     */
    public String errorInfo(int code) {
        if (code == 2) {
            return "参数异常";
        } else if (code == 3) {
            return "库中无数据";
        } else if (code == 4) {
            return "系统异常";
        } else if (code == 5) {
            return "读卡超时";
        } else if (code == 6) {
            return "网络错误";
        } else if (code == 7) {
            return "手动取消";
        } else if (code == 11) {
            return "超时错误";
        } else if (code == 12) {
            return "非法终端";
        } else if (code == 13) {
            return "次数用尽";
        } else if (code == 14) {
            return "查询异常";
        } else {
            return "未知错误";
        }
    }
}
