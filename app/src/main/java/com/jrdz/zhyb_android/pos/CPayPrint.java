package com.jrdz.zhyb_android.pos;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;

import com.centerm.smartpos.aidl.printer.AidlPrinter;
import com.centerm.smartpos.aidl.printer.AidlPrinterStateChangeListener;
import com.centerm.smartpos.aidl.printer.PrintDataObject;
import com.centerm.smartpos.aidl.printer.PrinterParams;
import com.centerm.smartpos.aidl.sys.AidlDeviceManager;
import com.centerm.smartpos.constant.Constant;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.LogUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 拜雨
 * @date 2020-03
 * @desc
 */
public class CPayPrint {

    private AidlPrinter printDev = null;
//    private ExecutorService printService;
    private Handler mHandler = new Handler(Looper.getMainLooper());

    private CPayPrint() {}

    CPayPrint(AidlDeviceManager manager) {
        try {
            /*连接设备*/
            printDev = AidlPrinter.Stub.asInterface(manager.getDevice(Constant.DEVICE_TYPE.DEVICE_TYPE_PRINTERDEV));
            /*初始化打印参数*/
            printDev.initPrinter();
            /*设置灰度*/
            printDev.setPrinterGray(0x03);
            /*支持队列打印*/
            printDev.setPrintQueue(true);
//            printService = Executors.newFixedThreadPool(2);
        } catch (Exception e) {
            LogUtils.e("打印机初始化失败");
            e.printStackTrace();
        }
    }

    public void print(Bitmap bitmap, PrintCallback callback) throws RemoteException {
        int state = printDev.getPrinterState();

        if (state == 0) {
            printDev.printBmpFast(bitmap, Constant.ALIGN.CENTER, new AidlPrinterStateChangeListener.Stub() {
                @Override
                public void onPrintFinish() throws RemoteException {
                    mHandler.post(() -> callback.onCall(9999, "打印完成"));
                }

                @Override
                public void onPrintError(int i) throws RemoteException {
                    mHandler.post(() -> callback.onCall(i, getErrorMsg(i)));
                }

                @Override
                public void onPrintOutOfPaper() throws RemoteException {
                    mHandler.post(() -> callback.onCall(4104, getErrorMsg(4104)));
                }
            });
            printDev.spitPaper(120);
            return;
        }

        callback.onCall(state, getErrorMsg(state));
    }

    public void printSingle(String text, PrintCallback callback) throws Exception {
        int state = printDev.getPrinterState();

        if (state == 0) {
            List<PrintDataObject> list = new ArrayList<>();
            list.add(new PrintDataObject(text));
            list.add(new PrintDataObject("                  "));
            list.add(new PrintDataObject("                  "));

            printDev.printText(list, new AidlPrinterStateChangeListener.Stub() {
                @Override
                public void onPrintFinish() throws RemoteException {
                    mHandler.post(() -> callback.onCall(9999, "打印完成"));
                }

                @Override
                public void onPrintError(int i) throws RemoteException {
                    mHandler.post(() -> callback.onCall(i, getErrorMsg(i)));
                }

                @Override
                public void onPrintOutOfPaper() throws RemoteException {
                    mHandler.post(() -> callback.onCall(4104, getErrorMsg(4104)));
                }
            });
            printDev.spitPaper(120);
            return;
        }

        callback.onCall(state, getErrorMsg(state));
    }

    public void printDatas(String text,Bitmap bitmap, PrintCallback callback) throws Exception {
        int state = printDev.getPrinterState();

        if (state == 0) {
            List<PrinterParams> list = new ArrayList<>();

            //存根联
            PrinterParams paramTextCG01=new PrinterParams();
            paramTextCG01.setDataType(PrinterParams.DATATYPE.TEXT);
            paramTextCG01.setText("===存根联(医保局留存)===");
            paramTextCG01.setBold(true);
            paramTextCG01.setTextSize(28);
            //存根联
            PrinterParams paramTextCG02=new PrinterParams();
            paramTextCG02.setDataType(PrinterParams.DATATYPE.TEXT);
            paramTextCG02.setText("\n\n===存根联(医疗机构留存)===");
            paramTextCG02.setBold(true);
            paramTextCG02.setTextSize(28);
            //客户联
            PrinterParams paramTextKH=new PrinterParams();
            paramTextKH.setDataType(PrinterParams.DATATYPE.TEXT);
            paramTextKH.setText("\n\n========客户联========");
            paramTextKH.setBold(true);
            paramTextKH.setTextSize(28);

            PrinterParams paramText=new PrinterParams();
            paramText.setDataType(PrinterParams.DATATYPE.TEXT);
            paramText.setText(text);

            PrinterParams paramImg=new PrinterParams();
            paramImg.setDataType(PrinterParams.DATATYPE.IMAGE);
            paramImg.setBitmap(bitmap);
            paramImg.setAlign(PrinterParams.ALIGN.CENTER);

            int unions = EmptyUtils.isEmpty(MechanismInfoUtils.getUnions())?3:Integer.valueOf(MechanismInfoUtils.getUnions());

            if (unions>=1){
                //存根联（医保局留存）
                list.add(paramTextCG01);
                list.add(paramText);
                list.add(paramImg);
            }

            if (unions>=2){
                //存根联（医疗机构留存）
                list.add(paramTextCG02);
                list.add(paramText);
                list.add(paramImg);
            }

            if (unions>=3){
                //客户联
                list.add(paramTextKH);
                list.add(paramText);
                list.add(paramImg);
            }

            printDev.printDatas(list, new AidlPrinterStateChangeListener.Stub() {
                @Override
                public void onPrintFinish() throws RemoteException {
                    mHandler.post(() -> callback.onCall(9999, "打印完成"));
                }

                @Override
                public void onPrintError(int i) throws RemoteException {
                    mHandler.post(() -> callback.onCall(i, getErrorMsg(i)));
                }

                @Override
                public void onPrintOutOfPaper() throws RemoteException {
                    mHandler.post(() -> callback.onCall(4104, getErrorMsg(4104)));
                }
            });
            printDev.spitPaper(120);
            return;
        }

        callback.onCall(state, getErrorMsg(state));
    }

    public void printDatas_order(String text,Bitmap bitmap, PrintCallback callback) throws Exception {
        int state = printDev.getPrinterState();

        if (state == 0) {
            List<PrinterParams> list = new ArrayList<>();

            PrinterParams paramText=new PrinterParams();
            paramText.setDataType(PrinterParams.DATATYPE.TEXT);
            paramText.setText(text);

            PrinterParams paramImg=new PrinterParams();
            paramImg.setDataType(PrinterParams.DATATYPE.IMAGE);
            paramImg.setBitmap(bitmap);
            paramImg.setAlign(PrinterParams.ALIGN.CENTER);

            list.add(paramText);
            list.add(paramImg);

            printDev.printDatas(list, new AidlPrinterStateChangeListener.Stub() {
                @Override
                public void onPrintFinish() throws RemoteException {
                    mHandler.post(() -> callback.onCall(9999, "打印完成"));
                }

                @Override
                public void onPrintError(int i) throws RemoteException {
                    mHandler.post(() -> callback.onCall(i, getErrorMsg(i)));
                }

                @Override
                public void onPrintOutOfPaper() throws RemoteException {
                    mHandler.post(() -> callback.onCall(4104, getErrorMsg(4104)));
                }
            });
            printDev.spitPaper(120);
            return;
        }

        callback.onCall(state, getErrorMsg(state));
    }

    private String getErrorMsg(int state) {
        switch (state) {
            case 4098:
                return "打印机未打开";
            case 4099:
                return "打印机繁忙";
            case 4100:
                return "打印机无响应";
            case 4101:
                return "打印机数据错误";
            case 4102:
                return "打印机高温";
            case 4103:
                return "打印机过热";
            case 4104:
                return "打印机缺纸";
            case 4105:
                return "打印机命令错误";
            case 4112:
                return "打印机内存不足";
            case 4113:
                return "打印机未知异常";
            case 4115:
                return "打印机电量低";

            default:
                return "未知错误";
        }

    }

    public interface PrintCallback {

        void onCall(int code, String msg);
    }
}
