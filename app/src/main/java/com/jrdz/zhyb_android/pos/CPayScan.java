//package com.jrdz.zhyb_android.pos;
//
//import android.os.Handler;
//import android.os.Looper;
//import android.os.RemoteException;
//
//import com.centerm.smartpos.aidl.qrscan.AidlQuickScanZbar;
//import com.centerm.smartpos.aidl.qrscan.AidlScanCallback;
//import com.centerm.smartpos.aidl.qrscan.CameraBeanZbar;
//import com.centerm.smartpos.aidl.sys.AidlDeviceManager;
//import com.centerm.smartpos.constant.Constant;
//import com.frame.compiler.utils.LogUtils;
//import java.util.HashMap;
//
///**
// * @author 拜雨
// * @date 2020-03
// * @desc 扫码
// */
//public class CPayScan {
//    private AidlQuickScanZbar scanDev = null;
//    private Handler mHandler = new Handler(Looper.getMainLooper());
//
//    private CPayScan() {}
//
//    CPayScan(AidlDeviceManager manager) {
//        try {
//            scanDev = AidlQuickScanZbar.Stub.asInterface(manager.getDevice(Constant.DEVICE_TYPE.DEVICE_TYPE_QUICKSCAN));
//        } catch (Exception e) {
//            LogUtils.e("扫码初始化失败");
//            e.printStackTrace();
//        }
//    }
//
//    public void scan(ScanCodeCallback codeCallback) {
//        try {
//            CameraBeanZbar zbar = new CameraBeanZbar(0, 1280, 720, 0, Integer.MAX_VALUE, 0, 1);
//            HashMap<String, Object> externalMap = new HashMap<>();
//            externalMap.put("ShowPreview", true);
//            externalMap.put("ScanEffect", true);
//            externalMap.put("Persist", false);
//            zbar.setExternalMap(externalMap);
//            scanDev.scanQRCode(zbar, new AidlScanCallback.Stub() {
//                @Override
//                public void onCaptured(String s, int i) throws RemoteException {
//                    mHandler.post(() -> {
//                        codeCallback.onCall(100, s);
//                    });
//                }
//
//                @Override
//                public void onFailed(int code) throws RemoteException {
//                    mHandler.post(() -> {
//                        if (code == -3) {
//                            codeCallback.onCall(-3, "扫码超时");
//                        } else if (code == -2001) {
//                            codeCallback.onCall(-2001, "取消操作");
//                        } else {
//                            codeCallback.onCall(code, "操作异常");
//                        }
//                    });
//                }
//            });
//        } catch (RemoteException e) {
//            e.printStackTrace();
//            LogUtils.e("scan fail: " + e.getMessage());
//            codeCallback.onCall(-99, "操作异常");
//        }
//    }
//
//    public void cancel() {
//        try {
//            scanDev.cancelQRscan();
//        } catch (RemoteException e) {
//            e.printStackTrace();
//            LogUtils.e("cancel qr scan fail: " + e.getMessage());
//        }
//    }
//
//    public interface ScanCodeCallback {
//        void onCall(int code, String msg);
//    }
//}
