package com.jrdz.zhyb_android.pos;

import android.os.RemoteException;
import android.util.Log;

import com.centerm.smartpos.aidl.sys.AidlDeviceManager;
import com.centerm.smartpos.aidl.sys.AidlSystemSettingService;
import com.centerm.smartpos.constant.Constant;

/**
 * @author 拜雨
 * @date 2020-03
 * @desc 系统接口调用
 */
public class CPaySystem {

    private AidlSystemSettingService systemSettingService;

    private CPaySystem() {}

    CPaySystem(AidlDeviceManager manager) {
        try {
            /*连接设备*/
            systemSettingService = AidlSystemSettingService.Stub.asInterface(manager.getDevice(Constant.DEVICE_TYPE.DEVICE_TYPE_SYS));
        } catch (Exception e) {
            Log.e("66666","初始化系统服务失败");
            e.printStackTrace();
        }
    }

    public String readSerialNum() {
        try {
            return systemSettingService.readSerialNum();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return "unknown";
    }
}
