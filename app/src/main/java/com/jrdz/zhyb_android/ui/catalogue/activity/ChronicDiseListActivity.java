package com.jrdz.zhyb_android.ui.catalogue.activity;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.ui.catalogue.adapter.ChronicDiseAdapter;
import com.jrdz.zhyb_android.ui.settlement.model.QueryPersonalInfoModel;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.catalogue.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/27
 * 描    述： 慢性病列表
 * ================================================
 */
public class ChronicDiseListActivity extends BaseRecyclerViewActivity {
    private ArrayList<QueryPersonalInfoModel.PsnOpspRegBean> feedetailBeans;

    @Override
    public int getLayoutId() {
        return R.layout.activity_chronic_dise_list;
    }

    @Override
    public void initAdapter() {
        mAdapter = new ChronicDiseAdapter();
    }

    @Override
    public void initData() {
        feedetailBeans = getIntent().getParcelableArrayListExtra("feedetailBeans");
        super.initData();
        mAdapter.setNewData(feedetailBeans);
    }

    @Override
    public void initEvent() {
        super.initEvent();
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter,view,position);

        Intent intent = new Intent();
        intent.putExtra("selectData", ((ChronicDiseAdapter) adapter).getItem(position));
        setResult(RESULT_OK, intent);
        finish();
    }

    public static void newIntance(Activity activity, ArrayList<QueryPersonalInfoModel.PsnOpspRegBean> feedetailBeans, int requestCode) {
        Intent intent = new Intent(activity, ChronicDiseListActivity.class);
        intent.putParcelableArrayListExtra("feedetailBeans", feedetailBeans);
        activity.startActivityForResult(intent, requestCode);
    }
}