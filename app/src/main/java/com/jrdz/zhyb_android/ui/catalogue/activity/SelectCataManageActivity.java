package com.jrdz.zhyb_android.ui.catalogue.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.adapter.CataTabsAdapter;
import com.jrdz.zhyb_android.ui.catalogue.fragment.ChinaCataEnaListFragment;
import com.jrdz.zhyb_android.ui.catalogue.fragment.MedSerCataEnaListFragment;
import com.jrdz.zhyb_android.ui.catalogue.fragment.MedSubCataEnaListFragment;
import com.jrdz.zhyb_android.ui.catalogue.fragment.PhaPreCataEnaListFragment;
import com.jrdz.zhyb_android.ui.catalogue.fragment.WestCataEnaListFragment;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.ComplaintStoreActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.ShopDetailActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.SmartPhaMainActivity_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopDetailMoreModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.widget.pop.ShopDetailMorePop;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：已对照目录管理
 * ================================================
 */
public class SelectCataManageActivity extends BaseActivity {
    private LinearLayout mLlTitle;
    private FrameLayout mFlClose,mFlMore;
    private ImageView mIvMore;
    private AppCompatEditText mEtSearch;
    private TextView mTvSearch;
    private SlidingTabLayout mStbTablayout;
    private ViewPager mViewpager;

    private String from;
    private List<DataDicModel> listTypes;
    private ShopDetailMorePop shopDetailMorePop;//更多弹框

    @Override
    public int getLayoutId() {
        return R.layout.activity_enacata_mana;
    }

    @Override
    public void initView() {
        super.initView();
        mLlTitle = findViewById(R.id.ll_title);
        mFlClose = findViewById(R.id.fl_close);
        mEtSearch = findViewById(R.id.et_search);
        mTvSearch = findViewById(R.id.tv_search);
        mFlMore = findViewById(R.id.fl_more);
        mIvMore = findViewById(R.id.iv_more);
        mStbTablayout = findViewById(R.id.stb_tablayout);
        mViewpager = findViewById(R.id.viewpager);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mLlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        from=getIntent().getStringExtra("from");
        super.initData();
        listTypes = CommonlyUsedDataUtils.getInstance().getListTypeData();
        if (listTypes == null || listTypes.isEmpty()) {
            showShortToast("药品类型有误，请重新打开app");
            finish();
            return;
        }
        if ("1".equals(from)){
            mFlClose.setVisibility(View.VISIBLE);
        }else if ("2".equals(from)){
            mFlClose.setVisibility(View.GONE);
        }

        initViewPage();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    search();
                    return true;
                }
                return false;
            }
        });

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())){
                    MsgBus.sendCataEnableRefresh().post(listTypes.get(mStbTablayout.getCurrentTab()).getValue());
                }
            }
        });
        mFlClose.setOnClickListener(this);
        mTvSearch.setOnClickListener(this);
        mFlMore.setOnClickListener(this);

//        mStbTablayout.setOnTabSelectListener(new OnTabSelectListener() {
//            @Override
//            public void onTabSelect(int position) {
//                if (mEtSearch!=null){
//                    mEtSearch.setText("");
//                }
//            }
//
//            @Override
//            public void onTabReselect(int position) {
//
//            }
//        });
    }

    //初始化
    private void initViewPage() {
        CataTabsAdapter baseViewPagerAndTabsAdapter_new = new CataTabsAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getCustomeItem(String value,String label, int position) {
                switch (value) {
                    case "101"://西药中成药目录
                        return WestCataEnaListFragment.newIntance(value, from,label);
                    case "102"://中药饮片目录
                        return ChinaCataEnaListFragment.newIntance(value, from,label);
                    case "103"://医疗机构自制剂目录
                        return PhaPreCataEnaListFragment.newIntance(value, from,label);
                    case "201"://医疗服务项目目录
                        return MedSerCataEnaListFragment.newIntance(value, from,label);
                    case "301"://医用耗材目录
                        return MedSubCataEnaListFragment.newIntance(value, from,label);
                    default:
                        return WestCataEnaListFragment.newIntance(value, from,label);
                }
            }
        };

        baseViewPagerAndTabsAdapter_new.setData(listTypes);
        mViewpager.setAdapter(baseViewPagerAndTabsAdapter_new);
        mStbTablayout.setViewPager(mViewpager);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close://关闭
                goFinish();
                break;
            case R.id.tv_search://搜索
                search();
                break;
            case R.id.fl_more://更多
                onMorePop();
                break;
        }
    }

    //搜索
    private void search() {
        if (EmptyUtils.isEmpty(mEtSearch.getText().toString())){
            showShortToast("请输入目录名称/拼音简码");
            return;
        }

        MsgBus.sendCataEnableRefresh().post(listTypes.get(mStbTablayout.getCurrentTab()).getValue());
    }

    //更多弹框
    private void onMorePop() {
        if (shopDetailMorePop == null) {
            shopDetailMorePop = new ShopDetailMorePop(SelectCataManageActivity.this, CommonlyUsedDataUtils.getInstance().getSelectCataManageMoreData());
        }

        shopDetailMorePop.setOnListener(new ShopDetailMorePop.IOptionListener() {
            @Override
            public void onItemClick(ShopDetailMoreModel item) {
                switch (item.getText()) {
                    case "药品目录同步":
                        showWaitDialog();
                        SyncMedListMap();
                        break;
                    case "新增":
                        CatalogueManageActivity.newIntance(SelectCataManageActivity.this);
                        break;
                }
            }
        });

        shopDetailMorePop.showPopupWindow(mIvMore);
    }

    //药品目录同步
    private void SyncMedListMap() {
        BaseModel.sendSyncMedListMapRequest(TAG, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showTipDialog(returnData.getMsg());
            }
        });
    }

    public String getSearchText(){
        return EmptyUtils.strEmpty(mEtSearch.getText().toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (shopDetailMorePop != null) {
            shopDetailMorePop.onCleanListener();
            shopDetailMorePop.onDestroy();
        }
    }

    //from 1.来自目录管理 2.来自选择药品目录
    public static void newIntance(Context context,String from) {
        Intent intent = new Intent(context, SelectCataManageActivity.class);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }
    //from 1.来自目录管理 2.来自选择药品目录
    public static void newIntance(Activity activity, String from, int requestCode) {
        Intent intent = new Intent(activity, SelectCataManageActivity.class);
        intent.putExtra("from", from);
        activity.startActivityForResult(intent, requestCode);
    }
}
