package com.jrdz.zhyb_android.ui.catalogue.activity;

import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.database.CatalogueModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.adapter.ChinaCataEnaListAdapter;
import com.jrdz.zhyb_android.ui.catalogue.adapter.WestCataEnaListAdapter;
import com.jrdz.zhyb_android.ui.catalogue.model.CatalogueEnableListModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：已对照目录管理(中药)
 * ================================================
 */
public class SelectChinaCataManageActivity extends SelectWestCataManageActivity {

    @Override
    public void initAdapter() {
        mAdapter = new ChinaCataEnaListAdapter("2");
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        Intent intent = new Intent();
        intent.putExtra("catalogueModels", ((ChinaCataEnaListAdapter)adapter).getItem(position));
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    public static void newIntance(Activity activity, String listType, String listTypeName, int requestCode) {
        Intent intent = new Intent(activity, SelectChinaCataManageActivity.class);
        intent.putExtra("listType", listType);
        intent.putExtra("listTypeName", listTypeName);
        activity.startActivityForResult(intent, requestCode);
    }
}
