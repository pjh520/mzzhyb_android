package com.jrdz.zhyb_android.ui.catalogue.activity;

import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.adapter.SelectMultDiseCataAdapter;
import com.jrdz.zhyb_android.ui.catalogue.model.DiseCataModel;
import com.scwang.smart.refresh.layout.api.RefreshLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.catalogue.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-25
 * 描    述：疾病目录--多选
 * ================================================
 */
public class SelectMultDiseCataActivity extends BaseRecyclerViewActivity {
    private LinearLayout mLlTitle;
    private FrameLayout mFlClose;
    private AppCompatEditText mEtSearch;
    private FrameLayout mFlClean;
    private TextView mTvSearch;
    private TextView mTvNum;
    private ShapeTextView mTvSure;

    private int selectNum=0;
    private int selectSize;

    @Override
    public int getLayoutId() {
        return R.layout.activity_select_mult_disecata;
    }

    @Override
    public void initView() {
        super.initView();
        mLlTitle = findViewById(R.id.ll_title);
        mFlClose = findViewById(R.id.fl_close);
        mEtSearch = findViewById(R.id.et_search);
        mFlClean = findViewById(R.id.fl_clean);
        mTvSearch = findViewById(R.id.tv_search);
        mTvNum = findViewById(R.id.tv_num);
        mTvSure = findViewById(R.id.tv_sure);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mLlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initAdapter() {
        mAdapter = new SelectMultDiseCataAdapter();
    }

    @Override
    public void initData() {
        selectSize=getIntent().getIntExtra("selectSize", 0);
        super.initData();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    mFlClean.setVisibility(View.GONE);
                } else {
                    mFlClean.setVisibility(View.VISIBLE);
                }
            }
        });
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                    return true;
                }
                return false;
            }
        });
        mFlClose.setOnClickListener(this);
        mFlClean.setOnClickListener(this);
        mTvSearch.setOnClickListener(this);
        mTvSure.setOnClickListener(this);

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        super.getData();
        DiseCataModel.sendDiseCataRequest(TAG, Constants.Api.GET_DIAGCATALOGUE_URL, String.valueOf(mPageNum), "20", mEtSearch.getText().toString(),
                new CustomerJsonCallBack<DiseCataModel>() {
                    @Override
                    public void onRequestError(DiseCataModel returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        if (mPageNum == 0) {
                            if (mTvNum!=null){
                                mTvNum.setText("0条");
                            }
                        }
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(DiseCataModel returnData) {
                        hideRefreshView();
                        List<DiseCataModel.DataBean> infos = returnData.getData();
                        if (mAdapter!=null&&infos != null) {
                            if (mPageNum == 0) {
                                mAdapter.setNewData(infos);
                                if (mTvNum!=null){
                                    mTvNum.setText(returnData.getTotalItems() + "条");
                                }
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close://关闭
                goFinish();
                break;
            case R.id.fl_clean://清空
                mEtSearch.setText("");
                KeyboardUtils.hideSoftInput(mEtSearch);
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.tv_search://搜索
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.tv_sure://确认
                if (selectNum==0){
                    showShortToast("请选择病种");
                    return;
                }

                ArrayList<DiseCataModel.DataBean> dataBeans=new ArrayList<>();
                for (DiseCataModel.DataBean datum : ((SelectMultDiseCataAdapter) mAdapter).getData()) {
                    if (datum.isChoose()){
                        dataBeans.add(datum);
                    }
                }

                Intent intent = new Intent();
                intent.putExtra("selectDatas", dataBeans);
                setResult(Activity.RESULT_OK, intent);
                finish();
                break;
        }
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        DiseCataModel.DataBean itemData = ((SelectMultDiseCataAdapter) adapter).getItem(position);
        switch (view.getId()) {
            case R.id.fl_select://选中需要下载的数据
                ImageView ivSelect = view.findViewById(R.id.iv_select);
                if (itemData.isChoose()) {//取消选中
                    ivSelect.setImageResource(R.drawable.ic_product_select_nor);
                    itemData.setChoose(false);
                    selectNum--;
                } else {//选中
                    if (selectNum>=5-selectSize){
                        showShortToast("最多选择5个");
                        return;
                    }
                    ivSelect.setImageResource(R.drawable.ic_product_select_pre);
                    itemData.setChoose(true);
                    selectNum++;
                }
                break;
        }
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        selectNum=0;
        super.onRefresh(refreshLayout);
    }
}
