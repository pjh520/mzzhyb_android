package com.jrdz.zhyb_android.ui.catalogue.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.database.CatalogueModel;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.adapter.CataTabsAdapter;
import com.jrdz.zhyb_android.ui.catalogue.adapter.WestCataEnaListAdapter;
import com.jrdz.zhyb_android.ui.catalogue.fragment.ChinaCataEnaListFragment;
import com.jrdz.zhyb_android.ui.catalogue.fragment.MedSerCataEnaListFragment;
import com.jrdz.zhyb_android.ui.catalogue.fragment.MedSubCataEnaListFragment;
import com.jrdz.zhyb_android.ui.catalogue.fragment.PhaPreCataEnaListFragment;
import com.jrdz.zhyb_android.ui.catalogue.fragment.WestCataEnaListFragment;
import com.jrdz.zhyb_android.ui.catalogue.model.CatalogueEnableListModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：已对照目录管理(西药中成药)
 * ================================================
 */
public class SelectWestCataManageActivity extends BaseRecyclerViewActivity {
    private LinearLayout mLlTitle;
    private FrameLayout mFlClose;
    private AppCompatEditText mEtSearch;
    private TextView mTvSearch;
    protected TextView mTvNumName, mTvNum;

    protected String listType, listTypeName;

    @Override
    public int getLayoutId() {
        return R.layout.activity_west_enacata_mana;
    }

    @Override
    public void initView() {
        super.initView();
        mLlTitle = findViewById(R.id.ll_title);
        mFlClose = findViewById(R.id.fl_close);
        mEtSearch = findViewById(R.id.et_search);
        mTvSearch = findViewById(R.id.tv_search);
        mTvNumName = findViewById(R.id.tv_num_name);
        mTvNum = findViewById(R.id.tv_num);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mLlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        listType = getIntent().getStringExtra("listType");
        listTypeName = getIntent().getStringExtra("listTypeName");
        super.initData();

        mTvNumName.setText("医药机构目录-" + listTypeName);
        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    search();
                    return true;
                }
                return false;
            }
        });

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                }
            }
        });
        mFlClose.setOnClickListener(this);
        mTvSearch.setOnClickListener(this);

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void initAdapter() {
        mAdapter = new WestCataEnaListAdapter("2");
    }

    @Override
    public void getData() {
        CatalogueEnableListModel.sendCatalogueEnableListRequest(TAG, listType, EmptyUtils.strEmpty(mEtSearch.getText().toString()),
                String.valueOf(mPageNum), "20", new CustomerJsonCallBack<CatalogueEnableListModel>() {
                    @Override
                    public void onRequestError(CatalogueEnableListModel returnData, String msg) {
                        hideRefreshView();
                        if (!isFinishing()){
                            setLoadMoreFail();
                            if (mPageNum == 0) {
                                mTvNum.setText("0条");
                            }
                            showShortToast(msg);
                        }
                    }

                    @Override
                    public void onRequestSuccess(CatalogueEnableListModel returnData) {
                        hideRefreshView();
                        if (!isFinishing()){
                            if (mPageNum == 0) {
                                mTvNum.setText(returnData.getTotalItems() + "条");
                            }
                            List<CatalogueModel> infos = returnData.getData();
                            if (mAdapter!=null&&infos != null) {
                                if (mPageNum == 0) {
                                    mAdapter.setNewData(infos);
                                    if (infos.size() <= 0) {
                                        mAdapter.isUseEmpty(true);
                                    }
                                } else {
                                    mAdapter.addData(infos);
                                    mAdapter.loadMoreComplete();
                                }

                                if (infos.isEmpty()) {
                                    if (mAdapter.getData().size() < 8) {
                                        mAdapter.loadMoreEnd(true);
                                    } else {
                                        mAdapter.loadMoreEnd();
                                    }
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        Intent intent = new Intent();
        intent.putExtra("catalogueModels", ((WestCataEnaListAdapter) adapter).getItem(position));
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close://关闭
                goFinish();
                break;
            case R.id.tv_search://搜索
                search();
                break;
        }
    }

    //搜索
    private void search() {
        if (EmptyUtils.isEmpty(mEtSearch.getText().toString())) {
            showShortToast("请输入目录名称/拼音简码");
            return;
        }

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    public static void newIntance(Activity activity, String listType, String listTypeName, int requestCode) {
        Intent intent = new Intent(activity, SelectWestCataManageActivity.class);
        intent.putExtra("listType", listType);
        intent.putExtra("listTypeName", listTypeName);
        activity.startActivityForResult(intent, requestCode);
    }
}
