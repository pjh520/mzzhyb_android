package com.jrdz.zhyb_android.ui.catalogue.activity;

import android.content.Context;
import android.content.Intent;
import android.text.InputFilter;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.text.MoneyValueFilter;
import com.hjq.shape.view.ShapeEditText;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.database.CatalogueModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.model.WestCataListModel;
import com.jrdz.zhyb_android.ui.home.model.ScanCataRefreshModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述： 西药目录对照
 * ================================================
 */
public class UpdateWestEnaCataActivity extends BaseActivity {
    private TextView mTvTitle,mTvMedListCodgTag,mTvNameTag;
    private ShapeEditText mEtMedListCodg,mEtName;
    private ShapeTextView mTvDrugSpecCode;
    private ShapeTextView mTvDrugTypeName;
    private ShapeTextView mTvProdentpName;
    private ShapeTextView mTvAprvno;
    private ShapeEditText mEtPrice;
    private ShapeTextView mTvUpload;

    private CatalogueModel resultObjBean;
    private int pos,from;

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_westcata;
    }

    @Override
    public void initView() {
        super.initView();
        mTvTitle= findViewById(R.id.tv_title);
        mEtMedListCodg = findViewById(R.id.et_med_list_codg);
        mTvMedListCodgTag = findViewById(R.id.tv_med_list_codg_tag);
        mEtName = findViewById(R.id.et_name);
        mTvNameTag= findViewById(R.id.tv_name_tag);
        mTvDrugSpecCode = findViewById(R.id.tv_drug_spec_code);
        mTvDrugTypeName = findViewById(R.id.tv_drug_type_name);
        mTvProdentpName = findViewById(R.id.tv_prodentp_name);
        mTvAprvno = findViewById(R.id.tv_aprvno);
        mEtPrice = findViewById(R.id.et_price);
        mTvUpload = findViewById(R.id.tv_upload);
    }

    @Override
    public void initData() {
        resultObjBean = getIntent().getParcelableExtra("resultObjBean");
        pos= getIntent().getIntExtra("pos",0);
        from= getIntent().getIntExtra("from",0);
        super.initData();
        setRightTitleView("删除");
        mTvUpload.setText("修改价格");
        setTitle("更新目录");

        //设置输入金额的限制
        mEtPrice.setFilters(new InputFilter[]{new MoneyValueFilter()});

        mEtMedListCodg.setEnabled(false);
        mTvMedListCodgTag.setText("*");
        mEtName.setEnabled(false);
        mTvNameTag.setText("*");

        mEtMedListCodg.setText(EmptyUtils.strEmpty(resultObjBean.getFixmedins_hilist_id()));
        mTvTitle.setText(EmptyUtils.strEmpty(resultObjBean.getFixmedins_hilist_name()));
        mEtName.setText(EmptyUtils.strEmpty(resultObjBean.getFixmedins_hilist_name()));
        mTvDrugSpecCode.setText(EmptyUtils.strEmpty(resultObjBean.getDrug_spec_code()));
        mTvDrugTypeName.setText(EmptyUtils.strEmpty(resultObjBean.getDrug_type_name()));
        mTvProdentpName.setText(EmptyUtils.strEmpty(resultObjBean.getEnterpriseName()));
        mTvAprvno.setText(EmptyUtils.strEmpty(resultObjBean.getAprvno_begndate()));
        mEtPrice.setText(EmptyUtils.strEmpty(resultObjBean.getPrice()));
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvUpload.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_upload:
                upload();
                break;
        }
    }

    @Override
    public void rightTitleViewClick() {
        showWaitDialog();
        BaseModel.sendDelMatchRequest(TAG, resultObjBean.getFixmedins_hilist_id(), resultObjBean.getList_type(), resultObjBean.getMed_list_codg(), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("删除机构目录成功");
                MsgBus.sendCataEnableUpdate().post(resultObjBean.getList_type());
                goFinish();
            }
        });
    }

    //上传目录
    private void upload() {
        if (EmptyUtils.isEmpty(mEtMedListCodg.getText().toString())) {
            showShortToast("请输入医疗目录编码");
            return;
        }
        if (EmptyUtils.isEmpty(mEtName.getText().toString())) {
            showShortToast("请输入药品商品名");
            return;
        }
        if (!RxTool.isPrice(mEtPrice.getText().toString())) {
            showShortToast("请输入正确的金额");
            return;
        }
//        List<CatalogueModel> cataEnaData = CommonlyUsedDataUtils.getInstance().getIsCataEnaData(mEtMedListCodg.getText().toString());
//        if (cataEnaData != null && !cataEnaData.isEmpty() && !resultObjBean.getMed_list_codg().equals(cataEnaData.get(0).getMed_list_codg())) {
//            showShortToast("该机构医疗编码已存在");
//            return;
//        }
//
//        if (cataEnaData != null && !cataEnaData.isEmpty()) {
//            double diffPrice = new BigDecimal(mEtPrice.getText().toString()).subtract(new BigDecimal(cataEnaData.get(0).getPrice())).doubleValue();
//            if (diffPrice == 0) {
//                showShortToast("该目录已对照");
//                return;
//            }
//        }

        showWaitDialog();
        BaseModel.sendContentsEditRequest(TAG,resultObjBean.getList_type(),mEtMedListCodg.getText().toString(),mEtName.getText().toString(),
                EmptyUtils.strEmpty(resultObjBean.getMed_list_codg()),mEtPrice.getText().toString(),new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                if (0==from){
                    MsgBus.sendCataEnableUpdate().post(resultObjBean.getList_type());
                }else {
                    MsgBus.sendScanCataRefresh().post(new ScanCataRefreshModel(mEtName.getText().toString(),mEtPrice.getText().toString(),pos));
                }

                showShortToast("更新机构目录成功");
                goFinish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        resultObjBean = null;
    }

    public static void newIntance(Context context, CatalogueModel resultObjBean) {
        Intent intent = new Intent(context, UpdateWestEnaCataActivity.class);
        intent.putExtra("resultObjBean", resultObjBean);
        context.startActivity(intent);
    }

    public static void newIntance(Context context, CatalogueModel resultObjBean,int pos,int from) {
        Intent intent = new Intent(context, UpdateWestEnaCataActivity.class);
        intent.putExtra("resultObjBean", resultObjBean);
        intent.putExtra("pos", pos);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }
}
