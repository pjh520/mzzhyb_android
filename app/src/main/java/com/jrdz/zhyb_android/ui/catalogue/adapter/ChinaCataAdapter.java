package com.jrdz.zhyb_android.ui.catalogue.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.catalogue.model.ChinaCataListModel;
import com.jrdz.zhyb_android.ui.catalogue.model.WestCataListModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：
 * ================================================
 */
public class ChinaCataAdapter extends BaseQuickAdapter<ChinaCataListModel.DataBean, BaseViewHolder> {
    public ChinaCataAdapter() {
        super(R.layout.layout_chinacata_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ChinaCataListModel.DataBean resultObjBean) {
        baseViewHolder.setText(R.id.tv_title, "名\u3000\u3000称："+resultObjBean.getVer_name());
        baseViewHolder.setText(R.id.tv_med_list_codg, "目录编码："+resultObjBean.getMed_list_codg());
        baseViewHolder.setText(R.id.tv_medicinal_parts, "药用部位："+resultObjBean.getMed_part());
        baseViewHolder.setText(R.id.tv_general_usage, "常规用法："+resultObjBean.getCnvl_used());
        baseViewHolder.setText(R.id.tv_sexual_taste, "性\u3000\u3000味："+resultObjBean.getNatfla());
    }
}
