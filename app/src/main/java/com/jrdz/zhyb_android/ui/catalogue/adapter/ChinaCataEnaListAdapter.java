package com.jrdz.zhyb_android.ui.catalogue.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.database.CatalogueModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/15
 * 描    述：
 * ================================================
 */
public class ChinaCataEnaListAdapter extends BaseQuickAdapter<CatalogueModel, BaseViewHolder> {
    private final String mFrom;

    public ChinaCataEnaListAdapter(String from) {
        super(R.layout.layout_chinacata_ena_item, null);
        this.mFrom=from;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        TextView tvUpdate=baseViewHolder.getView(R.id.tv_update);
        if (tvUpdate!=null){
            tvUpdate.setVisibility("1".equals(mFrom)?View.VISIBLE:View.GONE);
        }
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, CatalogueModel catalogueModel) {
        baseViewHolder.setText(R.id.tv_title, "名称:"+catalogueModel.getFixmedins_hilist_name());
        baseViewHolder.setText(R.id.tv_med_list_codg, EmptyUtils.strEmpty(catalogueModel.getFixmedins_hilist_id()));
        baseViewHolder.setText(R.id.tv_medicinal_parts, EmptyUtils.strEmpty(catalogueModel.getMed_part()));
        baseViewHolder.setText(R.id.tv_general_usage, EmptyUtils.strEmpty(catalogueModel.getCnvl_used()));
        baseViewHolder.setText(R.id.tv_sexual_taste, EmptyUtils.strEmpty(catalogueModel.getNatfla()));
        baseViewHolder.setText(R.id.tv_price, "¥"+catalogueModel.getPrice());
    }
}
