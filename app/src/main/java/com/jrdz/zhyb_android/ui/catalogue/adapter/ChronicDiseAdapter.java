package com.jrdz.zhyb_android.ui.catalogue.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.settlement.model.QueryPersonalInfoModel;


/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.catalogue.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/23
 * 描    述：
 * ================================================
 */
public class ChronicDiseAdapter extends BaseQuickAdapter<QueryPersonalInfoModel.PsnOpspRegBean, BaseViewHolder> {
    public ChronicDiseAdapter() {
        super(R.layout.layout_chronic_dise_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, QueryPersonalInfoModel.PsnOpspRegBean diseCataModel) {
        baseViewHolder.setText(R.id.tv_disecata_code, "疾病编码:"+diseCataModel.getOpsp_dise_code());
        baseViewHolder.setText(R.id.tv_disecata_name, "疾病名称:"+diseCataModel.getOpsp_dise_name());
    }
}
