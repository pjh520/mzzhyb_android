package com.jrdz.zhyb_android.ui.catalogue.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.catalogue.model.DiseCataModel;


/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.catalogue.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/23
 * 描    述：
 * ================================================
 */
public class DiseCataAdapter extends BaseQuickAdapter<DiseCataModel.DataBean, BaseViewHolder> {
    public DiseCataAdapter() {
        super(R.layout.layout_disecata_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, DiseCataModel.DataBean diseCataModel) {
        baseViewHolder.setText(R.id.tv_disecata_code, "病种编码:"+diseCataModel.getCode());
        baseViewHolder.setText(R.id.tv_disecata_name, "病种名称:"+diseCataModel.getName());
    }
}
