package com.jrdz.zhyb_android.ui.catalogue.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.catalogue.model.MedSerCataListModel;
import com.jrdz.zhyb_android.ui.catalogue.model.PhaPreCataListModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：
 * ================================================
 */
public class MedSerCataAdapter extends BaseQuickAdapter<MedSerCataListModel.DataBean, BaseViewHolder> {
    public MedSerCataAdapter() {
        super(R.layout.layout_medsercata_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, MedSerCataListModel.DataBean resultObjBean) {
        baseViewHolder.setText(R.id.tv_title, "名\u3000\u3000称："+resultObjBean.getMedicalServiceItemsName());
        baseViewHolder.setText(R.id.tv_med_list_codg, "目录编码："+resultObjBean.getMed_list_codg());
        baseViewHolder.setText(R.id.tv_prcunt, "计价单位："+resultObjBean.getPrcunt());
    }
}
