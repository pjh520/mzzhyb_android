//package com.jrdz.zhyb_android.ui.catalogue.adapter;
//
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.CheckBox;
//import android.widget.FrameLayout;
//
//import androidx.annotation.NonNull;
//
//import com.chad.library.adapter.base.BaseQuickAdapter;
//import com.chad.library.adapter.base.BaseViewHolder;
//import com.jrdz.zhyb_android.R;
//import com.jrdz.zhyb_android.database.CatalogueModel;
//
///**
// * ================================================
// * 项目名称：zhyb_android
// * 包    名：com.jrdz.zhyb_android.ui.home.adapter
// * 作    者：彭俊鸿
// * 邮    箱：1031028399@qq.com
// * 版    本：1.0
// * 创建日期：2021/12/15
// * 描    述：
// * ================================================
// */
//public class SelectCataEnaListAdapter extends BaseQuickAdapter<CatalogueModel, BaseViewHolder> {
//    public SelectCataEnaListAdapter() {
//        super(R.layout.layout_catalogue_enable_item, null);
//    }
//
//    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
//    @NonNull
//    @Override
//    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
//        baseViewHolder.addOnClickListener(R.id.fl_cb);
//        return baseViewHolder;
//    }
//
//    @Override
//    protected void convert(@NonNull BaseViewHolder baseViewHolder, CatalogueModel catalogueModel) {
//        FrameLayout flCb=baseViewHolder.getView(R.id.fl_cb);
//        CheckBox cb=baseViewHolder.getView(R.id.cb);
//
//        baseViewHolder.setText(R.id.tv_med_list_codg, "药品编码:"+catalogueModel.getFixmedins_hilist_id());
//        baseViewHolder.setText(R.id.tv_reg_nam, "商品名称  "+catalogueModel.getFixmedins_hilist_name());
//        baseViewHolder.setText(R.id.tv_drug_spec_code, "注册剂型  "+catalogueModel.getDrug_spec_code());
//        baseViewHolder.setText(R.id.tv_prodentp_name, "药品企业  "+catalogueModel.getProdentp_name());
//
//        flCb.setVisibility(View.VISIBLE);
//        cb.setChecked(catalogueModel.choose?true:false);
//    }
//}
