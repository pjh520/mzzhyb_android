package com.jrdz.zhyb_android.ui.catalogue.adapter;

import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.catalogue.model.DiseCataModel;


/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.catalogue.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/23
 * 描    述：
 * ================================================
 */
public class SelectMultDiseCataAdapter extends BaseQuickAdapter<DiseCataModel.DataBean, BaseViewHolder> {
    public SelectMultDiseCataAdapter() {
        super(R.layout.layout_select_mult_disecata_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.fl_select);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, DiseCataModel.DataBean diseCataModel) {
        ImageView ivProductSelect=baseViewHolder.getView(R.id.iv_select);
        baseViewHolder.setText(R.id.tv_disecata_code, "病种编码:"+diseCataModel.getCode());
        baseViewHolder.setText(R.id.tv_disecata_name, "病种名称:"+diseCataModel.getName());

        ivProductSelect.setImageResource(diseCataModel.isChoose()?R.drawable.ic_product_select_pre:R.drawable.ic_product_select_nor);
    }
}
