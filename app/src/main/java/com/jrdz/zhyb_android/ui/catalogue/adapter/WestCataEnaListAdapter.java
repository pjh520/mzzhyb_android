package com.jrdz.zhyb_android.ui.catalogue.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.database.CatalogueModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/15
 * 描    述：
 * ================================================
 */
public class WestCataEnaListAdapter extends BaseQuickAdapter<CatalogueModel, BaseViewHolder> {
    private final String mFrom;

    public WestCataEnaListAdapter(String from) {
        super(R.layout.layout_catalogue_enable_item, null);
        this.mFrom=from;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        TextView tvUpdate=baseViewHolder.getView(R.id.tv_update);
        if (tvUpdate!=null){
            tvUpdate.setVisibility("1".equals(mFrom)?View.VISIBLE:View.GONE);
        }
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, CatalogueModel catalogueModel) {
        baseViewHolder.setText(R.id.tv_reg_nam, "药品名称:"+catalogueModel.getFixmedins_hilist_name());
        baseViewHolder.setText(R.id.tv_med_list_codg, EmptyUtils.strEmpty(catalogueModel.getFixmedins_hilist_id()));
        baseViewHolder.setText(R.id.tv_drug_spec_code, EmptyUtils.strEmpty(catalogueModel.getDrug_spec_code()));
        baseViewHolder.setText(R.id.tv_drug_type_name, EmptyUtils.strEmpty(catalogueModel.getDrug_type_name()));
        baseViewHolder.setText(R.id.tv_prodentp_name, EmptyUtils.strEmpty(catalogueModel.getEnterpriseName()));
        baseViewHolder.setText(R.id.tv_aprvno_begndate, EmptyUtils.strEmpty(catalogueModel.getAprvno_begndate()));
        baseViewHolder.setText(R.id.tv_price, "¥"+catalogueModel.getPrice());
    }
}
