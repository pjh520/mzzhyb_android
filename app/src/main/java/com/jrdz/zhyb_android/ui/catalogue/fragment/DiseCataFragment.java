package com.jrdz.zhyb_android.ui.catalogue.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.listener.OnMultiClickListener;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.activity.CatalogueManageActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.DiseCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.SelectCataManageActivity;
import com.jrdz.zhyb_android.ui.catalogue.adapter.DiseCataAdapter;
import com.jrdz.zhyb_android.ui.catalogue.adapter.WestCataEnaListAdapter;
import com.jrdz.zhyb_android.ui.catalogue.model.DiseCataModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.util.ArrayList;
import java.util.List;

import cody.bus.ObserverWrapper;
import razerdp.basepopup.BasePopupWindow;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.catalogue.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/23
 * 描    述：疾病目录
 * ================================================
 */
public class DiseCataFragment extends BaseRecyclerViewFragment {
    protected TextView mTvNumName,mTvNum;

    private String value, api, from;
    protected DiseCataActivity diseCataActivity;

    //刷新数据
    private ObserverWrapper<String> mObserver = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String tag) {
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.fragment_cata_mana;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mTvNumName= view.findViewById(R.id.tv_num_name);
        mTvNum = view.findViewById(R.id.tv_num);

        mTvNumName.setText("目录条数");
    }

    @Override
    public void initAdapter() {
        mAdapter = new DiseCataAdapter();
    }

    @Override
    public void initData() {
        value = getArguments().getString("value", "");
        api = getArguments().getString("api", "");
        from = getArguments().getString("from", "");
        super.initData();
        MsgBus.sendDiseRefresh().observe(this, mObserver);
        diseCataActivity = (DiseCataActivity) getActivity();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        super.getData();
        DiseCataModel.sendDiseCataRequest(TAG + value, api, String.valueOf(mPageNum), "20", diseCataActivity == null ? "" : diseCataActivity.getSearchText(),
                new CustomerJsonCallBack<DiseCataModel>() {
                    @Override
                    public void onRequestError(DiseCataModel returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        if (mPageNum == 0) {
                            if (mTvNum!=null){
                                mTvNum.setText("0条");
                            }
                        }
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(DiseCataModel returnData) {
                        hideRefreshView();
                        if (mPageNum == 0) {
                            if (mTvNum!=null){
                                mTvNum.setText(returnData.getTotalItems() + "条");
                            }
                        }
                        List<DiseCataModel.DataBean> infos = returnData.getData();
                        if (mAdapter!=null&&infos != null) {
                            if (mPageNum == 0) {
                                mAdapter.setNewData(infos);
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        if ("2".equals(from)||"3".equals(from)) {//返回该项数据
            Intent intent = new Intent();
            intent.putExtra("selectDatas", ((DiseCataAdapter) adapter).getItem(position));
            ((DiseCataActivity) getActivity()).setResult(Activity.RESULT_OK, intent);
            ((DiseCataActivity) getActivity()).finish();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public static DiseCataFragment newIntance(String value, String api, String from) {
        DiseCataFragment diseCataFragment = new DiseCataFragment();
        Bundle bundle = new Bundle();
        bundle.putString("value", value);
        bundle.putString("api", api);
        bundle.putString("from", from);
        diseCataFragment.setArguments(bundle);
        return diseCataFragment;
    }
}
