package com.jrdz.zhyb_android.ui.catalogue.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.database.CatalogueModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.activity.SelectCataManageActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.UpdateMedSerCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.UpdateMedSerEnaCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.adapter.MedSerCataAdapter;
import com.jrdz.zhyb_android.ui.catalogue.adapter.MedSerCataEnaListAdapter;
import com.jrdz.zhyb_android.ui.catalogue.adapter.PhaPreCataEnaListAdapter;
import com.jrdz.zhyb_android.ui.catalogue.adapter.WestCataEnaListAdapter;
import com.jrdz.zhyb_android.ui.catalogue.model.MedSerCataListModel;
import com.jrdz.zhyb_android.ui.catalogue.model.PhaPreCataListModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.util.ArrayList;
import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/15
 * 描    述：医疗机构自制剂目录列表
 * ================================================
 */
public class MedSerCataEnaListFragment extends WestCataEnaListFragment  {
    @Override
    public void initAdapter() {
        mAdapter = new MedSerCataEnaListAdapter(from);
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        if ("1".equals(from)){//进入修改药品页面
            UpdateMedSerEnaCataActivity.newIntance(getContext(),((MedSerCataEnaListAdapter) adapter).getItem(position));
        }else if ("2".equals(from)){//返回该项数据
            Intent intent = new Intent();
            intent.putExtra("catalogueModels", ((MedSerCataEnaListAdapter)adapter).getItem(position));
            ((SelectCataManageActivity) getActivity()).setResult(Activity.RESULT_OK, intent);
            ((SelectCataManageActivity) getActivity()).finish();
        }
    }

    public static MedSerCataEnaListFragment newIntance(String listType,String from,String listTypeName) {
        MedSerCataEnaListFragment medSerCataEnaListFragment = new MedSerCataEnaListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("listType", listType);
        bundle.putString("from", from);
        bundle.putString("listTypeName", listTypeName);
        medSerCataEnaListFragment.setArguments(bundle);
        return medSerCataEnaListFragment;
    }
}

