package com.jrdz.zhyb_android.ui.catalogue.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.widget.CustRefreshLayout;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.activity.CatalogueManageActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.UpdateMedSerCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.adapter.MedSerCataAdapter;
import com.jrdz.zhyb_android.ui.catalogue.adapter.PhaPreCataAdapter;
import com.jrdz.zhyb_android.ui.catalogue.model.MedSerCataListModel;
import com.jrdz.zhyb_android.ui.catalogue.model.PhaPreCataListModel;
import com.scwang.smart.refresh.layout.constant.RefreshState;

import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/15
 * 描    述： 医疗服务项目目录
 * ================================================
 */
public class MedSerCataManaFragment extends WestCataManaFragment {

    @Override
    public void initAdapter() {
        mAdapter = new MedSerCataAdapter();
    }

    @Override
    public void getData() {
        MedSerCataListModel.sendMedSerCataListRequest(TAG + listType, String.valueOf(mPageNum), "10",
                catalogueManageActivity == null ? "" : catalogueManageActivity.getSearchText(), new CustomerJsonCallBack<MedSerCataListModel>() {
                    @Override
                    public void onRequestError(MedSerCataListModel returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        if (mPageNum == 0) {
                            if (mTvNum!=null){
                                mTvNum.setText("0条");
                            }
                        }
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(MedSerCataListModel returnData) {
                        hideRefreshView();
                        if (mPageNum == 0) {
                            if (mTvNum!=null){
                                mTvNum.setText(returnData.getTotalItems() + "条");
                            }
                        }
                        List<MedSerCataListModel.DataBean> infos = returnData.getData();
                        if (mAdapter!=null&&infos != null) {
                            if (mPageNum == 0) {
                                mAdapter.setNewData(infos);
                                if (infos.size() <= 0) {
                                    mAdapter.isUseEmpty(true);
                                }
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        UpdateMedSerCataActivity.newIntance(getContext(),listType, ((MedSerCataAdapter) adapter).getItem(position));
    }

    public static MedSerCataManaFragment newIntance(String listType,String listTypeName) {
        MedSerCataManaFragment phaPreCataManaFragment = new MedSerCataManaFragment();
        Bundle bundle = new Bundle();
        bundle.putString("listType", listType);
        bundle.putString("listTypeName", listTypeName);
        phaPreCataManaFragment.setArguments(bundle);
        return phaPreCataManaFragment;
    }
}
