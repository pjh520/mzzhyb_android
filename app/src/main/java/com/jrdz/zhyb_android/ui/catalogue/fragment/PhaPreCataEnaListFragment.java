package com.jrdz.zhyb_android.ui.catalogue.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jrdz.zhyb_android.ui.catalogue.activity.SelectCataManageActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.UpdatePhaPreEnaCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.adapter.PhaPreCataEnaListAdapter;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/15
 * 描    述：医疗机构自制剂目录列表
 * ================================================
 */
public class PhaPreCataEnaListFragment extends WestCataEnaListFragment  {

    @Override
    public void initAdapter() {
        mAdapter = new PhaPreCataEnaListAdapter(from);
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        if ("1".equals(from)){//进入修改药品页面
            UpdatePhaPreEnaCataActivity.newIntance(getContext(),((PhaPreCataEnaListAdapter) adapter).getItem(position));
        }else if ("2".equals(from)){//返回该项数据
            Intent intent = new Intent();
            intent.putExtra("catalogueModels", ((PhaPreCataEnaListAdapter)adapter).getItem(position));
            ((SelectCataManageActivity) getActivity()).setResult(Activity.RESULT_OK, intent);
            ((SelectCataManageActivity) getActivity()).finish();
        }
    }

    public static PhaPreCataEnaListFragment newIntance(String listType,String from,String listTypeName) {
        PhaPreCataEnaListFragment phaPreCataEnaListFragment = new PhaPreCataEnaListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("listType", listType);
        bundle.putString("from", from);
        bundle.putString("listTypeName", listTypeName);
        phaPreCataEnaListFragment.setArguments(bundle);
        return phaPreCataEnaListFragment;
    }
}



