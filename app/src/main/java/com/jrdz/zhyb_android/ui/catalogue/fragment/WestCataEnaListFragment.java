package com.jrdz.zhyb_android.ui.catalogue.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.database.CatalogueModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.activity.SelectCataManageActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.UpdateWestEnaCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.adapter.WestCataEnaListAdapter;
import com.jrdz.zhyb_android.ui.catalogue.model.CatalogueEnableListModel;
import com.zhy.http.okhttp.OkHttpUtils;

import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/15
 * 描    述：西药已启用目录列表
 * ================================================
 */
public class WestCataEnaListFragment extends BaseRecyclerViewFragment {
    protected TextView mTvNumName,mTvNum;

    protected String listType, from,listTypeName;
    private SelectCataManageActivity selectCataManageActivity;

    //监听外部搜索
    private ObserverWrapper<String> mOnrefreshObserve = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    };
    //目录修改监听
    private ObserverWrapper<String> mOnCataUpdateObserve = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            if (value.equals(listType)) {
                showWaitDialog();
                onRefresh(mRefreshLayout);
            }
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.fragment_cata_mana;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void initAdapter() {
        mAdapter = new WestCataEnaListAdapter(from);
    }

    @Override
    public void initView(View view) {
        super.initView(view);

        mTvNumName= view.findViewById(R.id.tv_num_name);
        mTvNum = view.findViewById(R.id.tv_num);
    }

    @Override
    public void initData() {
        listType = getArguments().getString("listType", "");
        from = getArguments().getString("from", "");
        listTypeName= getArguments().getString("listTypeName");
        super.initData();
        selectCataManageActivity = (SelectCataManageActivity) getActivity();
        MsgBus.sendCataEnableRefresh().observeForever(mOnrefreshObserve);
        MsgBus.sendCataEnableUpdate().observeForever(mOnCataUpdateObserve);

        mTvNumName.setText("医药机构目录-"+listTypeName);
        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        super.getData();

        CatalogueEnableListModel.sendCatalogueEnableListRequest(TAG+listType, listType, selectCataManageActivity == null ? "" : selectCataManageActivity.getSearchText(),
                String.valueOf(mPageNum), "20", new CustomerJsonCallBack<CatalogueEnableListModel>() {
                    @Override
                    public void onRequestError(CatalogueEnableListModel returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        if (mPageNum == 0) {
                            if (mTvNum!=null){
                                mTvNum.setText("0条");
                            }
                        }
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(CatalogueEnableListModel returnData) {
                        hideRefreshView();
                        if (mPageNum == 0) {
                            if (mTvNum!=null){
                                mTvNum.setText(returnData.getTotalItems() + "条");
                            }
                        }
                        List<CatalogueModel> infos = returnData.getData();
                        if (mAdapter!=null&&infos != null) {
                            if (mPageNum == 0) {
                                mAdapter.setNewData(infos);
                                if (infos.size() <= 0) {
                                    mAdapter.isUseEmpty(true);
                                }
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter,view,position);

        if ("1".equals(from)) {//进入修改药品页面
            UpdateWestEnaCataActivity.newIntance(getContext(),((WestCataEnaListAdapter) adapter).getItem(position));
        } else if ("2".equals(from)) {//返回该项数据
            Intent intent = new Intent();
            intent.putExtra("catalogueModels", ((WestCataEnaListAdapter) adapter).getItem(position));
            ((SelectCataManageActivity) getActivity()).setResult(Activity.RESULT_OK, intent);
            ((SelectCataManageActivity) getActivity()).finish();
        }
    }

    @Override
    protected void requestCancle() {
        OkHttpUtils.getInstance().cancelTag(TAG+listType);//取消以Activity.this作为tag的请求
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MsgBus.sendCataEnableRefresh().removeObserver(mOnrefreshObserve);
        MsgBus.sendCataEnableUpdate().removeObserver(mOnCataUpdateObserve);
    }

    public static WestCataEnaListFragment newIntance(String listType, String from,String listTypeName) {
        WestCataEnaListFragment westCataEnaListFragment = new WestCataEnaListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("listType", listType);
        bundle.putString("from", from);
        bundle.putString("listTypeName", listTypeName);
        westCataEnaListFragment.setArguments(bundle);
        return westCataEnaListFragment;
    }
}
