package com.jrdz.zhyb_android.ui.catalogue.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.database.CatalogueModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.catalogue.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/16
 * 描    述：
 * ================================================
 */
public class CatalogueEnableListModel {
    /**
     * code : 1
     * msg : 请求成功
     * obj : {"currentPage":1,"resultObj":[{"begndate":"2021-12-13 16:24:14","contents_id":18375,"contents_match_id":18378,"create_time":1639383854000,"create_user":"test","enddate":"2021-12-13 16:24:14","fixmedins_code":"H61080200030","fixmedins_hilist_id":"d","fixmedins_hilist_name":"薄荷","id":18378,"ideleted":0,"list_type":"103","med_list_codg":"d","price":20,"prodentp_name":"1","regPha":"1","update_time":1639383854000,"update_user":"test"}]}
     */

    private String code;
    private String msg;
    private String totalItems;
    private List<CatalogueModel> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<CatalogueModel> getData() {
        return data;
    }

    public void setData(List<CatalogueModel> data) {
        this.data = data;
    }

    //获取已对照目录列表
    public static void sendCatalogueEnableListRequest(final String TAG,String itemType,String name, String pageindex, String pagesize,
                                                      final CustomerJsonCallBack<CatalogueEnableListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("itemType", itemType);
        jsonObject.put("name", name);
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_CONTENTSMATCHLIST_URL, jsonObject.toJSONString(), callback);
    }
}
