package com.jrdz.zhyb_android.ui.catalogue.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：
 * ================================================
 */
public class ChinaCataListModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-01-24 02:35:55
     * data : [{"CMDPCatalogueId":1,"med_list_codg":"T000100030\r\n","ver_name":"白芷\r\n","med_part":"干燥根\r\n","cnvl_used":"3～10g。\r\n","reg_spec":null,"natfla":"辛,温。归胃、大肠、肺经。\r\n","Price":0}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * med_list_codg : T000100030
         * ver_name : 白芷
         * med_part : 干燥根
         * cnvl_used : 3～10g。
         * natfla : 辛,温。归胃、大肠、肺经。
         */

        private String med_list_codg;
        private String ver_name;
        private String med_part;
        private String cnvl_used;
        private String natfla;

        public DataBean(String med_list_codg, String ver_name, String med_part, String cnvl_used, String natfla) {
            this.med_list_codg = med_list_codg;
            this.ver_name = ver_name;
            this.med_part = med_part;
            this.cnvl_used = cnvl_used;
            this.natfla = natfla;
        }

        public String getMed_list_codg() {
            return med_list_codg;
        }

        public void setMed_list_codg(String med_list_codg) {
            this.med_list_codg = med_list_codg;
        }

        public String getVer_name() {
            return ver_name;
        }

        public void setVer_name(String ver_name) {
            this.ver_name = ver_name;
        }

        public String getMed_part() {
            return med_part;
        }

        public void setMed_part(String med_part) {
            this.med_part = med_part;
        }

        public String getCnvl_used() {
            return cnvl_used;
        }

        public void setCnvl_used(String cnvl_used) {
            this.cnvl_used = cnvl_used;
        }

        public String getNatfla() {
            return natfla;
        }

        public void setNatfla(String natfla) {
            this.natfla = natfla;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.med_list_codg);
            dest.writeString(this.ver_name);
            dest.writeString(this.med_part);
            dest.writeString(this.cnvl_used);
            dest.writeString(this.natfla);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.med_list_codg = in.readString();
            this.ver_name = in.readString();
            this.med_part = in.readString();
            this.cnvl_used = in.readString();
            this.natfla = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //中药饮片目录列表
    public static void sendChinaCataListRequest(final String TAG, String pageindex, String pagesize, String name,
                                                final CustomerJsonCallBack<ChinaCataListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", name);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_CHINESEMEDICINELIST_URL, jsonObject.toJSONString(), callback);
    }
}
