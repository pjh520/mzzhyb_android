package com.jrdz.zhyb_android.ui.catalogue.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.catalogue.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/23
 * 描    述：
 * ================================================
 */
public class DiseCataModel {
    /**
     * code : 1
     * msg : 请求成功
     * obj : {"currentPage":1,"resultObj":[{"contents_id":"75593","diag_code":"A33.x00","diag_name":"新生儿破伤风","diag_type":"1"}]}
     */

    private String code;
    private String msg;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * name : 新生儿破伤风
         * code : A33.x00
         * diag_type : 1
         * diag_typename : 西医主要诊断
         */

        private String name;
        private String code;
        private String diag_type;
        private String diag_typename;

        //程序自用
        private boolean isChoose;

        public DataBean() {}

        public DataBean(String name, String code, String diag_type, String diag_typename) {
            this.name = name;
            this.code = code;
            this.diag_type = diag_type;
            this.diag_typename = diag_typename;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDiag_type() {
            return diag_type;
        }

        public void setDiag_type(String diag_type) {
            this.diag_type = diag_type;
        }

        public String getDiag_typename() {
            return diag_typename;
        }

        public void setDiag_typename(String diag_typename) {
            this.diag_typename = diag_typename;
        }

        public boolean isChoose() {
            return isChoose;
        }

        public void setChoose(boolean choose) {
            isChoose = choose;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.name);
            dest.writeString(this.code);
            dest.writeString(this.diag_type);
            dest.writeString(this.diag_typename);
            dest.writeByte(this.isChoose ? (byte) 1 : (byte) 0);
        }

        protected DataBean(Parcel in) {
            this.name = in.readString();
            this.code = in.readString();
            this.diag_type = in.readString();
            this.diag_typename = in.readString();
            this.isChoose = in.readByte() != 0;
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //疾病目录列表
    public static void sendDiseCataRequest(final String TAG, String api, String pageindex, String pagesize, String name,
                                           final CustomerJsonCallBack<DiseCataModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", name);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + api, jsonObject.toJSONString(), callback);
    }
}
