package com.jrdz.zhyb_android.ui.catalogue.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：
 * ================================================
 */
public class MedSerCataListModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-01-24 06:43:01
     * data : [{"med_list_codg":"002203020120000-220400005","MedicalServiceItemsName":"临床操作的彩色多普勒超声引导\r\n","prcunt":"台"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * med_list_codg : 002203020120000-220400005
         * MedicalServiceItemsName : 临床操作的彩色多普勒超声引导
         * prcunt : 台
         */

        private String med_list_codg;
        private String MedicalServiceItemsName;
        private String prcunt;
        private String field6;//项目内涵
        private String field4;//项目说明

        public DataBean(String med_list_codg, String medicalServiceItemsName, String prcunt) {
            this.med_list_codg = med_list_codg;
            MedicalServiceItemsName = medicalServiceItemsName;
            this.prcunt = prcunt;
        }

        public String getMed_list_codg() {
            return med_list_codg;
        }

        public void setMed_list_codg(String med_list_codg) {
            this.med_list_codg = med_list_codg;
        }

        public String getMedicalServiceItemsName() {
            return MedicalServiceItemsName;
        }

        public void setMedicalServiceItemsName(String MedicalServiceItemsName) {
            this.MedicalServiceItemsName = MedicalServiceItemsName;
        }

        public String getPrcunt() {
            return prcunt;
        }

        public void setPrcunt(String prcunt) {
            this.prcunt = prcunt;
        }

        public String getField6() {
            return field6;
        }

        public void setField6(String field6) {
            this.field6 = field6;
        }

        public String getField4() {
            return field4;
        }

        public void setField4(String field4) {
            this.field4 = field4;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.med_list_codg);
            dest.writeString(this.MedicalServiceItemsName);
            dest.writeString(this.prcunt);
            dest.writeString(this.field6);
            dest.writeString(this.field4);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.med_list_codg = in.readString();
            this.MedicalServiceItemsName = in.readString();
            this.prcunt = in.readString();
            this.field6 = in.readString();
            this.field4 = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //医疗服务项目目录列表
    public static void sendMedSerCataListRequest(final String TAG, String pageindex, String pagesize, String name,
                                                 final CustomerJsonCallBack<MedSerCataListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", name);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MEDICALSERVICELIST_URL, jsonObject.toJSONString(), callback);
    }
}
