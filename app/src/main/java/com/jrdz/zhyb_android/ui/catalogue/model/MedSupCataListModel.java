package com.jrdz.zhyb_android.ui.catalogue.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：
 * ================================================
 */
public class MedSupCataListModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-01-24 03:48:42
     * data : [{"med_list_codg":"C01010100101007049000000001\r\n","hi_genname":"001-支架\r\n","spec":"JSNB-14030\r\n","spec_mol":"12","mcs_matl":null,"prodentp_name":"常州新区佳森医用支架器械有限公司\r\n","reg_fil_no":"国械注准20173464608\r\n"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * med_list_codg : C01010100101007049000000001
         * hi_genname : 001-支架
         * spec : JSNB-14030
         * spec_mol : 12
         * mcs_matl : null
         * prodentp_name : 常州新区佳森医用支架器械有限公司
         * reg_fil_no : 国械注准20173464608

         */

        private String med_list_codg;
        private String hi_genname;
        private String spec;
        private String spec_mol;
        private String mcs_matl;
        private String prodentp_name;
        private String reg_fil_no;

        public DataBean(String med_list_codg, String hi_genname, String spec, String spec_mol, String mcs_matl, String prodentp_name, String reg_fil_no) {
            this.med_list_codg = med_list_codg;
            this.hi_genname = hi_genname;
            this.spec = spec;
            this.spec_mol = spec_mol;
            this.mcs_matl = mcs_matl;
            this.prodentp_name = prodentp_name;
            this.reg_fil_no = reg_fil_no;
        }

        public String getMed_list_codg() {
            return med_list_codg;
        }

        public void setMed_list_codg(String med_list_codg) {
            this.med_list_codg = med_list_codg;
        }

        public String getHi_genname() {
            return hi_genname;
        }

        public void setHi_genname(String hi_genname) {
            this.hi_genname = hi_genname;
        }

        public String getSpec() {
            return spec;
        }

        public void setSpec(String spec) {
            this.spec = spec;
        }

        public String getSpec_mol() {
            return spec_mol;
        }

        public void setSpec_mol(String spec_mol) {
            this.spec_mol = spec_mol;
        }

        public String getMcs_matl() {
            return mcs_matl;
        }

        public void setMcs_matl(String mcs_matl) {
            this.mcs_matl = mcs_matl;
        }

        public String getProdentp_name() {
            return prodentp_name;
        }

        public void setProdentp_name(String prodentp_name) {
            this.prodentp_name = prodentp_name;
        }

        public String getReg_fil_no() {
            return reg_fil_no;
        }

        public void setReg_fil_no(String reg_fil_no) {
            this.reg_fil_no = reg_fil_no;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.med_list_codg);
            dest.writeString(this.hi_genname);
            dest.writeString(this.spec);
            dest.writeString(this.spec_mol);
            dest.writeString(this.mcs_matl);
            dest.writeString(this.prodentp_name);
            dest.writeString(this.reg_fil_no);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.med_list_codg = in.readString();
            this.hi_genname = in.readString();
            this.spec = in.readString();
            this.spec_mol = in.readString();
            this.mcs_matl = in.readString();
            this.prodentp_name = in.readString();
            this.reg_fil_no = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //中药饮片目录列表
    public static void sendMedSupCataListRequest(final String TAG, String pageindex, String pagesize,String name,
                                                 final CustomerJsonCallBack<MedSupCataListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", name);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MEDICALSUPPLIESLIST_URL, jsonObject.toJSONString(), callback);
    }
}
