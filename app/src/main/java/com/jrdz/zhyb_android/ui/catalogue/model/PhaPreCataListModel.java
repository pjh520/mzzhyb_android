package com.jrdz.zhyb_android.ui.catalogue.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：
 * ================================================
 */
public class PhaPreCataListModel {
    /**
     * code : 1
     * msg : 请求成功
     * obj : {"currentPage":1,"resultObj":[{"aprvno_begndate":" ","drug_spec_code":" ","drug_type_name":" ","med_list_codg":"X61000000000000000000000001","prodentp_name":" ","reg_nam":"无"}]}
     */

    private String code;
    private String msg;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * dosforom :"null"
         * drug_prodname :  "null"
         * each_dos :"X61000000000000000000000001"
         * med_list_codg :"null"
         * user_frqu : "null"
         */
        private String dosform;
        private String drug_prodname;
        private String each_dos;
        private String med_list_codg;
        private String used_frqu;

        public DataBean(String med_list_codg,String drug_prodname,String dosform,  String each_dos,  String used_frqu) {
            this.med_list_codg = med_list_codg;
            this.drug_prodname = drug_prodname;
            this.dosform = dosform;
            this.each_dos = each_dos;
            this.used_frqu = used_frqu;
        }

        public String getDosform() {
            return dosform;
        }

        public void setDosform(String dosform) {
            this.dosform = dosform;
        }

        public String getDrug_prodname() {
            return drug_prodname;
        }

        public void setDrug_prodname(String drug_prodname) {
            this.drug_prodname = drug_prodname;
        }

        public String getEach_dos() {
            return each_dos;
        }

        public void setEach_dos(String each_dos) {
            this.each_dos = each_dos;
        }

        public String getMed_list_codg() {
            return med_list_codg;
        }

        public void setMed_list_codg(String med_list_codg) {
            this.med_list_codg = med_list_codg;
        }

        public String getUsed_frqu() {
            return used_frqu;
        }

        public void setUsed_frqu(String used_frqu) {
            this.used_frqu = used_frqu;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.dosform);
            dest.writeString(this.drug_prodname);
            dest.writeString(this.each_dos);
            dest.writeString(this.med_list_codg);
            dest.writeString(this.used_frqu);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.dosform = in.readString();
            this.drug_prodname = in.readString();
            this.each_dos = in.readString();
            this.med_list_codg = in.readString();
            this.used_frqu = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //中药饮片目录列表
    public static void sendPhaPreCataListRequest(final String TAG, String pageindex, String pagesize, String name,
                                                 final CustomerJsonCallBack<PhaPreCataListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", name);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_PHAPRELIST_URL, jsonObject.toJSONString(), callback);
    }
}
