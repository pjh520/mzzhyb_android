package com.jrdz.zhyb_android.ui.catalogue.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：
 * ================================================
 */
public class WestCataListModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-01-23 11:27:42
     * data : [{"WMCPMCatalogueId":1,"med_list_codg":"XA01ABD075A002010100483","reg_nam":"地喹氯铵含片","drugadstacode":"86900483000019","reg_dosform":"片剂(口含)\r\n","reg_spec":"0.25mg\r\n","EnterpriseName":"汕头经济特区明治医药有限公司","ApprovalNumber":"国药准字H20067255"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * med_list_codg : XA01ABD075A002010100483
         * reg_nam : 地喹氯铵含片
         * reg_dosform : 片剂(口含)
         * reg_spec : 0.25mg
         * EnterpriseName : 汕头经济特区明治医药有限公司
         * ApprovalNumber : 国药准字H20067255
         */

        private String med_list_codg;
        private String reg_nam;
        private String reg_dosform;
        private String reg_spec;
        private String EnterpriseName;
        private String ApprovalNumber;
        private String listType;

        public DataBean(String med_list_codg, String reg_nam, String reg_dosform, String reg_spec, String enterpriseName, String approvalNumber) {
            this.med_list_codg = med_list_codg;
            this.reg_nam = reg_nam;
            this.reg_dosform = reg_dosform;
            this.reg_spec = reg_spec;
            EnterpriseName = enterpriseName;
            ApprovalNumber = approvalNumber;
        }

        public String getMed_list_codg() {
            return med_list_codg;
        }

        public void setMed_list_codg(String med_list_codg) {
            this.med_list_codg = med_list_codg;
        }

        public String getReg_nam() {
            return reg_nam;
        }

        public void setReg_nam(String reg_nam) {
            this.reg_nam = reg_nam;
        }

        public String getReg_dosform() {
            return reg_dosform;
        }

        public void setReg_dosform(String reg_dosform) {
            this.reg_dosform = reg_dosform;
        }

        public String getReg_spec() {
            return reg_spec;
        }

        public void setReg_spec(String reg_spec) {
            this.reg_spec = reg_spec;
        }

        public String getEnterpriseName() {
            return EnterpriseName;
        }

        public void setEnterpriseName(String EnterpriseName) {
            this.EnterpriseName = EnterpriseName;
        }

        public String getApprovalNumber() {
            return ApprovalNumber;
        }

        public String getListType() {
            return listType;
        }

        public void setListType(String listType) {
            this.listType = listType;
        }

        public void setApprovalNumber(String ApprovalNumber) {
            this.ApprovalNumber = ApprovalNumber;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.med_list_codg);
            dest.writeString(this.reg_nam);
            dest.writeString(this.reg_dosform);
            dest.writeString(this.reg_spec);
            dest.writeString(this.EnterpriseName);
            dest.writeString(this.ApprovalNumber);
            dest.writeString(this.listType);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.med_list_codg = in.readString();
            this.reg_nam = in.readString();
            this.reg_dosform = in.readString();
            this.reg_spec = in.readString();
            this.EnterpriseName = in.readString();
            this.ApprovalNumber = in.readString();
            this.listType = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //西药目录列表
    public static void sendCatalogueRequest(final String TAG, String pageindex, String pagesize,String name,
                                            final CustomerJsonCallBack<WestCataListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", name);
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_WESTERNMEDICINELIST_URL, jsonObject.toJSONString(), callback);
    }

    //扫码查询西药中成药
    public static void sendScanCodeListRequest(final String TAG, String barCode,final CustomerJsonCallBack<WestCataListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("barCode", barCode);
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_SCANCODELIST_URL, jsonObject.toJSONString(), callback);
    }
}
