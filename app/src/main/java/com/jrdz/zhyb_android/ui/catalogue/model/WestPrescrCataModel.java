package com.jrdz.zhyb_android.ui.catalogue.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.jrdz.zhyb_android.database.CatalogueModel;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.catalogue.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/5/8
 * 描    述：西药中成药处方  选择药品信息
 * ================================================
 */
public class WestPrescrCataModel implements Parcelable {
    private ArrayList<DataBean> data = new ArrayList<>();//存储选中的药品目录
    private String diagResult;//诊断结果
    private String hejiPrice;//合计价格
    private String prescr_accessoryId;//处方图片id
    private String prescr_accessoryUrl;//处方图片url
    private String prescrNo;//处方号

    public static class DataBean implements Parcelable {
        private CatalogueModel catalogueModel;//接口获取到的药品信息
        private int consump = 1;//每次用量
        private String consumpCompany="片";//每次用量 单位
        private String frequency="每日一次";//频率
        private int num = 1;//数量
        private String numCompany="片";//数量 单位
        private String usage="饭后  口服";//用法
        private String remarks="";//备注

        public CatalogueModel getCatalogueModel() {
            return catalogueModel;
        }

        public void setCatalogueModel(CatalogueModel catalogueModel) {
            this.catalogueModel = catalogueModel;
        }

        public int getConsump() {
            return consump;
        }

        public void setConsump(int consump) {
            this.consump = consump;
        }

        public String getConsumpCompany() {
            return consumpCompany;
        }

        public void setConsumpCompany(String consumpCompany) {
            this.consumpCompany = consumpCompany;
        }

        public String getFrequency() {
            return frequency;
        }

        public void setFrequency(String frequency) {
            this.frequency = frequency;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

        public String getNumCompany() {
            return numCompany;
        }

        public void setNumCompany(String numCompany) {
            this.numCompany = numCompany;
        }

        public String getUsage() {
            return usage;
        }

        public void setUsage(String usage) {
            this.usage = usage;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.catalogueModel, flags);
            dest.writeInt(this.consump);
            dest.writeString(this.consumpCompany);
            dest.writeString(this.frequency);
            dest.writeInt(this.num);
            dest.writeString(this.numCompany);
            dest.writeString(this.usage);
            dest.writeString(this.remarks);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.catalogueModel = in.readParcelable(CatalogueModel.class.getClassLoader());
            this.consump = in.readInt();
            this.consumpCompany = in.readString();
            this.frequency = in.readString();
            this.num = in.readInt();
            this.numCompany = in.readString();
            this.usage = in.readString();
            this.remarks = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    public ArrayList<DataBean> getData() {
        return data;
    }

    public void setData(ArrayList<DataBean> data) {
        this.data = data;
    }

    public String getDiagResult() {
        return diagResult;
    }

    public void setDiagResult(String diagResult) {
        this.diagResult = diagResult;
    }

    public String getHejiPrice() {
        return hejiPrice;
    }

    public void setHejiPrice(String hejiPrice) {
        this.hejiPrice = hejiPrice;
    }

    public String getPrescr_accessoryId() {
        return prescr_accessoryId;
    }

    public void setPrescr_accessoryId(String prescr_accessoryId) {
        this.prescr_accessoryId = prescr_accessoryId;
    }

    public String getPrescr_accessoryUrl() {
        return prescr_accessoryUrl;
    }

    public void setPrescr_accessoryUrl(String prescr_accessoryUrl) {
        this.prescr_accessoryUrl = prescr_accessoryUrl;
    }

    public String getPrescrNo() {
        return prescrNo;
    }

    public void setPrescrNo(String prescrNo) {
        this.prescrNo = prescrNo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.data);
        dest.writeString(this.diagResult);
        dest.writeString(this.hejiPrice);
        dest.writeString(this.prescr_accessoryId);
        dest.writeString(this.prescr_accessoryUrl);
        dest.writeString(this.prescrNo);
    }

    public WestPrescrCataModel() {
    }

    protected WestPrescrCataModel(Parcel in) {
        this.data = in.createTypedArrayList(DataBean.CREATOR);
        this.diagResult = in.readString();
        this.hejiPrice = in.readString();
        this.prescr_accessoryId = in.readString();
        this.prescr_accessoryUrl = in.readString();
        this.prescrNo = in.readString();
    }

    public static final Parcelable.Creator<WestPrescrCataModel> CREATOR = new Parcelable.Creator<WestPrescrCataModel>() {
        @Override
        public WestPrescrCataModel createFromParcel(Parcel source) {
            return new WestPrescrCataModel(source);
        }

        @Override
        public WestPrescrCataModel[] newArray(int size) {
            return new WestPrescrCataModel[size];
        }
    };
}
