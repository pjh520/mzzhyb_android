package com.jrdz.zhyb_android.ui.home.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.graphics.ColorUtils;
import androidx.core.widget.NestedScrollView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.wheel.TimeWheelUtils;
import com.frame.compiler.widget.customPop.CustomerBottomListPop;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/9
 * 描    述：添加科室
 * ================================================
 */
public class AddDepartmentActivity extends BaseActivity implements CustomerBottomListPop.IBottomChooseListener<DataDicModel> {
    private NestedScrollView mScrollview;
    protected EditText mEtHospDeptName,mEtHospDeptCodg,mEtDeptResperName,mEtDeptResperTel,mEtDrPsncnt,mEtPharPsncnt,mEtNursPsncnt
            ,mEtTecnPsncnt,mEtAprvBedCnt,mEtHiCrtfBedCnt,mEtDeptMedServScp,mEtItro,mEtMemo;
    protected TextView mTvCaty,mTvDeptEstbdat,mTvBegntime,mTvEndtime,mTvPoolareaNo;
    protected ShapeTextView mTvAdd;
    protected ImageView mIvBegntime;

    private CustomerBottomListPop customerBottomListPop;
    private TimeWheelUtils timeWheelUtils;
    protected List<DataDicModel> catyData;
    private int bannerHeight;


    @Override
    public int getLayoutId() {
        return R.layout.activity_add_department_manage;
    }

    @Override
    public void initView() {
        super.initView();
        mScrollview= findViewById(R.id.scrollview);
        mEtHospDeptName = findViewById(R.id.et_hosp_dept_name);
        mEtHospDeptCodg = findViewById(R.id.et_hosp_dept_codg);
        mTvCaty = findViewById(R.id.tv_caty);
        mTvDeptEstbdat = findViewById(R.id.tv_dept_estbdat);
        mTvBegntime = findViewById(R.id.tv_begntime);
        mIvBegntime= findViewById(R.id.iv_begntime);
        mTvEndtime = findViewById(R.id.tv_endtime);
        mEtDeptResperName = findViewById(R.id.et_dept_resper_name);
        mEtDeptResperTel = findViewById(R.id.et_dept_resper_tel);
        mEtDrPsncnt = findViewById(R.id.et_dr_psncnt);
        mEtPharPsncnt = findViewById(R.id.et_phar_psncnt);
        mEtNursPsncnt = findViewById(R.id.et_nurs_psncnt);
        mEtTecnPsncnt = findViewById(R.id.et_tecn_psncnt);
        mEtAprvBedCnt = findViewById(R.id.et_aprv_bed_cnt);
        mEtHiCrtfBedCnt = findViewById(R.id.et_hi_crtf_bed_cnt);
        mEtDeptMedServScp = findViewById(R.id.et_dept_med_serv_scp);
        mTvPoolareaNo = findViewById(R.id.tv_poolarea_no);
        mEtItro = findViewById(R.id.et_itro);
        mEtMemo = findViewById(R.id.et_memo);
        mTvAdd = findViewById(R.id.tv_add);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(false)
                .titleBar(mTitleBar);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        super.initData();
        bannerHeight = getResources().getDimensionPixelOffset(R.dimen.dp_110);
        catyData=CommonlyUsedDataUtils.getInstance().getCatyData();
        customerBottomListPop = new CustomerBottomListPop(this, this);
        timeWheelUtils = new TimeWheelUtils();
        timeWheelUtils.isShowDay(true, true, true, true, true, true);
        //获取参保就医区划
        String insuplcAdmdvsText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("AREA_ADMVS", MechanismInfoUtils.getAdmdvs());
        mTvPoolareaNo.setText(insuplcAdmdvsText);

        // TODO: 2022/2/16 代码填充测试数据
//        mEtHospDeptName.setText("全科医疗科");
//        mEtHospDeptCodg.setText("01");
//        mTvCaty.setTag(catyData.get(1).getValue());
//        mTvCaty.setText(catyData.get(1).getLabel());
//        mTvDeptEstbdat.setText("2012-11-10");
//        mTvBegntime.setText("2012-11-10");
//        mTvEndtime.setText("2022-11-10");
//        mEtDeptResperName.setText("甄三");
//        mEtDeptResperTel.setText("13859005800");
//        mEtDrPsncnt.setText("1");
//        mEtPharPsncnt.setText("1");
//        mEtNursPsncnt.setText("1");
//        mEtTecnPsncnt.setText("1");
//        mEtAprvBedCnt.setText("10");
//        mEtHiCrtfBedCnt.setText("1");
//        mEtDeptMedServScp.setText("全科医疗");
        mEtItro.setText(MechanismInfoUtils.getFixmedinsName()+"全科医疗科");
//        mEtMemo.setText(MechanismInfoUtils.getFixmedinsName()+"全科医疗科");
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvCaty.setOnClickListener(this);
        mTvDeptEstbdat.setOnClickListener(this);
        mTvBegntime.setOnClickListener(this);
        mTvEndtime.setOnClickListener(this);
        mTvAdd.setOnClickListener(this);

        mScrollview.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY <= 0) {
                    mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                            , ContextCompat.getColor(AddDepartmentActivity.this, R.color.colorAccent), 0));
                } else if (scrollY <= bannerHeight) {
                    float alpha = (float) scrollY / bannerHeight;
                    mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                            , ContextCompat.getColor(AddDepartmentActivity.this, R.color.colorAccent), alpha));
                } else {
                    mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                            , ContextCompat.getColor(AddDepartmentActivity.this, R.color.colorAccent), 1));
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()) {
            case R.id.tv_caty://选择科别
                KeyboardUtils.hideSoftInput(AddDepartmentActivity.this);
                customerBottomListPop.setDatas("1", "请选择科室", mTvCaty.getText().toString(),catyData);
                customerBottomListPop.showPopupWindow();
                break;
            case R.id.tv_dept_estbdat://选择成立日期
                KeyboardUtils.hideSoftInput(AddDepartmentActivity.this);
                timeWheelUtils.showTimeWheel(AddDepartmentActivity.this, "成立日期", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mTvDeptEstbdat.setText(EmptyUtils.strEmpty(dateInfo));
                    }
                });
                break;
            case R.id.tv_begntime://选择开始时间
                KeyboardUtils.hideSoftInput(AddDepartmentActivity.this);
                timeWheelUtils.showTimeWheel(AddDepartmentActivity.this, "开始时间", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mTvBegntime.setText(EmptyUtils.strEmpty(dateInfo));
                    }
                });
                break;
            case R.id.tv_endtime://选择结束时间
                KeyboardUtils.hideSoftInput(AddDepartmentActivity.this);
                timeWheelUtils.showTimeWheel(AddDepartmentActivity.this, "结束时间", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mTvEndtime.setText(EmptyUtils.strEmpty(dateInfo));
                    }
                });
                break;
            case R.id.tv_add://新增科室
                addDepartment();
                break;
        }
    }

    @Override
    public void onItemClick(String tag,DataDicModel str) {
        switch (tag) {
            case "1":
                mTvCaty.setTag(str.getValue());
                mTvCaty.setText(EmptyUtils.strEmpty(str.getText()));
                break;
        }
    }

    //新增科室
    protected void addDepartment() {
        if (EmptyUtils.isEmpty(mEtHospDeptName.getText().toString())) {
            showShortToast("请输入医院科室名称");
            return;
        }
        if (EmptyUtils.isEmpty(mEtHospDeptCodg.getText().toString())) {
            showShortToast("请输入医院科室编码");
            return;
        }
        if (EmptyUtils.isEmpty(mTvCaty.getTag().toString())) {
            showShortToast("请选择科别");
            return;
        }
        if (EmptyUtils.isEmpty(mTvDeptEstbdat.getText().toString())) {
            showShortToast("请选择科室成立日期");
            return;
        }
        if (EmptyUtils.isEmpty(mTvBegntime.getText().toString())) {
            showShortToast("请选择开始时间");
            return;
        }
        if (EmptyUtils.isEmpty(mEtDeptResperName.getText().toString())) {
            showShortToast("请输入科室负责人姓名");
            return;
        }
        if (EmptyUtils.isEmpty(mEtDeptResperTel.getText().toString())) {
            showShortToast("请输入科室负责人电话");
            return;
        }
        if (!"1".equals(String.valueOf(mEtDeptResperTel.getText().charAt(0))) || mEtDeptResperTel.getText().length() != 11) {
            showShortToast("请输入正确的电话号码");
            return;
        }

        if (EmptyUtils.isEmpty(mEtDrPsncnt.getText().toString())) {
            showShortToast("请输入医师人数");
            return;
        }
        if (EmptyUtils.isEmpty(mEtPharPsncnt.getText().toString())) {
            showShortToast("请输入药师人数");
            return;
        }
        if (EmptyUtils.isEmpty(mEtNursPsncnt.getText().toString())) {
            showShortToast("请输入护士人数");
            return;
        }
        if (EmptyUtils.isEmpty(mEtTecnPsncnt.getText().toString())) {
            showShortToast("请输入技师人数");
            return;
        }
        if (EmptyUtils.isEmpty(mEtAprvBedCnt.getText().toString())) {
            showShortToast("请输入批准床位数量");
            return;
        }
        if (EmptyUtils.isEmpty(mEtItro.getText().toString())) {
            showShortToast("请输入简介");
            return;
        }

        showWaitDialog();
        BaseModel.sendAddDeptRequest(TAG, mEtHospDeptCodg.getText().toString(), mTvCaty.getTag().toString(),
                mEtHospDeptName.getText().toString(), mTvBegntime.getText().toString(),
                mTvEndtime.getText().toString(), mEtItro.getText().toString(), mEtDeptResperName.getText().toString(),
                mEtDeptResperTel.getText().toString(), mEtDeptMedServScp.getText().toString(),
                mTvDeptEstbdat.getText().toString(), mEtAprvBedCnt.getText().toString(), mEtHiCrtfBedCnt.getText().toString(),
                EmptyUtils.strEmpty(MechanismInfoUtils.getAdmdvs()), mEtDrPsncnt.getText().toString(), mEtPharPsncnt.getText().toString(),
                mEtNursPsncnt.getText().toString(), mEtTecnPsncnt.getText().toString(), mEtMemo.getText().toString(), new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("添加科室成功");
                        setResult(RESULT_OK);
                        finish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (catyData!=null){
            catyData.clear();
            catyData=null;
        }

        if (customerBottomListPop != null) {
            customerBottomListPop.dismiss();
            customerBottomListPop.onDestroy();
        }

        if (timeWheelUtils != null) {
            timeWheelUtils.dissTimeWheel();
            timeWheelUtils = null;
        }
    }

    public static void newIntance(Activity activity, int requestCode) {
        Intent intent = new Intent(activity, AddDepartmentActivity.class);
        activity.startActivityForResult(intent,requestCode);
    }
}
