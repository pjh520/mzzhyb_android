package com.jrdz.zhyb_android.ui.home.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.ColorUtils;
import androidx.core.widget.NestedScrollView;

import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.MD5Util;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.frame.compiler.widget.countDownTime.CustomCountDownTimer;
import com.frame.compiler.widget.glide.GlideUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.model.DepartmentManageModel;
import com.jrdz.zhyb_android.ui.home.model.KVModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/11
 * 描    述：新增医师
 * ================================================
 */
public class AddDoctorActivity extends BaseActivity implements CompressUploadSinglePicUtils.IChoosePic {
    private NestedScrollView mScrollview;
    protected LinearLayout mLlDept, mLlUserid, mLlPwd;
    protected View mLineDept, mLineUserid, linePwd;
    protected TextView mTvDeptId, mTvDescUserid;
    protected EditText mEtUserid;
    protected EditText mEtUsername;
    protected EditText mEtPhone, mEtVerifCode, mEtPassword;
    protected ShapeFrameLayout mSflAddPhoto;
    protected FrameLayout mFlCert;
    protected ImageView mIvDoctorPhoto, mIvDelete;
    protected TextView mTvDrType, mTvTime;
    protected ShapeTextView mTvAdd;

    private WheelUtils wheelUtils;
    private int choosePos1 = 0;
    private DepartmentManageModel.DataBean resultObjBean;
    private CompressUploadSinglePicUtils compressUploadSinglePicUtils;
    private int bannerHeight;
    private CustomCountDownTimer mCustomCountDownTimer;

    @Override
    public int getLayoutId() {
        return R.layout.activity_add_doctor;
    }

    @Override
    public void initView() {
        super.initView();
        mScrollview = findViewById(R.id.scrollview);
        mLlDept = findViewById(R.id.ll_dept);
        mTvDeptId = findViewById(R.id.tv_deptId);
        mLineDept = findViewById(R.id.line_dept);
        mEtUserid = findViewById(R.id.et_userid);
        mLlUserid = findViewById(R.id.ll_userid);
        mLineUserid = findViewById(R.id.line_userid);
        mTvDescUserid = findViewById(R.id.tv_desc_userid);
        mEtUsername = findViewById(R.id.et_username);
        mLlPwd = findViewById(R.id.ll_pwd);
        linePwd = findViewById(R.id.line_pwd);
        mEtPassword = findViewById(R.id.et_password);
        mTvDrType = findViewById(R.id.tv_drType);
        mEtPhone = findViewById(R.id.et_phone);
        mEtVerifCode = findViewById(R.id.et_verif_code);
        mTvTime = findViewById(R.id.tv_time);
        mSflAddPhoto = findViewById(R.id.sfl_add_photo);
        mFlCert = findViewById(R.id.fl_cert);
        mIvDoctorPhoto = findViewById(R.id.iv_doctor_photo);
        mIvDelete = findViewById(R.id.iv_delete);
        mTvAdd = findViewById(R.id.tv_add);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(false)
                .titleBar(mTitleBar);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        bannerHeight = getResources().getDimensionPixelOffset(R.dimen.dp_321);

        super.initData();
        wheelUtils = new WheelUtils();

        if ("1".equals(MechanismInfoUtils.getFixmedinsType())) {//医院
            mLlDept.setVisibility(View.VISIBLE);
            mLineDept.setVisibility(View.VISIBLE);
        } else if ("2".equals(MechanismInfoUtils.getFixmedinsType())) {//药店
            mLlDept.setVisibility(View.GONE);
            mLineDept.setVisibility(View.GONE);
        }

        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvDeptId.setOnClickListener(this);
        mTvDrType.setOnClickListener(this);
        mSflAddPhoto.setOnClickListener(this);
        mIvDoctorPhoto.setOnClickListener(this);
        mIvDelete.setOnClickListener(this);
        mTvAdd.setOnClickListener(this);
        mTvTime.setOnClickListener(this);
        mScrollview.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY <= 0) {
                    mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                            , ContextCompat.getColor(AddDoctorActivity.this, R.color.colorAccent), 0));
                } else if (scrollY <= bannerHeight) {
                    float alpha = (float) scrollY / bannerHeight;
                    mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                            , ContextCompat.getColor(AddDoctorActivity.this, R.color.colorAccent), alpha));
                } else {
                    mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                            , ContextCompat.getColor(AddDoctorActivity.this, R.color.colorAccent), 1));
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()) {
            case R.id.tv_deptId://获取科室
                DepartmentManageActivity.newIntance(AddDoctorActivity.this, "2", Constants.RequestCode.SELECT_DEPART_CODE);
                break;
            case R.id.tv_drType://获取类型
                KeyboardUtils.hideSoftInput(AddDoctorActivity.this);
                wheelUtils.showWheel(AddDoctorActivity.this, "", choosePos1,
                        CommonlyUsedDataUtils.getInstance().getDrType(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                choosePos1 = choosePos;
                                mTvDrType.setTag(dateInfo.getCode());
                                mTvDrType.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                            }
                        });
                break;
            case R.id.sfl_add_photo://添加图片
                KeyboardUtils.hideSoftInput(AddDoctorActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(CompressUploadSinglePicUtils.PIC_ADDDOCTOR_TAG);
                break;
            case R.id.iv_doctor_photo://查看图片
                OpenImage.with(AddDoctorActivity.this)
                        //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                        .setClickImageView(mIvDoctorPhoto)
                        //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                        .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                        //RecyclerView的数据
                        .setImageUrl(String.valueOf(mIvDoctorPhoto.getTag(R.id.tag_2)), MediaType.IMAGE)
                        //点击的ImageView所在数据的位置
                        .setClickPosition(0)
                        //开始展示大图
                        .show();
                break;
            case R.id.iv_delete://删除图片
                mSflAddPhoto.setVisibility(View.VISIBLE);
                mSflAddPhoto.setEnabled(true);

                mIvDoctorPhoto.setTag(R.id.tag_1, "");
                mIvDoctorPhoto.setTag(R.id.tag_2, "");
                mFlCert.setVisibility(View.GONE);
                break;
            case R.id.tv_time://获取短信验证码
                //2022-07-25 先请求接口 接口返回发送短信验证码成功 然后开始倒计时
                sendSms();
                break;
            case R.id.tv_add://新增医师
                addDoctor();
                break;
        }
    }

    //发送短信
    private void sendSms() {
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString().trim()) || mEtPhone.getText().toString().length() < 11) {
            showShortToast("手机号码格式不正确!");
            return;
        }
        showWaitDialog();
        BaseModel.sendSendsmsRequest(TAG, mEtPhone.getText().toString(), "7", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();

                //发送短信验证码成功
                mTvTime.setEnabled(false);
                showShortToast("验证码已发送");
                mTvTime.setTextColor(getResources().getColor(R.color.color_ff0202));
                setTimeStart(60000);
            }
        });
    }

    //开启定时器  millisecond:倒计时的时间
    private void setTimeStart(long millisecond) {
        //防止内存泄漏
        if (null != mCustomCountDownTimer) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        mCustomCountDownTimer = new CustomCountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long S = millisUntilFinished / 1000;
                mTvTime.setText(S < 10 ? "0" + S + "s" : S + "s");
            }

            @Override
            public void onFinish() {
                mTvTime.setEnabled(true);
                mTvTime.setTextColor(getResources().getColor(R.color.color_4870e0));
                mTvTime.setText("获取验证码");
            }
        };

        mCustomCountDownTimer.start();
    }

    //新增医师
    protected void addDoctor() {
        if ("1".equals(MechanismInfoUtils.getFixmedinsType()) && (mTvDeptId.getTag() == null || EmptyUtils.isEmpty(String.valueOf(mTvDeptId.getTag())))) {//医院
            showShortToast("请选择科室");
            return;
        }
        if (EmptyUtils.isEmpty(mTvDrType.getText().toString())) {
            showShortToast("请选择人员类型");
            return;
        }
        if (EmptyUtils.isEmpty(mEtUserid.getText().toString())) {
            showShortToast("请输入人员编码");
            return;
        }

        //医保医师以字母D开头，医保药师以字母Y开头，医保护士以字母N开头，且医师编码由字母和12位数字组成
        switch ((String) mTvDrType.getTag()) {
            case "1"://医师
                if (!mEtUserid.getText().toString().startsWith("D")) {
                    showShortToast("医师以大写字母D开头");
                    return;
                }
                break;
            case "2"://药师
                if (!mEtUserid.getText().toString().startsWith("Y")) {
                    showShortToast("药师以大写字母Y开头");
                    return;
                }
                break;
            case "3"://护士
                if (!mEtUserid.getText().toString().startsWith("N")) {
                    showShortToast("护士以大写字母N开头");
                    return;
                }
                break;
            case "4"://普通店员
                if (!mEtUserid.getText().toString().startsWith("P")) {
                    showShortToast("普通店员以大写字母P开头");
                    return;
                }
                break;
        }

//        if ("1".equals(MechanismInfoUtils.getFixmedinsType())) {//医院
        //判断医师编码是否是13位
        if (mEtUserid.getText().toString().length() != 13) {
            showShortToast("请输入13位人员编码");
            return;
        }
//        } else {//药店
//            //判断医师编码是否是7位
//            if (mEtUserid.getText().toString().length() != 7) {
//                showShortToast("请输入7位人员编码");
//                return;
//            }
//        }

        if (EmptyUtils.isEmpty(mEtUsername.getText().toString())) {
            showShortToast("请输入姓名");
            return;
        }

        if (EmptyUtils.isEmpty(mEtPhone.getText().toString())) {
            showShortToast("请输入电话");
            return;
        }

        if (!"1".equals(String.valueOf(mEtPhone.getText().charAt(0))) || mEtPhone.getText().length() != 11) {
            showShortToast("请输入正确的手机号码");
            return;
        }

//        if (EmptyUtils.isEmpty(mEtVerifCode.getText().toString())) {
//            showShortToast("请输入验证码");
//            return;
//        }

        if (EmptyUtils.isEmpty(mEtPassword.getText().toString())) {
            showShortToast("请输入账户密码");
            return;
        }
        if (mEtPassword.getText().toString().length() < 6) {
            showShortToast("密码长度为6-12位");
            return;
        }

        showWaitDialog();
        BaseModel.sendAddDoctorRequest(TAG, EmptyUtils.strEmpty((String) mTvDeptId.getTag()), mTvDeptId.getText().toString(), mEtUserid.getText().toString(), mEtUsername.getText().toString(),
                MD5Util.up32(mEtPassword.getText().toString()), (String) mTvDrType.getTag(), mTvDrType.getText().toString(), mEtPhone.getText().toString(),
                null == mIvDoctorPhoto.getTag(R.id.tag_1) ? "" : String.valueOf(mIvDoctorPhoto.getTag(R.id.tag_1)),
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("添加医师成功");

                        setResult(RESULT_OK);
                        finish();
                    }
                });
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            GlideUtils.loadImg(url, mIvDoctorPhoto);

            mSflAddPhoto.setVisibility(View.GONE);
            mSflAddPhoto.setEnabled(false);

            mIvDoctorPhoto.setTag(R.id.tag_1, accessoryId);
            mIvDoctorPhoto.setTag(R.id.tag_2, url);
            mFlCert.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }
    //--------------------------选择图片----------------------------------------------------

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RequestCode.SELECT_DEPART_CODE && resultCode == RESULT_OK) {
            resultObjBean = data.getParcelableExtra("data");

            mTvDeptId.setTag(resultObjBean.getHosp_dept_codg());
            mTvDeptId.setText(EmptyUtils.strEmpty(resultObjBean.getHosp_dept_name()));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        resultObjBean = null;
        if (wheelUtils != null) {
            wheelUtils.dissWheel();
        }

        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }

        if (mCustomCountDownTimer != null) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
    }

    public static void newIntance(Activity activity, int requestCode) {
        Intent intent = new Intent(activity, AddDoctorActivity.class);
        activity.startActivityForResult(intent, requestCode);
    }
}
