package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.index.activity.ActivateDeviceActivity;
import com.jrdz.zhyb_android.utils.AppManager_Acivity;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/1/19
 * 描    述： 绑定关系
 * ================================================
 */
public class BindRelatActivity extends BaseActivity {
    private TextView mTvFixmedinsCode;
    private TextView mTvFixmedinsName;
    private TextView mTvUscc;
    private TextView mTvFixmedinsType;
    private TextView mTvHospLv;
    private TextView mTvDescribe;
    private ShapeTextView mTvApplyUnbind;
    private LinearLayout mLlHospLv;

    @Override
    public int getLayoutId() {
        return R.layout.activity_bind_relat;
    }

    @Override
    public void initView() {
        mTvFixmedinsCode = findViewById(R.id.tv_fixmedins_code);
        mTvFixmedinsName = findViewById(R.id.tv_fixmedins_name);
        mTvUscc = findViewById(R.id.tv_uscc);
        mTvFixmedinsType = findViewById(R.id.tv_fixmedins_type);
        mTvHospLv = findViewById(R.id.tv_hosp_lv);
        mTvDescribe = findViewById(R.id.tv_describe);
        mTvApplyUnbind = findViewById(R.id.tv_apply_unbind);
        mLlHospLv= findViewById(R.id.ll_hosp_lv);
    }

    @Override
    public void initData() {
        String fixmedinsTypeText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("FIXMEDINS_TYPE", MechanismInfoUtils.getFixmedinsType());
        String hospLvText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("HOSP_LV", MechanismInfoUtils.getHospLv());
        mTvFixmedinsCode.setText(EmptyUtils.strEmpty(MechanismInfoUtils.getFixmedinsCode()));
        mTvFixmedinsName.setText(EmptyUtils.strEmpty(MechanismInfoUtils.getFixmedinsName()));
        mTvUscc.setText(EmptyUtils.strEmpty(MechanismInfoUtils.getUscc()));
        mTvFixmedinsType.setText(EmptyUtils.strEmpty(fixmedinsTypeText));
        mTvHospLv.setText(EmptyUtils.strEmpty(hospLvText));

        if ("1".equals(MechanismInfoUtils.getFixmedinsType())){
            mLlHospLv.setVisibility(View.VISIBLE);
        }else {
            mLlHospLv.setVisibility(View.GONE);
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvApplyUnbind.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()){
            case R.id.tv_apply_unbind://申请解绑
                showWaitDialog();
                applyUnbind();
                break;
        }
    }

    //申请解绑
    private void applyUnbind() {
        BaseModel.sendApplyUnbindingRequest(TAG, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                ActivateDeviceActivity.newIntance(BindRelatActivity.this);
                goFinish();
            }
        });
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, BindRelatActivity.class);
        context.startActivity(intent);
    }
}
