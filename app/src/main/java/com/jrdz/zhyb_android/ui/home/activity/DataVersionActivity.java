package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.database.UserInfoModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.index.model.DataDicListModel;
import com.jrdz.zhyb_android.utils.MMKVUtils;

import org.litepal.LitePal;

import java.util.Iterator;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/1/22
 * 描    述：数据版本
 * ================================================
 */
public class DataVersionActivity extends BaseActivity {
    private TextView mTvDict;
    private ShapeTextView mStvDictReset;
    private ShapeTextView mTvDataSync;
    private int count = 0;
    private CustomerDialogUtils customerDialogUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_data_version;
    }

    @Override
    public void initView() {
        super.initView();

        mTvDict = findViewById(R.id.tv_dict);
        mStvDictReset = findViewById(R.id.stv_dict_reset);
        mTvDataSync = findViewById(R.id.tv_data_sync);
    }

    @Override
    public void initData() {
        super.initData();
        customerDialogUtils = new CustomerDialogUtils();
        mTvDict.setText("数据字典版本：" + MMKVUtils.getString("ver", "19000101000000"));
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mStvDictReset.setOnClickListener(this);
        mTvDataSync.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()) {
            case R.id.stv_dict_reset://数据字典版本重置
                customerDialogUtils.showDialog(DataVersionActivity.this, "提示", "是否确认重置版本号?", "取消", "确定",
                        new CustomerDialogUtils.IDialogListener() {

                            @Override
                            public void onBtn01Click() {
                            }

                            @Override
                            public void onBtn02Click() {
                                MMKVUtils.putString("ver", "19000101000000");//存储数据字典版本号
                                showShortToast("版本重置成功");
                                mTvDict.setText("数据字典版本：19000101000000");
                            }
                        });
                break;
            case R.id.tv_data_sync://数据同步
                count = 0;
                if ("19000101000000".equals(MMKVUtils.getString("ver", "19000101000000"))) {
                    LitePal.deleteAll(DataDicModel.class);
                }

                showWaitDialog("数据同步中...");
                getDataDicList();
                break;
        }
    }

    //获取字典数据
    private void getDataDicList() {
        DataDicListModel.sendDataDicListRequest(TAG, MMKVUtils.getString("ver", "19000101000000"), new CustomerJsonCallBack<DataDicListModel>() {
            @Override
            public void onRequestError(DataDicListModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(DataDicListModel returnData) {
                saveData(returnData.getMax_ver(), returnData.getData());
            }
        });
    }

    //保存字典数据到数据库
    private void saveData(String maxVer, List<DataDicModel> datas) {
        if (datas != null && !datas.isEmpty()) {
            mTvDict.setText("数据字典版本：" + maxVer);
            DataDicModel dataDicModel = LitePal.findFirst(DataDicModel.class);
            if (dataDicModel == null) {//首次存储数据
                saveDataDicFirst(maxVer, datas);
            } else {//已存在字典数据，这时候应该增量更新
                saveDataDic(maxVer, datas);
            }
        } else {
            hideWaitDialog();
            showShortToast("暂无数据更新");
        }
    }

    //首次存储字典表数据
    private void saveDataDicFirst(String maxVer, List<DataDicModel> datas) {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                Iterator<DataDicModel> iterator = datas.iterator();
                while  (iterator .hasNext()){
                    DataDicModel dataDicModel=iterator.next();
                    if ("0".equals(dataDicModel.getIsEnable())) {//开启 数据改变
                        iterator.remove();
                    }
                }

                boolean isSaveSuccess = LitePal.saveAll(datas);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isSaveSuccess) {
                            hideWaitDialog();
                            if (EmptyUtils.isEmpty(maxVer)) {
                                showShortToast("返回版本号有误");
                            } else {
                                MMKVUtils.putString("ver", maxVer);//存储数据字典版本号
                                showShortToast("数据同步成功");
                            }
                        } else {
                            if (0 == count) {
                                showShortToast("字典存储失败，重新存储");
                                count++;
                                saveData(maxVer, datas);
                            } else {
                                hideWaitDialog();
                                showShortToast("存储空间不足。");
                            }
                        }
                    }
                });
            }
        });
    }

    //已存在字典数据，这时候应该增量更新
    private void saveDataDic(String maxVer, List<DataDicModel> datas) {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                for (DataDicModel data : datas) {
                    List<DataDicModel> dataDicModels = LitePal.where("type=? and value=?", data.getType(), data.getValue()).find(DataDicModel.class);
                    if (dataDicModels != null && !dataDicModels.isEmpty()) {
                        if ("1".equals(data.getIsEnable())) {//开启 数据改变
                            data.update(dataDicModels.get(0).getId());
                        } else {//删除该数据
                            for (DataDicModel dataDicModel : dataDicModels) {
                                dataDicModel.delete();
                            }
                        }
                    } else {
                        if ("1".equals(data.getIsEnable())) {//开启 数据改变
                            data.save();
                        }
                    }
                }

                hideWaitDialog();
                if (EmptyUtils.isEmpty(maxVer)) {
                    showShortToast("返回版本号有误");
                } else {
                    MMKVUtils.putString("ver", maxVer);//存储数据字典版本号
                    showShortToast("数据同步成功");
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
        }
        super.onDestroy();
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, DataVersionActivity.class);
        context.startActivity(intent);
    }
}
