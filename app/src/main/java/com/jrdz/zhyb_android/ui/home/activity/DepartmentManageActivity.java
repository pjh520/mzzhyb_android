package com.jrdz.zhyb_android.ui.home.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.adapter.DepartmentManageAdapter;
import com.jrdz.zhyb_android.ui.home.model.DepartmentManageModel;
import com.jrdz.zhyb_android.ui.home.model.StmtTotalListModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.util.ArrayList;
import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/10
 * 描    述：科室管理列表
 * ================================================
 */
public class DepartmentManageActivity extends BaseRecyclerViewActivity {
    private String from="1";

    @Override
    public void initAdapter() {
        mAdapter = new DepartmentManageAdapter();
    }

    @Override
    public void initData() {
        from=getIntent().getStringExtra("from");
        super.initData();
        setRightTitleView("新增");

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        DepartmentManageModel.sendDepartmentManageRequest(TAG,String.valueOf(mPageNum), "20", new CustomerJsonCallBack<DepartmentManageModel>() {
            @Override
            public void onRequestError(DepartmentManageModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(DepartmentManageModel returnData) {
                hideRefreshView();
                List<DepartmentManageModel.DataBean> infos = returnData.getData();
                if (mAdapter!=null&&infos != null) {
                    for (DepartmentManageModel.DataBean info : infos) {
                        for (DataDicModel insutypeDatum : CommonlyUsedDataUtils.getInstance().getCatyData()) {
                            if (info.getCaty().equals(insutypeDatum.getValue())) {
                                info.setCaty_name(insutypeDatum.getLabel());
                                break;
                            }
                        }
                    }

                    if (mPageNum == 0) {
                        mAdapter.setNewData(infos);
                    } else {
                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void rightTitleViewClick() {
        AddDepartmentActivity.newIntance(DepartmentManageActivity.this,Constants.RequestCode.DEL_ADD_DEPART_CODE);
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter,view,position);

        if ("1".equals(from)){//进入科室编辑页面
            UpdateOrDelDepartActivity.newIntance(DepartmentManageActivity.this,position,((DepartmentManageAdapter)adapter).getData().get(position),
                    Constants.RequestCode.DEL_UPDATE_DEPART_CODE);
        }else if ("2".equals(from)){//返回科室数据
            Intent intent=new Intent();
            intent.putExtra("data", ((DepartmentManageAdapter)adapter).getData().get(position));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==Constants.RequestCode.DEL_UPDATE_DEPART_CODE&&resultCode==RESULT_OK){
            String type = data.getStringExtra("type");
            int pos = data.getIntExtra("pos",-1);
            switch (type){
                case "0"://删除
                    if (pos!=-1){
                        mAdapter.remove(pos);
                    }
                    break;
                case "1"://更新
                    DepartmentManageModel.DataBean resultObjBean = data.getParcelableExtra("data");
                    mAdapter.getData().set(pos,resultObjBean);
                    mAdapter.notifyItemChanged(pos);
                    break;
            }
        }else if (requestCode==Constants.RequestCode.DEL_ADD_DEPART_CODE&&resultCode==RESULT_OK){
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    }

    //from 1:科室管理列表 2：选择科室管理
    public static void newIntance(Context context,String from) {
        Intent intent = new Intent(context, DepartmentManageActivity.class);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }

    //from 1:科室管理列表 2：选择科室管理
    public static void newIntance(Activity activity, String from, int requestCode) {
        Intent intent = new Intent(activity, DepartmentManageActivity.class);
        intent.putExtra("from", from);
        activity.startActivityForResult(intent,requestCode);
    }
}
