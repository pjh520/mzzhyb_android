package com.jrdz.zhyb_android.ui.home.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.adapter.DoctorManageAdapter;
import com.jrdz.zhyb_android.ui.home.model.ApplyResultModel;
import com.jrdz.zhyb_android.ui.home.model.DoctorManageModel;

import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/11
 * 描    述：医师管理列表
 * ================================================
 */
public class DoctorManageActivity extends BaseRecyclerViewActivity {
    private String from = "1";
    private CustomerDialogUtils customerDialogUtils;
    //操作监听
    private ObserverWrapper<ApplyResultModel> mApplyResultObserver=new ObserverWrapper<ApplyResultModel>() {
        @Override
        public void onChanged(@Nullable ApplyResultModel value) {
            if (mAdapter==null)return;
            switch (value.getType()){
                case "0"://删除
                    mAdapter.remove(value.getPos());
                    break;
                case "1"://开通
                    ((DoctorManageAdapter)mAdapter).getItem(value.getPos()).setApproveStatus("5");
                    if (!EmptyUtils.isEmpty(value.getAccessoryId())){
                        ((DoctorManageAdapter)mAdapter).getItem(value.getPos()).setAccessoryId(value.getAccessoryId());
                    }
                    if (!EmptyUtils.isEmpty(value.getAccessoryUrl())){
                        ((DoctorManageAdapter)mAdapter).getItem(value.getPos()).setAccessoryUrl(value.getAccessoryUrl());
                    }
                    break;
                case "2"://注销
                    ((DoctorManageAdapter)mAdapter).getItem(value.getPos()).setApproveStatus("0");
                    break;
                case "3"://修改手机号成功
                    ((DoctorManageAdapter)mAdapter).getItem(value.getPos()).setDr_phone(value.getPhone());
                    mAdapter.notifyItemChanged(value.getPos());
                    break;
            }
        }
    };

    @Override
    public void initAdapter() {
        mAdapter = new DoctorManageAdapter();
    }

    @Override
    public void initData() {
        from = getIntent().getStringExtra("from");
        super.initData();
        setRightTitleView("新增");
        MsgBus.sendApplyResult().observe(this, mApplyResultObserver);

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        DoctorManageModel.sendDoctorManageModelRequest(TAG, String.valueOf(mPageNum), "20", new CustomerJsonCallBack<DoctorManageModel>() {
            @Override
            public void onRequestError(DoctorManageModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(DoctorManageModel returnData) {
                hideRefreshView();
                List<DoctorManageModel.DataBean> infos = returnData.getData();
                if (mAdapter!=null&&infos != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(infos);
                    } else {
                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void rightTitleViewClick() {
        AddDoctorActivity.newIntance(DoctorManageActivity.this, Constants.RequestCode.ADD_DOCTOR_CODE);
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        DoctorManageModel.DataBean itemData = ((DoctorManageAdapter) adapter).getItem(position);
        if ("1".equals(from)) {//进入人员查看页面
            PersonLookActivity.newIntance(DoctorManageActivity.this, itemData);
        } else if ("2".equals(from)) {//返回医师数据
            Intent intent = new Intent();
            intent.putExtra("data", itemData);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        DoctorManageModel.DataBean itemData = ((DoctorManageAdapter) adapter).getItem(position);
        switch (view.getId()) {
            case R.id.tv_edit://编辑
                EditDoctorActivity.newIntance(DoctorManageActivity.this, position, itemData);
                break;
            case R.id.tv_open://启用
                onOpen(itemData, "1", adapter, position);
                break;
            case R.id.tv_close://禁用
                onClose(itemData, "0", adapter, position);
                break;
        }
    }

    //启用
    private void onOpen(DoctorManageModel.DataBean itemData, String isEnable, BaseQuickAdapter adapter, int position) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(DoctorManageActivity.this, "提示", "注意：启用后用户恢复智慧医保，在线购药的使用。",
                "取消", "确定", new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        //启用医师
                        showWaitDialog();
                        BaseModel.sendEditdoctorIsEnableRequest(TAG, itemData.getDoctorId(), isEnable, new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                showShortToast("启用成功");

                                itemData.setIsEnable("1");
                                adapter.notifyItemChanged(position);
                            }
                        });
                    }
                });
    }

    //禁用
    private void onClose(DoctorManageModel.DataBean itemData, String isEnable, BaseQuickAdapter adapter, int position) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(DoctorManageActivity.this, "提示", "注意：禁用后用户在智慧医保，在线购药将不能进行操作，其他功能不受影响。谨慎操作！",
                "取消", "确定", new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        //禁用医师
                        showWaitDialog();
                        BaseModel.sendEditdoctorIsEnableRequest(TAG, itemData.getDoctorId(), isEnable, new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                showShortToast("禁用成功");

                                itemData.setIsEnable("0");
                                adapter.notifyItemChanged(position);
                            }
                        });
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RequestCode.ADD_DOCTOR_CODE && resultCode == RESULT_OK) {
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
        }
    }

    //from 1:医师管理列表 2：选择医师
    public static void newIntance(Context context, String from) {
        Intent intent = new Intent(context, DoctorManageActivity.class);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }

    //from 1:医师管理列表 2：选择医师
    public static void newIntance(Activity activity, String from, int requestCode) {
        Intent intent = new Intent(activity, DoctorManageActivity.class);
        intent.putExtra("from", from);
        activity.startActivityForResult(intent, requestCode);
    }
}