package com.jrdz.zhyb_android.ui.home.activity;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.countDownTime.CustomCountDownTimer;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.model.ApplyResultModel;
import com.jrdz.zhyb_android.ui.home.model.DoctorManageModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.SettleInApplyActivity_doctor;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

import cody.bus.ObserverWrapper;


/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-09-28
 * 描    述：人员编辑 页面
 * ================================================
 */
public class EditDoctorActivity extends BaseActivity {
    private LinearLayout mLlDept;
    private TextView mTvDeptId;
    private View mLineDept;
    private TextView mTvDrType;
    private EditText mEtUserid;
    private EditText mEtUsername;
    private EditText mEtPhone, mEtVerifCode;
    private TextView mTvTime;
    private ShapeFrameLayout mSflAddPhoto;
    private ImageView mIvDoctorPhoto;
    private ShapeTextView mTvUpdatePhone, mTvUpdate;

    private CustomCountDownTimer mCustomCountDownTimer;
    private int pos = 0;
    private DoctorManageModel.DataBean resultObjBean;
    private CustomerDialogUtils customerDialogUtils;
    //操作监听
    private ObserverWrapper<ApplyResultModel> mApplyResultObserver = new ObserverWrapper<ApplyResultModel>() {
        @Override
        public void onChanged(@Nullable ApplyResultModel value) {
            if (resultObjBean == null) return;
            switch (value.getType()) {
                case "1"://开通
                    resultObjBean.setApproveStatus("5");
                    if (!EmptyUtils.isEmpty(value.getAccessoryId())){
                        resultObjBean.setAccessoryId(value.getAccessoryId());
                    }
                    if (!EmptyUtils.isEmpty(value.getAccessoryUrl())){
                        resultObjBean.setAccessoryUrl(value.getAccessoryUrl());
                    }
                    setDoctorPhoto();
                    break;
                case "2"://注销
                    resultObjBean.setApproveStatus("0");
                    break;
            }
        }
    };


    @Override
    public int getLayoutId() {
        return R.layout.activity_edit_doctor;
    }

    @Override
    public void initView() {
        super.initView();
        mLlDept = findViewById(R.id.ll_dept);
        mTvDeptId = findViewById(R.id.tv_deptId);
        mLineDept = findViewById(R.id.line_dept);
        mTvDrType = findViewById(R.id.tv_drType);
        mEtUserid = findViewById(R.id.et_userid);
        mEtUsername = findViewById(R.id.et_username);
        mEtPhone = findViewById(R.id.et_phone);
        mEtVerifCode = findViewById(R.id.et_verif_code);
        mTvTime = findViewById(R.id.tv_time);
        mSflAddPhoto = findViewById(R.id.sfl_add_photo);
        mIvDoctorPhoto = findViewById(R.id.iv_doctor_photo);
        mTvUpdate = findViewById(R.id.tv_update);
        mTvUpdatePhone = findViewById(R.id.tv_update_phone);
    }

    @Override
    public void initData() {
        pos = getIntent().getIntExtra("pos", 0);
        resultObjBean = getIntent().getParcelableExtra("resultObjBean");
        super.initData();

        setRightTitleView("删除");
        MsgBus.sendApplyResult().observe(this, mApplyResultObserver);

        if ("1".equals(MechanismInfoUtils.getFixmedinsType())) {//定点医院
            mLlDept.setVisibility(View.VISIBLE);
            mLineDept.setVisibility(View.VISIBLE);

            mTvDeptId.setTag(resultObjBean.getHosp_dept_codg());
            mTvDeptId.setText(EmptyUtils.strEmpty(resultObjBean.getHosp_dept_name()));
        } else {//定点药店
            mLlDept.setVisibility(View.GONE);
            mLineDept.setVisibility(View.GONE);
        }

        mTvDrType.setTag(resultObjBean.getDr_type());
        mTvDrType.setText(EmptyUtils.strEmpty(resultObjBean.getDr_type_name()));
        mEtUserid.setText(EmptyUtils.strEmpty(resultObjBean.getDr_code()));
        mEtUsername.setText(EmptyUtils.strEmpty(resultObjBean.getDr_name()));
        mEtPhone.setText(EmptyUtils.strEmpty(resultObjBean.getDr_phone()));

        setDoctorPhoto();

        //管理员入驻智慧药房后此处可以点击“开通智慧药房”
        //管理员未入驻智慧药房，此处“开通智慧药房”按钮置灰
        if (MechanismInfoUtils.isApplySuccess()) {//机构审核通过
            mTvUpdate.setEnabled(true);
        } else {
            mTvUpdate.setEnabled(false);
        }
    }

    //设置职业资格图片
    private void setDoctorPhoto() {
        mIvDoctorPhoto.setTag(R.id.tag_1, EmptyUtils.strEmpty(resultObjBean.getAccessoryId()));
        if (EmptyUtils.isEmpty(resultObjBean.getAccessoryUrl())) {
            mSflAddPhoto.setVisibility(View.VISIBLE);
            mIvDoctorPhoto.setVisibility(View.GONE);
        } else {
            GlideUtils.loadImg(resultObjBean.getAccessoryUrl(), mIvDoctorPhoto);

            mSflAddPhoto.setVisibility(View.GONE);
            mIvDoctorPhoto.setTag(R.id.tag_2, resultObjBean.getAccessoryUrl());
            mIvDoctorPhoto.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvTime.setOnClickListener(this);
        mIvDoctorPhoto.setOnClickListener(this);
        mTvUpdatePhone.setOnClickListener(this);
        mTvUpdate.setOnClickListener(this);
    }

    @Override
    public void rightTitleViewClick() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(EditDoctorActivity.this, "提示", "注意：需要先注销在线购药，且在米脂智慧医保没有数据，谨慎操作！！！",
                "取消", "确定", new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        //删除医师
                        showWaitDialog();
                        BaseModel.sendDelDoctorRequest(TAG, resultObjBean.getDoctorId(), new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                showShortToast("删除医师成功");
                                MsgBus.sendApplyResult().post(new ApplyResultModel("0", pos));
                                goFinish();
                            }
                        });
                    }
                });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_time://获取短信验证码
                // 2022-07-25 先请求接口 接口返回发送短信验证码成功 然后开始倒计时
                sendSms();
                break;
            case R.id.iv_doctor_photo://查看图片
                OpenImage.with(EditDoctorActivity.this)
                        //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                        .setClickImageView(mIvDoctorPhoto)
                        //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                        .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                        //RecyclerView的数据
                        .setImageUrl(String.valueOf(mIvDoctorPhoto.getTag(R.id.tag_2)), MediaType.IMAGE)
                        //点击的ImageView所在数据的位置
                        .setClickPosition(0)
                        //开始展示大图
                        .show();
                break;
            case R.id.tv_update_phone://修改手机号码
                updatePhone();
                break;
            case R.id.tv_update://确认修改
                update();
                break;
        }
    }

    //发送短信
    private void sendSms() {
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString().trim()) || mEtPhone.getText().toString().trim().length() < 11) {
            showShortToast("手机号码格式不正确!");
            return;
        }
        showWaitDialog();
        BaseModel.sendSendsmsRequest(TAG, mEtPhone.getText().toString().trim(), "7", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();

                //发送短信验证码成功
                mTvTime.setEnabled(false);
                showShortToast("验证码已发送");
                mTvTime.setTextColor(getResources().getColor(R.color.color_ff0202));
                setTimeStart(60000);
            }
        });
    }

    //开启定时器  millisecond:倒计时的时间
    private void setTimeStart(long millisecond) {
        //防止内存泄漏
        if (null != mCustomCountDownTimer) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        mCustomCountDownTimer = new CustomCountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long S = millisUntilFinished / 1000;
                mTvTime.setText(S < 10 ? "0" + S + "s" : S + "s");
            }

            @Override
            public void onFinish() {
                mTvTime.setEnabled(true);
                mTvTime.setTextColor(getResources().getColor(R.color.color_4870e0));
                mTvTime.setText("获取验证码");
            }
        };

        mCustomCountDownTimer.start();
    }

    //修改手机号码
    private void updatePhone() {
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString())) {
            showShortToast("请输入电话");
            return;
        }

        if (!"1".equals(String.valueOf(mEtPhone.getText().charAt(0))) || mEtPhone.getText().length() != 11) {
            showShortToast("请输入正确的手机号码");
            return;
        }

//        if (EmptyUtils.isEmpty(mEtVerifCode.getText().toString())) {
//            showShortToast("请输入验证码");
//            return;
//        }

        showWaitDialog();
        BaseModel.sendUpdateDoctorRequest(TAG,EmptyUtils.strEmpty(resultObjBean.getDoctorId()), EmptyUtils.strEmpty(resultObjBean.getHosp_dept_codg()), EmptyUtils.strEmpty(resultObjBean.getHosp_dept_name()),
                EmptyUtils.strEmpty(resultObjBean.getDr_code()), EmptyUtils.strEmpty(resultObjBean.getDr_name()), EmptyUtils.strEmpty(resultObjBean.getDr_pwd()),
                EmptyUtils.strEmpty(resultObjBean.getDr_type()), EmptyUtils.strEmpty(resultObjBean.getDr_type_name()), mEtPhone.getText().toString(),
                EmptyUtils.strEmpty(resultObjBean.getAccessoryId()), new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("修改手机号成功");

                        MsgBus.sendApplyResult().post(new ApplyResultModel("3", pos,mEtPhone.getText().toString()));
                        goFinish();
                    }
                });
    }

    //确认修改
    private void update() {
        SettleInApplyActivity_doctor.newIntance(this, pos, resultObjBean);
    }

    @Override
    protected void onDestroy() {
        if (mCustomCountDownTimer != null) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
        }
        super.onDestroy();
        pos = 0;
        resultObjBean = null;
    }

    public static void newIntance(Activity activity, int pos, DoctorManageModel.DataBean resultObjBean) {
        Intent intent = new Intent(activity, EditDoctorActivity.class);
        intent.putExtra("pos", pos);
        intent.putExtra("resultObjBean", resultObjBean);
        activity.startActivity(intent);
    }
}
