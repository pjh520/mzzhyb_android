package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.SelectCataManageActivity;
import com.jrdz.zhyb_android.ui.home.adapter.LargeVersionAdapter;
import com.jrdz.zhyb_android.ui.home.model.LargeVersionModel;
import com.jrdz.zhyb_android.ui.settlement.activity.OutpatRegListActivtiy;
import com.jrdz.zhyb_android.ui.settlement.activity.OutpatSettleRecordActivtiy;
import com.jrdz.zhyb_android.ui.settlement.activity.PharmacySettleRecordActivtiy;
import com.jrdz.zhyb_android.ui.settlement.activity.QueryPersonalInfoActivity;
import com.jrdz.zhyb_android.ui.settlement.activity.SettleTypeActivity;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/21
 * 描    述：
 * ================================================
 */
public class LargeVersionActivity extends BaseRecyclerViewActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_large_version;
    }

    @Override
    public void initAdapter() {
        mAdapter = new LargeVersionAdapter();
        mAdapter.setHeaderAndEmpty(true);
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        return new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false);
    }

    @Override
    public void initData() {
        super.initData();
        initHeadView();

        if (LoginUtils.isManage()) {
            mAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getLargever_Manage(MechanismInfoUtils.getFixmedinsType()));
        } else {
            mAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getLargever_Physician(MechanismInfoUtils.getFixmedinsType()));
        }
    }

    private void initHeadView() {
        View headView = LayoutInflater.from(this).inflate(R.layout.layout_large_version_headview, mRecyclerView, false);

        mAdapter.addHeaderView(headView);
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter,view,position);

        LargeVersionModel sortItem = ((LargeVersionAdapter) adapter).getItem(position);
        switch (sortItem.getId()) {
            case "1001"://绑定关系
                BindRelatActivity.newIntance(LargeVersionActivity.this);
                break;
            case "1002"://数据版本
                DataVersionActivity.newIntance(LargeVersionActivity.this);
                break;
            case "1003"://科室管理
                DepartmentManageActivity.newIntance(LargeVersionActivity.this, "1");
                break;
            case "1004"://医师管理
                DoctorManageActivity.newIntance(LargeVersionActivity.this, "1");
                break;
            case "1005"://目录管理
                SelectCataManageActivity.newIntance(LargeVersionActivity.this, "1");
                break;
            case "1006"://挂号记录
                if (LoginUtils.isManage()) {
                    if ("1".equals(MechanismInfoUtils.getFixmedinsType())) {//医院
                        showShortToast("请登录医师账户，进行该业务操作");
                    }else {
                        showShortToast("请登录药师账户，进行该业务操作");
                    }
                } else {
                    OutpatRegListActivtiy.newIntance(LargeVersionActivity.this);
                }
                break;
            case "1007"://结算对账
                StmtInfoListActvity.newIntance(LargeVersionActivity.this);
                break;
            case "1008"://月结查询
                MonthSettQueryActivity.newIntance(LargeVersionActivity.this);
                break;
            case "1009"://门诊结算
                if (LoginUtils.isManage()) {
                    showShortToast("请登录医师账户，进行该业务操作");
                } else {
                    SettleTypeActivity.newIntance(LargeVersionActivity.this);
                }
                break;
            case "1010"://药店结算
                if (LoginUtils.isManage()) {
                    showShortToast("请登录药师账户，进行该业务操作");
                } else {
                    QueryPersonalInfoActivity.newIntance(LargeVersionActivity.this, "2");
                }
                break;
            case "1011"://结算记录
                if ("1".equals(MechanismInfoUtils.getFixmedinsType())) {
                    OutpatSettleRecordActivtiy.newIntance(LargeVersionActivity.this);
                } else {
                    PharmacySettleRecordActivtiy.newIntance(LargeVersionActivity.this);
                }
                break;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, LargeVersionActivity.class);
        context.startActivity(intent);
    }
}
