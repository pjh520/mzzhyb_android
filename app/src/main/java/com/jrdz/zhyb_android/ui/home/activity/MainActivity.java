package com.jrdz.zhyb_android.ui.home.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baidu.location.LocationClient;
import com.baidu.ocr.sdk.OCR;
import com.baidu.ocr.sdk.OnResultListener;
import com.baidu.ocr.sdk.exception.OCRError;
import com.baidu.ocr.sdk.model.AccessToken;
import com.frame.compiler.utils.BroadCastReceiveUtils;
import com.frame.compiler.utils.DateUtil;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.viewPage.BaseViewPagerAdapter;
import com.frame.compiler.widget.CustomViewPager;
import com.frame.compiler.widget.costomBottomTab.BottomTabModel;
import com.frame.compiler.widget.costomBottomTab.CustomBottomTabLayout;
import com.frame.compiler.widget.permission.PermissionHelper;
import com.frame.compiler.widget.toast.ToastUtil;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.ui.home.fragment.HomeFragment;
import com.jrdz.zhyb_android.ui.home.fragment.MineFragment;
import com.jrdz.zhyb_android.ui.home.fragment.NewsFragment;
import com.jrdz.zhyb_android.ui.index.activity.ActivateDeviceActivity;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredMainActivity;
import com.jrdz.zhyb_android.ui.login.activity.LoginActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.OpenPrescListActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.OrderDetailActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.OrderDetailActivity_user;
import com.jrdz.zhyb_android.utils.AppManager_Acivity;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.library.jpush_library.PushMessageReceiver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import cody.bus.ObserverWrapper;

public class MainActivity extends BaseActivity implements TextToSpeech.OnInitListener {
    private CustomViewPager mVpHome;
    private CustomBottomTabLayout mBottomTabLayout;

    private String[] mTabNames = {"首页", "资讯公告", "我的"};
    //未选择状态下的本地图片
    private int[] mUnSelectIcons = {
            R.drawable.ic_home_nor,
            R.drawable.ic_news_nor,
            R.drawable.ic_mine_nor};
    //选择状态下的本地图片
    private int[] mSelectIcons = {
            R.drawable.ic_home_pre,
            R.drawable.ic_news_pre,
            R.drawable.ic_mine_pre};

    //未选中的颜色
    private int mUnSelectColor = R.color.color_606265;
    //选中的颜色
    private int mSelectColor = R.color.color_4870e0;
    //tab数据
    private List<BottomTabModel> mBottomTabs = new ArrayList<>();
    private int currentTab;

    private TextToSpeech mTextToSpeech;
    private HashMap myHashAlarm;
    private PermissionHelper permissionHelper;

    //开启activity
    private BroadCastReceiveUtils mStartActivity = new BroadCastReceiveUtils() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra("type");
            switch (type) {
                case "0"://跳转注册激活
                    ActivateDeviceActivity.newIntance(MainActivity.this);
                    finish();
                    break;
                case "2"://账户被注销--跳转登录页面
                    showShortToast("账号已注销,请重新登录");
                    LoginActivity.newIntance(MainActivity.this);
                    break;
                case "3"://出了管理员的其他人员身份被禁用--跳转登录页面
                    Activity activity01 = AppManager_Acivity.getInstance().currentActivity();
                    if (activity01 instanceof LoginActivity){

                    }else {
                        showShortToast("该账号已禁用");
                        LoginActivity.newIntance(MainActivity.this);
                    }
                    break;
            }
        }
    };

    //登录成功信息
    private ObserverWrapper<Integer> mLoginObserve = new ObserverWrapper<Integer>() {
        @Override
        public void onChanged(@Nullable Integer value) {
            //设置机构、用户信息
            if (mWmk != null) {
                mWmk.setText(MechanismInfoUtils.getFixmedinsName(), LoginUtils.getUserName(), DateUtil.getStringDate("yyyyMMdd"));
            }
        }
    };
    //接受推送消息
    private BroadCastReceiveUtils mPushReceiver = new BroadCastReceiveUtils() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra("type");
            String notifiy_text = intent.getStringExtra("notifiy_text");
            String notificationExtras = intent.getStringExtra("extras");

            switch (type){
                case "onNotifyMessageArrived"://通知到达
                    String voiceNotification = MechanismInfoUtils.getVoiceNotification();//（1开启0关闭）
                    try {
                        JSONObject jsonObject = JSON.parseObject(notificationExtras);
                        String voice = jsonObject.getString("voice");//(0不语音1语音）

                        if ("1".equals(voice)&&"1".equals(voiceNotification)&&!EmptyUtils.isEmpty(notifiy_text)){
                            speakOut(notifiy_text);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                case "onNotifyMessageOpened"://点击通知
                    if (!EmptyUtils.isEmpty(notificationExtras)){
                        //type 1、商户订单详情 2、用户订单详情 3、商户首页 4、用户首页
                        try {
                            JSONObject jsonObject = JSON.parseObject(notificationExtras);
                            String open_type = jsonObject.getString("type");
                            String open_OrderNo = jsonObject.getString("OrderNo");

                            switch (EmptyUtils.strEmpty(open_type)){
                                case "1"://商户订单详情
                                    OrderDetailActivity.newIntance(MainActivity.this, open_OrderNo);
                                    break;
                                case "2"://用户订单详情
                                    OrderDetailActivity_user.newIntance(MainActivity.this, open_OrderNo, 2);
                                    break;
                                case "3"://商户首页
                                    MainActivity.newIntance(MainActivity.this, 0);
                                    break;
                                case "4"://用户首页
                                    InsuredMainActivity.newIntance(MainActivity.this, 0);
                                    break;
                                case "5"://订单修改通知
                                    OrderDetailActivity_user.newIntance(MainActivity.this, open_OrderNo, 2);
                                    break;
                                case "6"://开处方列表页面
                                    OpenPrescListActivity.newIntance(MainActivity.this);
                                    break;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initView() {
        super.initView();

        mVpHome = findViewById(R.id.vp_home);
        mBottomTabLayout = findViewById(R.id.bottomTabLayout);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(false)
                .titleBar(mTitleBar);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        super.initData();
        myHashAlarm = new HashMap();
        myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_NOTIFICATION));
        mTextToSpeech = new TextToSpeech(this, this);

        BroadCastReceiveUtils.registerLocalReceiver(this, Constants.Action.SEND_START_MAINACTIVITY_0, mStartActivity);
        BroadCastReceiveUtils.registerLocalReceiver(this, PushMessageReceiver.SEND_PUSH_SPEECH_SOUNDS, mPushReceiver);
        getNotificationPermission();

        currentTab = getIntent().getIntExtra("currentTab", 0);
        MsgBus.sendLoginStatus().observe(this, mLoginObserve);
        initViewPager();
        initBottomTabs();
        setCurrentTab(currentTab);

        //在pos机上面跟华为统一扫码冲突(已做兼容 pos扫码使用pos机自带的)
        if (!Constants.Configure.IS_POS) {
            initAccessToken();
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();
    }

    //申请通知栏权限
    private void getNotificationPermission(){
        if (!XXPermissions.isGranted(this, Permission.POST_NOTIFICATIONS)) {
            if (permissionHelper==null){
                permissionHelper = new PermissionHelper();
            }
            permissionHelper.requestPermission(this, new PermissionHelper.onPermissionListener() {
                @Override
                public void onSuccess() {
                }

                @Override
                public void onNoAllSuccess(String noAllSuccessText) {}

                @Override
                public void onFail(String failText) {
                    showShortToast("通知栏权限申请失败");

                }
            },"通知栏权限:收到消息推送时,没有该权限将不能收到消息提示。", Permission.POST_NOTIFICATIONS);
        }
    }

    //初始化ViewPager
    private void initViewPager() {
        BaseViewPagerAdapter baseViewPagerAndTabsAdapter_new = new BaseViewPagerAdapter(getSupportFragmentManager(), 3) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0://首页
                        return HomeFragment.newIntance();
                    case 1://医保资讯
                        return NewsFragment.newIntance();
                    case 2://我的
                        return MineFragment.newIntance();
                }
                return null;
            }
        };
        mVpHome.setOffscreenPageLimit(2);
        mVpHome.setAdapter(baseViewPagerAndTabsAdapter_new);
    }

    //初始化底部的tab栏目
    private void initBottomTabs() {
        mBottomTabs.clear();
        for (int i = 0; i < mUnSelectIcons.length; i++) {
            BottomTabModel mBottomTab = null;
            mBottomTab = new BottomTabModel(mTabNames[i], mUnSelectColor,
                    mSelectColor, mUnSelectIcons[i], mSelectIcons[i], null, null);
            mBottomTabs.add(mBottomTab);
        }
        mBottomTabLayout.setBottomTabData(mBottomTabs);

        mBottomTabLayout.setUpWithViewPager(mVpHome, new CustomBottomTabLayout.onUpWithViewPagerListener() {
            @Override
            public boolean onTabSelect(int position) {
                mVpHome.setCurrentItem(position, false);//设置当前显示标签页为第一页
                setTabImmersionBar(position);
                return true;
            }

            @Override
            public void onPageSelected(int position) {
                mBottomTabLayout.setCurrentTab(position);
            }
        });
    }

    //设置当前的currentTab
    public void setCurrentTab(int currentTab) {
        if (mBottomTabLayout != null) {
            mBottomTabLayout.setCurrentTab(currentTab);
        }
        if (mVpHome != null) {
            mVpHome.setCurrentItem(currentTab);
        }
    }

    //设置各个tab页面得沉浸式状态栏
    private void setTabImmersionBar(int currentTab) {
        switch (currentTab) {
            case 0:
                ImmersionBar.with(this).statusBarDarkFont(false).transparentStatusBar().init();
                break;
            case 1:
                ImmersionBar.with(this).statusBarDarkFont(true, 0.2f).transparentStatusBar().init();
                break;
            case 2:
                ImmersionBar.with(this).statusBarDarkFont(false).transparentStatusBar().init();
                break;
        }
    }

    //以license文件方式初始化
    private void initAccessToken() {
        OCR.getInstance(this).initAccessToken(new OnResultListener<AccessToken>() {
            @Override
            public void onResult(AccessToken accessToken) {
            }

            @Override
            public void onError(OCRError error) {
                Log.e("baiduOcr", "licence方式获取token失败==" + error.getMessage());
            }
        }, getApplicationContext());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        int currentTab = intent.getIntExtra("currentTab", 0);
        setCurrentTab(currentTab);
    }

    //---------------------语音播报 start--------------------------------------------------
    //使用TextToSpeech 实现文字转语音
    public void speakOut(String text) {
        if (mTextToSpeech != null && !mTextToSpeech.isSpeaking()) {
            //朗读，注意这里三个参数的added in API level 4   四个参数的added in API level 21
            // 执行朗读的方法
//            speak(CharSequence text,int queueMode,Bundle params,String utteranceId);
//            // 将朗读的的声音记录成音频文件
//            synthesizeToFile(CharSequence text,Bundle params,File file,String utteranceId);
//            第二个参数queueMode用于指定发音队列模式，两种模式选择
//                （1）TextToSpeech.QUEUE_FLUSH：该模式下在有新任务时候会清除当前语音任务，执行新的语音任务
//                （2）TextToSpeech.QUEUE_ADD：该模式下会把新的语音任务放到语音任务之后，
//            等前面的语音任务执行完了才会执行新的语音任务

            mTextToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, myHashAlarm);
        }
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            // 设置音调,1.0是常规
            mTextToSpeech.setPitch(1.0f);
            //设定语速 ，默认1.0正常语速
            mTextToSpeech.setSpeechRate(1.0f);
            HashMap myHashAlarm = new HashMap();
            myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_NOTIFICATION));
            int result = mTextToSpeech.setLanguage(Locale.CHINA);
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showShortToast("数据丢失或不支持,请按照资讯-pos语音不能播放问题解决文档处理");
                    }
                });
            }
        }
    }
    //---------------------语音播报 end--------------------------------------------------

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mTextToSpeech != null) {
            mTextToSpeech.stop();
            mTextToSpeech.shutdown();//关闭,释放资源
            mTextToSpeech = null;
        }
        BroadCastReceiveUtils.unregisterLocalReceiver(this, mStartActivity);
        BroadCastReceiveUtils.unregisterLocalReceiver(this, mPushReceiver);
    }

    //正常流程进来
    public static void newIntance(Context context, int currentTab) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("currentTab", currentTab);
        context.startActivity(intent);
    }
}