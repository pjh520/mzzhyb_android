package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;

import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.adapter.MedInsuCataQueryAdapter;
import com.jrdz.zhyb_android.ui.home.model.MedInsuCataModel;
import com.jrdz.zhyb_android.ui.home.model.NotifyModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/7
 * 描    述：医保目录信息查询
 * ================================================
 */
public class MedInsuCataQueryActivity extends BaseRecyclerViewActivity {

    @Override
    public void initAdapter() {
        mAdapter=new MedInsuCataQueryAdapter();
    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        super.getData();

        MedInsuCataModel.sendMedInsuCataRequest(TAG, String.valueOf(mPageNum), "20", new CustomerJsonCallBack<MedInsuCataModel>() {
            @Override
            public void onRequestError(MedInsuCataModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(MedInsuCataModel returnData) {
                hideRefreshView();
            }
        });
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, MedInsuCataQueryActivity.class);
        context.startActivity(intent);
    }
}
