package com.jrdz.zhyb_android.ui.home.activity;

import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.wheel.TimeWheelUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-06-07
 * 描    述： 月结申报
 * ================================================
 */
public class MonthSettDeclarActivity extends BaseActivity {
    private TextView mTvAuditDate;
    private ShapeTextView mTvQuery;

    private TimeWheelUtils auditTimeWheelUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_month_sett_declar;
    }

    @Override
    public void initView() {
        super.initView();

        mTvAuditDate = findViewById(R.id.tv_audit_date);
        mTvQuery = findViewById(R.id.tv_query);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvAuditDate.setOnClickListener(this);
        mTvQuery.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_audit_date://清算年月
                if (auditTimeWheelUtils == null) {
                    auditTimeWheelUtils = new TimeWheelUtils();
                    auditTimeWheelUtils.isShowDay(true, true, true, true, false, false);
                }
                auditTimeWheelUtils.showTimeWheel(MonthSettDeclarActivity.this, "清算年月", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mTvAuditDate.setText(EmptyUtils.strEmpty(dateInfo));
                    }
                });
                break;
            case R.id.tv_query://查询
                if (EmptyUtils.isEmpty(mTvAuditDate.getText().toString())) {
                    showShortToast("请选择清算年月");
                    return;
                }

                String firstDate = mTvAuditDate.getText().toString()+"-01";
                String endDate = getMonthEndData(firstDate);

                showWaitDialog();
                BaseModel.sendMonthSettDeclarRequest(TAG, firstDate, endDate, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("申报成功");
                        setResult(RESULT_OK);
                        goFinish();
                    }
                });
                break;
        }
    }

    //获取每个月的最后一天日期
    private String getMonthEndData(String auditDate){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date =sdf.parse(auditDate);

            Calendar calendar=Calendar.getInstance();
            calendar.setTime(date);

            calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            return sdf.format(calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (auditTimeWheelUtils != null) {
            auditTimeWheelUtils.dissTimeWheel();
            auditTimeWheelUtils = null;
        }
    }
}
