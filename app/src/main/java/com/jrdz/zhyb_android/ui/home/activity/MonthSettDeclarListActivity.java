package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.wheel.TimeWheelUtils;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.database.CatalogueModel;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.adapter.MonthSettDeclarListAdapter;
import com.jrdz.zhyb_android.ui.home.model.MonthSettDeclarListModel;
import com.jrdz.zhyb_android.ui.home.model.StmtTotalListModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-06-07
 * 描    述：  月结申报列表页面
 * ================================================
 */
public class MonthSettDeclarListActivity extends BaseRecyclerViewActivity {
    private TextView mTvTime, mTvSearchTime;
    private ShapeLinearLayout mSllTime;

    private TimeWheelUtils timeWheelUtils;
    private String begndate="";

    @Override
    public int getLayoutId() {
        return R.layout.activity_month_sett_declar_list;
    }

    @Override
    public void initView() {
        super.initView();

        mSllTime = findViewById(R.id.sll_time);
        mTvTime = findViewById(R.id.tv_time);
        mTvSearchTime = findViewById(R.id.tv_search_time);
    }

    @Override
    public void initAdapter() {
        mAdapter=new MonthSettDeclarListAdapter();
    }

    @Override
    public void initData() {
        super.initData();

        setRightTitleView("申报");

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mSllTime.setOnClickListener(this);
        mTvSearchTime.setOnClickListener(this);

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void getData() {
        super.getData();
        MonthSettDeclarListModel.sendMonthSettDeclarListRequest(TAG,begndate, String.valueOf(mPageNum), "20", new CustomerJsonCallBack<MonthSettDeclarListModel>() {
            @Override
            public void onRequestError(MonthSettDeclarListModel returnData, String msg) {
                hideRefreshView();
                if (!isFinishing()){
                    setLoadMoreFail();
                    showShortToast(msg);
                }
            }

            @Override
            public void onRequestSuccess(MonthSettDeclarListModel returnData) {
                hideRefreshView();
                if (!isFinishing()){
                    List<MonthSettDeclarListModel.DataBean> infos = returnData.getData();
                    if (mAdapter!=null&&infos != null) {
                        for (MonthSettDeclarListModel.DataBean info : infos) {
                            for (DataDicModel insutypeDatum : CommonlyUsedDataUtils.getInstance().getInsutypeData()) {
                                if (info.getInsutype().equals(insutypeDatum.getValue())) {
                                    info.setInsutype_name(insutypeDatum.getLabel());
                                    break;
                                }
                            }
                        }

                        if (mPageNum == 0) {
                            mAdapter.setNewData(infos);
                            if (infos.size() <= 0) {
                                mAdapter.isUseEmpty(true);
                            }
                        } else {
                            mAdapter.addData(infos);
                            mAdapter.loadMoreComplete();
                        }

                        if (infos.isEmpty()) {
                            if (mAdapter.getData().size() < 5) {
                                mAdapter.loadMoreEnd(true);
                            } else {
                                mAdapter.loadMoreEnd();
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.sll_time://选择时间
                if (timeWheelUtils == null) {
                    timeWheelUtils = new TimeWheelUtils();
                    timeWheelUtils.isShowDay(true, true, true, true, true, true);
                }
                timeWheelUtils.showTimeWheel(MonthSettDeclarListActivity.this, "选择申报日期", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        begndate=EmptyUtils.strEmpty(dateInfo);
                        mTvTime.setText(EmptyUtils.strEmpty(dateInfo));
                    }
                });
                break;
            case R.id.tv_search_time://时间搜索
                if (EmptyUtils.isEmpty(mTvTime.getText().toString())) {
                    showShortToast("请选择申报日期");
                    return;
                }

                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
        }
    }

    @Override
    public void rightTitleViewClick() {
        startActivityForResult(MonthSettDeclarActivity.class, new OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                showWaitDialog();
                onRefresh(mRefreshLayout);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timeWheelUtils != null) {
            timeWheelUtils.dissTimeWheel();
            timeWheelUtils = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, MonthSettDeclarListActivity.class);
        context.startActivity(intent);
    }
}
