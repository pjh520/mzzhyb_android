package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.LogUtils;
import com.frame.compiler.widget.tabLayout.CommonTabLayout;
import com.frame.compiler.widget.tabLayout.listener.CustomTabEntity;
import com.frame.compiler.widget.tabLayout.listener.OnTabSelectListener;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.pos.CPayDevice;
import com.jrdz.zhyb_android.pos.CPayPrint;
import com.jrdz.zhyb_android.ui.home.adapter.MonthSettQueryAdapter;
import com.jrdz.zhyb_android.ui.home.model.MonthSettModel;
import com.jrdz.zhyb_android.ui.home.model.MonthSettTypeModel;
import com.jrdz.zhyb_android.ui.home.model.QueryMonSetlModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/24
 * 描    述：月结查询
 * ================================================
 */
public class MonthSettQueryActivity extends BaseActivity implements BaseQuickAdapter.OnItemClickListener {
    private FrameLayout mFlForward, mFlBackoff;
    private RecyclerView mRlDate;
    private CommonTabLayout mStbTablayout;
    private TextView mTvContent;

    private MonthSettModel currentMonthSettModel;
    private ShapeTextView currentView;
    private LinearLayoutManager mLinearLayoutManager;
    private ArrayList<CustomTabEntity> monthSettTypeDatas;
    private String settlementClassification = "";

    @Override
    public int getLayoutId() {
        return R.layout.activity_month_sett_query;
    }

    @Override
    public void initView() {
        super.initView();
        mFlForward = findViewById(R.id.fl_forward);
        mFlBackoff = findViewById(R.id.fl_backoff);
        mRlDate = findViewById(R.id.rl_date);
        mStbTablayout = findViewById(R.id.stb_tablayout);
        mTvContent = findViewById(R.id.tv_content);
    }

    @Override
    public void initData() {
        super.initData();

        setRightTitleView("打印");
        if (Constants.Configure.IS_POS){
            mTitleBar.getRightView().setVisibility(View.VISIBLE);
        }else {
            mTitleBar.getRightView().setVisibility(View.GONE);
        }
        //设置列表
        mLinearLayoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        mRlDate.setLayoutManager(mLinearLayoutManager);
        MonthSettQueryAdapter monthSettQueryAdapter = new MonthSettQueryAdapter();
        mRlDate.setAdapter(monthSettQueryAdapter);
        monthSettQueryAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getSixDateData());

        //设置类型
        monthSettTypeDatas = CommonlyUsedDataUtils.getInstance().getMonthSettTypeData();
        mStbTablayout.setTabData(monthSettTypeDatas);

        monthSettQueryAdapter.setOnItemClickListener(this);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mStbTablayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                CustomTabEntity item = monthSettTypeDatas.get(position);
                if (item instanceof MonthSettTypeModel) {
                    settlementClassification = ((MonthSettTypeModel) item).type;
                }

                queryMonSetl();
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
//        mFlForward.setOnClickListener(this);
//        mFlBackoff.setOnClickListener(this);
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            return;
        }
        MonthSettModel item = ((MonthSettQueryAdapter) baseQuickAdapter).getItem(i);
        if (!item.isChoose()) {
            if (currentMonthSettModel != null) {
                currentMonthSettModel.setChoose(false);
            }

            if (currentView != null) {
                currentView.getShapeDrawableBuilder().setStrokeColor(getResources().getColor(R.color.color_333333))
                        .setSolidColor(getResources().getColor(R.color.bar_transparent)).intoBackground();
                currentView.setTextColor(getResources().getColor(R.color.color_333333));
            }

            currentView = view.findViewById(R.id.tv);
            currentMonthSettModel = item;

            currentMonthSettModel.setChoose(true);
            currentView.getShapeDrawableBuilder().setStrokeColor(getResources().getColor(R.color.bar_transparent))
                    .setSolidColor(getResources().getColor(R.color.color_4970e0)).intoBackground();
            currentView.setTextColor(getResources().getColor(R.color.white));

            queryMonSetl();
        }
    }

    //查询月结
    private void queryMonSetl() {
        if (currentMonthSettModel == null) {
            return;
        }
        showWaitDialog();
        QueryMonSetlModel.sendQueryMonSetlRequest(TAG, currentMonthSettModel.getTag(), settlementClassification, new CustomerJsonCallBack<QueryMonSetlModel>() {
            @Override
            public void onRequestError(QueryMonSetlModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
                mTvContent.setText("");
                mTvContent.setVisibility(View.GONE);
            }

            @Override
            public void onRequestSuccess(QueryMonSetlModel returnData) {
                hideWaitDialog();
                mTvContent.setVisibility(View.VISIBLE);
                if (returnData.getData() != null) {
                    mTvContent.setText(EmptyUtils.strEmpty(returnData.getData().getContent()));
                } else {
                    mTvContent.setText("");
                }
            }
        });
    }

    @Override
    public void rightTitleViewClick() {
        if (!EmptyUtils.isEmpty(mTvContent.getText().toString())){
            printText(mTvContent.getText().toString());
        }else {
            showShortToast("月结信息为空");
        }
    }


    //打印文本
    private void printText(String content) {
        showWaitDialog("正在打印...");
        CPayPrint printer = CPayDevice.getCPayPrint();

        try {
            printer.printSingle(content, new CPayPrint.PrintCallback() {
                @Override
                public void onCall(int code, String msg) {
                    hideWaitDialog();
                    if (code != 9999) {
                        showShortToast(msg);
                    }
                }
            });
        } catch (Exception e) {
            LogUtils.e("=========================打印异常了"+e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_forward:
                int lastVisible = mLinearLayoutManager.findLastVisibleItemPosition();
                mRlDate.smoothScrollToPosition(lastVisible + 3);
                break;
            case R.id.fl_backoff:
                int firstVisible = mLinearLayoutManager.findFirstVisibleItemPosition();
                mRlDate.smoothScrollToPosition(firstVisible - 3 <= 0 ? 0 : firstVisible - 3);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (currentMonthSettModel != null) {
            currentMonthSettModel = null;
        }

        if (currentView != null) {
            currentView = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, MonthSettQueryActivity.class);
        context.startActivity(intent);
    }
}
