package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customWebview.CommonJs;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.model.NewsDetailModel;
import com.jrdz.zhyb_android.ui.insured.activity.MizhiNewsDetailActivity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/28
 * 描    述： 公告详情
 * ================================================
 */
public class NewsDetailActivity extends MyWebViewActivity {
    protected TextView mTvTitle;
    protected TextView mTvTime;

    protected String id = "";

    @Override
    public int getLayoutId() {
        return R.layout.activity_news_detail;
    }

    @Override
    public void initView() {
        mTvTitle = findViewById(R.id.tv_title);
        mTvTime = findViewById(R.id.tv_time);
        super.initView();
    }

    @Override
    public void initData() {
        id = getIntent().getStringExtra("id");
        super.initData();
        mTitleBar.setVisibility(View.VISIBLE);

        showWaitDialog();
        getData();
    }

    //获取数据
    public void getData() {
        NewsDetailModel.sendNewsDetailRequest(TAG, id, new CustomerJsonCallBack<NewsDetailModel>() {
            @Override
            public void onRequestError(NewsDetailModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(NewsDetailModel returnData) {
                hideWaitDialog();
                NewsDetailModel.DataBean data = returnData.getData();
                if (data != null ) {
                    mTitle=EmptyUtils.strEmpty(data.getTitle());
                    setTitle(EmptyUtils.strEmpty(data.getTitle()));
                    mTvTitle.setText(EmptyUtils.strEmpty(data.getTitle()));
                    mTvTime.setText(EmptyUtils.strEmpty(data.getCreateDT()));
                    x5Webview.loadHtml(CommonJs.headTop+EmptyUtils.strEmpty(data.getContent())+CommonJs.IMG_CLICK_JS+CommonJs.headBottom);
                    x5Webview.addJavascriptInterface(this, "imageOnclick");//代理名
                }
            }
        });
    }

    @JavascriptInterface
    public void openImage(String src) {
        OpenImage.with(NewsDetailActivity.this)
                //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                .setNoneClickView()
                //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                //RecyclerView的数据
                .setImageUrl(src, MediaType.IMAGE)
                //点击的ImageView所在数据的位置
                .setClickPosition(0)
                //开始展示大图
                .show();
    }

    public static void newIntance(Context context, String id) {
        Intent intent = new Intent(context, NewsDetailActivity.class);
        intent.putExtra("id", id);
        context.startActivity(intent);
    }
}
