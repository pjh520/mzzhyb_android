package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.home.model.DoctorManageModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.LookGoodsActivity;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

import java.math.BigDecimal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-09-28
 * 描    述：人员查看 页面
 * ================================================
 */
public class PersonLookActivity extends BaseActivity {
    private TextView mTvDeptId;
    private TextView mTvDrType;
    private TextView mTvUserid;
    private TextView mTvUsername;
    private TextView mTvPhone;
    private ImageView mIvCert;

    private DoctorManageModel.DataBean resultObjBean;

    @Override
    public int getLayoutId() {
        return R.layout.activity_person_look;
    }

    @Override
    public void initView() {
        super.initView();
        mTvDeptId = findViewById(R.id.tv_deptId);
        mTvDrType = findViewById(R.id.tv_drType);
        mTvUserid = findViewById(R.id.tv_userid);
        mTvUsername = findViewById(R.id.tv_username);
        mTvPhone = findViewById(R.id.tv_phone);
        mIvCert = findViewById(R.id.iv_cert);
    }

    @Override
    public void initData() {
        resultObjBean = getIntent().getParcelableExtra("resultObjBean");
        super.initData();

        if ("1".equals(MechanismInfoUtils.getFixmedinsType())){
            mTvDeptId.setText("所属科室："+EmptyUtils.strEmptyToText(resultObjBean.getHosp_dept_name(),"--"));
            mTvUserid.setText("人员编码："+EmptyUtils.strEmptyToText(resultObjBean.getDr_code(),"--"));
        }

        mTvDrType.setText("人员类型："+EmptyUtils.strEmptyToText(resultObjBean.getDr_type_name(),"--"));
        mTvUsername.setText("姓名："+EmptyUtils.strEmptyToText(resultObjBean.getDr_name(),"--"));
        mTvPhone.setText("电话："+EmptyUtils.strEmptyToText(resultObjBean.getDr_phone(),"--"));
        //设置营业执照
        if (!EmptyUtils.isEmpty(resultObjBean.getAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, resultObjBean.getAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    ShapeLinearLayout.LayoutParams layoutParams = (ShapeLinearLayout.LayoutParams) mIvCert.getLayoutParams();
                    if (radio >= 1.7) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_340))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_200) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_200));
                    }

                    mIvCert.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(resultObjBean.getAccessoryUrl(), mIvCert);
                }
            });
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mIvCert.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.iv_cert:
                if (EmptyUtils.isEmpty(resultObjBean.getAccessoryUrl())){
                    return;
                }
                OpenImage.with(PersonLookActivity.this)
                        //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                        .setClickImageView(mIvCert)
                        //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                        .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                        //RecyclerView的数据
                        .setImageUrl(resultObjBean.getAccessoryUrl(), MediaType.IMAGE)
                        //点击的ImageView所在数据的位置
                        .setClickPosition(0)
                        //开始展示大图
                        .show();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        resultObjBean = null;
    }

    public static void newIntance(Context context, DoctorManageModel.DataBean resultObjBean) {
        Intent intent = new Intent(context, PersonLookActivity.class);
        intent.putExtra("resultObjBean", resultObjBean);
        context.startActivity(intent);
    }
}
