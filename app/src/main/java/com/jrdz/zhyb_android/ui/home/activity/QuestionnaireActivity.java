package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSON;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.view.ShapeEditText;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.adapter.AnswerAdapter;
import com.jrdz.zhyb_android.ui.home.model.AddQuestionnaireModel;
import com.jrdz.zhyb_android.ui.home.model.DoctorManageModel;
import com.jrdz.zhyb_android.ui.home.model.QuestionnaireModel;
import com.jrdz.zhyb_android.utils.appgray.ObservableArrayList;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-09-06
 * 描    述：调查问卷
 * ================================================
 */
public class QuestionnaireActivity extends BaseActivity {
    private LinearLayout mLlContain;
    private ShapeTextView mTvCommit;
    private List<QuestionnaireModel.DataBean> infos;
    private ArrayList<AddQuestionnaireModel> addQuestionnaireModels=new ArrayList<>();
    private String questionnaire;

    @Override
    public int getLayoutId() {
        return R.layout.activity_questionnaire;
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mTitleBar).keyboardEnable(true);
        mImmersionBar.init();
    }

    @Override
    public void initView() {
        super.initView();

        mLlContain = findViewById(R.id.ll_contain);
        mTvCommit = findViewById(R.id.tv_commit);
    }

    @Override
    public void initData() {
        questionnaire=getIntent().getStringExtra("questionnaire");
        super.initData();

        showWaitDialog();
        getData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvCommit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()){
            case R.id.tv_commit://提交
                commit();
                break;
        }
    }

    public void getData() {
        QuestionnaireModel.sendQuestionnaireRequest(TAG,EmptyUtils.strEmpty(questionnaire), new CustomerJsonCallBack<QuestionnaireModel>() {
            @Override
            public void onRequestError(QuestionnaireModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(QuestionnaireModel returnData) {
                hideWaitDialog();
                infos=returnData.getData();
                if (infos!=null){
                    setListData(infos);

                    mTvCommit.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    //设置列表数据
    private void setListData(List<QuestionnaireModel.DataBean> infos) {
        mLlContain.removeAllViews();
        for (QuestionnaireModel.DataBean info : infos) {
            View view = LayoutInflater.from(this).inflate(R.layout.layout_questionnaire_item, mLlContain, false);
            TextView tvTitle = view.findViewById(R.id.tv_title);
            CustomeRecyclerView crvAnswer = view.findViewById(R.id.crv_answer);
            ShapeEditText etContent= view.findViewById(R.id.et_content);
            //设置问题标题
            tvTitle.setText(EmptyUtils.strEmpty(info.getItemTitle()));
            if ("3".equals(info.getItemType())){//输入框模式
                crvAnswer.setVisibility(View.GONE);
                etContent.setVisibility(View.VISIBLE);
            }else {//答案模式
                crvAnswer.setVisibility(View.VISIBLE);
                etContent.setVisibility(View.GONE);

                //设置答案列表
                crvAnswer.setHasFixedSize(true);
                crvAnswer.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
                AnswerAdapter answerAdapter = new AnswerAdapter();
                crvAnswer.setAdapter(answerAdapter);
                answerAdapter.setNewData(info.getTemplateSubItem());

                answerAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                        if ("1".equals(info.getItemType())) {//单选
                            singleChoose(answerAdapter, view, i);
                        } else if ("2".equals(info.getItemType())) {//多选
                            multipleChoose(answerAdapter, view, i);
                        }
                    }
                });
            }

            mLlContain.addView(view);
        }
    }

    //单选
    private void singleChoose(AnswerAdapter answerAdapter, View view, int i) {
        QuestionnaireModel.DataBean.TemplateSubItemBean itemData = answerAdapter.getItem(i);
        if (!itemData.isChoose()) {//未选中  那么点击之后 要选中
            ImageView ivRegion02=view.findViewById(R.id.iv_region);

            ImageView currentFlItem = answerAdapter.getCurrentFlItem();
            //判断是否已经有选中的 有的话 直接设置为未选中
            if (currentFlItem != null) {
                currentFlItem.setImageResource(R.drawable.ic_dagou_nor);
            }
            QuestionnaireModel.DataBean.TemplateSubItemBean currentData = answerAdapter.getCurrentData();
            if (currentData != null) {
                currentData.setChoose(false);
            }

            ivRegion02.setImageResource(R.drawable.ic_dagou_pre);
            itemData.setChoose(true);

            answerAdapter.setCurrentFlItem(ivRegion02);
            answerAdapter.setCurrentData(itemData);
        }
    }

    //多选
    private void multipleChoose(AnswerAdapter answerAdapter, View view, int i) {
        QuestionnaireModel.DataBean.TemplateSubItemBean itemData = answerAdapter.getItem(i);
        if (itemData.isChoose()) {//选中  那么点击之后 未选中
            ImageView ivRegion02=view.findViewById(R.id.iv_region);

            ivRegion02.setImageResource(R.drawable.ic_dagou_nor);
            itemData.setChoose(false);
        }else {//未选中  那么点击之后 要选中
            ImageView ivRegion02=view.findViewById(R.id.iv_region);

            ivRegion02.setImageResource(R.drawable.ic_dagou_pre);
            itemData.setChoose(true);
        }
    }

    private void commit() {
        if (infos==null){
            showShortToast("请先选择答案");
            return;
        }

        addQuestionnaireModels.clear();
        QuestionnaireModel.DataBean info;//记录遍历的对象
        boolean hasChoose=false;//记录每一道必须题，是否有选择答案
        for (int i = 0; i < infos.size(); i++) {
            info=infos.get(i);
            hasChoose=false;

            if ("3".equals(info.getItemType())){//输入框模式
                View view=mLlContain.getChildAt(i);
                ShapeEditText etContent= view.findViewById(R.id.et_content);
                if (!EmptyUtils.isEmpty(etContent.getText().toString())){
                    addQuestionnaireModels.add(new AddQuestionnaireModel(info.getItemCode(),etContent.getText().toString(),questionnaire));
                    hasChoose=true;
                }
            }else {//答案模式
                String subItem="";
                if (info.getTemplateSubItem()!=null&&!info.getTemplateSubItem().isEmpty()){
                    for (QuestionnaireModel.DataBean.TemplateSubItemBean answerDatum : info.getTemplateSubItem()) {
                        if (answerDatum.isChoose()){
                            subItem+=answerDatum.getSubItemCode()+"、";
                            hasChoose=true;
                        }
                    }
                    if (!EmptyUtils.isEmpty(subItem)){
                        subItem=subItem.substring(0, subItem.length()-1);
                        addQuestionnaireModels.add(new AddQuestionnaireModel(info.getItemCode(),subItem,questionnaire));
                    }
                }
            }

            if ("1".equals(info.getIsMandatory())&&!hasChoose){
                showShortToast("第"+(i+1)+"道题,"+("3".equals(info.getItemType())?"请填写":"请选择"));
                return;
            }
        }

        showWaitDialog();
        BaseModel.sendAddQuestionnaireRequest(TAG, JSON.toJSONString(addQuestionnaireModels), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast(returnData.getMsg());
                setResult(RESULT_OK);
                goFinish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (infos!=null){
            infos.clear();
            infos=null;
        }
        if (addQuestionnaireModels!=null){
            addQuestionnaireModels.clear();
            addQuestionnaireModels=null;
        }

    }

    public static void newIntance(Context context,String questionnaire) {
        Intent intent = new Intent(context, QuestionnaireActivity.class);
        intent.putExtra("questionnaire", questionnaire);
        context.startActivity(intent);
    }
}
