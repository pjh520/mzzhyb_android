package com.jrdz.zhyb_android.ui.home.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.database.CatalogueModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.activity.SelectCataManageActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.UpdateWestCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.UpdateWestEnaCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.adapter.WestCataEnaListAdapter;
import com.jrdz.zhyb_android.ui.catalogue.model.CatalogueEnableListModel;
import com.jrdz.zhyb_android.ui.catalogue.model.WestCataListModel;
import com.jrdz.zhyb_android.ui.home.model.ScanCataRefreshModel;

import java.util.ArrayList;
import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/24
 * 描    述：扫机构码 获取的数据
 * ================================================
 */
public class ScanCataListActivtiy extends BaseRecyclerViewActivity {
    private String listType,from;
    private ArrayList<CatalogueModel> infos;

    //目录修改监听
    private ObserverWrapper<ScanCataRefreshModel> mOnCataUpdateObserve=new ObserverWrapper<ScanCataRefreshModel>() {
        @Override
        public void onChanged(@Nullable ScanCataRefreshModel value) {
            if (null!=value){
                CatalogueModel changData = ((WestCataEnaListAdapter) mAdapter).getItem(value.getPos());
                if (null!=changData){
                    changData.setFixmedins_hilist_name(EmptyUtils.strEmpty(value.getName()));
                    changData.setPrice(EmptyUtils.strEmpty(value.getPrice()));

                    mAdapter.notifyItemChanged(value.getPos());
                }
            }
        }
    };

    @Override
    public void initAdapter() {
        mAdapter = new WestCataEnaListAdapter(from);
    }

    @Override
    public void initData() {
        listType=getIntent().getStringExtra("listType");
        from=getIntent().getStringExtra("from");
        infos = getIntent().getParcelableArrayListExtra("infos");
        super.initData();

        MsgBus.sendScanCataRefresh().observe(this,mOnCataUpdateObserve);
        onRefresh(mRefreshLayout);
    }

    @Override
    public void getData() {
        super.getData();

        if (mAdapter!=null){
            mAdapter.setNewData(infos);
        }
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter,view,position);
        if ("1".equals(from)) {//进入修改药品页面
            UpdateWestEnaCataActivity.newIntance(ScanCataListActivtiy.this,((WestCataEnaListAdapter) adapter).getItem(position),position,1);
        }else {//返回该项数据
            Intent intent = new Intent();
            intent.putExtra("catalogueModels", ((WestCataEnaListAdapter) adapter).getItem(position));
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public static void newIntance(Context context,String listType,String from, ArrayList<CatalogueModel> infos) {
        Intent intent = new Intent(context, ScanCataListActivtiy.class);
        intent.putExtra("listType",listType);
        intent.putExtra("from",from);
        intent.putParcelableArrayListExtra("infos", infos);
        context.startActivity(intent);
    }

    public static void newIntance(Activity activity,String listType,String from, ArrayList<CatalogueModel> infos,int requestCode) {
        Intent intent = new Intent(activity, ScanCataListActivtiy.class);
        intent.putExtra("listType",listType);
        intent.putExtra("from",from);
        intent.putParcelableArrayListExtra("infos", infos);
        activity.startActivityForResult(intent,requestCode);
    }
}
