package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.DragStoreHouseModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodDetailModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.GoodDetailActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.OnlineBuyDrugActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OnlineBuyDrugModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarRefreshModel;
import com.jrdz.zhyb_android.utils.AppManager_GoodsDetailsAcivity;
import com.jrdz.zhyb_android.utils.InsuredLoginSmrzUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.widget.ShoppingCartAnimationView;
import com.jrdz.zhyb_android.widget.pop.ShopCarPop;
import com.zhy.http.okhttp.OkHttpUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-12-21
 * 描    述：扫码支付-商品详情
 * ================================================
 */
public class ScanPayGoodDetailActivity extends BaseActivity {
    private ImageView mIvPic;
    private ShapeTextView mStvOtc;
    private TextView mTvTitle,mTvPrice,mTvMainFunc;
    private TextView mTvUsage;
    private TextView mTvValidity;
    private TextView mTvStorage;
    private TextView mTvSpec;
    private TextView mTvDosage;
    private View mLineBottom;
    private LinearLayout mLlBottom;
    private LinearLayout mLlShopcar;
    private FrameLayout mFlShopcarIcon;
    private ShapeTextView mSvRedPoint;
    private TextView mTvAddShopcar;
    private ShapeTextView mTvApplyBuy;

    private String goodsNo;
    private GoodDetailModel.DataBean pagerData;
    ArrayList<GoodsModel.DataBean> hasJoinedShopCarDatas = new ArrayList<>();//记录加入购物车的数据
    private int requestTag = 0;
    private String totalRedNum = "0";//选择的商品总数量
    private String totalPrice = "0";//选择的商品总价
    private ShopCarPop shopCarPop;
    private CustomerDialogUtils cleanShopCarDialog;
    private ViewGroup mDecorView;//添加小红点的父view

    //订单详情操作监听
    private ObserverWrapper<ShopCarRefreshModel> mShopCarObserver=new ObserverWrapper<ShopCarRefreshModel>() {
        @Override
        public void onChanged(@Nullable ShopCarRefreshModel shopCarRefreshModel) {
            if (shopCarRefreshModel==null)return;
            switch (shopCarRefreshModel.getTag_value()){
                case "3":
                    if (!EmptyUtils.isEmpty(TAG)&&!TAG.equals(shopCarRefreshModel.getTag())){
                        requestTag=0;
                        showWaitDialog();
                        getPagerData();
                        getShopCarData();
                    }
                    break;
            }
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.activity_scanpay_gooddetail;
    }

    @Override
    public void initView() {
        super.initView();
        mIvPic = findViewById(R.id.iv_pic);
        mStvOtc = findViewById(R.id.stv_otc);
        mTvTitle= findViewById(R.id.tv_title);
        mTvPrice= findViewById(R.id.tv_price);
        mTvMainFunc = findViewById(R.id.tv_main_func);
        mTvUsage = findViewById(R.id.tv_usage);
        mTvValidity = findViewById(R.id.tv_validity);
        mTvStorage = findViewById(R.id.tv_storage);
        mTvSpec = findViewById(R.id.tv_spec);
        mTvDosage = findViewById(R.id.tv_dosage);
        mLineBottom = findViewById(R.id.line_bottom);
        mLlBottom = findViewById(R.id.ll_bottom);
        mLlShopcar = findViewById(R.id.ll_shopcar);
        mFlShopcarIcon = findViewById(R.id.fl_shopcar_icon);
        mSvRedPoint = findViewById(R.id.sv_red_point);
        mTvAddShopcar= findViewById(R.id.tv_add_shopcar);
        mTvApplyBuy = findViewById(R.id.tv_apply_buy);

        mDecorView = (ViewGroup) getWindow().getDecorView();
    }

    @Override
    public void initData() {
        goodsNo = getIntent().getStringExtra("goodsNo");
        MsgBus.sendShopCarRefresh().observe(this, mShopCarObserver);
        super.initData();

        showWaitDialog();
        getPagerData();
        getShopCarData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mLlShopcar.setOnClickListener(this);
        mTvAddShopcar.setOnClickListener(this);
        mTvApplyBuy.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.ll_shopcar://购物车弹框
                if (hasJoinedShopCarDatas.isEmpty()) {
                    showShortToast("请添加商品~");
                } else {
                    onShopcarPop();
                }
                break;
            case R.id.tv_add_shopcar://加入购物车
                onAddShopcar();
                break;
            case R.id.tv_apply_buy://提交
                applyBuy();
                break;
        }
    }

    private void getPagerData() {
        GoodDetailModel.sendGoodDetailRequest(TAG, goodsNo, new CustomerJsonCallBack<GoodDetailModel>() {
            @Override
            public void onRequestError(GoodDetailModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GoodDetailModel returnData) {
                dissWaitDailog();
                if (isFinishing()) return;
                GoodDetailModel.DataBean data = returnData.getData();
                if (data != null) {
                    setPagerData(data);
                }
            }
        });
    }

    //设置详情数据
    private void setPagerData(GoodDetailModel.DataBean data) {
        pagerData = data;
        if (EmptyUtils.isEmpty(data.getItemType()) || "0".equals(data.getItemType())) {
            mStvOtc.setVisibility(View.GONE);
        } else {
            mStvOtc.setVisibility(View.VISIBLE);
            mStvOtc.setText("1".equals(data.getItemType()) ? "OTC" : "处方药");
        }
        mTvTitle.setText(EmptyUtils.strEmptyToText(data.getGoodsName(), "--"));
        mTvPrice.setText("1".equals(data.getIsPromotion())?EmptyUtils.strEmpty(data.getPreferentialPrice()):EmptyUtils.strEmpty(data.getPrice()));
        mTvMainFunc.setText(EmptyUtils.strEmptyToText(data.getEfccAtd(), "--"));
        mTvUsage.setText(EmptyUtils.strEmptyToText(data.getUsualWay(), "--"));
        mTvValidity.setText(EmptyUtils.strEmptyToText(data.getExpiryDateCount() + "个月", "--"));
        mTvStorage.setText(EmptyUtils.strEmptyToText(data.getStorageConditions(), "--"));
        mTvSpec.setText(EmptyUtils.strEmptyToText(data.getSpec(), "--"));
        mTvDosage.setText(EmptyUtils.strEmptyToText(data.getRegDosform(), "--"));
    }

    //获取已加入购物车的数据
    private void getShopCarData() {
        // 2022-11-05 请求接口 获取已加入购物车数据
        ShopCarModel.sendShopCarRequest_mana(TAG, MechanismInfoUtils.getFixmedinsCode(), "0", "1", new CustomerJsonCallBack<ShopCarModel>() {
            @Override
            public void onRequestError(ShopCarModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(ShopCarModel returnData) {
                dissWaitDailog();
                hasJoinedShopCarDatas.clear();
                List<ShopCarModel.DataBean> datas = returnData.getData();
                if (datas != null && !datas.isEmpty() && datas.get(0).getShoppingCartGoods() != null) {
                    //已加入购物车的数据
                    hasJoinedShopCarDatas.addAll(datas.get(0).getShoppingCartGoods());
                }
                calculatePrice(null, null);
            }
        });
    }

    //购物车弹框
    private void onShopcarPop() {
        if (shopCarPop == null) {
            shopCarPop = new ShopCarPop(ScanPayGoodDetailActivity.this, hasJoinedShopCarDatas, totalRedNum, totalPrice);
        } else {
            shopCarPop.setData(hasJoinedShopCarDatas, totalRedNum, totalPrice);
        }

        shopCarPop.setOnListener(new ShopCarPop.IOptionListener() {
            @Override
            public void onCleanShopCar() {
                if (cleanShopCarDialog == null) {
                    cleanShopCarDialog = new CustomerDialogUtils();
                }
                cleanShopCarDialog.showDialog(ScanPayGoodDetailActivity.this, "提示", "确定清空购物车商品？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        onDelAllShopCar();
                    }
                });
            }

            @Override
            public void onItemClick(GoodsModel.DataBean itemData) {
                if (!pagerData.getGoodsNo().equals(itemData.getGoodsNo())) {
                    // 2022-11-05 跳转商品详情页面
                    ScanPayGoodDetailActivity.newIntance(ScanPayGoodDetailActivity.this, itemData.getGoodsNo());
                }
            }

            @Override
            public void onPopAdd(GoodsModel.DataBean itemData, TextView tvNum, TextView tvPrice) {
                onAddShopcar(itemData, tvNum, tvPrice, "add");
            }

            @Override
            public void onPopReduce(GoodsModel.DataBean itemData, TextView tvNum, TextView tvPrice) {
                onAddShopcar(itemData, tvNum, tvPrice, "reduce");
            }
        });
        shopCarPop.showPopupWindow(mLineBottom);
    }

    //清空购物车
    private void onDelAllShopCar() {
        showWaitDialog();
        BaseModel.sendDelAllShopCarRequest_mana(TAG, MechanismInfoUtils.getFixmedinsCode(), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("清空购物车成功");
                MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "3"));
                hasJoinedShopCarDatas.clear();
                pagerData.setShoppingCartNum(0);
                calculatePrice(null, null);
            }
        });
    }

    //加入购物车
    private void onAddShopcar() {
        if (pagerData == null) return;
        double num = new BigDecimal(String.valueOf(pagerData.getShoppingCartNum()+1)).subtract(new BigDecimal(pagerData.getInventoryQuantity())).doubleValue();
        if (num>0){
            showShortToast("该商品库存不足,请联系商家");
            return;
        }

        showWaitDialog();
        BaseModel.sendAddShopCarRequest_mana(TAG, pagerData.getFixmedins_code(), pagerData.getGoodsNo(), pagerData.getShoppingCartNum() + 1, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("加入购物车成功");
                MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "3"));
                GoodsModel.DataBean addGoodsData = new GoodsModel.DataBean(pagerData.getGoodsNo(), pagerData.getItemType(), pagerData.getEfccAtd(),
                        Integer.valueOf(EmptyUtils.strEmptyToText(pagerData.getInventoryQuantity(),"0")), pagerData.getPrice(), pagerData.getIsPlatformDrug(), pagerData.getFixmedins_code(),
                        pagerData.getGoodsLocation(), pagerData.getCatalogueId(), pagerData.getBannerAccessoryId1(), pagerData.getDrugsClassification(),
                        pagerData.getDrugsClassificationName(), pagerData.getIsPromotion(), pagerData.getGoodsName(),
                        pagerData.getPromotionDiscount(), pagerData.getPreferentialPrice(), pagerData.getIsOnSale(),
                        pagerData.getBannerAccessoryUrl1(), pagerData.getMonthlySales(), pagerData.getShoppingCartNum() + 1,
                        pagerData.getSpec(),pagerData.getAssociatedDiagnostics(),pagerData.getIsShowGoodsSold(),pagerData.getIsShowMargin());
                pagerData.setShoppingCartNum(addGoodsData.getShoppingCartNum());
                addCart(mTvAddShopcar, addGoodsData);//ParcelUtils.copy(productModel)
            }
        });
    }

    //加入购物车--购物车列表
    private void onAddShopcar(GoodsModel.DataBean itemData, TextView tvNum, TextView tvPrice, String tag) {
        showWaitDialog();
        BaseModel.sendAddShopCarRequest_mana(TAG, itemData.getFixmedins_code(), itemData.getGoodsNo(), itemData.getShoppingCartNum(),
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "3"));
                        switch (tag) {
                            case "add":
                                onAddCarList(itemData, tvNum, tvPrice);
                                break;
                            case "reduce":
                                onReduceCarList(itemData, "2", tvNum, tvPrice);
                                break;
                        }

                        if (pagerData.getGoodsNo().equals(itemData.getGoodsNo())) {
                            //若属于该商品详情 那么只需将该商品数据中 被加入购物车的数量进行增减
                            pagerData.setShoppingCartNum(itemData.getShoppingCartNum());
                        }
                    }
                });
    }

    //加入购物车的记录列表
    private void onAddCarList(GoodsModel.DataBean item, TextView tvNum, TextView tvPrice) {
        //判断当前添加的商品 是否商品数为1 为1说明是新添加的商品 若不是 则说明数量大于1 那么遍历列表 查询是同一个id的情况下 把数量设置进去
        if (item.getShoppingCartNum() == 1) {
            hasJoinedShopCarDatas.add(item);
        } else {
            for (GoodsModel.DataBean hasJoinedShopCarData : hasJoinedShopCarDatas) {
                if (hasJoinedShopCarData.getGoodsNo().equals(item.getGoodsNo())) {
                    hasJoinedShopCarData.setShoppingCartNum(item.getShoppingCartNum());
                    break;
                }
            }
        }

        Log.e("CarList", "Add_CarListsize=====" + hasJoinedShopCarDatas.size());
        // 2022-11-04 计算总价
        calculatePrice(tvNum, tvPrice);
    }

    //去除购物车的记录列表  from1:来自店铺详情页面列表 2:来自店铺详情 购物车弹框
    public void onReduceCarList(GoodsModel.DataBean item, String from, TextView tvNum, TextView tvPrice) {
        //判断当前添加的商品 是否商品数为0 为0说明是要删除的商品
        if ("1".equals(from)) {
            if (item.getShoppingCartNum() <= 0) {
                hasJoinedShopCarDatas.remove(item);
            } else {
                for (GoodsModel.DataBean hasJoinedShopCarData : hasJoinedShopCarDatas) {
                    if (hasJoinedShopCarData.getGoodsNo().equals(item.getGoodsNo())) {
                        hasJoinedShopCarData.setShoppingCartNum(item.getShoppingCartNum());
                        break;
                    }
                }
            }
        }

        Log.e("CarList", "Reduce_CarListsize=====" + hasJoinedShopCarDatas.size());
        // 2022-11-04 计算总价
        calculatePrice(tvNum, tvPrice);
    }

    //把商品添加到购物车的动画效果
    private void addCart(View view, GoodsModel.DataBean item) {
        ShoppingCartAnimationView shoppingCartAnimationView = new ShoppingCartAnimationView(this);
        int position[] = new int[2];
        view.getLocationInWindow(position);
        shoppingCartAnimationView.setStartPosition(new Point(position[0], position[1]));
        mDecorView.addView(shoppingCartAnimationView);
        int endPosition[] = new int[2];
        mFlShopcarIcon.getLocationInWindow(endPosition);
        shoppingCartAnimationView.setEndPosition(new Point(endPosition[0], endPosition[1]));
        shoppingCartAnimationView.startBeizerAnimation();

        //2022-11-04  加入购物车的记录列表
        onAddCarList(item, null, null);
    }

    //计算价格
    private void calculatePrice(TextView tvNum, TextView tvPrice) {
        totalRedNum = "0";
        totalPrice = "0";
        for (GoodsModel.DataBean rightLinkModel : hasJoinedShopCarDatas) {
            if ("1".equals(rightLinkModel.getIsPromotion())) {//有折扣
                totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(rightLinkModel.getPreferentialPrice())
                        .multiply(new BigDecimal(String.valueOf(rightLinkModel.getShoppingCartNum())))).toPlainString();

            } else {//没有折扣
                totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(rightLinkModel.getPrice())
                        .multiply(new BigDecimal(String.valueOf(rightLinkModel.getShoppingCartNum())))).toPlainString();
            }
            totalRedNum = new BigDecimal(totalRedNum).add(new BigDecimal(String.valueOf(rightLinkModel.getShoppingCartNum()))).toPlainString();
        }

        if ("0".equals(totalRedNum)) {
            mSvRedPoint.setVisibility(View.GONE);
        } else {
            mSvRedPoint.setVisibility(View.VISIBLE);
            mSvRedPoint.setText(totalRedNum);
        }

        if (tvNum != null) {
            tvNum.setText("(共" + totalRedNum + "件商品)");
        }
        if (tvPrice != null) {
            tvPrice.setText(totalPrice);
        }
    }

    //提交
    private void applyBuy() {
        if (shopCarPop!=null&&shopCarPop.isShowing()){//结算购物车的数据
            shopCarPop.dismiss();
            applyBuyMthod("1",totalPrice,totalRedNum,hasJoinedShopCarDatas);
        }else {//结算当前的商品
            if (pagerData==null) {
                showShortToast("数据有误，请重新进入页面");
                return;
            }

            int inventoryQuantity=new BigDecimal(EmptyUtils.strEmptyToText(pagerData.getInventoryQuantity(),"0")).intValue();
            if (inventoryQuantity<=0){
                showShortToast("库存不足，请联系商家");
                return;
            }

            ArrayList<GoodsModel.DataBean> dataBeans=new ArrayList<>();
            GoodsModel.DataBean addGoodsData = new GoodsModel.DataBean(pagerData.getGoodsNo(), pagerData.getItemType(), pagerData.getEfccAtd(),
                    inventoryQuantity, pagerData.getPrice(), pagerData.getIsPlatformDrug(), pagerData.getFixmedins_code(),
                    pagerData.getGoodsLocation(), pagerData.getCatalogueId(), pagerData.getBannerAccessoryId1(), pagerData.getDrugsClassification(),
                    pagerData.getDrugsClassificationName(), pagerData.getIsPromotion(), pagerData.getGoodsName(),
                    pagerData.getPromotionDiscount(), pagerData.getPreferentialPrice(), pagerData.getIsOnSale(),
                    pagerData.getBannerAccessoryUrl1(), pagerData.getMonthlySales(), 1,
                    pagerData.getSpec(),pagerData.getAssociatedDiagnostics(),pagerData.getIsShowGoodsSold(),pagerData.getIsShowMargin());
            dataBeans.add(addGoodsData);

            applyBuyMthod("2",mTvPrice.getText().toString(),"1",dataBeans);
        }
    }

    //提交订单  type 1代表提交购物车商品结算 2代码直接买单个商品
    private void applyBuyMthod(String type,String TotalAmount, String GoodsNum, ArrayList<GoodsModel.DataBean> goodBeans) {
        showWaitDialog();
        OnlineBuyDrugModel.sendScanOrderRequest(TAG, MechanismInfoUtils.getFixmedinsCode(), TotalAmount, GoodsNum,
                goodBeans, new CustomerJsonCallBack<OnlineBuyDrugModel>() {
                    @Override
                    public void onRequestError(OnlineBuyDrugModel returnData, String msg) {
                        hideWaitDialog();
                        showTipDialog(msg);
                    }

                    @Override
                    public void onRequestSuccess(OnlineBuyDrugModel returnData) {
                        hideWaitDialog();
                        showShortToast(returnData.getMsg());
                        if ("1".equals(type)){
                            MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "3"));
                        }

                        OnlineBuyDrugModel.DataBean data = returnData.getData();
                        if (data!=null&&!EmptyUtils.isEmpty(data.getOrderNo())){
                            Intent intent=new Intent(ScanPayGoodDetailActivity.this,ScanPayQRCodeActivity.class);
                            intent.putExtra("OrderNo", data.getOrderNo());
                            startActivityForResult(intent, new OnActivityCallback() {
                                @Override
                                public void onActivityResult(int resultCode, @Nullable Intent data) {
                                    if (resultCode==RESULT_OK){
                                        if ("1".equals(type)){
                                            showWaitDialog();
                                            requestTag=0;
                                            getShopCarData();
                                            getPagerData();
                                        }
                                    }
                                }
                            });
                        }else {
                            showShortToast("订单数据有误，请重新下单");
                        }
                    }
                });
    }

    //隐藏加载框
    public void dissWaitDailog() {
        requestTag++;
        if (requestTag >= 2) {
            hideWaitDialog();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (shopCarPop != null) {
            shopCarPop.onCleanListener();
            shopCarPop.onDestroy();
        }

        if (cleanShopCarDialog != null) {
            cleanShopCarDialog.onclean();
            cleanShopCarDialog = null;
        }
    }

    public static void newIntance(Context context, String goodsNo) {
        Intent intent = new Intent(context, ScanPayGoodDetailActivity.class);
        intent.putExtra("goodsNo", goodsNo);
        context.startActivity(intent);
    }
}
