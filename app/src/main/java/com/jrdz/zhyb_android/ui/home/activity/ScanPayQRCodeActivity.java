package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frame.compiler.utils.DecimalFormatUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.CustRefreshLayout;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.text.MoneyValueFilter;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.view.ShapeEditText;
import com.hjq.shape.view.ShapeTextView;
import com.huawei.hms.hmsscankit.ScanUtil;
import com.huawei.hms.hmsscankit.WriterException;
import com.huawei.hms.ml.scan.HmsBuildBitmapOption;
import com.huawei.hms.ml.scan.HmsScan;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.model.ScanPayStatusModel;
import com.jrdz.zhyb_android.ui.insured.model.UpdateOrderDiscountAmountModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.OrderDetailModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TagTextBean;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.widget.TagTextView;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.constant.RefreshState;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-12-19
 * 描    述：扫码支付-收款页面
 * ================================================
 */
public class ScanPayQRCodeActivity extends BaseActivity implements OnRefreshListener {
    protected CustRefreshLayout mRefreshLayout;
    private ImageView mIvQrcode;
    private TextView mTvMoney, mTvShopName;
    private LinearLayout mLlOrderListContain;
    private TextView mTvTotalNum;
    private TextView mTvTotalPrice;
    private ShapeEditText mEtDiscountAmount;
    private ShapeEditText mEtDiscount;
    private TextView mTvRealPrice;
    private ShapeTextView mTvSure;

    private String orderNo;
    private OrderDetailModel.DataBean detailData;
    List<TagTextBean> tags = new ArrayList<>();
    private Handler handler;
    private String totalPrice = "0";

    @Override
    public int getLayoutId() {
        return R.layout.activity_scanpay_qrcode;
    }

    @Override
    public void initView() {
        super.initView();
        mRefreshLayout = findViewById(R.id.refreshLayout);
        mIvQrcode = findViewById(R.id.iv_qrcode);
        mTvMoney = findViewById(R.id.tv_money);
        mTvShopName = findViewById(R.id.tv_shop_name);
        mLlOrderListContain = findViewById(R.id.ll_order_list_contain);
        mTvTotalNum = findViewById(R.id.tv_total_num);
        mTvTotalPrice = findViewById(R.id.tv_total_price);

        mEtDiscountAmount = findViewById(R.id.et_discount_amount);
        mEtDiscount = findViewById(R.id.et_discount);
        mTvRealPrice = findViewById(R.id.tv_real_price);
        mTvSure = findViewById(R.id.tv_sure);
        if (mTitleBar != null) {
            mTitleBar.getLeftView().setVisibility(View.GONE);
        } else {
            showShortToast("控件还未初始化!");
        }
    }

    @Override
    public void initData() {
        orderNo = getIntent().getStringExtra("OrderNo");
        super.initData();
        handler = new Handler(getMainLooper());
        //设置输入金额的限制
        mEtDiscountAmount.setFilters(new InputFilter[]{new MoneyValueFilter().setDigits(1)});
        //设置下拉刷新
        setRefreshInfo();
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setEnableLoadMore(false);

        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                int type = HmsScan.QRCODE_SCAN_TYPE;
                int width = getResources().getDimensionPixelSize(R.dimen.dp_350);
                int height = width;
                HmsBuildBitmapOption options = new HmsBuildBitmapOption.Creator().setBitmapBackgroundColor(Color.WHITE).setBitmapColor(Color.BLACK).setBitmapMargin(0).create();

                try {
                    // 如果未设置HmsBuildBitmapOption对象，生成二维码参数options置null。
                    Bitmap qrBitmap = ScanUtil.buildBitmap(orderNo, type, width, height, options);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mIvQrcode.setImageBitmap(qrBitmap);
                        }
                    });

                } catch (WriterException e) {
                    Log.w("buildBitmap", e);
                }
            }
        });
        setRightIcon(getResources().getDrawable(R.drawable.ic_white_close));

        showWaitDialog();
        getDetailData();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvSure.setOnClickListener(this);
    }

    //设置SmartRefreshLayout的刷新 加载样式
    public void setRefreshInfo() {
        mRefreshLayout.setPrimaryColorsId(R.color.transparent, R.color.white);
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(this));
        mRefreshLayout.setHeaderMaxDragRate(5);//最大显示下拉高度/Header标准高度  头部下拉高度的比例
    }

    //获取详情数据
    protected void getDetailData() {
        //2022-10-12 模拟请求数据
        OrderDetailModel.sendOrderDetailRequest(TAG, orderNo, new CustomerJsonCallBack<OrderDetailModel>() {
            @Override
            public void onRequestError(OrderDetailModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(OrderDetailModel returnData) {
                hideWaitDialog();
                detailData = returnData.getData();
                if (detailData != null) {
                    setDetailData();

                    pollingMethod();
                }
            }
        });
    }

    //设置页面数据
    private void setDetailData() {
        // 2022-10-31 判断是否有带处方药
        mLlOrderListContain.removeAllViews();
        totalPrice = "0";
        int totalNum = 0;
        List<OrderDetailModel.DataBean.OrderGoodsBean> orderGoods = detailData.getOrderGoods();
        for (int i = 0, size = orderGoods.size(); i < size; i++) {
            OrderDetailModel.DataBean.OrderGoodsBean orderGood = orderGoods.get(i);

            totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(orderGood.getPrice())
                    .multiply(new BigDecimal(String.valueOf(orderGood.getGoodsNum())))).toPlainString();
            totalNum += orderGood.getGoodsNum();

            //设置商品信息
            View view = LayoutInflater.from(this).inflate(R.layout.layout_orderlist_product_item, mLlOrderListContain, false);
            ImageView ivPic = view.findViewById(R.id.iv_pic);
            ShapeTextView stvOtc = view.findViewById(R.id.stv_otc);
            ShapeTextView stvEnterpriseTag = view.findViewById(R.id.stv_enterprise_tag);
            TagTextView tvTitle = view.findViewById(R.id.tv_title);
            TextView tvEstimatePrice = view.findViewById(R.id.tv_estimate_price);
            TextView tvRealPrice = view.findViewById(R.id.tv_real_price);
            View lineItem = view.findViewById(R.id.line_item);

            GlideUtils.loadImg(orderGood.getBannerAccessoryUrl1(), ivPic, R.drawable.ic_good_placeholder,
                    new RoundedCornersTransformation(getResources().getDimensionPixelSize(R.dimen.dp_16), 0));
            //判断是否是处方药
            if ("1".equals(orderGood.getItemType())) {
                stvOtc.setText("OTC");
                stvOtc.setVisibility(View.VISIBLE);
            } else if ("2".equals(orderGood.getItemType())) {
                stvOtc.setText("处方药");
                stvOtc.setVisibility(View.VISIBLE);
            } else {
                stvOtc.setVisibility(View.GONE);
            }
            //判断是否是平台药 若是 需要展示医保
            tags.clear();
            if ("1".equals(orderGood.getIsPlatformDrug())) {//是平台药
                tags.add(new TagTextBean("医保", R.color.color_ee8734));
                tvTitle.setContentAndTag(EmptyUtils.strEmpty(orderGood.getGoodsName()), tags);
            } else {
                tvTitle.setContentAndTag(EmptyUtils.strEmpty(orderGood.getGoodsName()), tags);
            }

            //判断1.是否支持企业基金支付
            if ("1".equals(orderGood.getIsEnterpriseFundPay())) {
                stvEnterpriseTag.setVisibility(View.VISIBLE);
            } else {
                stvEnterpriseTag.setVisibility(View.GONE);
            }

            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(orderGood.getPrice())));
            tvRealPrice.setText("x" + orderGood.getGoodsNum());
            //2022-10-10 最后一个item的分割线需要隐藏
            if (i == size - 1) {
                lineItem.setVisibility(View.GONE);
            } else {
                lineItem.setVisibility(View.VISIBLE);
            }
            mLlOrderListContain.addView(view);
        }

        mTvShopName.setText(EmptyUtils.strEmpty(detailData.getStoreName()));
        mTvTotalNum.setText("共" + totalNum + "件商品");
        mTvTotalPrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(totalPrice)));
        mTvMoney.setText("¥" + EmptyUtils.strEmpty(DecimalFormatUtils.noZero(detailData.getTotalAmount())));

        mEtDiscountAmount.setText(DecimalFormatUtils.noZero(EmptyUtils.strEmpty(detailData.getDiscountAmount())));
        mEtDiscount.setText(DecimalFormatUtils.noZero(EmptyUtils.strEmpty(detailData.getDiscount())));
        mTvRealPrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(detailData.getTotalAmount())));
    }

    //轮询
    private void pollingMethod() {
        if (handler == null) return;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getPayStatus();
            }
        }, 3000);
    }

    //轮询-获取支付状态
    private void getPayStatus() {
        ScanPayStatusModel.sendQueryOrderStatusRequest(TAG, orderNo, new CustomerJsonCallBack<ScanPayStatusModel>() {
            @Override
            public void onRequestError(ScanPayStatusModel returnData, String msg) {
                hideRefreshView();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(ScanPayStatusModel returnData) {
                hideRefreshView();
                ScanPayStatusModel.DataBean data = returnData.getData();
                if (data != null) {
                    if ("1".equals(data.getOrderStatus())) {//待支付
                        pollingMethod();
                    } else if ("4".equals(data.getOrderStatus())) {//支付成功
                        ScanPayResultActivity.newIntance(ScanPayQRCodeActivity.this, data.getOrderStatus(), detailData.getTotalAmount());
                        goFinish();
                    } else if ("9".equals(data.getOrderStatus())) {//支付失败
                        ScanPayResultActivity.newIntance(ScanPayQRCodeActivity.this, data.getOrderStatus(), returnData.getMsg());
                        goFinish();
                    }
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_sure://确认
                sure();
                break;
        }
    }

    @Override
    public void rightTitleViewClick() {
        goFinish();
    }

    //确认添加优惠
    private void sure() {
        double discount = new BigDecimal(EmptyUtils.strEmptyToText(mEtDiscount.getText().toString(),"100")).doubleValue();
        if (discount==0||discount>100){
            showShortToast("优惠折扣请输入1-100的数字");
            return;
        }

        showWaitDialog();
        UpdateOrderDiscountAmountModel.sendUpdateOrderDiscountAmountRequest(TAG, orderNo, EmptyUtils.strEmptyToText(mEtDiscountAmount.getText().toString(),"0"),
                EmptyUtils.strEmptyToText(mEtDiscount.getText().toString(), "100"), new CustomerJsonCallBack<UpdateOrderDiscountAmountModel>() {
                    @Override
                    public void onRequestError(UpdateOrderDiscountAmountModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(UpdateOrderDiscountAmountModel returnData) {
                        hideWaitDialog();
                        showShortToast(returnData.getMsg());
                        UpdateOrderDiscountAmountModel.DataBean data = returnData.getData();
                        if (data != null) {
                            mTvMoney.setText("¥" + EmptyUtils.strEmpty(DecimalFormatUtils.noZero(data.getTotalAmount())));
                            mTvRealPrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(data.getTotalAmount())));
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        super.onBackPressed();
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        getPayStatus();
    }

    //关闭刷新的view
    public void hideRefreshView() {
        if (mRefreshLayout != null && mRefreshLayout.getState() == RefreshState.Refreshing) {
            mRefreshLayout.finishRefresh();
        } else {
            hideWaitDialog();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
    }
}
