package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.AddGoodsDraftsActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.AddGoodsSuccessActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.SpecialAreaAddGoodsActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.TransactionDetailActivity_mana;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-12-20
 * 描    述：扫码支付结果页
 * ================================================
 */
public class ScanPayResultActivity extends BaseActivity {
    private ImageView mIvResult;
    private TextView mTvResult;
    private TextView mTvDescribe01;
    private ShapeTextView mTvContinueAddGoods;

    private String tag;
    private String msg;

    @Override
    public int getLayoutId() {
        return R.layout.activity_scanpay_result;
    }

    @Override
    public void initView() {
        super.initView();
        mIvResult = findViewById(R.id.iv_result);
        mTvResult = findViewById(R.id.tv_result);
        mTvDescribe01 = findViewById(R.id.tv_describe01);
        mTvContinueAddGoods = findViewById(R.id.tv_continue_add_goods);
    }

    @Override
    public void initData() {
        tag=getIntent().getStringExtra("tag");
        msg=getIntent().getStringExtra("msg");
        super.initData();
        if ("4".equals(tag)){
            mIvResult.setImageResource(R.drawable.ic_settlein_apply_success);
            mTvResult.setText("支付成功");
            mTvDescribe01.setTextColor(getResources().getColor(R.color.color_333333));
            mTvDescribe01.setText("¥"+msg);
        } else if ("9".equals(tag)){
            mIvResult.setImageResource(R.drawable.ic_settlein_apply_fail);
            mTvResult.setText("支付失败");
            mTvDescribe01.setTextColor(getResources().getColor(R.color.color_f1a040));
            mTvDescribe01.setText(msg);
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvContinueAddGoods.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_continue_add_goods://明细查询
                goTransactionDetail();
                break;
        }
    }

    //跳转明细查询
    protected void goTransactionDetail() {
        TransactionDetailActivity_mana.newIntance(ScanPayResultActivity.this);
        goFinish();
    }

    public static void newIntance(Context context, String tag, String msg) {
        Intent intent = new Intent(context, ScanPayResultActivity.class);
        intent.putExtra("tag", tag);
        intent.putExtra("msg", msg);
        context.startActivity(intent);
    }
}
