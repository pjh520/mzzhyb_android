package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.CostomLoadMoreView;
import com.frame.compiler.widget.CustRefreshLayout;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.hjq.shape.view.ShapeView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.adapter.ShopProductRightLinkAdapter;
import com.jrdz.zhyb_android.ui.mine.activity.HwScanActivity;
import com.jrdz.zhyb_android.ui.mine.activity.ZbarScanActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.LeftLinkAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OnlineBuyDrugModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarRefreshModel;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.utils.ParcelUtils;
import com.jrdz.zhyb_android.widget.ShoppingCartAnimationView;
import com.jrdz.zhyb_android.widget.pop.ScanPaySelectDrugPop;
import com.jrdz.zhyb_android.widget.pop.ShopCarPop;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.constant.RefreshState;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-12-19
 * 描    述：扫码支付-分类页面
 * ================================================
 */
public class ScanPaySelectDrugActivity extends BaseActivity implements OnRefreshListener, BaseQuickAdapter.RequestLoadMoreListener {
    private LinearLayout mLlTitle;
    private FrameLayout mFlClose,mFlScan;
    private ShapeLinearLayout mSllSearch;
    private EditText mEtSearch;
    private ShapeTextView mStvSearch;
    private View mLine;
    private RecyclerView mRlLeftList;
    private TextView mTvTitle;
    private LinearLayout mLlAll;
    private TextView mTvAll;
    private ShapeView mSvAll;
    private LinearLayout mLlSales;
    private TextView mTvSales;
    private ShapeView mSvSales;
    private LinearLayout mLlPrice;
    private TextView mTvPrice;
    private ShapeView mSvPrice;
    private CustRefreshLayout mRefreshLayout;
    private RecyclerView mRlRightList;
    private View mLineBottom;
    private LinearLayout mLlShopcar;
    private FrameLayout mFlShopcarIcon;
    private ShapeTextView mSvRedPoint;
    private TextView mTvTotalPrice;
    private ShapeTextView mTvApplyBuy;

    private ViewGroup mDecorView;
    private LeftLinkAdapter leftLinkAdapter;
    private ShopProductRightLinkAdapter rightLinkAdapter;
    private int currentSelectPos = 0;
    private int mPageNum = 0;
    private String catalogueId = "", catalogueName = "";
    private String BySales = "0", ByPrice = "0";
    ArrayList<GoodsModel.DataBean> hasJoinedShopCarDatas = new ArrayList<>();//记录加入购物车的数据
    private ShopCarPop shopCarPop;
    private String totalRedNum = "0";//选择的商品总数量
    private String totalPrice = "0";//选择的商品总价
    private CustomerDialogUtils cleanShopCarDialog;
    private int textColorPre;
    private int textColorNor;
    private int requestTag = 0;
    private ScanPaySelectDrugPop scanPaySelectDrugPop;

    //订单详情操作监听
    private ObserverWrapper<ShopCarRefreshModel> mShopCarObserver=new ObserverWrapper<ShopCarRefreshModel>() {
        @Override
        public void onChanged(@Nullable ShopCarRefreshModel shopCarRefreshModel) {
            if (shopCarRefreshModel==null)return;
            switch (shopCarRefreshModel.getTag_value()){
                case "3":
                    if (!EmptyUtils.isEmpty(TAG)&&!TAG.equals(shopCarRefreshModel.getTag())){
                        requestTag=0;
                        showWaitDialog();
                        onRefresh(mRefreshLayout);
                        getShopCarData();
                    }
                    break;
            }
        }
    };


    @Override
    public int getLayoutId() {
        return R.layout.activity_scanpay_selectdrug;
    }

    @Override
    public void initView() {
        super.initView();
        mLlTitle = findViewById(R.id.ll_title);
        mFlClose = findViewById(R.id.fl_close);
        mFlScan= findViewById(R.id.fl_scan);
        mSllSearch = findViewById(R.id.sll_search);
        mEtSearch = findViewById(R.id.et_search);
        mStvSearch = findViewById(R.id.stv_search);
        mLine = findViewById(R.id.line);
        mRlLeftList = findViewById(R.id.rl_left_list);
        mTvTitle = findViewById(R.id.tv_title);
        mLlAll = findViewById(R.id.ll_all);
        mTvAll = findViewById(R.id.tv_all);
        mSvAll = findViewById(R.id.sv_all);
        mLlSales = findViewById(R.id.ll_sales);
        mTvSales = findViewById(R.id.tv_sales);
        mSvSales = findViewById(R.id.sv_sales);
        mLlPrice = findViewById(R.id.ll_price);
        mTvPrice = findViewById(R.id.tv_price);
        mSvPrice = findViewById(R.id.sv_price);
        mRefreshLayout = findViewById(R.id.refreshLayout);
        mRlRightList = findViewById(R.id.rl_right_list);

        mLineBottom = findViewById(R.id.line_bottom);
        mLlShopcar = findViewById(R.id.ll_shopcar);
        mFlShopcarIcon = findViewById(R.id.fl_shopcar_icon);
        mSvRedPoint = findViewById(R.id.sv_red_point);
        mTvTotalPrice = findViewById(R.id.tv_total_price);
        mTvApplyBuy = findViewById(R.id.tv_apply_buy);

        mDecorView = (ViewGroup) getWindow().getDecorView();
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mLlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        MsgBus.sendShopCarRefresh().observe(this, mShopCarObserver);
        super.initData();
        textColorPre = getResources().getColor(R.color.color_4870e0);
        textColorNor = getResources().getColor(R.color.color_333333);
        setRefreshInfo();

        initLeftView();
        initRightView();
        showWaitDialog();
        getLeftData();
        getShopCarData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        //=========================左边列表===================================================
        //分类列表item点击事件
        leftLinkAdapter.setOnItemClickListener(new OnLeftItemClickListener());
        //=========================左边列表===================================================
        mFlClose.setOnClickListener(this);
        mFlScan.setOnClickListener(this);
        mStvSearch.setOnClickListener(this);
        mLlAll.setOnClickListener(this);
        mLlSales.setOnClickListener(this);
        mLlPrice.setOnClickListener(this);
        mLlShopcar.setOnClickListener(this);
        mTvApplyBuy.setOnClickListener(this);
    }

    //设置SmartRefreshLayout的刷新 加载样式
    protected void setRefreshInfo() {
        mRefreshLayout.setEnableRefresh(true);
        mRefreshLayout.setEnableLoadMore(false);
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setPrimaryColorsId(R.color.bar_transparent, R.color.txt_color_666);
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(this));
        mRefreshLayout.setHeaderMaxDragRate(5);//最大显示下拉高度/Header标准高度  头部下拉高度的比例

        mRefreshLayout.setDisableContentWhenRefresh(true);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close://关闭
                goFinish();
                break;
            case R.id.fl_scan://扫码添加购物车
                if (Constants.Configure.IS_POS) {
                    goPosScan();
                } else {
                    goHwScan();
                }
                break;
            case R.id.stv_search://搜索
                search();
                break;
            case R.id.ll_all://全部
                setViewScreen(0);
                BySales="0";
                ByPrice="0";
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.ll_sales://销量
                setViewScreen(1);
                BySales="1";
                ByPrice="0";
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.ll_price://价格
                setViewScreen(2);
                BySales="0";
                ByPrice="2";
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.ll_shopcar://购物车弹框
                if (hasJoinedShopCarDatas.isEmpty()) {
                    showShortToast("请添加商品~");
                } else {
                    onShopcarPop();
                }
                break;
            case R.id.tv_apply_buy://立即购买
                applyBuy();
                break;
        }
    }

    //使用pos机自带的扫码
    private void goPosScan() {
        startActivityForResult(new Intent(ScanPaySelectDrugActivity.this, ZbarScanActivity.class), new BaseActivity.OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == -1) {
                    String scanResult = data.getStringExtra("scanResult");
                    scanOrgan(scanResult);
                }
            }
        });
    }

    //手机端使用华为扫码
    private void goHwScan() {
        startActivityForResult(new Intent(ScanPaySelectDrugActivity.this, HwScanActivity.class), new BaseActivity.OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == -1) {
                    String scanResult = data.getStringExtra("scanResult");
                    scanOrgan(scanResult);
                }
            }
        });
    }

    //将条形码传给后台 获取到商品信息之后
    private void scanOrgan(String scanResult) {
        showSearchPop("",scanResult);
    }

    //设置全部 销量 价格 点击之后 的样式
    private void setViewScreen(int tag) {
        switch (tag) {
            case 0:
                mTvAll.setTextColor(textColorPre);
                mSvAll.setVisibility(View.VISIBLE);
                mTvSales.setTextColor(textColorNor);
                mSvSales.setVisibility(View.INVISIBLE);
                mTvPrice.setTextColor(textColorNor);
                mSvPrice.setVisibility(View.INVISIBLE);
                break;
            case 1:
                mTvAll.setTextColor(textColorNor);
                mSvAll.setVisibility(View.INVISIBLE);
                mTvSales.setTextColor(textColorPre);
                mSvSales.setVisibility(View.VISIBLE);
                mTvPrice.setTextColor(textColorNor);
                mSvPrice.setVisibility(View.INVISIBLE);
                break;
            case 2:
                mTvAll.setTextColor(textColorNor);
                mSvAll.setVisibility(View.INVISIBLE);
                mTvSales.setTextColor(textColorNor);
                mSvSales.setVisibility(View.INVISIBLE);
                mTvPrice.setTextColor(textColorPre);
                mSvPrice.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void search() {
        if (EmptyUtils.isEmpty(mEtSearch.getText().toString())) {
            showShortToast("请输入目录名称/拼音简码");
            return;
        }
        showSearchPop(mEtSearch.getText().toString(),"");
    }

    //显示搜索弹框
    private void showSearchPop(String mSearchText,String mBarCode) {
        KeyboardUtils.hideSoftInput(this);
        if (scanPaySelectDrugPop == null) {
            scanPaySelectDrugPop = new ScanPaySelectDrugPop(ScanPaySelectDrugActivity.this, mSearchText, mBarCode);
        } else {
            scanPaySelectDrugPop.setData(mSearchText, mBarCode);
        }

        scanPaySelectDrugPop.setOnListener(new ScanPaySelectDrugPop.IOptionListener() {

            @Override
            public void onShowWait() {
                showWaitDialog();
            }

            @Override
            public void onHideWait() {
                hideWaitDialog();
            }

            @Override
            public void onSearchResultAddShopcar(GoodsModel.DataBean itemData,ViewGroup viewGroup,RecyclerView recyclerView, BaseQuickAdapter baseQuickAdapter, View view, int pos, String tag) {
                onAddShopcar(itemData,viewGroup,recyclerView, baseQuickAdapter, view, pos, tag);
            }
        });
        scanPaySelectDrugPop.showPopupWindow(mLineBottom);
    }

    //获取已加入购物车的数据
    private void getShopCarData() {
        // 2022-11-05 请求接口 获取已加入购物车数据
        ShopCarModel.sendShopCarRequest_mana(TAG,MechanismInfoUtils.getFixmedinsCode(), "0", "1", new CustomerJsonCallBack<ShopCarModel>() {
            @Override
            public void onRequestError(ShopCarModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(ShopCarModel returnData) {
                dissWaitDailog();
                hasJoinedShopCarDatas.clear();
                List<ShopCarModel.DataBean> datas = returnData.getData();
                if (datas != null && !datas.isEmpty() && datas.get(0).getShoppingCartGoods() != null) {
                    //已加入购物车的数据
                    hasJoinedShopCarDatas.addAll(datas.get(0).getShoppingCartGoods());
                }
                calculatePrice(null, null);
            }
        });
    }

    //提交订单
    private void applyBuy() {
        if (hasJoinedShopCarDatas.isEmpty()) {
            showShortToast("请选择商品");
            return;
        }
        if (shopCarPop != null && shopCarPop.isShowing()) {//结算购物车的数据
            shopCarPop.dismiss();
        }

        showWaitDialog();
        OnlineBuyDrugModel.sendScanOrderRequest(TAG, MechanismInfoUtils.getFixmedinsCode(), totalPrice, totalRedNum,
                hasJoinedShopCarDatas, new CustomerJsonCallBack<OnlineBuyDrugModel>() {
                    @Override
                    public void onRequestError(OnlineBuyDrugModel returnData, String msg) {
                        hideWaitDialog();
                        showTipDialog(msg);
                    }

                    @Override
                    public void onRequestSuccess(OnlineBuyDrugModel returnData) {
                        hideWaitDialog();
                        showShortToast(returnData.getMsg());
                        OnlineBuyDrugModel.DataBean data = returnData.getData();
                        if (data!=null&&!EmptyUtils.isEmpty(data.getOrderNo())){
                            Intent intent=new Intent(ScanPaySelectDrugActivity.this,ScanPayQRCodeActivity.class);
                            intent.putExtra("OrderNo", data.getOrderNo());
                            startActivityForResult(intent, new OnActivityCallback() {
                                @Override
                                public void onActivityResult(int resultCode, @Nullable Intent data) {
                                    if (resultCode==RESULT_OK){
                                        showWaitDialog();
                                        requestTag=0;
                                        getShopCarData();
                                        onRefresh(mRefreshLayout);
                                    }
                                }
                            });
                        }else {
                            showShortToast("订单数据有误，请重新下单");
                        }
                    }
                });
    }

    //=========================左边列表===================================================
    //初始化左边分类view
    private void initLeftView() {
        mRlLeftList.setHasFixedSize(true);
        mRlLeftList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        leftLinkAdapter = new LeftLinkAdapter();
        mRlLeftList.setAdapter(leftLinkAdapter);
    }

    //初始化左边分类数据
    private void getLeftData() {
        PhaSortModel.sendSortRequest(TAG, "1", new CustomerJsonCallBack<PhaSortModel>() {
            @Override
            public void onRequestError(PhaSortModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(PhaSortModel returnData) {
                List<PhaSortModel.DataBean> datas = returnData.getData();

                if (leftLinkAdapter != null && datas != null) {
                    leftLinkAdapter.setNewData(datas);
                    //设置数据已经请求的id
                    for (int i = 0; i < datas.size(); i++) {
                        PhaSortModel.DataBean data = datas.get(i);
                        if (i == 0) {
                            data.setChoose("1");
                            catalogueId = data.getCatalogueId();
                            catalogueName = data.getCatalogueName();
                            if (mTvTitle != null) {
                                mTvTitle.setText(data.getCatalogueName());
                            }
                        } else if (i == 1) {
                            data.setChoose("bottom01");
                        } else {
                            data.setChoose("0");
                        }
                    }

                    onRefresh(mRefreshLayout);
                } else {
                    dissWaitDailog();
                    showShortToast("数据有误，请重新进入页面");
                }
            }
        });
    }

    //左边列表item点击事件
    private class OnLeftItemClickListener implements BaseQuickAdapter.OnItemClickListener {

        @Override
        public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
            LeftLinkAdapter leftLinkAdapter = ((LeftLinkAdapter) baseQuickAdapter);
            PhaSortModel.DataBean leftLinkModel = leftLinkAdapter.getItem(i);

            if (!"1".equals(leftLinkModel.getChoose())) {//只响应未被选中的item
                moveToMiddle(mRlLeftList, i);

                leftLinkAdapter.setSelectedPosttion(baseQuickAdapter.getItemCount(), currentSelectPos, i);
                currentSelectPos = i;
                //设置数据已经请求的id
                catalogueId = leftLinkModel.getCatalogueId();
                catalogueName = leftLinkModel.getCatalogueName();
                mTvTitle.setText(leftLinkModel.getCatalogueName());

                showWaitDialog();
                onRefresh(mRefreshLayout);
            }
        }
    }

    //将当前选中的item居中
    public void moveToMiddle(RecyclerView recyclerView, int position) {
        //先从RecyclerView的LayoutManager中获取当前第一项和最后一项的Position
        int firstItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
        int lastItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
        //中间位置
        int middle = (firstItem + lastItem) / 2;
        // 取绝对值，index下标是当前的位置和中间位置的差，下标为index的view的top就是需要滑动的距离
        int index = (position - middle) >= 0 ? position - middle : -(position - middle);
        //左侧列表一共有getChildCount个Item，如果>这个值会返回null，程序崩溃，如果>getChildCount直接滑到指定位置,或者,都一样啦
        if (index >= recyclerView.getChildCount()) {
            recyclerView.scrollToPosition(position);
        } else {
            //如果当前位置在中间位置上面，往下移动，这里为了防止越界
            if (position < middle) {
                recyclerView.scrollBy(0, -recyclerView.getChildAt(index).getTop());
                // 在中间位置的下面，往上移动
            } else {
                recyclerView.scrollBy(0, recyclerView.getChildAt(index).getTop());
            }
        }
    }
    //=========================左边列表===================================================

    //=========================右边列表===================================================
    public void initRightView() {
        mRlRightList.setHasFixedSize(true);
        mRlRightList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rightLinkAdapter = new ShopProductRightLinkAdapter();
        mRlRightList.setAdapter(rightLinkAdapter);
        //设置开启上拉加载更多
        rightLinkAdapter.setEnableLoadMore(true);
        //设置上拉加载更多监听
        rightLinkAdapter.setOnLoadMoreListener(this, mRlRightList);
        rightLinkAdapter.setLoadMoreView(new CostomLoadMoreView());
        rightLinkAdapter.setEmptyView(R.layout.layout_empty_view, mRlRightList);

        rightLinkAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_1000)) {
                    return;
                }
                GoodsModel.DataBean itemData = rightLinkAdapter.getItem(i);
                switch (view.getId()) {
                    case R.id.fl_num_add://数量加
                        int num = itemData.getShoppingCartNum();
                        if (num + 1 <= itemData.getInventoryQuantity()) {
                            onAddShopcar(itemData, baseQuickAdapter, view, i, "add");
                        } else {
                            showShortToast("该商品库存不足,请联系商家");
                        }
                        break;
                    case R.id.fl_num_reduce://数量减
                        onAddShopcar(itemData, baseQuickAdapter, view, i, "reduce");
                        break;
                }

            }
        });

        rightLinkAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_1000)) {
                    return;
                }
                GoodsModel.DataBean itemData = rightLinkAdapter.getItem(i);
                ScanPayGoodDetailActivity.newIntance(ScanPaySelectDrugActivity.this, itemData.getGoodsNo());
            }
        });
    }

    //获取右边列表数据
    private void getRightData() {
        GoodsModel.sendSortGoodsListRequest_mana(TAG, String.valueOf(mPageNum), "20", "",
                catalogueId, "", MechanismInfoUtils.getFixmedinsCode(), BySales, ByPrice,"", new CustomerJsonCallBack<GoodsModel>() {
                    @Override
                    public void onRequestError(GoodsModel returnData, String msg) {
                        dissWaitDailog();
                        setLoadMoreFail();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(GoodsModel returnData) {
                        dissWaitDailog();
                        List<GoodsModel.DataBean> datas = returnData.getData();
                        if (rightLinkAdapter != null && datas != null) {
                            //获取数据成功
                            if (mPageNum == 0) {
                                rightLinkAdapter.setNewData(datas);
                                if (mRlRightList != null) {
                                    mRlRightList.scrollToPosition(0);
                                }
                            } else {
                                rightLinkAdapter.addData(datas);
                                rightLinkAdapter.loadMoreComplete();
                            }

                            if (datas.isEmpty()) {
                                if (rightLinkAdapter.getData().size() < 8) {
                                    rightLinkAdapter.loadMoreEnd(true);
                                } else {
                                    rightLinkAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        mPageNum = 0;
        getRightData();
    }

    @Override
    public void onLoadMoreRequested() {
        mPageNum++;
        getRightData();
    }

    protected void setLoadMoreFail() {
        if (rightLinkAdapter != null) {
            rightLinkAdapter.loadMoreFail();
        }

        if (mPageNum > 1) {
            mPageNum -= 1;
        }
    }

    //购物车弹框
    private void onShopcarPop() {
        if (shopCarPop == null) {
            shopCarPop = new ShopCarPop(ScanPaySelectDrugActivity.this, hasJoinedShopCarDatas, totalRedNum, totalPrice);
        } else {
            shopCarPop.setData(hasJoinedShopCarDatas, totalRedNum, totalPrice);
        }

        shopCarPop.setOnListener(new ShopCarPop.IOptionListener() {
            @Override
            public void onCleanShopCar() {
                if (cleanShopCarDialog == null) {
                    cleanShopCarDialog = new CustomerDialogUtils();
                }
                cleanShopCarDialog.showDialog(ScanPaySelectDrugActivity.this, "提示", "确定清空购物车商品？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        onDelAllShopCar();
                    }
                });
            }

            @Override
            public void onItemClick(GoodsModel.DataBean itemData) {
                // 2022-11-05 跳转商品详情页面
                ScanPayGoodDetailActivity.newIntance(ScanPaySelectDrugActivity.this, itemData.getGoodsNo());
            }

            @Override
            public void onPopAdd(GoodsModel.DataBean itemData, TextView tvNum, TextView tvPrice) {
                onAddShopcar(itemData, tvNum, tvPrice, "add");
            }

            @Override
            public void onPopReduce(GoodsModel.DataBean itemData, TextView tvNum, TextView tvPrice) {
                onAddShopcar(itemData, tvNum, tvPrice, "reduce");
            }
        });
        shopCarPop.showPopupWindow(mLineBottom);
    }

    //清空购物车
    private void onDelAllShopCar() {
        showWaitDialog();
        BaseModel.sendDelAllShopCarRequest_mana(TAG, MechanismInfoUtils.getFixmedinsCode(), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("清空购物车成功");
                hasJoinedShopCarDatas.clear();
                // 2022-11-29 刷新全部商品 当前页的数据
                onCleanShopCar();
                calculatePrice(null, null);
            }
        });
    }

    //加入购物车--购物车列表
    private void onAddShopcar(GoodsModel.DataBean itemData, TextView tvNum, TextView tvPrice, String tag) {
        showWaitDialog();
        BaseModel.sendAddShopCarRequest_mana(TAG, itemData.getFixmedins_code(), itemData.getGoodsNo(), itemData.getShoppingCartNum(),
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        switch (tag) {
                            case "add":
                                onAddCarList(itemData, tvNum, tvPrice);
                                onRefreshRightItem(itemData);
                                break;
                            case "reduce":
                                onReduceCarList(itemData, "2", tvNum, tvPrice);
                                onRefreshRightItem(itemData);
                                break;
                        }
                    }
                });
    }

    //加入购物车--右侧商品列表
    private void onAddShopcar(GoodsModel.DataBean itemData, BaseQuickAdapter baseQuickAdapter, View view, int pos, String tag) {
        showWaitDialog();
        BaseModel.sendAddShopCarRequest_mana(TAG, itemData.getFixmedins_code(), itemData.getGoodsNo(), "add".equals(tag) ? itemData.getShoppingCartNum() + 1 : itemData.getShoppingCartNum() - 1,
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        switch (tag) {
                            case "add":
                                TextView tvSelectNum01 = (TextView) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.tv_select_num);

                                if (itemData.getShoppingCartNum() == 0) {
                                    FrameLayout flNumReduce01 = (FrameLayout) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.fl_num_reduce);
                                    ShapeTextView stv12 = (ShapeTextView) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.stv_02);
                                    TextView tvRealPrice = (TextView) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.tv_real_price);

                                    flNumReduce01.setVisibility(View.VISIBLE);
                                    tvSelectNum01.setVisibility(View.VISIBLE);
                                    stv12.setVisibility(View.GONE);
                                    tvRealPrice.setVisibility(View.GONE);

                                    itemData.setShoppingCartNum(1);
                                    tvSelectNum01.setText("1");
                                } else {
                                    itemData.setShoppingCartNum(itemData.getShoppingCartNum() + 1);
                                    tvSelectNum01.setText(String.valueOf(itemData.getShoppingCartNum()));
                                }

                                addCart(view, ParcelUtils.copy(itemData));
                                break;
                            case "reduce":
                                TextView tvSelectNum02 = (TextView) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.tv_select_num);

                                if (itemData.getShoppingCartNum() == 1) {
                                    FrameLayout flNumReduce02 = (FrameLayout) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.fl_num_reduce);
                                    ShapeTextView stv22 = (ShapeTextView) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.stv_02);
                                    TextView tvRealPrice = (TextView) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.tv_real_price);

                                    flNumReduce02.setVisibility(View.GONE);
                                    tvSelectNum02.setVisibility(View.GONE);
                                    double promotionDiscountVlaue = new BigDecimal(itemData.getPromotionDiscount()).doubleValue();
                                    if ("1".equals(itemData.getIsPromotion())&&promotionDiscountVlaue<10){
                                        stv22.setVisibility(View.VISIBLE);
                                        tvRealPrice.setVisibility(View.VISIBLE);
                                    }

                                    itemData.setShoppingCartNum(0);
                                    tvSelectNum02.setText("0");
                                } else {
                                    itemData.setShoppingCartNum(itemData.getShoppingCartNum() - 1);
                                    tvSelectNum02.setText(String.valueOf(itemData.getShoppingCartNum()));
                                }

                                onReduceCarList(ParcelUtils.copy(itemData), "1", null, null);
                                break;
                        }
                    }
                });
    }

    //加入购物车--搜索商品列表
    private void onAddShopcar(GoodsModel.DataBean itemData,ViewGroup viewGroup,RecyclerView recyclerView, BaseQuickAdapter baseQuickAdapter, View view, int pos, String tag) {
        showWaitDialog();
        BaseModel.sendAddShopCarRequest_mana(TAG, itemData.getFixmedins_code(), itemData.getGoodsNo(), "add".equals(tag) ? itemData.getShoppingCartNum() + 1 : itemData.getShoppingCartNum() - 1,
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        switch (tag) {
                            case "add":
                                TextView tvSelectNum01 = (TextView) baseQuickAdapter.getViewByPosition(recyclerView, pos, R.id.tv_select_num);

                                if (itemData.getShoppingCartNum() == 0) {
                                    FrameLayout flNumReduce01 = (FrameLayout) baseQuickAdapter.getViewByPosition(recyclerView, pos, R.id.fl_num_reduce);
                                    ShapeTextView stv12 = (ShapeTextView) baseQuickAdapter.getViewByPosition(recyclerView, pos, R.id.stv_02);
                                    TextView tvRealPrice = (TextView) baseQuickAdapter.getViewByPosition(recyclerView, pos, R.id.tv_real_price);

                                    flNumReduce01.setVisibility(View.VISIBLE);
                                    tvSelectNum01.setVisibility(View.VISIBLE);
                                    stv12.setVisibility(View.GONE);
                                    tvRealPrice.setVisibility(View.GONE);

                                    itemData.setShoppingCartNum(1);
                                    tvSelectNum01.setText("1");
                                } else {
                                    itemData.setShoppingCartNum(itemData.getShoppingCartNum() + 1);
                                    tvSelectNum01.setText(String.valueOf(itemData.getShoppingCartNum()));
                                }

                                addCart(viewGroup,view, ParcelUtils.copy(itemData));
                                break;
                            case "reduce":
                                TextView tvSelectNum02 = (TextView) baseQuickAdapter.getViewByPosition(recyclerView, pos, R.id.tv_select_num);

                                if (itemData.getShoppingCartNum() == 1) {
                                    FrameLayout flNumReduce02 = (FrameLayout) baseQuickAdapter.getViewByPosition(recyclerView, pos, R.id.fl_num_reduce);
                                    ShapeTextView stv22 = (ShapeTextView) baseQuickAdapter.getViewByPosition(recyclerView, pos, R.id.stv_02);
                                    TextView tvRealPrice = (TextView) baseQuickAdapter.getViewByPosition(recyclerView, pos, R.id.tv_real_price);

                                    flNumReduce02.setVisibility(View.GONE);
                                    tvSelectNum02.setVisibility(View.GONE);
                                    double promotionDiscountVlaue = new BigDecimal(itemData.getPromotionDiscount()).doubleValue();
                                    if ("1".equals(itemData.getIsPromotion())&&promotionDiscountVlaue<10){
                                        stv22.setVisibility(View.VISIBLE);
                                        tvRealPrice.setVisibility(View.VISIBLE);
                                    }


                                    itemData.setShoppingCartNum(0);
                                    tvSelectNum02.setText("0");
                                } else {
                                    itemData.setShoppingCartNum(itemData.getShoppingCartNum() - 1);
                                    tvSelectNum02.setText(String.valueOf(itemData.getShoppingCartNum()));
                                }

                                onReduceCarList(ParcelUtils.copy(itemData), "1", null, null);
                                break;
                        }
                    }
                });
    }

    //当外部购物车弹框 改变选择的数量时 需要更新列表中对应的数量
    public void onRefreshRightItem(GoodsModel.DataBean item) {
        List<GoodsModel.DataBean> allDatas = rightLinkAdapter.getData();
        GoodsModel.DataBean itemData;
        for (int i = 0, size = allDatas.size(); i < size; i++) {
            itemData = allDatas.get(i);
            if (itemData.getGoodsNo().equals(item.getGoodsNo())) {
                itemData.setShoppingCartNum(item.getShoppingCartNum());
                rightLinkAdapter.notifyItemChanged(i);
                break;
            }
        }
    }

    //当外部购物车弹框 清空购物车时 需要更新列表
    public void onCleanShopCar() {
        for (GoodsModel.DataBean datum : rightLinkAdapter.getData()) {
            datum.setShoppingCartNum(0);
        }

        rightLinkAdapter.notifyDataSetChanged();
    }

    //把商品添加到购物车的动画效果
    public void addCart(View view, GoodsModel.DataBean item) {
        addCart(mDecorView,view,item);
    }

    //把商品添加到购物车的动画效果
    public void addCart(ViewGroup decorView,View view, GoodsModel.DataBean item) {
        ShoppingCartAnimationView shoppingCartAnimationView = new ShoppingCartAnimationView(this);
        int position[] = new int[2];
        view.getLocationInWindow(position);
        shoppingCartAnimationView.setStartPosition(new Point(position[0], position[1]));
        decorView.addView(shoppingCartAnimationView);
        int endPosition[] = new int[2];
        mFlShopcarIcon.getLocationInWindow(endPosition);
        shoppingCartAnimationView.setEndPosition(new Point(endPosition[0], endPosition[1]));
        shoppingCartAnimationView.startBeizerAnimation();

        //2022-11-04  加入购物车的记录列表
        onAddCarList(item, null, null);
    }

    //加入购物车的记录列表
    private void onAddCarList(GoodsModel.DataBean item, TextView tvNum, TextView tvPrice) {
        //判断当前添加的商品 是否商品数为1 为1说明是新添加的商品 若不是 则说明数量大于1 那么遍历列表 查询是同一个id的情况下 把数量设置进去
        if (item.getShoppingCartNum() == 1) {
            hasJoinedShopCarDatas.add(item);
        } else {
            for (GoodsModel.DataBean hasJoinedShopCarData : hasJoinedShopCarDatas) {
                if (hasJoinedShopCarData.getGoodsNo().equals(item.getGoodsNo())) {
                    hasJoinedShopCarData.setShoppingCartNum(item.getShoppingCartNum());
                    break;
                }
            }
        }

        Log.e("CarList", "Add_CarListsize=====" + hasJoinedShopCarDatas.size());
        // 2022-11-04 计算总价
        calculatePrice(tvNum, tvPrice);
    }

    //去除购物车的记录列表  from1:来自店铺详情页面列表 2:来自店铺详情 购物车弹框
    public void onReduceCarList(GoodsModel.DataBean item, String from, TextView tvNum, TextView tvPrice) {
        //判断当前添加的商品 是否商品数为0 为0说明是要删除的商品
        if ("1".equals(from)) {
            if (item.getShoppingCartNum() <= 0) {
                hasJoinedShopCarDatas.remove(item);
            } else {
                for (GoodsModel.DataBean hasJoinedShopCarData : hasJoinedShopCarDatas) {
                    if (hasJoinedShopCarData.getGoodsNo().equals(item.getGoodsNo())) {
                        hasJoinedShopCarData.setShoppingCartNum(item.getShoppingCartNum());
                        break;
                    }
                }
            }
        }

        Log.e("CarList", "Reduce_CarListsize=====" + hasJoinedShopCarDatas.size());
        // 2022-11-04 计算总价
        calculatePrice(tvNum, tvPrice);
    }

    //计算价格
    private void calculatePrice(TextView tvNum, TextView tvPrice) {
        totalRedNum = "0";
        totalPrice = "0";
        for (GoodsModel.DataBean rightLinkModel : hasJoinedShopCarDatas) {
            if ("1".equals(rightLinkModel.getIsPromotion())) {//有折扣
                totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(rightLinkModel.getPreferentialPrice())
                        .multiply(new BigDecimal(String.valueOf(rightLinkModel.getShoppingCartNum())))).toPlainString();
            } else {//没有折扣
                totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(rightLinkModel.getPrice())
                        .multiply(new BigDecimal(String.valueOf(rightLinkModel.getShoppingCartNum())))).toPlainString();
            }
            totalRedNum = new BigDecimal(totalRedNum).add(new BigDecimal(String.valueOf(rightLinkModel.getShoppingCartNum()))).toPlainString();
        }

        if ("0".equals(totalRedNum)) {
            mSvRedPoint.setVisibility(View.GONE);
        } else {
            mSvRedPoint.setVisibility(View.VISIBLE);
            mSvRedPoint.setText(totalRedNum);
        }

        if (tvNum != null) {
            tvNum.setText("(共" + totalRedNum + "件商品)");
        }
        if (tvPrice != null) {
            tvPrice.setText(totalPrice);
        }

        mTvTotalPrice.setText(totalPrice);
    }
    //=========================右边列表===================================================

    //隐藏加载框
    public void dissWaitDailog() {
        requestTag++;
        if (requestTag >= 2) {
            if (mRefreshLayout != null && mRefreshLayout.getState() == RefreshState.Refreshing) {
                mRefreshLayout.finishRefresh();
            } else {
                hideWaitDialog();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (shopCarPop != null) {
            shopCarPop.onCleanListener();
            shopCarPop.onDestroy();
        }

        if (scanPaySelectDrugPop != null) {
            scanPaySelectDrugPop.onCleanListener();
            scanPaySelectDrugPop.onDestroy();
        }

        if (cleanShopCarDialog != null) {
            cleanShopCarDialog.onclean();
            cleanShopCarDialog = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, ScanPaySelectDrugActivity.class);
        context.startActivity(intent);
    }
}
