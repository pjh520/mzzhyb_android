package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.adapter.StmtDetailAdapter;
import com.jrdz.zhyb_android.ui.home.model.StmtDetailModel;
import com.jrdz.zhyb_android.ui.home.model.StmtInfoListModel;
import com.jrdz.zhyb_android.ui.home.model.StmtTotalModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/28
 * 描    述：结算对明细账
 * ================================================
 */
public class StmtDetailActivity extends BaseRecyclerViewActivity {

    private String StmtTotalId;

    @Override
    public void initAdapter() {
        mAdapter=new StmtDetailAdapter();
    }

    @Override
    public void initData() {
        StmtTotalId = getIntent().getStringExtra("StmtTotalId");
        super.initData();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        super.getData();
        StmtDetailModel.sendStmtDetailRequest(TAG, StmtTotalId, String.valueOf(mPageNum), "20", new CustomerJsonCallBack<StmtDetailModel>() {
            @Override
            public void onRequestError(StmtDetailModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(StmtDetailModel returnData) {
                hideRefreshView();
                List<StmtDetailModel.DataBean> infos = returnData.getData();
                if (mAdapter!=null&&infos != null) {
                    for (StmtDetailModel.DataBean info : infos) {
                        for (DataDicModel insutypeDatum : CommonlyUsedDataUtils.getInstance().getRefdSetlFlagData()) {
                            if (info.getRefd_setl_flag().equals(insutypeDatum.getValue())) {
                                info.setRefd_setl_flag_name(insutypeDatum.getLabel());
                                break;
                            }
                        }

                        for (DataDicModel stmtRslt : CommonlyUsedDataUtils.getInstance().getStmtRsltData()) {
                            if (info.getStmt_rslt().equals(stmtRslt.getValue())) {
                                info.setStmt_rslt_name(stmtRslt.getLabel());
                                break;
                            }
                        }
                    }

                    if (mPageNum == 0) {
                        mAdapter.setNewData(infos);
                    } else {
                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    public static void newIntance(Context context, String StmtTotalId) {
        Intent intent = new Intent(context, StmtDetailActivity.class);
        intent.putExtra("StmtTotalId", StmtTotalId);
        context.startActivity(intent);
    }
}
