package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.wheel.TimeWheelUtils;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.adapter.StmtInfoListAdapter;
import com.jrdz.zhyb_android.ui.home.model.DepartmentManageModel;
import com.jrdz.zhyb_android.ui.home.model.KVModel;
import com.jrdz.zhyb_android.ui.home.model.StmtDetailModel;
import com.jrdz.zhyb_android.ui.home.model.StmtInfoListModel;
import com.jrdz.zhyb_android.ui.home.model.StmtTotalListModel;
import com.jrdz.zhyb_android.ui.home.model.StmtTotalModel;
import com.jrdz.zhyb_android.ui.settlement.adapter.SettleRecordAdapter;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/28
 * 描    述： 结算对账信息列表
 * ================================================
 */
public class StmtInfoListActvity extends BaseRecyclerViewActivity {
    private ObserverWrapper<String> mStmlListObserver=new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    };

    @Override
    public void initAdapter() {
        mAdapter = new StmtInfoListAdapter();
        mAdapter.setHeaderAndEmpty(true);
    }

    @Override
    public void initData() {
        super.initData();
        MsgBus.sendStmlListRefresh().observe(this, mStmlListObserver);
        setRightTitleView("对账查询");

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void getData() {
        super.getData();

        StmtTotalListModel.sendStmtTotalListRequest(TAG, String.valueOf(mPageNum), "20", new CustomerJsonCallBack<StmtTotalListModel>() {
            @Override
            public void onRequestError(StmtTotalListModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(StmtTotalListModel returnData) {
                hideRefreshView();
                List<StmtTotalListModel.DataBean> infos = returnData.getData();
                if (mAdapter!=null&&infos != null) {
                    for (StmtTotalListModel.DataBean info : infos) {
                        for (DataDicModel insutypeDatum : CommonlyUsedDataUtils.getInstance().getInsutypeData()) {
                            if (info.getInsutype().equals(insutypeDatum.getValue())) {
                                info.setInsutype_name(insutypeDatum.getLabel());
                                break;
                            }
                        }

                        for (DataDicModel medTypeDatum : CommonlyUsedDataUtils.getInstance().getClrTypeData()) {
                            if (info.getClr_type().equals(medTypeDatum.getValue())) {
                                info.setClr_type_name(medTypeDatum.getLabel());
                                break;
                            }
                        }

                        for (DataDicModel medTypeDatum : CommonlyUsedDataUtils.getInstance().getStmtRsltData()) {
                            if (info.getStmt_rslt().equals(medTypeDatum.getValue())) {
                                info.setStmt_rslt_name(medTypeDatum.getLabel());
                                break;
                            }
                        }
                    }

                    if (mPageNum == 0) {
                        mAdapter.setNewData(infos);
                        if (infos.size() <= 0) {
                            mAdapter.isUseEmpty(true);
                        }
                    } else {
                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void rightTitleViewClick() {
        StmtQueryActvitiy.newIntance(this);
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter,view,position);
        switch (view.getId()) {
            case R.id.tv_sub_ledger://对明细账
                StmtTotalListModel.DataBean itemData = ((StmtInfoListAdapter) adapter).getItem(position);
                if ("0".equals(itemData.getIsStmtDetail())){//没对账
                    showWaitDialog();
                    subLedger(itemData);
                }else {
                    showShortToast("已对过明细账，请点击查看明细账按钮");
                }
                break;
            case R.id.tv_view_sub_ledger://查看明细账
                StmtTotalListModel.DataBean itemData02 = ((StmtInfoListAdapter) adapter).getItem(position);
                if ("0".equals(itemData02.getIsStmtDetail())){//没对账
                    showShortToast("请先点击对明细账按钮");
                }else {
                    StmtDetailActivity.newIntance(StmtInfoListActvity.this,EmptyUtils.strEmpty(itemData02.getStmtTotalId()));
                }
                break;
        }
    }

    //对明细账
    private void subLedger(StmtTotalListModel.DataBean itemData) {
        BaseModel.sendStmtDetailRequest(TAG,EmptyUtils.strEmpty(itemData.getStmtTotalId()), EmptyUtils.strEmpty(itemData.getSetl_optins()),
                EmptyUtils.strEmpty(itemData.getStmt_begndate()), EmptyUtils.strEmpty(itemData.getStmt_enddate()), EmptyUtils.strEmpty(itemData.getMedfee_sumamt()),
                EmptyUtils.strEmpty(itemData.getFund_pay_sumamt()), EmptyUtils.strEmpty(itemData.getAcct_pay()), EmptyUtils.strEmpty(itemData.getFixmedins_setl_cnt()),
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showTipDialog(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        itemData.setIsStmtDetail("1");
                        showShortToast("对明细账成功");
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, StmtInfoListActvity.class);
        context.startActivity(intent);
    }
}
