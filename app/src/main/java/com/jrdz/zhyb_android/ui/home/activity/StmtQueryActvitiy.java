package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.wheel.TimeWheelUtils;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/1/28
 * 描    述：对账查询
 * ================================================
 */
public class StmtQueryActvitiy extends BaseActivity {
    private TextView mTvStmtBegndate;
    private TextView mTvStmtEnddate;
    private ShapeTextView mTvQuery;

    private TimeWheelUtils timeWheelUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_stmt_query;
    }

    @Override
    public void initView() {
        super.initView();
        mTvStmtBegndate = findViewById(R.id.tv_stmt_begndate);
        mTvStmtEnddate = findViewById(R.id.tv_stmt_enddate);
        mTvQuery = findViewById(R.id.tv_query);
    }

    @Override
    public void initData() {
        super.initData();

        timeWheelUtils = new TimeWheelUtils();
        timeWheelUtils.isShowDay(true, true, true, true, true, true);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvStmtBegndate.setOnClickListener(this);
        mTvStmtEnddate.setOnClickListener(this);
        mTvQuery.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_stmt_begndate://开始时间
                timeWheelUtils.showTimeWheel(StmtQueryActvitiy.this, "开始时间", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mTvStmtBegndate.setText(EmptyUtils.strEmpty(dateInfo));
                    }
                });
                break;
            case R.id.tv_stmt_enddate://结束时间
                timeWheelUtils.showTimeWheel(StmtQueryActvitiy.this, "结束时间", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mTvStmtEnddate.setText(EmptyUtils.strEmpty(dateInfo));
                    }
                });
                break;
            case R.id.tv_query://查询
                if (EmptyUtils.isEmpty(mTvStmtBegndate.getText().toString())) {
                    showShortToast("请选择对账开始日期");
                    return;
                }

                if (EmptyUtils.isEmpty(mTvStmtEnddate.getText().toString())) {
                    showShortToast("请选择对账结束日期");
                    return;
                }

                StmtQueryResultActvitiy.newIntance(StmtQueryActvitiy.this,mTvStmtBegndate.getText().toString(),mTvStmtEnddate.getText().toString());
                break;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timeWheelUtils != null) {
            timeWheelUtils.dissTimeWheel();
            timeWheelUtils = null;
        }
    }

    //对账查询
    public static void newIntance(Context context) {
        Intent intent = new Intent(context, StmtQueryActvitiy.class);
        context.startActivity(intent);
    }
}
