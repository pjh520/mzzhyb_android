package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.CheckBox;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.ui.home.adapter.MonthSettQueryAdapter;
import com.jrdz.zhyb_android.ui.home.model.MonthSettModel;
import com.jrdz.zhyb_android.ui.home.model.QueryMonSetlModel;
import com.jrdz.zhyb_android.utils.AppManager_Acivity;
import com.frame.compiler.utils.gson.GsonUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.adapter.StmtQueryResultAdapter;
import com.jrdz.zhyb_android.ui.home.model.StmtQueryResultModel;
import com.jrdz.zhyb_android.ui.home.model.StmtQueryResultParmaModel;
import com.jrdz.zhyb_android.ui.home.model.StmtTotalModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/1/28
 * 描    述：对账查询结果页面
 * ================================================
 */
public class StmtQueryResultActvitiy extends BaseRecyclerViewActivity {
    private ShapeTextView mTvGeneralLedger;

    private String begndate, enddate;
    private StmtQueryResultModel.DataBean currentMonthSettModel;
    private CheckBox currentView;

    @Override
    public int getLayoutId() {
        return R.layout.activtiy_stmt_queryresult;
    }

    @Override
    public void initAdapter() {
        mAdapter = new StmtQueryResultAdapter();
    }

    @Override
    public void initView() {
        super.initView();

        mTvGeneralLedger = findViewById(R.id.tv_general_ledger);
    }

    @Override
    public void initData() {
        begndate = getIntent().getStringExtra("begndate");
        enddate = getIntent().getStringExtra("enddate");
        super.initData();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvGeneralLedger.setOnClickListener(this);
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void getData() {
        super.getData();

        StmtQueryResultModel.sendStmtQueryResultRequest(TAG, begndate, enddate, new CustomerJsonCallBack<StmtQueryResultModel>() {
            @Override
            public void onRequestError(StmtQueryResultModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(StmtQueryResultModel returnData) {
                hideRefreshView();
                List<StmtQueryResultModel.DataBean> infos = returnData.getData();
                if (mAdapter!=null&&infos != null) {
                    for (StmtQueryResultModel.DataBean info : infos) {
                        for (DataDicModel insutypeDatum : CommonlyUsedDataUtils.getInstance().getInsutypeData()) {
                            if (info.getInsutype().equals(insutypeDatum.getValue())) {
                                info.setInsutype_name(insutypeDatum.getLabel());
                                break;
                            }
                        }

                        for (DataDicModel medTypeDatum : CommonlyUsedDataUtils.getInstance().getClrTypeData()) {
                            if (info.getClr_type().equals(medTypeDatum.getValue())) {
                                info.setClr_type_name(medTypeDatum.getLabel());
                                break;
                            }
                        }
                    }
                    if (mPageNum == 0) {
                        mAdapter.setNewData(infos);
                        if (infos.size() <= 0) {
                            mAdapter.isUseEmpty(true);
                        }
                    } else {
                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter baseQuickAdapter, View view, int position) {
        super.customItemChildClick(baseQuickAdapter, view, position);
        switch (view.getId()) {
            case R.id.fl_cb://选中需要的数据
                if (!((StmtQueryResultAdapter) baseQuickAdapter).getItem(position).isChoose()) {
                    if (currentMonthSettModel != null) {
                        currentMonthSettModel.setChoose(false);
                    }

                    if (currentView != null) {
                        currentView.setChecked(false);
                    }

                    currentView = view.findViewById(R.id.cb);
                    currentMonthSettModel = ((StmtQueryResultAdapter) baseQuickAdapter).getItem(position);

                    currentMonthSettModel.setChoose(true);
                    currentView.setChecked(true);
                }
                break;
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()) {
            case R.id.tv_general_ledger://对总账
                generalLedger();
                break;
        }
    }

    //对总账
    private void generalLedger() {
        if (currentMonthSettModel == null) {
            showShortToast("请选择需要对账的数据项");
            return;
        }

        showWaitDialog();
        StmtQueryResultParmaModel stmtQueryResultParmaModel = new StmtQueryResultParmaModel();

        StmtQueryResultParmaModel.StmtTotalInfoBean stmtTotalInfoBean = new StmtQueryResultParmaModel.StmtTotalInfoBean(currentMonthSettModel.getInsutype(),
                currentMonthSettModel.getClr_type(), MechanismInfoUtils.getAdmdvs(), begndate, enddate,
                currentMonthSettModel.getMedfee_sumamt(), currentMonthSettModel.getFund_pay_sumamt(), currentMonthSettModel.getAcct_pay(),
                currentMonthSettModel.getFixmedins_setl_cnt());
        stmtQueryResultParmaModel.getStmtTotalInfo().add(stmtTotalInfoBean);

        StmtTotalModel.sendStmtTotalRequest(TAG, GsonUtils.getInstance().toJson(stmtQueryResultParmaModel), new CustomerJsonCallBack<StmtTotalModel>() {
            @Override
            public void onRequestError(StmtTotalModel returnData, String msg) {
                hideWaitDialog();
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(StmtTotalModel returnData) {
                hideWaitDialog();
                if (returnData.getData() != null) {
                    showShortToast(returnData.getData().getStmt_rslt_dscr());
                    MsgBus.sendStmlListRefresh().post("1");
                    AppManager_Acivity.getInstance().finishActivity(StmtQueryActvitiy.class);
                    finish();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (currentMonthSettModel != null) {
            currentMonthSettModel = null;
        }

        if (currentView != null) {
            currentView = null;
        }
    }

    //对账查询
    public static void newIntance(Context context, String begndate, String enddate) {
        Intent intent = new Intent(context, StmtQueryResultActvitiy.class);
        intent.putExtra("begndate", begndate);
        intent.putExtra("enddate", enddate);
        context.startActivity(intent);
    }
}
