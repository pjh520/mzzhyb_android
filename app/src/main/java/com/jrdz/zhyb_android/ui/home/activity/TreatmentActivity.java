package com.jrdz.zhyb_android.ui.home.activity;

import android.content.Context;
import android.content.Intent;

import com.frame.compiler.utils.DateUtil;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.adapter.TreatmentAdapter;
import com.jrdz.zhyb_android.ui.home.model.TreatmentModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/28
 * 描    述：人员待遇查询
 * ================================================
 */
public class TreatmentActivity extends BaseRecyclerViewActivity {
    private String psn_no, insutype, med_type,insuplc_admdvs;

    @Override
    public void initAdapter() {
        mAdapter = new TreatmentAdapter();
    }

    @Override
    public void initData() {
        psn_no = getIntent().getStringExtra("psn_no");
        insutype = getIntent().getStringExtra("insutype");
        med_type = getIntent().getStringExtra("med_type");
        insuplc_admdvs = getIntent().getStringExtra("insuplc_admdvs");
        super.initData();

        showWaitDialog();
        getTreatmentData();
    }

    //获取人员待遇数据
    private void getTreatmentData() {
        TreatmentModel.sendTreatmentRequest(TAG, psn_no, insutype, med_type,insuplc_admdvs, new CustomerJsonCallBack<TreatmentModel>() {
            @Override
            public void onRequestError(TreatmentModel returnData, String msg) {
                hideWaitDialog();
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(TreatmentModel returnData) {
                hideWaitDialog();
                List<TreatmentModel.DataBean> obj = returnData.getData();
                if (mAdapter!=null&&obj != null) {
                    for (TreatmentModel.DataBean trtinfo : obj) {
                        for (DataDicModel fundPayTypeDatum : CommonlyUsedDataUtils.getInstance().getFundPayTypeData()) {
                            if (trtinfo.getFund_pay_type().equals(fundPayTypeDatum.getValue())) {
                                trtinfo.setFund_pay_type_name(fundPayTypeDatum.getLabel());
                                break;
                            }
                        }

                        for (DataDicModel trtEnjymntFlagDatum : CommonlyUsedDataUtils.getInstance().getTrtEnjymntFlagData()) {
                            if (trtinfo.getTrt_enjymnt_flag().equals(trtEnjymntFlagDatum.getValue())) {
                                trtinfo.setTrt_enjymnt_flag_name(trtEnjymntFlagDatum.getLabel());
                                break;
                            }
                        }
                    }

                    mAdapter.setNewData(obj);
                }
            }
        });
    }

    //待遇检查
    public static void newIntance(Context context, String psn_no, String insutype, String med_type,String insuplc_admdvs) {
        Intent intent = new Intent(context, TreatmentActivity.class);
        intent.putExtra("psn_no", psn_no);
        intent.putExtra("insutype", insutype);
        intent.putExtra("med_type", med_type);
        intent.putExtra("insuplc_admdvs", insuplc_admdvs);
        context.startActivity(intent);
    }
}
