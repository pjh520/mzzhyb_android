package com.jrdz.zhyb_android.ui.home.activity;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.frame.compiler.utils.DateUtil;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.model.DepartmentManageModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.MMKVUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/9
 * 描    述：修改或者删除科室
 * ================================================
 */
public class UpdateOrDelDepartActivity extends AddDepartmentActivity {
    private DepartmentManageModel.DataBean resultObjBean;
    private int pos = 0;
    private CustomerDialogUtils customerDialogUtils;

    @Override
    public void initData() {
        setRightTitleView("删除");
        pos = getIntent().getIntExtra("pos", 0);
        resultObjBean = getIntent().getParcelableExtra("resultObjBean");
        super.initData();

        mEtHospDeptCodg.setEnabled(false);
        mEtHospDeptCodg.setTextColor(getResources().getColor(R.color.txt_color_999));
        mTvBegntime.setEnabled(false);
        mTvBegntime.setTextColor(getResources().getColor(R.color.txt_color_999));
        mIvBegntime.setVisibility(View.GONE);

        if (resultObjBean == null) {
            showShortToast("数据有误，请重新进入页面");
            finish();
            return;
        }
        mTvAdd.setText("修改");
        customerDialogUtils = new CustomerDialogUtils();
        //获取参保就医区划
        String insuplcAdmdvsText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("AREA_ADMVS", resultObjBean.getPoolarea_no());
        mTvPoolareaNo.setText(insuplcAdmdvsText);
        mEtHospDeptName.setText(EmptyUtils.strEmpty(resultObjBean.getHosp_dept_name()));
        mEtHospDeptCodg.setText(EmptyUtils.strEmpty(resultObjBean.getHosp_dept_codg()));

        //初始设置科别数据
        if (catyData != null) {
            for (DataDicModel catyDatum : catyData) {
                if (catyDatum.getValue().equals(resultObjBean.getCaty())) {
                    mTvCaty.setTag(resultObjBean.getCaty());
                    mTvCaty.setText(EmptyUtils.strEmpty(catyDatum.getLabel()));
                    break;
                }
            }
        }

        mTvDeptEstbdat.setText(EmptyUtils.strEmpty(resultObjBean.getDept_estbdat()));
        mTvBegntime.setText(EmptyUtils.strEmpty(resultObjBean.getBegntime()));
        mTvEndtime.setText(EmptyUtils.strEmpty(resultObjBean.getEndtime()));
        mEtDeptResperName.setText(EmptyUtils.strEmpty(resultObjBean.getDept_resper_name()));
        mEtDeptResperTel.setText(EmptyUtils.strEmpty(resultObjBean.getDept_resper_tel()));
        mEtDrPsncnt.setText(EmptyUtils.strEmpty(resultObjBean.getDr_psncnt()));
        mEtPharPsncnt.setText(EmptyUtils.strEmpty(resultObjBean.getPhar_psncnt()));
        mEtNursPsncnt.setText(EmptyUtils.strEmpty(resultObjBean.getNurs_psncnt()));
        mEtTecnPsncnt.setText(EmptyUtils.strEmpty(resultObjBean.getTecn_psncnt()));
        mEtAprvBedCnt.setText(EmptyUtils.strEmpty(resultObjBean.getAprv_bed_cnt()));
        mEtHiCrtfBedCnt.setText(EmptyUtils.strEmpty(resultObjBean.getHi_crtf_bed_cnt()));
        mEtDeptMedServScp.setText(EmptyUtils.strEmpty(resultObjBean.getDept_med_serv_scp()));
        mEtItro.setText(EmptyUtils.strEmpty(resultObjBean.getItro()));
        mEtMemo.setText(EmptyUtils.strEmpty(resultObjBean.getMemo()));
    }

    @Override
    public void rightTitleViewClick() {
        customerDialogUtils.showDialog(UpdateOrDelDepartActivity.this, "提示", "是否确认删除科室?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        //删除科室
                        showWaitDialog();
                        BaseModel.sendDeletDeptRequest(TAG,resultObjBean.getDeptId(),resultObjBean.getHosp_dept_codg(), resultObjBean.getHosp_dept_name(), resultObjBean.getBegntime(), new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                showShortToast("删除科室成功");
                                Intent intent = new Intent();
                                intent.putExtra("type", "0");
                                intent.putExtra("pos", pos);
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        });
                    }
                });
    }

    //新增科室
    @Override
    protected void addDepartment() {
        if (EmptyUtils.isEmpty(mEtHospDeptName.getText().toString())) {
            showShortToast("请输入医院科室名称");
            return;
        }
        if (EmptyUtils.isEmpty(mEtHospDeptCodg.getText().toString())) {
            showShortToast("请输入医院科室编码");
            return;
        }
        if (EmptyUtils.isEmpty(mTvCaty.getTag().toString())) {
            showShortToast("请选择科别");
            return;
        }
        if (EmptyUtils.isEmpty(mTvDeptEstbdat.getText().toString())) {
            showShortToast("请选择科室成立日期");
            return;
        }
        if (EmptyUtils.isEmpty(mTvBegntime.getText().toString())) {
            showShortToast("请选择开始时间");
            return;
        }
        if (EmptyUtils.isEmpty(mEtDeptResperName.getText().toString())) {
            showShortToast("请输入科室负责人姓名");
            return;
        }
        if (EmptyUtils.isEmpty(mEtDeptResperTel.getText().toString())) {
            showShortToast("请输入科室负责人电话");
            return;
        }
        if (EmptyUtils.isEmpty(mEtDrPsncnt.getText().toString())) {
            showShortToast("请输入医师人数");
            return;
        }
        if (EmptyUtils.isEmpty(mEtPharPsncnt.getText().toString())) {
            showShortToast("请输入药师人数");
            return;
        }
        if (EmptyUtils.isEmpty(mEtNursPsncnt.getText().toString())) {
            showShortToast("请输入护士人数");
            return;
        }
        if (EmptyUtils.isEmpty(mEtTecnPsncnt.getText().toString())) {
            showShortToast("请输入技师人数");
            return;
        }
        if (EmptyUtils.isEmpty(mEtAprvBedCnt.getText().toString())) {
            showShortToast("请输入批准床位数量");
            return;
        }
        if (EmptyUtils.isEmpty(mEtItro.getText().toString())) {
            showShortToast("请输入简介");
            return;
        }

        showWaitDialog();
        BaseModel.sendUpdateDeptRequest(TAG, resultObjBean.getDeptId(), resultObjBean.getHosp_dept_codg(), mEtHospDeptCodg.getText().toString(), mTvCaty.getTag().toString(),
                mEtHospDeptName.getText().toString(), mTvBegntime.getText().toString(), mTvEndtime.getText().toString(), mEtItro.getText().toString(),
                mEtDeptResperName.getText().toString(), mEtDeptResperTel.getText().toString(), mEtDeptMedServScp.getText().toString(),
                mTvDeptEstbdat.getText().toString(), mEtAprvBedCnt.getText().toString(), mEtHiCrtfBedCnt.getText().toString(),
                EmptyUtils.strEmpty(MechanismInfoUtils.getAdmdvs()), mEtDrPsncnt.getText().toString(), mEtPharPsncnt.getText().toString(),
                mEtNursPsncnt.getText().toString(), mEtTecnPsncnt.getText().toString(), mEtMemo.getText().toString(), new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("更新科室成功");
                        resultObjBean.setData(resultObjBean.getDeptId(), mEtHospDeptCodg.getText().toString(), mTvCaty.getTag().toString(),mTvCaty.getText().toString(),
                                mEtHospDeptName.getText().toString(), mTvBegntime.getText().toString(), mTvEndtime.getText().toString(),
                                mEtItro.getText().toString(), mEtDeptResperName.getText().toString(), mEtDeptResperTel.getText().toString(), mEtDeptMedServScp.getText().toString(),
                                mTvDeptEstbdat.getText().toString(), mEtAprvBedCnt.getText().toString(), mEtHiCrtfBedCnt.getText().toString(),
                                mTvPoolareaNo.getText().toString(), mEtDrPsncnt.getText().toString(), mEtPharPsncnt.getText().toString(),
                                mEtNursPsncnt.getText().toString(), mEtTecnPsncnt.getText().toString(), mEtMemo.getText().toString());

                        Intent intent = new Intent();
                        intent.putExtra("type", "1");
                        intent.putExtra("pos", pos);
                        intent.putExtra("data", resultObjBean);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
        }
        super.onDestroy();
        pos = 0;
        resultObjBean = null;
    }

    public static void newIntance(Activity activity, int pos, DepartmentManageModel.DataBean resultObjBean, int requestCode) {
        Intent intent = new Intent(activity, UpdateOrDelDepartActivity.class);
        intent.putExtra("pos", pos);
        intent.putExtra("resultObjBean", resultObjBean);
        activity.startActivityForResult(intent, requestCode);
    }
}
