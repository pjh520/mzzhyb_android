package com.jrdz.zhyb_android.ui.home.adapter;

import android.graphics.Color;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.QuestionnaireModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-09-06
 * 描    述：
 * ================================================
 */
public class AnswerAdapter extends BaseQuickAdapter<QuestionnaireModel.DataBean.TemplateSubItemBean, BaseViewHolder> {
    private QuestionnaireModel.DataBean.TemplateSubItemBean currentData;
    private ImageView currentFlItem;

    public AnswerAdapter() {
        super(R.layout.layout_answer_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, QuestionnaireModel.DataBean.TemplateSubItemBean item) {
        ImageView ivRegion02=baseViewHolder.getView(R.id.iv_region);
        baseViewHolder.setText(R.id.tv_region, EmptyUtils.strEmpty(item.getSubItemTitle()));

        if (item.isChoose()){
            currentFlItem=ivRegion02;
            currentData=item;

            ivRegion02.setImageResource(R.drawable.ic_dagou_pre);
        }else {
            ivRegion02.setImageResource(R.drawable.ic_dagou_nor);
        }
    }

    public QuestionnaireModel.DataBean.TemplateSubItemBean getCurrentData() {
        return currentData;
    }

    public void setCurrentData(QuestionnaireModel.DataBean.TemplateSubItemBean currentData) {
        this.currentData = currentData;
    }

    public ImageView getCurrentFlItem() {
        return currentFlItem;
    }

    public void setCurrentFlItem(ImageView currentFlItem) {
        this.currentFlItem = currentFlItem;
    }
}
