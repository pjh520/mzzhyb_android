package com.jrdz.zhyb_android.ui.home.adapter;

import androidx.annotation.NonNull;


import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.DateUtil;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.DepartmentManageModel;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/10
 * 描    述：
 * ================================================
 */
public class DepartmentManageAdapter extends BaseQuickAdapter<DepartmentManageModel.DataBean, BaseViewHolder> {
    public DepartmentManageAdapter() {
        super(R.layout.layout_department_manage_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, DepartmentManageModel.DataBean resultObjBean) {
        baseViewHolder.setText(R.id.tv_hospital_name, EmptyUtils.strEmpty(resultObjBean.getHosp_dept_name()));
        baseViewHolder.setText(R.id.tv_hosp_dept_codg, "科室编码：" + resultObjBean.getHosp_dept_codg());
        baseViewHolder.setText(R.id.tv_dept, "科"+mContext.getResources().getString(R.string.spaces)+mContext.getResources().getString(R.string.spaces)+"别：" + resultObjBean.getCaty_name());
        baseViewHolder.setText(R.id.tv_begntime, "成立日期：" + resultObjBean.getDept_estbdat());
        baseViewHolder.setText(R.id.tv_dept_resper_name, "负" + mContext.getResources().getString(R.string.spaces_half) + "责" +
                mContext.getResources().getString(R.string.spaces_half) + "人：" + resultObjBean.getDept_resper_name() + "  " + resultObjBean.getDept_resper_tel());
    }
}
