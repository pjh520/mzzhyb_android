package com.jrdz.zhyb_android.ui.home.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.StringUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.DoctorManageModel;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/11
 * 描    述：
 * ================================================
 */
public class DoctorManageAdapter extends BaseQuickAdapter<DoctorManageModel.DataBean, BaseViewHolder> {
    public DoctorManageAdapter() {
        super(R.layout.layout_doctor_manage_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder = super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.tv_edit,R.id.tv_open,R.id.tv_close);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, DoctorManageModel.DataBean doctorManageModel) {
        TextView tvDept=baseViewHolder.getView(R.id.tv_dept);
        TextView tvOpen=baseViewHolder.getView(R.id.tv_open);
        TextView tvClose=baseViewHolder.getView(R.id.tv_close);

        //（0禁用1启用）
        if ("0".equals(doctorManageModel.getIsEnable())){//禁用
            tvOpen.setEnabled(true);
            tvClose.setEnabled(false);
        }else {//启用
            tvOpen.setEnabled(false);
            tvClose.setEnabled(true);
        }

        baseViewHolder.setText(R.id.tv_name, EmptyUtils.strEmpty(doctorManageModel.getDr_name()));
        baseViewHolder.setText(R.id.tv_dr_type_name, EmptyUtils.strEmpty(doctorManageModel.getDr_type_name()));
        baseViewHolder.setText(R.id.tv_phone, StringUtils.encryptionPhone(doctorManageModel.getDr_phone()));
        baseViewHolder.setText(R.id.tv_userid,"人员编码："+doctorManageModel.getDr_code());
        if ("1".equals(MechanismInfoUtils.getFixmedinsType())){//医院
            tvDept.setVisibility(View.VISIBLE);
            tvDept.setText("所属科室："+doctorManageModel.getHosp_dept_name());
        }else {//药店
            tvDept.setVisibility(View.GONE);
        }

    }
}
