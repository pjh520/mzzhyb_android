package com.jrdz.zhyb_android.ui.home.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.marqueeView.XMarqueeView;
import com.frame.compiler.widget.marqueeView.XMarqueeViewAdapter;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.NotifyModel;

/**
 * ================================================
 * 项目名称：Pumeng_master
 * 包    名：com.languo.pumeng_master.ui.more.schools.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/7/10 0010
 * 描    述：
 * ================================================
 */
public class HomeMarqAdapter extends XMarqueeViewAdapter<NotifyModel.DataBean> {
    private IOptionListener iOptionListener;

    public HomeMarqAdapter(IOptionListener iOptionListener) {
        this.iOptionListener=iOptionListener;
    }

    @Override
    public View onCreateView(XMarqueeView parent) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_home_marquee_item, null);
    }

    @Override
    public void onBindView(View parent, View view, int position) {
        TextView tvText = (TextView) view.findViewById(R.id.tv_text);
        tvText.setText("•  "+EmptyUtils.strEmpty(mDatas.get(position).getTitle()));
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iOptionListener.onItemClick(mDatas.get(position));
            }
        });
    }

    public interface IOptionListener{
        void onItemClick(NotifyModel.DataBean dataBean);
    }
}
