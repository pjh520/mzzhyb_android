package com.jrdz.zhyb_android.ui.home.adapter;

import android.widget.ImageView;
import androidx.annotation.NonNull;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.HomeSortModel;

public class HomeSortAdapter extends BaseQuickAdapter<HomeSortModel, BaseViewHolder> {
    public HomeSortAdapter() {
        super(R.layout.layout_home_sort_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, HomeSortModel item) {
        ImageView ivMall = helper.getView(R.id.iv_mall);

        ivMall.setImageResource(item.getImg());
        helper.setText(R.id.tv_mall,item.getText());
    }
}
