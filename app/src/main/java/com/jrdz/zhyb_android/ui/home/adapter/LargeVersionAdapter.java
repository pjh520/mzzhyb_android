package com.jrdz.zhyb_android.ui.home.adapter;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.LargeVersionModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/21
 * 描    述：
 * ================================================
 */
public class LargeVersionAdapter extends BaseQuickAdapter<LargeVersionModel, BaseViewHolder> {
    public LargeVersionAdapter() {
        super(R.layout.layout_large_version_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, LargeVersionModel largeVersionModel) {
        View line01=baseViewHolder.getView(R.id.line01);
        ImageView iv=baseViewHolder.getView(R.id.iv);
        View line02=baseViewHolder.getView(R.id.line02);

        if (0==baseViewHolder.getAbsoluteAdapterPosition()%2){
            line01.setVisibility(View.GONE);
            line02.setVisibility(View.VISIBLE);
        }else {
            line01.setVisibility(View.VISIBLE);
            line02.setVisibility(View.GONE);
        }

        iv.setImageResource(largeVersionModel.getImg());
    }
}
