package com.jrdz.zhyb_android.ui.home.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.MedInsuCataModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/7
 * 描    述：
 * ================================================
 */
public class MedInsuCataQueryAdapter extends BaseQuickAdapter<MedInsuCataModel, BaseViewHolder> {
    public MedInsuCataQueryAdapter() {
        super(R.layout.layout_medinsucata_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, MedInsuCataModel medInsuCataModel) {

    }
}
