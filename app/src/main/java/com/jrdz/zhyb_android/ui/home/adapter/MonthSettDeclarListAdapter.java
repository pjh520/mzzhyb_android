package com.jrdz.zhyb_android.ui.home.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.hjq.shape.view.ShapeView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.MonthSettDeclarListModel;
import com.jrdz.zhyb_android.ui.home.model.StmtInfoListModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-06-07
 * 描    述：
 * ================================================
 */
public class MonthSettDeclarListAdapter extends BaseQuickAdapter<MonthSettDeclarListModel.DataBean, BaseViewHolder> {
    public  MonthSettDeclarListAdapter() {
        super(R.layout.layout_month_sett_declar_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, MonthSettDeclarListModel.DataBean resultObjBean) {
        baseViewHolder.setText(R.id.tv_decla_time, "申报时间："+resultObjBean.getCrte_time());
        baseViewHolder.setText(R.id.tv_decla_status, "清算状态："+EmptyUtils.strEmptyToText(resultObjBean.getClr_stasname(),"--"));
        baseViewHolder.setText(R.id.tv_decla_date, "清算年月："+EmptyUtils.strEmptyToText(resultObjBean.getClr_ym(),"--"));
        baseViewHolder.setText(R.id.tv_decla_start_date, "开始日期："+EmptyUtils.strEmptyToText(resultObjBean.getBegndate(),"--"));
        baseViewHolder.setText(R.id.tv_decla_end_date, "结束日期："+EmptyUtils.strEmptyToText(resultObjBean.getEnddate(),"--"));

        String clr_type="";
        switch (resultObjBean.getClr_type()){
            case "11":
                clr_type="门诊";
                break;
            case "41":
                clr_type="药店购药";
                break;
            case "21":
                clr_type="住院";
                break;
            case "99":
                clr_type="其他";
                break;
        }
        baseViewHolder.setText(R.id.tv_decla_type, "清算类别："+EmptyUtils.strEmptyToText(clr_type,"--"));
        baseViewHolder.setText(R.id.tv_decla_mode, "清算方式："+EmptyUtils.strEmptyToText(resultObjBean.getClr_wayname(),"--"));
        baseViewHolder.setText(R.id.tv_decla_person_time, "清算人次："+EmptyUtils.strEmptyToText(resultObjBean.getClr_psntime(),"--"));
        baseViewHolder.setText(R.id.tv_decla_insur, "险种："+EmptyUtils.strEmptyToText(resultObjBean.getInsutype_name(),"--"));
        baseViewHolder.setText(R.id.tv_medfee_sumamt, "医疗费用总额："+EmptyUtils.strEmptyToText(resultObjBean.getMedfee_sumamt(),"--"));
        baseViewHolder.setText(R.id.tv_hi_agre_sumfee, "医保费用总额："+EmptyUtils.strEmptyToText(resultObjBean.getHi_agre_sumfee(),"--"));
        baseViewHolder.setText(R.id.tv_fund_appy_sum, "基金申报总额："+EmptyUtils.strEmptyToText(resultObjBean.getFund_appy_sum(),"--"));
        baseViewHolder.setText(R.id.tv_cash_payamt, "现金支付金额："+EmptyUtils.strEmptyToText(resultObjBean.getCash_payamt(),"--"));
        baseViewHolder.setText(R.id.tv_acct_pay, "个人账户支付："+EmptyUtils.strEmptyToText(resultObjBean.getAcct_pay(),"--"));
    }
}
