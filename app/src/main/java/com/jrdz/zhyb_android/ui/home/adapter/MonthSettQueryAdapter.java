package com.jrdz.zhyb_android.ui.home.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.MonthSettModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/24
 * 描    述：
 * ================================================
 */
public class MonthSettQueryAdapter extends BaseQuickAdapter<MonthSettModel, BaseViewHolder> {
    public MonthSettQueryAdapter() {
        super(R.layout.layout_month_sett_query_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, MonthSettModel monthSettModel) {
        ShapeTextView mTv = baseViewHolder.getView(R.id.tv);
        mTv.setText(EmptyUtils.strEmpty(monthSettModel.getShowText()));

        if (monthSettModel.isChoose()){
            mTv.getShapeDrawableBuilder().setStrokeColor(mContext.getResources().getColor(R.color.bar_transparent))
                    .setSolidColor(mContext.getResources().getColor(R.color.color_4970e0)).intoBackground();
            mTv.setTextColor(mContext.getResources().getColor(R.color.white));
        }else {
            mTv.getShapeDrawableBuilder().setStrokeColor(mContext.getResources().getColor(R.color.color_333333))
                    .setSolidColor(mContext.getResources().getColor(R.color.bar_transparent)).intoBackground();
            mTv.setTextColor(mContext.getResources().getColor(R.color.color_333333));
        }
    }
}
