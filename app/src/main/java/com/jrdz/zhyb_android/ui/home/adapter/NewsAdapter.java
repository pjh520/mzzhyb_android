package com.jrdz.zhyb_android.ui.home.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.DateUtil;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.NotifyModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/26
 * 描    述：
 * ================================================
 */
public class NewsAdapter extends BaseQuickAdapter<NotifyModel.DataBean, BaseViewHolder> {
    public NewsAdapter() {
        super(R.layout.layout_news_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, NotifyModel.DataBean resultObjBean) {
        baseViewHolder.setText(R.id.tv_title, EmptyUtils.strEmpty(resultObjBean.getTitle()));
        baseViewHolder.setText(R.id.tv_time, DateUtil.converToStandardTime(resultObjBean.getCreateDT(),"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"));
    }
}
