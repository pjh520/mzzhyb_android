package com.jrdz.zhyb_android.ui.home.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.QueryComFunctModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/20
 * 描    述：
 * ================================================
 */
public class QueryComFunctAdapter extends BaseQuickAdapter<QueryComFunctModel, BaseViewHolder> {
    public QueryComFunctAdapter() {
        super(R.layout.layout_query_comfunct_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, QueryComFunctModel queryComFunctModel) {
        ImageView ivIcon=baseViewHolder.getView(R.id.iv_icon);
        ivIcon.setImageResource(queryComFunctModel.getImg());
        baseViewHolder.setText(R.id.tv_text, EmptyUtils.strEmpty(queryComFunctModel.getText()));
    }
}
