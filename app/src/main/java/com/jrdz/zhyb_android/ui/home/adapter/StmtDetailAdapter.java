package com.jrdz.zhyb_android.ui.home.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.StmtDetailModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/28
 * 描    述：
 * ================================================
 */
public class StmtDetailAdapter extends BaseQuickAdapter<StmtDetailModel.DataBean, BaseViewHolder> {
    public StmtDetailAdapter() {
        super(R.layout.layout_stmtdetail_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, StmtDetailModel.DataBean resultObjBean) {
        baseViewHolder.setText(R.id.tv_setl_id, "结算编码："+resultObjBean.getSetl_id());
        baseViewHolder.setText(R.id.tv_fixmedins_refd_setl_flag, "退费结算标志："+resultObjBean.getRefd_setl_flag_name());
        baseViewHolder.setText(R.id.tv_stmt_rslt, "对账结果："+(EmptyUtils.isEmpty(resultObjBean.getStmt_rslt_name())?resultObjBean.getStmt_rslt():resultObjBean.getStmt_rslt_name()));
        baseViewHolder.setText(R.id.tv_memo, "备注："+resultObjBean.getMemo());
    }
}
