package com.jrdz.zhyb_android.ui.home.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.DateUtil;
import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.hjq.shape.view.ShapeView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.StmtInfoListModel;
import com.jrdz.zhyb_android.ui.home.model.StmtTotalListModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/28
 * 描    述：
 * ================================================
 */
public class StmtInfoListAdapter extends BaseQuickAdapter<StmtTotalListModel.DataBean, BaseViewHolder> {
    public StmtInfoListAdapter() {
        super(R.layout.layout_stmtinfolist_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.tv_sub_ledger);
        baseViewHolder.addOnClickListener(R.id.tv_view_sub_ledger);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, StmtTotalListModel.DataBean resultObjBean) {
        TextView tvStatus=baseViewHolder.getView(R.id.tv_status);
        TextView tvRemarks=baseViewHolder.getView(R.id.tv_remarks);
        ShapeView s_line=baseViewHolder.getView(R.id.s_line);
        LinearLayout ll_btn=baseViewHolder.getView(R.id.ll_btn);

        baseViewHolder.setText(R.id.tv_reconcil_time, "对账时间:"+ resultObjBean.getCreateDT());
        baseViewHolder.setText(R.id.tv_status, EmptyUtils.strEmpty(resultObjBean.getStmt_rslt_name()));
        baseViewHolder.setText(R.id.tv_begin_time, "开始日期:"+DateUtil.TimeStamp2Date2(String.valueOf(DateUtil.dateToStamp2(resultObjBean.getStmt_begndate(),"yyyy-MM-dd HH:mm:ss")),"yyyy-MM-dd"));
        baseViewHolder.setText(R.id.tv_end_time, "结束日期:"+DateUtil.TimeStamp2Date2(String.valueOf(DateUtil.dateToStamp2(resultObjBean.getStmt_enddate(),"yyyy-MM-dd HH:mm:ss")),"yyyy-MM-dd"));
        baseViewHolder.setText(R.id.tv_insutype, "险种类型:"+resultObjBean.getInsutype_name());
        baseViewHolder.setText(R.id.tv_clr_type, "清算类别:"+resultObjBean.getClr_type_name());
        baseViewHolder.setText(R.id.tv_medfee_sumamt, "医疗费总额:"+resultObjBean.getMedfee_sumamt());
        baseViewHolder.setText(R.id.tv_fund_pay_sumamt, "基金支付总额:"+resultObjBean.getFund_pay_sumamt());
        baseViewHolder.setText(R.id.tv_acct_pay, "个人账户支付:"+resultObjBean.getAcct_pay());
        baseViewHolder.setText(R.id.tv_sett_num, "结算笔数:"+resultObjBean.getFixmedins_setl_cnt());
        baseViewHolder.setText(R.id.tv_remarks, "备注:"+resultObjBean.getStmt_rslt_dscr());

        if ("0".equals(resultObjBean.getStmt_rslt())){//平
            s_line.setVisibility(View.GONE);
            ll_btn.setVisibility(View.GONE);
            tvRemarks.setVisibility(View.GONE);
            tvStatus.setTextColor(mContext.getResources().getColor(R.color.color_4870e0));
        }else {//不平
            s_line.setVisibility(View.VISIBLE);
            ll_btn.setVisibility(View.VISIBLE);
            tvRemarks.setVisibility(View.VISIBLE);
            tvStatus.setTextColor(mContext.getResources().getColor(R.color.color_ff0202));
        }
    }
}
