package com.jrdz.zhyb_android.ui.home.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.StmtQueryResultModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/1/28
 * 描    述：
 * ================================================
 */
public class StmtQueryResultAdapter extends BaseQuickAdapter<StmtQueryResultModel.DataBean, BaseViewHolder> {

    public StmtQueryResultAdapter() {
        super(R.layout.layout_stmtquery_result_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.fl_cb);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, StmtQueryResultModel.DataBean item) {
        CheckBox cb=baseViewHolder.getView(R.id.cb);
        baseViewHolder.setText(R.id.tv_insutype, "险种类型:"+item.getInsutype_name());
        baseViewHolder.setText(R.id.tv_clr_type, "清算类别:"+item.getClr_type_name());
        baseViewHolder.setText(R.id.tv_medfee_sumamt, "医疗费总额:"+item.getMedfee_sumamt());
        baseViewHolder.setText(R.id.tv_fund_pay_sumamt, "基金支付总额:"+item.getFund_pay_sumamt());
        baseViewHolder.setText(R.id.tv_acct_pay, "个人账户支付:"+item.getAcct_pay());
        baseViewHolder.setText(R.id.tv_sett_num, "结算笔数:"+item.getFixmedins_setl_cnt());
        cb.setChecked(item.choose?true:false);
    }
}
