package com.jrdz.zhyb_android.ui.home.adapter;

import androidx.annotation.NonNull;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.TreatmentModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/28
 * 描    述：
 * ================================================
 */
public class TreatmentAdapter extends BaseQuickAdapter<TreatmentModel.DataBean, BaseViewHolder> {
    public TreatmentAdapter() {
        super(R.layout.layout_treatment_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, TreatmentModel.DataBean trtinfoBean) {
        baseViewHolder.setText(R.id.tv_fund_pay_type, EmptyUtils.isEmpty(trtinfoBean.getFund_pay_type_name())?
                trtinfoBean.getFund_pay_type():trtinfoBean.getFund_pay_type_name());
        baseViewHolder.setText(R.id.tv_begndate, EmptyUtils.strEmpty(trtinfoBean.getBegndate()));
        baseViewHolder.setText(R.id.tv_trt_enjymnt_flag, EmptyUtils.isEmpty(trtinfoBean.getTrt_enjymnt_flag_name())?
                trtinfoBean.getTrt_enjymnt_flag():trtinfoBean.getTrt_enjymnt_flag_name());
    }
}
