package com.jrdz.zhyb_android.ui.home.fragment;

import static android.app.Activity.RESULT_OK;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.ColorUtils;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.IsInstallUtils;
import com.frame.compiler.widget.CustRefreshLayout;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.banner.listener.OnBannerClickListener;
import com.frame.compiler.widget.banner.manager.GlideAppSimpleImageManager_round;
import com.frame.compiler.widget.banner.widget.BannerLayout;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.marqueeView.XMarqueeView;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.window.EasyWindow;
import com.hjq.window.draggable.SpringDraggable;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.base.MyWebViewActivity_html;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.database.CatalogueModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.activity.CatalogueManageActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.DiseCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.SelectCataManageActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.UpdateWestCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.UpdateWestEnaCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.model.WestCataListModel;
import com.jrdz.zhyb_android.ui.home.activity.BindRelatActivity;
import com.jrdz.zhyb_android.ui.home.activity.DataVersionActivity;
import com.jrdz.zhyb_android.ui.home.activity.DepartmentManageActivity;
import com.jrdz.zhyb_android.ui.home.activity.DoctorManageActivity;
import com.jrdz.zhyb_android.ui.home.activity.LargeVersionActivity;
import com.jrdz.zhyb_android.ui.home.activity.MainActivity;
import com.jrdz.zhyb_android.ui.home.activity.MonthSettDeclarListActivity;
import com.jrdz.zhyb_android.ui.home.activity.MonthSettQueryActivity;
import com.jrdz.zhyb_android.ui.home.activity.NewsDetailActivity;
import com.jrdz.zhyb_android.ui.home.activity.QuestionnaireActivity;
import com.jrdz.zhyb_android.ui.home.activity.ScanCataListActivtiy;
import com.jrdz.zhyb_android.ui.home.activity.ScanPaySelectDrugActivity;
import com.jrdz.zhyb_android.ui.home.activity.StmtInfoListActvity;
import com.jrdz.zhyb_android.ui.home.adapter.HomeMarqAdapter;
import com.jrdz.zhyb_android.ui.home.adapter.HomeSortAdapter;
import com.jrdz.zhyb_android.ui.home.adapter.QueryComFunctAdapter;
import com.jrdz.zhyb_android.ui.home.model.BannerDataModel;
import com.jrdz.zhyb_android.ui.home.model.HomeSortModel;
import com.jrdz.zhyb_android.ui.home.model.NotifyModel;
import com.jrdz.zhyb_android.ui.home.model.QueryComFunctModel;
import com.jrdz.zhyb_android.ui.home.model.QuestionnairePopModel;
import com.jrdz.zhyb_android.ui.home.model.ScanOrganModel;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredMainActivity;
import com.jrdz.zhyb_android.ui.insured.model.EpaymentUrlModel;
import com.jrdz.zhyb_android.ui.mine.activity.HwScanActivity;
import com.jrdz.zhyb_android.ui.mine.activity.ZbarScanActivity;
import com.jrdz.zhyb_android.ui.settlement.activity.OutpatLogListActivtiy;
import com.jrdz.zhyb_android.ui.settlement.activity.OutpatRegListActivtiy;
import com.jrdz.zhyb_android.ui.settlement.activity.OutpatSettleRecordActivtiy;
import com.jrdz.zhyb_android.ui.settlement.activity.PharmacySettleRecordActivtiy;
import com.jrdz.zhyb_android.ui.settlement.activity.PrescrActivity;
import com.jrdz.zhyb_android.ui.settlement.activity.QueryPersonalInfoActivity;
import com.jrdz.zhyb_android.ui.settlement.activity.SettleTypeActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.SettleInApplyActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.SettleInApplyResultActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.SmartPhaMainActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.QueryDoctorActivationStatusModel;
import com.jrdz.zhyb_android.utils.AppManager_Acivity;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.widget.CropScreen.ScreenShotActivity;
import com.jrdz.zhyb_android.widget.pop.QuestionnairePop;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.constant.RefreshState;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/08
 * 描    述：首页
 * ================================================
 */
public class HomeFragment extends HomePermissionFragment implements OnBannerClickListener, BaseQuickAdapter.OnItemClickListener, HomeMarqAdapter.IOptionListener, OnRefreshListener {
    protected CustRefreshLayout mRefreshLayout;
    private NestedScrollView mScrollview;
    private ImageView mIvHead;
    private TextView mTvUserName, mTvFixmedinsName;
    private FrameLayout mFlBackInsured;
    private ImageView mIvFixmedinsType;
    private BannerLayout mBanner;
    private LinearLayout mLlHead, mLlZhyf, mLlScanMedinsu, mLlLargeVersion, mLlZfb,mLlSscanPay;
    private ImageView mLlSettlement, mLlSettlement02, mLlOutpatientRecords, mRlCatamanage, mRlSettrecord;
    private CustomeRecyclerView mRlSort, mRlQueryComFunct, mRlQueryOtherComfunct;
    private XMarqueeView marquee;

    private int bannerHeight;
    private HomeSortAdapter homeSortAdapter;
    private QueryComFunctAdapter queryComFunctAdapter, queryOtherComFunctAdapter;
    private HomeMarqAdapter homeMarqAdapter;
    private int requestTag = 0;
    public String urlText01 = "alipays://platformapi/startapp?appId=2021001123625885&page=pages%2findex%2findex%3fprovideId%3d2088241201533517%26chInfo%3dXSyibaochaxunqianzhi%26";
    public String urlText02 = "https://ds.alipay.com/?scheme=alipays://platformapi/startapp?appId=2021001123625885&page=pages%2findex%2findex%3fprovideId%3d2088241201533517%26chInfo%3dXSyibaochaxunqianzhi%26returnUrl%3dhttps%253a%252f%252fwww.alipay.com";

    private QuestionnairePop questionnairePop;
//    private PermissionHelper permissionHelper;
//    private ArrayList<String> permiss;

    //登录成功信息
    private ObserverWrapper<Integer> mLoginObserve = new ObserverWrapper<Integer>() {
        @Override
        public void onChanged(@Nullable Integer value) {
            //设置机构、用户信息
            if (mTvUserName != null && mTvFixmedinsName != null && mRlSort != null && mIvHead != null && homeSortAdapter != null) {
                GlideUtils.loadImg(LoginUtils.getThrAccessoryUrl(), mIvHead, R.drawable.ic_insured_head, new CircleCrop());
                mTvUserName.setText(EmptyUtils.strEmpty(LoginUtils.getUserName()));
                mTvFixmedinsName.setText(EmptyUtils.strEmpty(MechanismInfoUtils.getFixmedinsName()));
                //设置轮播图下面三张分类
                setDiffTypeShowData();

                //设置推荐页面的分类
                ArrayList<HomeSortModel> homeSortModels;
                if (LoginUtils.isManage()) {
                    homeSortModels = CommonlyUsedDataUtils.getInstance().getHomeSort_Manage(MechanismInfoUtils.getFixmedinsType());
                } else {
                    homeSortModels = CommonlyUsedDataUtils.getInstance().getHomeSort_Physician(MechanismInfoUtils.getFixmedinsType());
                }

                if (homeSortAdapter!=null){
                    homeSortAdapter.setNewData(homeSortModels);
                }
                goNext();
                onRefresh(mRefreshLayout);
            }
        }
    };
    //后台切换前台监听
    private ObserverWrapper<Boolean> mBackgroundObserve = new ObserverWrapper<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean value) {
            if (value) {
                showWaitDialog();
                onRefresh(mRefreshLayout);
            }
        }
    };

    private ObserverWrapper<String> mUpdateManaObserve = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            switch (value) {
                case "2"://更新用户头像
                    GlideUtils.loadImg(LoginUtils.getThrAccessoryUrl(), mIvHead, R.drawable.ic_insured_head, new CircleCrop());
                    break;
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mRefreshLayout = view.findViewById(R.id.refreshLayout);
        mScrollview = view.findViewById(R.id.scrollview);
        mFlBackInsured = view.findViewById(R.id.fl_back_insured);
        mLlHead = view.findViewById(R.id.ll_head);
        mIvHead = view.findViewById(R.id.iv_head);
        mTvUserName = view.findViewById(R.id.tv_user_name);
        mIvFixmedinsType = view.findViewById(R.id.iv_fixmedins_type);
        mTvFixmedinsName = view.findViewById(R.id.tv_fixmedins_name);
        mBanner = view.findViewById(R.id.banner);
        mLlSettlement = view.findViewById(R.id.iv_settlement);
        mLlSettlement02 = view.findViewById(R.id.iv_settlement02);
        mLlOutpatientRecords = view.findViewById(R.id.iv_outpatient_records);
        mRlCatamanage = view.findViewById(R.id.iv_catamanage);
        mRlSettrecord = view.findViewById(R.id.iv_settrecord);
        mRlSort = view.findViewById(R.id.rl_sort);
        marquee = view.findViewById(R.id.marquee);
        mRlQueryComFunct = view.findViewById(R.id.rl_query_com_funct);
        mRlQueryOtherComfunct = view.findViewById(R.id.rl_query_other_comfunct);
        mLlZhyf = view.findViewById(R.id.ll_zhyf);
        mLlScanMedinsu = view.findViewById(R.id.ll_scan_medinsu);
        mLlLargeVersion = view.findViewById(R.id.ll_large_version);
        mLlZfb = view.findViewById(R.id.ll_zfb);
        mLlSscanPay = view.findViewById(R.id.ll_scan_pay);

        hideLeftView();
        ImmersionBar.setTitleBar(this, mTitleBar);
        mTitleBar.setLineVisible(false);
        mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                , ContextCompat.getColor(getContext(), R.color.colorAccent), 0));
        mTitleBar.setTitleColor(getResources().getColor(R.color.white));
        mTitleBar.setTitle("");
    }

    @Override
    public void initData() {
        super.initData();
        MsgBus.sendLoginStatus().observe(this, mLoginObserve);
        MsgBus.sendIsBackground().observe(this, mBackgroundObserve);
        MsgBus.updateManaInfo().observe(this, mUpdateManaObserve);
        //设置下拉刷新
        setRefreshInfo();
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setEnableLoadMore(false);

        bannerHeight = getResources().getDimensionPixelOffset(R.dimen.dp_232);
        //设置机构、用户信息
        GlideUtils.loadImg(LoginUtils.getThrAccessoryUrl(), mIvHead, R.drawable.ic_insured_head, new CircleCrop());
        mTvUserName.setText(EmptyUtils.strEmpty(LoginUtils.getUserName()));
        mTvFixmedinsName.setText(EmptyUtils.strEmpty(MechanismInfoUtils.getFixmedinsName()));
        //设置轮播图下面三张分类
        setDiffTypeShowData();
        //设置推荐页面的分类
        ArrayList<HomeSortModel> homeSortModels;
        if (LoginUtils.isManage()) {
            homeSortModels = CommonlyUsedDataUtils.getInstance().getHomeSort_Manage(MechanismInfoUtils.getFixmedinsType());
        } else {
            homeSortModels = CommonlyUsedDataUtils.getInstance().getHomeSort_Physician(MechanismInfoUtils.getFixmedinsType());
        }
        mRlSort.setHasFixedSize(true);
        mRlSort.setLayoutManager(new GridLayoutManager(getContext(), 5, GridLayoutManager.VERTICAL, false));
        homeSortAdapter = new HomeSortAdapter();
        mRlSort.setAdapter(homeSortAdapter);
        homeSortAdapter.setNewData(homeSortModels);
        //初始化资讯轮播
        homeMarqAdapter = new HomeMarqAdapter(this);
        marquee.setAdapter(homeMarqAdapter);

        //常用功能查询
        mRlQueryComFunct.setHasFixedSize(true);
        mRlQueryComFunct.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
        queryComFunctAdapter = new QueryComFunctAdapter();
        mRlQueryComFunct.setAdapter(queryComFunctAdapter);
        queryComFunctAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getHomeCommFunct());
        //其他功能查询
        mRlQueryOtherComfunct.setHasFixedSize(true);
        mRlQueryOtherComfunct.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
        queryOtherComFunctAdapter = new QueryComFunctAdapter();
        mRlQueryOtherComfunct.setAdapter(queryOtherComFunctAdapter);
        queryOtherComFunctAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getHomeOtherCommFunct());

        //获取首页跑马灯资讯
        showWaitDialog();
        getBannerData();
        getHomeMarqData();
    }

    //不同类型 展示不同
    private void setDiffTypeShowData() {
        if ("1".equals(MechanismInfoUtils.getFixmedinsType())) {//医院  门诊结算
            mLlSettlement.setVisibility(View.GONE);
            mLlSettlement02.setVisibility(View.VISIBLE);
            mLlOutpatientRecords.setVisibility(View.VISIBLE);
        } else {//药店结算
            mLlSettlement.setVisibility(View.VISIBLE);
            mLlSettlement02.setVisibility(View.GONE);
            mLlOutpatientRecords.setVisibility(View.GONE);
        }

        //只有怡康 可以扫码直付
        if ("1".equals(MechanismInfoUtils.getIsOTO())){
            mLlSscanPay.setVisibility(View.VISIBLE);
        }else {
            mLlSscanPay.setVisibility(View.GONE);
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mFlBackInsured.setOnClickListener(this);
        mLlHead.setOnClickListener(this);
        mLlSettlement.setOnClickListener(this);
        mLlSettlement02.setOnClickListener(this);
        mLlOutpatientRecords.setOnClickListener(this);
        mRlCatamanage.setOnClickListener(this);
        mRlSettrecord.setOnClickListener(this);
        mLlZhyf.setOnClickListener(this);
        mLlScanMedinsu.setOnClickListener(this);
        mLlLargeVersion.setOnClickListener(this);
        mLlZfb.setOnClickListener(this);
        mLlSscanPay.setOnClickListener(this);
        homeSortAdapter.setOnItemClickListener(this);
        queryComFunctAdapter.setOnItemClickListener(mOnFunctListener);
        queryOtherComFunctAdapter.setOnItemClickListener(mOnFunctListener);

        mScrollview.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY <= 0) {
                    mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                            , ContextCompat.getColor(getContext(), R.color.colorAccent), 0));
                    mTitleBar.setTitle("");
                } else if (scrollY <= bannerHeight) {
                    float alpha = (float) scrollY / bannerHeight;
                    mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                            , ContextCompat.getColor(getContext(), R.color.colorAccent), alpha));
                    mTitleBar.setTitle("");
                } else {
                    mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                            , ContextCompat.getColor(getContext(), R.color.colorAccent), 1));
                    mTitleBar.setTitle("米脂智慧医保");
                }
            }
        });
    }

    //设置SmartRefreshLayout的刷新 加载样式
    public void setRefreshInfo() {
        mRefreshLayout.setPrimaryColorsId(R.color.windowbackground, R.color.txt_color_666);
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
        mRefreshLayout.setHeaderMaxDragRate(5);//最大显示下拉高度/Header标准高度  头部下拉高度的比例
    }

    //获取轮播图数据
    private void getBannerData() {
        BannerDataModel.sendBannerDataRequest(TAG, new CustomerJsonCallBack<BannerDataModel>() {
            @Override
            public void onRequestError(BannerDataModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BannerDataModel returnData) {
                dissWaitDailog();
                List<BannerDataModel.DataBean> bannerDatas = returnData.getData();
                if (bannerDatas != null&&!bannerDatas.isEmpty()) {
                    //测试替换图片标记 为了给米脂先看效果图 直接替换图片
                    for (BannerDataModel.DataBean bannerData : bannerDatas) {
                        bannerData.setImgurl("https://up.enterdesk.com/edpic/cc/4c/f9/cc4cf93461658f9caf395bd244170b54.jpg");
                    }
                    if (bannerDatas.size() == 1) {
                        mBanner.initTips(false, false, false)
                                .setImageLoaderManager(new GlideAppSimpleImageManager_round())
                                .setPageNumViewMargin(12, 12, 12, 12)
                                .setViewPagerTouchMode(true)
                                .initListResources(bannerDatas)
                                .switchBanner(false)
                                .setOnBannerClickListener(HomeFragment.this);
                    } else {
                        mBanner.initTips(false, true, false)
                                .setImageLoaderManager(new GlideAppSimpleImageManager_round())
                                .setPageNumViewMargin(12, 12, 12, 12)
                                .setViewPagerTouchMode(false)
                                .initListResources(bannerDatas)
                                .switchBanner(true)
                                .setOnBannerClickListener(HomeFragment.this);
                    }
                }
            }
        });
    }

    //获取首页跑马灯资讯
    private void getHomeMarqData() {
        NotifyModel.sendNotifyRequest(TAG, "0", "6", new CustomerJsonCallBack<NotifyModel>() {
            @Override
            public void onRequestError(NotifyModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(NotifyModel returnData) {
                dissWaitDailog();
                if (returnData.getData() != null) {
                    List<NotifyModel.DataBean> infos = returnData.getData();
                    if (infos.size() == 1) {
                        marquee.setItemCount(1);
                    } else if (infos.size() > 1) {
                        marquee.setItemCount(2);
                    }
                    homeMarqAdapter.setData(infos);
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_back_insured://返回参保人首页
                if (!AppManager_Acivity.getInstance().hasActivity(InsuredMainActivity.class)) {
                    InsuredMainActivity.newIntance(getContext(), 0);
                }
                getActivity().finish();
                break;
            case R.id.ll_head://跳转我的页面
                ((MainActivity) getActivity()).setCurrentTab(2);
                break;
            case R.id.iv_settlement://药店结算
                if (LoginUtils.isManage()) {
                    showShortToast("请登录药师账户，进行该业务操作");
                } else {
                    QueryPersonalInfoActivity.newIntance(getContext(), "2");
                }
                break;
            case R.id.iv_settlement02://门诊结算
                if (LoginUtils.isManage()) {
                    showShortToast("请登录医师账户，进行该业务操作");
                } else {
                    SettleTypeActivity.newIntance(getContext());
                }
                break;
            case R.id.iv_outpatient_records://门诊记录
                if (LoginUtils.isManage()) {
                    if ("1".equals(MechanismInfoUtils.getFixmedinsType())) {//医院
                        showShortToast("请登录医师账户，进行该业务操作");
                    }else {
                        showShortToast("请登录药师账户，进行该业务操作");
                    }
                } else {
                    OutpatRegListActivtiy.newIntance(getContext());
                }
                break;
            case R.id.iv_catamanage://目录管理
                SelectCataManageActivity.newIntance(getContext(), "1");
                break;
            case R.id.iv_settrecord://门诊/药店结算记录
                if (LoginUtils.isManage()) {
                    if ("1".equals(MechanismInfoUtils.getFixmedinsType())) {//医院
                        showShortToast("请登录医师账户，进行该业务操作");
                    }else {
                        showShortToast("请登录药师账户，进行该业务操作");
                    }
                } else {
                    if ("1".equals(MechanismInfoUtils.getFixmedinsType())) {
                        OutpatSettleRecordActivtiy.newIntance(getContext());
                    } else {
                        PharmacySettleRecordActivtiy.newIntance(getContext());
                    }
                }
                break;
            case R.id.ll_zhyf://智慧药房
                // 2022-10-27  首先先判断用户类型 类型（1、管理员认证2、医师认证3、其他人员认证）
                //《1》是管理员身份 判断是否已经审核通过 1.未审核通过 点击进入管理员入驻申请页面 2.审核通过 进入智慧药房首页
                //《2》是医师身份 判断是否已经审核通过 1.未审核通过 点击进入医师入驻申请页面 2.审核通过 进入智慧药房首页
                //《3》是其他人员身份 判断是否已经审核通过 1.未审核通过 点击进入其他人员入驻申请页面 2.审核通过 进入智慧药房首页
                switch (LoginUtils.getType()) {
                    case "1"://管理员身份
                        //判断是否已经点击“立即登录”或者“立即支付”
                        if ("5".equals(MechanismInfoUtils.getApproveStatus())) {//审核通过 并且已经点击“立即登录”或者“立即支付” 直接进入智慧药房首页
                            SmartPhaMainActivity.newIntance(getContext(), 0);
                        } else {//未点击
                            //判断是否有入驻申请
                            if ("0".equals(MechanismInfoUtils.getApproveStatus())) {//还没入驻申请 进入入驻申请 页面
                                SettleInApplyActivity.newIntance(getContext());
                            } else {//已经有入驻申请
                                queryDoctorActivationStatus("1");
                            }
                        }
                        break;
                    case "2"://医师身份
                        //首先判断机构的是否审核通过
                        if (MechanismInfoUtils.isApplySuccess()) {//机构审核通过
                            //判断是否已经审核通过
                            if ("5".equals(LoginUtils.getApproveStatus())) {//审核通过 并且已经点击“立即登录” 直接进入智慧药房首页
                                SmartPhaMainActivity.newIntance(getContext(), 0);
                            } else {//未审核通过
                                queryDoctorActivationStatus("2");
                            }
                        } else {
                            showShortToast("管理员入驻申请审核通过后其他人员才能注册申请");
                        }
                        break;
                    default://其他人员身份
                        //首先判断机构的是否审核通过
                        if (MechanismInfoUtils.isApplySuccess()) {//机构审核通过
                            //判断是否已经审核通过
                            if ("5".equals(LoginUtils.getApproveStatus())) {//审核通过  直接进入智慧药房首页
                                SmartPhaMainActivity.newIntance(getContext(), 0);
                            } else {//未审核通过
                                queryDoctorActivationStatus("3");
                            }
                        } else {
                            showShortToast("管理员入驻申请审核通过后其他人员才能注册申请");
                        }
                        break;
                }
                break;
            case R.id.ll_scan_medinsu://扫医保
                if (Constants.Configure.IS_POS) {
                    goPosScan();
                } else {
                    goHwScan();
                }
                break;
            case R.id.ll_large_version://大字版
                LargeVersionActivity.newIntance(getContext());
                break;
            case R.id.ll_zfb://跳转支付宝 电子凭证
                if (IsInstallUtils.getInstance().checkAliPayInstalled(getContext())) {
                    Intent intent = new Intent();
                    intent.setData(Uri.parse(urlText01));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    showShortToast("请安装支付宝软件");

                    //如果是pos机 跳转pos机自带的应用市场
                    if (Constants.Configure.IS_POS) {
                        try {
                            String packageName = "com.centerm.cpay.applicationshop";
                            String className = "com.centerm.cpay.applicationshop.activity.AppDetailActivity";
                            Intent intent = new Intent();
                            //这里改成你的应用包名
                            intent.putExtra("packageName", "com.eg.android.AlipayGphone");
                            ComponentName componentName = new ComponentName(packageName, className);
                            intent.setComponent(componentName);
                            startActivity(intent);
                        }catch (Exception e){
                            showShortToast("未发现pos机应用市场");
                        }

                    } else {
                        Intent intent = new Intent();
                        intent.setAction("android.intent.action.VIEW");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        Uri url = Uri.parse(urlText02);
                        intent.setData(url);
                        startActivity(intent);
                    }
                }
                break;
            case R.id.ll_scan_pay://扫码支付
                ScanPaySelectDrugActivity.newIntance(getContext());
                break;
        }
    }

    //查询人员认证状态
    private void queryDoctorActivationStatus(String type) {
        showWaitDialog();
        QueryDoctorActivationStatusModel.sendQueryDoctorActivationStatusRequest(TAG, new CustomerJsonCallBack<QueryDoctorActivationStatusModel>() {
            @Override
            public void onRequestError(QueryDoctorActivationStatusModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(QueryDoctorActivationStatusModel returnData) {
                hideWaitDialog();
                QueryDoctorActivationStatusModel.DataBean data = returnData.getData();

                if ("5".equals(data.getApproveStatus())) {//审核通过 直接进入智慧药房首页
                    if ("1".equals(type)){
                        MechanismInfoUtils.setApproveStatus(data.getApproveStatus());
                    }else {
                        LoginUtils.setApproveStatus(data.getApproveStatus());
                    }
                    SmartPhaMainActivity.newIntance(getContext(), 0);
                } else {
                    if ("1".equals(type)){
                        SettleInApplyResultActivity.newIntance(getContext());
                    }else {
                        showShortToast("请开通在线购药");
                    }
                }
            }
        });
    }

    //使用pos机自带的扫码
    private void goPosScan() {
        startActivityForResult(new Intent(getContext(), ZbarScanActivity.class), new OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == -1) {
                    String scanResult = data.getStringExtra("scanResult");
                    showWaitDialog();
                    scanOrgan(scanResult);
                }
            }
        });
    }

    //手机端使用华为扫码
    private void goHwScan() {
        startActivityForResult(new Intent(getContext(), HwScanActivity.class), new OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == -1) {
                    String scanResult = data.getStringExtra("scanResult");
                    showWaitDialog();
                    scanOrgan(scanResult);
                }
            }
        });
    }

    //扫机构
    private void scanOrgan(String barCode) {
        ScanOrganModel.sendScanOrganRequest(TAG, barCode, new CustomerJsonCallBack<ScanOrganModel>() {
            @Override
            public void onRequestError(ScanOrganModel returnData, String msg) {
                hideWaitDialog();
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(ScanOrganModel returnData) {
                hideWaitDialog();
                String type = returnData.getType();
                if ("1".equals(type)) {//机构目录
                    ArrayList<CatalogueModel> data1 = returnData.getData1();
                    if (data1 != null && !data1.isEmpty()) {
                        if (data1.size() == 1) {
                            UpdateWestEnaCataActivity.newIntance(getContext(), data1.get(0));
                        } else {
                            ScanCataListActivtiy.newIntance(getContext(), "101", "1", data1);
                        }
                    }
                } else if ("2".equals(type)) {//医保目录
                    List<WestCataListModel.DataBean> data2 = returnData.getData2();
                    if (data2 != null && !data2.isEmpty()) {
                        WestCataListModel.DataBean dataBean = data2.get(0);
                        UpdateWestCataActivity.newIntance(getContext(), dataBean.getListType(), dataBean);
                    }
                }
            }
        });
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            return;
        }
        HomeSortModel sortItem = ((HomeSortAdapter) baseQuickAdapter).getItem(i);
        switch (sortItem.getId()) {
            case "1001"://绑定关系
                BindRelatActivity.newIntance(getContext());
                break;
            case "1002"://科室管理
                DepartmentManageActivity.newIntance(getContext(), "1");
                break;
            case "1003"://医师管理
                DoctorManageActivity.newIntance(getContext(), "1");
                break;
            case "1004"://电子处方
                PrescrActivity.newIntance(getContext());
                break;
            case "1005"://门诊日志
                OutpatLogListActivtiy.newIntance(getContext());
                break;
            case "1006"://月结查询
                MonthSettQueryActivity.newIntance(getContext());
                break;
            case "1007"://月结申报
                MonthSettDeclarListActivity.newIntance(getContext());
                break;
            case "1008"://结算对账
                StmtInfoListActvity.newIntance(getContext());
                break;
            case "1009"://数据版本
                DataVersionActivity.newIntance(getContext());
                break;
            case "1010"://城乡居民医保缴费
//                getEpaymentUrl();
                MyWebViewActivity.newIntance(getContext(), "城乡居民医保缴费", Constants.WebUrl.SXYBGF_SXSBJF_URL2, true, false);

//                if (IsInstallUtils.getInstance().checkAliPayInstalled(getContext())) {
////                    if (Constants.Configure.IS_POS){
////                        getPermissionToPos();
////                    }else {
//                        goZfb();
////                    }
//                } else {
//                    showShortToast("请安装支付宝软件");
//
//                    //如果是pos机 跳转pos机自带的应用市场
//                    if (Constants.Configure.IS_POS) {
//                        try {
//                            String packageName = "com.centerm.cpay.applicationshop";
//                            String className = "com.centerm.cpay.applicationshop.activity.AppDetailActivity";
//                            Intent intent = new Intent();
//                            //这里改成你的应用包名
//                            intent.putExtra("packageName", "com.eg.android.AlipayGphone");
//                            ComponentName componentName = new ComponentName(packageName, className);
//                            intent.setComponent(componentName);
//                            startActivity(intent);
//                        }catch (Exception e){
//                            showShortToast("未发现pos机应用市场");
//                        }
//                    } else {
//                        Intent intent = new Intent();
//                        intent.setAction("android.intent.action.VIEW");
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        Uri url = Uri.parse(urlText02);
//                        intent.setData(url);
//                        startActivity(intent);
//                    }
//                }
                break;
        }
    }

    //常用功能查询  其他功能查询
    private BaseQuickAdapter.OnItemClickListener mOnFunctListener = new BaseQuickAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                return;
            }
            QueryComFunctModel sortItem = ((QueryComFunctAdapter) adapter).getItem(position);
            switch (sortItem.getId()) {
                case "1101"://医保目录查询
                    CatalogueManageActivity.newIntance(getContext());
                    break;
                case "1102"://病种目录查询
                    DiseCataActivity.newIntance(getContext(), "1");
                    break;
                case "1103"://人员待遇查询
                    QueryPersonalInfoActivity.newIntance(getContext(), "3");
                    break;
                case "1201"://医保目录信息查询
//                    MedInsuCataQueryActivity.newIntance(getContext());
                    showShortToast("功能正在开发中...");
                    break;
                case "1202"://医疗目录与医保目录匹配信息查询
                    showShortToast("功能正在开发中...");
                    break;
                case "1203"://医药机构目录匹配信息查询
                    showShortToast("功能正在开发中...");
                    break;
                case "1204"://医保目录限价信息查询
                    showShortToast("功能正在开发中...");
                    break;
                case "1205"://医保目录先自付比例信息查询
                    showShortToast("功能正在开发中...");
                    break;
            }
        }
    };

    @Override
    public void onBannerClick(View view, int position, Object model) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            return;
        }
        if (model instanceof BannerDataModel.DataBean) {
            BannerDataModel.DataBean dataBean = (BannerDataModel.DataBean) model;
            if (dataBean != null) {
                //轮播图type 1代表纯广告 2跳外链 3跳公告        跳公告的id我放在url里面
                switch (dataBean.getTypeX()) {
                    case "1":
                        break;
                    case "2":
                        MyWebViewActivity.newIntance(getContext(), dataBean.getTitle(), dataBean.getUrlX(), true, false);
                        break;
                    case "3":
                        NewsDetailActivity.newIntance(getContext(), dataBean.getUrlX());
                        break;
                }
            }
        }
    }

    @Override
    public void onItemClick(NotifyModel.DataBean dataBean) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            return;
        }
        if (dataBean != null) {
            //1代表url  2代表内容
            switch (dataBean.getType()) {
                case "1":
                    MyWebViewActivity.newIntance(getContext(), dataBean.getTitle(), dataBean.getUrl(), true, false);
                    break;
                case "2":
                    NewsDetailActivity.newIntance(getContext(), dataBean.getNoticeId());
                    break;
                case "4":
                    QuestionnaireActivity.newIntance(getContext(), dataBean.getUrl());
                    break;
            }
        }
    }

    //获取E缴费医保缴费页面地址
    private void getEpaymentUrl() {
        showWaitDialog();
        EpaymentUrlModel.sendEpaymentUrlRequest(TAG, EmptyUtils.isEmpty(LoginUtils.getDr_phone()) ? EmptyUtils.strEmpty(LoginUtils.getIDNumber()) : LoginUtils.getDr_phone(), new CustomerJsonCallBack<EpaymentUrlModel>() {
            @Override
            public void onRequestError(EpaymentUrlModel returnData, String msg) {
                hideWaitDialog();
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(EpaymentUrlModel returnData) {
                hideWaitDialog();
                String data = returnData.getData();
                if (!EmptyUtils.isEmpty(data)){
                    MyWebViewActivity_html.newIntance(getContext(), "城乡居民医保缴费", data, true, false);
                }else {
                    showTipDialog("网页数据为空，请重新点击按钮获取");
                }
            }
        });
    }

    //pos机时 因为要开启打印机 所以需要申请后台弹框权限
    private void getPermissionToPos() {
//        if (permissionHelper==null){
//            permissionHelper = new PermissionHelper();
//        }
//        if (permiss==null||permiss.isEmpty()){
//            permiss=new ArrayList();
//            permiss.add(Permission.SYSTEM_ALERT_WINDOW);
//        }
//        permissionHelper.requestPermission(this, new PermissionHelper.onPermissionListener() {
//            @Override
//            public void onSuccess() {
//                showGlobalWindow(getActivity().getApplication());
//                goZfb();
//            }
//
//            @Override
//            public void onNoAllSuccess(String noAllSuccessText) {}
//
//            @Override
//            public void onFail(String failText) {
//                showShortToast("权限申请失败，app将不能弹出打印弹框");
//                goZfb();
//            }
//        }, permiss, null);
    }

    //跳转支付宝
    private void goZfb() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.WebUrl.ALIPAYS_SXSBJF_URL));
        startActivity(intent);
    }

    /**
     * 显示全局弹窗
     */
    public void showGlobalWindow(Application application) {
        // 传入 Application 表示这个是一个全局的 Toast
        EasyWindow.with(application)
                .setContentView(R.layout.float_window)
                .setGravity(Gravity.END | Gravity.BOTTOM)
                .setYOffset(200)
                // 设置指定的拖拽规则
                .setDraggable(new SpringDraggable(SpringDraggable.ORIENTATION_HORIZONTAL))
                .setOnClickListener(R.id.tv_float, new EasyWindow.OnClickListener<TextView>() {

                    @Override
                    public void onClick(EasyWindow<?> globalWindow, TextView view) {
                        EasyWindow.with(application)
                                .setContentView(R.layout.layout_global_window_config)
                                .setOnClickListener(R.id.tv_btn01, new EasyWindow.OnClickListener<TextView>() {

                                    @Override
                                    public void onClick(final EasyWindow<?> window, TextView view) {
                                        window.cancel();
                                    }
                                })
                                .setOnClickListener(R.id.tv_btn02, new EasyWindow.OnClickListener<TextView>() {

                                    @Override
                                    public void onClick(final EasyWindow<?> window, TextView view) {
                                        window.cancel();
                                        String imagPath = BaseGlobal.getImageDir()+System.currentTimeMillis() + "参保缴费成功截图凭证.jpg";
                                        startActivity(ScreenShotActivity.createIntent(getContext(), imagPath,0));
                                    }
                                })
                                .show();
                        // 点击后跳转到拨打电话界面
                        // Intent intent = new Intent(Intent.ACTION_DIAL);
                        // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        // toast.startActivity(intent);
                        // 安卓 10 在后台跳转 Activity 需要额外适配
                        // https://developer.android.google.cn/about/versions/10/privacy/changes#background-activity-starts
                    }
                })
                .show();
    }

    @Override
    public void goNext() {
        QuestionnairePopModel.sendIsQuestionnaireRequest(TAG, new CustomerJsonCallBack<QuestionnairePopModel>() {
            @Override
            public void onRequestError(QuestionnairePopModel returnData, String msg) {
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(QuestionnairePopModel returnData) {
                QuestionnairePopModel.DataBean dataBean =returnData.getData();
                if (dataBean != null && "1".equals(dataBean.getIsQuestionnaire())){
                    showQuestionnairePop(dataBean.getIsConstraint(),dataBean.getQuestionnaireTemplateCode());
                }
            }
        });
    }

    //展示调查问卷的弹框
    private void showQuestionnairePop(String enforce,String questionnaireCode) {
        if (questionnairePop==null){
            questionnairePop = new QuestionnairePop(getContext(), "1".equals(enforce) ? true : false,questionnaireCode, new QuestionnairePop.IOptionListener() {
                @Override
                public void onQuestionnaireAgree(String code) {
                    Intent intent=new Intent(getContext(), QuestionnaireActivity.class);
                    intent.putExtra("questionnaire", code);
                    startActivityForResult(intent, new OnActivityCallback() {
                        @Override
                        public void onActivityResult(int resultCode, @Nullable Intent data) {
                            if (resultCode==RESULT_OK&&questionnairePop!=null){
                                questionnairePop.dismiss();
                            }
                        }
                    });
                }

                @Override
                public void onQuestionnaireCancle() {}
            });
        }else {
            questionnairePop.setIsForce("1".equals(enforce) ? true : false,questionnaireCode);
        }

        questionnairePop.showPopupWindow();
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        requestTag = 0;

        getBannerData();
        getHomeMarqData();
    }

    //隐藏加载框
    public void dissWaitDailog() {
        requestTag++;
        if (requestTag >= 2) {
            hideRefreshView();
        }
    }

    //关闭刷新的view
    public void hideRefreshView() {
        if (mRefreshLayout.getState() == RefreshState.Refreshing) {
            mRefreshLayout.finishRefresh();
        } else {
            hideWaitDialog();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mBanner != null) {
            mBanner.start();
        }

        if (marquee != null && marquee.isFlippingLessCount()) {
            marquee.startFlipping();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        EasyWindow.recycleAll();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mBanner != null) {
            mBanner.stop();
        }
        if (marquee != null) {
            marquee.stopFlipping();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBanner != null) {
            mBanner.destroy();
            mBanner = null;
        }

        if (questionnairePop != null) {
            questionnairePop.onCleanListener();
            questionnairePop.dismiss();
            questionnairePop = null;
        }
    }

    public static HomeFragment newIntance() {
        HomeFragment homeFragment = new HomeFragment();
        return homeFragment;
    }
}
