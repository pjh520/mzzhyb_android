package com.jrdz.zhyb_android.ui.home.fragment;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.net.Uri;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.FileUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.lbs_library.utils.CustomLocationBean;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.QuestionnaireActivity;
import com.jrdz.zhyb_android.ui.home.model.CheckVersionUpdateModel;
import com.jrdz.zhyb_android.ui.login.activity.LoginActivity;
import com.jrdz.zhyb_android.ui.mine.activity.AboutUsActivity;
import com.jrdz.zhyb_android.ui.mine.activity.AgreementActivtiy;
import com.jrdz.zhyb_android.ui.mine.activity.HelpActivity;
import com.jrdz.zhyb_android.ui.mine.activity.SettingActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.OrderListActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.PdfViewActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.UpdateOrderActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.PhaSortMineAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortMineModel;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.InquiryInfoActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.PaySuccessActivity;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.utils.location.CoordinateUtils;
import com.jrdz.zhyb_android.utils.location.LocationTool;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/20
 * 描    述：我的页面
 * ================================================
 */
public class MineFragment extends BaseFragment {
    private FrameLayout mFlSetting;
    private ImageView mIvAccount;
    private CustomeRecyclerView mSrlOrderSort;
    private TextView mTvName, mTvPhone,mTvTypeName, mTvCacheSize, mTvVersion;
    private LinearLayout mLlCache, mLlAgreement, mLlAboutus, mLlExit, mLlHelp, mLlVersionUpdate;

    private CustomerDialogUtils customerDialogUtils;
    private PhaSortMineAdapter orderSortAdapter;//我的订单 适配器

    //登录成功信息
    private ObserverWrapper<Integer> mLoginObserve = new ObserverWrapper<Integer>() {
        @Override
        public void onChanged(@Nullable Integer value) {
            //设置机构、用户信息
            if (mTvName != null && mTvPhone != null&& mTvTypeName != null) {
                GlideUtils.loadImg(LoginUtils.getThrAccessoryUrl(), mIvAccount,R.drawable.ic_insured_head,new CircleCrop());
                mTvName.setText(LoginUtils.getUserName());
                mTvPhone.setText(StringUtils.encryptionPhone(LoginUtils.isManage() ? MechanismInfoUtils.getPhone() :LoginUtils.getDr_phone()));
                mTvTypeName.setText(LoginUtils.isManage() ? "管理员" :LoginUtils.getDrTypeName());
            }
        }
    };
    private ObserverWrapper<String> mUpdateManaObserve=new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            switch (value){
                case "2"://更新用户头像
                    GlideUtils.loadImg(LoginUtils.getThrAccessoryUrl(), mIvAccount,R.drawable.ic_insured_head,new CircleCrop());
                    break;
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_mine;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mFlSetting = view.findViewById(R.id.fl_setting);
        mIvAccount= view.findViewById(R.id.iv_account);

        mTvName = view.findViewById(R.id.tv_name);
        mTvPhone = view.findViewById(R.id.tv_phone);
        mTvTypeName = view.findViewById(R.id.tv_type_name);

        mSrlOrderSort = view.findViewById(R.id.srl_order_sort);

        mLlCache = view.findViewById(R.id.ll_cache);
        mTvCacheSize = view.findViewById(R.id.tv_cache_size);
        mLlHelp = view.findViewById(R.id.ll_help);
        mLlAgreement = view.findViewById(R.id.ll_agreement);
        mLlVersionUpdate = view.findViewById(R.id.ll_version_update);
        mTvVersion = view.findViewById(R.id.tv_version);
        mLlAboutus = view.findViewById(R.id.ll_about_us);
        mLlExit = view.findViewById(R.id.ll_exit);
    }

    @Override
    public void initData() {
        super.initData();
        MsgBus.sendLoginStatus().observe(this, mLoginObserve);
        MsgBus.updateManaInfo().observe(this, mUpdateManaObserve);

        GlideUtils.loadImg(LoginUtils.getThrAccessoryUrl(), mIvAccount,R.drawable.ic_insured_head,new CircleCrop());
        mTvName.setText(LoginUtils.getUserName());
        mTvPhone.setText(StringUtils.encryptionPhone(LoginUtils.isManage() ? MechanismInfoUtils.getPhone() :LoginUtils.getDr_phone()));
        mTvTypeName.setText(LoginUtils.isManage() ? "管理员" :LoginUtils.getDrTypeName());

        //我的订单
        mSrlOrderSort.setHasFixedSize(true);
        mSrlOrderSort.setLayoutManager(new GridLayoutManager(getContext(), 4, GridLayoutManager.VERTICAL, false));
        orderSortAdapter = new PhaSortMineAdapter();
        mSrlOrderSort.setAdapter(orderSortAdapter);
        orderSortAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getOrderSortData());

        setCacheSize();
        mTvVersion.setText(RxTool.getVersion(getContext()) + Constants.Configure.VERSIONNAME_TAG);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mFlSetting.setOnClickListener(this);
        mLlCache.setOnClickListener(this);
        mLlHelp.setOnClickListener(this);
        mLlAgreement.setOnClickListener(this);
        mLlVersionUpdate.setOnClickListener(this);
        mLlAboutus.setOnClickListener(this);
        mLlExit.setOnClickListener(this);

        //我的订单 item点击事件
        orderSortAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                PhaSortMineModel itemData = orderSortAdapter.getItem(i);
                switch (itemData.getId()){
                    case "4001"://全部
                        OrderListActivity.newIntance(getContext(),0);
                        break;
                    case "4002"://待付款
                        OrderListActivity.newIntance(getContext(),1);
                        break;
                    case "4003"://待发货
                        OrderListActivity.newIntance(getContext(),2);
                        break;
                    case "4004"://待收货
                        OrderListActivity.newIntance(getContext(),3);
                        break;
                    case "4005"://已完成
                        OrderListActivity.newIntance(getContext(),4);
                        break;
                    case "4006"://已取消
                        OrderListActivity.newIntance(getContext(),5);
                        break;
                }
            }
        });
    }

    @Override
    public void rightTitleViewClick() {
        showShortToast("设置");
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_setting://设置
                SettingActivity.newIntance(getContext());
                break;
            case R.id.ll_cache://清楚缓存
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(getContext(), "提示", "确定要清除缓存么？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        cleanCache();
                    }
                });
                break;
            case R.id.ll_help://帮助中心
                HelpActivity.newIntance(getContext());
                break;
            case R.id.ll_agreement://应用协议
                AgreementActivtiy.newIntance(getContext());
                break;
            case R.id.ll_version_update://版本更新
                //检测更新
                checkForUpdate();
                break;
            case R.id.ll_about_us://关于我们
                AboutUsActivity.newIntance(getContext());

//                String path = "/storage/emulated/0/Android/data/com.jrdz.zhyb_android/files/zhyb/门诊日志";
//                File file = new File(path);
//                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                    Uri contentUri = FileProvider.getUriForFile(RxTool.getContext(), RxTool.getContext().getPackageName()+".fileprovider", file);
//                    intent.setDataAndType(contentUri, "application/vnd.android.package-archive");
//                } else {
//                    intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                }
//
//                try {
//                    startActivity(intent);
////            startActivity(Intent.createChooser(intent,"选择浏览工具"));
//                } catch (ActivityNotFoundException e) {
//                    e.printStackTrace();
//                }

//                Uri uri = Uri.parse("content://com.android.externalstorage.documents/document/primary:Download");
//                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//                intent.addCategory(Intent.CATEGORY_OPENABLE);
//                intent.setType("*/*");
//                intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, uri);
//                startActivityForResult(intent, 1);
                break;
            case R.id.ll_exit://退出登录
                exit();
                break;
        }
    }

    //获取是否有新版本的信息
    private void checkForUpdate() {
        CheckVersionUpdateModel.sendCheckVersionUpdateRequest(TAG, new CustomerJsonCallBack<CheckVersionUpdateModel>() {
            @Override
            public void onRequestError(CheckVersionUpdateModel returnData, String msg) {
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(CheckVersionUpdateModel returnData) {
                CheckVersionUpdateModel.DataBean info = returnData.getData();
                //1.当版本号小于后台返回的版本号 那么就需要更新
                if (info != null && RxTool.getVersionCode(getContext()) < info.getInteriorVersionNum()) {
                    showUpdatePop(info.getUrl());
                } else {
                    showShortToast("已是最新版本");
                }
            }
        });
    }

    //显示更新弹框
    private void showUpdatePop(String downloadurl) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(getContext(), "提示", "发现新版本，是否更新？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                if (Constants.Configure.IS_POS) {
                    goPosMarket();
                } else {
                    goBrowser(downloadurl);
                }
            }
        });
    }

    //跳转pos机的应用市场更新app
    private void goPosMarket() {
        String packageName = "com.centerm.cpay.applicationshop";
        String className = "com.centerm.cpay.applicationshop.activity.AppDetailActivity";
        Intent intent = new Intent();
        //这里改成你的应用包名
        intent.putExtra("packageName", "com.jrdz.zhyb_android");
        ComponentName componentName = new ComponentName(packageName, className);
        intent.setComponent(componentName);
        startActivity(intent);
    }

    //跳转浏览器下载app
    private void goBrowser(String downloadurl) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(downloadurl));
        // 注意此处的判断intent.resolveActivity()可以返回显示该Intent的Activity对应的组件名
        // 官方解释 : Name of the component implementing an activity that can display the intent
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
//            final ComponentName componentName = intent.resolveActivity(getContext().getPackageManager());
            //  LogUtil.d("suyan = " + componentName.getClassName());
            startActivity(Intent.createChooser(intent, "请选择浏览器"));
        } else {
            showShortToast("链接错误或无浏览器");
        }
    }

    //设置缓存的大小
    private void setCacheSize() {
        ThreadUtils.executeBySingle(new ThreadUtils.SimpleTask<String>() {
            @Nullable
            @Override
            public String doInBackground() throws Throwable {
                long glideDirSize = FileUtils.getDirLength(BaseGlobal.getImageGlideDir());
                long compressedTempDirSize = FileUtils.getDirLength(BaseGlobal.getImageCompressedTempDir());
                long photoCacheDirDirSize = FileUtils.getDirLength(BaseGlobal.getPhotoCacheDir());
                long imageCropSize = FileUtils.getDirLength(BaseGlobal.getImageCrop());
                long audioSize = FileUtils.getDirLength(BaseGlobal.getAudioDir());
                long crashDir = FileUtils.getDirLength(BaseGlobal.getCrashDir());
                long picShotDir = FileUtils.getDirLength(BaseGlobal.getPicShotDir());
                long totalSize = glideDirSize + compressedTempDirSize + photoCacheDirDirSize + imageCropSize + audioSize + crashDir + picShotDir;
                return FileUtils.byte2FitMemorySize(totalSize);
            }

            @Override
            public void onSuccess(@Nullable String result) {
                mTvCacheSize.setText(EmptyUtils.strEmpty(result));
            }
        });
    }

    //清楚缓存
    private void cleanCache() {
        showWaitDialog();
        ThreadUtils.executeBySingle(new ThreadUtils.SimpleTask<Void>() {
            @Nullable
            @Override
            public Void doInBackground() throws Throwable {
                FileUtils.deleteAllInDir(BaseGlobal.getImageGlideDir());
                FileUtils.deleteAllInDir(BaseGlobal.getImageCompressedTempDir());
                FileUtils.deleteAllInDir(BaseGlobal.getPhotoCacheDir());
                FileUtils.deleteAllInDir(BaseGlobal.getImageCrop());
                FileUtils.deleteAllInDir(BaseGlobal.getAudioDir());
                FileUtils.deleteAllInDir(BaseGlobal.getCrashDir());
                FileUtils.deleteAllInDir(BaseGlobal.getPicShotDir());
                return null;
            }

            @Override
            public void onSuccess(@Nullable Void result) {
                hideWaitDialog();
                setCacheSize();
            }
        });
    }

    //退出登录
    private void exit() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(getContext(), "提示", "是否确认退出登录?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        showWaitDialog();
                        BaseModel.sendLogoutRequest(TAG, new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                LoginActivity.newIntance(getContext());
                            }
                        });
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isDataInitialized) {
            setCacheSize();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static MineFragment newIntance() {
        MineFragment mineFragment = new MineFragment();
        return mineFragment;
    }
}
