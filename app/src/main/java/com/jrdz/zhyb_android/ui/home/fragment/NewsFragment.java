package com.jrdz.zhyb_android.ui.home.fragment;

import android.content.Context;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.adapter.DiseCataAdapter;
import com.jrdz.zhyb_android.ui.catalogue.model.DiseCataModel;
import com.jrdz.zhyb_android.ui.catalogue.model.MedSerCataListModel;
import com.jrdz.zhyb_android.ui.home.activity.NewsDetailActivity;
import com.jrdz.zhyb_android.ui.home.activity.QuestionnaireActivity;
import com.jrdz.zhyb_android.ui.home.adapter.NewsAdapter;
import com.jrdz.zhyb_android.ui.home.model.NotifyModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/26
 * 描    述： 资讯fragment
 * ================================================
 */
public class NewsFragment extends BaseRecyclerViewFragment {

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_news;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        hideLeftView();
        ImmersionBar.setTitleBar(this, mTitleBar);
        setTitle("资讯公告");
    }

    @Override
    protected void initAdapter() {
        mAdapter=new NewsAdapter();
    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    protected void getData() {
        super.getData();
        NotifyModel.sendNotifyRequest(TAG, String.valueOf(mPageNum), "20", new CustomerJsonCallBack<NotifyModel>() {
            @Override
            public void onRequestError(NotifyModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(NotifyModel returnData) {
                hideRefreshView();
                List<NotifyModel.DataBean> infos = returnData.getData();
                if (mAdapter!=null&&infos != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(infos);
                    } else {
                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter,view,position);
        NotifyModel.DataBean itemData = ((NewsAdapter) adapter).getItem(position);
        if (itemData!=null){
            //1代表url  2代表内容
            switch (itemData.getType()){
                case "1":
                    MyWebViewActivity.newIntance(getContext(), itemData.getTitle(),itemData.getUrl(),true,false);
                    break;
                case "2":
                    NewsDetailActivity.newIntance(getContext(),itemData.getNoticeId());
                    break;
                case "4":
                    QuestionnaireActivity.newIntance(getContext(),itemData.getUrl());
                    break;
            }
        }
    }

    public static NewsFragment newIntance() {
        NewsFragment newsFragment = new NewsFragment();
        return newsFragment;
    }
}
