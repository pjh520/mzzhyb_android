package com.jrdz.zhyb_android.ui.home.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-09-14
 * 描    述：
 * ================================================
 */
public class AddQuestionnaireModel {
    private String ItemCode;
    private String ItemValue;
    private String QuestionnaireTemplateCode;



    public AddQuestionnaireModel(String itemCode, String itemValue, String questionnaireTemplateCode) {
        ItemCode = itemCode;
        ItemValue = itemValue;
        QuestionnaireTemplateCode = questionnaireTemplateCode;
    }

    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }

    public String getItemValue() {
        return ItemValue;
    }

    public void setItemValue(String itemValue) {
        ItemValue = itemValue;
    }

    public String getQuestionnaireTemplateCode() {
        return QuestionnaireTemplateCode;
    }

    public void setQuestionnaireTemplateCode(String questionnaireTemplateCode) {
        QuestionnaireTemplateCode = questionnaireTemplateCode;
    }
}
