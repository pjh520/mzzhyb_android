package com.jrdz.zhyb_android.ui.home.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-01-10
 * 描    述：
 * ================================================
 */
public class ApplyResultModel {
    private String type;//0代表删除 1代表申请  2代表注销 3代表修改手机号码
    private int pos;
    private String phone;
    private String accessoryUrl;//职业资格证图片url
    private String accessoryId;//职业资格证图片id

    public ApplyResultModel(String type, int pos) {
        this.type = type;
        this.pos = pos;
    }

    public ApplyResultModel(String type, int pos, String phone) {
        this.type = type;
        this.pos = pos;
        this.phone = phone;
    }

    public ApplyResultModel(String type, int pos, String accessoryUrl, String accessoryId) {
        this.type = type;
        this.pos = pos;
        this.accessoryUrl = accessoryUrl;
        this.accessoryId = accessoryId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAccessoryUrl() {
        return accessoryUrl;
    }

    public void setAccessoryUrl(String accessoryUrl) {
        this.accessoryUrl = accessoryUrl;
    }

    public String getAccessoryId() {
        return accessoryId;
    }

    public void setAccessoryId(String accessoryId) {
        this.accessoryId = accessoryId;
    }
}
