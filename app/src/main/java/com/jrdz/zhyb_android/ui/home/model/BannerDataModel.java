package com.jrdz.zhyb_android.ui.home.model;

import com.flyjingfish.openimagelib.beans.OpenImageUrl;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.widget.banner.listener.BannerModelCallBack;
import com.google.gson.annotations.SerializedName;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：My Application
 * 包    名：com.example.myapplication.model
 * 版    本：1.0
 * 创建日期：2021/10/11 0011
 * 描    述：
 * ================================================
 */
public class BannerDataModel{
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-02-09 18:58:50
     * data : [{"title":"医保轮播图！","url":"#","imgurl":"http://113.135.194.23:3080/banner.png","type":"1"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String timeInterval;//轮播时间间隔
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(String timeInterval) {
        this.timeInterval = timeInterval;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements BannerModelCallBack<String>, OpenImageUrl{

        /**
         * title : 医保轮播图！
         * url : #
         * imgurl : http://113.135.194.23:3080/banner.png
         * type : 1
         */

        private String title;
        @SerializedName("url")
        private String urlX;
        private String imgurl;
        @SerializedName("type")
        private String typeX;

        public DataBean(String title, String urlX, String imgurl, String typeX) {
            this.title = title;
            this.urlX = urlX;
            this.imgurl = imgurl;
            this.typeX = typeX;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrlX() {
            return urlX;
        }

        public void setUrlX(String urlX) {
            this.urlX = urlX;
        }

        public String getImgurl() {
            return imgurl;
        }

        public void setImgurl(String imgurl) {
            this.imgurl = imgurl;
        }

        @Override
        public String getBannerUrl() {
            return imgurl;
        }

        @Override
        public String getBannerTitle() {
            return null;
        }

        public String getTypeX() {
            return typeX;
        }

        /* =================== 实现大图预览     ==================== */
        @Override
        public MediaType getType() {
            return MediaType.IMAGE;
        }

        @Override
        public String getImageUrl() {
            return imgurl;
        }

        @Override
        public String getVideoUrl() {
            return "";
        }

        @Override
        public String getCoverImageUrl() {
            return imgurl;
        }
    }

    //获取智慧医保首页轮播图
    public static void sendBannerDataRequest(final String TAG,final CustomerJsonCallBack<BannerDataModel> callback) {

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL+ Constants.Api.GET_ROTATEPICTURES_URL,"", callback);
    }

    //获取参保人首页轮播图
    public static void sendInsuredBannerDataRequest(final String TAG,final CustomerJsonCallBack<BannerDataModel> callback) {

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL+ Constants.Api.GET_INSURED_ROTATEPICTURES_URL,"", callback);
    }
}
