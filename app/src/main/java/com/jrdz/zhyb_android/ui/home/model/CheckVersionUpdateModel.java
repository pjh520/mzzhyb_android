package com.jrdz.zhyb_android.ui.home.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/9
 * 描    述：
 * ================================================
 */
public class CheckVersionUpdateModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-02-09 18:58:50
     * data : [{"title":"医保轮播图！","url":"#","imgurl":"http://113.135.194.23:3080/banner.png","type":"1"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean{
        private String title;//更新标题
        private String content;//更新内容
        private String externalVersionNum;//版本号 v1.0.0这种展示用的
        private int interiorVersionNum;//数字版本 1,2,3这样 用来判断是否有新版本更新
        private String isConstraint;//是否是强制更新
        private String url;//app下载链接

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getExternalVersionNum() {
            return externalVersionNum;
        }

        public void setExternalVersionNum(String externalVersionNum) {
            this.externalVersionNum = externalVersionNum;
        }

        public int getInteriorVersionNum() {
            return interiorVersionNum;
        }

        public void setInteriorVersionNum(int interiorVersionNum) {
            this.interiorVersionNum = interiorVersionNum;
        }

        public String getIsConstraint() {
            return isConstraint;
        }

        public void setIsConstraint(String isConstraint) {
            this.isConstraint = isConstraint;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    //检查更新
    public static void sendCheckVersionUpdateRequest(final String TAG, final CustomerJsonCallBack<CheckVersionUpdateModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_APPUPDATE_URL,"", callback);
    }
}
