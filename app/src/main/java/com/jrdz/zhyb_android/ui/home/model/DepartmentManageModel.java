package com.jrdz.zhyb_android.ui.home.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/10
 * 描    述：
 * ================================================
 */
public class DepartmentManageModel {
    /**
     * code : 1
     * msg : 请求成功
     * obj : {"currentPage":1,"resultObj":[{"aprv_bed_cnt":10,"begntime":1352476800000,"caty":"A02","create_time":1639129239000,"create_user":"test","dept_estbdat":1352476800000,"dept_med_serv_scp":"全科医疗","dept_resper_name":"甄三","dept_resper_tel":"15060338988","dr_psncnt":1,"endtime":1668009600000,"hi_crtf_bed_cnt":1,"hosp_dept_codg":"01","hosp_dept_name":"全科医疗科01","id":3,"ideleted":0,"itro":"全科医疗0101010101","nurs_psncnt":1,"phar_psncnt":1,"poolarea_no":"610802","tecn_psncnt":1,"update_time":1639129239000}],"showCount":20,"totalPage":1}
     */

    private String code;
    private String msg;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * deptId : 3
         * hosp_dept_codg : 01
         * caty : A02
         * hosp_dept_name : 全科医疗科01
         * begntime : 2012-11-10T00:00:00
         * endtime : 2022-11-10T00:00:00
         * itro : 全科医疗0101010101
         * dept_resper_name : 甄三
         * dept_resper_tel : 13859005800
         * dept_med_serv_scp : 全科医疗
         * dept_estbdat : 2012-11-10
         * aprv_bed_cnt : 10
         * hi_crtf_bed_cnt : 1
         * poolarea_no : 610802
         * dr_psncnt : 1
         * phar_psncnt : 1
         * nurs_psncnt : 1
         * tecn_psncnt : 1
         * memo : 全科医疗010101010111
         */

        private String deptId;
        private String hosp_dept_codg;
        private String caty;
        private String caty_name;
        private String hosp_dept_name;
        private String begntime;
        private String endtime;
        private String itro;
        private String dept_resper_name;
        private String dept_resper_tel;
        private String dept_med_serv_scp;
        private String dept_estbdat;
        private String aprv_bed_cnt;
        private String hi_crtf_bed_cnt;
        private String poolarea_no;
        private String dr_psncnt;
        private String phar_psncnt;
        private String nurs_psncnt;
        private String tecn_psncnt;
        private String memo;

        public void setData(String deptId, String hosp_dept_codg, String caty,String caty_name, String hosp_dept_name, String begntime, String endtime,
                        String itro, String dept_resper_name, String dept_resper_tel, String dept_med_serv_scp, String dept_estbdat,
                        String aprv_bed_cnt, String hi_crtf_bed_cnt, String poolarea_no, String dr_psncnt, String phar_psncnt,
                        String nurs_psncnt, String tecn_psncnt, String memo) {
            this.deptId = deptId;
            this.hosp_dept_codg = hosp_dept_codg;
            this.caty = caty;
            this.caty_name = caty_name;
            this.hosp_dept_name = hosp_dept_name;
            this.begntime = begntime;
            this.endtime = endtime;
            this.itro = itro;
            this.dept_resper_name = dept_resper_name;
            this.dept_resper_tel = dept_resper_tel;
            this.dept_med_serv_scp = dept_med_serv_scp;
            this.dept_estbdat = dept_estbdat;
            this.aprv_bed_cnt = aprv_bed_cnt;
            this.hi_crtf_bed_cnt = hi_crtf_bed_cnt;
            this.poolarea_no = poolarea_no;
            this.dr_psncnt = dr_psncnt;
            this.phar_psncnt = phar_psncnt;
            this.nurs_psncnt = nurs_psncnt;
            this.tecn_psncnt = tecn_psncnt;
            this.memo = memo;
        }

        public String getDeptId() {
            return deptId;
        }

        public void setDeptId(String deptId) {
            this.deptId = deptId;
        }

        public String getHosp_dept_codg() {
            return hosp_dept_codg;
        }

        public void setHosp_dept_codg(String hosp_dept_codg) {
            this.hosp_dept_codg = hosp_dept_codg;
        }

        public String getCaty() {
            return caty;
        }

        public void setCaty(String caty) {
            this.caty = caty;
        }

        public String getCaty_name() {
            return caty_name;
        }

        public void setCaty_name(String caty_name) {
            this.caty_name = caty_name;
        }

        public String getHosp_dept_name() {
            return hosp_dept_name;
        }

        public void setHosp_dept_name(String hosp_dept_name) {
            this.hosp_dept_name = hosp_dept_name;
        }

        public String getBegntime() {
            return begntime;
        }

        public void setBegntime(String begntime) {
            this.begntime = begntime;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getItro() {
            return itro;
        }

        public void setItro(String itro) {
            this.itro = itro;
        }

        public String getDept_resper_name() {
            return dept_resper_name;
        }

        public void setDept_resper_name(String dept_resper_name) {
            this.dept_resper_name = dept_resper_name;
        }

        public String getDept_resper_tel() {
            return dept_resper_tel;
        }

        public void setDept_resper_tel(String dept_resper_tel) {
            this.dept_resper_tel = dept_resper_tel;
        }

        public String getDept_med_serv_scp() {
            return dept_med_serv_scp;
        }

        public void setDept_med_serv_scp(String dept_med_serv_scp) {
            this.dept_med_serv_scp = dept_med_serv_scp;
        }

        public String getDept_estbdat() {
            return dept_estbdat;
        }

        public void setDept_estbdat(String dept_estbdat) {
            this.dept_estbdat = dept_estbdat;
        }

        public String getAprv_bed_cnt() {
            return aprv_bed_cnt;
        }

        public void setAprv_bed_cnt(String aprv_bed_cnt) {
            this.aprv_bed_cnt = aprv_bed_cnt;
        }

        public String getHi_crtf_bed_cnt() {
            return hi_crtf_bed_cnt;
        }

        public void setHi_crtf_bed_cnt(String hi_crtf_bed_cnt) {
            this.hi_crtf_bed_cnt = hi_crtf_bed_cnt;
        }

        public String getPoolarea_no() {
            return poolarea_no;
        }

        public void setPoolarea_no(String poolarea_no) {
            this.poolarea_no = poolarea_no;
        }

        public String getDr_psncnt() {
            return dr_psncnt;
        }

        public void setDr_psncnt(String dr_psncnt) {
            this.dr_psncnt = dr_psncnt;
        }

        public String getPhar_psncnt() {
            return phar_psncnt;
        }

        public void setPhar_psncnt(String phar_psncnt) {
            this.phar_psncnt = phar_psncnt;
        }

        public String getNurs_psncnt() {
            return nurs_psncnt;
        }

        public void setNurs_psncnt(String nurs_psncnt) {
            this.nurs_psncnt = nurs_psncnt;
        }

        public String getTecn_psncnt() {
            return tecn_psncnt;
        }

        public void setTecn_psncnt(String tecn_psncnt) {
            this.tecn_psncnt = tecn_psncnt;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.deptId);
            dest.writeString(this.hosp_dept_codg);
            dest.writeString(this.caty);
            dest.writeString(this.caty_name);
            dest.writeString(this.hosp_dept_name);
            dest.writeString(this.begntime);
            dest.writeString(this.endtime);
            dest.writeString(this.itro);
            dest.writeString(this.dept_resper_name);
            dest.writeString(this.dept_resper_tel);
            dest.writeString(this.dept_med_serv_scp);
            dest.writeString(this.dept_estbdat);
            dest.writeString(this.aprv_bed_cnt);
            dest.writeString(this.hi_crtf_bed_cnt);
            dest.writeString(this.poolarea_no);
            dest.writeString(this.dr_psncnt);
            dest.writeString(this.phar_psncnt);
            dest.writeString(this.nurs_psncnt);
            dest.writeString(this.tecn_psncnt);
            dest.writeString(this.memo);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.deptId = in.readString();
            this.hosp_dept_codg = in.readString();
            this.caty = in.readString();
            this.caty_name = in.readString();
            this.hosp_dept_name = in.readString();
            this.begntime = in.readString();
            this.endtime = in.readString();
            this.itro = in.readString();
            this.dept_resper_name = in.readString();
            this.dept_resper_tel = in.readString();
            this.dept_med_serv_scp = in.readString();
            this.dept_estbdat = in.readString();
            this.aprv_bed_cnt = in.readString();
            this.hi_crtf_bed_cnt = in.readString();
            this.poolarea_no = in.readString();
            this.dr_psncnt = in.readString();
            this.phar_psncnt = in.readString();
            this.nurs_psncnt = in.readString();
            this.tecn_psncnt = in.readString();
            this.memo = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //科室列表
    public static void sendDepartmentManageRequest(final String TAG, String pageindex, String pagesize,
                                                   final CustomerJsonCallBack<DepartmentManageModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_GETDEPTLIST_URL, jsonObject.toJSONString(), callback);
    }
}
