package com.jrdz.zhyb_android.ui.home.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;
import com.jrdz.zhyb_android.utils.DeviceID;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/11
 * 描    述：
 * ================================================
 */
public class DoctorManageModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-01-23 02:52:13
     * data : [{"doctorId":1,"hosp_dept_codg":"","hosp_dept_name":"全科医疗科01","dr_code":"001","dr_name":"沈万三","dr_pwd":"4A7D1ED414474E4033AC29CCB8653D9B","dr_type":1,"dr_type_name":"医师"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * doctorId : 1
         * hosp_dept_codg :
         * hosp_dept_name : 全科医疗科01
         * dr_code : 001
         * dr_name : 沈万三
         * dr_pwd : 4A7D1ED414474E4033AC29CCB8653D9B
         * dr_type : 1
         * dr_type_name : 医师
         */

        private String doctorId;
        private String hosp_dept_codg;
        private String hosp_dept_name;
        private String dr_code;
        private String dr_name;
        private String dr_pwd;
        private String dr_type;
        private String type;
        private String dr_type_name;
        private String dr_phone;
        private String accessoryUrl;
        private String accessoryUrl1;
        private String accessoryId;
        private String subAccessoryId;
        private String ApproveStatus;
        private String IsEnable;

        public String getDoctorId() {
            return doctorId;
        }

        public void setDoctorId(String doctorId) {
            this.doctorId = doctorId;
        }

        public String getHosp_dept_codg() {
            return hosp_dept_codg;
        }

        public void setHosp_dept_codg(String hosp_dept_codg) {
            this.hosp_dept_codg = hosp_dept_codg;
        }

        public String getHosp_dept_name() {
            return hosp_dept_name;
        }

        public void setHosp_dept_name(String hosp_dept_name) {
            this.hosp_dept_name = hosp_dept_name;
        }

        public String getDr_code() {
            return dr_code;
        }

        public void setDr_code(String dr_code) {
            this.dr_code = dr_code;
        }

        public String getDr_name() {
            return dr_name;
        }

        public void setDr_name(String dr_name) {
            this.dr_name = dr_name;
        }

        public String getDr_pwd() {
            return dr_pwd;
        }

        public void setDr_pwd(String dr_pwd) {
            this.dr_pwd = dr_pwd;
        }

        public String getDr_type() {
            return dr_type;
        }

        public void setDr_type(String dr_type) {
            this.dr_type = dr_type;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDr_type_name() {
            return dr_type_name;
        }

        public void setDr_type_name(String dr_type_name) {
            this.dr_type_name = dr_type_name;
        }

        public String getDr_phone() {
            return dr_phone;
        }

        public void setDr_phone(String dr_phone) {
            this.dr_phone = dr_phone;
        }

        public String getAccessoryUrl() {
            return accessoryUrl;
        }

        public void setAccessoryUrl(String accessoryUrl) {
            this.accessoryUrl = accessoryUrl;
        }

        public String getAccessoryUrl1() {
            return accessoryUrl1;
        }

        public void setAccessoryUrl1(String accessoryUrl1) {
            this.accessoryUrl1 = accessoryUrl1;
        }

        public String getAccessoryId() {
            return accessoryId;
        }

        public void setAccessoryId(String accessoryId) {
            this.accessoryId = accessoryId;
        }

        public String getSubAccessoryId() {
            return subAccessoryId;
        }

        public void setSubAccessoryId(String subAccessoryId) {
            this.subAccessoryId = subAccessoryId;
        }

        public String getApproveStatus() {
            return ApproveStatus;
        }

        public void setApproveStatus(String approveStatus) {
            ApproveStatus = approveStatus;
        }

        public String getIsEnable() {
            return IsEnable;
        }

        public void setIsEnable(String isEnable) {
            IsEnable = isEnable;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.doctorId);
            dest.writeString(this.hosp_dept_codg);
            dest.writeString(this.hosp_dept_name);
            dest.writeString(this.dr_code);
            dest.writeString(this.dr_name);
            dest.writeString(this.dr_pwd);
            dest.writeString(this.dr_type);
            dest.writeString(this.type);
            dest.writeString(this.dr_type_name);
            dest.writeString(this.dr_phone);
            dest.writeString(this.accessoryUrl);
            dest.writeString(this.accessoryUrl1);
            dest.writeString(this.accessoryId);
            dest.writeString(this.subAccessoryId);
            dest.writeString(this.ApproveStatus);
            dest.writeString(this.IsEnable);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.doctorId = in.readString();
            this.hosp_dept_codg = in.readString();
            this.hosp_dept_name = in.readString();
            this.dr_code = in.readString();
            this.dr_name = in.readString();
            this.dr_pwd = in.readString();
            this.dr_type = in.readString();
            this.type = in.readString();
            this.dr_type_name = in.readString();
            this.dr_phone = in.readString();
            this.accessoryUrl = in.readString();
            this.accessoryUrl1 = in.readString();
            this.accessoryId = in.readString();
            this.subAccessoryId = in.readString();
            this.ApproveStatus = in.readString();
            this.IsEnable = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //医师列表
    public static void sendDoctorManageModelRequest(final String TAG, String pageindex, String pagesize,
                                                    final CustomerJsonCallBack<DoctorManageModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_DOCTORLIST_URL, jsonObject.toJSONString(), callback);
    }

    //医师列表
    public static void sendDoctorManageModelRequest2(final String TAG, String pageindex, String pagesize,String fixmedins_code,String fixmedins_name,
                                                    final CustomerJsonCallBack<DoctorManageModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        RequestData.requesNetWork_Json2(TAG,fixmedins_code,fixmedins_name, DeviceID.getDeviceIDByMMKV(), Constants.BASE_URL + Constants.Api.GET_DOCTORLIST_URL, jsonObject.toJSONString(), callback);
    }
}
