package com.jrdz.zhyb_android.ui.home.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.frame.compiler.widget.customPop.model.BottomChooseBean;
import com.frame.compiler.widget.wheel.entity.IWheelEntity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/9
 * 描    述：
 * ================================================
 */
public class KVModel implements BottomChooseBean,IWheelEntity {
    private String code;
    private String text;

    public KVModel(String text,String code) {
        this.text = text;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getWheelText() {
        return text;
    }

    @Override
    public String getText() {
        return text;
    }
}
