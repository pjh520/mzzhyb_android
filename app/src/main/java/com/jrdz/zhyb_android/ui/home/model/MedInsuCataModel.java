package com.jrdz.zhyb_android.ui.home.model;

import com.alibaba.fastjson.JSONObject;
import com.frame.compiler.utils.DateUtil;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/7
 * 描    述：
 * ================================================
 */
public class MedInsuCataModel {

    //医保目录信息查询
    public static void sendMedInsuCataRequest(final String TAG, String pageindex, String pagesize,
                                         final CustomerJsonCallBack<MedInsuCataModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("updt_time", DateUtil.getStringDate("yyyy-MM-dd"));
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_QUERYHILIST_URL, jsonObject.toJSONString(), callback);
    }
}
