package com.jrdz.zhyb_android.ui.home.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-06-07
 * 描    述：
 * ================================================
 */
public class MonthSettDeclarListModel {

    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-02-09 18:58:50
     * data : [{"title":"医保轮播图！","url":"#","imgurl":"http://113.135.194.23:3080/banner.png","type":"1"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean  {
        /**
         * acct_pay : 0
         * act_dfr_amt : 0
         * begndate : 2022-05-01
         * cash_payamt : 2007.98
         * clr_appy_evt_id : 000000000000000000000001651210
         * clr_optins : 610824
         * clr_psntime : 72
         * clr_stas : 10
         * clr_type : 11
         * clr_type_lv2 : 3901
         * clr_ym : 202206
         * crte_optins_no : 1624279743000000070
         * crte_time : 2022-06-23 15:11:56
         * crter_id : 1634567150438081922
         * crter_name : 刘宝宏
         * det_sumamt : 0
         * dfr_amt : 0
         * dpst_sumamt : 0
         * enddate : 2022-05-31
         * fee_clr_id : 5663348
         * fin_optins : 610824
         * fix_blng_admdvs : 610824
         * fixmedins_code : H61082401378
         * fixmedins_name : 西新区阳光卫生室
         * fixmedins_type : 1
         * fund_appy_sum : 1772.45
         * hi_agre_sumfee : 2780.43
         * insutype : 390
         * medfee_sumamt : 2780.43
         * opt_time : 2022-06-13 15:11:55
         * opter_id : 1634567150438081922
         * opter_name : 刘宝宏
         * optins_no : 1624279743000000070
         * poolarea_no : null
         * rid : 610000202206131511563737989926
         * sumamt_det_sumamt : 0
         * updt_time : 2022-06-13 15:11:56
         * vali_flag : 1
         */

        private String acct_pay;
        private String act_dfr_amt;
        private String begndate;
        private String cash_payamt;
        private String clr_appy_evt_id;
        private String clr_optins;
        private String clr_psntime;
        private String clr_stas;
        private String clr_stasname;
        private String clr_type;
        private String clr_type_lv2;
        private String clr_ym;
        private String crte_optins_no;
        private String crte_time;
        private String crter_id;
        private String crter_name;
        private String det_sumamt;
        private String dfr_amt;
        private String dpst_sumamt;
        private String enddate;
        private String fee_clr_id;
        private String fin_optins;
        private String fix_blng_admdvs;
        private String fixmedins_code;
        private String fixmedins_name;
        private String fixmedins_type;
        private String fund_appy_sum;
        private String hi_agre_sumfee;
        private String insutype;
        private String insutype_name;
        private String medfee_sumamt;
        private String opt_time;
        private String opter_id;
        private String opter_name;
        private String optins_no;
        private String poolarea_no;
        private String rid;
        private String sumamt_det_sumamt;
        private String updt_time;
        private String vali_flag;
        private String clr_wayname;

        public String getAcct_pay() {
            return acct_pay;
        }

        public void setAcct_pay(String acct_pay) {
            this.acct_pay = acct_pay;
        }

        public String getAct_dfr_amt() {
            return act_dfr_amt;
        }

        public void setAct_dfr_amt(String act_dfr_amt) {
            this.act_dfr_amt = act_dfr_amt;
        }

        public String getBegndate() {
            return begndate;
        }

        public void setBegndate(String begndate) {
            this.begndate = begndate;
        }

        public String getCash_payamt() {
            return cash_payamt;
        }

        public void setCash_payamt(String cash_payamt) {
            this.cash_payamt = cash_payamt;
        }

        public String getClr_appy_evt_id() {
            return clr_appy_evt_id;
        }

        public void setClr_appy_evt_id(String clr_appy_evt_id) {
            this.clr_appy_evt_id = clr_appy_evt_id;
        }

        public String getClr_optins() {
            return clr_optins;
        }

        public void setClr_optins(String clr_optins) {
            this.clr_optins = clr_optins;
        }

        public String getClr_psntime() {
            return clr_psntime;
        }

        public void setClr_psntime(String clr_psntime) {
            this.clr_psntime = clr_psntime;
        }

        public String getClr_stas() {
            return clr_stas;
        }

        public void setClr_stas(String clr_stas) {
            this.clr_stas = clr_stas;
        }

        public String getClr_stasname() {
            return clr_stasname;
        }

        public void setClr_stasname(String clr_stasname) {
            this.clr_stasname = clr_stasname;
        }

        public String getClr_type() {
            return clr_type;
        }

        public void setClr_type(String clr_type) {
            this.clr_type = clr_type;
        }

        public String getClr_type_lv2() {
            return clr_type_lv2;
        }

        public void setClr_type_lv2(String clr_type_lv2) {
            this.clr_type_lv2 = clr_type_lv2;
        }

        public String getClr_ym() {
            return clr_ym;
        }

        public void setClr_ym(String clr_ym) {
            this.clr_ym = clr_ym;
        }

        public String getCrte_optins_no() {
            return crte_optins_no;
        }

        public void setCrte_optins_no(String crte_optins_no) {
            this.crte_optins_no = crte_optins_no;
        }

        public String getCrte_time() {
            return crte_time;
        }

        public void setCrte_time(String crte_time) {
            this.crte_time = crte_time;
        }

        public String getCrter_id() {
            return crter_id;
        }

        public void setCrter_id(String crter_id) {
            this.crter_id = crter_id;
        }

        public String getCrter_name() {
            return crter_name;
        }

        public void setCrter_name(String crter_name) {
            this.crter_name = crter_name;
        }

        public String getDet_sumamt() {
            return det_sumamt;
        }

        public void setDet_sumamt(String det_sumamt) {
            this.det_sumamt = det_sumamt;
        }

        public String getDfr_amt() {
            return dfr_amt;
        }

        public void setDfr_amt(String dfr_amt) {
            this.dfr_amt = dfr_amt;
        }

        public String getDpst_sumamt() {
            return dpst_sumamt;
        }

        public void setDpst_sumamt(String dpst_sumamt) {
            this.dpst_sumamt = dpst_sumamt;
        }

        public String getEnddate() {
            return enddate;
        }

        public void setEnddate(String enddate) {
            this.enddate = enddate;
        }

        public String getFee_clr_id() {
            return fee_clr_id;
        }

        public void setFee_clr_id(String fee_clr_id) {
            this.fee_clr_id = fee_clr_id;
        }

        public String getFin_optins() {
            return fin_optins;
        }

        public void setFin_optins(String fin_optins) {
            this.fin_optins = fin_optins;
        }

        public String getFix_blng_admdvs() {
            return fix_blng_admdvs;
        }

        public void setFix_blng_admdvs(String fix_blng_admdvs) {
            this.fix_blng_admdvs = fix_blng_admdvs;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getFixmedins_name() {
            return fixmedins_name;
        }

        public void setFixmedins_name(String fixmedins_name) {
            this.fixmedins_name = fixmedins_name;
        }

        public String getFixmedins_type() {
            return fixmedins_type;
        }

        public void setFixmedins_type(String fixmedins_type) {
            this.fixmedins_type = fixmedins_type;
        }

        public String getFund_appy_sum() {
            return fund_appy_sum;
        }

        public void setFund_appy_sum(String fund_appy_sum) {
            this.fund_appy_sum = fund_appy_sum;
        }

        public String getHi_agre_sumfee() {
            return hi_agre_sumfee;
        }

        public void setHi_agre_sumfee(String hi_agre_sumfee) {
            this.hi_agre_sumfee = hi_agre_sumfee;
        }

        public String getInsutype() {
            return insutype;
        }

        public void setInsutype(String insutype) {
            this.insutype = insutype;
        }

        public String getInsutype_name() {
            return insutype_name;
        }

        public void setInsutype_name(String insutype_name) {
            this.insutype_name = insutype_name;
        }

        public String getMedfee_sumamt() {
            return medfee_sumamt;
        }

        public void setMedfee_sumamt(String medfee_sumamt) {
            this.medfee_sumamt = medfee_sumamt;
        }

        public String getOpt_time() {
            return opt_time;
        }

        public void setOpt_time(String opt_time) {
            this.opt_time = opt_time;
        }

        public String getOpter_id() {
            return opter_id;
        }

        public void setOpter_id(String opter_id) {
            this.opter_id = opter_id;
        }

        public String getOpter_name() {
            return opter_name;
        }

        public void setOpter_name(String opter_name) {
            this.opter_name = opter_name;
        }

        public String getOptins_no() {
            return optins_no;
        }

        public void setOptins_no(String optins_no) {
            this.optins_no = optins_no;
        }

        public String getPoolarea_no() {
            return poolarea_no;
        }

        public void setPoolarea_no(String poolarea_no) {
            this.poolarea_no = poolarea_no;
        }

        public String getRid() {
            return rid;
        }

        public void setRid(String rid) {
            this.rid = rid;
        }

        public String getSumamt_det_sumamt() {
            return sumamt_det_sumamt;
        }

        public void setSumamt_det_sumamt(String sumamt_det_sumamt) {
            this.sumamt_det_sumamt = sumamt_det_sumamt;
        }

        public String getUpdt_time() {
            return updt_time;
        }

        public void setUpdt_time(String updt_time) {
            this.updt_time = updt_time;
        }

        public String getVali_flag() {
            return vali_flag;
        }

        public void setVali_flag(String vali_flag) {
            this.vali_flag = vali_flag;
        }

        public String getClr_wayname() {
            return clr_wayname;
        }

        public void setClr_wayname(String clr_wayname) {
            this.clr_wayname = clr_wayname;
        }
    }

    //月结核定结果查询
    public static void sendMonthSettDeclarListRequest(final String TAG,String begndate, String pageindex, String pagesize,
                                              final CustomerJsonCallBack<MonthSettDeclarListModel> callback) {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("clr_type", "");
        jsonObject.put("clr_way", "");
        jsonObject.put("clr_type_lv2", "");
        jsonObject.put("begndate", begndate);
        jsonObject.put("enddate", "");
        jsonObject.put("page_num", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_QUERYMONSETLAPPYINFO_URL, jsonObject.toJSONString(), callback);
    }
}
