package com.jrdz.zhyb_android.ui.home.model;

import com.frame.compiler.widget.tabLayout.listener.CustomTabEntity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-05-07
 * 描    述：
 * ================================================
 */
public class MonthSettTypeModel implements CustomTabEntity {
    public String type;
    public String title;

    public MonthSettTypeModel(String type,String title) {
        this.type = type;
        this.title = title;
    }

    @Override
    public String getTabTitle() {
        return title;
    }

    @Override
    public int getTabSelectedIcon() {
        return 0;
    }

    @Override
    public int getTabUnselectedIcon() {
        return 0;
    }
}