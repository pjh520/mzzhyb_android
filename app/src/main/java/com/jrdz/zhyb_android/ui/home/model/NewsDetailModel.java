package com.jrdz.zhyb_android.ui.home.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/28
 * 描    述：
 * ================================================
 */
public class NewsDetailModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-03-02 15:33:17
     * data : {"title":"app上线","content":"app上线","CreateDT":"2022-02-02 00:00:00"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : app上线
         * content : app上线
         * CreateDT : 2022-02-02 00:00:00
         */

        private String Title;
        private String NewsContent;
        private String CreateDT;

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            this.Title = title;
        }

        public String getContent() {
            return NewsContent;
        }

        public void setContent(String content) {
            this.NewsContent = content;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }
    }

    //资讯公告详情
    public static void sendNewsDetailRequest(final String TAG, String id, final CustomerJsonCallBack<NewsDetailModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_NEWSDETAIL_URL, jsonObject.toJSONString(), callback);
    }
}
