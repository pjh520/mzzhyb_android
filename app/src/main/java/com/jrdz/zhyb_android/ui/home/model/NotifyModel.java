package com.jrdz.zhyb_android.ui.home.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/26
 * 描    述：
 * ================================================
 */
public class NotifyModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-02-06 15:25:13
     * data : [{"title":"74种药品新增进入国家医保目录，含7种罕见病用药！","url":"https://mp.weixin.qq.com/s/pcVpwwbPLHXpbBwZVtPdkg","imgurl":"http://113.135.194.23:3080/banner.png","typename":"医保资讯"},{"title":"重磅喜讯！4种抗血液肿瘤药物新增进入国家医保目录","url":"https://mp.weixin.qq.com/s/7hkvqUfyJErUDH0WgbeZVg","imgurl":"http://113.135.194.23:3080/banner.png","typename":"医保资讯"},{"title":"新版国家医保目录落地！一周一次重磅降糖药诺和泰惠及糖尿病患者！","url":"https://mp.weixin.qq.com/s/56Y53JWFlNb7M0wuak-GKA","imgurl":"http://113.135.194.23:3080/banner.png","typename":"医保资讯"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : 74种药品新增进入国家医保目录，含7种罕见病用药！
         * url : https://mp.weixin.qq.com/s/pcVpwwbPLHXpbBwZVtPdkg
         * imgurl : http://113.135.194.23:3080/banner.png
         * typename : 医保资讯
         */
        private String NoticeId;
        private String title;
        private String url;
        private String imgurl;
        private String typename;
        private String CreateDT;
        private String type;

        public DataBean(String title, String url, String createDT, String type) {
            this.title = title;
            this.url = url;
            CreateDT = createDT;
            this.type = type;
        }

        public String getNoticeId() {
            return NoticeId;
        }

        public void setNoticeId(String noticeId) {
            NoticeId = noticeId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getImgurl() {
            return imgurl;
        }

        public void setImgurl(String imgurl) {
            this.imgurl = imgurl;
        }

        public String getTypename() {
            return typename;
        }

        public void setTypename(String typename) {
            this.typename = typename;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String createDT) {
            CreateDT = createDT;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    //资讯公告
    public static void sendNotifyRequest(final String TAG, String pageindex, String pagesize,
                                         final CustomerJsonCallBack<NotifyModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_NOTIFY_URL, jsonObject.toJSONString(), callback);
    }

    //参保人资讯公告
    public static void sendInsuredNotifyRequest(final String TAG, String pageindex, String pagesize,
                                         final CustomerJsonCallBack<NotifyModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_NOTIFY_URL, jsonObject.toJSONString(), callback);
    }
}

