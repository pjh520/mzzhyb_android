package com.jrdz.zhyb_android.ui.home.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/20
 * 描    述：
 * ================================================
 */
public class QueryComFunctModel {
    private String id;
    private int img;
    private String text;

    public QueryComFunctModel(String id, int img, String text) {
        this.id = id;
        this.img = img;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public int getImg() {
        return img;
    }

    public String getText() {
        return text;
    }
}
