package com.jrdz.zhyb_android.ui.home.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/24
 * 描    述：
 * ================================================
 */
public class QueryMonSetlModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-02-24 23:12:46
     * data : {"content":"月结查询\n医疗机构编码:H61080200030\n医疗机构名称:榆阳区安定精神病医院\n查询年月:2022年02月\n统计时间:2022-02-24 23:12:46\n结算笔数:1\n医疗费总额:0.01元\n基金支付总额:0元\n个人账户支付总额:0.01元\n个人现金支付总额:0元"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * content : 月结查询
         医疗机构编码:H61080200030
         医疗机构名称:榆阳区安定精神病医院
         查询年月:2022年02月
         统计时间:2022-02-24 23:12:46
         结算笔数:1
         医疗费总额:0.01元
         基金支付总额:0元
         个人账户支付总额:0.01元
         个人现金支付总额:0元
         */

        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    //查看对明细账  settlementClassification： ""全部 1医保结算 2自费结算
    public static void sendQueryMonSetlRequest(final String TAG, String stmt_begndate,String settlementClassification,
                                               final CustomerJsonCallBack<QueryMonSetlModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("stmt_begndate", stmt_begndate);
        jsonObject.put("clr_type", "1".equals(MechanismInfoUtils.getFixmedinsType())?"11":"41");
        jsonObject.put("settlementClassification", settlementClassification);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_QUERYMONSETL_URL, jsonObject.toJSONString(), callback);
    }
}
