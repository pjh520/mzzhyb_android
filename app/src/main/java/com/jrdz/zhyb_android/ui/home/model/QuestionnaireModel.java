package com.jrdz.zhyb_android.ui.home.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-09-06
 * 描    述：
 * ================================================
 */
public class QuestionnaireModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-09-14 09:09:17
     * data : [{"ItemTitle":"1、您下载智慧医保APP的途径是什么？","ItemCode":"1","ItemSort":1,"ItemType":1,"IsMandatory":0,"QuestionnaireTemplateCode":"Questionnaire","TemplateSubItem":[{"SubItemTitle":"软件商城","SubItemCode":"1-1"},{"SubItemTitle":"榆林医保公众号","SubItemCode":"1-2"},{"SubItemTitle":"浏览器","SubItemCode":"1-3"},{"SubItemTitle":"朋友推荐","SubItemCode":"1-4"},{"SubItemTitle":"其他","SubItemCode":"1-5"}]}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * ItemTitle : 1、您下载智慧医保APP的途径是什么？
         * ItemCode : 1
         * ItemSort : 1
         * ItemType : 1
         * IsMandatory : 0
         * QuestionnaireTemplateCode : Questionnaire
         * TemplateSubItem : [{"SubItemTitle":"软件商城","SubItemCode":"1-1"},{"SubItemTitle":"榆林医保公众号","SubItemCode":"1-2"},{"SubItemTitle":"浏览器","SubItemCode":"1-3"},{"SubItemTitle":"朋友推荐","SubItemCode":"1-4"},{"SubItemTitle":"其他","SubItemCode":"1-5"}]
         */

        private String ItemTitle;//标题
        private String ItemCode;
        private String ItemType;//单选1或者多选2或者输入框3
        private String IsMandatory;//是否必填（0、否 1是）
        private String QuestionnaireTemplateCode;
        private List<TemplateSubItemBean> TemplateSubItem;

        public String getItemTitle() {
            return ItemTitle;
        }

        public void setItemTitle(String ItemTitle) {
            this.ItemTitle = ItemTitle;
        }

        public String getItemCode() {
            return ItemCode;
        }

        public void setItemCode(String ItemCode) {
            this.ItemCode = ItemCode;
        }

        public String getItemType() {
            return ItemType;
        }

        public void setItemType(String ItemType) {
            this.ItemType = ItemType;
        }

        public String getIsMandatory() {
            return IsMandatory;
        }

        public void setIsMandatory(String IsMandatory) {
            this.IsMandatory = IsMandatory;
        }

        public String getQuestionnaireTemplateCode() {
            return QuestionnaireTemplateCode;
        }

        public void setQuestionnaireTemplateCode(String QuestionnaireTemplateCode) {
            this.QuestionnaireTemplateCode = QuestionnaireTemplateCode;
        }

        public List<TemplateSubItemBean> getTemplateSubItem() {
            return TemplateSubItem;
        }

        public void setTemplateSubItem(List<TemplateSubItemBean> TemplateSubItem) {
            this.TemplateSubItem = TemplateSubItem;
        }

        public static class TemplateSubItemBean {
            /**
             * SubItemTitle : 软件商城
             * SubItemCode : 1-1
             */

            private String SubItemTitle;
            private String SubItemCode;
            private boolean isChoose=false;

            public String getSubItemTitle() {
                return SubItemTitle;
            }

            public void setSubItemTitle(String SubItemTitle) {
                this.SubItemTitle = SubItemTitle;
            }

            public String getSubItemCode() {
                return SubItemCode;
            }

            public void setSubItemCode(String SubItemCode) {
                this.SubItemCode = SubItemCode;
            }

            public boolean isChoose() {
                return isChoose;
            }

            public void setChoose(boolean choose) {
                isChoose = choose;
            }
        }
    }

    //获取调查问卷模板
    public static void sendQuestionnaireRequest(final String TAG,String questionnaire, final CustomerJsonCallBack<QuestionnaireModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("QuestionnaireTemplateCode", questionnaire);
        RequestData.requesNetWork_Json(TAG,Constants.BASE_URL + Constants.Api.GET_QUERYQUESTIONNAIRETEMPLATE_URL, jsonObject.toJSONString(), callback);
    }
}
