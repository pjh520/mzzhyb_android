package com.jrdz.zhyb_android.ui.home.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.QueryDoctorActivationStatusModel;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-12-11
 * 描    述：
 * ================================================
 */
public class QuestionnairePopModel {

//    {
//        "code": "1",
//            "msg": "已填写问卷调查",
//            "server_time": "2023-12-11 20:30:54",
//            "data": {
//        "IsQuestionnaire": "0",
//                "IsConstraint": "1",
//                "QuestionnaireTemplateCode": "Questionnaire"
//    }
//    }

    private String code;
    private String msg;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private String IsQuestionnaire;//是否有调查问卷  1有0没有
        private String IsConstraint;//是否强制开启 1开启2不开启
        private String QuestionnaireTemplateCode;//调查问卷模板代码

        public String getIsQuestionnaire() {
            return IsQuestionnaire;
        }

        public void setIsQuestionnaire(String isQuestionnaire) {
            IsQuestionnaire = isQuestionnaire;
        }

        public String getIsConstraint() {
            return IsConstraint;
        }

        public void setIsConstraint(String isConstraint) {
            IsConstraint = isConstraint;
        }

        public String getQuestionnaireTemplateCode() {
            return QuestionnaireTemplateCode;
        }

        public void setQuestionnaireTemplateCode(String questionnaireTemplateCode) {
            QuestionnaireTemplateCode = questionnaireTemplateCode;
        }
    }

    public static void sendIsQuestionnaireRequest(final String TAG,final CustomerJsonCallBack<QuestionnairePopModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STORE_ISQUESTIONNAIRE_URL, "", callback);
    }
}
