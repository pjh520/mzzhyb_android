package com.jrdz.zhyb_android.ui.home.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/24
 * 描    述：
 * ================================================
 */
public class ScanCataRefreshModel {
    private String name;
    private String price;
    private int pos;

    public ScanCataRefreshModel(String name, String price, int pos) {
        this.name = name;
        this.price = price;
        this.pos = pos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }
}
