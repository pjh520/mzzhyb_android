package com.jrdz.zhyb_android.ui.home.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.database.CatalogueModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;
import com.jrdz.zhyb_android.ui.catalogue.model.WestCataListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/21
 * 描    述：
 * ================================================
 */
public class ScanOrganModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-02-21 21:32:22
     * data : [{"drug_type_name":"0.4ml:4mg","reg_dosform":"眼用制剂","MICComparisonId":57,"MIC_Code":"XS01JXS191G010020178976","ItemCode":"XS01JXS191G010020178976","ItemName":"羧甲基纤维素钠滴眼液","ItemType":"101","Specification":"","EnterpriseName":"Allergan Pharmaceuticals Ireland","ApprovalNumber":"H20140946","CatalogCassification":0,"Price":0.01}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String type;
    private ArrayList<CatalogueModel> data1;
    private List<WestCataListModel.DataBean> data2;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<CatalogueModel> getData1() {
        return data1;
    }

    public void setData1(ArrayList<CatalogueModel> data1) {
        this.data1 = data1;
    }

    public List<WestCataListModel.DataBean> getData2() {
        return data2;
    }

    public void setData2(List<WestCataListModel.DataBean> data2) {
        this.data2 = data2;
    }

    //扫码查询已对照目录表
    public static void sendScanOrganRequest(final String TAG, String barCode,final CustomerJsonCallBack<ScanOrganModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("barCode", barCode);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_COMPA_SCANCODELIST_URL, jsonObject.toJSONString(), callback);
    }
}
