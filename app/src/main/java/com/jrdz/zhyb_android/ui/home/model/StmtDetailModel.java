package com.jrdz.zhyb_android.ui.home.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/28
 * 描    述：
 * ================================================
 */
public class StmtDetailModel {
    /**
     * code : 1
     * msg : 请求成功
     * obj : {"currentPage":1,"resultObj":[{"acct_pay":0,"begntime":"2021-12-28 11:06:48","create_time":1639639874000,"create_user":"admin","fixmedins_code":"H61080200030","fulamt_ownpay_amt":30,"fund_pay_sumamt":0,"id":5,"ideleted":1,"insutype":"310","mdtrt_cert_no":"U0ZaOzY2Njg4ODvLzruztLo7NjEyNzI2MTk2NjA5MjEwMDExOzIwMjEtMTEtMzAgMTY6NTg6MjA","mdtrt_cert_type":"02","mdtrt_id":"4102012","med_type":"41","medfee_sumamt":30,"medins_setl_id":"H61080200030202112161531101547","psn_no":"61000006000000000010866022","refd_setl_flag":"0","setl_id":"3064008","update_time":1640420026000,"update_user":"admin"}],"showCount":10,"totalPage":3}
     */

    private String code;
    private String msg;
    private ArrayList<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<DataBean> getData() {
        return data;
    }

    public void setData(ArrayList<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * StmtDetailRecordId : 162
         * StmtTotalId : 4
         * psn_no : 61000006000000000010866022
         * mdtrt_id : 159300002
         * setl_id : 150200001
         * msgid : null
         * stmt_rslt : 1
         * refd_setl_flag : 1
         * memo : 此结算信息在结算中心存在定点机构中不存在;
         * CreateDT : 2022-02-18 13:50:30
         */

        private int StmtDetailRecordId;
        private int StmtTotalId;
        private String psn_no;
        private String mdtrt_id;
        private String setl_id;
        private String msgid;
        private String stmt_rslt;
        private String stmt_rslt_name;
        private String refd_setl_flag;
        private String refd_setl_flag_name;
        private String memo;
        private String CreateDT;

        public int getStmtDetailRecordId() {
            return StmtDetailRecordId;
        }

        public void setStmtDetailRecordId(int StmtDetailRecordId) {
            this.StmtDetailRecordId = StmtDetailRecordId;
        }

        public int getStmtTotalId() {
            return StmtTotalId;
        }

        public void setStmtTotalId(int StmtTotalId) {
            this.StmtTotalId = StmtTotalId;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getMdtrt_id() {
            return mdtrt_id;
        }

        public void setMdtrt_id(String mdtrt_id) {
            this.mdtrt_id = mdtrt_id;
        }

        public String getSetl_id() {
            return setl_id;
        }

        public void setSetl_id(String setl_id) {
            this.setl_id = setl_id;
        }

        public String getMsgid() {
            return msgid;
        }

        public void setMsgid(String msgid) {
            this.msgid = msgid;
        }

        public String getStmt_rslt() {
            return stmt_rslt;
        }

        public void setStmt_rslt(String stmt_rslt) {
            this.stmt_rslt = stmt_rslt;
        }

        public String getStmt_rslt_name() {
            return stmt_rslt_name;
        }

        public void setStmt_rslt_name(String stmt_rslt_name) {
            this.stmt_rslt_name = stmt_rslt_name;
        }

        public String getRefd_setl_flag() {
            return refd_setl_flag;
        }

        public void setRefd_setl_flag(String refd_setl_flag) {
            this.refd_setl_flag = refd_setl_flag;
        }

        public String getRefd_setl_flag_name() {
            return refd_setl_flag_name;
        }

        public void setRefd_setl_flag_name(String refd_setl_flag_name) {
            this.refd_setl_flag_name = refd_setl_flag_name;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }
    }

    //查看对明细账
    public static void sendStmtDetailRequest(final String TAG, String StmtTotalId, String pageindex, String pagesize,
                                             final CustomerJsonCallBack<StmtDetailModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("StmtTotalId", StmtTotalId);
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STMTDETAILRECORDLIST_URL, jsonObject.toJSONString(), callback);
    }
}
