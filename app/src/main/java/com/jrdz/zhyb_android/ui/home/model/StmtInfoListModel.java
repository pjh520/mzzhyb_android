package com.jrdz.zhyb_android.ui.home.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/28
 * 描    述：
 * ================================================
 */
public class StmtInfoListModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-02-07 17:47:01
     * data : [{"SettlementId":1,"psn_no":"61000006000000000010866022","mdtrt_cert_type":"02","mdtrt_cert_no":"612726196609210011","med_type":"11","medfee_sumamt":"0.01","psn_setlway":"01","mdtrt_id":"159000001","chrg_bchno":"20220207174210001","acct_used_flag":"1","insutype":"310","invono":"","fulamt_ownpay_amt":"0","overlmt_selfpay":"0","preselfpay_amt":"0","inscp_scp_amt":"0","Status":1}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * SettlementId : 1
         * psn_no : 61000006000000000010866022
         * mdtrt_cert_type : 02
         * mdtrt_cert_no : 612726196609210011
         * med_type : 11
         * medfee_sumamt : 0.01
         * psn_setlway : 01
         * mdtrt_id : 159000001
         * chrg_bchno : 20220207174210001
         * acct_used_flag : 1
         * insutype : 310
         * invono :
         * fulamt_ownpay_amt : 0
         * overlmt_selfpay : 0
         * preselfpay_amt : 0
         * inscp_scp_amt : 0
         * Status : 1
         */

        private String setl_id;
        private String ipt_otp_no;
        private String psn_no;
        private String mdtrt_cert_type;
        private String mdtrt_cert_no;
        private String med_type;
        private String med_type_name;
        private String medfee_sumamt;
        private String psn_setlway;
        private String mdtrt_id;
        private String chrg_bchno;
        private String acct_used_flag;
        private String insutype;
        private String insutype_name;
        private String invono;
        private String fulamt_ownpay_amt;
        private String overlmt_selfpay;
        private String preselfpay_amt;
        private String inscp_scp_amt;
        private String Status;
        private String StatusName;
        private String setl_time;
        private String fund_pay_sumamt;
        private String acct_pay;
        private String IsUpload;
        private String original_setl_id;
        private String psn_name;
        private String psn_cash_pay;

        public String getSetl_id() {
            return setl_id;
        }

        public void setSetl_id(String setl_id) {
            this.setl_id = setl_id;
        }

        public String getIpt_otp_no() {
            return ipt_otp_no;
        }

        public void setIpt_otp_no(String ipt_otp_no) {
            this.ipt_otp_no = ipt_otp_no;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getMdtrt_cert_type() {
            return mdtrt_cert_type;
        }

        public void setMdtrt_cert_type(String mdtrt_cert_type) {
            this.mdtrt_cert_type = mdtrt_cert_type;
        }

        public String getMdtrt_cert_no() {
            return mdtrt_cert_no;
        }

        public void setMdtrt_cert_no(String mdtrt_cert_no) {
            this.mdtrt_cert_no = mdtrt_cert_no;
        }

        public String getMed_type() {
            return med_type;
        }

        public void setMed_type(String med_type) {
            this.med_type = med_type;
        }

        public String getMed_type_name() {
            return med_type_name;
        }

        public void setMed_type_name(String med_type_name) {
            this.med_type_name = med_type_name;
        }

        public String getMedfee_sumamt() {
            return medfee_sumamt;
        }

        public void setMedfee_sumamt(String medfee_sumamt) {
            this.medfee_sumamt = medfee_sumamt;
        }

        public String getPsn_setlway() {
            return psn_setlway;
        }

        public void setPsn_setlway(String psn_setlway) {
            this.psn_setlway = psn_setlway;
        }

        public String getMdtrt_id() {
            return mdtrt_id;
        }

        public void setMdtrt_id(String mdtrt_id) {
            this.mdtrt_id = mdtrt_id;
        }

        public String getChrg_bchno() {
            return chrg_bchno;
        }

        public void setChrg_bchno(String chrg_bchno) {
            this.chrg_bchno = chrg_bchno;
        }

        public String getAcct_used_flag() {
            return acct_used_flag;
        }

        public void setAcct_used_flag(String acct_used_flag) {
            this.acct_used_flag = acct_used_flag;
        }

        public String getInsutype() {
            return insutype;
        }

        public void setInsutype(String insutype) {
            this.insutype = insutype;
        }

        public String getInsutype_name() {
            return insutype_name;
        }

        public void setInsutype_name(String insutype_name) {
            this.insutype_name = insutype_name;
        }

        public String getInvono() {
            return invono;
        }

        public void setInvono(String invono) {
            this.invono = invono;
        }

        public String getFulamt_ownpay_amt() {
            return fulamt_ownpay_amt;
        }

        public void setFulamt_ownpay_amt(String fulamt_ownpay_amt) {
            this.fulamt_ownpay_amt = fulamt_ownpay_amt;
        }

        public String getOverlmt_selfpay() {
            return overlmt_selfpay;
        }

        public void setOverlmt_selfpay(String overlmt_selfpay) {
            this.overlmt_selfpay = overlmt_selfpay;
        }

        public String getPreselfpay_amt() {
            return preselfpay_amt;
        }

        public void setPreselfpay_amt(String preselfpay_amt) {
            this.preselfpay_amt = preselfpay_amt;
        }

        public String getInscp_scp_amt() {
            return inscp_scp_amt;
        }

        public void setInscp_scp_amt(String inscp_scp_amt) {
            this.inscp_scp_amt = inscp_scp_amt;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String Status) {
            this.Status = Status;
        }

        public String getStatusName() {
            return StatusName;
        }

        public void setStatusName(String statusName) {
            StatusName = statusName;
        }

        public String getSetl_time() {
            return setl_time;
        }

        public void setSetl_time(String setl_time) {
            this.setl_time = setl_time;
        }

        public String getFund_pay_sumamt() {
            return fund_pay_sumamt;
        }

        public void setFund_pay_sumamt(String fund_pay_sumamt) {
            this.fund_pay_sumamt = fund_pay_sumamt;
        }

        public String getAcct_pay() {
            return acct_pay;
        }

        public void setAcct_pay(String acct_pay) {
            this.acct_pay = acct_pay;
        }

        public String getIsUpload() {
            return IsUpload;
        }

        public void setIsUpload(String isUpload) {
            IsUpload = isUpload;
        }

        public String getOriginal_setl_id() {
            return original_setl_id;
        }

        public void setOriginal_setl_id(String original_setl_id) {
            this.original_setl_id = original_setl_id;
        }

        public String getPsn_name() {
            return psn_name;
        }

        public void setPsn_name(String psn_name) {
            this.psn_name = psn_name;
        }

        public String getPsn_cash_pay() {
            return psn_cash_pay;
        }

        public void setPsn_cash_pay(String psn_cash_pay) {
            this.psn_cash_pay = psn_cash_pay;
        }
    }

    //获取门诊结算列表
    public static void sendSettleInfoRequest(final String TAG, String key, String pageindex, String pagesize,
                                             final CustomerJsonCallBack<StmtInfoListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("key", key);
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_SETTLEINFOLIST_URL, jsonObject.toJSONString(), callback);
    }

    //获取药店结算列表
    public static void sendPharmacySettleInfoRequest(final String TAG,String key, String pageindex, String pagesize,
                                             final CustomerJsonCallBack<StmtInfoListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("key", key);
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_PHARMACYSETTLEMENTLIST_URL, jsonObject.toJSONString(), callback);
    }
}
