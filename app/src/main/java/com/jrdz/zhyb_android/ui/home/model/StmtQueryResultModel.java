package com.jrdz.zhyb_android.ui.home.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/1/28
 * 描    述：
 * ================================================
 */
public class StmtQueryResultModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-02-09 14:09:27
     * data : [{"insutype":"310","clr_type":11,"medfee_sumamt":0.02,"fund_pay_sumamt":0,"acct_pay":0.02,"fixmedins_setl_cnt":1}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * insutype : 310
         * clr_type : 11
         * medfee_sumamt : 0.02
         * fund_pay_sumamt : 0
         * acct_pay : 0.02
         * fixmedins_setl_cnt : 1
         */

        private String insutype;
        private String insutype_name;
        private String clr_type;
        private String clr_type_name;
        private String medfee_sumamt;
        private String fund_pay_sumamt;
        private String acct_pay;
        private String fixmedins_setl_cnt;
        public boolean choose;

        public String getInsutype() {
            return insutype;
        }

        public void setInsutype(String insutype) {
            this.insutype = insutype;
        }

        public String getInsutype_name() {
            return insutype_name;
        }

        public void setInsutype_name(String insutype_name) {
            this.insutype_name = insutype_name;
        }

        public String getClr_type() {
            return clr_type;
        }

        public void setClr_type(String clr_type) {
            this.clr_type = clr_type;
        }

        public String getClr_type_name() {
            return clr_type_name;
        }

        public void setClr_type_name(String clr_type_name) {
            this.clr_type_name = clr_type_name;
        }

        public String getMedfee_sumamt() {
            return medfee_sumamt;
        }

        public void setMedfee_sumamt(String medfee_sumamt) {
            this.medfee_sumamt = medfee_sumamt;
        }

        public String getFund_pay_sumamt() {
            return fund_pay_sumamt;
        }

        public void setFund_pay_sumamt(String fund_pay_sumamt) {
            this.fund_pay_sumamt = fund_pay_sumamt;
        }

        public String getAcct_pay() {
            return acct_pay;
        }

        public void setAcct_pay(String acct_pay) {
            this.acct_pay = acct_pay;
        }

        public String getFixmedins_setl_cnt() {
            return fixmedins_setl_cnt;
        }

        public void setFixmedins_setl_cnt(String fixmedins_setl_cnt) {
            this.fixmedins_setl_cnt = fixmedins_setl_cnt;
        }

        public boolean isChoose() {
            return choose;
        }

        public void setChoose(boolean choose) {
            this.choose = choose;
        }
    }

    //获取本地结算信息
    public static void sendStmtQueryResultRequest(final String TAG,  String stmt_begndate,String stmt_enddate, final CustomerJsonCallBack<StmtQueryResultModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("stmt_begndate", stmt_begndate);
        jsonObject.put("stmt_enddate", stmt_enddate);
        jsonObject.put("clr_type", "1".equals(MechanismInfoUtils.getFixmedinsType())?"11":"41");//门诊11，药店41 MechanismInfoUtils.getFixmedinsType() 1医院 2药店
//        jsonObject.put("clr_type", "41");

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STMTTOTALLIST_URL, jsonObject.toJSONString(), callback);
    }
}
