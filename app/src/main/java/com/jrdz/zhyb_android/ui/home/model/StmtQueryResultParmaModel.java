package com.jrdz.zhyb_android.ui.home.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/9
 * 描    述：
 * ================================================
 */
public class StmtQueryResultParmaModel {

    private ArrayList<StmtTotalInfoBean> StmtTotalInfo=new ArrayList<>();

    public ArrayList<StmtTotalInfoBean> getStmtTotalInfo() {
        return StmtTotalInfo;
    }

    public void setStmtTotalInfo(ArrayList<StmtTotalInfoBean> stmtTotalInfo) {
        StmtTotalInfo = stmtTotalInfo;
    }

    public static class StmtTotalInfoBean{
        private String insutype;
        private String clr_type;
        private String setl_optins;
        private String stmt_begndate;
        private String stmt_enddate;
        private String medfee_sumamt;
        private String fund_pay_sumamt;
        private String acct_pay;
        private String fixmedins_setl_cnt;

        public StmtTotalInfoBean(String insutype, String clr_type, String setl_optins, String stmt_begndate, String stmt_enddate,
                                 String medfee_sumamt, String fund_pay_sumamt, String acct_pay, String fixmedins_setl_cnt) {
            this.insutype = insutype;
            this.clr_type = clr_type;
            this.setl_optins = setl_optins;
            this.stmt_begndate = stmt_begndate;
            this.stmt_enddate = stmt_enddate;
            this.medfee_sumamt = medfee_sumamt;
            this.fund_pay_sumamt = fund_pay_sumamt;
            this.acct_pay = acct_pay;
            this.fixmedins_setl_cnt = fixmedins_setl_cnt;
        }

        public String getInsutype() {
            return insutype;
        }

        public void setInsutype(String insutype) {
            this.insutype = insutype;
        }

        public String getClr_type() {
            return clr_type;
        }

        public void setClr_type(String clr_type) {
            this.clr_type = clr_type;
        }

        public String getSetl_optins() {
            return setl_optins;
        }

        public void setSetl_optins(String setl_optins) {
            this.setl_optins = setl_optins;
        }

        public String getStmt_begndate() {
            return stmt_begndate;
        }

        public void setStmt_begndate(String stmt_begndate) {
            this.stmt_begndate = stmt_begndate;
        }

        public String getStmt_enddate() {
            return stmt_enddate;
        }

        public void setStmt_enddate(String stmt_enddate) {
            this.stmt_enddate = stmt_enddate;
        }

        public String getMedfee_sumamt() {
            return medfee_sumamt;
        }

        public void setMedfee_sumamt(String medfee_sumamt) {
            this.medfee_sumamt = medfee_sumamt;
        }

        public String getFund_pay_sumamt() {
            return fund_pay_sumamt;
        }

        public void setFund_pay_sumamt(String fund_pay_sumamt) {
            this.fund_pay_sumamt = fund_pay_sumamt;
        }

        public String getAcct_pay() {
            return acct_pay;
        }

        public void setAcct_pay(String acct_pay) {
            this.acct_pay = acct_pay;
        }

        public String getFixmedins_setl_cnt() {
            return fixmedins_setl_cnt;
        }

        public void setFixmedins_setl_cnt(String fixmedins_setl_cnt) {
            this.fixmedins_setl_cnt = fixmedins_setl_cnt;
        }
    }
}
