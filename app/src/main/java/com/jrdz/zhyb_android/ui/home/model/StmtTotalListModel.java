package com.jrdz.zhyb_android.ui.home.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/9
 * 描    述：
 * ================================================
 */
public class StmtTotalListModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-02-09 14:09:27
     * data : [{"insutype":"310","clr_type":11,"medfee_sumamt":0.02,"fund_pay_sumamt":0,"acct_pay":0.02,"fixmedins_setl_cnt":1}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * insutype : 310
         * clr_type : 11
         * medfee_sumamt : 0.02
         * fund_pay_sumamt : 0
         * acct_pay : 0.02
         * fixmedins_setl_cnt : 1
         */

        private String StmtTotalId;
        private String insutype;
        private String setl_optins;
        private String insutype_name;
        private String clr_type;
        private String clr_type_name;
        private String medfee_sumamt;
        private String fund_pay_sumamt;
        private String acct_pay;
        private String fixmedins_setl_cnt;
        private String CreateDT;
        private String stmt_begndate;
        private String stmt_enddate;
        private String stmt_rslt;
        private String stmt_rslt_name;
        private String stmt_rslt_dscr;
        private String isStmtDetail;

        public String getStmtTotalId() {
            return StmtTotalId;
        }

        public void setStmtTotalId(String stmtTotalId) {
            StmtTotalId = stmtTotalId;
        }

        public String getInsutype() {
            return insutype;
        }

        public void setInsutype(String insutype) {
            this.insutype = insutype;
        }

        public String getSetl_optins() {
            return setl_optins;
        }

        public void setSetl_optins(String setl_optins) {
            this.setl_optins = setl_optins;
        }

        public String getInsutype_name() {
            return insutype_name;
        }

        public void setInsutype_name(String insutype_name) {
            this.insutype_name = insutype_name;
        }

        public String getClr_type() {
            return clr_type;
        }

        public void setClr_type(String clr_type) {
            this.clr_type = clr_type;
        }

        public String getClr_type_name() {
            return clr_type_name;
        }

        public void setClr_type_name(String clr_type_name) {
            this.clr_type_name = clr_type_name;
        }

        public String getMedfee_sumamt() {
            return medfee_sumamt;
        }

        public void setMedfee_sumamt(String medfee_sumamt) {
            this.medfee_sumamt = medfee_sumamt;
        }

        public String getFund_pay_sumamt() {
            return fund_pay_sumamt;
        }

        public void setFund_pay_sumamt(String fund_pay_sumamt) {
            this.fund_pay_sumamt = fund_pay_sumamt;
        }

        public String getAcct_pay() {
            return acct_pay;
        }

        public void setAcct_pay(String acct_pay) {
            this.acct_pay = acct_pay;
        }

        public String getFixmedins_setl_cnt() {
            return fixmedins_setl_cnt;
        }

        public void setFixmedins_setl_cnt(String fixmedins_setl_cnt) {
            this.fixmedins_setl_cnt = fixmedins_setl_cnt;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String createDT) {
            CreateDT = createDT;
        }

        public String getStmt_begndate() {
            return stmt_begndate;
        }

        public void setStmt_begndate(String stmt_begndate) {
            this.stmt_begndate = stmt_begndate;
        }

        public String getStmt_enddate() {
            return stmt_enddate;
        }

        public void setStmt_enddate(String stmt_enddate) {
            this.stmt_enddate = stmt_enddate;
        }

        public String getStmt_rslt() {
            return stmt_rslt;
        }

        public void setStmt_rslt(String stmt_rslt) {
            this.stmt_rslt = stmt_rslt;
        }

        public String getStmt_rslt_name() {
            return stmt_rslt_name;
        }

        public void setStmt_rslt_name(String stmt_rslt_name) {
            this.stmt_rslt_name = stmt_rslt_name;
        }

        public String getStmt_rslt_dscr() {
            return stmt_rslt_dscr;
        }

        public void setStmt_rslt_dscr(String stmt_rslt_dscr) {
            this.stmt_rslt_dscr = stmt_rslt_dscr;
        }

        public String getIsStmtDetail() {
            return isStmtDetail;
        }

        public void setIsStmtDetail(String isStmtDetail) {
            this.isStmtDetail = isStmtDetail;
        }
    }

    //对账结果记录列表
    public static void sendStmtTotalListRequest(final String TAG, String pageindex, String pagesize,
                                               final CustomerJsonCallBack<StmtTotalListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STMTINFOLIST_URL, jsonObject.toJSONString(), callback);
    }
}
