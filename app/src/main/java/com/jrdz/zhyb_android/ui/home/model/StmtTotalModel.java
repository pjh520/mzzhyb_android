package com.jrdz.zhyb_android.ui.home.model;

import com.alibaba.fastjson.JSONObject;
import com.frame.compiler.utils.RxTool;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;
import com.library.constantStorage.ConstantStorage;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/28
 * 描    述：
 * ================================================
 */
public class StmtTotalModel {
    /**
     * code : 1
     * msg : 请求成功
     * obj : {"stmtinfo":{"setl_optins":"","stmt_rslt":"1","stmt_rslt_dscr":"医疗费总额与医保中心数据不一致!个人账户支出总额与医保中心数据不一致!"}}
     */

    private String code;
    private String msg;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * setl_optins :
         * stmt_rslt : 1
         * stmt_rslt_dscr : 医疗费总额与医保中心数据不一致!个人账户支出总额与医保中心数据不一致!
         */
        private String setl_optins;
        private String stmt_rslt;
        private String stmt_rslt_dscr;

        public String getSetl_optins() {
            return setl_optins;
        }

        public void setSetl_optins(String setl_optins) {
            this.setl_optins = setl_optins;
        }

        public String getStmt_rslt() {
            return stmt_rslt;
        }

        public void setStmt_rslt(String stmt_rslt) {
            this.stmt_rslt = stmt_rslt;
        }

        public String getStmt_rslt_dscr() {
            return stmt_rslt_dscr;
        }

        public void setStmt_rslt_dscr(String stmt_rslt_dscr) {
            this.stmt_rslt_dscr = stmt_rslt_dscr;
        }
    }

    //结算对总账
    public static void sendStmtTotalRequest(final String TAG, String parma, final CustomerJsonCallBack<StmtTotalModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STMTTOTAL_URL, parma, callback);
    }
}
