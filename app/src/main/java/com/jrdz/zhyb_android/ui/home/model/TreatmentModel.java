package com.jrdz.zhyb_android.ui.home.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/28
 * 描    述：
 * ================================================
 */
public class TreatmentModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-01-28 16:10:34
     * data : [{"begndate":"2022-01-28 16:10:34","enddate":null,"fund_pay_type":"310200","psn_no":"61000006000000000010866022","trt_chk_rslt":"","trt_chk_type":"01","trt_enjymnt_flag":"1"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * begndate : 2022-01-28 16:10:34
         * enddate : null
         * fund_pay_type : 310200
         * psn_no : 61000006000000000010866022
         * trt_chk_rslt :
         * trt_chk_type : 01
         * trt_enjymnt_flag : 1
         */

        private String begndate;
        private String enddate;
        private String fund_pay_type;
        private String fund_pay_type_name;
        private String psn_no;
        private String trt_chk_rslt;
        private String trt_chk_type;
        private String trt_enjymnt_flag;
        private String trt_enjymnt_flag_name;

        public String getBegndate() {
            return begndate;
        }

        public void setBegndate(String begndate) {
            this.begndate = begndate;
        }

        public String getEnddate() {
            return enddate;
        }

        public void setEnddate(String enddate) {
            this.enddate = enddate;
        }

        public String getFund_pay_type() {
            return fund_pay_type;
        }

        public void setFund_pay_type(String fund_pay_type) {
            this.fund_pay_type = fund_pay_type;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getTrt_chk_rslt() {
            return trt_chk_rslt;
        }

        public void setTrt_chk_rslt(String trt_chk_rslt) {
            this.trt_chk_rslt = trt_chk_rslt;
        }

        public String getTrt_chk_type() {
            return trt_chk_type;
        }

        public void setTrt_chk_type(String trt_chk_type) {
            this.trt_chk_type = trt_chk_type;
        }

        public String getTrt_enjymnt_flag() {
            return trt_enjymnt_flag;
        }

        public void setTrt_enjymnt_flag(String trt_enjymnt_flag) {
            this.trt_enjymnt_flag = trt_enjymnt_flag;
        }

        public String getFund_pay_type_name() {
            return fund_pay_type_name;
        }

        public void setFund_pay_type_name(String fund_pay_type_name) {
            this.fund_pay_type_name = fund_pay_type_name;
        }

        public String getTrt_enjymnt_flag_name() {
            return trt_enjymnt_flag_name;
        }

        public void setTrt_enjymnt_flag_name(String trt_enjymnt_flag_name) {
            this.trt_enjymnt_flag_name = trt_enjymnt_flag_name;
        }
    }

    //待遇检查
    public static void sendTreatmentRequest(final String TAG, String psn_no, String insutype,String med_type,String insuplc_admdvs,
                                            final CustomerJsonCallBack<TreatmentModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("psn_no", psn_no);
        jsonObject.put("insutype", insutype);
        jsonObject.put("med_type", med_type);
        jsonObject.put("dise_codg", "");

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_GETTREATMENT_URL, jsonObject.toJSONString(),insuplc_admdvs, callback);
    }
}
