package com.jrdz.zhyb_android.ui.index.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.countDownTime.CustomCountDownTimer;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.permission.PermissionHelper;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;
import com.hjq.shape.view.ShapeEditText;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.AddDoctorActivity;
import com.jrdz.zhyb_android.ui.index.model.ActivateDeviceModel;
import com.jrdz.zhyb_android.ui.login.activity.LoginActivity;
import com.jrdz.zhyb_android.utils.DeviceID;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils;
import com.tencent.bugly.crashreport.CrashReport;

import java.math.BigDecimal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/8
 * 描    述：激活设备
 * ================================================
 */
public class ActivateDeviceActivity extends BaseActivity implements CompressUploadSinglePicUtils.IChoosePic {
    private EditText mEtFixmedinsCode;
    private EditText mEtFixmedinsName;
    private FrameLayout mFlCert;
    private ImageView mIvAddLicense, mIvAddphoto, mIvCert, mIvDelete;
    private EditText mEtFixmedinsPhone;
    private ShapeEditText mEtManaName;
    private ShapeEditText mEtManaPwd;
    private EditText mEtManaVerificationCode;
    private TextView mTvTime;
    private RadioGroup mSrbGroup;
    private ShapeTextView mTvActivate;

    private PermissionHelper permissionHelper;
    private String guid = "";
    private CompressUploadSinglePicUtils compressUploadSinglePicUtils;
    private CustomCountDownTimer mCustomCountDownTimer;

    @Override
    public int getLayoutId() {
        return R.layout.activity_activate_device;
    }

    @Override
    public void initView() {
        super.initView();
        mEtFixmedinsCode = findViewById(R.id.et_fixmedins_code);
        mEtFixmedinsName = findViewById(R.id.et_fixmedins_name);
        mIvAddLicense = findViewById(R.id.iv_add_license);
        mIvAddphoto = findViewById(R.id.iv_add_photo);
        mFlCert = findViewById(R.id.fl_cert);
        mIvCert = findViewById(R.id.iv_cert);
        mIvDelete = findViewById(R.id.iv_delete);
        mSrbGroup = findViewById(R.id.srb_group);
        mEtManaName = findViewById(R.id.et_mana_name);
        mEtFixmedinsPhone = findViewById(R.id.et_fixmedins_phone);
        mEtManaPwd = findViewById(R.id.et_mana_pwd);
        mEtManaVerificationCode = findViewById(R.id.et_mana_verificationCode);
        mTvTime = findViewById(R.id.tv_time);
        mTvActivate = findViewById(R.id.tv_activate);
    }

    @Override
    public void initData() {
        super.initData();
        // TODO: 2022/2/16 代码填充测试数据
//        mEtFixmedinsCode.setText("H61080200145");
//        mEtFixmedinsName.setText("榆林市第二医院");
//        mEtFixmedinsPhone.setText("13859005801");

        //正式环境 用户账户 不能随便用
//        mEtFixmedinsCode.setText("H61082401378");
//        mEtFixmedinsName.setText("西新区阳光卫生室");
//        mEtFixmedinsPhone.setText("13859005801");
        //2.申请存储权限
        getPermission();

        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mIvAddLicense.setOnClickListener(this);
        mIvCert.setOnClickListener(this);
        mIvDelete.setOnClickListener(this);
        mTvTime.setOnClickListener(this);
        mTvActivate.setOnClickListener(this);
    }

    @Override
    protected void initWaterMark() {
        //禁用页面水印
    }

    //申请存储权限
    private void getPermission() {
        permissionHelper = new PermissionHelper();
        permissionHelper.requestPermission(this, new PermissionHelper.onPermissionListener() {
            @Override
            public void onSuccess() {
                showWaitDialog();
                ThreadUtils.getSinglePool().execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            guid = DeviceID.getAndroidUniId();//f9665089da844c75ab11442860d1f2de
                            // 也可以通过CrashReport类设置，适合无法在初始化sdk时获取到deviceId的场景，context和deviceId不能为空（或空字符串）
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!EmptyUtils.isEmpty(guid)) {
                                        CrashReport.setDeviceId(ActivateDeviceActivity.this, guid);
                                    }
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        hideWaitDialog();
                    }
                });
            }

            @Override
            public void onNoAllSuccess(String noAllSuccessText) {

            }

            @Override
            public void onFail(String failText) {
                showShortToast("权限申请失败，app将不能激活");
            }
        }, "存储权限:注册时,需要存储app的唯一码到手机", Permission.MANAGE_EXTERNAL_STORAGE);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.iv_add_license://添加证件信息
                KeyboardUtils.hideSoftInput(ActivateDeviceActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(CompressUploadSinglePicUtils.PIC_ACTIVATEDEVICE_TAG);
                break;
            case R.id.iv_cert://查看图片
                OpenImage.with(ActivateDeviceActivity.this)
                        //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                        .setClickImageView(mIvCert)
                        //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                        .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                        //RecyclerView的数据
                        .setImageUrl(String.valueOf(mIvCert.getTag(R.id.tag_2)), MediaType.IMAGE)
                        //点击的ImageView所在数据的位置
                        .setClickPosition(0)
                        //开始展示大图
                        .show();
                break;
            case R.id.iv_delete://删除证件信息
                mIvAddLicense.setVisibility(View.VISIBLE);
                mIvAddphoto.setVisibility(View.VISIBLE);
                mIvAddLicense.setEnabled(true);

                mIvCert.setTag(R.id.tag_1, "");
                mIvCert.setTag(R.id.tag_2, "");
                mFlCert.setVisibility(View.GONE);
                break;
            case R.id.tv_time://获取短信验证码
                // 2022-07-25 先请求接口 接口返回发送短信验证码成功 然后开始倒计时
                sendSms();
                break;
            case R.id.tv_activate://激活
                activate();
                break;
        }
    }

    //发送短信
    private void sendSms() {
        if (EmptyUtils.isEmpty(mEtFixmedinsPhone.getText().toString().trim()) || mEtFixmedinsPhone.getText().toString().length() < 11) {
            showShortToast("手机号码格式不正确!");
            return;
        }
        showWaitDialog();
        BaseModel.sendSendsmsRequest(TAG, mEtFixmedinsPhone.getText().toString().trim(), "1", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();

                //发送短信验证码成功
                mTvTime.setEnabled(false);
                showShortToast("验证码已发送");
                mTvTime.setTextColor(getResources().getColor(R.color.color_ff0202));
                setTimeStart(60000);
            }
        });
    }

    //开启定时器  millisecond:倒计时的时间
    private void setTimeStart(long millisecond) {
        //防止内存泄漏
        if (null != mCustomCountDownTimer) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        mCustomCountDownTimer = new CustomCountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long S = millisUntilFinished / 1000;
                mTvTime.setText(S < 10 ? "0" + S + "s" : S + "s");
            }

            @Override
            public void onFinish() {
                mTvTime.setEnabled(true);
                mTvTime.setTextColor(getResources().getColor(R.color.color_4870e0));
                mTvTime.setText("获取验证码");
            }
        };

        mCustomCountDownTimer.start();
    }

    //激活设备
    private void activate() {
        if (EmptyUtils.isEmpty(mEtFixmedinsCode.getText().toString())) {
            showShortToast("请输入医药机构编码");
            return;
        }

        if (EmptyUtils.isEmpty(mEtFixmedinsName.getText().toString())) {
            showShortToast("请输入医药机构名称");
            return;
        }

        if (EmptyUtils.isEmpty(mEtManaName.getText().toString())) {
            showShortToast("请输入负责人姓名");
            return;
        }

        if (EmptyUtils.isEmpty(mEtFixmedinsPhone.getText().toString())) {
            showShortToast("请输入联系人手机号");
            return;
        }

        if (!"1".equals(String.valueOf(mEtFixmedinsPhone.getText().charAt(0))) || mEtFixmedinsPhone.getText().length() != 11) {
            showShortToast("请输入正确的手机号码");
            return;
        }

        if (EmptyUtils.isEmpty(mEtManaPwd.getText().toString()) || mEtManaPwd.getText().toString().length() < 6) {
            showShortToast("密码由6~12位数字组成!");
            return;
        }

//        if (EmptyUtils.isEmpty(mEtManaVerificationCode.getText().toString())) {
//            showShortToast("请输入验证码");
//            return;
//        }

        if (EmptyUtils.isEmpty(guid)) {
            showShortToast("请进入应用设置页面，开启存储权限");
            return;
        }

        String fixmedins_type = "1";
        if (mSrbGroup.getCheckedRadioButtonId() == R.id.srb_pharmacy) {
            fixmedins_type = "2";
        }

        showWaitDialog();
        ActivateDeviceModel.sendActivateDeviceRequest(TAG, mEtFixmedinsCode.getText().toString(), mEtFixmedinsName.getText().toString(), guid,
                mEtFixmedinsPhone.getText().toString(), fixmedins_type, mIvCert.getTag(R.id.tag_1) == null ? "" : String.valueOf(mIvCert.getTag(R.id.tag_1)),
                mEtManaName.getText().toString(), mEtManaPwd.getText().toString(), new CustomerJsonCallBack<ActivateDeviceModel>() {
                    @Override
                    public void onRequestError(ActivateDeviceModel returnData, String msg) {
                        hideWaitDialog();
                        showTipDialog(msg);
                    }

                    @Override
                    public void onRequestSuccess(ActivateDeviceModel returnData) {
                        //跳转首页
                        hideWaitDialog();
                        showShortToast(EmptyUtils.strEmptyToText(returnData.getMsg(), "激活成功，请登录"));
                        LoginActivity.newIntance(ActivateDeviceActivity.this);
                        finish();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == XXPermissions.REQUEST_CODE) {
            showWaitDialog();
            ThreadUtils.getSinglePool().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        guid = DeviceID.getAndroidUniId();//f9665089da844c75ab11442860d1f2de
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    hideWaitDialog();
                }
            });
        }
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String accessoryId, String url, int width, int height) {//2.269
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCert.getLayoutParams();
            if (radio >= 2.269) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_590));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_590))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_260) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_260));
            }

            mIvCert.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCert);

            mIvAddLicense.setVisibility(View.GONE);
            mIvAddphoto.setVisibility(View.GONE);
            mIvAddLicense.setEnabled(false);

            mIvCert.setTag(R.id.tag_1, accessoryId);
            mIvCert.setTag(R.id.tag_2, url);
            mFlCert.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }
    //--------------------------选择图片----------------------------------------------------

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }

        if (mCustomCountDownTimer != null) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, ActivateDeviceActivity.class);
        context.startActivity(intent);
    }
}
