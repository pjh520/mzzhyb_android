package com.jrdz.zhyb_android.ui.index.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.alipay.mobile.android.verify.sdk.MPVerifyService;
import com.alipay.mobile.android.verify.sdk.ServiceFactory;
import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.common.BaiduMapSDKException;
import com.frame.bugly_library.BuglyUtils;
import com.frame.compiler.manager.AppStatusManager;
import com.frame.compiler.utils.BroadCastReceiveUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.base.baseWebview.BaseWebviewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.MainActivity;
import com.jrdz.zhyb_android.ui.index.model.DataDicListModel;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredMainActivity;
import com.jrdz.zhyb_android.ui.login.activity.LoginActivity;
import com.jrdz.zhyb_android.ui.mine.activity.AboutUsActivity;
import com.jrdz.zhyb_android.utils.AppManager_Acivity;
import com.jrdz.zhyb_android.utils.DeviceID;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MMKVUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.widget.pop.PrivacyAgreementPop;
import com.library.jpush_library.JPushUtils;
import com.tencent.bugly.crashreport.CrashReport;

import org.litepal.LitePal;

import java.util.Iterator;
import java.util.List;

/**
 * ================================================
 * 项目名称：dgonline-android
 * 包    名：com.aten.dgonline_android.ui.index
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2018/10/24
 * 描    述：欢迎页面（启动页面）
 * ================================================
 */
public class WelcomeActivity extends BaseActivity implements PrivacyAgreementPop.IOptionListener {
    private ImageView mAdImage;
    private int count = 0;
    private PrivacyAgreementPop privacyAgreementPop;

    @Override
    public boolean FirstOnCreate() {
        /**在应用的入口activity加入以下代码，解决首次安装应用，在安装界面直接点击打开.点击home健回到桌面，
         * 再次点击应用图标，进入应用时多次初始化SplashActivity的问题
         * */
        if (!this.isTaskRoot()) {
            Intent intent = getIntent();
            if (intent != null) {
                String action = intent.getAction();
                if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(action)) {
                    finish();
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    protected void afterSetcontentView() {
        getWindow().setBackgroundDrawable(null);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        //app状态改为正常
        AppStatusManager.getInstance().setAppStatus(AppStatusManager.STATUS_NORMAL);
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_welcome;
    }

    @Override
    public void initView() {
        super.initView();

        mAdImage = findViewById(R.id.ad_image);
    }

    @Override
    protected void initImmersionBar() {
        mImmersionBar = ImmersionBar.with(this);
        mImmersionBar.init();
    }

    @Override
    public boolean hasNeedTitle() {
        return false;
    }

    @Override
    public void initData() {
        super.initData();
        mAdImage.postDelayed(new Runnable() {
            @Override
            public void run() {
                showPrivacyAgreementPop();
            }
        }, 300);
    }

    @Override
    protected void initWaterMark() {
        //禁用页面水印
    }

    //弹出用户协议
    private void showPrivacyAgreementPop() {
        if (!MMKVUtils.getBoolean("privacyAgreemen")) {
            privacyAgreementPop = new PrivacyAgreementPop(WelcomeActivity.this, this);
            privacyAgreementPop.setOutSideDismiss(false).setBackPressEnable(false).showPopupWindow();
        } else {
            getActivateStatus();
        }
    }

    @Override
    public void onAgree() {
        BroadCastReceiveUtils.sendLocalBroadCast(this, Constants.Action.SEND_AGREE_PRIVACY);
        BuglyUtils.getInstance().initBugly(getApplication(), Constants.Configure.BUGLY_APPID, Constants.Configure.BUGLY_LOG_ENABLE);
        //允许百度地图运行
        SDKInitializer.setAgreePrivacy(getApplicationContext(), true);
        // 在使用 SDK 各组间之前初始化 context 信息，传入 ApplicationContext
        // 默认本地个性化地图初始化方法
        try {
            SDKInitializer.initialize(getApplicationContext());
            //自4.3.0起，百度地图SDK所有接口均支持百度坐标和国测局坐标，用此方法设置您使用的坐标类型.
            //包括BD09LL和GCJ02两种坐标，默认是BD09LL坐标。
            SDKInitializer.setCoordType(CoordType.BD09LL);
        } catch (BaiduMapSDKException e) {

        }
        //允许极光推送运行
        JPushUtils.init(getApplicationContext());
        getActivateStatus();
    }

    @Override
    public void onCancle() {
        //如果开发者调用kill或者exit之类的方法杀死进程，请务必在此之前调用onKillProcess方法，用来保存统计数据。
        AppManager_Acivity.getInstance().AppExit();
    }

    @Override
    public void goUserAgreement() {
        MyWebViewActivity.newIntance(WelcomeActivity.this, "用户协议", Constants.BASE_URL + Constants.WebUrl.USER_AGREEMENT_URL, true, false);
    }

    @Override
    public void goPrivacyPolicy() {
        MyWebViewActivity.newIntance(WelcomeActivity.this, "隐私政策", Constants.BASE_URL + Constants.WebUrl.PRIVACY_POLICY_URL, true, false);
    }

    //获取用户激活状态
    private void getActivateStatus() {
        showWaitDialog("数据更新中...");
        //判断用户是否已激活
        if (MechanismInfoUtils.isActivate()) {//已激活 更新下唯一码
            updateUniCode();
        } else {//未激活 获取到字典数据后 直接跳转参保人首页
            getDataDicList();
        }
    }

    //更新唯一值
    private void updateUniCode() {
        try {
            String androidUniId = DeviceID.getAndroidUniId();//f9665089da844c75ab11442860d1f2de
            // 也可以通过CrashReport类设置，适合无法在初始化sdk时获取到deviceId的场景，context和deviceId不能为空（或空字符串）
            if (!EmptyUtils.isEmpty(androidUniId)) {
                CrashReport.setDeviceId(this, androidUniId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getDataDicList();
    }

    //获取字典数据
    private void getDataDicList() {
        DataDicListModel.sendDataDicListRequest(TAG, MMKVUtils.getString("ver", "19000101000000"), new CustomerJsonCallBack<DataDicListModel>() {
            @Override
            public void onRequestError(DataDicListModel returnData, String msg) {
                hideWaitDialog();
                //请求错误的情况 弹框提示用户是否重试
                showErrorTip(msg);
            }

            @Override
            public void onRequestSuccess(DataDicListModel returnData) {
                saveData(returnData.getMax_ver(), returnData.getData());
            }
        });
    }

    //保存字典数据到数据库
    private void saveData(String maxVer, List<DataDicModel> datas) {
        if (datas != null && !datas.isEmpty()) {
            DataDicModel dataDicModel = LitePal.findFirst(DataDicModel.class);
            if (dataDicModel == null) {//首次存储数据
                saveDataDicFirst(maxVer, datas);
            } else {//已存在字典数据，这时候应该增量更新
                saveDataDic(maxVer, datas);
            }
        } else {
            hideWaitDialog();
            goMainPager();
        }
    }

    //首次存储字典表数据
    private void saveDataDicFirst(String maxVer, List<DataDicModel> datas) {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                Iterator<DataDicModel> iterator = datas.iterator();
                while (iterator.hasNext()) {
                    DataDicModel dataDicModel = iterator.next();
                    if ("0".equals(dataDicModel.getIsEnable())) {//开启 数据改变
                        iterator.remove();
                    }
                }
                boolean isSaveSuccess = LitePal.saveAll(datas);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isSaveSuccess) {
                            if (EmptyUtils.isEmpty(maxVer)) {
                                showShortToast("返回版本号有误");
                            } else {
                                MMKVUtils.putString("ver", maxVer);//存储数据字典版本号
                            }

                            hideWaitDialog();
                            goMainPager();
                        } else {
                            if (0 == count) {
                                showShortToast("字典存储失败，重新存储");
                                count++;
                                saveData(maxVer, datas);
                            } else {
                                hideWaitDialog();
                                showShortToast("存储空间不足。");
                                goMainPager();
                            }
                        }
                    }
                });
            }
        });
    }

    //已存在字典数据，这时候应该增量更新
    private void saveDataDic(String maxVer, List<DataDicModel> datas) {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                for (DataDicModel data : datas) {
                    List<DataDicModel> dataDicModels = LitePal.where("type=? and value=?", data.getType(), data.getValue()).find(DataDicModel.class);
                    if (dataDicModels != null && !dataDicModels.isEmpty()) {
                        if ("1".equals(data.getIsEnable())) {//开启 数据改变
                            data.update(dataDicModels.get(0).getId());
                        } else {//删除该数据
                            for (DataDicModel dataDicModel : dataDicModels) {
                                dataDicModel.delete();
                            }
                        }
                    } else {
                        if ("1".equals(data.getIsEnable())) {//开启 数据改变
                            data.save();
                        }
                    }
                }

                if (EmptyUtils.isEmpty(maxVer)) {
                    showShortToast("返回版本号有误");
                } else {
                    MMKVUtils.putString("ver", maxVer);//存储数据字典版本号
                }
                hideWaitDialog();
                goMainPager();
            }
        });
    }

    //进入首页
    public void goMainPager() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (LoginUtils.isLogin()) {
                    MainActivity.newIntance(WelcomeActivity.this, 0);
                } else {
                    InsuredMainActivity.newIntance(WelcomeActivity.this, 0);
//                    LoginActivity.newIntance(WelcomeActivity.this);
                }
                overridePendingTransition(R.anim.screen_zoom_in, R.anim.screen_zoom_out);
                finish();
            }
        });
    }

    @Override
    public void onCloseLoad() {
        super.onCloseLoad();
        AppManager_Acivity.getInstance().AppExit();
    }

    @Override
    public void onRetryLoad() {
        super.onRetryLoad();

        mAdImage.postDelayed(new Runnable() {
            @Override
            public void run() {
                showWaitDialog("数据更新中...");
                getDataDicList();
            }
        }, 500);
    }

    //Activity屏蔽物理返回按钮
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (privacyAgreementPop != null) {
            privacyAgreementPop.onCleanListener();
            privacyAgreementPop.dismiss();
            privacyAgreementPop = null;
        }
    }
}
