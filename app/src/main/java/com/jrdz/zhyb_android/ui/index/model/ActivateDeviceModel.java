package com.jrdz.zhyb_android.ui.index.model;

import com.alibaba.fastjson.JSONObject;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.MD5Util;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/8
 * 描    述：
 * ================================================
 */
public class ActivateDeviceModel {
    /**
     * code : 1
     * msg : 请求成功
     * obj : {"admdvs":"610802","fixmedins_name":"榆阳区安定精神病医院","fixmedins_type":"1","hosp_lv":"11","token":"dfa982df-4b3b-49da-a2ec-122227caa28e","uscc":"526108023522537499"}
     */

    private String code;
    private String msg;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * admdvs : 610802
         * fixmedins_code : X4S545S544D11F2F2F233E6E
         * fixmedins_name : 榆阳区安定精神病医院
         * fixmedins_type : 1
         * hosp_lv : 11
         * medinsLv : 2
         * uscc : 526108023522537499
         */

        private String admdvs;
        private String fixmedins_code;
        private String fixmedins_name;
        private String fixmedins_type;
        private String hosp_lv;
        private String medinsLv;
        private String uscc;
        private String Unions;
        private String phone;
        private String ElectronicPrescription;
        private String OutpatientMdtrtinfo;
        private String accessoryId;
        private String AccessoryUrl;
        //智慧药房版本-新增店铺审核状态
        private String ApproveStatus;//（0、从未提交审核1、审核中2、审核未通过3、审核通过4、禁止审核）
        //智慧药房版本-新增店铺入驻模式
        private String EntryMode;//(0、无模式1、固定抽佣模式2、管理服务模式)
        //智慧药房版本-订单语音提醒
        private String VoiceNotification;//(1开启0关闭)
        private String IsEnterpriseFundPay;//是否允许企业基金支付（0不允许 1允许）
        private String IsOTO;//（1是0不是）是否是O2Ｏ商家

        public String getAdmdvs() {
            return admdvs;
        }

        public void setAdmdvs(String admdvs) {
            this.admdvs = admdvs;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getFixmedins_name() {
            return fixmedins_name;
        }

        public void setFixmedins_name(String fixmedins_name) {
            this.fixmedins_name = fixmedins_name;
        }

        public String getFixmedins_type() {
            return fixmedins_type;
        }

        public void setFixmedins_type(String fixmedins_type) {
            this.fixmedins_type = fixmedins_type;
        }

        public String getHosp_lv() {
            return hosp_lv;
        }

        public void setHosp_lv(String hosp_lv) {
            this.hosp_lv = hosp_lv;
        }

        public String getMedinsLv() {
            return medinsLv;
        }

        public void setMedinsLv(String medinsLv) {
            this.medinsLv = medinsLv;
        }

        public String getUscc() {
            return uscc;
        }

        public void setUscc(String uscc) {
            this.uscc = uscc;
        }

        public String getUnions() {
            return Unions;
        }

        public void setUnions(String unions) {
            Unions = unions;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getElectronicPrescription() {
            return ElectronicPrescription;
        }

        public void setElectronicPrescription(String electronicPrescription) {
            ElectronicPrescription = electronicPrescription;
        }

        public String getOutpatientMdtrtinfo() {
            return OutpatientMdtrtinfo;
        }

        public void setOutpatientMdtrtinfo(String outpatientMdtrtinfo) {
            OutpatientMdtrtinfo = outpatientMdtrtinfo;
        }

        public String getAccessoryId() {
            return accessoryId;
        }

        public void setAccessoryId(String accessoryId) {
            this.accessoryId = accessoryId;
        }

        public String getAccessoryUrl() {
            return AccessoryUrl;
        }

        public void setAccessoryUrl(String accessoryUrl) {
            AccessoryUrl = accessoryUrl;
        }

        public String getApproveStatus() {
            return ApproveStatus;
        }

        public void setApproveStatus(String approveStatus) {
            ApproveStatus = approveStatus;
        }

        public String getEntryMode() {
            return EntryMode;
        }

        public void setEntryMode(String entryMode) {
            EntryMode = entryMode;
        }

        public String getVoiceNotification() {
            return VoiceNotification;
        }

        public void setVoiceNotification(String voiceNotification) {
            VoiceNotification = voiceNotification;
        }

        public String getIsEnterpriseFundPay() {
            return IsEnterpriseFundPay;
        }

        public void setIsEnterpriseFundPay(String isEnterpriseFundPay) {
            IsEnterpriseFundPay = isEnterpriseFundPay;
        }

        public String getIsOTO() {
            return IsOTO;
        }

        public void setIsOTO(String isOTO) {
            IsOTO = isOTO;
        }
    }

    public static void sendActivateDeviceRequest(final String TAG, String fixmedins_code, String fixmedins_name,String guid,
                                                 String fixmedins_phone, String fixmedins_type, String accessoryId,String Director,
                                                 String password,final CustomerJsonCallBack<ActivateDeviceModel> callback) {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("fixmedins_phone", fixmedins_phone);
        jsonObject.put("fixmedins_type", fixmedins_type);
        jsonObject.put("accessoryId", accessoryId);
        //手机厂商 deviceBrand   手机型号 systemModel  系统版本号 systemVersion
        jsonObject.put("deviceBrand", EmptyUtils.strEmpty(android.os.Build.BRAND));
        jsonObject.put("systemModel", EmptyUtils.strEmpty(android.os.Build.MODEL));
        jsonObject.put("systemVersion", EmptyUtils.strEmpty(android.os.Build.VERSION.RELEASE));

        jsonObject.put("Director", Director);//负责人名字
        jsonObject.put("password", MD5Util.up32(password));//密码
//        jsonObject.put("verificationCode", verificationCode);//验证码

        RequestData.requesNetWork_Json2(TAG,fixmedins_code,fixmedins_name,guid,Constants.BASE_URL+ Constants.Api.GET_ACTIVATEDEVICE_URL,jsonObject.toJSONString(), callback);
    }

    public static void sendQueryMedinsinfoRequest(final String TAG, String fixmedins_code,String fixmedins_name,
                                                 final CustomerJsonCallBack<ActivateDeviceModel> callback) {
        RequestData.requesNetWork_Json4(TAG,fixmedins_code,fixmedins_name,Constants.BASE_URL+ Constants.Api.GET_QUERYMEDINSINFO_URL,"", callback);
    }
}
