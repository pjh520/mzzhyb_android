package com.jrdz.zhyb_android.ui.index.model;

import com.alibaba.fastjson.JSONObject;
import com.frame.compiler.utils.RxTool;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;
import com.jrdz.zhyb_android.net.RequestData;
import com.library.constantStorage.ConstantStorage;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.login.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/11
 * 描    述：
 * ================================================
 */
public class DataDicListModel{
    /**
     * code : 1
     * msg : 请求成功
     * max_ver : "20220116145208"
     * obj : [{"label":"定点医药机构","sort":1,"type":"dcla_souc","values":"01","isEnable": 1}]
     */
    private String code;
    private String msg;
    private String max_ver;
    private List<DataDicModel> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMax_ver() {
        return max_ver;
    }

    public void setMax_ver(String max_ver) {
        this.max_ver = max_ver;
    }

    public List<DataDicModel> getData() {
        return data;
    }

    public void setData(List<DataDicModel> data) {
        this.data = data;
    }

    //获取字典表数据
    public static void sendDataDicListRequest(final String TAG,String ver,final CustomerJsonCallBack<DataDicListModel> callback) {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("ver", ver);
        InsuredRequestData.requesNetWork_Json(TAG,Constants.BASE_URL+ Constants.Api.GET_DATADICLIST_URL,jsonObject.toJSONString(), callback);
    }
}
