package com.jrdz.zhyb_android.ui.insured.activity;


import android.graphics.Bitmap;

import androidx.annotation.NonNull;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.model.SpecialDrugDetailModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.FreeBackPicModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-18
 * 描    述：特殊药品备案-重新备案
 * ================================================
 */
public class AgainSpecialDrugRegActivity extends SpecialDrugRegActivity {
    protected String specialDrugFilingId;

    @Override
    public void initData() {
        specialDrugFilingId = getIntent().getStringExtra("specialDrugFilingId");
        super.initData();

        showWaitDialog();
        getPageData();
    }

    //获取页面数据
    private void getPageData() {
        SpecialDrugDetailModel.sendSpecialDrugDetailRequest(TAG, specialDrugFilingId, new CustomerJsonCallBack<SpecialDrugDetailModel>() {
            @Override
            public void onRequestError(SpecialDrugDetailModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(SpecialDrugDetailModel returnData) {
                hideWaitDialog();
                setPageData(returnData.getData());
            }
        });
    }

    //设置页面数据
    protected void setPageData(SpecialDrugDetailModel.DataBean data) {
        if (data == null) return;
        mEtPerName.setText(EmptyUtils.strEmpty(data.getName()));
        mEtPerCertno.setText(EmptyUtils.strEmpty(data.getId_card_no()));
        mEtPerPhone.setText(EmptyUtils.strEmpty(data.getPhone()));

        //添加备案基础资料
        List<SpecialDrugDetailModel.DataBean.MedInsuDirListBean> med_insu_dir_list = data.getMed_insu_dir_list();
        if (med_insu_dir_list != null) {
            for (SpecialDrugDetailModel.DataBean.MedInsuDirListBean medInsuDirListBean : med_insu_dir_list) {
                onBasisDataCommit(medInsuDirListBean);
            }
        }

        //添加图片增加按钮数据-申请表资料
        List<SpecialDrugDetailModel.DataBean.SpecialDrugPicBean> applicationFormAccessoryUrl = data.getApplicationFormAccessoryUrl1();
        if (applicationFormAccessoryUrl != null) {
            for (int i = applicationFormAccessoryUrl.size() - 1; i >= 0; i--) {
                SpecialDrugDetailModel.DataBean.SpecialDrugPicBean specialDrugPicBean = applicationFormAccessoryUrl.get(i);
                freeBackPicAdapter01.getData().add(0, new FreeBackPicModel(specialDrugPicBean.getAccessoryId(), Constants.BASE_URL + specialDrugPicBean.getAccessoryUrl(), PIC_NOR));
            }
        }
        freeBackPicAdapter01.notifyDataSetChanged();
        //添加图片增加按钮数据-处方资料
        List<SpecialDrugDetailModel.DataBean.SpecialDrugPicBean> prescriptionAccessoryUrl = data.getPrescriptionAccessoryUrl1();
        if (prescriptionAccessoryUrl != null) {
            for (int i = prescriptionAccessoryUrl.size() - 1; i >= 0; i--) {
                SpecialDrugDetailModel.DataBean.SpecialDrugPicBean specialDrugPicBean = prescriptionAccessoryUrl.get(i);
                freeBackPicAdapter02.getData().add(0, new FreeBackPicModel(specialDrugPicBean.getAccessoryId(), Constants.BASE_URL + specialDrugPicBean.getAccessoryUrl(), PIC_NOR));
            }
        }
        freeBackPicAdapter02.notifyDataSetChanged();
        //添加图片增加按钮数据-诊断资料
        List<SpecialDrugDetailModel.DataBean.SpecialDrugPicBean> diagnosisAccessoryUrl = data.getDiagnosisAccessoryUrl1();
        if (diagnosisAccessoryUrl != null) {
            for (int i = diagnosisAccessoryUrl.size() - 1; i >= 0; i--) {
                SpecialDrugDetailModel.DataBean.SpecialDrugPicBean specialDrugPicBean = diagnosisAccessoryUrl.get(i);
                freeBackPicAdapter03.getData().add(0, new FreeBackPicModel(specialDrugPicBean.getAccessoryId(), Constants.BASE_URL + specialDrugPicBean.getAccessoryUrl(), PIC_NOR));
            }
        }
        freeBackPicAdapter03.notifyDataSetChanged();
        //添加图片增加按钮数据-病历资料
        List<SpecialDrugDetailModel.DataBean.SpecialDrugPicBean> caseAccessoryUrl = data.getCaseAccessoryUrl1();
        if (caseAccessoryUrl != null) {
            for (int i = caseAccessoryUrl.size() - 1; i >= 0; i--) {
                SpecialDrugDetailModel.DataBean.SpecialDrugPicBean specialDrugPicBean = caseAccessoryUrl.get(i);
                freeBackPicAdapter04.getData().add(0, new FreeBackPicModel(specialDrugPicBean.getAccessoryId(), Constants.BASE_URL + specialDrugPicBean.getAccessoryUrl(), PIC_NOR));
            }
        }
        freeBackPicAdapter04.notifyDataSetChanged();
        //签名数据
        commitmentLetterAccessoryId = data.getCommitmentLetterAccessoryId();
        GlideUtils.getImageWidHeig(this, data.getCommitmentLetterAccessoryUrl(), new GlideUtils.IGetImageData() {
            @Override
            public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                setSignature(data.getCommitmentLetterAccessoryUrl(), width, height);
            }
        });
    }

    @Override
    protected void commit() {
        if (EmptyUtils.isEmpty(mEtPerName.getText().toString())) {
            showShortToast("请填写备案姓名");
            goSpecifyLocation(mLlFixed.getTop());
            return;
        }
        if (EmptyUtils.isEmpty(mEtPerCertno.getText().toString())) {
            showShortToast("请填写身份证号");
            goSpecifyLocation(mLlFixed.getTop() + getResources().getDimensionPixelSize(R.dimen.dp_87));
            return;
        }
        if (EmptyUtils.isEmpty(mEtPerPhone.getText().toString())) {
            showShortToast("请填写联系电话");
            goSpecifyLocation(mLlFixed.getTop() + 2 * getResources().getDimensionPixelSize(R.dimen.dp_87));
            return;
        }
        //添加数据的时候 是否有从表单直接添加一条数据
        isAddByForm=false;
        if (basicDataModels == null || basicDataModels.isEmpty()) {
            int basisDataPerfect = getBasisDataPerfect();

            if (basisDataPerfect<7) {
                showTipDialog("备案基础资料尚未填写完整，请先填写");
                goSpecifyLocation(mLlBasisDataTag.getTop());
                return;
            } else {
                SpecialDrugDetailModel.DataBean.MedInsuDirListBean medInsuDirListBean = new SpecialDrugDetailModel.DataBean.MedInsuDirListBean(
                        null == mEtFixmedinsName.getTag() ? "" : mEtFixmedinsName.getTag().toString(), mEtFixmedinsName.getText().toString(),
                        mEtDeptName.getText().toString(),
                        null == mEtDoctorName.getTag() ? "" : mEtDoctorName.getTag().toString(), mEtDoctorName.getText().toString(),
                        null == mEtDeptName.getTag() ? "" : mEtDeptName.getTag().toString(),
                        null == mEtDrugName.getTag(R.id.tag_1) ? "" : mEtDrugName.getTag(R.id.tag_1).toString(), mEtDrugName.getText().toString(),
                        mEtDrugNum.getText().toString(),
                        null == mEtDrugName.getTag(R.id.tag_2) ? "" : mEtDrugName.getTag(R.id.tag_2).toString(),
                        mEtStartDate.getText().toString(), mEtEndDate.getText().toString());
                isAddByForm=true;
                basicDataModels.add(medInsuDirListBean);
            }
        } else {
            int basisDataPerfect = getBasisDataPerfect();
            if (basisDataPerfect>0&&basisDataPerfect<7){
                showTipDialog2("第" + (basicDataModels.size() + 1) + "个药品信息尚未填写完整，确定要提交", new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                        goSpecifyLocation(mLlBasisDataTag.getTop());
                    }

                    @Override
                    public void onBtn02Click() {
                        continueCommit(isAddByForm);
                    }
                });
                return;
            }else if (basisDataPerfect==7){
                SpecialDrugDetailModel.DataBean.MedInsuDirListBean medInsuDirListBean = new SpecialDrugDetailModel.DataBean.MedInsuDirListBean(
                        null == mEtFixmedinsName.getTag() ? "" : mEtFixmedinsName.getTag().toString(), mEtFixmedinsName.getText().toString(),
                        mEtDeptName.getText().toString(),
                        null == mEtDoctorName.getTag() ? "" : mEtDoctorName.getTag().toString(), mEtDoctorName.getText().toString(),
                        null == mEtDeptName.getTag() ? "" : mEtDeptName.getTag().toString(),
                        null == mEtDrugName.getTag(R.id.tag_1) ? "" : mEtDrugName.getTag(R.id.tag_1).toString(), mEtDrugName.getText().toString(),
                        mEtDrugNum.getText().toString(),
                        null == mEtDrugName.getTag(R.id.tag_2) ? "" : mEtDrugName.getTag(R.id.tag_2).toString(),
                        mEtStartDate.getText().toString(), mEtEndDate.getText().toString());
                isAddByForm=true;
                basicDataModels.add(medInsuDirListBean);
            }
        }

        continueCommit(isAddByForm);
    }

    //继续提交

    @Override
    protected void continueCommit(boolean isAddByForm) {
        String applicationFormAccessoryId1 = getPicSplicingData(freeBackPicAdapter01);
        String prescriptionAccessoryId1 = getPicSplicingData(freeBackPicAdapter02);
        if (EmptyUtils.isEmpty(prescriptionAccessoryId1)) {
            showShortToast("处方为必填项");
            goSpecifyLocation(mLlImglistTag.getTop() + mLlImglist02Tag.getTop());
            if (isAddByForm){
                basicDataModels.remove(basicDataModels.size()-1);
            }
            return;
        }
        String diagnosisAccessoryId1 = getPicSplicingData(freeBackPicAdapter03);
        String caseAccessoryId1 = getPicSplicingData(freeBackPicAdapter04);

        showWaitDialog();
        BaseModel.sendAgainAddSpecialDrugRequest(TAG, specialDrugFilingId, mEtPerName.getText().toString(), mEtPerCertno.getText().toString(), mEtPerPhone.getText().toString(),
                applicationFormAccessoryId1, prescriptionAccessoryId1, diagnosisAccessoryId1, caseAccessoryId1, commitmentLetterAccessoryId, basicDataModels,
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                        if (isAddByForm){
                            basicDataModels.remove(basicDataModels.size()-1);
                        }
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast(returnData.getMsg());
                        setResult(RESULT_OK);
                        goFinish();
                    }
                });
    }
}
