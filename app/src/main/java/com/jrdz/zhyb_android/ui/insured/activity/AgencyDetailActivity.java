package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.insured.model.AgencyQueryModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-02-23
 * 描    述：经办机构详细页面
 * ================================================
 */
public class AgencyDetailActivity extends BaseActivity {
    private TextView mTvAgencyName;
    private TextView mTvAgencyTime;
    private TextView mTvAgencyPhone;
    private TextView mTvAgencyAddress;
    private TextView mTvAgencyRemark;

    private AgencyQueryModel.DataBean itemData;
    private CustomerDialogUtils customerDialogUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_agency_detail;
    }

    @Override
    public void initView() {
        super.initView();
        mTvAgencyName = findViewById(R.id.tv_agency_name);
        mTvAgencyTime = findViewById(R.id.tv_agency_time);
        mTvAgencyPhone = findViewById(R.id.tv_agency_phone);
        mTvAgencyAddress = findViewById(R.id.tv_agency_address);
        mTvAgencyRemark = findViewById(R.id.tv_agency_remark);
    }

    @Override
    public void initData() {
        itemData=getIntent().getParcelableExtra("itemData");
        super.initData();

        mTvAgencyPhone.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        mTvAgencyPhone.getPaint().setAntiAlias(true);

        mTvAgencyName.setText(EmptyUtils.strEmpty(itemData.getAgencyName()));
        mTvAgencyTime.setText("工作时间:"+itemData.getWorkTime());
        mTvAgencyPhone.setText(EmptyUtils.strEmpty(itemData.getTelephone()));
        mTvAgencyAddress.setText("机构地址:"+itemData.getAddress());
        mTvAgencyRemark.setText("备注:"+itemData.getRemark());
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvAgencyPhone.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_agency_phone:
                showPhone(itemData.getTelephone());
                break;
        }
    }

    //显示电话
    private void showPhone(String phone) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(AgencyDetailActivity.this, "提示", "是否拨打客服电话" + phone + "?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        RxTool.takePhone(AgencyDetailActivity.this, phone);
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context, AgencyQueryModel.DataBean itemData) {
        Intent intent = new Intent(context, AgencyDetailActivity.class);
        intent.putExtra("itemData", itemData);
        context.startActivity(intent);
    }
}
