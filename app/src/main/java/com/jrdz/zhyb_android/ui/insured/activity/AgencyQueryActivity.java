package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.model.KVModel;
import com.jrdz.zhyb_android.ui.insured.adapter.AgencyQueryAdapter;
import com.jrdz.zhyb_android.ui.insured.model.AgencyQueryModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-02-17
 * 描    述：经办机构查询
 * ================================================
 */
public class AgencyQueryActivity extends BaseRecyclerViewActivity {
    private AppCompatEditText mEtSearch;
    private FrameLayout mFlClean;
    private TextView mTvSearch;
    private LinearLayout mLlZoning;
    private TextView mTvZoning,mTvFindFixmedinsNum;
    private ImageView mIvZoning;

    private WheelUtils wheelUtils;
    private int choosePos1 = 0;

    @Override
    public int getLayoutId() {
        return R.layout.actitiy_agency_query;
    }

    @Override
    public void initView() {
        super.initView();
        mEtSearch = findViewById(R.id.et_search);
        mFlClean = findViewById(R.id.fl_clean);
        mTvSearch = findViewById(R.id.tv_search);
        mLlZoning = findViewById(R.id.ll_zoning);
        mTvZoning = findViewById(R.id.tv_zoning);
        mIvZoning = findViewById(R.id.iv_zoning);
        mTvFindFixmedinsNum = findViewById(R.id.tv_find_fixmedins_num);
    }

    @Override
    public void initAdapter() {
        mAdapter = new AgencyQueryAdapter();
    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                    return true;
                }
                return false;
            }
        });

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())){
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                }
            }
        });

        mTvSearch.setOnClickListener(this);
        mLlZoning.setOnClickListener(this);
        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        super.getData();
        AgencyQueryModel.sendAgencyQueryRequest(TAG, String.valueOf(mPageNum), "20", null == mTvZoning.getTag() ? "" : String.valueOf(mTvZoning.getTag()),
                mEtSearch.getText().toString(), new CustomerJsonCallBack<AgencyQueryModel>() {
                    @Override
                    public void onRequestError(AgencyQueryModel returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(AgencyQueryModel returnData) {
                        hideRefreshView();
                        List<AgencyQueryModel.DataBean> infos = returnData.getData();
                        if (mAdapter!=null&&infos != null) {
                            if (mPageNum == 0) {
                                if (mTvFindFixmedinsNum!=null){
                                    mTvFindFixmedinsNum.setText(EmptyUtils.strEmptyToText(returnData.getTotalItems(), "0"));
                                }
                                mAdapter.setNewData(infos);
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_search://搜索
                if (EmptyUtils.isEmpty(mEtSearch.getText().toString())) {
                    showShortToast("请输入搜索关键词");
                    return;
                }
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.ll_zoning://医保区划
                KeyboardUtils.hideSoftInput(AgencyQueryActivity.this);
                if (wheelUtils == null) {
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(AgencyQueryActivity.this, "", choosePos1,
                        CommonlyUsedDataUtils.getInstance().getFixmedinsZoning(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                choosePos1 = choosePos;
                                mTvZoning.setTag(dateInfo.getCode());
                                mTvZoning.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                                showWaitDialog();
                                onRefresh(mRefreshLayout);
                            }
                        });
                break;
        }
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        AgencyQueryModel.DataBean itemData = ((AgencyQueryAdapter) adapter).getItem(position);
        AgencyDetailActivity.newIntance(this, itemData);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (wheelUtils != null) {
            wheelUtils.dissWheel();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, AgencyQueryActivity.class);
        context.startActivity(intent);
    }
}
