package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.hjq.shape.layout.ShapeLinearLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-05-16
 * 描    述：门诊慢病申报
 * ================================================
 */
public class ChronicDiseasesHandleActivity extends BaseActivity {
    private ShapeLinearLayout mLlChronicDiseasesReg;
    private ShapeLinearLayout mLlChronicDiseasesQuery;
    private ShapeLinearLayout mLlChronicDiseasesReview;

    @Override
    public int getLayoutId() {
        return R.layout.activity_chronic_diseases_handle;
    }

    @Override
    public void initView() {
        super.initView();
        mLlChronicDiseasesReg = findViewById(R.id.ll_chronic_diseases_reg);
        mLlChronicDiseasesQuery = findViewById(R.id.ll_chronic_diseases_query);
        mLlChronicDiseasesReview = findViewById(R.id.ll_chronic_diseases_review);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mLlChronicDiseasesReg.setOnClickListener(this);
        mLlChronicDiseasesQuery.setOnClickListener(this);
        mLlChronicDiseasesReview.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.ll_chronic_diseases_reg://门诊慢病申报
                showShortToast("暂未开放");
                break;
            case R.id.ll_chronic_diseases_query://申报记录查询
                showShortToast("暂未开放");
                break;
            case R.id.ll_chronic_diseases_review://门诊慢病年审
                showShortToast("暂未开放");
                break;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, ChronicDiseasesHandleActivity.class);
        context.startActivity(intent);
    }
}
