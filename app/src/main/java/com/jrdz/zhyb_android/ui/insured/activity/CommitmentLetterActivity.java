package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;

import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ImageUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.toast.ToastUtil;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.mine.model.FileUploadModel;
import com.jrdz.zhyb_android.ui.settlement.activity.SmallTicketActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.OrderDetailActivity_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.PaySuccessActivity;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_insured;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;
import com.jrdz.zhyb_android.widget.pop.HandWritePop;

import java.io.File;
import java.math.BigDecimal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-23
 * 描    述：承诺书
 * ================================================
 */
public class CommitmentLetterActivity extends BaseActivity implements HandWritePop.IOptionListener {
    private ShapeTextView mTvSignature;

    String imagPath = BaseGlobal.getPicShotDir() + "commitment_letter.png";//存储签名的路径
    private HandWritePop handWritePop;

    @Override
    public int getLayoutId() {
        return R.layout.activity_commitment_letter;
    }

    @Override
    public void initView() {
        super.initView();
        mTvSignature = findViewById(R.id.tv_signature);
    }

    @Override
    public void initData() {
        super.initData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvSignature.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_signature://开始签名
                onSignature();
                break;
        }
    }

    //开始签名
    private void onSignature() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(this, "请备案人本人签名", "请确保备案人本人进行签名,以免无法通过备案", 1, "确认", "", R.color.color_4970e0, R.color.color_4970e0,
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                        if (handWritePop==null){
                            handWritePop=new HandWritePop(CommitmentLetterActivity.this, CommitmentLetterActivity.this);
                        }

                        handWritePop.showPopupWindow();
                    }

                    @Override
                    public void onBtn02Click() {
                    }
                });

    }

    @Override
    public void commit(Bitmap mBitmap) {
        showWaitDialog();
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                int bitmapWidth = mBitmap.getWidth();// Bitmap的宽
                int bitmapHeight = mBitmap.getHeight();// Bitmap的高
                boolean isSaveSuccess = ImageUtils.save(mBitmap, imagPath, Bitmap.CompressFormat.PNG, true);
                if (isSaveSuccess) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            upSignPic(bitmapWidth,bitmapHeight);
                        }
                    });
                } else {
                    hideWaitDialog();
                    showShortToast("图片保存失败，请重新保存");
                }
            }
        });
    }

    //上传签名
    private void upSignPic(int bitmapWidth,int bitmapHeight) {
        // 2022-04-23 模拟上传一张照片成功
        FileUploadModel.sendFileUploadRequest_insured(TAG, CompressUploadSinglePicUtils_insured.PIC_SPECIAL_DRUG_05_TAG, new File(imagPath), new CustomerJsonCallBack<FileUploadModel>() {
            @Override
            public void onRequestError(FileUploadModel returnData, String msg) {
                hideWaitDialog();
                ToastUtil.show(msg);
            }

            @Override
            public void onRequestSuccess(FileUploadModel returnData) {
                hideWaitDialog();
                FileUploadModel.DataBean data = returnData.getData();
                if (data!=null&&!EmptyUtils.isEmpty(data.getAccessoryId())&&!EmptyUtils.isEmpty(data.getAccessoryUrl())){
                    ToastUtil.show("上传成功");
                    Intent intent=new Intent();
                    intent.putExtra("accessoryId", data.getAccessoryId());
                    intent.putExtra("accessoryUrl",data.getAccessoryUrl());
                    intent.putExtra("bitmapWidth",bitmapWidth);
                    intent.putExtra("bitmapHeight",bitmapHeight);
                    setResult(RESULT_OK,intent);
                    goFinish();
                }else {
                    ToastUtil.show("上传失败");
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        if (handWritePop!=null){
            handWritePop.onClean();
        }
        super.onDestroy();
    }
}
