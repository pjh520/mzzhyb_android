package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;

import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-06-13
 * 描    述：企业基金交易密码
 * ================================================
 */
public class EnterprisePwdActivity extends BaseActivity {
    private LinearLayout mLlSettingPwd;
    private LinearLayout mLlUpdatePwd;
    private LinearLayout mLlResetPwd;

    @Override
    public int getLayoutId() {
        return R.layout.activity_enterprise_pwd;
    }

    @Override
    public void initView() {
        super.initView();

        mLlSettingPwd = findViewById(R.id.ll_setting_pwd);
        mLlUpdatePwd = findViewById(R.id.ll_update_pwd);
        mLlResetPwd = findViewById(R.id.ll_reset_pwd);
    }

    @Override
    public void initData() {
        super.initData();

        if ("1".equals(InsuredLoginUtils.getIsSetPaymentPwd())){
            mLlSettingPwd.setVisibility(View.GONE);
            mLlUpdatePwd.setVisibility(View.VISIBLE);
            mLlResetPwd.setVisibility(View.VISIBLE);
        }else {
            mLlSettingPwd.setVisibility(View.VISIBLE);
            mLlUpdatePwd.setVisibility(View.GONE);
            mLlResetPwd.setVisibility(View.GONE);
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mLlSettingPwd.setOnClickListener(this);
        mLlUpdatePwd.setOnClickListener(this);
        mLlResetPwd.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        switch (v.getId()){
            case R.id.ll_setting_pwd://设置交易密码
                SetEnterprisePwdActivity.newIntance(EnterprisePwdActivity.this);
                break;
            case R.id.ll_update_pwd://修改交易密码
                UpdateEnterprisePwdActivity.newIntance(EnterprisePwdActivity.this);
                break;
            case R.id.ll_reset_pwd://重置交易密码
                ResetEnterprisePwdActivity.newIntance(EnterprisePwdActivity.this);
                break;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, EnterprisePwdActivity.class);
        context.startActivity(intent);
    }
}
