package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.fingerprint_library.FingerManager;
import com.frame.fingerprint_library.callback.SimpleFingerCallback;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.database.InsuredUserInfoModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.model.InsuredLoginModel;
import com.jrdz.zhyb_android.utils.AppManager_Acivity;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.MMKVUtils;

import javax.crypto.Cipher;

import cn.jpush.android.api.JPushInterface;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-07-22
 * 描    述：指纹登录页面
 * ================================================
 */
public class FingerprintloginActivity extends BaseActivity {
    private TextView mTvOtherLoginType01;
    private ImageView mIvOtherLogin;
    private TextView mTvOtherLoginType02;
    private TextView mTvPwdLogin;

    private int backPager;

    @Override
    public int getLayoutId() {
        return R.layout.activity_fingerprint_login;
    }

    @Override
    public void initView() {
        super.initView();
        mTvOtherLoginType01 = findViewById(R.id.tv_other_login_type_01);
        mIvOtherLogin = findViewById(R.id.iv_other_login);
        mTvOtherLoginType02 = findViewById(R.id.tv_other_login_type_02);
        mTvPwdLogin = findViewById(R.id.tv_pwd_login);
    }

    @Override
    public void initData() {
        backPager = getIntent().getIntExtra("backPager", 0);
        super.initData();
        InsuredLoginUtils.clearAccount(true);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mIvOtherLogin.setOnClickListener(this);
        mTvPwdLogin.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.iv_other_login://指纹登录
                quickLogin();
                break;
            case R.id.tv_pwd_login://密码登录
                if (!AppManager_Acivity.getInstance().hasActivity(InsuredLoginActivity.class)) {
                    InsuredLoginActivity.newIntance(FingerprintloginActivity.this, backPager);
                }

                finish();
                break;
        }
    }

    //指纹登录
    private void quickLogin() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            FingerManager.build().setApplication(getApplication())
                    .setTitle("指纹验证")
                    .setDes("请按下指纹")
                    .setNegativeText("取消")
                    .setDecrypt(true)
                    //.setFingerDialogApi23(new MyFingerDialog())//如果你需要自定义android P 以下系统弹窗就设置,注意需要继承BaseFingerDialog，不设置会使用默认弹窗
                    .setFingerCallback(new SimpleFingerCallback() {
                        @Override
                        public void onSucceed(Cipher cipher, byte[] bytes) {
                            String encodeInfo = new String(bytes);
                            if (encodeInfo.contains("@#@")) {
                                String[] datas = encodeInfo.split("@#@");
                                if (datas.length == 2) {
                                    fingerprintlogin(datas[0], datas[1]);
                                } else {
                                    MMKVUtils.putString("finger_login_key", "");
                                    showShortToast("指纹登录密码记录有误，请使用密码登录，重新认证指纹");
                                }
                            }
                        }

                        @Override
                        public void onFailed() {
                            showShortToast("指纹无法识别");
                        }

                        @Override
                        public void onChange() {
                            FingerManager.updateFingerData(FingerprintloginActivity.this);
//                            quickLogin();
                            MMKVUtils.putString("finger_login_key", "");
                            showShortToast("指纹发生改变了，请使用密码登录");
                        }

                        @Override
                        public byte[] onDoFinal() {
                            return Base64.decode(MMKVUtils.getString("finger_login_key", ""), Base64.URL_SAFE);
                        }

                        @Override
                        public void onCancel() {
                            showShortToast("取消指纹登录");
                        }
                    })
                    .create()
                    .startListener(FingerprintloginActivity.this);
        }
    }

    //指纹登录真正的地方
    private void fingerprintlogin(String username, String password) {
        showWaitDialog();
        InsuredLoginModel.sendInsuredLoginRequest(TAG, EmptyUtils.strEmpty(username), EmptyUtils.strEmpty(password),
                EmptyUtils.strEmpty(JPushInterface.getRegistrationID(this)), new CustomerJsonCallBack<InsuredLoginModel>() {
            @Override
            public void onRequestError(InsuredLoginModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(InsuredLoginModel returnData) {
                InsuredLoginModel.DataBean data = returnData.getData();
                if (data != null) {
                    InsuredUserInfoModel insuredUserInfoModel = new InsuredUserInfoModel(EmptyUtils.strEmpty(data.getPhone()), EmptyUtils.strEmpty(data.getName()),
                            EmptyUtils.strEmpty(data.getIsFace()), EmptyUtils.strEmpty(data.getId_card_no()), EmptyUtils.strEmpty(data.getAccessoryId()),
                            EmptyUtils.strEmpty(data.getAccessoryUrl()),password,data.getNickname(),data.getAssociatedDiseases(),data.getSex(),
                            data.getIsSetPaymentPwd(),data.getIsEnterpriseFundPay(),data.getUserID());

                    insuredUserInfoModel.save();

                    onLoginSuccess();
                } else {
                    hideWaitDialog();
                    showShortToast("数据有误，请重新登录");
                }
            }
        });
    }

    //登录成功
    private void onLoginSuccess() {
        MsgBus.sendInsuredLoginStatus().post(1);
        hideWaitDialog();
        goMainPage();
    }

    //跳转首页
    private void goMainPage() {
        if (1 == backPager) {
            InsuredMainActivity.newIntance(FingerprintloginActivity.this, 0);
            overridePendingTransition(R.anim.screen_zoom_in, R.anim.screen_zoom_out);
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        if (1 == backPager) {
            InsuredMainActivity.newIntance(FingerprintloginActivity.this, 0);
        }
        super.onBackPressed();
    }

    //backPager 0:代表返回上一页 1：代表回到首页
    public static void newIntance(Context context, int backPager) {
        Intent intent = new Intent(context, FingerprintloginActivity.class);
        intent.putExtra("backPager", backPager);
        context.startActivity(intent);
    }
}
