package com.jrdz.zhyb_android.ui.insured.activity;

import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.adapter.FixmedinsListAdapter;
import com.jrdz.zhyb_android.ui.insured.adapter.SpecialDrugListAdapter;
import com.jrdz.zhyb_android.ui.insured.model.FixmedinsListModel;
import com.jrdz.zhyb_android.ui.insured.model.SpecialDrugListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-15
 * 描    述：机构信息
 * ================================================
 */
public class FixmedinsListActivity extends BaseRecyclerViewActivity {
    private EditText mEtSearch;
    private ShapeTextView mStvSearch;
    private TextView mTvNumName,mTvNum;

    @Override
    public int getLayoutId() {
        return R.layout.activity_special_drug_list;
    }

    @Override
    public void initView() {
        super.initView();
        mEtSearch = findViewById(R.id.et_search);
        mStvSearch = findViewById(R.id.stv_search);
        mTvNumName= findViewById(R.id.tv_num_name);
        mTvNum = findViewById(R.id.tv_num);
        mTvNumName.setText("机构目录");
        mEtSearch.setHint("可机构/医师编码或者姓名搜索");
    }

    @Override
    public void initAdapter() {
        mAdapter = new FixmedinsListAdapter();
    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                    return true;
                }
                return false;
            }
        });
        mStvSearch.setOnClickListener(this);

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void getData() {
        super.getData();
        FixmedinsListModel.sendFixmedinsListRequest(TAG, String.valueOf(mPageNum), "10",
                EmptyUtils.strEmpty(mEtSearch.getText().toString()), new CustomerJsonCallBack<FixmedinsListModel>() {
                    @Override
                    public void onRequestError(FixmedinsListModel returnData, String msg) {
                        hideRefreshView();
                        showShortToast(msg);
                        if (mPageNum == 0) {
                            if (mTvNum!=null){
                                mTvNum.setText("0条");
                            }
                        }
                    }

                    @Override
                    public void onRequestSuccess(FixmedinsListModel returnData) {
                        hideRefreshView();
                        List<FixmedinsListModel.DataBean> infos = returnData.getData();
                        if (mAdapter != null && infos != null) {
                            if (mPageNum == 0) {
                                mAdapter.setNewData(infos);
                                if (mTvNum!=null){
                                    mTvNum.setText(returnData.getTotalItems() + "条");
                                }
                                if (infos.size() <= 0) {
                                    mAdapter.isUseEmpty(true);
                                }
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.stv_search:
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
        }
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        Intent intent = new Intent();
        intent.putExtra("fixmedinsData", ((FixmedinsListAdapter) adapter).getItem(position));
        setResult(Activity.RESULT_OK, intent);
        goFinish();
    }
}
