package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;

import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-05-06
 * 描    述：备案须知
 * ================================================
 */
public class HandleNoticeActivity extends BaseActivity {
    private FrameLayout mFlCb;
    private CheckBox mCb;
    private ShapeTextView mTvSubmit;

    @Override
    public int getLayoutId() {
        return R.layout.activity_handle_notice;
    }

    @Override
    public void initView() {
        super.initView();
        mFlCb = findViewById(R.id.fl_cb);
        mCb = findViewById(R.id.cb);
        mTvSubmit = findViewById(R.id.tv_submit);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mFlCb.setOnClickListener(this);
        mTvSubmit.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.fl_cb:
                if (mCb.isChecked()) {
                    mCb.setChecked(false);
                    mTvSubmit.setEnabled(false);
                } else {
                    mCb.setChecked(true);
                    mTvSubmit.setEnabled(true);
                }
                break;
            case R.id.tv_submit:
                SpecialDrugRegActivity.newIntance(HandleNoticeActivity.this);
                goFinish();
                break;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, HandleNoticeActivity.class);
        context.startActivity(intent);
    }
}
