package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.mobile.android.verify.sdk.MPVerifyService;
import com.alipay.mobile.android.verify.sdk.ServiceFactory;
import com.alipay.mobile.android.verify.sdk.interfaces.ICallback;
import com.alipay.mobile.android.verify.sdk.interfaces.IService;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.LogUtils;
import com.frame.compiler.widget.permission.PermissionHelper;
import com.hjq.permissions.Permission;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.model.SmrzInitDataModel;
import com.jrdz.zhyb_android.ui.insured.model.SmrzResultModel;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-05-19
 * 描    述：实名认证新增-先填写姓名 身份证数据
 * ================================================
 */
public class InputSmrzBaseDataActivity extends BaseActivity {
    private EditText mEtName;
    private EditText mEtCertNo;
    private ShapeTextView mTvSmrz;
    private PermissionHelper permissionHelper;
    private IService mService;

    @Override
    public int getLayoutId() {
        return R.layout.activity_input_smrz_basedata;
    }

    @Override
    public void initData() {
        super.initData();
        //支付宝人脸认证---初始化函数涉及获取设备信息，因此需要在隐私弹框之后调用
        MPVerifyService.markUserAgreedPrivacyPolicy(this.getApplicationContext());
        // 初始化函数涉及获取设备信息，因此需要在隐私弹框之后调用，建议尽早初始化
        mService = ServiceFactory.create(this).build();
    }

    @Override
    public void initView() {
        super.initView();
        mEtName = findViewById(R.id.et_name);
        mEtCertNo = findViewById(R.id.et_cert_no);
        mTvSmrz = findViewById(R.id.tv_smrz);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvSmrz.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_smrz://人脸识别
                getReadPhoneStatePermission();
                break;
        }
    }

    public void  getReadPhoneStatePermission(){
        if (permissionHelper==null){
            permissionHelper = new PermissionHelper();
        }
        permissionHelper.requestPermission(this, new PermissionHelper.onPermissionListener() {
            @Override
            public void onSuccess() {
                getSmrzInitData(mEtName.getText().toString(), mEtCertNo.getText().toString());
            }

            @Override
            public void onNoAllSuccess(String noAllSuccessText) {}

            @Override
            public void onFail(String failText) {
                showShortToast("读取电话状态权限失败,请开启才能使用人脸认证");
            }
        },"读取电话状态权限:支付宝人脸认证需要使用该权限。", Permission.READ_PHONE_STATE);
    }

    //获取实名认证初始化数据
    private void getSmrzInitData(String name,String certNo) {
        if (EmptyUtils.isEmpty(name)) {
            showShortToast("请输入真实姓名");
            return;
        }

        if (EmptyUtils.isEmpty(certNo)) {
            showShortToast("请输入证件号码");
            return;
        }

        if (certNo.length()!=15&&certNo.length()!=18) {
            showShortToast("身份证号码为15位或者18位");
            return;
        }

        showWaitDialog();
        SmrzInitDataModel.sendSmrzInitDataRequest(TAG, name, certNo, new CustomerJsonCallBack<SmrzInitDataModel>() {
            @Override
            public void onRequestError(SmrzInitDataModel returnData, String msg) {
                hideWaitDialog();
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(SmrzInitDataModel returnData) {
                hideWaitDialog();
                SmrzInitDataModel.DataBean data = returnData.getData();
                if (data!=null&&!EmptyUtils.isEmpty(data.getCertify_id())){
                    goSmrz(data.getCertify_id(),data.getCertify_url(),name, certNo);
                }else {
                    showShortToast("返回数据有误，请重新认证");
                }
            }
        });
    }

    //调用支付宝实名认证
    private void goSmrz(String certify_id,String certify_url,String name,String certNo) {
        // 封装认证数据
        Map<String, Object> requestInfo = new HashMap<>();
        requestInfo.put("certifyId", certify_id);
        requestInfo.put("bizCode", InsuredPersonalActivity.GET_ZFB_BIZCODE);
        // 发起认证
        mService.startService(requestInfo, new ICallback() {
            @Override
            public void onResponse(Map<String, String> response) {
                if (response==null){
                    showShortToast("实名认证有误，请重新验证");
                    return;
                }
                //调用者获得的数据: {"ex":"{}","result":"{\"certifyId\":\"7cae7d0518fdaeff93829bcd80854d59\"}","resultStatus":"9000"}
                LogUtils.e("认证数据："+ JSON.toJSONString(response));
                String responseCode = response.get("resultStatus");
                if (EmptyUtils.isEmpty(responseCode)){
                    showShortToast("系统异常,请重新认证");
                    return;
                }
                switch (responseCode){
                    case "9001"://等待支付宝端完成认证
                        break;
                    case "9000"://认证通过，业务方需要去支付宝网关接口查询最终状态。
                        getSmrzResult(certify_id,name, certNo);
                        break;
                    case "6001"://用户取消了业务流程，主动退出。
                        showShortToast("用户取消了业务流程，主动退出");
                        break;
                    case "6002"://网络异常。
                        showShortToast("网络异常");
                        break;
                    case "6003"://业务异常。（用户因为特定原因刷脸不通过）。
                        showShortToast("业务异常。（用户因为特定原因刷脸不通过）。");
                        break;
                    case "6004"://刷脸频次过高或失败次数过多，请您稍等再试。
                        showShortToast("刷脸频次过高或失败次数过多，请您稍等再试。");
                        break;
                    case "6005"://API 限流中
                        showShortToast("API 限流中");
                        break;
                    case "4000"://系统异常。
                        showShortToast("系统异常");
                        break;
                    case "4001"://客户端接入异常，如： 非法 BizCode 没有调用 MPVerifyService.markUserAgreedPrivacyPolicy
                        showShortToast("客户端接入异常");
                        break;
                    case "4002"://没有赋予摄像头权限。
                        showShortToast("没有赋予摄像头权限");
                        break;
                    case "4003"://非权限问题的启动摄像头失败，可能是硬件问题或者 OS 问题
                        showShortToast("非权限问题的启动摄像头失败，可能是硬件问题或者 OS 问题");
                        break;
                }

            }
        });
    }

    //获取实名认证结果
    private void getSmrzResult(String certify_id,String name,String certNo){
        showWaitDialog();
        SmrzResultModel.sendSmrzResultRequest(TAG, certify_id, new CustomerJsonCallBack<SmrzResultModel>() {
            @Override
            public void onRequestError(SmrzResultModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(SmrzResultModel returnData) {
                hideWaitDialog();
                SmrzResultModel.DataBean data = returnData.getData();
                if (data!=null){
                    if ("SUCCESS".equals(data.getPassed())){
                        //更新数据库 把姓名 身份证入录
                        InsuredLoginUtils.setIsFace("1");
                        InsuredLoginUtils.setName(name);
                        InsuredLoginUtils.setIdCardNo(certNo);

                        MsgBus.sendSmrzResult_user().post(data.getPassed());
                        goFinish();
                    }else {
                        showShortToast("实名认证有误，请重新验证");
                    }
                }
            }
        });
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, InputSmrzBaseDataActivity.class);
        context.startActivity(intent);
    }
}
