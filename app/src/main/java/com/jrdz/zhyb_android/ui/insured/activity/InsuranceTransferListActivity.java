package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.ui.insured.adapter.InsuranceTransferListAdapter;
import com.jrdz.zhyb_android.utils.H5RequestUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginSmrzUtils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-07-09
 * 描    述：医保转移申请
 * ================================================
 */
public class InsuranceTransferListActivity extends BaseRecyclerViewActivity {
    protected InsuredLoginSmrzUtils insuredLoginSmrzUtils;
    private H5RequestUtils h5RequestUtils;

    @Override
    public void initAdapter() {
        mAdapter=new InsuranceTransferListAdapter();
    }

    @Override
    public void initData() {
        super.initData();

        setPagerData();
    }

    //设置页面数据
    protected void setPagerData() {
        String[] datas={"跨省医保转移申请","省内医保转移申请","医保转移查询"};
        mAdapter.setNewData(new ArrayList(Arrays.asList(datas)));
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        String itemData = ((InsuranceTransferListAdapter) adapter).getItem(position);
        if (insuredLoginSmrzUtils == null) {
            insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
        }

        if (insuredLoginSmrzUtils.isLoginSmrz(InsuranceTransferListActivity.this)) {
            switch (itemData){
                case "跨省医保转移申请":
                    goH5Pager("跨省医保转移申请",Constants.H5URL.H5_CROSS_PROVINCE_URL);
                    break;
                case "省内医保转移申请":
                    goH5Pager("省内医保转移申请",Constants.H5URL.H5_PROVINCIAL_URL);
                    break;
                case "医保转移查询":
                    goH5Pager("医保转移查询",Constants.H5URL.H5_TRANSFERREACORDSEARCH_URL);
                    break;
            }
        }
    }

    //跳转H5链接
    protected void goH5Pager(String title, String h5Url) {
        if (h5RequestUtils==null){
            h5RequestUtils=new H5RequestUtils();
        }
        h5RequestUtils.goPager(InsuranceTransferListActivity.this, TAG, title, h5Url, new H5RequestUtils.IRequestListener() {
            @Override
            public void onShowWaitDialog() {
                showWaitDialog();
            }

            @Override
            public void onHideWaitDialog() {
                hideWaitDialog();
            }

            @Override
            public void onFail(String msg) {
                showTipDialog(msg);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (insuredLoginSmrzUtils != null) {
            insuredLoginSmrzUtils.cleanData();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, InsuranceTransferListActivity.class);
        context.startActivity(intent);
    }
}