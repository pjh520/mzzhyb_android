package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.ui.catalogue.adapter.CataTabsAdapter;
import com.jrdz.zhyb_android.ui.insured.fragment.InsuredChinaCataManaFragment;
import com.jrdz.zhyb_android.ui.insured.fragment.InsuredMedSerCataManaFragment;
import com.jrdz.zhyb_android.ui.insured.fragment.InsuredMedSupCataManaFragment;
import com.jrdz.zhyb_android.ui.insured.fragment.InsuredPhaPreCataManaFragment;
import com.jrdz.zhyb_android.ui.insured.fragment.InsuredWestCataManaFragment;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述： 参保人目录管理
 * ================================================
 */
public class InsuredCataManaActivity extends BaseActivity{
    private LinearLayout mLlTitle;
    private FrameLayout mFlClose;
    private AppCompatEditText mEtSearch;
    private TextView mTvSearch;
    private SlidingTabLayout mStbTablayout;
    private ViewPager mViewpager;

    private List<DataDicModel> listTypes;

    @Override
    public int getLayoutId() {
        return R.layout.activity_cata_mana;
    }

    @Override
    public void initView() {
        super.initView();
        mLlTitle = findViewById(R.id.ll_title);
        mFlClose = findViewById(R.id.fl_close);
        mEtSearch = findViewById(R.id.et_search);
        mTvSearch = findViewById(R.id.tv_search);
        mStbTablayout = findViewById(R.id.stb_tablayout);
        mViewpager = findViewById(R.id.viewpager);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mLlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        super.initData();
        listTypes = CommonlyUsedDataUtils.getInstance().getListTypeData_user();
        if (listTypes == null || listTypes.isEmpty()) {
            showShortToast("药品类型有误，请重新打开app");
            finish();
            return;
        }

        initViewPage();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    search();
                    return true;
                }
                return false;
            }
        });
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())){
                    MsgBus.sendCataRefresh().post(listTypes.get(mStbTablayout.getCurrentTab()).getValue());
                }
            }
        });
        mFlClose.setOnClickListener(this);
        mTvSearch.setOnClickListener(this);
    }

    //初始化
    private void initViewPage() {
        CataTabsAdapter baseViewPagerAndTabsAdapter_new = new CataTabsAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getCustomeItem(String value,String label, int position) {
                switch (value) {
                    case "101"://西药中成药目录
                        return InsuredWestCataManaFragment.newIntance(value,label);
                    case "102"://中药饮片目录
                        return InsuredChinaCataManaFragment.newIntance(value,label);
                    case "103"://医疗机构自制剂目录
                        return InsuredPhaPreCataManaFragment.newIntance(value,label);
                    case "201"://医疗服务项目目录
                        return InsuredMedSerCataManaFragment.newIntance(value,label);
                    case "301"://医用耗材目录
                        return InsuredMedSupCataManaFragment.newIntance(value,label);
                    default:
                        return InsuredWestCataManaFragment.newIntance(value,label);
                }
            }
        };

        baseViewPagerAndTabsAdapter_new.setData(listTypes);
        mViewpager.setAdapter(baseViewPagerAndTabsAdapter_new);
        mStbTablayout.setViewPager(mViewpager);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close://关闭
                goFinish();
                break;
            case R.id.tv_search://搜索
                search();
                break;
        }
    }

    //搜索
    private void search() {
        if (EmptyUtils.isEmpty(mEtSearch.getText().toString())){
            showShortToast("请输入目录名称/拼音简码");
            return;
        }

        MsgBus.sendCataRefresh().post(listTypes.get(mStbTablayout.getCurrentTab()).getValue());
    }

    public String getSearchText(){
        return EmptyUtils.strEmpty(mEtSearch.getText().toString());
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, InsuredCataManaActivity.class);
        context.startActivity(intent);
    }
}
