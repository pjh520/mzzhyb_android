package com.jrdz.zhyb_android.ui.insured.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.ui.catalogue.adapter.CataTabsAdapter;
import com.jrdz.zhyb_android.ui.catalogue.fragment.DiseCataFragment;
import com.jrdz.zhyb_android.ui.insured.fragment.InsuredDiseCataFragment;
import com.jrdz.zhyb_android.ui.insured.fragment.InsuredManteCataFragment;
import com.jrdz.zhyb_android.ui.insured.fragment.InsuredZYJBCataFragment;
import com.jrdz.zhyb_android.ui.insured.fragment.InsuredZYZHCataFragment;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.catalogue.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/23
 * 描    述： 参保人疾病目录
 * ================================================
 */
public class InsuredDiseCataActivity extends BaseActivity {
    private LinearLayout mLlTitle;
    private FrameLayout mFlClose;
    private AppCompatEditText mEtSearch;
    private FrameLayout mFlClean;
    private TextView mTvSearch;
    private SlidingTabLayout mStbTablayout;
    private ViewPager mViewpager;

    private String from;
    private List<DataDicModel> diseTypeDatas;

    @Override
    public int getLayoutId() {
        return R.layout.activity_disecata_manage;
    }

    @Override
    public void initView() {
        super.initView();
        mLlTitle = findViewById(R.id.ll_title);
        mFlClose = findViewById(R.id.fl_close);
        mEtSearch = findViewById(R.id.et_search);
        mFlClean = findViewById(R.id.fl_clean);
        mTvSearch = findViewById(R.id.tv_search);
        mStbTablayout = findViewById(R.id.stb_tablayout);
        mViewpager = findViewById(R.id.viewpager);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mLlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        from = getIntent().getStringExtra("from");
        super.initData();
        diseTypeDatas = CommonlyUsedDataUtils.getInstance().getDiseTypeData();
        if (diseTypeDatas == null || diseTypeDatas.isEmpty()) {
            showShortToast("疾病数据有误，请重新打开app");
            finish();
            return;
        }

        initViewPage();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    mFlClean.setVisibility(View.GONE);
                    MsgBus.sendDiseRefresh().post(diseTypeDatas.get(mStbTablayout.getCurrentTab()).getValue());
                } else {
                    mFlClean.setVisibility(View.VISIBLE);
                }
            }
        });
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    search();
                    return true;
                }
                return false;
            }
        });
        mFlClose.setOnClickListener(this);
        mFlClean.setOnClickListener(this);
        mTvSearch.setOnClickListener(this);
    }

    //初始化
    private void initViewPage() {
        CataTabsAdapter baseViewPagerAndTabsAdapter_new = new CataTabsAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getCustomeItem(String value,String label,  int position) {
                switch (value) {
                    case "111"://疾病与诊断目录
                        return InsuredDiseCataFragment.newIntance(from);
                    case "112"://门诊慢特病病种目录
                        return InsuredManteCataFragment.newIntance(from);
                    case "113"://中医疾病目录
                        return InsuredZYJBCataFragment.newIntance(from);
                    case "114"://中医症候目录
                        return InsuredZYZHCataFragment.newIntance(from);
                    default:
                        return InsuredDiseCataFragment.newIntance(from);
                }
            }
        };

        baseViewPagerAndTabsAdapter_new.setData(diseTypeDatas);
        mViewpager.setAdapter(baseViewPagerAndTabsAdapter_new);
        mStbTablayout.setViewPager(mViewpager);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close://关闭
                goFinish();
                break;
            case R.id.fl_clean://清空
                mEtSearch.setText("");
                KeyboardUtils.hideSoftInput(mEtSearch);
                MsgBus.sendDiseRefresh().post(diseTypeDatas.get(mStbTablayout.getCurrentTab()).getValue());
                break;
            case R.id.tv_search://搜索
                search();
                break;
        }
    }

    //搜索
    private void search() {
        if (EmptyUtils.isEmpty(mEtSearch.getText().toString())) {
            showShortToast("请输入搜索关键词");
            return;
        }

        MsgBus.sendDiseRefresh().post(diseTypeDatas.get(mStbTablayout.getCurrentTab()).getValue());
    }

    public String getSearchText() {
        return EmptyUtils.strEmpty(mEtSearch.getText().toString());
    }

    //from 1.来自病种目录查询 2.来自选择病种目录
    public static void newIntance(Context context, String from) {
        Intent intent = new Intent(context, InsuredDiseCataActivity.class);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }
    //from 1.来自病种目录查询 2.来自选择病种目录
    public static void newIntance(Activity activity, String from, int requestCode) {
        Intent intent = new Intent(activity, InsuredDiseCataActivity.class);
        intent.putExtra("from", from);
        activity.startActivityForResult(intent, requestCode);
    }
}
