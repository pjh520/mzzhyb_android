package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.ColorUtils;
import androidx.core.widget.NestedScrollView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.MD5Util;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.countDownTime.CustomCountDownTimer;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.permission.PermissionHelper;
import com.frame.compiler.widget.tabLayout.CommonTabLayout;
import com.frame.compiler.widget.tabLayout.listener.OnTabSelectListener;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.database.InsuredUserInfoModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.model.GetAPPAuditModel;
import com.jrdz.zhyb_android.ui.insured.model.InsuredLoginModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.DeviceID;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.MMKVUtils;
import com.jrdz.zhyb_android.utils.textutils.TextLightUtils;
import com.tencent.bugly.crashreport.CrashReport;

import cn.jpush.android.api.JPushInterface;

public class InsuredLoginActivity extends BaseActivity {
    private NestedScrollView mScrollview;
    private FrameLayout mFlClose;
    private CommonTabLayout mStbTab;
    private ShapeLinearLayout mSllPhone;
    private EditText mEtPhone;
    private EditText mEtPwd;
    private ShapeTextView mTvLogin;
    private TextView mTvPwdTag,mTvTime,mTvAgreeRule;
    private TextView mTvRegist,mTvForget,mTvOtherLoginType01,mTvOtherLoginType02,mTvVersion01,mTvVersion02;
    private ImageView mIvOtherLogin;
    private LinearLayout mLlVersion01,mLlVersion02;
    private FrameLayout flCb;
    private CheckBox cb;

    private int bannerHeight;
    private int backPager;
    private CustomerDialogUtils customerDialogUtils;
    private PermissionHelper permissionHelper;
    private boolean isAPPAudit=false;
    private String guid;
    private TextLightUtils textLightUtils;
    private String type = "1";

    private CustomCountDownTimer mCustomCountDownTimer;

    @Override
    public int getLayoutId() {
        return R.layout.activity_insured_login;
    }

    @Override
    public void initView() {
        super.initView();
        mScrollview = findViewById(R.id.scrollview);
        mFlClose = findViewById(R.id.fl_close);
        mStbTab = findViewById(R.id.stb_tab);
        mSllPhone= findViewById(R.id.sll_phone);
        mEtPhone = findViewById(R.id.et_phone);
        mEtPwd = findViewById(R.id.et_pwd);
        mTvPwdTag = findViewById(R.id.tv_pwd_tag);
        mTvTime = findViewById(R.id.tv_time);
        mTvLogin = findViewById(R.id.tv_login);
        mTvAgreeRule = findViewById(R.id.tv_agree_rule);
        flCb = findViewById(R.id.fl_cb);
        cb = findViewById(R.id.cb);

        mTvRegist= findViewById(R.id.tv_regist);
        mTvForget= findViewById(R.id.tv_forget);
        mTvOtherLoginType01= findViewById(R.id.tv_other_login_type_01);
        mIvOtherLogin= findViewById(R.id.iv_other_login);
        mTvOtherLoginType02= findViewById(R.id.tv_other_login_type_02);
        mLlVersion01= findViewById(R.id.ll_version_01);
        mTvVersion01= findViewById(R.id.tv_version_01);
        mLlVersion02= findViewById(R.id.ll_version_02);
        mTvVersion02= findViewById(R.id.tv_version_02);
    }

    @Override
    public void initData() {
        bannerHeight = getResources().getDimensionPixelOffset(R.dimen.dp_100);
        backPager = getIntent().getIntExtra("backPager", 0);
        super.initData();
        InsuredLoginUtils.clearAccount(true);
        //初始化tablayout
        mStbTab.setTabData(CommonlyUsedDataUtils.getInstance().getInsuredLoginTabData());

        textLightUtils=new TextLightUtils();
        textLightUtils.setHighlightColor(mTvAgreeRule, new TextLightUtils.IClickableSpan() {
            @Override
            public void onClickable(String title, String url) {
                MyWebViewActivity.newIntance(InsuredLoginActivity.this, title, url,
                        true, false);
            }
        });

        mTitleBar.getLeftView().setVisibility(View.GONE);
        mTitleBar.setLineVisible(false);
        mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT, ContextCompat.getColor(this, R.color.colorAccent), 0));
        mTitleBar.setTitleColor(getResources().getColor(R.color.white));
        mTitleBar.setTitle("");

        mTvVersion01.setText("版本号:" + EmptyUtils.strEmpty(RxTool.getVersion(this)) + Constants.Configure.VERSIONNAME_TAG);
        mTvVersion02.setText("版本号:" + EmptyUtils.strEmpty(RxTool.getVersion(this)) + Constants.Configure.VERSIONNAME_TAG);

        if (EmptyUtils.isEmpty(MMKVUtils.getString("finger_login_key", ""))){//关闭指纹登录
            mTvOtherLoginType01.setVisibility(View.GONE);
            mIvOtherLogin.setVisibility(View.GONE);
            mTvOtherLoginType02.setVisibility(View.GONE);

            mLlVersion01.setVisibility(View.GONE);
            mLlVersion02.setVisibility(View.VISIBLE);
        }else {//开启指纹登录
            mTvOtherLoginType01.setVisibility(View.VISIBLE);
            mIvOtherLogin.setVisibility(View.VISIBLE);
            mTvOtherLoginType02.setVisibility(View.VISIBLE);

            mLlVersion01.setVisibility(View.VISIBLE);
            mLlVersion02.setVisibility(View.GONE);
        }

        //获取app是否在应用市场审核
        if (Constants.Configure.IS_APP_MARKET){
            getAPPAudit();
        }else {
            isAPPAudit=false;
        }

        //2.申请存储权限
        getPermission();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mFlClose.setOnClickListener(this);
        mTvTime.setOnClickListener(this);
        mTvLogin.setOnClickListener(this);
        flCb.setOnClickListener(this);
        mIvOtherLogin.setOnClickListener(this);
        mTvRegist.setOnClickListener(this);
        mTvForget.setOnClickListener(this);
        mScrollview.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY <= 0) {
                    mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                            , ContextCompat.getColor(InsuredLoginActivity.this, R.color.colorAccent), 0));
                    mTitleBar.setTitle("");

                    mFlClose.setVisibility(View.VISIBLE);
                    mFlClose.setEnabled(true);
                    mTitleBar.getLeftView().setVisibility(View.GONE);
                } else if (scrollY <= bannerHeight) {
                    float alpha = (float) scrollY / bannerHeight;
                    mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                            , ContextCompat.getColor(InsuredLoginActivity.this, R.color.colorAccent), alpha));
                    mTitleBar.setTitle("");

                    if (View.VISIBLE==mFlClose.getVisibility()){
                        mFlClose.setVisibility(View.INVISIBLE);
                        mFlClose.setEnabled(false);
                        mTitleBar.getLeftView().setVisibility(View.VISIBLE);
                    }
                } else {
                    mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                            , ContextCompat.getColor(InsuredLoginActivity.this, R.color.colorAccent), 1));
                    mTitleBar.setTitle("欢迎来到米脂医保");
                }
            }
        });

        mStbTab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                switch (position) {
                    case 0://密码登录
                        type = "1";
                        mTvPwdTag.setText("密码");
                        mEtPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        mTvTime.setVisibility(View.GONE);
                        break;
                    case 1://验证码登录
                        type = "2";
                        mTvPwdTag.setText("验证码");
                        mEtPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        mTvTime.setVisibility(View.VISIBLE);
                        break;
                }

                mEtPwd.setText("");
            }

            @Override
            public void onTabReselect(int position) {
            }
        });
    }

    //获取app的审核状态
    private void getAPPAudit() {
        GetAPPAuditModel.sendGetAPPAuditRequest(TAG, new CustomerJsonCallBack<GetAPPAuditModel>() {
            @Override
            public void onRequestError(GetAPPAuditModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GetAPPAuditModel returnData) {
                hideWaitDialog();
                GetAPPAuditModel.DataBean info = returnData.getData();
                //1.当版本号小于后台返回的版本号 那么就不需要隐藏
                if (info != null && RxTool.getVersionCode(InsuredLoginActivity.this)>=info.getInteriorVersionNum()) {
                    //（0未审核 1已审核）
                    if (0==info.getAPPApprovalStatus()){
                        isAPPAudit=true;
                    }else {
                        isAPPAudit=false;
                    }
                }else {
                    isAPPAudit=false;
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close://关闭
                goFinish();
                break;
            case R.id.fl_cb:
                if (cb.isChecked()) {
                    cb.setChecked(false);
                } else {
                    cb.setChecked(true);
                }
                break;
            case R.id.tv_time://获取短信验证码
                //2022-07-25 先请求接口 接口返回发送短信验证码成功 然后开始倒计时
                sendSms();
                break;
            case R.id.tv_login://登录
                if ("1".equals(type)){
                    login();
                }else {
                    smsLogin();
                }
                break;
            case R.id.iv_other_login://指纹登录
                FingerprintloginActivity.newIntance(InsuredLoginActivity.this,backPager);
                break;
            case R.id.tv_regist://注册
                InsuredRegistActivity.newIntance(InsuredLoginActivity.this);
                break;
            case R.id.tv_forget://忘记密码
                InsuredFindPwdActivity.newIntance(InsuredLoginActivity.this);
                break;
        }
    }

    //申请存储权限
    private void getPermission() {
        permissionHelper = new PermissionHelper();
        permissionHelper.requestPermission(this, new PermissionHelper.onPermissionListener() {
            @Override
            public void onSuccess() {
                getUniId();
            }

            @Override
            public void onNoAllSuccess(String noAllSuccessText) {}

            @Override
            public void onFail(String failText) {
                showShortToast(failText);
            }
        }, "存储权限:登录的时候,需要存储app的唯一码到手机",Permission.MANAGE_EXTERNAL_STORAGE);
    }

    //获取唯一码
    private void getUniId() {
        showWaitDialog();
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    guid = DeviceID.getAndroidUniId();//f9665089da844c75ab11442860d1f2de
                    // 也可以通过CrashReport类设置，适合无法在初始化sdk时获取到deviceId的场景，context和deviceId不能为空（或空字符串）
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!EmptyUtils.isEmpty(guid)) {
                                CrashReport.setDeviceId(InsuredLoginActivity.this, guid);
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                hideWaitDialog();
            }
        });
    }

    //发送短信
    private void sendSms() {
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString().trim()) || mEtPhone.getText().toString().length() < 11) {
            showShortToast("手机号码格式不正确!");
            return;
        }
        showWaitDialog();
        BaseModel.sendSendsmsRequest(TAG, mEtPhone.getText().toString(), "1", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();

                //发送短信验证码成功
                mTvTime.setEnabled(false);
                showShortToast("验证码已发送");
                mTvTime.setTextColor(getResources().getColor(R.color.color_ff0202));
                setTimeStart(60000);
            }
        });
    }

    //开启定时器  millisecond:倒计时的时间
    private void setTimeStart(long millisecond) {
        //防止内存泄漏
        if (null != mCustomCountDownTimer) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        mCustomCountDownTimer = new CustomCountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long S = millisUntilFinished / 1000;
                mTvTime.setText(S < 10 ? "0" + S + "s" : S + "s");
            }

            @Override
            public void onFinish() {
                mTvTime.setEnabled(true);
                mTvTime.setTextColor(getResources().getColor(R.color.color_4870e0));
                mTvTime.setText("获取验证码");
            }
        };

        mCustomCountDownTimer.start();
    }

    //登录
    private void login() {
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString())) {
            showShortToast("请输入账号");
            return;
        }

        if (EmptyUtils.isEmpty(mEtPwd.getText().toString())) {
            showShortToast("请输入密码");
            return;
        }

        if (!isAPPAudit&&EmptyUtils.isEmpty(guid)){
            showShortToast("请到系统设置页面手动授予权限");
            // 如果是被永久拒绝就跳转到应用权限系统设置页面
            XXPermissions.startPermissionActivity(InsuredLoginActivity.this, Permission.Group.STORAGE);
            return;
        }

        if (!cb.isChecked()) {
            if (customerDialogUtils == null) {
                customerDialogUtils = new CustomerDialogUtils();
            }
            customerDialogUtils.showDialog(InsuredLoginActivity.this, "提示", "登录代表同意《用户协议》和《隐私政策》", "取消", "同意并授权",
                    new CustomerDialogUtils.IDialogListener() {

                        @Override
                        public void onBtn01Click() {
                        }

                        @Override
                        public void onBtn02Click() {
                            cb.setChecked(true);
                        }
                    });
            return;
        }

        showWaitDialog();
        InsuredLoginModel.sendInsuredLoginRequest(TAG, mEtPhone.getText().toString(), MD5Util.up32(mEtPwd.getText().toString()),
                EmptyUtils.strEmpty(JPushInterface.getRegistrationID(this)), new CustomerJsonCallBack<InsuredLoginModel>() {
            @Override
            public void onRequestError(InsuredLoginModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(InsuredLoginModel returnData) {
                InsuredLoginModel.DataBean data = returnData.getData();
                if (data != null) {
                    InsuredUserInfoModel insuredUserInfoModel= new InsuredUserInfoModel(EmptyUtils.strEmpty(data.getPhone()),EmptyUtils.strEmpty(data.getName()),
                            EmptyUtils.strEmpty(data.getIsFace()),EmptyUtils.strEmpty(data.getId_card_no()),EmptyUtils.strEmpty(data.getAccessoryId()),
                            EmptyUtils.strEmpty(data.getAccessoryUrl()),MD5Util.up32(mEtPwd.getText().toString()),data.getNickname(),
                            data.getAssociatedDiseases(),data.getSex(),data.getIsSetPaymentPwd(),data.getIsEnterpriseFundPay(),data.getUserID());

                    insuredUserInfoModel.save();

                    onLoginSuccess();
                } else {
                    hideWaitDialog();
                    showShortToast("数据有误，请重新登录");
                }
            }
        });
    }

    //验证码登录
    private void smsLogin() {
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString())) {
            showShortToast("请输入账号");
            return;
        }

        if (EmptyUtils.isEmpty(mEtPwd.getText().toString())) {
            showShortToast("请输入验证码");
            return;
        }

        if (!isAPPAudit&&EmptyUtils.isEmpty(guid)){
            showShortToast("请到系统设置页面手动授予权限");
            // 如果是被永久拒绝就跳转到应用权限系统设置页面
            XXPermissions.startPermissionActivity(InsuredLoginActivity.this, Permission.Group.STORAGE);
            return;
        }

        if (!cb.isChecked()) {
            if (customerDialogUtils == null) {
                customerDialogUtils = new CustomerDialogUtils();
            }
            customerDialogUtils.showDialog(InsuredLoginActivity.this, "提示", "登录代表同意《用户协议》和《隐私政策》", "取消", "同意并授权",
                    new CustomerDialogUtils.IDialogListener() {

                        @Override
                        public void onBtn01Click() {
                        }

                        @Override
                        public void onBtn02Click() {
                            cb.setChecked(true);
                        }
                    });
            return;
        }

        showWaitDialog();
        InsuredLoginModel.sendInsuredSmsCodeLoginRequest(TAG, mEtPhone.getText().toString(), mEtPwd.getText().toString(),
                EmptyUtils.strEmpty(JPushInterface.getRegistrationID(this)), new CustomerJsonCallBack<InsuredLoginModel>() {
                    @Override
                    public void onRequestError(InsuredLoginModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(InsuredLoginModel returnData) {
                        InsuredLoginModel.DataBean data = returnData.getData();
                        if (data != null) {
                            InsuredUserInfoModel insuredUserInfoModel= new InsuredUserInfoModel(EmptyUtils.strEmpty(data.getPhone()),EmptyUtils.strEmpty(data.getName()),
                                    EmptyUtils.strEmpty(data.getIsFace()),EmptyUtils.strEmpty(data.getId_card_no()),EmptyUtils.strEmpty(data.getAccessoryId()),
                                    EmptyUtils.strEmpty(data.getAccessoryUrl()),EmptyUtils.strEmpty(data.getPwd()),data.getNickname(),
                                    data.getAssociatedDiseases(),data.getSex(),data.getIsSetPaymentPwd(),data.getIsEnterpriseFundPay(),data.getUserID());

                            insuredUserInfoModel.save();

                            onLoginSuccess();
                        } else {
                            hideWaitDialog();
                            showShortToast("数据有误，请重新登录");
                        }
                    }
                });
    }

    //登录成功
    private void onLoginSuccess() {
        MsgBus.sendInsuredLoginStatus().post(1);
        hideWaitDialog();
        goMainPage();
    }

    //跳转首页
    private void goMainPage() {
        if (1 == backPager) {
            InsuredMainActivity.newIntance(InsuredLoginActivity.this, 0);
            overridePendingTransition(R.anim.screen_zoom_in, R.anim.screen_zoom_out);
        }

        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == XXPermissions.REQUEST_CODE) {
            if (XXPermissions.isGranted(this,  Permission.Group.STORAGE)){
                getUniId();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (1 == backPager) {
            InsuredMainActivity.newIntance(InsuredLoginActivity.this, 0);
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if (textLightUtils!=null){
            textLightUtils.onDestoryListener();
        }

        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }

        if (mCustomCountDownTimer != null) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
    }

    //backPager 0:代表返回上一页 1：代表回到首页
    public static void newIntance(Context context, int backPager) {
        Intent intent = new Intent(context, InsuredLoginActivity.class);
        intent.putExtra("backPager", backPager);
        context.startActivity(intent);
    }
}
