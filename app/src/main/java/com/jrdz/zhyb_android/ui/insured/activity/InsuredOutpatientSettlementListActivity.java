package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.ui.home.activity.MainActivity;
import com.jrdz.zhyb_android.ui.insured.adapter.InsuranceTransferListAdapter;
import com.jrdz.zhyb_android.ui.login.activity.LoginActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.SmartPhaMainActivity_user;
import com.jrdz.zhyb_android.utils.InsuredLoginSmrzUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-07-09
 * 描    述：门诊结算
 * ================================================
 */
public class InsuredOutpatientSettlementListActivity extends InsuranceTransferListActivity{
    @Override
    protected void setPagerData() {
        String[] datas={"个人便民购药","医药机构结算"};
        mAdapter.setNewData(new ArrayList(Arrays.asList(datas)));
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        String itemData = ((InsuranceTransferListAdapter) adapter).getItem(position);
        if (insuredLoginSmrzUtils == null) {
            insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
        }

        if (insuredLoginSmrzUtils.isLoginSmrz(InsuredOutpatientSettlementListActivity.this)) {
            switch (itemData){
                case "个人便民购药":
                    SmartPhaMainActivity_user.newIntance(InsuredOutpatientSettlementListActivity.this, 0);
                    break;
                case "医药机构结算":
                    //判断用户是否已激活
                    if (LoginUtils.isLogin()) {//已激活 判断是否已登录智慧医保
                        MainActivity.newIntance(InsuredOutpatientSettlementListActivity.this, 0);
                    } else {
                        LoginActivity.newIntance(InsuredOutpatientSettlementListActivity.this);
                    }
                    break;
            }
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, InsuredOutpatientSettlementListActivity.class);
        context.startActivity(intent);
    }
}
