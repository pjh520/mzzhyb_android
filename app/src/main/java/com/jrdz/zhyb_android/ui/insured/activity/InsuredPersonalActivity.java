package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.fastjson.JSONObject;
import com.alipay.mobile.android.verify.sdk.BizCode;
import com.alipay.mobile.android.verify.sdk.ServiceFactory;
import com.alipay.mobile.android.verify.sdk.interfaces.ICallback;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.model.SmrzInitDataModel;
import com.jrdz.zhyb_android.ui.insured.model.SmrzResultModel;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;

import java.util.Map;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-08-11
 * 描    述： 参保人 个人信息页面
 * ================================================
 */
public class InsuredPersonalActivity extends BaseActivity {
    public static final String GET_ZFB_BIZCODE = BizCode.Value.FACE_APP;// 支付宝人脸认证

    private ImageView mIvHead,mIvName,mIvCertno;
    private LinearLayout mLlHead;
    private TextView mTvName;
    private LinearLayout mLlName;
    private TextView mTvPhone;
    private LinearLayout mLlPhone;
    private TextView mTvCertno;
    private LinearLayout mLlCertno;
    private TextView mTvSmrz;
    private LinearLayout mLlSmrz;

    //更新参保人手机号
    private ObserverWrapper<String> mUpdateInsuredPhoneObserve = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            switch (value){
                case "3"://更新姓名
                    mTvName.setText(StringUtils.encryptionName(InsuredLoginUtils.getName()));
                    break;
                case "4"://更新身份证号码
                    mTvCertno.setText(StringUtils.encryptionIDCard(InsuredLoginUtils.getIdCardNo()));
                    break;
            }
        }
    };
    //实名认证结果
    private ObserverWrapper<String> mSmrzResultObserve=new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String s) {
            if ("SUCCESS".equals(s)){
                mTvSmrz.setText("已认证");

                mLlHead.setEnabled(false);

                mTvName.setText(StringUtils.encryptionName(InsuredLoginUtils.getName()));
                mTvCertno.setText(StringUtils.encryptionIDCard(InsuredLoginUtils.getIdCardNo()));
            }
        }
    };
    @Override
    public int getLayoutId() {
        return R.layout.activity_insured_personal;
    }

    @Override
    public void initView() {
        super.initView();
        mIvHead = findViewById(R.id.iv_head);
        mLlHead = findViewById(R.id.ll_head);
        mIvName= findViewById(R.id.iv_name);
        mTvName = findViewById(R.id.tv_name);
        mLlName = findViewById(R.id.ll_name);
        mTvPhone = findViewById(R.id.tv_phone);
        mLlPhone = findViewById(R.id.ll_phone);
        mIvCertno= findViewById(R.id.iv_certno);
        mTvCertno = findViewById(R.id.tv_certno);
        mLlCertno = findViewById(R.id.ll_certno);
        mTvSmrz = findViewById(R.id.tv_smrz);
        mLlSmrz = findViewById(R.id.ll_smrz);
    }

    @Override
    public void initData() {
        super.initData();
        MsgBus.updateInsuredInfo().observe(this, mUpdateInsuredPhoneObserve);
        MsgBus.sendSmrzResult_user().observe(this, mSmrzResultObserve);
        GlideUtils.loadImg(InsuredLoginUtils.getAccessoryUrl(), mIvHead,R.drawable.ic_insured_head,new CircleCrop());
        mTvName.setText(StringUtils.encryptionName(InsuredLoginUtils.getName()));
        mTvPhone.setText(StringUtils.encryptionPhone(InsuredLoginUtils.getPhone()));
        mTvCertno.setText(StringUtils.encryptionIDCard(InsuredLoginUtils.getIdCardNo()));

        if ("0".equals(InsuredLoginUtils.getIsFace())){//未实名认证
            mTvSmrz.setText("未认证");

            mLlHead.setEnabled(true);
            mLlName.setEnabled(true);
            mLlCertno.setEnabled(true);

            mIvName.setVisibility(View.VISIBLE);
            mIvCertno.setVisibility(View.VISIBLE);
        }else {
            mTvSmrz.setText("已认证");

            mLlHead.setEnabled(false);
            mLlName.setEnabled(false);
            mLlCertno.setEnabled(false);

            mIvName.setVisibility(View.GONE);
            mIvCertno.setVisibility(View.GONE);
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mLlName.setOnClickListener(this);
        mLlCertno.setOnClickListener(this);
        mLlSmrz.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.ll_name://修改名字
                UpdateNameActivity.newIntance(InsuredPersonalActivity.this);
                break;
            case R.id.ll_certno://修改身份证号码
                UpdateCerNoActivity.newIntance(InsuredPersonalActivity.this);
                break;
            case R.id.ll_smrz://实名认证
                if ("1".equals(InsuredLoginUtils.getIsFace())){
                    InsuredSmrzResultActivity.newIntance(InsuredPersonalActivity.this);
                }else {
                    InputSmrzBaseDataActivity.newIntance(InsuredPersonalActivity.this);
                }
//                InputSmrzBaseDataActivity.newIntance(InsuredPersonalActivity.this);
                break;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, InsuredPersonalActivity.class);
        context.startActivity(intent);
    }
}
