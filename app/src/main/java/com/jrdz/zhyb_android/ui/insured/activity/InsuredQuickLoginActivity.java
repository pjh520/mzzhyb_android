package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.switchButton.SwitchButton;
import com.frame.fingerprint_library.FingerManager;
import com.frame.fingerprint_library.SharePreferenceUtil;
import com.frame.fingerprint_library.callback.SimpleFingerCallback;
import com.frame.fingerprint_library.util.PhoneInfoCheck;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MMKVUtils;

import java.io.UnsupportedEncodingException;

import javax.crypto.Cipher;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-07-25
 * 描    述： 快捷登录页面
 * ================================================
 */
public class InsuredQuickLoginActivity extends BaseActivity {
    private TextView mTvNonsupport;
    private SwitchButton mSbQuickLogin;
    private LinearLayout mLlQuickLogin;

    private CustomerDialogUtils customerDialogUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_insured_quick_login;
    }

    @Override
    public void initView() {
        super.initView();
        mTvNonsupport = findViewById(R.id.tv_nonsupport);
        mSbQuickLogin = findViewById(R.id.sb_quick_login);
        mLlQuickLogin = findViewById(R.id.ll_quick_login);
    }

    @Override
    public void initData() {
        super.initData();

        if (FingerManager.isHardwareDetected(this)) {
            //设备支持指纹登录
            mTvNonsupport.setVisibility(View.INVISIBLE);
            mSbQuickLogin.setEnabled(true);
        } else {
            //设备不支持指纹登录
            mTvNonsupport.setVisibility(View.VISIBLE);
            mSbQuickLogin.setEnabled(false);
        }

        //设置指纹登录是否开启
        mSbQuickLogin.setCheckedImmediatelyNoEvent(!EmptyUtils.isEmpty(MMKVUtils.getString("finger_login_key", "")));
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mSbQuickLogin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {//开启指纹登录
                    openQuickLogin();
                } else {//关闭指纹登录
                    MMKVUtils.putString("finger_login_key", "");
                    mSbQuickLogin.setCheckedImmediatelyNoEvent(false);
                }
            }
        });
    }

    //开启快捷登录
    private void openQuickLogin() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            switch (FingerManager.checkSupport(this)) {
                case DEVICE_UNSUPPORTED:
                    showShortToast("您的设备不支持指纹");
                    mSbQuickLogin.setCheckedImmediatelyNoEvent(false);
                    break;
                case SUPPORT_WITHOUT_KEYGUARD:
                    mSbQuickLogin.setCheckedImmediatelyNoEvent(false);
                    //设备支持但未处于安全保护中（你的设备必须是使用屏幕锁保护的，这个屏幕锁可以是password，PIN或者图案都行）
                    showOpenSettingDialog("您还未录屏幕锁保护，是否现在开启?");
                    break;
                case SUPPORT_WITHOUT_DATA:
                    mSbQuickLogin.setCheckedImmediatelyNoEvent(false);
                    showOpenSettingDialog("您还未录入指纹信息，是否现在录入?");
                    break;
                case SUPPORT:
                    FingerManager.build().setApplication(getApplication())
                            .setTitle("指纹验证")
                            .setDes("请按下指纹")
                            .setNegativeText("取消")
                            .setDecrypt(false)
                            //.setFingerDialogApi23(new MyFingerDialog())//如果你需要自定义android P 以下系统弹窗就设置,注意需要继承BaseFingerDialog，不设置会使用默认弹窗
                            .setFingerCallback(new SimpleFingerCallback() {
                                @Override
                                public void onSucceed(Cipher cipher, byte[] bytes) {
                                    try {
                                        MMKVUtils.putString("finger_login_key", Base64.encodeToString(bytes, Base64.URL_SAFE));
                                        //存储aes 解密用到的偏移量
                                        String siv = Base64.encodeToString(cipher.getIV(), Base64.URL_SAFE);
                                        SharePreferenceUtil.saveAesIvData(RxTool.getContext(), siv);

                                        mSbQuickLogin.setCheckedImmediatelyNoEvent(true);
                                        showShortToast("指纹登录开启成功");
                                    } catch (Exception e) {
                                        e.printStackTrace();

                                        showShortToast("数据出错，请重新验证指纹");
                                        mSbQuickLogin.setCheckedImmediatelyNoEvent(false);
                                    }
                                }

                                @Override
                                public void onFailed() {
                                    showShortToast("指纹无法识别");
                                    mSbQuickLogin.setCheckedImmediatelyNoEvent(false);
                                }

                                @Override
                                public void onChange() {
                                    FingerManager.updateFingerData(InsuredQuickLoginActivity.this);
                                    openQuickLogin();
                                }

                                @Override
                                public byte[] onDoFinal() {
                                    //存储用户信息
                                    String encodeInfo = InsuredLoginUtils.getPhone() + "@#@" + InsuredLoginUtils.getPwd();
                                    byte[] bytes;
                                    try {
                                        bytes = encodeInfo.getBytes("utf-8");
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                        bytes = new byte[0];
                                    }
                                    return bytes;
                                }

                                @Override
                                public void onCancel() {
                                    showShortToast("取消指纹登录开启");
                                    mSbQuickLogin.setCheckedImmediatelyNoEvent(false);
                                }
                            })
                            .create()
                            .startListener(InsuredQuickLoginActivity.this);
                    break;
                default:
            }
        }
    }

    private void showOpenSettingDialog(String msg) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(this, "提示", msg, "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        startFingerprint();
                    }
                });
    }

    /**
     * 引导指纹录入
     */
    public void startFingerprint() {
        final String BRAND = Build.BRAND;
        PhoneInfoCheck.getInstance(this, BRAND).startFingerprint();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, InsuredQuickLoginActivity.class);
        context.startActivity(intent);
    }
}
