package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.MD5Util;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.countDownTime.CustomCountDownTimer;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.utils.PwdUtils;
import com.jrdz.zhyb_android.utils.textutils.TextLightUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-07-25
 * 描    述：参保人注册页面
 * ================================================
 */
public class InsuredRegistActivity extends BaseActivity {
    private EditText mEtPhone;
    private EditText mEtVerifCode;
    private TextView mTvTime;
    private EditText mEtPwd,mEtAgainPwd;
    private ImageView mIvPwdVisibility;
    private FrameLayout mFlPwdVisibility;
    private ImageView mIvAgainPwdVisibility;
    private FrameLayout mFlAgainPwdVisibility;
    private CheckBox mCb;
    private FrameLayout mFlCb;
    private TextView mTvAgreeRule;
    private ShapeTextView mTvRegist;

    private CustomCountDownTimer mCustomCountDownTimer;
    private boolean pwdVisi = false,againPwdVisi=false;
    private CustomerDialogUtils customerDialogUtils;
    private TextLightUtils textLightUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_insured_regist;
    }

    @Override
    public void initView() {
        super.initView();
        mEtPhone = findViewById(R.id.et_phone);
        mEtVerifCode = findViewById(R.id.et_verif_code);
        mTvTime = findViewById(R.id.tv_time);

        mEtPwd = findViewById(R.id.et_pwd);
        mIvPwdVisibility = findViewById(R.id.iv_pwd_visibility);
        mFlPwdVisibility = findViewById(R.id.fl_pwd_visibility);

        mEtAgainPwd= findViewById(R.id.et_again_pwd);
        mIvAgainPwdVisibility = findViewById(R.id.iv_again_pwd_visibility);
        mFlAgainPwdVisibility = findViewById(R.id.fl_again_pwd_visibility);

        mCb = findViewById(R.id.cb);
        mFlCb = findViewById(R.id.fl_cb);
        mTvAgreeRule = findViewById(R.id.tv_agree_rule);
        mTvRegist = findViewById(R.id.tv_regist);
    }

    @Override
    public void initData() {
        super.initData();
        textLightUtils=new TextLightUtils();
        textLightUtils.setHighlightColor(mTvAgreeRule, new TextLightUtils.IClickableSpan() {
            @Override
            public void onClickable(String title, String url) {
                MyWebViewActivity.newIntance(InsuredRegistActivity.this, title, url,true, false);
            }
        });
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvTime.setOnClickListener(this);
        mFlPwdVisibility.setOnClickListener(this);
        mFlAgainPwdVisibility.setOnClickListener(this);
        mFlCb.setOnClickListener(this);
        mTvRegist.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()){
            case R.id.tv_time://获取短信验证码
                //2022-07-25 先请求接口 接口返回发送短信验证码成功 然后开始倒计时
                sendSms();
                break;
            case R.id.fl_pwd_visibility://登录密码是否可见
                pwdVisibility();
                break;
            case R.id.fl_again_pwd_visibility://再次输入密码是否可见
                againPwdVisibility();
                break;
            case R.id.fl_cb:
                if (mCb.isChecked()) {
                    mCb.setChecked(false);
                } else {
                    mCb.setChecked(true);
                }
                break;
            case R.id.tv_regist://注册
                regist();
                break;
        }
    }

    //发送短信
    private void sendSms() {
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString().trim()) || mEtPhone.getText().toString().length() < 11) {
            showShortToast("手机号码格式不正确!");
            return;
        }
        showWaitDialog();
        BaseModel.sendSendsmsRequest(TAG, mEtPhone.getText().toString().trim(), "1", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();

                //发送短信验证码成功
                mTvTime.setEnabled(false);
                showShortToast("验证码已发送");
                mTvTime.setTextColor(getResources().getColor(R.color.color_ff0202));
                setTimeStart(60000);
            }
        });
    }

    //开启定时器  millisecond:倒计时的时间
    private void setTimeStart(long millisecond) {
        //防止内存泄漏
        if (null != mCustomCountDownTimer) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        mCustomCountDownTimer = new CustomCountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long S = millisUntilFinished / 1000;
                mTvTime.setText(S < 10 ? "0" + S + "s" : S + "s");
            }

            @Override
            public void onFinish() {
                mTvTime.setEnabled(true);
                mTvTime.setTextColor(getResources().getColor(R.color.color_4870e0));
                mTvTime.setText("获取验证码");
            }
        };

        mCustomCountDownTimer.start();
    }

    //设置密码是否可见
    private void pwdVisibility() {
        if (pwdVisi == false) {
            mEtPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            mIvPwdVisibility.setImageResource(R.drawable.ic_pwd_open);
            pwdVisi = true;
        } else {
            mEtPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mIvPwdVisibility.setImageResource(R.drawable.ic_pwd_close);
            pwdVisi = false;
        }
        RxTool.setEditTextCursorLocation(mEtPwd);
    }

    //设置再次密码是否可见
    private void againPwdVisibility() {
        if (againPwdVisi == false) {
            mEtAgainPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            mIvAgainPwdVisibility.setImageResource(R.drawable.ic_pwd_open);
            againPwdVisi = true;
        } else {
            mEtAgainPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mIvAgainPwdVisibility.setImageResource(R.drawable.ic_pwd_close);
            againPwdVisi = false;
        }
        RxTool.setEditTextCursorLocation(mEtAgainPwd);
    }

    //注册
    private void regist() {
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString().trim()) || mEtPhone.getText().toString().length() < 11) {
            showShortToast("手机号码格式不正确!");
            return;
        }

        if (EmptyUtils.isEmpty(mEtVerifCode.getText().toString().trim())) {
            showShortToast("请输入验证码!");
            return;
        }

        if (!PwdUtils.isLetterDigit(mEtPwd.getText().toString().trim())){
            showShortToast("密码由8~16位字母和数字组成!");
            return;
        }

        if (mEtPwd.getText().toString().length() < 8) {
            showShortToast("密码由8~16位字母和数字组成!");
            return;
        }

        if (!mEtPwd.getText().toString().trim().equals(mEtAgainPwd.getText().toString().trim())) {
            showShortToast("两次密码输入不一样，请重新输入");
            return;
        }

        if (!mCb.isChecked()) {
            if (customerDialogUtils == null) {
                customerDialogUtils = new CustomerDialogUtils();
            }
            customerDialogUtils.showDialog(InsuredRegistActivity.this, "提示", "登录代表同意《用户协议》和《隐私政策》", "取消", "同意并授权",
                    new CustomerDialogUtils.IDialogListener() {

                        @Override
                        public void onBtn01Click() {
                        }

                        @Override
                        public void onBtn02Click() {
                            mCb.setChecked(true);
                        }
                    });
            return;
        }

        showWaitDialog();
        BaseModel.sendInsuredRegistRequest(TAG, mEtPhone.getText().toString(), mEtVerifCode.getText().toString(), MD5Util.up32(mEtPwd.getText().toString()),
                MD5Util.up32(mEtAgainPwd.getText().toString()), new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showTipDialog(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("注册成功！");
                        finish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        if (textLightUtils!=null){
            textLightUtils.onDestoryListener();
        }
        super.onDestroy();

        if (mCustomCountDownTimer != null) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }

        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, InsuredRegistActivity.class);
        context.startActivity(intent);
    }

}
