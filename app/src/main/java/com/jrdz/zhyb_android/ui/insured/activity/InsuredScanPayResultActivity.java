package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;

import com.jrdz.zhyb_android.ui.home.activity.ScanPayResultActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.TransactionDetailActivity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-12-20
 * 描    述：扫码支付结果
 * ================================================
 */
public class InsuredScanPayResultActivity extends ScanPayResultActivity {

    @Override
    protected void goTransactionDetail() {
        TransactionDetailActivity.newIntance(this);
    }

    public static void newIntance(Context context, String tag, String msg) {
        Intent intent = new Intent(context, InsuredScanPayResultActivity.class);
        intent.putExtra("tag", tag);
        intent.putExtra("msg", msg);
        context.startActivity(intent);
    }
}
