package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.FileUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.model.CheckVersionUpdateModel;
import com.jrdz.zhyb_android.ui.mine.activity.HelpActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.AddressManageActivtiy;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.MMKVUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_insured;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.mine.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/1/22
 * 描    述：参保人模块--设置页面
 * ================================================
 */
public class InsuredSettingActivity extends BaseActivity implements CompressUploadSinglePicUtils_insured.IChoosePic {
    private LinearLayout mLlHead,mLlMyAddress,mLlPhone,mLlUpdatePwd,mLlClean,mLlHelp,mLlEnterprisePwd,mLlAccountCancellation,mLlQuickLogin,mLlVersion;
    private TextView mTvPhone,mTvClean,mTvVersion;
    private ImageView mIvHead;

    private CompressUploadSinglePicUtils_insured compressUploadSinglePicUtils;

    //更新参保人手机号
    private ObserverWrapper<String> mUpdateInsuredPhoneObserve = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            switch (value){
                case "1"://更新手机号
                    mTvPhone.setText(StringUtils.encryptionPhone(InsuredLoginUtils.getPhone()));
                    break;
            }
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.activity_insured_setting;
    }

    @Override
    public void initView() {
        super.initView();
        mLlHead= findViewById(R.id.ll_head);
        mIvHead = findViewById(R.id.iv_head);
        mLlMyAddress= findViewById(R.id.ll_my_address);
        mLlPhone = findViewById(R.id.ll_phone);
        mTvPhone = findViewById(R.id.tv_phone);
        mLlUpdatePwd = findViewById(R.id.ll_update_pwd);
        mLlClean= findViewById(R.id.ll_clean);
        mTvClean= findViewById(R.id.tv_clean);
        mLlHelp= findViewById(R.id.ll_help);
        mLlEnterprisePwd= findViewById(R.id.ll_enterprise_pwd);
        mLlAccountCancellation= findViewById(R.id.ll_account_cancellation);
        mLlQuickLogin = findViewById(R.id.ll_quick_login);
        mLlVersion= findViewById(R.id.ll_version);
        mTvVersion= findViewById(R.id.tv_version);
    }

    @Override
    public void initData() {
        super.initData();
        MsgBus.updateInsuredInfo().observe(this, mUpdateInsuredPhoneObserve);
        mTvPhone.setText(StringUtils.encryptionPhone(InsuredLoginUtils.getPhone()));
        GlideUtils.loadImg(InsuredLoginUtils.getAccessoryUrl(), mIvHead,R.drawable.ic_insured_head,new CircleCrop());

        setCacheSize();
        mTvVersion.setText(RxTool.getVersion(this) + Constants.Configure.VERSIONNAME_TAG);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mLlHead.setOnClickListener(this);
        mLlMyAddress.setOnClickListener(this);
        mLlPhone.setOnClickListener(this);
        mLlUpdatePwd.setOnClickListener(this);
        mLlClean.setOnClickListener(this);
        mLlHelp.setOnClickListener(this);
        mLlEnterprisePwd.setOnClickListener(this);
        mLlAccountCancellation.setOnClickListener(this);
        mLlQuickLogin.setOnClickListener(this);
        mLlVersion.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.ll_head://选择图片上传头像
                if (compressUploadSinglePicUtils==null){
                    compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_insured();
                    compressUploadSinglePicUtils.initChoosePic(this, true, InsuredSettingActivity.this);
                }

                compressUploadSinglePicUtils.showChoosePicTypeDialog(CompressUploadSinglePicUtils_insured.PIC_HEAD_TAG);
                break;
            case R.id.ll_my_address://我的地址
                AddressManageActivtiy.newIntance(InsuredSettingActivity.this);
                break;
            case R.id.ll_phone://修改手机号码
                InsuredUpdatePhoneActivity.newIntance(InsuredSettingActivity.this);
                break;
            case R.id.ll_update_pwd://登录密码修改
                InsuredUpdatePwdActivity.newIntance(InsuredSettingActivity.this);
                break;
            case R.id.ll_clean://清除缓存
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(InsuredSettingActivity.this, "提示", "确定要清除缓存么？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        cleanCache();
                    }
                });
                break;
            case R.id.ll_help://帮助中心
                HelpActivity.newIntance(InsuredSettingActivity.this);
                break;
            case R.id.ll_enterprise_pwd://企业基金交易密码
                EnterprisePwdActivity.newIntance(InsuredSettingActivity.this);
                break;
            case R.id.ll_account_cancellation://注销账户
                logOff();
                break;
            case R.id.ll_quick_login://快捷登录
                InsuredQuickLoginActivity.newIntance(InsuredSettingActivity.this);
                break;
            case R.id.ll_version://版本号
                //检测更新
                checkForUpdate();
                break;
        }
    }

    //设置缓存的大小
    private void setCacheSize() {
        ThreadUtils.executeBySingle(new ThreadUtils.SimpleTask<String>() {
            @Nullable
            @Override
            public String doInBackground() throws Throwable {
                long glideDirSize = FileUtils.getDirLength(BaseGlobal.getImageGlideDir());
                long compressedTempDirSize = FileUtils.getDirLength(BaseGlobal.getImageCompressedTempDir());
                long photoCacheDirDirSize = FileUtils.getDirLength(BaseGlobal.getPhotoCacheDir());
                long imageCropSize = FileUtils.getDirLength(BaseGlobal.getImageCrop());
                long audioSize = FileUtils.getDirLength(BaseGlobal.getAudioDir());
                long crashDir = FileUtils.getDirLength(BaseGlobal.getCrashDir());
                long picShotDir = FileUtils.getDirLength(BaseGlobal.getPicShotDir());
                long totalSize = glideDirSize + compressedTempDirSize + photoCacheDirDirSize + imageCropSize + audioSize + crashDir + picShotDir;
                return FileUtils.byte2FitMemorySize(totalSize);
            }

            @Override
            public void onSuccess(@Nullable String result) {
                mTvClean.setText(EmptyUtils.strEmpty(result));
            }
        });
    }

    //清楚缓存
    private void cleanCache() {
        showWaitDialog();
        ThreadUtils.executeBySingle(new ThreadUtils.SimpleTask<Void>() {
            @Nullable
            @Override
            public Void doInBackground() throws Throwable {
                FileUtils.deleteAllInDir(BaseGlobal.getImageGlideDir());
                FileUtils.deleteAllInDir(BaseGlobal.getImageCompressedTempDir());
                FileUtils.deleteAllInDir(BaseGlobal.getPhotoCacheDir());
                FileUtils.deleteAllInDir(BaseGlobal.getImageCrop());
                FileUtils.deleteAllInDir(BaseGlobal.getAudioDir());
                FileUtils.deleteAllInDir(BaseGlobal.getCrashDir());
                FileUtils.deleteAllInDir(BaseGlobal.getPicShotDir());
                return null;
            }

            @Override
            public void onSuccess(@Nullable Void result) {
                hideWaitDialog();
                setCacheSize();
            }
        });
    }

    //注销账户
    private void logOff() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(this, "提示", "是否确认注销账户?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {}

                    @Override
                    public void onBtn02Click() {
                        showWaitDialog();
                        BaseModel.sendLogOffRequest_user(TAG, new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                showShortToast("注销成功");
                                InsuredLoginActivity.newIntance(InsuredSettingActivity.this, 1);
                            }
                        });
                    }
                });
    }

    //获取是否有新版本的信息
    private void checkForUpdate() {
        CheckVersionUpdateModel.sendCheckVersionUpdateRequest(TAG, new CustomerJsonCallBack<CheckVersionUpdateModel>() {
            @Override
            public void onRequestError(CheckVersionUpdateModel returnData, String msg) {
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(CheckVersionUpdateModel returnData) {
                CheckVersionUpdateModel.DataBean info = returnData.getData();
                //1.当版本号小于后台返回的版本号 那么就需要更新
                if (info != null && RxTool.getVersionCode(InsuredSettingActivity.this) < info.getInteriorVersionNum()) {
                    showUpdatePop(info.getUrl());
                } else {
                    showShortToast("已是最新版本");
                }
            }
        });
    }

    //显示更新弹框
    private void showUpdatePop(String downloadurl) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(InsuredSettingActivity.this, "提示", "发现新版本，是否更新？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                if (Constants.Configure.IS_POS) {
                    goPosMarket();
                } else {
                    goBrowser(downloadurl);
                }
            }
        });
    }

    //跳转pos机的应用市场更新app
    private void goPosMarket() {
        String packageName = "com.centerm.cpay.applicationshop";
        String className = "com.centerm.cpay.applicationshop.activity.AppDetailActivity";
        Intent intent = new Intent();
        //这里改成你的应用包名
        intent.putExtra("packageName", "com.jrdz.zhyb_android");
        ComponentName componentName = new ComponentName(packageName, className);
        intent.setComponent(componentName);
        startActivity(intent);
    }

    //跳转浏览器下载app
    private void goBrowser(String downloadurl) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(downloadurl));
        // 注意此处的判断intent.resolveActivity()可以返回显示该Intent的Activity对应的组件名
        // 官方解释 : Name of the component implementing an activity that can display the intent
        if (intent.resolveActivity(getPackageManager()) != null) {
//            final ComponentName componentName = intent.resolveActivity(getContext().getPackageManager());
            //  LogUtil.d("suyan = " + componentName.getClassName());
            startActivity(Intent.createChooser(intent, "请选择浏览器"));
        } else {
            showShortToast("链接错误或无浏览器");
        }
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            GlideUtils.loadImg(url, mIvHead,R.drawable.ic_insured_head,new CircleCrop());
            InsuredLoginUtils.setAccessoryId(accessoryId);
            InsuredLoginUtils.setAccessoryUrl(url);
            MsgBus.updateInsuredInfo().post("2");
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }
    //--------------------------选择图片----------------------------------------------------

    @Override
    public void onResume() {
        super.onResume();
        if (mTvClean!=null) {
            setCacheSize();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, InsuredSettingActivity.class);
        context.startActivity(intent);
    }
}
