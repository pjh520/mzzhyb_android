package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.ImageUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.permission.PermissionHelper;
import com.hjq.permissions.Permission;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.huawei.hms.hmsscankit.ScanUtil;
import com.huawei.hms.hmsscankit.WriterException;
import com.huawei.hms.ml.scan.HmsBuildBitmapOption;
import com.huawei.hms.ml.scan.HmsScan;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.settlement.activity.SettleSuccessActivity;
import com.jrdz.zhyb_android.ui.settlement.activity.SmallTicketActivity;
import com.jrdz.zhyb_android.utils.ScreenshotUtil;

import java.io.File;
import java.io.FileNotFoundException;

import cn.bingoogolapple.qrcode.core.BGAQRCodeUtil;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-07-26
 * 描    述： 参保人分享页面
 * ================================================
 */
public class InsuredSharedActivity extends BaseActivity {
    private ShapeLinearLayout mSllQrCode;
    private ImageView mIvQrCode;
    private ShapeTextView mTvSave;
    private PermissionHelper permissionHelper;

    @Override
    public int getLayoutId() {
        return R.layout.activity_insured_shared;
    }

    @Override
    public void initView() {
        super.initView();
        mSllQrCode= findViewById(R.id.sll_qr_code);
        mIvQrCode = findViewById(R.id.iv_qr_code);
        mTvSave = findViewById(R.id.tv_save);
    }

    @Override
    public void initData() {
        super.initData();

        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                String content = "http://113.135.194.23:3080/appinstall/zhyb_sign.apk";
                int type = HmsScan.QRCODE_SCAN_TYPE;
                int width = getResources().getDimensionPixelSize(R.dimen.dp_376);
                int height = width;
                HmsBuildBitmapOption options = new HmsBuildBitmapOption.Creator().setBitmapBackgroundColor(Color.WHITE).setBitmapColor(Color.BLACK).setBitmapMargin(0).create();

                try {
                    // 如果未设置HmsBuildBitmapOption对象，生成二维码参数options置null。
                    Bitmap qrBitmap = ScanUtil.buildBitmap(content, type, width, height, options);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mIvQrCode.setImageBitmap(qrBitmap);
                        }
                    });

                } catch (WriterException e) {
                    Log.w("buildBitmap", e);
                }
            }
        });
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvSave.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_save://保存图片
                if (permissionHelper==null){
                    permissionHelper = new PermissionHelper();
                }

                permissionHelper.requestPermission(this, new PermissionHelper.onPermissionListener() {
                    @Override
                    public void onSuccess() {
                        savePic();
                    }

                    @Override
                    public void onNoAllSuccess(String noAllSuccessText) {

                    }

                    @Override
                    public void onFail(String failText) {
                        showShortToast("权限申请失败，app将不能保存图片");
                    }
                }, "存储权限:保存图片时,必须申请的权限", Permission.MANAGE_EXTERNAL_STORAGE);
                break;
        }
    }

    //保存图片
    private void savePic() {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                String imagPath = BaseGlobal.getImageDir() + "qrcode.jpg";
                ImageUtils.save(ScreenshotUtil.compressImage(ScreenshotUtil.view2Bitmap2(mSllQrCode)), imagPath, Bitmap.CompressFormat.JPEG);
                // 最后通知图库更新
                try {
                    MediaStore.Images.Media.insertImage(getContentResolver(), imagPath, "qrcode", "qrcode");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showShortToast("保存成功");
                    }
                });
            }
        });
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, InsuredSharedActivity.class);
        context.startActivity(intent);
    }
}
