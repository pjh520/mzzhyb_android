package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.widget.TextView;

import com.frame.compiler.utils.StringUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-08-18
 * 描    述： 实名认证成功页面
 * ================================================
 */
public class InsuredSmrzResultActivity extends BaseActivity {
    private TextView mTvName;
    private TextView mTvCertNo;

    @Override
    public int getLayoutId() {
        return R.layout.activity_insured_smrz_result;
    }

    @Override
    public void initView() {
        super.initView();
        mTvName = findViewById(R.id.tv_name);
        mTvCertNo = findViewById(R.id.tv_cert_no);
    }

    @Override
    public void initData() {
        super.initData();

        mTvName.setText("真实姓名："+ StringUtils.encryptionName(InsuredLoginUtils.getName()));
        mTvCertNo.setText("证件号码："+ StringUtils.encryptionIDCard(InsuredLoginUtils.getIdCardNo()));
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, InsuredSmrzResultActivity.class);
        context.startActivity(intent);
    }
}
