package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.widget.countDownTime.CustomCountDownTimer;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.textutils.TextLightUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-07-25
 * 描    述： 更改手机号码
 * ================================================
 */
public class InsuredUpdatePhoneActivity extends BaseActivity {
    private TextView mTvPhoneDescribe;
    private EditText mEtPhone;
    private EditText mEtVerifCode;
    private TextView mTvTime;
    private ShapeTextView mTvUpdate;

    private CustomCountDownTimer mCustomCountDownTimer;

    @Override
    public int getLayoutId() {
        return R.layout.activity_insured_update_phone;
    }

    @Override
    public void initView() {
        super.initView();
        mTvPhoneDescribe = findViewById(R.id.tv_phone_describe);
        mEtPhone = findViewById(R.id.et_phone);
        mEtVerifCode = findViewById(R.id.et_verif_code);
        mTvTime = findViewById(R.id.tv_time);
        mTvUpdate = findViewById(R.id.tv_update);
    }

    @Override
    public void initData() {
        super.initData();

        mTvPhoneDescribe.setText("更换手机号后，下次登录可使用新手机号进行登录，当前手机号为"+ StringUtils.encryptionPhone(InsuredLoginUtils.getPhone()));
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvTime.setOnClickListener(this);
        mTvUpdate.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()){
            case R.id.tv_time://获取短信验证码
                // 2022-07-25 先请求接口 接口返回发送短信验证码成功 然后开始倒计时
                sendSms();
                break;
            case R.id.tv_update://立即更新
                update();
                break;
        }
    }

    //发送短信
    private void sendSms() {
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString().trim()) || mEtPhone.getText().toString().length() < 11) {
            showShortToast("手机号码格式不正确!");
            return;
        }
        showWaitDialog();
        BaseModel.sendSendsmsRequest(TAG, mEtPhone.getText().toString().trim(), "2", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();

                //发送短信验证码成功
                mTvTime.setEnabled(false);
                showShortToast("验证码已发送");
                mTvTime.setTextColor(getResources().getColor(R.color.color_ff0202));
                setTimeStart(60000);
            }
        });
    }

    //开启定时器  millisecond:倒计时的时间
    private void setTimeStart(long millisecond) {
        //防止内存泄漏
        if (null != mCustomCountDownTimer) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        mCustomCountDownTimer = new CustomCountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long S = millisUntilFinished / 1000;
                mTvTime.setText(S < 10 ? "0" + S + "s" : S + "s");
            }

            @Override
            public void onFinish() {
                mTvTime.setEnabled(true);
                mTvTime.setTextColor(getResources().getColor(R.color.color_4870e0));
                mTvTime.setText("获取验证码");
            }
        };

        mCustomCountDownTimer.start();
    }

    //立即更新
    private void update() {
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString().trim()) || mEtPhone.getText().toString().length() < 11) {
            showShortToast("手机号码格式不正确!");
            return;
        }

        if (EmptyUtils.isEmpty(mEtVerifCode.getText().toString().trim())) {
            showShortToast("请输入验证码!");
            return;
        }

        BaseModel.sendPhoneUpdateRequest(TAG, mEtPhone.getText().toString().trim(), mEtVerifCode.getText().toString().trim(), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("手机号修改成功");
                // 2022-08-10 修改参保人用户表里面的手机号
                InsuredLoginUtils.setPhone(mEtPhone.getText().toString());
                MsgBus.updateInsuredInfo().post("1");
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mCustomCountDownTimer != null) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, InsuredUpdatePhoneActivity.class);
        context.startActivity(intent);
    }
}
