package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxTool;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.utils.PwdUtils;

/**
 * ================================================
 * 项目名称：Pumeng_master
 * 包    名：com.languo.pumeng_master.ui.login.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/6/2
 * 描    述：修改密码
 * ================================================
 */
public class InsuredUpdatePwdActivity extends BaseActivity {
    private EditText mEtOldpwd, mEtNewpwd, mEtAgainpwd;
    private ImageView mIvOldpwdVisibility, mIvNewpwdVisibility, mIvAgainpwdVisibility;
    private FrameLayout mFlOldpwdVisibility, mFlNewpwdVisibility, mFlAgainpwdVisibility;
    private ShapeTextView mTvSure;

    private boolean oldPwdVisi = false, newPwdVisi = false, againPwdVisi = false;

    @Override
    public int getLayoutId() {
        return R.layout.activity_insured_update_pwd;
    }

    @Override
    public void initView() {
        super.initView();
        mEtOldpwd = findViewById(R.id.et_oldpwd);
        mIvOldpwdVisibility = findViewById(R.id.iv_oldpwd_visibility);
        mFlOldpwdVisibility = findViewById(R.id.fl_oldpwd_visibility);
        mEtNewpwd = findViewById(R.id.et_newpwd);
        mIvNewpwdVisibility = findViewById(R.id.iv_newpwd_visibility);
        mFlNewpwdVisibility = findViewById(R.id.fl_newpwd_visibility);
        mEtAgainpwd = findViewById(R.id.et_againpwd);
        mIvAgainpwdVisibility = findViewById(R.id.iv_againpwd_visibility);
        mFlAgainpwdVisibility = findViewById(R.id.fl_againpwd_visibility);
        mTvSure = findViewById(R.id.tv_sure);
    }

    @Override
    public void initData() {
        super.initData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mFlOldpwdVisibility.setOnClickListener(this);
        mFlNewpwdVisibility.setOnClickListener(this);
        mFlAgainpwdVisibility.setOnClickListener(this);
        mTvSure.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()) {
            case R.id.fl_oldpwd_visibility:
                pwdOldVisibility();
                break;
            case R.id.fl_newpwd_visibility:
                pwdNewVisibility();
                break;
            case R.id.fl_againpwd_visibility:
                pwdAgainVisibility();
                break;
            case R.id.tv_sure:
                updatePwd();
                break;
        }
    }

    //设置旧密码是否可见
    private void pwdOldVisibility() {
//        if (mEtOldpwd.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
//            mEtOldpwd.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
//            mIvOldpwdVisibility.setImageResource(R.drawable.yanjing1);
//        }else {
//            mEtOldpwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
//            mIvOldpwdVisibility.setImageResource(R.drawable.yanjing2);
//        }

        if (oldPwdVisi == false) {
            mEtOldpwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            mIvOldpwdVisibility.setImageResource(R.drawable.yanjing2);
            oldPwdVisi = true;
        } else {
            mEtOldpwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mIvOldpwdVisibility.setImageResource(R.drawable.yanjing1);
            oldPwdVisi = false;
        }
        RxTool.setEditTextCursorLocation(mEtOldpwd);
    }

    //设置新密码是否可见
    private void pwdNewVisibility() {
//        if (mEtNewpwd.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
//            mEtNewpwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//            mIvNewpwdVisibility.setImageResource(R.drawable.yanjing1);
//        } else {
//            mEtNewpwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
//            mIvNewpwdVisibility.setImageResource(R.drawable.yanjing2);
//        }
        if (newPwdVisi == false) {
            mEtNewpwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            mIvNewpwdVisibility.setImageResource(R.drawable.yanjing2);
            newPwdVisi = true;
        } else {
            mEtNewpwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mIvNewpwdVisibility.setImageResource(R.drawable.yanjing1);
            newPwdVisi = false;
        }
        RxTool.setEditTextCursorLocation(mEtNewpwd);
    }

    //设置再次输入密码是否可见
    private void pwdAgainVisibility() {
//        if (mEtAgainpwd.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
//            mEtAgainpwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//            mIvAgainpwdVisibility.setImageResource(R.drawable.yanjing1);
//        } else {
//            mEtAgainpwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
//            mIvAgainpwdVisibility.setImageResource(R.drawable.yanjing2);
//        }
        if (againPwdVisi == false) {//显示密码
            mEtAgainpwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            mIvAgainpwdVisibility.setImageResource(R.drawable.yanjing2);
            againPwdVisi = true;
        } else {//隐藏密码
            mEtAgainpwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mIvAgainpwdVisibility.setImageResource(R.drawable.yanjing1);
            againPwdVisi = false;
        }
        RxTool.setEditTextCursorLocation(mEtAgainpwd);
    }

    //设置用户名 密码
    private void updatePwd() {
        if (EmptyUtils.isEmpty(mEtOldpwd.getText().toString())) {
            showShortToast("请输入旧密码");
            return;
        }

        if (!PwdUtils.isLetterDigit(mEtNewpwd.getText().toString().trim())){
            showShortToast("密码由8~16位字母和数字组成!");
            return;
        }

        if (mEtNewpwd.getText().toString().length() < 8) {
            showShortToast("密码长度为8-16位");
            return;
        }

        if (!mEtNewpwd.getText().toString().equals(mEtAgainpwd.getText().toString())) {
            showShortToast("两次输入的密码不一致");
            return;
        }

        showWaitDialog();
        BaseModel.sendInsuredUpdatePwdRequest(TAG, mEtOldpwd.getText().toString().trim(), mEtNewpwd.getText().toString().trim(), mEtAgainpwd.getText().toString().trim(),
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("密码修改成功,请重新登录");
                        InsuredLoginActivity.newIntance(InsuredUpdatePwdActivity.this, 1);
                        goFinish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, InsuredUpdatePwdActivity.class);
        context.startActivity(intent);
    }
}
