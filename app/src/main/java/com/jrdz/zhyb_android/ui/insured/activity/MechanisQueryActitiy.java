package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.frame.compiler.utils.viewPage.BaseViewPagerAndTabsAdapter_new;
import com.frame.compiler.widget.CustomViewPager;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.frame.lbs_library.utils.CustomLocationBean;
import com.frame.lbs_library.utils.ILocationListener;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.insured.fragment.EmployeeMedicalFragment;
import com.jrdz.zhyb_android.ui.insured.fragment.EmployeePharmacyFragment;
import com.jrdz.zhyb_android.ui.insured.fragment.MechanisMedicalQueryFragment;
import com.jrdz.zhyb_android.ui.insured.fragment.MechanisPharmacyQueryFragment;
import com.jrdz.zhyb_android.utils.location.LocationTool;

import java.util.Arrays;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-02-14
 * 描    述：机构列表信息
 * ================================================
 */
public class MechanisQueryActitiy extends BaseActivity {
    private SlidingTabLayout mStbTab;
    private CustomViewPager mVp;

    String[] titles = new String[]{"定点医药机构", "定点零售药店"};

    @Override
    public int getLayoutId() {
        return R.layout.activity_employee_clinic;
    }

    @Override
    public void initView() {
        super.initView();

        mStbTab = findViewById(R.id.stb_tab);
        mVp = findViewById(R.id.vp);
    }

    @Override
    public void initData() {
        super.initData();
        showWaitDialog();
        //获取自己当前的位置
        LocationTool.getInstance().registerLocation(this, new ILocationListener() {
            @Override
            public void onLocationSuccess(CustomLocationBean customLocationBean) {
                hideWaitDialog();
                initTabLayout(customLocationBean.getLatitude(),customLocationBean.getLongitude());
            }

            @Override
            public void onLocationError(String errorText) {
                hideWaitDialog();
                showTipDialog(errorText);
            }
        });
    }

    //初始化tablayout 跟 viewpager
    protected void initTabLayout(double lat,double lon) {
        BaseViewPagerAndTabsAdapter_new baseViewPagerAndTabsAdapter_new = new BaseViewPagerAndTabsAdapter_new(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0://定点医疗机构
                        return MechanisMedicalQueryFragment.newIntance(lat,lon);
                    case 1://定点零售药店
                        return MechanisPharmacyQueryFragment.newIntance(lat,lon);
                    default:
                        return MechanisMedicalQueryFragment.newIntance(lat,lon);
                }
            }
        };

        baseViewPagerAndTabsAdapter_new.setData(Arrays.asList(titles));
        mVp.setAdapter(baseViewPagerAndTabsAdapter_new);
        mStbTab.setViewPager(mVp);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        LocationTool.getInstance().unRegisterLocation();
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, MechanisQueryActitiy.class);
        context.startActivity(intent);
    }
}
