package com.jrdz.zhyb_android.ui.insured.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customWebview.CommonJs;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.NewsDetailActivity;
import com.jrdz.zhyb_android.ui.home.model.MizhiNewsDetailModel;
import com.jrdz.zhyb_android.ui.home.model.NewsDetailModel;
import com.jrdz.zhyb_android.ui.insured.model.InsuredNewsModel;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.GoodDetailActivity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/28
 * 描    述： 公告详情
 * ================================================
 */
public class MizhiNewsDetailActivity extends NewsDetailActivity {

//    public String dataString="米脂县电子处方<br />\n<br />\n<img alt=\"\" src=\"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/d67a6ed6-038f-481d-beb0-85b824c38119/9f7c1678-b1a3-416c-a095-755fbaf7bbc0.jpg\"/>";

    //获取数据
    @Override
    public void getData() {
        MizhiNewsDetailModel.sendNewsDetailRequest(TAG, id, new CustomerJsonCallBack<MizhiNewsDetailModel>() {
            @Override
            public void onRequestError(MizhiNewsDetailModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @SuppressLint("JavascriptInterface")
            @Override
            public void onRequestSuccess(MizhiNewsDetailModel returnData) {
                hideWaitDialog();
                InsuredNewsModel.DataBean data = returnData.getData();
                if (data != null ) {
                    mTitle=EmptyUtils.strEmpty(data.getTitle());
                    setTitle(EmptyUtils.strEmpty(data.getTitle()));
                    mTvTitle.setText(EmptyUtils.strEmpty(data.getTitle()));
                    mTvTime.setText(EmptyUtils.strEmpty(data.getCreateDT()));
                    x5Webview.loadHtml(CommonJs.headTop+EmptyUtils.strEmpty(data.getNewsContent())+CommonJs.IMG_CLICK_JS+CommonJs.headBottom);
                    x5Webview.addJavascriptInterface(MizhiNewsDetailActivity.this, "imageOnclick");//代理名
                }
            }
        });
    }

    @JavascriptInterface
    public void openImage(String src) {
        OpenImage.with(MizhiNewsDetailActivity.this)
                //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                .setNoneClickView()
                //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                //RecyclerView的数据
                .setImageUrl(src, MediaType.IMAGE)
                //点击的ImageView所在数据的位置
                .setClickPosition(0)
                //开始展示大图
                .show();
    }

    public static void newIntance(Context context, String id) {
        Intent intent = new Intent(context, MizhiNewsDetailActivity.class);
        intent.putExtra("id", id);
        context.startActivity(intent);
    }
}
