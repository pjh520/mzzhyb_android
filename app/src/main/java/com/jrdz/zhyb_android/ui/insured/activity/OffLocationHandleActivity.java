package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.ui.insured.adapter.InsuranceTransferListAdapter;
import com.jrdz.zhyb_android.utils.InsuredLoginSmrzUtils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-07-09
 * 描    述：异地就医备案
 * ================================================
 */
public class OffLocationHandleActivity extends InsuranceTransferListActivity{
    @Override
    protected void setPagerData() {
        String[] datas={"异地就医备案","异地就医备案查询","跨省异地就医结算信息查询","跨省异地就医备案信息查询"};
        mAdapter.setNewData(new ArrayList(Arrays.asList(datas)));
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        String itemData = ((InsuranceTransferListAdapter) adapter).getItem(position);
        if (insuredLoginSmrzUtils == null) {
            insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
        }

        if (insuredLoginSmrzUtils.isLoginSmrz(OffLocationHandleActivity.this)) {
            switch (itemData){
                case "异地就医备案":
                    goH5Pager("异地就医备案", Constants.H5URL.H5_ANOTHERPLACETREATREACORDAPPLY_URL);
                    break;
                case "异地就医备案查询":
                    goH5Pager("异地就医备案查询", Constants.H5URL.H5_ANOTHERPLACETREATREACORDSEARCHBASE_URL);
                    break;
                case "跨省异地就医结算信息查询":
                    goH5Pager("跨省异地就医结算信息查询", Constants.H5URL.H5_CONSUMERECORDQUERY_URL);
                    break;
                case "跨省异地就医备案信息查询":
                    goH5Pager("跨省异地就医备案信息查询", Constants.H5URL.H5_ANOTHERPLACETREATREACORDQUERY_URL);
                    break;
            }
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, OffLocationHandleActivity.class);
        context.startActivity(intent);
    }
}