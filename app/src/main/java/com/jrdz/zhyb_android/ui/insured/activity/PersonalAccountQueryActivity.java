package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ImageUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.permission.PermissionHelper;
import com.hjq.permissions.Permission;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.index.activity.ActivateDeviceActivity;
import com.jrdz.zhyb_android.ui.insured.model.PersonalInsuInfoQueryModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.DeviceID;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.ScreenshotUtil;
import com.jrdz.zhyb_android.widget.pop.PerAccountSharedPop;
import com.tencent.bugly.crashreport.CrashReport;

import java.io.FileNotFoundException;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-07-26
 * 描    述： 个人账户查询
 * ================================================
 */
public class PersonalAccountQueryActivity extends BaseActivity implements PerAccountSharedPop.IOptionListener {
    private TextView mTvZoning;
    private TextView mTvCorporateName;
    private TextView mTvInsuranceType;
    private TextView mTvInsuredStatus;
    private TextView mTvInsuredZoning;
    private TextView mTvPersonnelCate;
    private TextView mTvCivilServant;
    private TextView mTvMoney;
    private FrameLayout mFlMoneyVisinility;
    private ImageView mIvMoneyVisinility;
    private ShapeTextView mTvShared;

    private PerAccountSharedPop perAccountSharedPop;
    private PersonalInsuInfoQueryModel.DataBean.InsuinfoBean insuinfo;
    private PermissionHelper permissionHelper;
    private boolean moneyVisi = true;

    @Override
    public int getLayoutId() {
        return R.layout.activity_personal_account_query;
    }

    @Override
    public void initView() {
        super.initView();
        mTvZoning = findViewById(R.id.tv_zoning);
        mTvCorporateName = findViewById(R.id.tv_corporate_name);
        mTvInsuranceType = findViewById(R.id.tv_insurance_type);
        mTvInsuredStatus = findViewById(R.id.tv_insured_status);
        mTvInsuredZoning = findViewById(R.id.tv_insured_zoning);
        mTvPersonnelCate = findViewById(R.id.tv_personnel_cate);
        mTvCivilServant = findViewById(R.id.tv_civil_servant);
        mTvMoney = findViewById(R.id.tv_money);
        mFlMoneyVisinility= findViewById(R.id.fl_money_visinility);
        mIvMoneyVisinility= findViewById(R.id.iv_money_visinility);
        mTvShared = findViewById(R.id.tv_shared);

    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        getPersonalAccountData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mFlMoneyVisinility.setOnClickListener(this);
        mTvShared.setOnClickListener(this);
    }

    //获取人员信息
    private void getPersonalAccountData() {
        PersonalInsuInfoQueryModel.sendPersonalInsuInfoQueryRequest(TAG, "02",  InsuredLoginUtils.getIdCardNo(), new CustomerJsonCallBack<PersonalInsuInfoQueryModel>() {
            @Override
            public void onRequestError(PersonalInsuInfoQueryModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(PersonalInsuInfoQueryModel returnData) {
                hideWaitDialog();
                if (returnData.getData()!=null){
                    setPageDetail(returnData.getData());
                }
            }
        });
    }

    private void setPageDetail(PersonalInsuInfoQueryModel.DataBean data) {
        if (data.getInsuinfo()!=null&&!data.getInsuinfo().isEmpty()){
            insuinfo = data.getInsuinfo().get(0);
            //获取参保就医区划
            String insuplcAdmdvsText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("AREA_ADMVS", insuinfo.getInsuplc_admdvs());
            //获取险种类型
            String insutypeText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("INSUTYPE", insuinfo.getInsutype());
            //获取参保状态
            String psnInsuStasText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("PSN_INSU_STAS", insuinfo.getPsn_insu_stas());
            //获取人员类别
            String psnTypeText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("PSN_TYPE", insuinfo.getPsn_type());
            //获取是否公务员
            String cvlservFlagText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("CVLSERV_FLAG", insuinfo.getCvlserv_flag());

            mTvZoning.setText("统筹区划："+insuplcAdmdvsText);
            mTvCorporateName.setText(EmptyUtils.strEmptyToText(insuinfo.getEmp_name(), "--"));
            mTvInsuranceType.setText(insutypeText);
            mTvInsuredStatus.setText(psnInsuStasText);
            mTvInsuredZoning.setText(insuplcAdmdvsText);
            mTvPersonnelCate.setText(psnTypeText);
            mTvCivilServant.setText(cvlservFlagText);
            mTvMoney.setText("¥"+EmptyUtils.strEmptyToText(insuinfo.getBalc(), "0"));
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_money_visinility://是否可见金额
                if (moneyVisi){
                    mTvMoney.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    mIvMoneyVisinility.setImageResource(R.drawable.ic_pwd_close);
                    moneyVisi = false;
                }else {
                    mTvMoney.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    mIvMoneyVisinility.setImageResource(R.drawable.ic_pwd_open);
                    moneyVisi = true;
                }
                break;
            case R.id.tv_shared://分享给好友
                if (perAccountSharedPop == null) {
                    perAccountSharedPop = new PerAccountSharedPop(PersonalAccountQueryActivity.this,mTvInsuranceType.getText().toString(),
                            mTvInsuredStatus.getText().toString(),PersonalAccountQueryActivity.this);
                    perAccountSharedPop.setOutSideDismiss(true);
                }

                perAccountSharedPop.showPopupWindow();
                break;
        }
    }

    @Override
    public void onItemClick(int tag,View view) {
        switch (tag){
            case 1://保存图片
                if (permissionHelper==null){
                    permissionHelper = new PermissionHelper();
                }

                permissionHelper.requestPermission(this, new PermissionHelper.onPermissionListener() {
                    @Override
                    public void onSuccess() {
                        savePic(view);
                    }

                    @Override
                    public void onNoAllSuccess(String noAllSuccessText) {

                    }

                    @Override
                    public void onFail(String failText) {
                        showShortToast("权限申请失败，app将不能保存图片");
                    }
                }, "存储权限:保存图片时,必须申请的权限",  Permission.MANAGE_EXTERNAL_STORAGE);

                break;
        }
    }

    //保存图片到相册
    private void savePic(View view) {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                String imagPath = BaseGlobal.getImageDir() + "per_account.jpg";
                ImageUtils.save(ScreenshotUtil.compressImage(ScreenshotUtil.view2Bitmap2(view)), imagPath, Bitmap.CompressFormat.JPEG);
                // 最后通知图库更新
                try {
                    MediaStore.Images.Media.insertImage(getContentResolver(), imagPath, "per_account", "per_account");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showShortToast("保存成功");
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (perAccountSharedPop != null) {
            perAccountSharedPop.onClean();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, PersonalAccountQueryActivity.class);
        context.startActivity(intent);
    }
}
