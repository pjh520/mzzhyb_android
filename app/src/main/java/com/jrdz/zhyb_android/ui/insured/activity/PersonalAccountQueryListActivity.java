package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.alibaba.fastjson.JSON;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.H5JsonCallBack;
import com.jrdz.zhyb_android.ui.insured.adapter.InsuranceTransferListAdapter;
import com.jrdz.zhyb_android.ui.insured.model.WechatForYuLinLoginDataModel;
import com.jrdz.zhyb_android.ui.insured.model.WechatForYuLinLoginModel;
import com.jrdz.zhyb_android.utils.AESUtils;
import com.jrdz.zhyb_android.utils.H5RequestUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginSmrzUtils;
import com.library.constantStorage.ConstantStorage;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-07-03
 * 描    述：个人账户查询
 * ================================================
 */
public class PersonalAccountQueryListActivity extends InsuranceTransferListActivity{
    @Override
    protected void setPagerData() {
        String[] datas={"个人账户查询","缴费信息查询","消费信息查询","个人账户明细查询","个人就医记录查询","个人医疗账户返还查询"};
        mAdapter.setNewData(new ArrayList(Arrays.asList(datas)));
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        String itemData = ((InsuranceTransferListAdapter) adapter).getItem(position);
        if (insuredLoginSmrzUtils == null) {
            insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
        }

        if (insuredLoginSmrzUtils.isLoginSmrz(PersonalAccountQueryListActivity.this)) {
            switch (itemData){
                case "个人账户查询":
                    goH5Pager("个人账户查询", Constants.H5URL.H5_PERSONALACCOUNT_URL);
                    break;
                case "缴费信息查询":
                    goH5Pager("缴费信息查询", Constants.H5URL.H5_PAYMENTINFORMATIONQUERY_URL);
                    break;
                case "消费信息查询":
                    goH5Pager("消费信息查询", Constants.H5URL.H5_OUTPATIENTCONSUMPTIONINFORMATIONQUERY_URL);
                    break;
                case "个人账户明细查询":
                    goH5Pager("个人账户明细查询", Constants.H5URL.H5_PERSONACCOUNTDETAIL_URL);
                    break;
                case "个人就医记录查询":
                    goH5Pager("个人就医记录查询", Constants.H5URL.H5_PERSONALMEDICALRECORDSQUERY_URL);
                    break;
                case "个人医疗账户返还查询":
                    goH5Pager("个人医疗账户返还查询", Constants.H5URL.H5_PERSONALMEDICALACCOUNTRETURNQUERY_URL);
                    break;
            }
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, PersonalAccountQueryListActivity.class);
        context.startActivity(intent);
    }
}

