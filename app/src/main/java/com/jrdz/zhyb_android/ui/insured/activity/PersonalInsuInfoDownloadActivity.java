package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.collection.LruCache;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ImageUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.permission.PermissionHelper;
import com.hjq.permissions.Permission;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.adapter.PersonalInsuInfoQueryAdapter;
import com.jrdz.zhyb_android.ui.insured.model.PersonalInsuInfoQueryModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.ScreenshotUtil;

import java.io.FileNotFoundException;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-05-16
 * 描    述：参保凭证下载
 * ================================================
 */
public class PersonalInsuInfoDownloadActivity extends BaseActivity {
    private NestedScrollView mScrollView;
    private TextView mTvName,mTvCertno;
    private LinearLayout mLlInsuInfoList;
    private ShapeTextView mTvDownload;

    private PermissionHelper permissionHelper;


    @Override
    public int getLayoutId() {
        return R.layout.activity_personal_insuinfo_download;
    }

    @Override
    public void initView() {
        super.initView();
        mScrollView = findViewById(R.id.scroll_view);
        mTvName=findViewById(R.id.tv_name);
        mTvCertno=findViewById(R.id.tv_certno);
        mLlInsuInfoList = findViewById(R.id.ll_insu_info_list);
        mTvDownload = findViewById(R.id.tv_download);
    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        getData();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvDownload.setOnClickListener(this);
    }

    //获取数据
    private void getData() {
        PersonalInsuInfoQueryModel.sendPersonalInsuInfoQueryRequest(TAG, "02", InsuredLoginUtils.getIdCardNo(), new CustomerJsonCallBack<PersonalInsuInfoQueryModel>() {
            @Override
            public void onRequestError(PersonalInsuInfoQueryModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(PersonalInsuInfoQueryModel returnData) {
                hideWaitDialog();
                if (returnData.getData()!=null){
                    setPageDetail(returnData.getData());
                }
            }
        });
    }

    private void setPageDetail(PersonalInsuInfoQueryModel.DataBean data) {
        PersonalInsuInfoQueryModel.DataBean.BaseinfoBean baseinfo = data.getBaseinfo();
        if (baseinfo!=null){
            if (mTvName!=null){
                mTvName.setText(EmptyUtils.strEmptyToText(baseinfo.getPsn_name(),"--"));
            }
            if (mTvCertno!=null){
                mTvCertno.setText(StringUtils.encryptionIDCard(baseinfo.getCertno()));
            }
        }
        mLlInsuInfoList.removeAllViews();
        if (data.getInsuinfo()!=null){
            for (PersonalInsuInfoQueryModel.DataBean.InsuinfoBean item : data.getInsuinfo()) {
                View view= LayoutInflater.from(this).inflate(R.layout.layout_personal_insuinfo_query_item, mLlInsuInfoList, false);
                LinearLayout llParent=view.findViewById(R.id.ll_parent);
                ImageView ivDropdown=view.findViewById(R.id.iv_dropdown);
                LinearLayout llChild=view.findViewById(R.id.ll_child);
                TextView tvAdmdvs=view.findViewById(R.id.tv_admdvs);
                TextView tvInsuredStatue=view.findViewById(R.id.tv_insured_statue);
                TextView tvInsuredAdmdvs=view.findViewById(R.id.tv_insured_admdvs);
                TextView tvInsuredUnit=view.findViewById(R.id.tv_insured_unit);
                TextView tvPersonType=view.findViewById(R.id.tv_person_type);
                TextView tvInsuredStatue_02=view.findViewById(R.id.tv_insured_statue_02);
                TextView tvInsuredType=view.findViewById(R.id.tv_insured_type);
                //获取险种类型
                String insutypeText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("INSUTYPE", item.getInsutype());
                //获取参保就医区划
                String insuplcAdmdvsText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("AREA_ADMVS", item.getInsuplc_admdvs());
                //获取参保状态
                String psnInsuStasText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("PSN_INSU_STAS", item.getPsn_insu_stas());
                //获取人员类别
                String psnTypeText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("PSN_TYPE", item.getPsn_type());

                tvAdmdvs.setText(insutypeText);
                tvInsuredStatue.setText(psnInsuStasText);
                tvInsuredAdmdvs.setText(insuplcAdmdvsText);
                tvInsuredUnit.setText(EmptyUtils.strEmptyToText(item.getEmp_name(), "--"));
                tvPersonType.setText(psnTypeText);
                tvInsuredStatue_02.setText(psnInsuStasText);
                tvInsuredType.setText(insutypeText);

                if (item.isOpen()){
                    llChild.setVisibility(View.VISIBLE);
                    ivDropdown.setRotation(0);
                }else {
                    llChild.setVisibility(View.GONE);
                    ivDropdown.setRotation(180);
                }

                llParent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ImageView ivDropdown = v.findViewById(R.id.iv_dropdown);
                        if (item!=null&&ivDropdown!=null&&llChild!=null){
                            if (item.isOpen()){
                                //已打开的情况  现在点击 需要关闭
                                item.setOpen(false);
                                llChild.setVisibility(View.GONE);
                                ivDropdown.setRotation(180);
                            }else {
                                //已关闭的情况  现在点击 需要打开
                                item.setOpen(true);
                                llChild.setVisibility(View.VISIBLE);
                                ivDropdown.setRotation(0);
                            }
                        }
                    }
                });

                mLlInsuInfoList.addView(view);
            }
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()){
            case R.id.tv_download://下载
                onDownload();
                break;
        }
    }

    //下载
    private void onDownload() {
        if (permissionHelper==null){
            permissionHelper = new PermissionHelper();
        }

        permissionHelper.requestPermission(this, new PermissionHelper.onPermissionListener() {
            @Override
            public void onSuccess() {
                savePic(mScrollView);
            }

            @Override
            public void onNoAllSuccess(String noAllSuccessText) {

            }

            @Override
            public void onFail(String failText) {
                showShortToast("权限申请失败，app将不能保存图片");
            }
        }, "存储权限:保存图片时,必须申请的权限",  Permission.MANAGE_EXTERNAL_STORAGE);
    }

    //保存图片到相册
    private void savePic(NestedScrollView view) {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                String imag="per_insuInfo"+System.currentTimeMillis()+".jpg";
                String imagPath = BaseGlobal.getImageDir() +imag;
                ImageUtils.save(ScreenshotUtil.compressImage(ScreenshotUtil.nestedScrollViewScreenShot(view, Color.WHITE)), imagPath, Bitmap.CompressFormat.JPEG);
                // 最后通知图库更新
                try {
                    MediaStore.Images.Media.insertImage(getContentResolver(), imagPath, imag, imag);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showShortToast("保存成功");
                    }
                });
            }
        });
    }


    public static void newIntance(Context context) {
        Intent intent = new Intent(context, PersonalInsuInfoDownloadActivity.class);
        context.startActivity(intent);
    }
}
