package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.AddDoctorActivity;
import com.jrdz.zhyb_android.ui.home.activity.StmtDetailActivity;
import com.jrdz.zhyb_android.ui.home.activity.StmtInfoListActvity;
import com.jrdz.zhyb_android.ui.home.adapter.StmtInfoListAdapter;
import com.jrdz.zhyb_android.ui.home.model.KVModel;
import com.jrdz.zhyb_android.ui.home.model.StmtTotalListModel;
import com.jrdz.zhyb_android.ui.insured.adapter.PersonalInsuInfoQueryAdapter;
import com.jrdz.zhyb_android.ui.insured.model.PersonalInsuInfoQueryModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-07-26
 * 描    述： 个人参保信息查询页面
 * ================================================
 */
public class PersonalInsuInfoQueryActivity extends BaseRecyclerViewActivity {
    private TextView mTvName,mTvCertno;

    @Override
    public int getLayoutId() {
        return R.layout.activity_personal_insuinfo_query;
    }

    @Override
    public void initAdapter() {
        mAdapter=new PersonalInsuInfoQueryAdapter();
        mAdapter.setHeaderAndEmpty(true);
    }

    @Override
    public void initData() {
        super.initData();

        initHeadView();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    private void initHeadView() {
        View headView = LayoutInflater.from(this).inflate(R.layout.layout_perinsuinfo_query_headview, mRecyclerView, false);
        mTvName=headView.findViewById(R.id.tv_name);
        mTvCertno=headView.findViewById(R.id.tv_certno);

        mAdapter.addHeaderView(headView);
    }

    @Override
    public void getData() {
        PersonalInsuInfoQueryModel.sendPersonalInsuInfoQueryRequest(TAG, "02", InsuredLoginUtils.getIdCardNo(), new CustomerJsonCallBack<PersonalInsuInfoQueryModel>() {
            @Override
            public void onRequestError(PersonalInsuInfoQueryModel returnData, String msg) {
                hideRefreshView();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(PersonalInsuInfoQueryModel returnData) {
                hideRefreshView();
                if (returnData.getData()!=null){
                    setPageDetail(returnData.getData());
                }
            }
        });
    }

    private void setPageDetail(PersonalInsuInfoQueryModel.DataBean data) {
        PersonalInsuInfoQueryModel.DataBean.BaseinfoBean baseinfo = data.getBaseinfo();
        if (baseinfo!=null){
            if (mTvName!=null){
                mTvName.setText(EmptyUtils.strEmptyToText(baseinfo.getPsn_name(),"--"));
            }
            if (mTvCertno!=null){
                mTvCertno.setText(StringUtils.encryptionIDCard(baseinfo.getCertno()));
            }
        }
        if (mAdapter!=null&&data.getInsuinfo()!=null){
            mAdapter.setNewData(data.getInsuinfo());
        }
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter,view,position);
        switch (view.getId()) {
            case R.id.ll_parent:
                PersonalInsuInfoQueryModel.DataBean.InsuinfoBean itemData = ((PersonalInsuInfoQueryAdapter) adapter).getItem(position);
                ImageView ivDropdown = view.findViewById(R.id.iv_dropdown);
                LinearLayout llChild = (LinearLayout) adapter.getViewByPosition(mRecyclerView, position+1, R.id.ll_child);

                if (itemData!=null&&ivDropdown!=null&&llChild!=null){
                    if (itemData.isOpen()){
                        //已打开的情况  现在点击 需要关闭
                        itemData.setOpen(false);
                        llChild.setVisibility(View.GONE);
                        ivDropdown.setRotation(180);
                    }else {
                        //已关闭的情况  现在点击 需要打开
                        itemData.setOpen(true);
                        llChild.setVisibility(View.VISIBLE);
                        ivDropdown.setRotation(0);
                    }
                }
                break;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, PersonalInsuInfoQueryActivity.class);
        context.startActivity(intent);
    }
}
