package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;

import com.alibaba.fastjson.JSONObject;
import com.alipay.mobile.android.verify.sdk.MPVerifyService;
import com.alipay.mobile.android.verify.sdk.ServiceFactory;
import com.alipay.mobile.android.verify.sdk.interfaces.ICallback;
import com.alipay.mobile.android.verify.sdk.interfaces.IService;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.MD5Util;
import com.frame.compiler.widget.permission.PermissionHelper;
import com.hjq.permissions.Permission;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.model.SmrzInitDataModel;

import java.util.HashMap;
import java.util.Map;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-06-13
 * 描    述：修改交易密码
 * ================================================
 */
public class ResetEnterprisePwdActivity extends SetEnterprisePwdActivity {
    private PermissionHelper permissionHelper;
    private IService mService;

    @Override
    public void initData() {
        super.initData();
        //支付宝人脸认证---初始化函数涉及获取设备信息，因此需要在隐私弹框之后调用
        MPVerifyService.markUserAgreedPrivacyPolicy(this.getApplicationContext());
        // 初始化函数涉及获取设备信息，因此需要在隐私弹框之后调用，建议尽早初始化
        mService = ServiceFactory.create(this).build();
        mTvRegist.setText("人脸识别");
    }

    //确认
    @Override
    protected void regist() {
        if (permissionHelper==null){
            permissionHelper = new PermissionHelper();
        }
        permissionHelper.requestPermission(this, new PermissionHelper.onPermissionListener() {
            @Override
            public void onSuccess() {
                if (EmptyUtils.isEmpty(mEtCertNo.getText().toString())) {
                    showShortToast("请输入您的证件号码");
                    return;
                }
                if (mEtCertNo.getText().length() != 15 && mEtCertNo.getText().length() != 18) {
                    showShortToast("身份证号码为15位或者18位");
                    return;
                }
                if (EmptyUtils.isEmpty(mEtName.getText().toString())) {
                    showShortToast("请输入您的姓名");
                    return;
                }

                if (EmptyUtils.isEmpty(mEtPhone.getText().toString().trim()) || mEtPhone.getText().toString().length() < 11) {
                    showShortToast("手机号码格式不正确!");
                    return;
                }

                if (EmptyUtils.isEmpty(mEtVerifCode.getText().toString().trim())) {
                    showShortToast("请输入验证码!");
                    return;
                }

                if (mEtPwd.getText().toString().length() < 6) {
                    showShortToast("密码为6位数字!");
                    return;
                }

                if (!mEtPwd.getText().toString().trim().equals(mEtAgainPwd.getText().toString().trim())) {
                    showShortToast("两次密码输入不一样，请重新输入");
                    return;
                }

                showWaitDialog();
                getSmrzInitData();
            }

            @Override
            public void onNoAllSuccess(String noAllSuccessText) {}

            @Override
            public void onFail(String failText) {
                showShortToast("读取电话状态权限失败,请开启才能使用人脸认证");
            }
        },"读取电话状态权限:支付宝人脸认证需要使用该权限。", Permission.READ_PHONE_STATE);
    }

    //获取实名认证初始化数据
    private void getSmrzInitData() {
        SmrzInitDataModel.sendSmrzInitDataRequest(TAG,mEtName.getText().toString(),mEtCertNo.getText().toString(), new CustomerJsonCallBack<SmrzInitDataModel>() {
            @Override
            public void onRequestError(SmrzInitDataModel returnData, String msg) {
                hideWaitDialog();
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(SmrzInitDataModel returnData) {
                hideWaitDialog();
                SmrzInitDataModel.DataBean data = returnData.getData();
                if (data!=null&&!EmptyUtils.isEmpty(data.getCertify_id())){
                    goSmrz(data.getCertify_id(),data.getCertify_url());
                }else {
                    showShortToast("返回数据有误，请重新认证");
                }
            }
        });
    }

    //调用支付宝实名认证
    private void goSmrz(String certify_id,String certify_url) {
        // 封装认证数据
        Map<String, Object> requestInfo = new HashMap<>();
        requestInfo.put("certifyId", certify_id);
        requestInfo.put("bizCode", InsuredPersonalActivity.GET_ZFB_BIZCODE);

        // 发起认证
        mService.startService(requestInfo, new ICallback() {
            @Override
            public void onResponse(Map<String, String> response) {
                //调用者获得的数据: {"ex":"{}","result":"{\"certifyId\":\"7cae7d0518fdaeff93829bcd80854d59\"}","resultStatus":"9000"}
                String responseCode = response.get("resultStatus");
                switch (responseCode){
                    case "9001"://等待支付宝端完成认证
                        break;
                    case "9000"://认证通过，业务方需要去支付宝网关接口查询最终状态。
                        getSmrzResult(certify_id);
                        break;
                    case "6001"://用户取消了业务流程，主动退出。
                        showShortToast("用户取消了业务流程，主动退出");
                        break;
                    case "6002"://网络异常。
                        showShortToast("网络异常");
                        break;
                    case "6003"://业务异常。（用户因为特定原因刷脸不通过）。
                        showShortToast("业务异常。（用户因为特定原因刷脸不通过）。");
                        break;
                    case "6004"://刷脸频次过高或失败次数过多，请您稍等再试。
                        showShortToast("刷脸频次过高或失败次数过多，请您稍等再试。");
                        break;
                    case "6005"://API 限流中
                        showShortToast("API 限流中");
                        break;
                    case "4000"://系统异常。
                        showShortToast("系统异常");
                        break;
                    case "4001"://客户端接入异常，如： 非法 BizCode 没有调用 MPVerifyService.markUserAgreedPrivacyPolicy
                        showShortToast("客户端接入异常");
                        break;
                    case "4002"://没有赋予摄像头权限。
                        showShortToast("没有赋予摄像头权限");
                        break;
                    case "4003"://非权限问题的启动摄像头失败，可能是硬件问题或者 OS 问题
                        showShortToast("非权限问题的启动摄像头失败，可能是硬件问题或者 OS 问题");
                        break;
                }

            }
        });
    }

    //重置密码
    private void getSmrzResult(String certify_id){
        showWaitDialog();
        BaseModel.sendResetEnterprisePwdRequest(TAG, certify_id, MD5Util.up32(mEtPwd.getText().toString() + Constants.Configure.ENTERPRISE_PWD_TAG), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("企业基金交易密码重置成功！");
                finish();
            }
        });
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, ResetEnterprisePwdActivity.class);
        context.startActivity(intent);
    }
}
