package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frame.compiler.utils.DecimalFormatUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.CustRefreshLayout;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.text.MoneyValueFilter;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.view.ShapeEditText;
import com.hjq.shape.view.ShapeTextView;
import com.huawei.hms.hmsscankit.ScanUtil;
import com.huawei.hms.hmsscankit.WriterException;
import com.huawei.hms.ml.scan.HmsBuildBitmapOption;
import com.huawei.hms.ml.scan.HmsScan;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.ScanPayResultActivity;
import com.jrdz.zhyb_android.ui.home.model.ScanPayStatusModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.OrderDetailModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TagTextBean;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.OnlinePayActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.OrderDetailActivity_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.PaySuccessActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.model.EnterprisePayModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OrderDetailModel_user;
import com.jrdz.zhyb_android.utils.InputPwdUtils_Enterprise;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.widget.TagTextView;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.constant.RefreshState;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-12-19
 * 描    述：扫码支付-收款页面
 * ================================================
 */
public class ScanPayActivity extends BaseActivity implements InputPwdUtils_Enterprise.InputPwdListener_Enterprise {
    private TextView mTvMoney, mTvShopName;
    private ShapeTextView mTvPay;
    private LinearLayout mLlOrderListContain;
    private TextView mTvTotalNum;
    private TextView mTvTotalPrice;
    private TextView mTvDiscountAmount;
    private TextView mTvDiscount;
    private TextView mTvRealPrice;

    private String orderNo;
    private OrderDetailModel_user.DataBean detailData;
    List<TagTextBean> tags = new ArrayList<>();
    private InputPwdUtils_Enterprise inputPwdUtils_enterprise;

    @Override
    public int getLayoutId() {
        return R.layout.activity_scanpay;
    }

    @Override
    public void initView() {
        super.initView();
        mTvMoney = findViewById(R.id.tv_money);
        mTvPay = findViewById(R.id.tv_pay);
        mTvShopName = findViewById(R.id.tv_shop_name);
        mLlOrderListContain = findViewById(R.id.ll_order_list_contain);
        mTvTotalNum = findViewById(R.id.tv_total_num);
        mTvTotalPrice = findViewById(R.id.tv_total_price);

        mTvDiscountAmount = findViewById(R.id.tv_discount_amount);
        mTvDiscount = findViewById(R.id.tv_discount);
        mTvRealPrice = findViewById(R.id.tv_real_price);

        if (mTitleBar != null) {
            mTitleBar.getLeftView().setVisibility(View.GONE);
        } else {
            showShortToast("控件还未初始化!");
        }
    }

    @Override
    public void initData() {
        orderNo = getIntent().getStringExtra("OrderNo");
        super.initData();

        setRightIcon(getResources().getDrawable(R.drawable.ic_white_close));

        showWaitDialog();
        getDetailData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvPay.setOnClickListener(this);
    }

    //获取详情数据
    protected void getDetailData() {
        //2022-10-12 模拟请求数据
        OrderDetailModel_user.sendOrderDetailRequest_user(TAG, orderNo, new CustomerJsonCallBack<OrderDetailModel_user>() {
            @Override
            public void onRequestError(OrderDetailModel_user returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(OrderDetailModel_user returnData) {
                hideWaitDialog();
                detailData = returnData.getData();
                if (detailData != null) {
                    setDetailData();
                }
            }
        });
    }

    //设置页面数据
    private void setDetailData() {
        // 2022-10-31 判断是否有带处方药
        mLlOrderListContain.removeAllViews();
        String totalPrice = "0";
        int totalNum = 0;
        List<OrderDetailModel_user.DataBean.OrderGoodsBean> orderGoods = detailData.getOrderGoods();
        for (int i = 0, size = orderGoods.size(); i < size; i++) {
            OrderDetailModel_user.DataBean.OrderGoodsBean orderGood = orderGoods.get(i);

            totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(orderGood.getPrice())
                    .multiply(new BigDecimal(String.valueOf(orderGood.getGoodsNum())))).toPlainString();
            totalNum += orderGood.getGoodsNum();

            //设置商品信息
            View view = LayoutInflater.from(this).inflate(R.layout.layout_orderlist_product_item, mLlOrderListContain, false);
            ImageView ivPic = view.findViewById(R.id.iv_pic);
            ShapeTextView stvOtc = view.findViewById(R.id.stv_otc);
            ShapeTextView stvEnterpriseTag = view.findViewById(R.id.stv_enterprise_tag);
            TagTextView tvTitle = view.findViewById(R.id.tv_title);
            TextView tvEstimatePrice = view.findViewById(R.id.tv_estimate_price);
            TextView tvRealPrice = view.findViewById(R.id.tv_real_price);
            View lineItem = view.findViewById(R.id.line_item);

            GlideUtils.loadImg(orderGood.getBannerAccessoryUrl1(), ivPic, R.drawable.ic_good_placeholder,
                    new RoundedCornersTransformation(getResources().getDimensionPixelSize(R.dimen.dp_16), 0));
            //判断是否是处方药
            if ("1".equals(orderGood.getItemType())) {
                stvOtc.setText("OTC");
                stvOtc.setVisibility(View.VISIBLE);
            } else if ("2".equals(orderGood.getItemType())) {
                stvOtc.setText("处方药");
                stvOtc.setVisibility(View.VISIBLE);
            } else {
                stvOtc.setVisibility(View.GONE);
            }
            //判断是否是平台药 若是 需要展示医保
            tags.clear();
            if ("1".equals(orderGood.getIsPlatformDrug())) {//是平台药
                tags.add(new TagTextBean("医保", R.color.color_ee8734));
                tvTitle.setContentAndTag(EmptyUtils.strEmpty(orderGood.getGoodsName()), tags);
            } else {
                tvTitle.setContentAndTag(EmptyUtils.strEmpty(orderGood.getGoodsName()), tags);
            }


            //判断1.用户是否是企业基金用户 2.是否支持企业基金支付
            if ("1".equals(InsuredLoginUtils.getIsEnterpriseFundPay())&&"1".equals(orderGood.getIsEnterpriseFundPay())) {
                stvEnterpriseTag.setVisibility(View.VISIBLE);
            } else {
                stvEnterpriseTag.setVisibility(View.GONE);
            }

            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(orderGood.getPrice())));
            tvRealPrice.setText("x" + orderGood.getGoodsNum());
            //2022-10-10 最后一个item的分割线需要隐藏
            if (i == size - 1) {
                lineItem.setVisibility(View.GONE);
            } else {
                lineItem.setVisibility(View.VISIBLE);
            }
            mLlOrderListContain.addView(view);
        }

        mTvShopName.setText(EmptyUtils.strEmpty(detailData.getStoreName()));
        mTvTotalNum.setText("共" + totalNum + "件商品");
        mTvTotalPrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(totalPrice)));
        mTvMoney.setText("¥" + EmptyUtils.strEmpty(DecimalFormatUtils.noZero(detailData.getTotalAmount())));

        mTvDiscountAmount.setText(DecimalFormatUtils.noZero(EmptyUtils.strEmpty(detailData.getDiscountAmount()))+"元");
        mTvDiscount.setText(DecimalFormatUtils.noZero(EmptyUtils.strEmpty(detailData.getDiscount()))+"%");
        mTvRealPrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(detailData.getTotalAmount())));
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_pay://支付
                enterprisePay();
                break;
        }
    }

    @Override
    public void rightTitleViewClick() {
        goFinish();
    }

    //企业基金支付--1.能用企业基金， 必须满足2个条件：所购药品的商户支持使用企业基金；客户的企业基金余额大于0.
    private void enterprisePay() {
        if (!"1".equals(detailData.getIsEnterpriseFundPay())){
            showTipDialog("该商家不支持企业基金！");
            return;
        }
        //计算当前用户的企业基金 是否够支付
        double enterpriseBalance = new BigDecimal(detailData.getEnterpriseFundAmount()).subtract(new BigDecimal(detailData.getOnlineAmount())).doubleValue();
        if (enterpriseBalance >= 0) {//企业基金支付
            //初始化企业基金密码输入框
            mTitleBar.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (inputPwdUtils_enterprise == null) {
                        inputPwdUtils_enterprise = new InputPwdUtils_Enterprise();
                    }

                    inputPwdUtils_enterprise.showPwdDialog(ScanPayActivity.this, detailData.getEnterpriseFundAmount(), detailData.getOnlineAmount(), ScanPayActivity.this);
                }
            }, 400);
        } else {//
            showTipDialog("企业基金余额不足,请更换其他支付方式！");
        }
    }

    //企业基金密码输入完成回调
    @Override
    public void onComplete_Enterprise(String pwd) {
        //上传企业基金的数据
        showWaitDialog();
        EnterprisePayModel.sendScanPayEnterprisePayRequest(TAG, orderNo, pwd, new CustomerJsonCallBack<EnterprisePayModel>() {
            @Override
            public void onRequestError(EnterprisePayModel returnData, String msg) {
                hideWaitDialog();
//                showShortToast(msg);
                InsuredScanPayResultActivity.newIntance(ScanPayActivity.this,"9",msg);
            }

            @Override
            public void onRequestSuccess(EnterprisePayModel returnData) {
                hideWaitDialog();
                EnterprisePayModel.DataBean orderPayData = returnData.getData();
                if (orderPayData != null) {
                    detailData.setEnterpriseFundPayStatus("1");
                    InsuredScanPayResultActivity.newIntance(ScanPayActivity.this,"4",detailData.getOnlineAmount());
                    goFinish();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (inputPwdUtils_enterprise != null) {
            inputPwdUtils_enterprise.dismissDialog();
        }
    }

    public static void newIntance(Context context, String OrderNo) {
        Intent intent = new Intent(context, ScanPayActivity.class);
        intent.putExtra("OrderNo", OrderNo);
        context.startActivity(intent);
    }
}
