package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.MD5Util;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.countDownTime.CustomCountDownTimer;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-06-13
 * 描    述：设置交易密码
 * ================================================
 */
public class SetEnterprisePwdActivity extends BaseActivity {
    protected EditText mEtCertNo;
    protected EditText mEtName;
    protected EditText mEtPhone;
    protected EditText mEtVerifCode;
    private TextView mTvTime;
    protected EditText mEtPwd;
    private FrameLayout mFlPwdVisibility;
    private ImageView mIvPwdVisibility;
    protected EditText mEtAgainPwd;
    private FrameLayout mFlAgainPwdVisibility;
    private ImageView mIvAgainPwdVisibility;
    protected ShapeTextView mTvRegist;

    private CustomCountDownTimer mCustomCountDownTimer;
    private boolean pwdVisi = false, againPwdVisi = false;

    @Override
    public int getLayoutId() {
        return R.layout.activity_set_enterprise_pwd;
    }

    @Override
    public void initView() {
        super.initView();

        mEtCertNo = findViewById(R.id.et_cert_no);
        mEtName = findViewById(R.id.et_name);
        mEtPhone = findViewById(R.id.et_phone);
        mEtVerifCode = findViewById(R.id.et_verif_code);
        mTvTime = findViewById(R.id.tv_time);
        mEtPwd = findViewById(R.id.et_pwd);
        mFlPwdVisibility = findViewById(R.id.fl_pwd_visibility);
        mIvPwdVisibility = findViewById(R.id.iv_pwd_visibility);
        mEtAgainPwd = findViewById(R.id.et_again_pwd);
        mFlAgainPwdVisibility = findViewById(R.id.fl_again_pwd_visibility);
        mIvAgainPwdVisibility = findViewById(R.id.iv_again_pwd_visibility);
        mTvRegist = findViewById(R.id.tv_regist);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvTime.setOnClickListener(this);
        mFlPwdVisibility.setOnClickListener(this);
        mFlAgainPwdVisibility.setOnClickListener(this);
        mTvRegist.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()) {
            case R.id.tv_time://获取短信验证码
                //2022-07-25 先请求接口 接口返回发送短信验证码成功 然后开始倒计时
                sendSms();
                break;
            case R.id.fl_pwd_visibility://登录密码是否可见
                pwdVisibility();
                break;
            case R.id.fl_again_pwd_visibility://再次输入密码是否可见
                againPwdVisibility();
                break;
            case R.id.tv_regist://注册
                regist();
                break;
        }
    }

    //发送短信
    private void sendSms() {
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString().trim()) || mEtPhone.getText().toString().length() < 11) {
            showShortToast("手机号码格式不正确!");
            return;
        }
        showWaitDialog();
        BaseModel.sendSendsmsRequest(TAG, mEtPhone.getText().toString().trim(), "10", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();

                //发送短信验证码成功
                mTvTime.setEnabled(false);
                showShortToast("验证码已发送");
                mTvTime.setTextColor(getResources().getColor(R.color.color_ff0202));
                setTimeStart(60000);
            }
        });
    }

    //开启定时器  millisecond:倒计时的时间
    private void setTimeStart(long millisecond) {
        //防止内存泄漏
        if (null != mCustomCountDownTimer) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        mCustomCountDownTimer = new CustomCountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long S = millisUntilFinished / 1000;
                mTvTime.setText(S < 10 ? "0" + S + "s" : S + "s");
            }

            @Override
            public void onFinish() {
                mTvTime.setEnabled(true);
                mTvTime.setTextColor(getResources().getColor(R.color.color_4870e0));
                mTvTime.setText("获取验证码");
            }
        };

        mCustomCountDownTimer.start();
    }

    //设置密码是否可见
    private void pwdVisibility() {
        if (pwdVisi == false) {
            mEtPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            mIvPwdVisibility.setImageResource(R.drawable.ic_pwd_open);
            pwdVisi = true;
        } else {
            mEtPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mIvPwdVisibility.setImageResource(R.drawable.ic_pwd_close);
            pwdVisi = false;
        }
        RxTool.setEditTextCursorLocation(mEtPwd);
    }

    //设置再次密码是否可见
    private void againPwdVisibility() {
        if (againPwdVisi == false) {
            mEtAgainPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            mIvAgainPwdVisibility.setImageResource(R.drawable.ic_pwd_open);
            againPwdVisi = true;
        } else {
            mEtAgainPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mIvAgainPwdVisibility.setImageResource(R.drawable.ic_pwd_close);
            againPwdVisi = false;
        }
        RxTool.setEditTextCursorLocation(mEtAgainPwd);
    }

    //确认
    protected void regist() {
        if (EmptyUtils.isEmpty(mEtCertNo.getText().toString())) {
            showShortToast("请输入您的证件号码");
            return;
        }
        if (mEtCertNo.getText().length() != 15 && mEtCertNo.getText().length() != 18) {
            showShortToast("身份证号码为15位或者18位");
            return;
        }
        if (EmptyUtils.isEmpty(mEtName.getText().toString())) {
            showShortToast("请输入您的姓名");
            return;
        }

        if (EmptyUtils.isEmpty(mEtPhone.getText().toString().trim()) || mEtPhone.getText().toString().length() < 11) {
            showShortToast("手机号码格式不正确!");
            return;
        }

        if (EmptyUtils.isEmpty(mEtVerifCode.getText().toString().trim())) {
            showShortToast("请输入验证码!");
            return;
        }

        if (mEtPwd.getText().toString().length() < 6) {
            showShortToast("密码为6位数字!");
            return;
        }

        if (!mEtPwd.getText().toString().trim().equals(mEtAgainPwd.getText().toString().trim())) {
            showShortToast("两次密码输入不一样，请重新输入");
            return;
        }

        showWaitDialog();
        BaseModel.sendSetEnterprisePwdRequest(TAG, mEtCertNo.getText().toString(), mEtPhone.getText().toString(), mEtVerifCode.getText().toString(),
                MD5Util.up32(mEtPwd.getText().toString()+Constants.Configure.ENTERPRISE_PWD_TAG), new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showTipDialog(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        InsuredLoginUtils.setIsSetPaymentPwd("1");
                        showShortToast("企业基金交易密码设置成功！");
                        finish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mCustomCountDownTimer != null) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }

    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, SetEnterprisePwdActivity.class);
        context.startActivity(intent);
    }
}
