package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.frame.lbs_library.utils.CustomLocationBean;
import com.frame.lbs_library.utils.ILocationListener;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.model.KVModel;
import com.jrdz.zhyb_android.ui.insured.adapter.SlowCarePharmacyAdapter;
import com.jrdz.zhyb_android.ui.insured.model.MechanisQueryModel;
import com.jrdz.zhyb_android.ui.insured.model.SlowCarePharmacyModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.location.LocationTool;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-02-14
 * 描    述：定点慢保药店
 * ================================================
 */
public class SlowCarePharmacyActitiy extends BaseRecyclerViewActivity {
    private EditText mEtSearch;
    private TextView mTvSearch;
    private LinearLayout mLlDistance, mLlGrade, mLlZoning;
    private TextView mTvDistance, mTvGrade, mTvZoning, mTvTypeTag, mTvNum;

    private WheelUtils wheelUtils;

    private int choosePos1 = 0, choosePos2 = 0, choosePos3 = 0;
    private double lat = 0, lon = 0;

    @Override
    public int getLayoutId() {
        return R.layout.actitiy_slowcare_pharmacy;
    }

    @Override
    public void initView() {
        super.initView();
        mEtSearch = findViewById(R.id.et_search);
        mTvSearch = findViewById(R.id.stv_search);
        mLlDistance = findViewById(R.id.ll_distance);
        mTvDistance = findViewById(R.id.tv_distance);
        mLlGrade = findViewById(R.id.ll_grade);
        mTvGrade = findViewById(R.id.tv_grade);
        mLlZoning = findViewById(R.id.ll_zoning);
        mTvZoning = findViewById(R.id.tv_zoning);
        mTvTypeTag = findViewById(R.id.tv_type_tag);
        mTvNum = findViewById(R.id.tv_num);
    }

    @Override
    public void initAdapter() {
        mAdapter = new SlowCarePharmacyAdapter();
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void initData() {
        super.initData();
        showWaitDialog();
        //获取自己当前的位置
        LocationTool.getInstance().registerLocation(this, new ILocationListener() {
                    @Override
                    public void onLocationSuccess(CustomLocationBean customLocationBean) {
                        lat = customLocationBean.getLatitude();
                        lon = customLocationBean.getLongitude();
                        Log.e("Location", "location.getLatitude()=======" + customLocationBean.getLatitude() + "==location.getLongitude()==" + customLocationBean.getLongitude());
                        onRefresh(mRefreshLayout);
                    }

                    @Override
                    public void onLocationError(String errorText) {
                        hideRefreshView();
                        showTipDialog(errorText);
                    }
                });
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                    return true;
                }
                return false;
            }
        });

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                }
            }
        });

        mTvSearch.setOnClickListener(this);
        mLlDistance.setOnClickListener(this);
        mLlGrade.setOnClickListener(this);
        mLlZoning.setOnClickListener(this);
        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        super.getData();
        SlowCarePharmacyModel.sendSlowCarePharmacyRequest(TAG, String.valueOf(mPageNum), "10", mEtSearch.getText().toString(),
                null == mTvDistance.getTag() ? "1" : String.valueOf(mTvDistance.getTag()),
                null == mTvGrade.getTag() ? "" : String.valueOf(mTvGrade.getTag()),
                null == mTvZoning.getTag() ? "" : String.valueOf(mTvZoning.getTag()),String.valueOf(lat), String.valueOf(lon),
                new CustomerJsonCallBack<SlowCarePharmacyModel>() {
                    @Override
                    public void onRequestError(SlowCarePharmacyModel returnData, String msg) {
                        hideRefreshView();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(SlowCarePharmacyModel returnData) {
                        hideRefreshView();
                        List<SlowCarePharmacyModel.DataBean> infos = returnData.getData();
                        if (mAdapter != null && infos != null) {
                            if (mPageNum == 0) {
                                if (mTvNum != null) {
                                    mTvNum.setText(EmptyUtils.strEmptyToText(returnData.getTotalItems(), "0")+"家");
                                }
                                if (infos.size() <= 0) {
                                    mAdapter.isUseEmpty(true);
                                }
                                mAdapter.setNewData(infos);
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.stv_search://搜索
                if (EmptyUtils.isEmpty(mEtSearch.getText().toString())) {
                    showShortToast("请输入搜索关键词");
                    return;
                }
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.ll_distance://排序
                KeyboardUtils.hideSoftInput(SlowCarePharmacyActitiy.this);
                if (wheelUtils == null) {
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(SlowCarePharmacyActitiy.this, "", choosePos3,
                        CommonlyUsedDataUtils.getInstance().getFixmedinsSort02(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                choosePos3 = choosePos;
                                mTvDistance.setTag(dateInfo.getCode());
                                mTvDistance.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                                showWaitDialog();
                                onRefresh(mRefreshLayout);
                            }
                        });
                break;
            case R.id.ll_grade://定点批次
                KeyboardUtils.hideSoftInput(SlowCarePharmacyActitiy.this);
                if (wheelUtils == null) {
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(SlowCarePharmacyActitiy.this, "", choosePos1,
                        CommonlyUsedDataUtils.getInstance().getFixmedinsBatch(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                choosePos1 = choosePos;
                                mTvGrade.setTag(dateInfo.getCode());
                                mTvGrade.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                                showWaitDialog();
                                onRefresh(mRefreshLayout);
                            }
                        });
                break;
            case R.id.ll_zoning://医保区划
                KeyboardUtils.hideSoftInput(SlowCarePharmacyActitiy.this);
                if (wheelUtils == null) {
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(SlowCarePharmacyActitiy.this, "", choosePos2,
                        CommonlyUsedDataUtils.getInstance().getFixmedinsZoning02(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                choosePos2 = choosePos;
                                mTvZoning.setTag(dateInfo.getCode());
                                mTvZoning.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                                showWaitDialog();
                                onRefresh(mRefreshLayout);
                            }
                        });
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (wheelUtils != null) {
            wheelUtils.dissWheel();
        }

        LocationTool.getInstance().unRegisterLocation();
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, SlowCarePharmacyActitiy.class);
        context.startActivity(intent);
    }
}
