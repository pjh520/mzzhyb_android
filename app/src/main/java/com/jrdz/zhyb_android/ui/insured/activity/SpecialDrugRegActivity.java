package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.wheel.TimeWheelUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.model.FixmedinsListModel;
import com.jrdz.zhyb_android.ui.insured.model.SpecialDrugDetailModel;
import com.jrdz.zhyb_android.ui.insured.model.SpecialDrugListModel;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.SpecialDrugRegPicAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.model.FreeBackPicModel;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-12
 * 描    述：特殊药品备案
 * ================================================
 */
public class SpecialDrugRegActivity extends BaseActivity implements CompressUploadSinglePicUtils_zhyf.IChoosePic {
    protected NestedScrollView mScrollView;
    protected EditText mEtPerName, mEtPerCertno, mEtPerPhone, mEtFixmedinsName, mEtDeptName, mEtDoctorName, mEtDrugName, mEtDrugNum;
    protected LinearLayout mSllStartDate, mSllEndDate, mLlBasisDataList;
    protected TextView mTvFixmedinsNameSelect, mTvDeptNameSelect, mTvDoctorNameSelect, mTvDrugNameSelect, mTvDrugMpu, mEtStartDate, mEtEndDate;
    protected LinearLayout mLlFixed, mLlBasisDataTag, mLlImglistTag, mLlImglist02Tag;
    protected ShapeTextView mTvBasisDataCommit;

    protected CustomeRecyclerView mCrlImglist01;
    protected CustomeRecyclerView mCrlImglist02;
    protected CustomeRecyclerView mCrlImglist03;
    protected CustomeRecyclerView mCrlImglist04;
    private ShapeFrameLayout mSllSignature;
    private TextView mTvSignature;
    private ImageView mIvSignatureTag, mIvSignature;
    protected ShapeTextView mTvCommit;
    protected SpecialDrugRegPicAdapter freeBackPicAdapter01, freeBackPicAdapter02, freeBackPicAdapter03, freeBackPicAdapter04;

    public static final int PIC_NOR = 1;//新增图片
    public static final int PIC_ADDBTN = 2;//添加按钮
    public final static String TAG_SELECT_PIC_01 = "1";//申请表资料图片
    public final static String TAG_SELECT_PIC_02 = "2";//处方资料图片
    public final static String TAG_SELECT_PIC_03 = "3";//诊断资料图片
    public final static String TAG_SELECT_PIC_04 = "4";//病历资料图片
    private CompressUploadSinglePicUtils_zhyf compressUploadSinglePicUtils;
    private TimeWheelUtils timeWheelUtils;
    protected String commitmentLetterAccessoryId = "";//承诺书附件
    protected ArrayList<SpecialDrugDetailModel.DataBean.MedInsuDirListBean> basicDataModels = new ArrayList<>();
    protected boolean isAddByForm;

    @Override
    public int getLayoutId() {
        return R.layout.activity_special_drug_reg;
    }

    @Override
    protected void initImmersionBar() {
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mTitleBar).keyboardEnable(true);
        mImmersionBar.init();
    }

    @Override
    public void initView() {
        super.initView();
        mScrollView = findViewById(R.id.scrollView);
        mLlFixed = findViewById(R.id.ll_fixed);
        mEtPerName = findViewById(R.id.et_per_name);
        mEtPerCertno = findViewById(R.id.et_per_certno);
        mEtPerPhone = findViewById(R.id.et_per_phone);

        mLlBasisDataTag = findViewById(R.id.ll_basis_data_tag);
        mEtFixmedinsName = findViewById(R.id.et_fixmedins_name);
        mTvFixmedinsNameSelect = findViewById(R.id.tv_fixmedins_name_select);
        mEtDeptName = findViewById(R.id.et_dept_name);
        mTvDeptNameSelect = findViewById(R.id.tv_dept_name_select);
        mEtDoctorName = findViewById(R.id.et_doctor_name);
        mTvDoctorNameSelect = findViewById(R.id.tv_doctor_name_select);
        mEtDrugName = findViewById(R.id.et_drug_name);
        mTvDrugNameSelect = findViewById(R.id.tv_drug_name_select);
        mEtDrugNum = findViewById(R.id.et_drug_num);
        mTvDrugMpu = findViewById(R.id.tv_drug_mpu);
        mSllStartDate = findViewById(R.id.sll_start_date);
        mEtStartDate = findViewById(R.id.et_start_date);
        mSllEndDate = findViewById(R.id.sll_end_date);
        mEtEndDate = findViewById(R.id.et_end_date);
        mTvBasisDataCommit = findViewById(R.id.tv_basis_data_commit);
        mLlBasisDataList = findViewById(R.id.ll_basis_data_list);

        mLlImglistTag = findViewById(R.id.ll_imglist_tag);
        mCrlImglist01 = findViewById(R.id.crl_imglist_01);
        mLlImglist02Tag = findViewById(R.id.ll_imglist_02_tag);
        mCrlImglist02 = findViewById(R.id.crl_imglist_02);
        mCrlImglist03 = findViewById(R.id.crl_imglist_03);
        mCrlImglist04 = findViewById(R.id.crl_imglist_04);

        mSllSignature = findViewById(R.id.sll_signature);
        mIvSignatureTag = findViewById(R.id.iv_signature_tag);
        mTvSignature = findViewById(R.id.tv_signature);
        mIvSignature = findViewById(R.id.iv_signature);

        mTvCommit = findViewById(R.id.tv_commit);
    }

    @Override
    public void initData() {
        super.initData();
        mEtPerName.setText(EmptyUtils.strEmpty(InsuredLoginUtils.getName()));
        mEtPerCertno.setText(EmptyUtils.strEmpty(InsuredLoginUtils.getIdCardNo()));
        mEtPerPhone.setText(EmptyUtils.strEmpty(InsuredLoginUtils.getPhone()));

        //初始化获取图片
        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);

        FreeBackPicModel freeBackPic_addbtn = new FreeBackPicModel("-1", "", PIC_ADDBTN);
        //设置申请表资料图片列表
        mCrlImglist01.setHasFixedSize(true);
        mCrlImglist01.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
        freeBackPicAdapter01 = new SpecialDrugRegPicAdapter();
        mCrlImglist01.setAdapter(freeBackPicAdapter01);
        //添加图片增加按钮数据-申请表资料
        ArrayList<FreeBackPicModel> freeBackPicModels01 = new ArrayList<>();
        freeBackPicModels01.add(freeBackPic_addbtn);
        freeBackPicAdapter01.setNewData(freeBackPicModels01);

        //设置处方资料图片列表
        mCrlImglist02.setHasFixedSize(true);
        mCrlImglist02.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
        freeBackPicAdapter02 = new SpecialDrugRegPicAdapter();
        mCrlImglist02.setAdapter(freeBackPicAdapter02);
        //添加图片增加按钮数据-处方资料
        ArrayList<FreeBackPicModel> freeBackPicModels02 = new ArrayList<>();
        freeBackPicModels02.add(freeBackPic_addbtn);
        freeBackPicAdapter02.setNewData(freeBackPicModels02);

        //设置诊断资料图片列表
        mCrlImglist03.setHasFixedSize(true);
        mCrlImglist03.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
        freeBackPicAdapter03 = new SpecialDrugRegPicAdapter();
        mCrlImglist03.setAdapter(freeBackPicAdapter03);
        //添加图片增加按钮数据-诊断资料
        ArrayList<FreeBackPicModel> freeBackPicModels03 = new ArrayList<>();
        freeBackPicModels03.add(freeBackPic_addbtn);
        freeBackPicAdapter03.setNewData(freeBackPicModels03);

        //设置病历资料图片列表
        mCrlImglist04.setHasFixedSize(true);
        mCrlImglist04.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
        freeBackPicAdapter04 = new SpecialDrugRegPicAdapter();
        mCrlImglist04.setAdapter(freeBackPicAdapter04);
        //添加图片增加按钮数据-病历资料
        ArrayList<FreeBackPicModel> freeBackPicModels04 = new ArrayList<>();
        freeBackPicModels04.add(freeBackPic_addbtn);
        freeBackPicAdapter04.setNewData(freeBackPicModels04);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mSllStartDate.setOnClickListener(this);
        mSllEndDate.setOnClickListener(this);
        mTvFixmedinsNameSelect.setOnClickListener(this);
        mTvDeptNameSelect.setOnClickListener(this);
        mTvDoctorNameSelect.setOnClickListener(this);
        mTvDrugNameSelect.setOnClickListener(this);
        mTvBasisDataCommit.setOnClickListener(this);
        mSllSignature.setOnClickListener(this);
        mTvCommit.setOnClickListener(this);

        setImageClickEvent(freeBackPicAdapter01, 1);
        setImageClickEvent(freeBackPicAdapter02, 2);
        setImageClickEvent(freeBackPicAdapter03, 3);
        setImageClickEvent(freeBackPicAdapter04, 4);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.sll_start_date://开始日期
                KeyboardUtils.hideSoftInput(mSllStartDate);
                if (timeWheelUtils == null) {
                    timeWheelUtils = new TimeWheelUtils();
                    timeWheelUtils.isShowDay(true, true, true, true, true, true);
                }
                timeWheelUtils.showTimeWheel(SpecialDrugRegActivity.this, "开始日期", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mEtStartDate.setText(EmptyUtils.strEmpty(dateInfo));
                        if (!EmptyUtils.isEmpty(mEtStartDate.getText().toString()) && !EmptyUtils.isEmpty(mEtEndDate.getText().toString())) {
                            if (!isWithin3Months(mEtStartDate.getText().toString(), mEtEndDate.getText().toString())) {
                                mEtStartDate.setText("");
                                showTipDialog("开始时间到结束时间不能超过3个月");
                            }
                        }
                    }
                });
                break;
            case R.id.sll_end_date://结束日期
                KeyboardUtils.hideSoftInput(mSllEndDate);
                if (timeWheelUtils == null) {
                    timeWheelUtils = new TimeWheelUtils();
                    timeWheelUtils.isShowDay(true, true, true, true, true, true);
                }
                timeWheelUtils.showTimeWheel(SpecialDrugRegActivity.this, "结束日期", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mEtEndDate.setText(EmptyUtils.strEmpty(dateInfo));

                        if (!EmptyUtils.isEmpty(mEtStartDate.getText().toString()) && !EmptyUtils.isEmpty(mEtEndDate.getText().toString())) {
                            if (!isWithin3Months(mEtStartDate.getText().toString(), mEtEndDate.getText().toString())) {
                                mEtEndDate.setText("");
                                showTipDialog("开始时间到结束时间不能超过3个月");
                            }
                        }
                    }
                });
                break;
            case R.id.tv_fixmedins_name_select:
            case R.id.tv_dept_name_select:
            case R.id.tv_doctor_name_select:
                startActivityForResult(new Intent(SpecialDrugRegActivity.this, FixmedinsListActivity.class), new OnActivityCallback() {
                    @Override
                    public void onActivityResult(int resultCode, @Nullable Intent data) {
                        if (resultCode == -1) {
                            FixmedinsListModel.DataBean fixmedinsData = data.getParcelableExtra("fixmedinsData");
                            mEtFixmedinsName.setTag(fixmedinsData.getFixmedins_code());
                            mEtFixmedinsName.setText(EmptyUtils.strEmpty(fixmedinsData.getFixmedins_name()));
                            mEtDeptName.setTag(fixmedinsData.getProfessionalTitle());
                            mEtDeptName.setText(EmptyUtils.strEmpty(fixmedinsData.getHosp_dept_name()));
                            mEtDoctorName.setTag(fixmedinsData.getDr_code());
                            mEtDoctorName.setText(EmptyUtils.strEmpty(fixmedinsData.getDr_name()));
                        }
                    }
                });
                break;
            case R.id.tv_drug_name_select:
                startActivityForResult(new Intent(SpecialDrugRegActivity.this, SpecialDrugListActivity.class), new OnActivityCallback() {
                    @Override
                    public void onActivityResult(int resultCode, @Nullable Intent data) {
                        if (resultCode == -1) {
                            SpecialDrugListModel.DataBean specialDrugData = data.getParcelableExtra("specialDrugData");
                            mEtDrugName.setTag(R.id.tag_1, specialDrugData.getMIC_Code());//药品编码
                            mEtDrugName.setTag(R.id.tag_2, specialDrugData.getMpu());//最小包装单位
                            mEtDrugName.setText(EmptyUtils.strEmpty(specialDrugData.getItemName()));
                            mTvDrugMpu.setText(EmptyUtils.strEmpty(specialDrugData.getMpu()));
                        }
                    }
                });
                break;
            case R.id.tv_basis_data_commit://提交基础资料
                if (EmptyUtils.isEmpty(mEtFixmedinsName.getText().toString())) {
                    showShortToast("机构名称为必填项");
                    return;
                }
                if (EmptyUtils.isEmpty(mEtDeptName.getText().toString())) {
                    showShortToast("所属科室为必填项");
                    return;
                }
                if (EmptyUtils.isEmpty(mEtDoctorName.getText().toString())) {
                    showShortToast("医师名称为必填项");
                    return;
                }
                if (EmptyUtils.isEmpty(mEtDrugName.getText().toString())) {
                    showShortToast("药品名称为必填项");
                    return;
                }
                if (EmptyUtils.isEmpty(mEtDrugNum.getText().toString())) {
                    showShortToast("药品数量为必填项");
                    return;
                }
                if (EmptyUtils.isEmpty(mEtStartDate.getText().toString())) {
                    showShortToast("开始时间为必填项");
                    return;
                }
                if (EmptyUtils.isEmpty(mEtEndDate.getText().toString())) {
                    showShortToast("结束时间为必填项");
                    return;
                }
                onBasisDataCommit(new SpecialDrugDetailModel.DataBean.MedInsuDirListBean(
                        null == mEtFixmedinsName.getTag() ? "" : mEtFixmedinsName.getTag().toString(), mEtFixmedinsName.getText().toString(),
                        mEtDeptName.getText().toString(),
                        null == mEtDoctorName.getTag() ? "" : mEtDoctorName.getTag().toString(), mEtDoctorName.getText().toString(),
                        null == mEtDeptName.getTag() ? "" : mEtDeptName.getTag().toString(),
                        null == mEtDrugName.getTag(R.id.tag_1) ? "" : mEtDrugName.getTag(R.id.tag_1).toString(), mEtDrugName.getText().toString(),
                        mEtDrugNum.getText().toString(),
                        null == mEtDrugName.getTag(R.id.tag_2) ? "" : mEtDrugName.getTag(R.id.tag_2).toString(),
                        mEtStartDate.getText().toString(), mEtEndDate.getText().toString()));
                break;
            case R.id.sll_signature://查阅个人承诺书并完成签名
                onSignature();
                break;
            case R.id.tv_commit://提交
                commit();
                break;
        }
    }

    //提交基础资料
    protected void onBasisDataCommit(SpecialDrugDetailModel.DataBean.MedInsuDirListBean basicDataModel) {
        mEtFixmedinsName.setText("");
        mEtDeptName.setText("");
        mEtDoctorName.setText("");
        mEtDrugName.setText("");
        mEtDrugNum.setText("");
        mEtStartDate.setText("");
        mEtEndDate.setText("");
        //添加为基础备案列表的item
        View view = LayoutInflater.from(SpecialDrugRegActivity.this).inflate(R.layout.layout_basis_data_item, mLlBasisDataList, false);
        FrameLayout flDelete = view.findViewById(R.id.fl_delete);
        TextView tvFixmedinsName = view.findViewById(R.id.tv_fixmedins_name);
        TextView tvDeptName = view.findViewById(R.id.tv_dept_name);
        TextView tvDoctorName = view.findViewById(R.id.tv_doctor_name);
        TextView tvDrugCode = view.findViewById(R.id.tv_drug_code);
        TextView tvDrugName = view.findViewById(R.id.tv_drug_name);
        TextView tvDrugNum = view.findViewById(R.id.tv_drug_num);
        TextView tvBegintime = view.findViewById(R.id.tv_begintime);
        TextView tvEndtime = view.findViewById(R.id.tv_endtime);

        tvFixmedinsName.setText("机构名称：" + EmptyUtils.strEmptyToText(basicDataModel.getFixmedins_name(), "--"));
        tvDeptName.setText("科室名称：" + EmptyUtils.strEmptyToText(basicDataModel.getHosp_dept_name(), "--"));
        tvDoctorName.setText("医师名称：" + EmptyUtils.strEmptyToText(basicDataModel.getDr_name(), "--"));
        tvDrugCode.setText("药品代码：" + EmptyUtils.strEmptyToText(basicDataModel.getMIC_Code(), "--"));
        tvDrugName.setText("注册名称：" + EmptyUtils.strEmptyToText(basicDataModel.getItemName(), "--"));
        tvDrugNum.setText("药品数量：" + EmptyUtils.strEmptyToText(basicDataModel.getCnt(), "--") + basicDataModel.getCnt_prcunt());
        tvBegintime.setText("开始时间：" + EmptyUtils.strEmptyToText(basicDataModel.getBegintime(), "--"));
        tvEndtime.setText("结束时间：" + EmptyUtils.strEmptyToText(basicDataModel.getEndtime(), "--"));
        //删除选择
        flDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(SpecialDrugRegActivity.this, "提示", "是否确认删除该备案基础资料?", "取消", "确定",
                        new CustomerDialogUtils.IDialogListener() {

                            @Override
                            public void onBtn01Click() {
                            }

                            @Override
                            public void onBtn02Click() {
                                mLlBasisDataList.removeView(view);
                                basicDataModels.remove(basicDataModel);
                            }
                        });
            }
        });
        mLlBasisDataList.addView(view);
        basicDataModels.add(basicDataModel);
    }

    //计算日期--开始时间到结束时间设定逻辑不能超过3个月  true 在3个月之内 false 超过3个月
    private boolean isWithin3Months(String time1, String time2) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = sdf.parse(time1);
            Date date2 = sdf.parse(time2);
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            cal1.setTime(date1);
            cal2.setTime(date2);
            int intervalYear = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR);
            int intervalMonth = cal2.get(Calendar.MONTH) - cal1.get(Calendar.MONTH);
            int intervalDay = cal2.get(Calendar.DAY_OF_MONTH) - cal1.get(Calendar.DAY_OF_MONTH);
            int totalIntervalMonth = intervalYear * 12 + intervalMonth;

//            Log.e("isWithin3Months", "intervalYear=="+intervalYear+"intervalMonth=="+intervalMonth+
//                    "intervalDay=="+intervalDay+"totalIntervalMonth=="+totalIntervalMonth);
            if ((totalIntervalMonth > 0 && totalIntervalMonth < 3) || (totalIntervalMonth == 0 && intervalDay >= 0) || (totalIntervalMonth == 3 && intervalDay == 0)) {
                return true;
            }
            return false;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    //设置图片点击事件
    private void setImageClickEvent(SpecialDrugRegPicAdapter freeBackPicAdapter, int tag) {
        freeBackPicAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                FreeBackPicModel itemData = freeBackPicAdapter.getItem(i);
                if (PIC_NOR == itemData.getItemType()) {//查看图片
                    ImageView ivPhoto = view.findViewById(R.id.iv_photo);
                    OpenImage.with(SpecialDrugRegActivity.this)
                            //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                            .setClickImageView(ivPhoto)
                            //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                            .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP, true)
                            //RecyclerView的数据
                            .setImageUrl(itemData.getImg(), MediaType.IMAGE)
                            //点击的ImageView所在数据的位置
                            .setClickPosition(0)
                            //开始展示大图
                            .show();
                } else if (PIC_ADDBTN == itemData.getItemType()) {//新增按钮
                    KeyboardUtils.hideSoftInput(SpecialDrugRegActivity.this);
                    String imaTag = TAG_SELECT_PIC_01;
                    String type = CompressUploadSinglePicUtils_zhyf.PIC_SPECIAL_DRUG_01_TAG;
                    switch (tag) {
                        case 1:
                            imaTag = TAG_SELECT_PIC_01;
                            type = CompressUploadSinglePicUtils_zhyf.PIC_SPECIAL_DRUG_01_TAG;
                            break;
                        case 2:
                            imaTag = TAG_SELECT_PIC_02;
                            type = CompressUploadSinglePicUtils_zhyf.PIC_SPECIAL_DRUG_02_TAG;
                            break;
                        case 3:
                            imaTag = TAG_SELECT_PIC_03;
                            type = CompressUploadSinglePicUtils_zhyf.PIC_SPECIAL_DRUG_03_TAG;
                            break;
                        case 4:
                            imaTag = TAG_SELECT_PIC_04;
                            type = CompressUploadSinglePicUtils_zhyf.PIC_SPECIAL_DRUG_04_TAG;
                            break;
                    }
                    compressUploadSinglePicUtils.showChoosePicTypeDialog(imaTag, type);
                }
            }
        });

        freeBackPicAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }

                switch (view.getId()) {
                    case R.id.iv_delete://删除图片
                        deletePic(freeBackPicAdapter, i);
                        break;
                }
            }
        });
    }

    //删除图片
    private void deletePic(SpecialDrugRegPicAdapter freeBackPicAdapter, int pos) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(SpecialDrugRegActivity.this, "提示", "是否确认删除图片?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        //删除科室
                        showWaitDialog();
                        String accessoryId = freeBackPicAdapter.getData().get(pos).getId();
                        BaseModel.sendDelAttachmentUploadRequest(TAG, accessoryId, new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                freeBackPicAdapter.getData().remove(pos);
                                freeBackPicAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                });
    }

    //查阅个人承诺书并完成签名
    private void onSignature() {
        startActivityForResult(new Intent(this, CommitmentLetterActivity.class), new OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == RESULT_OK) {
                    commitmentLetterAccessoryId = data.getStringExtra("accessoryId");
                    String accessoryUrl = data.getStringExtra("accessoryUrl");
                    int bitmapWidth = data.getIntExtra("bitmapWidth", 0);
                    int bitmapHeight = data.getIntExtra("bitmapHeight", 0);
                    setSignature(accessoryUrl, bitmapWidth, bitmapHeight);
                }
            }
        });
    }

    //设置签名数据
    protected void setSignature(String accessoryUrl, int bitmapWidth, int bitmapHeight) {
        int imageWidth = getResources().getDimensionPixelSize(R.dimen.dp_160);// ImageView的宽
        mIvSignatureTag.setVisibility(View.GONE);
        mTvSignature.setVisibility(View.GONE);
        mIvSignature.setVisibility(View.VISIBLE);
        //计算展示图片的高度
        int imageHeight = new BigDecimal(String.valueOf(imageWidth))
                .divide(new BigDecimal(String.valueOf(bitmapWidth)), 2, BigDecimal.ROUND_HALF_UP)
                .multiply(new BigDecimal(String.valueOf(bitmapHeight))).intValue();
        FrameLayout.LayoutParams parma = (FrameLayout.LayoutParams) mIvSignature.getLayoutParams();
        parma.height = imageHeight;
        mIvSignature.setLayoutParams(parma);
        GlideUtils.loadImg(accessoryUrl, mIvSignature);

        mTvCommit.setEnabled(true);
    }

    //提交
    protected void commit() {
        if (EmptyUtils.isEmpty(mEtPerName.getText().toString())) {
            showShortToast("请填写备案姓名");
            goSpecifyLocation(mLlFixed.getTop());
            return;
        }
        if (EmptyUtils.isEmpty(mEtPerCertno.getText().toString())) {
            showShortToast("请填写身份证号");
            goSpecifyLocation(mLlFixed.getTop() + getResources().getDimensionPixelSize(R.dimen.dp_87));
            return;
        }
        if (EmptyUtils.isEmpty(mEtPerPhone.getText().toString())) {
            showShortToast("请填写联系电话");
            goSpecifyLocation(mLlFixed.getTop() + 2 * getResources().getDimensionPixelSize(R.dimen.dp_87));
            return;
        }

        //添加数据的时候 是否有从表单直接添加一条数据
        isAddByForm=false;
        if (basicDataModels == null || basicDataModels.isEmpty()) {
            int basisDataPerfect = getBasisDataPerfect();

            if (basisDataPerfect<7) {
                showTipDialog("备案基础资料尚未填写完整，请先填写");
                goSpecifyLocation(mLlBasisDataTag.getTop());
                return;
            } else {
                SpecialDrugDetailModel.DataBean.MedInsuDirListBean medInsuDirListBean = new SpecialDrugDetailModel.DataBean.MedInsuDirListBean(
                        null == mEtFixmedinsName.getTag() ? "" : mEtFixmedinsName.getTag().toString(), mEtFixmedinsName.getText().toString(),
                        mEtDeptName.getText().toString(),
                        null == mEtDoctorName.getTag() ? "" : mEtDoctorName.getTag().toString(), mEtDoctorName.getText().toString(),
                        null == mEtDeptName.getTag() ? "" : mEtDeptName.getTag().toString(),
                        null == mEtDrugName.getTag(R.id.tag_1) ? "" : mEtDrugName.getTag(R.id.tag_1).toString(), mEtDrugName.getText().toString(),
                        mEtDrugNum.getText().toString(),
                        null == mEtDrugName.getTag(R.id.tag_2) ? "" : mEtDrugName.getTag(R.id.tag_2).toString(),
                        mEtStartDate.getText().toString(), mEtEndDate.getText().toString());
                isAddByForm=true;
                basicDataModels.add(medInsuDirListBean);
            }
        } else {
            int basisDataPerfect = getBasisDataPerfect();
            if (basisDataPerfect>0&&basisDataPerfect<7){
                showTipDialog2("第" + (basicDataModels.size() + 1) + "个药品信息尚未填写完整，确定要提交", new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                        goSpecifyLocation(mLlBasisDataTag.getTop());
                    }

                    @Override
                    public void onBtn02Click() {
                        continueCommit(isAddByForm);
                    }
                });
                return;
            }else if (basisDataPerfect==7){
                SpecialDrugDetailModel.DataBean.MedInsuDirListBean medInsuDirListBean = new SpecialDrugDetailModel.DataBean.MedInsuDirListBean(
                        null == mEtFixmedinsName.getTag() ? "" : mEtFixmedinsName.getTag().toString(), mEtFixmedinsName.getText().toString(),
                        mEtDeptName.getText().toString(),
                        null == mEtDoctorName.getTag() ? "" : mEtDoctorName.getTag().toString(), mEtDoctorName.getText().toString(),
                        null == mEtDeptName.getTag() ? "" : mEtDeptName.getTag().toString(),
                        null == mEtDrugName.getTag(R.id.tag_1) ? "" : mEtDrugName.getTag(R.id.tag_1).toString(), mEtDrugName.getText().toString(),
                        mEtDrugNum.getText().toString(),
                        null == mEtDrugName.getTag(R.id.tag_2) ? "" : mEtDrugName.getTag(R.id.tag_2).toString(),
                        mEtStartDate.getText().toString(), mEtEndDate.getText().toString());
                isAddByForm=true;
                basicDataModels.add(medInsuDirListBean);
            }
        }

        continueCommit(isAddByForm);
    }

    //基础资料填写完善程度
    protected int getBasisDataPerfect(){
        int i=0;
        if (!EmptyUtils.isEmpty(mEtFixmedinsName.getText().toString())){
            i++;
        }
        if (!EmptyUtils.isEmpty(mEtDeptName.getText().toString())){
            i++;
        }
        if (!EmptyUtils.isEmpty(mEtDoctorName.getText().toString())){
            i++;
        }
        if (!EmptyUtils.isEmpty(mEtDrugName.getText().toString())){
            i++;
        }
        if (!EmptyUtils.isEmpty(mEtDrugNum.getText().toString())){
            i++;
        }
        if (!EmptyUtils.isEmpty(mEtStartDate.getText().toString())){
            i++;
        }
        if (!EmptyUtils.isEmpty(mEtEndDate.getText().toString())){
            i++;
        }

        return i;
    }

    //继续提交
    protected void continueCommit(boolean isAddByForm){
        String applicationFormAccessoryId1 = getPicSplicingData(freeBackPicAdapter01);
        String prescriptionAccessoryId1 = getPicSplicingData(freeBackPicAdapter02);
        if (EmptyUtils.isEmpty(prescriptionAccessoryId1)) {
            showShortToast("处方为必填项");
            goSpecifyLocation(mLlImglistTag.getTop() + mLlImglist02Tag.getTop());
            if (isAddByForm){
                basicDataModels.remove(basicDataModels.size()-1);
            }
            return;
        }
        String diagnosisAccessoryId1 = getPicSplicingData(freeBackPicAdapter03);
        String caseAccessoryId1 = getPicSplicingData(freeBackPicAdapter04);
        showWaitDialog();
        BaseModel.sendAddSpecialDrugRequest(TAG, mEtPerName.getText().toString(), mEtPerCertno.getText().toString(), mEtPerPhone.getText().toString(),
                applicationFormAccessoryId1, prescriptionAccessoryId1, diagnosisAccessoryId1, caseAccessoryId1, commitmentLetterAccessoryId, basicDataModels,
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                        if (isAddByForm){
                            basicDataModels.remove(basicDataModels.size()-1);
                        }
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast(returnData.getMsg());
                        goFinish();
                    }
                });
    }

    //滑动到指定的位置
    protected void goSpecifyLocation(int top) {
        mScrollView.fling(0);
        mScrollView.smoothScrollTo(0, top);
    }

    //获取图片拼接之后的数据
    protected String getPicSplicingData(SpecialDrugRegPicAdapter freeBackPicAdapter) {
        String accessoryId = "";
        for (FreeBackPicModel datum : freeBackPicAdapter.getData()) {
            if (PIC_NOR == datum.getItemType()) {
                accessoryId += "'" + datum.getId() + "',";
            }
        }
        if (!EmptyUtils.isEmpty(accessoryId)) {
            accessoryId = accessoryId.substring(0, accessoryId.length() - 1);
        }

        return accessoryId;
    }

    //显示tip信息
    public void showTipDialog2(String tip,final CustomerDialogUtils.IDialogListener iDialogListener) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(this, "提示", tip, 2, "继续填写", "直接提交", R.color.txt_color_666,
                R.color.color_4970e0, iDialogListener);
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String mTag, String accessoryId, String url, int width, int height) {
        switch (mTag) {
            case TAG_SELECT_PIC_01://申请表资料
                selectPicSuccess01(accessoryId, url, width, height);
                break;
            case TAG_SELECT_PIC_02://处方资料
                selectPicSuccess02(accessoryId, url, width, height);
                break;
            case TAG_SELECT_PIC_03://诊断资料
                selectPicSuccess03(accessoryId, url, width, height);
                break;
            case TAG_SELECT_PIC_04://病历资料
                selectPicSuccess04(accessoryId, url, width, height);
                break;
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }

    //申请表资料
    private void selectPicSuccess01(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            freeBackPicAdapter01.getData().add(0, new FreeBackPicModel(accessoryId, url, PIC_NOR));
            freeBackPicAdapter01.notifyDataSetChanged();
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //处方资料
    private void selectPicSuccess02(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            freeBackPicAdapter02.getData().add(0, new FreeBackPicModel(accessoryId, url, PIC_NOR));
            freeBackPicAdapter02.notifyDataSetChanged();
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //诊断资料
    private void selectPicSuccess03(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            freeBackPicAdapter03.getData().add(0, new FreeBackPicModel(accessoryId, url, PIC_NOR));
            freeBackPicAdapter03.notifyDataSetChanged();
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //病历资料
    private void selectPicSuccess04(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            freeBackPicAdapter04.getData().add(0, new FreeBackPicModel(accessoryId, url, PIC_NOR));
            freeBackPicAdapter04.notifyDataSetChanged();
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }
    //--------------------------选择图片----------------------------------------------------

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timeWheelUtils != null) {
            timeWheelUtils.dissTimeWheel();
            timeWheelUtils = null;
        }
        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }
        if (mLlBasisDataList != null) {
            mLlBasisDataList.removeAllViews();
            mLlBasisDataList = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, SpecialDrugRegActivity.class);
        context.startActivity(intent);
    }
}
