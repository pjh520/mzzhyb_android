package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.adapter.SpecialDrugRegQueryPicAdapter;
import com.jrdz.zhyb_android.ui.insured.model.SpecialDrugDetailModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-12
 * 描    述：特殊药品备案查询
 * ================================================
 */
public class SpecialDrugRegQueryActivity extends BaseActivity {
    protected TextView mTvPerName;
    protected TextView mTvPerPhone;
    protected TextView mTvPerCertno;
    protected TextView mTvInsuredAdmdvs;
    protected LinearLayout mLlBasisDataList;
    protected CustomeRecyclerView mCrlImglist01;
    protected CustomeRecyclerView mCrlImglist02;
    protected CustomeRecyclerView mCrlImglist03;
    protected CustomeRecyclerView mCrlImglist04;
    protected TextView mTvRegState, mTvRegReason;
    protected ShapeTextView mTvCommit;

    protected SpecialDrugRegQueryPicAdapter specialDrugRegQueryPicAdapter01, specialDrugRegQueryPicAdapter02, specialDrugRegQueryPicAdapter03, specialDrugRegQueryPicAdapter04;
    protected String specialDrugFilingId;
    private SpecialDrugDetailModel.DataBean pagerData;
    private CustomerDialogUtils customerDialogUtils02;

    @Override
    public int getLayoutId() {
        return R.layout.activity_special_drug_reg_query;
    }

    @Override
    public void initView() {
        super.initView();
        mTvPerName = findViewById(R.id.tv_per_name);
        mTvPerPhone = findViewById(R.id.tv_per_phone);
        mTvPerCertno = findViewById(R.id.tv_per_certno);
        mTvInsuredAdmdvs = findViewById(R.id.tv_insured_admdvs);
        mLlBasisDataList = findViewById(R.id.ll_basis_data_list);
        mCrlImglist01 = findViewById(R.id.crl_imglist_01);
        mCrlImglist02 = findViewById(R.id.crl_imglist_02);
        mCrlImglist03 = findViewById(R.id.crl_imglist_03);
        mCrlImglist04 = findViewById(R.id.crl_imglist_04);

        mTvRegState = findViewById(R.id.tv_reg_state);
        mTvRegReason = findViewById(R.id.tv_reg_reason);
        mTvCommit = findViewById(R.id.tv_commit);
    }

    @Override
    public void initData() {
        specialDrugFilingId = getIntent().getStringExtra("specialDrugFilingId");
        super.initData();

        //设置申请表资料图片列表
        mCrlImglist01.setHasFixedSize(true);
        mCrlImglist01.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
        specialDrugRegQueryPicAdapter01 = new SpecialDrugRegQueryPicAdapter();
        mCrlImglist01.setAdapter(specialDrugRegQueryPicAdapter01);
        //设置处方资料图片列表
        mCrlImglist02.setHasFixedSize(true);
        mCrlImglist02.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
        specialDrugRegQueryPicAdapter02 = new SpecialDrugRegQueryPicAdapter();
        mCrlImglist02.setAdapter(specialDrugRegQueryPicAdapter02);
        //设置诊断资料图片列表
        mCrlImglist03.setHasFixedSize(true);
        mCrlImglist03.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
        specialDrugRegQueryPicAdapter03 = new SpecialDrugRegQueryPicAdapter();
        mCrlImglist03.setAdapter(specialDrugRegQueryPicAdapter03);
        //设置病历资料图片列表
        mCrlImglist04.setHasFixedSize(true);
        mCrlImglist04.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
        specialDrugRegQueryPicAdapter04 = new SpecialDrugRegQueryPicAdapter();
        mCrlImglist04.setAdapter(specialDrugRegQueryPicAdapter04);

        showWaitDialog();
        getData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvCommit.setOnClickListener(this);
        setImageClickEvent(specialDrugRegQueryPicAdapter01);
        setImageClickEvent(specialDrugRegQueryPicAdapter02);
        setImageClickEvent(specialDrugRegQueryPicAdapter03);
        setImageClickEvent(specialDrugRegQueryPicAdapter04);
    }

    //获取页面数据
    protected void getData() {
        SpecialDrugDetailModel.sendSpecialDrugDetailRequest(TAG, specialDrugFilingId, new CustomerJsonCallBack<SpecialDrugDetailModel>() {
            @Override
            public void onRequestError(SpecialDrugDetailModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(SpecialDrugDetailModel returnData) {
                hideWaitDialog();
                setPageData(returnData.getData());
            }
        });
    }

    //设置页面数据
    protected void setPageData(SpecialDrugDetailModel.DataBean data) {
        if (data == null) return;
        pagerData=data;
        mTvPerName.setText("备案姓名：" + EmptyUtils.strEmptyToText(data.getName(), "--"));
        mTvPerPhone.setText("联系电话：" + EmptyUtils.strEmptyToText(data.getPhone(), "--"));
        mTvPerCertno.setText("身份证号：" + EmptyUtils.strEmptyToText(data.getId_card_no(), "--"));
        mTvInsuredAdmdvs.setText("参保区划：" + EmptyUtils.strEmptyToText(data.getAdmdvsName(), "--"));
        //添加图片增加按钮数据-申请表资料
        specialDrugRegQueryPicAdapter01.setNewData(data.getApplicationFormAccessoryUrl1());
        //添加图片增加按钮数据-处方资料
        specialDrugRegQueryPicAdapter02.setNewData(data.getPrescriptionAccessoryUrl1());
        //添加图片增加按钮数据-诊断资料
        specialDrugRegQueryPicAdapter03.setNewData(data.getDiagnosisAccessoryUrl1());
        //添加图片增加按钮数据-病历资料
        specialDrugRegQueryPicAdapter04.setNewData(data.getCaseAccessoryUrl1());

        mLlBasisDataList.removeAllViews();
        List<SpecialDrugDetailModel.DataBean.MedInsuDirListBean> med_insu_dir_list = data.getMed_insu_dir_list();
        if (med_insu_dir_list != null) {
            for (int i = 0; i < med_insu_dir_list.size(); i++) {
                onBasisDataCommit(String.valueOf(i + 1), med_insu_dir_list.get(i));
            }
        }
        setState(data);
    }

    //提交基础资料
    protected void onBasisDataCommit(String pos, SpecialDrugDetailModel.DataBean.MedInsuDirListBean basicDataModel) {
        //添加为基础备案列表的item
        View view = LayoutInflater.from(SpecialDrugRegQueryActivity.this).inflate(R.layout.layout_basis_data_item_02, mLlBasisDataList, false);
        TextView tvNum = view.findViewById(R.id.tv_num);
        TextView tvFixmedinsName = view.findViewById(R.id.tv_fixmedins_name);
        TextView tvDeptName = view.findViewById(R.id.tv_dept_name);
        TextView tvDoctorName = view.findViewById(R.id.tv_doctor_name);
        TextView tvDrugCode = view.findViewById(R.id.tv_drug_code);
        TextView tvDrugName = view.findViewById(R.id.tv_drug_name);
        TextView tvDrugNum = view.findViewById(R.id.tv_drug_num);
        TextView tvBegintime = view.findViewById(R.id.tv_begintime);
        TextView tvEndtime = view.findViewById(R.id.tv_endtime);

        tvNum.setText("药品" + pos);
        tvFixmedinsName.setText("机构名称：" + EmptyUtils.strEmptyToText(basicDataModel.getFixmedins_name(), "--"));
        tvDeptName.setText("科室名称：" + EmptyUtils.strEmptyToText(basicDataModel.getHosp_dept_name(), "--"));
        tvDoctorName.setText("医师名称：" + EmptyUtils.strEmptyToText(basicDataModel.getDr_name(), "--"));
        tvDrugCode.setText("药品代码：" + EmptyUtils.strEmptyToText(basicDataModel.getMIC_Code(), "--"));
        tvDrugName.setText("注册名称：" + EmptyUtils.strEmptyToText(basicDataModel.getItemName(), "--"));
        tvDrugNum.setText("药品数量：" + EmptyUtils.strEmptyToText(basicDataModel.getCnt(), "--")+basicDataModel.getCnt_prcunt());
        tvBegintime.setText("开始时间："+EmptyUtils.strEmptyToText(basicDataModel.getBegintime(), "--"));
        tvEndtime.setText("结束时间："+EmptyUtils.strEmptyToText(basicDataModel.getEndtime(), "--"));

        mLlBasisDataList.addView(view);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_commit://重新备案
                switch (((TextView) v).getText().toString()) {
                    case "撤销":
                        if (pagerData==null)return;
                        onCancle(pagerData.getId_card_no(),pagerData.getSpecialDrugFilingId());
                        break;
                    case "重新备案":
                        againCommit();
                        break;
                }
                break;
        }
    }

    //撤销
    private void onCancle(String id_card_no, String SpecialDrugFilingId) {
        if (customerDialogUtils02 == null) {
            customerDialogUtils02 = new CustomerDialogUtils();
        }
        customerDialogUtils02.showDialog(this, "提示", "是否确认撤销特药备案?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        showWaitDialog();
                        BaseModel.sendRevokeSpecialDrugRequest(TAG, EmptyUtils.strEmpty(id_card_no), SpecialDrugFilingId, new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showTipDialog(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                showShortToast(returnData.getMsg());

                                getDataToBack();
                                goFinish();
                            }
                        });
                    }
                });
    }

    //返回上个页面时 携带更新数据
    private void getDataToBack() {
        Intent intent=new Intent();
        intent.putExtra("specialDrugFilingId", specialDrugFilingId);
        setResult(RESULT_OK,intent);
    }

    //重新备案
    protected void againCommit() {
        Intent intent = new Intent(SpecialDrugRegQueryActivity.this, AgainSpecialDrugRegActivity.class);
        intent.putExtra("specialDrugFilingId", specialDrugFilingId);
        startActivityForResult(intent, new OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == RESULT_OK) {
                    getDataToBack();
                    showWaitDialog();
                    getData();
                }
            }
        });
    }

    //设置审核状态--备案审核状态（1、备案中 2、备案成功 3、备案失败 4.正在审核 5.已撤销）
    protected void setState(SpecialDrugDetailModel.DataBean data) {
        switch (data.getFilingStatus()) {
            case "1"://备案审核中
            case "4"://正在审核
                mTvRegState.setText("备案审核中！");
                mTvRegState.setTextColor(getResources().getColor(R.color.color_4870e0));
                mTvRegReason.setVisibility(View.VISIBLE);
                mTvRegReason.setText("需要1-3个工作日，辛苦等待！");
                mTvCommit.setVisibility(View.VISIBLE);
                mTvCommit.setText("撤销");
                break;
            case "2"://备案成功
                mTvRegState.setText("备案成功！");
                mTvRegState.setTextColor(getResources().getColor(R.color.color_5ec4b1));
                mTvRegReason.setVisibility(View.GONE);
                mTvCommit.setVisibility(View.GONE);
                break;
            case "3"://备案失败
                mTvRegState.setText("备案失败！");
                mTvRegState.setTextColor(getResources().getColor(R.color.color_ff0202));
                mTvRegReason.setVisibility(View.VISIBLE);
                mTvRegReason.setText("失败原因:" + EmptyUtils.strEmptyToText(data.getReviewComments(), "--"));
                mTvCommit.setVisibility(View.VISIBLE);
                mTvCommit.setText("重新备案");
                break;
            case "5"://已撤销
                mTvRegState.setText("撤销成功！");
                mTvRegState.setTextColor(getResources().getColor(R.color.color_5ec4b1));
                mTvRegReason.setVisibility(View.GONE);
                mTvCommit.setVisibility(View.GONE);
                break;
        }
    }

    //设置图片点击事件
    private void setImageClickEvent(SpecialDrugRegQueryPicAdapter specialDrugRegQueryPicAdapter) {
        specialDrugRegQueryPicAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                SpecialDrugDetailModel.DataBean.SpecialDrugPicBean itemData = specialDrugRegQueryPicAdapter.getItem(i);
                //查看图片
                ImageView ivPhoto = view.findViewById(R.id.iv);
                OpenImage.with(SpecialDrugRegQueryActivity.this)
                        //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                        .setClickImageView(ivPhoto)
                        //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                        .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP, true)
                        //RecyclerView的数据
                        .setImageUrl(Constants.BASE_URL + EmptyUtils.strEmpty(itemData.getAccessoryUrl()), MediaType.IMAGE)
                        //点击的ImageView所在数据的位置
                        .setClickPosition(0)
                        //开始展示大图
                        .show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mLlBasisDataList != null) {
            mLlBasisDataList.removeAllViews();
            mLlBasisDataList = null;
        }
        if (customerDialogUtils02 != null) {
            customerDialogUtils02.onclean();
            customerDialogUtils02 = null;
        }

    }

    public static void newIntance(Context context, String specialDrugFilingId) {
        Intent intent = new Intent(context, SpecialDrugRegQueryActivity.class);
        intent.putExtra("specialDrugFilingId", specialDrugFilingId);
        context.startActivity(intent);
    }
}
