package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.viewPage.BaseViewPagerAndTabsAdapter_new;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.insured.fragment.SpecialDrugHandleQueryFragment;
import java.util.Arrays;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-12
 * 描    述：特殊药品备案查询
 * ================================================
 */
public class SpecialDrugRegQueryListActivity extends BaseActivity {
    private TextView mTvDescribe;
    private SlidingTabLayout mStbOrder;
    private ViewPager mVpOrder;

    String[] titles = new String[]{"全部","备案中", "备案成功", "备案失败", "已撤销"};

    @Override
    public int getLayoutId() {
        return R.layout.activity_special_drug_unreg_list;
    }

    @Override
    public void initView() {
        super.initView();
        mTvDescribe=findViewById(R.id.tv_describe);
        mStbOrder = findViewById(R.id.stb_order);
        mVpOrder = findViewById(R.id.vp_order);

        mTvDescribe.setText("注意：点击对应药品进入详情页即可查询备案详情！");
    }

    @Override
    public void initData() {
        super.initData();

        initTabLayout();
    }

    private void initTabLayout() {
        BaseViewPagerAndTabsAdapter_new baseViewPagerAndTabsAdapter_new=new BaseViewPagerAndTabsAdapter_new(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                SpecialDrugHandleQueryFragment specialDrugHandleQueryFragment=null;
                switch (position){
                    case 0://全部
                        specialDrugHandleQueryFragment= SpecialDrugHandleQueryFragment.newIntance("");
                        break;
                    case 1://备案中
                        specialDrugHandleQueryFragment= SpecialDrugHandleQueryFragment.newIntance("1");
                        break;
                    case 2://备案成功
                        specialDrugHandleQueryFragment= SpecialDrugHandleQueryFragment.newIntance("2");
                        break;
                    case 3://备案失败
                        specialDrugHandleQueryFragment= SpecialDrugHandleQueryFragment.newIntance("3");
                        break;
                    case 4://已撤销
                        specialDrugHandleQueryFragment= SpecialDrugHandleQueryFragment.newIntance("5");
                        break;
                }
                return specialDrugHandleQueryFragment;
            }
        };

        baseViewPagerAndTabsAdapter_new.setData(Arrays.asList(titles));
        mVpOrder.setAdapter(baseViewPagerAndTabsAdapter_new);
        mStbOrder.setViewPager(mVpOrder);
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, SpecialDrugRegQueryListActivity.class);
        context.startActivity(intent);
    }
}
