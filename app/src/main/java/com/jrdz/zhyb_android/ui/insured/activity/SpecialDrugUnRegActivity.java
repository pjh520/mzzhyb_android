package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.adapter.SpecialDrugRegQueryPicAdapter;
import com.jrdz.zhyb_android.ui.insured.model.SpecialDrugDetailModel;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-12
 * 描    述：特殊药品撤销备案
 * ================================================
 */
public class SpecialDrugUnRegActivity extends BaseActivity {
    private TextView mTvSerialNumber;
    private TextView mTvPerName;
    private TextView mTvPerPhone;
    private TextView mTvPerCertno;
    private TextView mTvInsuredAdmdvs;
    private TextView mTvFixmedinsCode;
    private TextView mTvFixmedinsName;
    private TextView mTvDoctorCode;
    private TextView mTvDoctorName;
    private TextView mTvDrugName;
    private TextView mTvStartDate;
    private TextView mTvEndDate;
    private CustomeRecyclerView mCrlImglist01;
    private CustomeRecyclerView mCrlImglist02;
    private CustomeRecyclerView mCrlImglist03;
    private CustomeRecyclerView mCrlImglist04;
    private com.hjq.shape.view.ShapeEditText mEtContent;
    private ShapeTextView mTvCommit;

    private SpecialDrugRegQueryPicAdapter specialDrugRegQueryPicAdapter01, specialDrugRegQueryPicAdapter02, specialDrugRegQueryPicAdapter03, specialDrugRegQueryPicAdapter04;
    private String specialDrugFilingId;
    private CustomerDialogUtils customerDialogUtils;
    private SpecialDrugDetailModel.DataBean pagerData;

    @Override
    public int getLayoutId() {
        return R.layout.activity_special_drug_unreg;
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mTitleBar).keyboardEnable(true);
        mImmersionBar.init();
    }

    @Override
    public void initView() {
        super.initView();

        mTvSerialNumber = findViewById(R.id.tv_serial_number);
        mTvPerName = findViewById(R.id.tv_per_name);
        mTvPerPhone = findViewById(R.id.tv_per_phone);
        mTvPerCertno = findViewById(R.id.tv_per_certno);
        mTvInsuredAdmdvs = findViewById(R.id.tv_insured_admdvs);
        mTvFixmedinsCode = findViewById(R.id.tv_fixmedins_code);
        mTvFixmedinsName = findViewById(R.id.tv_fixmedins_name);
        mTvDoctorCode = findViewById(R.id.tv_doctor_code);
        mTvDoctorName = findViewById(R.id.tv_doctor_name);
        mTvDrugName = findViewById(R.id.tv_drug_name);
        mTvStartDate = findViewById(R.id.tv_start_date);
        mTvEndDate = findViewById(R.id.tv_end_date);
        mCrlImglist01 = findViewById(R.id.crl_imglist_01);
        mCrlImglist02 = findViewById(R.id.crl_imglist_02);
        mCrlImglist03 = findViewById(R.id.crl_imglist_03);
        mCrlImglist04 = findViewById(R.id.crl_imglist_04);
        mEtContent = findViewById(R.id.et_content);
        mTvCommit = findViewById(R.id.tv_commit);
    }

    @Override
    public void initData() {
        specialDrugFilingId = getIntent().getStringExtra("specialDrugFilingId");
        super.initData();

        //设置申请表资料图片列表
        mCrlImglist01.setHasFixedSize(true);
        mCrlImglist01.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
        specialDrugRegQueryPicAdapter01 = new SpecialDrugRegQueryPicAdapter();
        mCrlImglist01.setAdapter(specialDrugRegQueryPicAdapter01);
        //设置处方资料图片列表
        mCrlImglist02.setHasFixedSize(true);
        mCrlImglist02.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
        specialDrugRegQueryPicAdapter02 = new SpecialDrugRegQueryPicAdapter();
        mCrlImglist02.setAdapter(specialDrugRegQueryPicAdapter02);
        //设置诊断资料图片列表
        mCrlImglist03.setHasFixedSize(true);
        mCrlImglist03.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
        specialDrugRegQueryPicAdapter03 = new SpecialDrugRegQueryPicAdapter();
        mCrlImglist03.setAdapter(specialDrugRegQueryPicAdapter03);
        //设置病历资料图片列表
        mCrlImglist04.setHasFixedSize(true);
        mCrlImglist04.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
        specialDrugRegQueryPicAdapter04 = new SpecialDrugRegQueryPicAdapter();
        mCrlImglist04.setAdapter(specialDrugRegQueryPicAdapter04);

        showWaitDialog();
        getData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvCommit.setOnClickListener(this);

        setImageClickEvent(specialDrugRegQueryPicAdapter01);
        setImageClickEvent(specialDrugRegQueryPicAdapter02);
        setImageClickEvent(specialDrugRegQueryPicAdapter03);
        setImageClickEvent(specialDrugRegQueryPicAdapter04);
    }

    //获取页面数据
    protected void getData() {
        SpecialDrugDetailModel.sendSpecialDrugDetailRequest(TAG, specialDrugFilingId, new CustomerJsonCallBack<SpecialDrugDetailModel>() {
            @Override
            public void onRequestError(SpecialDrugDetailModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(SpecialDrugDetailModel returnData) {
                hideWaitDialog();
                setPageData(returnData.getData());
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_commit://提交
                onCommit();
                break;
        }
    }

    //设置页面数据
    protected void setPageData(SpecialDrugDetailModel.DataBean data) {
        if (data == null) return;
        pagerData = data;
        mTvSerialNumber.setText("待遇申报明细流水号：" + EmptyUtils.strEmptyToText("", "--"));
        mTvPerName.setText("备案姓名：" + EmptyUtils.strEmptyToText(data.getName(), "--"));
        mTvPerPhone.setText("联系电话：" + EmptyUtils.strEmptyToText(data.getPhone(), "--"));
        mTvPerCertno.setText("身份证号：" + EmptyUtils.strEmptyToText(data.getId_card_no(), "--"));
        mTvInsuredAdmdvs.setText("参保区划：" + EmptyUtils.strEmptyToText(data.getAdmdvsName(), "--"));
        mTvFixmedinsCode.setText("机构编码：" + EmptyUtils.strEmptyToText(data.getFixmedins_code(), "--"));
        mTvFixmedinsName.setText("机构名称：" + EmptyUtils.strEmptyToText(data.getFixmedins_name(), "--"));
        mTvDoctorCode.setText("医师编码：" + EmptyUtils.strEmptyToText(data.getDr_code(), "--"));
        mTvDoctorName.setText("医师名称：" + EmptyUtils.strEmptyToText(data.getDr_name(), "--"));
        mTvDrugName.setText("药品名称：" + EmptyUtils.strEmptyToText(data.getItemName(), "--"));
//        mTvStartDate.setText("开始时间：" + EmptyUtils.strEmptyToText(data.getBegintime(), "--"));
//        mTvEndDate.setText("结束时间：" + EmptyUtils.strEmptyToText(data.getEndtime(), "--"));
        //添加图片增加按钮数据-申请表资料
        specialDrugRegQueryPicAdapter01.setNewData(data.getApplicationFormAccessoryUrl1());
        //添加图片增加按钮数据-处方资料
        specialDrugRegQueryPicAdapter02.setNewData(data.getPrescriptionAccessoryUrl1());
        //添加图片增加按钮数据-诊断资料
        specialDrugRegQueryPicAdapter03.setNewData(data.getDiagnosisAccessoryUrl1());
        //添加图片增加按钮数据-病历资料
        specialDrugRegQueryPicAdapter04.setNewData(data.getCaseAccessoryUrl1());
    }

    //设置图片点击事件
    private void setImageClickEvent(SpecialDrugRegQueryPicAdapter specialDrugRegQueryPicAdapter) {
        specialDrugRegQueryPicAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                SpecialDrugDetailModel.DataBean.SpecialDrugPicBean itemData = specialDrugRegQueryPicAdapter.getItem(i);
                //查看图片
                ImageView ivPhoto = view.findViewById(R.id.iv);
                OpenImage.with(SpecialDrugUnRegActivity.this)
                        //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                        .setClickImageView(ivPhoto)
                        //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                        .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP, true)
                        //RecyclerView的数据
                        .setImageUrl(Constants.BASE_URL + EmptyUtils.strEmpty(itemData.getAccessoryUrl()), MediaType.IMAGE)
                        //点击的ImageView所在数据的位置
                        .setClickPosition(0)
                        //开始展示大图
                        .show();

            }
        });
    }

    //提交
    private void onCommit() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(SpecialDrugUnRegActivity.this, "提示", "是否确认撤销特药备案?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        showWaitDialog();
                        BaseModel.sendRevokeSpecialDrugRequest(TAG, EmptyUtils.strEmpty(pagerData.getId_card_no()),
                                EmptyUtils.strEmpty(mEtContent.getText().toString()),new CustomerJsonCallBack<BaseModel>() {
                                    @Override
                                    public void onRequestError(BaseModel returnData, String msg) {
                                        hideWaitDialog();
                                        showShortToast(msg);
                                    }

                                    @Override
                                    public void onRequestSuccess(BaseModel returnData) {
                                        hideWaitDialog();
                                        showShortToast(returnData.getMsg());
                                        setResult(RESULT_OK);
                                        goFinish();
                                    }
                                });
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
        }
    }

    public static void newIntance(Context context, String specialDrugFilingId) {
        Intent intent = new Intent(context, SpecialDrugUnRegActivity.class);
        intent.putExtra("specialDrugFilingId", specialDrugFilingId);
        context.startActivity(intent);
    }
}
