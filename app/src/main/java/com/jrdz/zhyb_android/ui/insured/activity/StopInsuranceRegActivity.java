package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.ui.insured.adapter.InsuranceTransferListAdapter;
import com.jrdz.zhyb_android.utils.InsuredLoginSmrzUtils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-07-09
 * 描    述：居民停保申请
 * ================================================
 */
public class StopInsuranceRegActivity extends InsuranceTransferListActivity{
    @Override
    protected void setPagerData() {
        String[] datas={"居民停保申请","居民停保记录查询"};
        mAdapter.setNewData(new ArrayList(Arrays.asList(datas)));
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        String itemData = ((InsuranceTransferListAdapter) adapter).getItem(position);
        if (insuredLoginSmrzUtils == null) {
            insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
        }

        if (insuredLoginSmrzUtils.isLoginSmrz(StopInsuranceRegActivity.this)) {
            switch (itemData){
                case "居民停保申请":
                    goH5Pager("居民停保申请", Constants.H5URL.H5_RESIDENTSUSPENDINSURANCE_URL);
                    break;
                case "居民停保记录查询":
                    showShortToast("暂未开放");
                    break;
            }
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, StopInsuranceRegActivity.class);
        context.startActivity(intent);
    }
}
