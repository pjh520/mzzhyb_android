package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.widget.EditText;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.MD5Util;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-06-13
 * 描    述：修改交易密码
 * ================================================
 */
public class UpdateEnterprisePwdActivity extends SetEnterprisePwdActivity {
    private EditText mEtOldPwd;

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_enterprise_pwd;
    }

    @Override
    public void initView() {
        super.initView();
        mEtOldPwd = findViewById(R.id.et_old_pwd);
    }

    //确认
    @Override
    protected void regist() {
        if (EmptyUtils.isEmpty(mEtCertNo.getText().toString())) {
            showShortToast("请输入您的证件号码");
            return;
        }
        if (mEtCertNo.getText().length() != 15 && mEtCertNo.getText().length() != 18) {
            showShortToast("身份证号码为15位或者18位");
            return;
        }
        if (EmptyUtils.isEmpty(mEtName.getText().toString())) {
            showShortToast("请输入您的姓名");
            return;
        }

        if (EmptyUtils.isEmpty(mEtPhone.getText().toString().trim()) || mEtPhone.getText().toString().length() < 11) {
            showShortToast("手机号码格式不正确!");
            return;
        }

        if (EmptyUtils.isEmpty(mEtVerifCode.getText().toString().trim())) {
            showShortToast("请输入验证码!");
            return;
        }
        if (EmptyUtils.isEmpty(mEtOldPwd.getText().toString().trim())) {
            showShortToast("请输入旧密码!");
            return;
        }
        if (mEtPwd.getText().toString().length() < 6) {
            showShortToast("密码为6位数字!");
            return;
        }

        if (!mEtPwd.getText().toString().trim().equals(mEtAgainPwd.getText().toString().trim())) {
            showShortToast("两次密码输入不一样，请重新输入");
            return;
        }

        showWaitDialog();
        BaseModel.sendUpdateEnterprisePwdRequest(TAG, mEtCertNo.getText().toString(), mEtPhone.getText().toString(), mEtVerifCode.getText().toString(),
                MD5Util.up32(mEtPwd.getText().toString() + Constants.Configure.ENTERPRISE_PWD_TAG),
                MD5Util.up32(mEtOldPwd.getText().toString() + Constants.Configure.ENTERPRISE_PWD_TAG), new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showTipDialog(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("企业基金交易密码修改成功！");
                        finish();
                    }
                });
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, UpdateEnterprisePwdActivity.class);
        context.startActivity(intent);
    }
}
