package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;

import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-08-11
 * 描    述： 修改
 * ================================================
 */
public class UpdateNameActivity extends BaseActivity {
    private EditText mEtName;
    private ShapeTextView mTvUpdate;

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_name;
    }

    @Override
    public void initView() {
        super.initView();
        mEtName = findViewById(R.id.et_name);
        mTvUpdate = findViewById(R.id.tv_update);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvUpdate.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()){
            case R.id.tv_update://更新姓名
                if (EmptyUtils.isEmpty(mEtName.getText().toString())) {
                    showShortToast("请输入真实姓名");
                    return;
                }
                showWaitDialog();
                BaseModel.sendUpdatePersonInfoRequest(TAG, mEtName.getText().toString().trim(), InsuredLoginUtils.getIdCardNo(), new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("姓名修改成功");

                        InsuredLoginUtils.setName(mEtName.getText().toString().trim());
                        MsgBus.updateInsuredInfo().post("3");
                        finish();
                    }
                });
                break;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, UpdateNameActivity.class);
        context.startActivity(intent);
    }
}
