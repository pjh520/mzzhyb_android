package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.LinearLayout;

import com.frame.compiler.utils.IsInstallUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.utils.H5RequestUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginSmrzUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-30
 * 描    述：我要办
 * ================================================
 */
public class WantDoActivity extends BaseActivity {
    private LinearLayout mLlEdicalInsurancePayment;
    private LinearLayout mLlInsuranceTransfer;
    private LinearLayout mLlSpecialDrugHandle;
    private LinearLayout mLlInsuranceReg;
    private LinearLayout mLlStopInsuranceRegister;

    public String urlText01 = "alipays://platformapi/startapp?appId=2021001123625885&page=pages%2findex%2findex%3fprovideId%3d2088241201533517%26chInfo%3dXSyibaochaxunqianzhi%26";
    public String urlText02 = "https://ds.alipay.com/?scheme=alipays://platformapi/startapp?appId=2021001123625885&page=pages%2findex%2findex%3fprovideId%3d2088241201533517%26chInfo%3dXSyibaochaxunqianzhi%26returnUrl%3dhttps%253a%252f%252fwww.alipay.com";
    private InsuredLoginSmrzUtils insuredLoginSmrzUtils;
    private H5RequestUtils h5RequestUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_want_do;
    }

    @Override
    public void initView() {
        super.initView();
        mLlEdicalInsurancePayment = findViewById(R.id.ll_edical_insurance_payment);
        mLlInsuranceTransfer = findViewById(R.id.ll_insurance_transfer);
        mLlSpecialDrugHandle = findViewById(R.id.ll_special_drug_handle);
        mLlInsuranceReg = findViewById(R.id.ll_insurance_reg);
        mLlStopInsuranceRegister = findViewById(R.id.ll_stop_insurance_register);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mLlEdicalInsurancePayment.setOnClickListener(this);
        mLlInsuranceTransfer.setOnClickListener(this);
        mLlSpecialDrugHandle.setOnClickListener(this);
        mLlInsuranceReg.setOnClickListener(this);
        mLlStopInsuranceRegister.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        if (insuredLoginSmrzUtils == null) {
            insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
        }

        if (insuredLoginSmrzUtils.isLoginSmrz(WantDoActivity.this)) {
            switch (v.getId()) {
                case R.id.ll_edical_insurance_payment://城乡居民医保缴费
                    goZFBInsurancePayment();
                    break;
                case R.id.ll_insurance_transfer://医保转移
                    InsuranceTransferListActivity.newIntance(WantDoActivity.this);
                    break;
                case R.id.ll_special_drug_handle://特殊药品申请备案
                    SpecialDrugHandleActivity.newIntance(WantDoActivity.this);
                    break;
                case R.id.ll_insurance_reg://城乡居民参保登记
                    goH5Pager("城乡居民参保登记",Constants.H5URL.H5_RESIDENTINSUREGISTRATION_URL);
                    break;
                case R.id.ll_stop_insurance_register://城乡居民停保申请
                    StopInsuranceRegActivity.newIntance(WantDoActivity.this);
                    break;
            }
        }
    }

    //跳转H5链接
    private void goH5Pager(String title, String h5Url) {
        if (h5RequestUtils==null){
            h5RequestUtils=new H5RequestUtils();
        }
        h5RequestUtils.goPager(WantDoActivity.this, TAG, title, h5Url, new H5RequestUtils.IRequestListener() {
            @Override
            public void onShowWaitDialog() {
                showWaitDialog();
            }

            @Override
            public void onHideWaitDialog() {
                hideWaitDialog();
            }

            @Override
            public void onFail(String msg) {
                showTipDialog(msg);
            }
        });
    }

    //城乡居民医保缴费--跳转支付宝
    private void goZFBInsurancePayment() {
        if (IsInstallUtils.getInstance().checkAliPayInstalled(WantDoActivity.this)) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.WebUrl.ALIPAYS_SXSBJF_URL));
            startActivity(intent);
        } else {
            showShortToast("请安装支付宝软件");

            //如果是pos机 跳转pos机自带的应用市场
            if (Constants.Configure.IS_POS) {
                String packageName = "com.centerm.cpay.applicationshop";
                String className = "com.centerm.cpay.applicationshop.activity.AppDetailActivity";
                Intent intent = new Intent();
                //这里改成你的应用包名
                intent.putExtra("packageName", "com.eg.android.AlipayGphone");
                ComponentName componentName = new ComponentName(packageName, className);
                intent.setComponent(componentName);
                startActivity(intent);
            } else {
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Uri url = Uri.parse(urlText02);
                intent.setData(url);
                startActivity(intent);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (insuredLoginSmrzUtils != null) {
            insuredLoginSmrzUtils.cleanData();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, WantDoActivity.class);
        context.startActivity(intent);
    }
}
