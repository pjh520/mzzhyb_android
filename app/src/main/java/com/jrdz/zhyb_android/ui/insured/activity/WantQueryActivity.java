package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.LinearLayout;

import com.frame.compiler.utils.IsInstallUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.ui.catalogue.activity.DiseCataActivity;
import com.jrdz.zhyb_android.utils.H5RequestUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginSmrzUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-30
 * 描    述：我要查
 * ================================================
 */
public class WantQueryActivity extends BaseActivity {
    private LinearLayout mLlInsuranceDetailsQuery,mLlFixmedinsQuery,mLlAgencyQuery,mLlDiseaseCategoryDirectoryQuery,mLlChronicDiseasesFixmedinsQuery,
            mLlWorkersFixmedinsQuery,mLlOutprovinceadmdvsQuery,mLlOutprovinceorgQuery;
    private InsuredLoginSmrzUtils insuredLoginSmrzUtils;
    private H5RequestUtils h5RequestUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_want_query;
    }

    @Override
    public void initView() {
        super.initView();
        mLlInsuranceDetailsQuery = findViewById(R.id.ll_insurance_details_query);
        mLlFixmedinsQuery = findViewById(R.id.ll_fixmedins_query);
        mLlAgencyQuery = findViewById(R.id.ll_agency_query);
        mLlDiseaseCategoryDirectoryQuery= findViewById(R.id.ll_disease_category_directory_query);
        mLlChronicDiseasesFixmedinsQuery = findViewById(R.id.ll_chronic_diseases_fixmedins_query);
        mLlWorkersFixmedinsQuery= findViewById(R.id.ll_workers_fixmedins_query);
        mLlOutprovinceadmdvsQuery = findViewById(R.id.ll_outprovinceadmdvs_query);
        mLlOutprovinceorgQuery = findViewById(R.id.ll_outprovinceorg_query);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mLlInsuranceDetailsQuery.setOnClickListener(this);
        mLlFixmedinsQuery.setOnClickListener(this);
        mLlAgencyQuery.setOnClickListener(this);
        mLlDiseaseCategoryDirectoryQuery.setOnClickListener(this);
        mLlChronicDiseasesFixmedinsQuery.setOnClickListener(this);
        mLlWorkersFixmedinsQuery.setOnClickListener(this);
        mLlOutprovinceadmdvsQuery.setOnClickListener(this);
        mLlOutprovinceorgQuery.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.ll_insurance_details_query://医保明细查询
                showShortToast("暂未开放");
                break;
            case R.id.ll_fixmedins_query://定点医疗机构查询
                MechanisQueryActitiy.newIntance(WantQueryActivity.this);
                break;
            case R.id.ll_agency_query://经办机构查询
                AgencyQueryActivity.newIntance(WantQueryActivity.this);
                break;
            case R.id.ll_disease_category_directory_query://病种目录查询
                DiseCataActivity.newIntance(WantQueryActivity.this, "1");
                break;
            case R.id.ll_chronic_diseases_fixmedins_query://门慢药店查询
                SlowCarePharmacyActitiy.newIntance(WantQueryActivity.this);
                break;
            case R.id.ll_workers_fixmedins_query://职工门诊统筹定点
                EmployeeClinicActivity.newIntance(WantQueryActivity.this);
                break;
            case R.id.ll_outprovinceadmdvs_query://跨省就医统筹区开通信息查询
                goH5Pager("跨省就医统筹区开通信息查询",Constants.H5URL.H5_OUTPROVINCEADMDVSQUERY_URL);
                break;
            case R.id.ll_outprovinceorg_query://跨省异地就医定点医疗机构查询
                goH5Pager("跨省异地就医定点医疗机构查询",Constants.H5URL.H5_OUTPROVINCEORGQUERY_URL);
                break;
        }
    }

    //跳转H5链接
    protected void goH5Pager(String title, String h5Url) {
        if (insuredLoginSmrzUtils == null) {
            insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
        }

        if (insuredLoginSmrzUtils.isLoginSmrz(WantQueryActivity.this)) {
            if (h5RequestUtils==null){
                h5RequestUtils=new H5RequestUtils();
            }
            h5RequestUtils.goPager(WantQueryActivity.this, TAG, title, h5Url, new H5RequestUtils.IRequestListener() {
                @Override
                public void onShowWaitDialog() {
                    showWaitDialog();
                }

                @Override
                public void onHideWaitDialog() {
                    hideWaitDialog();
                }

                @Override
                public void onFail(String msg) {
                    showTipDialog(msg);
                }
            });
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (insuredLoginSmrzUtils != null) {
            insuredLoginSmrzUtils.cleanData();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, WantQueryActivity.class);
        context.startActivity(intent);
    }
}
