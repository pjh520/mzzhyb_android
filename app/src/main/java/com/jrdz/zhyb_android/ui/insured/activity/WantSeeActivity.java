package com.jrdz.zhyb_android.ui.insured.activity;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.viewPage.BaseViewPagerAndTabsAdapter_new;
import com.frame.compiler.widget.CustomViewPager;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.insured.fragment.ComplainSuggestFragment;
import com.jrdz.zhyb_android.ui.insured.fragment.InsuredNewsFragment;
import com.jrdz.zhyb_android.ui.insured.fragment.NoticeFragment;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.OrderListActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.MsgListActivity_user;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.ElectPrescFragment_image;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.ElectPrescFragment_video;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ElectPrescModel2;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-30
 * 描    述：我要看页面
 * ================================================
 */
public class WantSeeActivity extends BaseActivity {
    private SlidingTabLayout mStbTab;
    private CustomViewPager mVp;

    String[] titles = new String[]{"政策宣传", "最新通知", "投诉建议"};
    private int currentTab;

    @Override
    public int getLayoutId() {
        return R.layout.activity_want_see;
    }

    @Override
    public void initView() {
        super.initView();

        mStbTab = findViewById(R.id.stb_tab);
        mVp = findViewById(R.id.vp);
    }

    @Override
    public void initData() {
        currentTab=getIntent().getIntExtra("currentTab",0);
        super.initData();

        initTabLayout();
    }

    //初始化tablayout 跟 viewpager
    protected void initTabLayout() {
        BaseViewPagerAndTabsAdapter_new baseViewPagerAndTabsAdapter_new = new BaseViewPagerAndTabsAdapter_new(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0://政策宣传
                        return InsuredNewsFragment.newIntance();
                    case 1://最新通知
                        return NoticeFragment.newIntance();
                    case 2://投诉建议
                        return ComplainSuggestFragment.newIntance();
                    default:
                        return InsuredNewsFragment.newIntance();
                }
            }
        };

        baseViewPagerAndTabsAdapter_new.setData(Arrays.asList(titles));
        mVp.setAdapter(baseViewPagerAndTabsAdapter_new);
        mStbTab.setViewPager(mVp);

        mStbTab.setCurrentTab(currentTab);
    }


    public static void newIntance(Context context, int currentTab) {
        Intent intent = new Intent(context, WantSeeActivity.class);
        intent.putExtra("currentTab",currentTab);
        context.startActivity(intent);
    }
}
