package com.jrdz.zhyb_android.ui.insured.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.insured.model.AgencyQueryModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-02-17
 * 描    述：
 * ================================================
 */
public class AgencyQueryAdapter extends BaseQuickAdapter<AgencyQueryModel.DataBean, BaseViewHolder> {
    public AgencyQueryAdapter() {
        super(R.layout.layout_agency_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, AgencyQueryModel.DataBean item) {
        baseViewHolder.setText(R.id.tv_agency_name, EmptyUtils.strEmpty(item.getAgencyName()));
        baseViewHolder.setText(R.id.tv_agency_phone, "联系电话:"+item.getTelephone());
        baseViewHolder.setText(R.id.tv_agency_address, "机构地址:"+item.getAddress());
    }
}
