package com.jrdz.zhyb_android.ui.insured.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.widget.glide.GlideUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.insured.model.CrlBtnModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：SuperWarehouse_Android
 * 包    名：com.tjl.super_warehouse.utils.shared_dailog
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2019/11/3 0003
 * 描    述：
 * ================================================
 */
public class CrlBtnAdapter extends BaseQuickAdapter<CrlBtnModel, BaseViewHolder> {
    public CrlBtnAdapter(@Nullable List<CrlBtnModel> data) {
        super(R.layout.layout_crlbtn_item, data);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, CrlBtnModel item) {
        ImageView iv=helper.getView(R.id.iv);

        GlideUtils.loadImg(item.getImgRes(),iv);
        helper.setText(R.id.tv,item.getTitle());
    }
}
