package com.jrdz.zhyb_android.ui.insured.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.DecimalFormatUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.insured.model.MechanisQueryModel;
import com.jrdz.zhyb_android.ui.insured.model.SlowCarePharmacyModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-05-11
 * 描    述：
 * ================================================
 */
public class EmployeeMedicalAdapter extends BaseQuickAdapter<SlowCarePharmacyModel.DataBean, BaseViewHolder> {
    public EmployeeMedicalAdapter() {
        super(R.layout.layout_employee_medical_item, null);
    }


    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, SlowCarePharmacyModel.DataBean dataBean) {
        baseViewHolder.setText(R.id.tv_fixmedins_name, EmptyUtils.strEmptyToText(dataBean.getFixmedins_name(), "--"));
        baseViewHolder.setText(R.id.tv_fixmedins_address, "机构地址："+EmptyUtils.strEmptyToText(dataBean.getAddress(), "--"));
        baseViewHolder.setText(R.id.tv_zoning, "统筹区划："+EmptyUtils.strEmptyToText(dataBean.getAdmdvsName(), "--"));
        baseViewHolder.setText(R.id.tv_distance, (dataBean.getDistance()>1000?(DecimalFormatUtils.keepPlaces("#.0",dataBean.getDistance()/1000)+"km"):(dataBean.getDistance())+"m"));

    }
}
