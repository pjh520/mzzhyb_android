package com.jrdz.zhyb_android.ui.insured.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.insured.model.FixmedinsListModel;
import com.jrdz.zhyb_android.ui.insured.model.SpecialDrugListModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-15
 * 描    述：机构信息
 * ================================================
 */
public class FixmedinsListAdapter extends BaseQuickAdapter<FixmedinsListModel.DataBean, BaseViewHolder> {
    public FixmedinsListAdapter() {
        super(R.layout.layout_fixmedins_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, FixmedinsListModel.DataBean itemData) {
        baseViewHolder.setText(R.id.tv_fixmedins_code, EmptyUtils.strEmptyToText(itemData.getFixmedins_code(),"--"));
        baseViewHolder.setText(R.id.tv_fixmedins_name, EmptyUtils.strEmptyToText(itemData.getFixmedins_name(),"--"));
        baseViewHolder.setText(R.id.tv_hosp_dept_name, EmptyUtils.strEmptyToText(itemData.getHosp_dept_name(),"--"));
        baseViewHolder.setText(R.id.tv_dr_code, EmptyUtils.strEmptyToText(itemData.getDr_code(),"--"));
        baseViewHolder.setText(R.id.tv_dr_name, EmptyUtils.strEmptyToText(itemData.getDr_name(),"--"));
        baseViewHolder.setText(R.id.tv_professional_title, EmptyUtils.strEmptyToText(itemData.getProfessionalTitle(),"--"));
    }
}
