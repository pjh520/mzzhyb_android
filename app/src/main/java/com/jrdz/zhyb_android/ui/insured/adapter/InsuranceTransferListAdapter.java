package com.jrdz.zhyb_android.ui.insured.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-07-09
 * 描    述：
 * ================================================
 */
public class InsuranceTransferListAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    public InsuranceTransferListAdapter() {
        super(R.layout.layout_insurance_transfer_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, String s) {
        baseViewHolder.setText(R.id.tv_text, EmptyUtils.strEmpty(s));
    }
}
