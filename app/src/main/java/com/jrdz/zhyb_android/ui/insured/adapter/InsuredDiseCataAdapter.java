package com.jrdz.zhyb_android.ui.insured.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.catalogue.model.DiseCataModel;
import com.jrdz.zhyb_android.ui.insured.model.InsuredDiseCataModel;


/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.catalogue.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/23
 * 描    述：
 * ================================================
 */
public class InsuredDiseCataAdapter extends BaseQuickAdapter<InsuredDiseCataModel.DataBean, BaseViewHolder> {
    public InsuredDiseCataAdapter() {
        super(R.layout.layout_insured_disecata_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, InsuredDiseCataModel.DataBean diseCataModel) {
        baseViewHolder.setText(R.id.tv_disecata_id, EmptyUtils.strEmptyToText(diseCataModel.getWes_med_dis_diag_id(),"--"));
        baseViewHolder.setText(R.id.tv_disecata_codes, EmptyUtils.strEmptyToText(diseCataModel.getCode(),"--"));
        baseViewHolder.setText(R.id.tv_disecata_name, EmptyUtils.strEmptyToText(diseCataModel.getName(),"--"));
        baseViewHolder.setText(R.id.tv_disecata_unicode, EmptyUtils.strEmptyToText(diseCataModel.getField20(),"--"));
    }
}
