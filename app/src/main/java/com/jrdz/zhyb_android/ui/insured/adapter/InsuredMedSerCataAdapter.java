package com.jrdz.zhyb_android.ui.insured.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.catalogue.model.MedSerCataListModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：
 * ================================================
 */
public class InsuredMedSerCataAdapter extends BaseQuickAdapter<MedSerCataListModel.DataBean, BaseViewHolder> {
    public InsuredMedSerCataAdapter() {
        super(R.layout.layout_insured_medsercata_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, MedSerCataListModel.DataBean resultObjBean) {
        baseViewHolder.setText(R.id.tv_title, "名称："+resultObjBean.getMedicalServiceItemsName());
        baseViewHolder.setText(R.id.tv_med_list_codg, "机构编码："+resultObjBean.getMed_list_codg());
        baseViewHolder.setText(R.id.tv_contain, "诊疗项目内涵："+ EmptyUtils.strEmptyToText(resultObjBean.getField6(),"--"));
        baseViewHolder.setText(R.id.tv_describe, "诊疗项目说明："+EmptyUtils.strEmptyToText(resultObjBean.getField4(),"--"));
    }
}
