package com.jrdz.zhyb_android.ui.insured.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.catalogue.model.MedSupCataListModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：
 * ================================================
 */
public class InsuredMedSupCataAdapter extends BaseQuickAdapter<MedSupCataListModel.DataBean, BaseViewHolder> {
    public InsuredMedSupCataAdapter() {
        super(R.layout.layout_insured_medsubcata_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, MedSupCataListModel.DataBean resultObjBean) {
        baseViewHolder.setText(R.id.tv_title, "名称："+resultObjBean.getHi_genname());
        baseViewHolder.setText(R.id.tv_med_list_codg, "机构编码："+resultObjBean.getMed_list_codg());
        baseViewHolder.setText(R.id.tv_spec, "规\u3000\u3000格："+resultObjBean.getSpec());
        baseViewHolder.setText(R.id.tv_prodentp_name, "生产企业："+resultObjBean.getProdentp_name());
        baseViewHolder.setText(R.id.tv_reg_fil_no, "备"+mContext.getResources().getString(R.string.spaces_half)+
                "案"+mContext.getResources().getString(R.string.spaces_half)+"号："+resultObjBean.getReg_fil_no());
    }
}
