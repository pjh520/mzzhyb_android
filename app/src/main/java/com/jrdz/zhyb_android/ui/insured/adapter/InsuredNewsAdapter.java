package com.jrdz.zhyb_android.ui.insured.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.insured.model.InsuredNewsModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-07-15
 * 描    述：
 * ================================================
 */
public class InsuredNewsAdapter extends BaseQuickAdapter<InsuredNewsModel.DataBean, BaseViewHolder> {
    private String mTag;

    public InsuredNewsAdapter(String tag) {
        super(R.layout.layout_insured_news_item, null);
        this.mTag=tag;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, InsuredNewsModel.DataBean item) {
        ImageView iv=baseViewHolder.getView(R.id.iv);
        int defaultPic=R.drawable.ic_dmmz_news_default;
        switch (mTag){
            case "1"://大美米脂
                defaultPic=R.drawable.ic_dmmz_news_default;
                break;
            case "2"://政策宣传
                defaultPic=R.drawable.ic_zcxc_news_default;
                break;
            case "3"://最新通知
                defaultPic=R.drawable.ic_zxtz_news_default;
                break;
        }
        GlideUtils.loadImg(EmptyUtils.strEmpty(item.getNewsUrl()), iv, defaultPic,
                new RoundedCornersTransformation(mContext.getResources().getDimensionPixelSize(R.dimen.dp_16), 0));

        baseViewHolder.setText(R.id.tv_title, EmptyUtils.strEmpty(item.getTitle()));
        baseViewHolder.setText(R.id.tv_time, EmptyUtils.strEmpty(item.getCreateDT()));
    }
}
