package com.jrdz.zhyb_android.ui.insured.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.catalogue.model.PhaPreCataListModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：
 * ================================================
 */
public class InsuredPhaPreCataAdapter extends BaseQuickAdapter<PhaPreCataListModel.DataBean, BaseViewHolder> {
    public InsuredPhaPreCataAdapter() {
        super(R.layout.layout_insured_phaprecata_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, PhaPreCataListModel.DataBean resultObjBean) {
        baseViewHolder.setText(R.id.tv_title, "药品名称："+resultObjBean.getDrug_prodname());
        baseViewHolder.setText(R.id.tv_med_list_codg, "机构编码："+resultObjBean.getMed_list_codg());
        baseViewHolder.setText(R.id.tv_medicinal_parts, "剂\u3000\u3000型："+resultObjBean.getDosform());
    }
}
