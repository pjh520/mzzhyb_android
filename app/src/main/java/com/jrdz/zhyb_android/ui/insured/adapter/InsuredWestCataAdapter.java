package com.jrdz.zhyb_android.ui.insured.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.catalogue.model.WestCataListModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：
 * ================================================
 */
public class InsuredWestCataAdapter extends BaseQuickAdapter<WestCataListModel.DataBean, BaseViewHolder> {
    public InsuredWestCataAdapter() {
        super(R.layout.layout_insured_catalogue_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, WestCataListModel.DataBean resultObjBean) {
        baseViewHolder.setText(R.id.tv_reg_nam,"药品名称："+resultObjBean.getReg_nam());
        baseViewHolder.setText(R.id.tv_med_list_codg,"机构编码："+resultObjBean.getMed_list_codg());
        baseViewHolder.setText(R.id.tv_drug_spec_code,"注册剂型："+resultObjBean.getReg_dosform());
        baseViewHolder.setText(R.id.tv_drug_type_name,"药品规格："+resultObjBean.getReg_spec());
        baseViewHolder.setText(R.id.tv_prodentp_name,"生产企业："+resultObjBean.getEnterpriseName());
        baseViewHolder.setText(R.id.tv_aprvno_begndate,"批准文号："+resultObjBean.getApprovalNumber());
    }
}
