package com.jrdz.zhyb_android.ui.insured.adapter;

import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.insured.model.MechanisQueryModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-02-16
 * 描    述：
 * ================================================
 */
public class MechanisQueryAdapter extends BaseQuickAdapter<MechanisQueryModel.DataBean, BaseViewHolder> {
    public MechanisQueryAdapter() {
        super(R.layout.layout_mechanis_query_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder = super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.tv_fixmedins_map);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, MechanisQueryModel.DataBean dataBean) {
        LinearLayout llFixmedinsOpen=baseViewHolder.getView(R.id.ll_fixmedins_open);
        TextView tvFixmedinsOpen=baseViewHolder.getView(R.id.tv_fixmedins_open);
        TextView tvFixmedinsDistance=baseViewHolder.getView(R.id.tv_fixmedins_distance);
        View line02=baseViewHolder.getView(R.id.line_02);

        baseViewHolder.setText(R.id.tv_fixmedins_code, "机构编码:"+dataBean.getFixmedins_code());
        baseViewHolder.setText(R.id.tv_fixmedins_grade, EmptyUtils.strEmpty(dataBean.getHospLvText()));
        baseViewHolder.setText(R.id.tv_fixmedins_name, EmptyUtils.strEmpty(dataBean.getFixmedins_name()));
        baseViewHolder.setText(R.id.tv_fixmedins_phone, "联系电话:  "+dataBean.getFixmedins_phone());
        baseViewHolder.setText(R.id.tv_fixmedins_address, "机构地址:  "+dataBean.getAddress());

        if (0==dataBean.getLatitude()&&0==dataBean.getLongitude()){
            tvFixmedinsDistance.setVisibility(View.GONE);
            line02.setVisibility(View.GONE);
        }else {
            tvFixmedinsDistance.setVisibility(View.VISIBLE);
            line02.setVisibility(View.VISIBLE);
        }

        tvFixmedinsDistance.setText((dataBean.getDistance()>1000?(dataBean.getDistance()/1000+"公里"):(dataBean.getDistance())+"米"));


        if ("5".equals(dataBean.getApproveStatus())){
            llFixmedinsOpen.setVisibility(View.VISIBLE);
        }else {
            llFixmedinsOpen.setVisibility(View.GONE);
        }

        tvFixmedinsOpen.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvFixmedinsOpen.getPaint().setAntiAlias(true);
    }
}
