package com.jrdz.zhyb_android.ui.insured.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.insured.model.PersonalInsuInfoQueryModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-07-26
 * 描    述：
 * ================================================
 */
public class PersonalInsuInfoQueryAdapter extends BaseQuickAdapter<PersonalInsuInfoQueryModel.DataBean.InsuinfoBean, BaseViewHolder> {
    public PersonalInsuInfoQueryAdapter() {
        super(R.layout.layout_personal_insuinfo_query_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.ll_parent);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, PersonalInsuInfoQueryModel.DataBean.InsuinfoBean item) {
        ImageView ivDropdown=baseViewHolder.getView(R.id.iv_dropdown);
        LinearLayout llChild=baseViewHolder.getView(R.id.ll_child);

        //获取险种类型
        String insutypeText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("INSUTYPE", item.getInsutype());
        //获取参保就医区划
        String insuplcAdmdvsText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("AREA_ADMVS", item.getInsuplc_admdvs());
        //获取参保状态
        String psnInsuStasText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("PSN_INSU_STAS", item.getPsn_insu_stas());
        //获取人员类别
        String psnTypeText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("PSN_TYPE", item.getPsn_type());


        baseViewHolder.setText(R.id.tv_admdvs,insutypeText);
        baseViewHolder.setText(R.id.tv_insured_statue,psnInsuStasText);
        baseViewHolder.setText(R.id.tv_insured_admdvs,insuplcAdmdvsText);
        baseViewHolder.setText(R.id.tv_insured_unit, EmptyUtils.strEmptyToText(item.getEmp_name(), "--"));
        baseViewHolder.setText(R.id.tv_person_type,psnTypeText);
        baseViewHolder.setText(R.id.tv_insured_statue_02,psnInsuStasText);
        baseViewHolder.setText(R.id.tv_insured_type,insutypeText);

        if (item.isOpen()){
            llChild.setVisibility(View.VISIBLE);

            ivDropdown.setRotation(0);
        }else {
            llChild.setVisibility(View.GONE);

            ivDropdown.setRotation(180);
        }
    }
}
