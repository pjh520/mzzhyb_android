package com.jrdz.zhyb_android.ui.insured.adapter;

import android.widget.BaseAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.insured.model.SpecialDrugListModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-15
 * 描    述：
 * ================================================
 */
public class SpecialDrugListAdapter extends BaseQuickAdapter<SpecialDrugListModel.DataBean, BaseViewHolder> {
    public SpecialDrugListAdapter() {
        super(R.layout.layout_special_drug_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, SpecialDrugListModel.DataBean itemData) {
        baseViewHolder.setText(R.id.tv_item_code, EmptyUtils.strEmptyToText(itemData.getMIC_Code(),"--"));
        baseViewHolder.setText(R.id.tv_item_name, EmptyUtils.strEmptyToText(itemData.getItemName(),"--"));
        baseViewHolder.setText(R.id.tv_medical_insurance_name, EmptyUtils.strEmptyToText(itemData.getMIC_Name(),"--"));
        baseViewHolder.setText(R.id.tv_dosforom, EmptyUtils.strEmptyToText(itemData.getRegDosform(),"--"));
        baseViewHolder.setText(R.id.tv_spec, EmptyUtils.strEmptyToText(itemData.getSpec(),"--"));
        baseViewHolder.setText(R.id.tv_mpq, EmptyUtils.strEmptyToText(itemData.getMpq(),"--"));
        baseViewHolder.setText(R.id.tv_prodentp_name, EmptyUtils.strEmptyToText(itemData.getDrugCompany(),"--"));
        baseViewHolder.setText(R.id.tv_approval_number, EmptyUtils.strEmptyToText(itemData.getApprovalNumber(),"--"));
        baseViewHolder.setText(R.id.tv_remarks, EmptyUtils.strEmptyToText(itemData.getRemark(),"--"));

    }
}
