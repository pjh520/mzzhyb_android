package com.jrdz.zhyb_android.ui.insured.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ImageUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.ui.insured.model.SpecialDrugDetailModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-16
 * 描    述：
 * ================================================
 */
public class SpecialDrugRegQueryPicAdapter extends BaseQuickAdapter<SpecialDrugDetailModel.DataBean.SpecialDrugPicBean, BaseViewHolder> {
    public SpecialDrugRegQueryPicAdapter() {
        super(R.layout.layout_special_drug_reg_query_pic_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, SpecialDrugDetailModel.DataBean.SpecialDrugPicBean itemData) {
        ImageView iv=baseViewHolder.getView(R.id.iv);
        GlideUtils.loadImg(Constants.BASE_URL+EmptyUtils.strEmpty(itemData.getAccessoryUrl()),iv);
    }
}
