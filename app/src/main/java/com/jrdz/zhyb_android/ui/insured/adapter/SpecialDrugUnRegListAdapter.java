package com.jrdz.zhyb_android.ui.insured.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.insured.model.SpecialDrugListModel;
import com.jrdz.zhyb_android.ui.insured.model.SpecialDrugUnRegListModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-15
 * 描    述：
 * ================================================
 */
public class SpecialDrugUnRegListAdapter extends BaseQuickAdapter<SpecialDrugUnRegListModel.DataBean, BaseViewHolder> {
    public SpecialDrugUnRegListAdapter() {
        super(R.layout.layout_special_drug_unreg_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.tv_cancle);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, SpecialDrugUnRegListModel.DataBean itemData) {
        ShapeTextView tvCancle=baseViewHolder.getView(R.id.tv_cancle);

        baseViewHolder.setText(R.id.tv_item_code, EmptyUtils.strEmptyToText(itemData.getMIC_Code(),"--"));
        baseViewHolder.setText(R.id.tv_item_name, EmptyUtils.strEmptyToText(itemData.getItemName(),"--"));
        baseViewHolder.setText(R.id.tv_reg_date, EmptyUtils.strEmptyToText(itemData.getCreateDT(),"--"));

        if ("4".equals(itemData.getFilingStatus())){
            tvCancle.setVisibility(View.GONE);
        }else {
            tvCancle.setVisibility(View.VISIBLE);
        }
    }
}
