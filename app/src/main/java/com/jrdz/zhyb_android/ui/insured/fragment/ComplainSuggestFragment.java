package com.jrdz.zhyb_android.ui.insured.fragment;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.view.ShapeEditText;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.activity.SpecialDrugRegActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.FreeBackPicAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.SpecialDrugRegPicAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.model.FreeBackPicModel;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-07-15
 * 描    述：投诉建议
 * ================================================
 */
public class ComplainSuggestFragment extends BaseFragment implements CompressUploadSinglePicUtils_zhyf.IChoosePic {
    private EditText mEtName;
    private EditText mEtPhone;
    private ShapeEditText mEtContent;
    private CustomeRecyclerView mCrlImglist;
    private ShapeTextView mTvSubmit;

    public final static String TAG_SELECT_PIC_01 = "1";//意见反馈图片
    public static final int PIC_NOR = 1;//新增图片
    public static final int PIC_ADDBTN = 2;//添加按钮
    private CompressUploadSinglePicUtils_zhyf compressUploadSinglePicUtils;
    private FreeBackPicAdapter freeBackPicAdapter;
    private CustomerDialogUtils customerDialogUtils;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_complain_suggest;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mEtName = view.findViewById(R.id.et_name);
        mEtPhone = view.findViewById(R.id.et_phone);
        mEtContent = view.findViewById(R.id.et_content);
        mCrlImglist = view.findViewById(R.id.crl_imglist);
        mTvSubmit = view.findViewById(R.id.tv_submit);
    }

    @Override
    public void initData() {
        super.initData();
        //初始化获取图片
        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(getActivity(), true, this);

        //设置图片列表
        mCrlImglist.setHasFixedSize(true);
        mCrlImglist.setLayoutManager(new GridLayoutManager(getContext(), 4, RecyclerView.VERTICAL, false));
        freeBackPicAdapter = new FreeBackPicAdapter();
        mCrlImglist.setAdapter(freeBackPicAdapter);

        //添加图片增加按钮数据
        ArrayList<FreeBackPicModel> freeBackPicModels = new ArrayList<>();
        FreeBackPicModel freeBackPic_addbtn = new FreeBackPicModel("-1", "", PIC_ADDBTN);
        freeBackPicModels.add(freeBackPic_addbtn);

        freeBackPicAdapter.setNewData(freeBackPicModels);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        //分类 item点击事件
        freeBackPicAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                FreeBackPicModel itemData = freeBackPicAdapter.getItem(i);
                if (PIC_NOR == itemData.getItemType()) {//查看图片
                    ImageView ivPhoto = view.findViewById(R.id.iv_photo);
                    OpenImage.with(ComplainSuggestFragment.this)
                            //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                            .setClickImageView(ivPhoto)
                            //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                            .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                            //RecyclerView的数据
                            .setImageUrl(itemData.getImg(), MediaType.IMAGE)
                            //点击的ImageView所在数据的位置
                            .setClickPosition(0)
                            //开始展示大图
                            .show();
                } else if (PIC_ADDBTN == itemData.getItemType()) {//新增按钮
                    KeyboardUtils.hideSoftInput(getActivity());
                    compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_01, CompressUploadSinglePicUtils_zhyf.PIC_FREEBACK_TAG);
                }
            }
        });

        freeBackPicAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }

                switch (view.getId()) {
                    case R.id.iv_delete://删除图片
                        deletePic(freeBackPicAdapter, i);
                        break;
                }
            }
        });
        mTvSubmit.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_submit://提交
                onSubmit();
                break;
        }
    }

    private void onSubmit() {
        if (!EmptyUtils.isEmpty(mEtPhone.getText().toString().trim())&&mEtPhone.getText().toString().length() < 11) {
            showShortToast("手机号码格式不正确!");
            return;
        }

        if (EmptyUtils.isEmpty(mEtContent.getText().toString())){
            showShortToast("请输入信息反馈");
            return;
        }

        String ComplaintAccessoryId = getPicSplicingData(freeBackPicAdapter);
        showWaitDialog();
        BaseModel.sendAddComplaintRequest(TAG, mEtName.getText().toString(), mEtPhone.getText().toString(), mEtContent.getText().toString(),
                ComplaintAccessoryId, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast(EmptyUtils.strEmpty(returnData.getMsg()));
                        goFinish();
                    }
                });
    }

    @Override
    public void onOssUpResult(String mTag, String accessoryId, String url, int width, int height) {
        switch (mTag) {
            case TAG_SELECT_PIC_01://banner 1号位
                selectPicSuccess(accessoryId, url, width, height);
                break;
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }

    private void selectPicSuccess(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(accessoryId)&&!EmptyUtils.isEmpty(url)) {
            FreeBackPicModel freeBackPic = new FreeBackPicModel(accessoryId, url, PIC_NOR);
            freeBackPicAdapter.getData().add(0,freeBackPic);
            freeBackPicAdapter.notifyDataSetChanged();
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //删除图片
    private void deletePic(FreeBackPicAdapter freeBackPicAdapter, int pos) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(getContext(), "提示", "是否确认删除图片?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        //删除科室
                        showWaitDialog();
                        String accessoryId = freeBackPicAdapter.getData().get(pos).getId();
                        BaseModel.sendDelAttachmentUploadRequest(TAG, accessoryId, new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                freeBackPicAdapter.getData().remove(pos);
                                freeBackPicAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                });
    }

    //获取图片拼接之后的数据
    protected String getPicSplicingData(FreeBackPicAdapter freeBackPicAdapter) {
        String accessoryId = "";
        for (FreeBackPicModel datum : freeBackPicAdapter.getData()) {
            if (PIC_NOR == datum.getItemType()) {
                accessoryId += "'" + datum.getId() + "',";
            }
        }
        if (!EmptyUtils.isEmpty(accessoryId)) {
            accessoryId = accessoryId.substring(0, accessoryId.length() - 1);
        }

        return accessoryId;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (compressUploadSinglePicUtils!=null){
            compressUploadSinglePicUtils.onDeatoryUtils();
        }

        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static ComplainSuggestFragment newIntance() {
        ComplainSuggestFragment complainSuggestFragment = new ComplainSuggestFragment();
        return complainSuggestFragment;
    }
}
