package com.jrdz.zhyb_android.ui.insured.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.frame.lbs_library.utils.CustomLocationBean;
import com.frame.lbs_library.utils.ILocationListener;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.model.KVModel;
import com.jrdz.zhyb_android.ui.insured.activity.SlowCarePharmacyActitiy;
import com.jrdz.zhyb_android.ui.insured.adapter.EmployeeMedicalAdapter;
import com.jrdz.zhyb_android.ui.insured.adapter.SlowCarePharmacyAdapter;
import com.jrdz.zhyb_android.ui.insured.model.MechanisQueryModel;
import com.jrdz.zhyb_android.ui.insured.model.SlowCarePharmacyModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.location.LocationTool;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-05-14
 * 描    述：定点医疗机构
 * ================================================
 */
public class EmployeeMedicalFragment extends BaseRecyclerViewFragment {
    protected EditText mEtSearch;
    protected TextView mTvSearch;
    protected LinearLayout mLlScreen,mLlDistance, mLlGrade, mLlZoning;
    protected TextView mTvDistance, mTvGrade, mTvZoning, mTvTypeTag, mTvNum;

    protected WheelUtils wheelUtils;

    protected int choosePos1 = 0, choosePos2 = 0, choosePos3 = 0;
    protected double lat = 0, lon = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public int getLayoutId() {
        return R.layout.actitiy_slowcare_pharmacy;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mEtSearch = view.findViewById(R.id.et_search);
        mTvSearch = view.findViewById(R.id.stv_search);
        mLlScreen= view.findViewById(R.id.ll_screen);
        mLlDistance = view.findViewById(R.id.ll_distance);
        mTvDistance = view.findViewById(R.id.tv_distance);
        mLlGrade = view.findViewById(R.id.ll_grade);
        mTvGrade = view.findViewById(R.id.tv_grade);
        mLlZoning = view.findViewById(R.id.ll_zoning);
        mTvZoning = view.findViewById(R.id.tv_zoning);
        mTvTypeTag = view.findViewById(R.id.tv_type_tag);
        mTvNum = view.findViewById(R.id.tv_num);

        mTitleBar.setVisibility(View.GONE);
    }

    @Override
    public void initAdapter() {
        mAdapter = new EmployeeMedicalAdapter();
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void initData() {
        lat=getArguments().getDouble("lat", 0);
        lon=getArguments().getDouble("lon", 0);
        super.initData();
        //隐藏定点批次
        mLlGrade.setVisibility(View.GONE);
        mTvTypeTag.setText("定点医疗机构");

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                    return true;
                }
                return false;
            }
        });

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                }
            }
        });

        mTvSearch.setOnClickListener(this);
        mLlDistance.setOnClickListener(this);
        mLlGrade.setOnClickListener(this);
        mLlZoning.setOnClickListener(this);
        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        super.getData();
        SlowCarePharmacyModel.sendQueryCoordinationMedinsListRequest(TAG, String.valueOf(mPageNum), "10", mEtSearch.getText().toString(),
                null == mTvDistance.getTag() ? "1" : String.valueOf(mTvDistance.getTag()),
                null == mTvZoning.getTag() ? "" : String.valueOf(mTvZoning.getTag()), String.valueOf(lat), String.valueOf(lon),
                new CustomerJsonCallBack<SlowCarePharmacyModel>() {
                    @Override
                    public void onRequestError(SlowCarePharmacyModel returnData, String msg) {
                        hideRefreshView();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(SlowCarePharmacyModel returnData) {
                        hideRefreshView();
                        List<SlowCarePharmacyModel.DataBean> infos = returnData.getData();
                        if (mAdapter != null && infos != null) {
                            if (mPageNum == 0) {
                                if (mTvNum != null) {
                                    mTvNum.setText(EmptyUtils.strEmptyToText(returnData.getTotalItems(), "0")+"家");
                                }
                                if (infos.size() <= 0) {
                                    mAdapter.isUseEmpty(true);
                                }
                                mAdapter.setNewData(infos);
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });

    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.stv_search://搜索
                if (EmptyUtils.isEmpty(mEtSearch.getText().toString())) {
                    showShortToast("请输入搜索关键词");
                    return;
                }
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.ll_distance://排序
                KeyboardUtils.hideSoftInput(getActivity());
                if (wheelUtils == null) {
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(getContext(), "", choosePos3,
                        CommonlyUsedDataUtils.getInstance().getFixmedinsSort02(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                choosePos3 = choosePos;
                                mTvDistance.setTag(dateInfo.getCode());
                                mTvDistance.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                                showWaitDialog();
                                onRefresh(mRefreshLayout);
                            }
                        });
                break;
            case R.id.ll_grade://机构等级
                KeyboardUtils.hideSoftInput(getActivity());
                if (wheelUtils == null) {
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(getContext(), "", choosePos1,
                        CommonlyUsedDataUtils.getInstance().getFixmedinsBatch(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                choosePos1 = choosePos;
                                mTvGrade.setTag(dateInfo.getCode());
                                mTvGrade.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                                showWaitDialog();
                                onRefresh(mRefreshLayout);
                            }
                        });
                break;
            case R.id.ll_zoning://医保区划
                KeyboardUtils.hideSoftInput(getActivity());
                if (wheelUtils == null) {
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(getContext(), "", choosePos2,
                        CommonlyUsedDataUtils.getInstance().getFixmedinsZoning02(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                choosePos2 = choosePos;
                                mTvZoning.setTag(dateInfo.getCode());
                                mTvZoning.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                                showWaitDialog();
                                onRefresh(mRefreshLayout);
                            }
                        });
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (wheelUtils != null) {
            wheelUtils.dissWheel();
        }
    }

    public static EmployeeMedicalFragment newIntance(double lat,double lon) {
        EmployeeMedicalFragment employeeMedicalFragment = new EmployeeMedicalFragment();
        Bundle bundle = new Bundle();
        bundle.putDouble("lat", lat);
        bundle.putDouble("lon", lon);
        employeeMedicalFragment.setArguments(bundle);
        return employeeMedicalFragment;
    }
}
