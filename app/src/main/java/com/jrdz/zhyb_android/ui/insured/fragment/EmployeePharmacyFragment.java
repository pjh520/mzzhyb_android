package com.jrdz.zhyb_android.ui.insured.fragment;

import android.os.Bundle;
import android.view.View;

import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.adapter.EmployeePharmacyAdapter;
import com.jrdz.zhyb_android.ui.insured.model.SlowCarePharmacyModel;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-05-14
 * 描    述：定点医疗机构
 * ================================================
 */
public class EmployeePharmacyFragment extends EmployeeMedicalFragment {

    @Override
    public void initAdapter() {
        mAdapter = new EmployeePharmacyAdapter();
    }

    @Override
    public void initData() {
        super.initData();
        //隐藏定点批次
        mLlScreen.setVisibility(View.GONE);
        mTvTypeTag.setText("定点零售药店");
    }

    @Override
    public void getData() {
        super.getData();
        SlowCarePharmacyModel.sendQueryCoordinationPharmacyListRequest(TAG, String.valueOf(mPageNum), "10", mEtSearch.getText().toString(),
                null == mTvDistance.getTag() ? "1" : String.valueOf(mTvDistance.getTag()),
                String.valueOf(lat), String.valueOf(lon), new CustomerJsonCallBack<SlowCarePharmacyModel>() {
                    @Override
                    public void onRequestError(SlowCarePharmacyModel returnData, String msg) {
                        hideRefreshView();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(SlowCarePharmacyModel returnData) {
                        hideRefreshView();
                        List<SlowCarePharmacyModel.DataBean> infos = returnData.getData();
                        if (mAdapter != null && infos != null) {
                            if (mPageNum == 0) {
                                if (mTvNum != null) {
                                    mTvNum.setText(EmptyUtils.strEmptyToText(returnData.getTotalItems(), "0") + "家");
                                }
                                if (infos.size() <= 0) {
                                    mAdapter.isUseEmpty(true);
                                }
                                mAdapter.setNewData(infos);
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    public static EmployeePharmacyFragment newIntance(double lat,double lon) {
        EmployeePharmacyFragment employeePharmacyFragment = new EmployeePharmacyFragment();
        Bundle bundle = new Bundle();
        bundle.putDouble("lat", lat);
        bundle.putDouble("lon", lon);
        employeePharmacyFragment.setArguments(bundle);
        return employeePharmacyFragment;
    }
}
