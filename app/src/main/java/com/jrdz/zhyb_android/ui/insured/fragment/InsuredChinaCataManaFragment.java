package com.jrdz.zhyb_android.ui.insured.fragment;

import android.os.Bundle;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.activity.UpdateChinaCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.adapter.ChinaCataAdapter;
import com.jrdz.zhyb_android.ui.catalogue.fragment.WestCataManaFragment;
import com.jrdz.zhyb_android.ui.catalogue.model.ChinaCataListModel;
import com.jrdz.zhyb_android.ui.insured.adapter.InsuredChinaCataAdapter;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/15
 * 描    述： 参保人中药饮片目录
 * ================================================
 */
public class InsuredChinaCataManaFragment extends InsuredWestCataManaFragment {
    @Override
    public void initAdapter() {
        mAdapter = new InsuredChinaCataAdapter();
    }

    @Override
    public void getData() {
        ChinaCataListModel.sendChinaCataListRequest(TAG + listType, String.valueOf(mPageNum), "10",
                catalogueManageActivity == null ? "" : catalogueManageActivity.getSearchText(), new CustomerJsonCallBack<ChinaCataListModel>() {
                    @Override
                    public void onRequestError(ChinaCataListModel returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        if (mPageNum == 0) {
                            if (mTvNum!=null){
                                mTvNum.setText("0条");
                            }
                        }
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(ChinaCataListModel returnData) {
                        hideRefreshView();
                        if (mPageNum == 0) {
                            if (mTvNum!=null){
                                mTvNum.setText(returnData.getTotalItems() + "条");
                            }
                        }
                        List<ChinaCataListModel.DataBean> infos = returnData.getData();
                        if (mAdapter!=null&&infos != null) {
                            if (mPageNum == 0) {
                                mAdapter.setNewData(infos);
                                if (infos.size() <= 0) {
                                    mAdapter.isUseEmpty(true);
                                }
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    public static InsuredChinaCataManaFragment newIntance(String listType, String listTypeName) {
        InsuredChinaCataManaFragment chinaCataManaFragment = new InsuredChinaCataManaFragment();
        Bundle bundle = new Bundle();
        bundle.putString("listType", listType);
        bundle.putString("listTypeName", listTypeName);
        chinaCataManaFragment.setArguments(bundle);
        return chinaCataManaFragment;
    }
}
