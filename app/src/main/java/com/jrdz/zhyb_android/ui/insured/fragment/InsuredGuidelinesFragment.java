package com.jrdz.zhyb_android.ui.insured.fragment;

import android.content.Context;
import android.view.View;

import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-26
 * 描    述：
 * ================================================
 */
public class InsuredGuidelinesFragment extends BaseFragment {
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_insured_guidelines;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        hideLeftView();
        ImmersionBar.setTitleBar(this, mTitleBar);
        setTitle("办事指南");
    }

    public static InsuredGuidelinesFragment newIntance() {
        InsuredGuidelinesFragment insuredGuidelinesFragment = new InsuredGuidelinesFragment();
        return insuredGuidelinesFragment;
    }
}
