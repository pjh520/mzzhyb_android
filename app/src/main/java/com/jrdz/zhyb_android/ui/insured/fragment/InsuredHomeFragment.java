package com.jrdz.zhyb_android.ui.insured.fragment;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.ColorUtils;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;

import com.alibaba.fastjson.JSON;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.IsInstallUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.widget.CustRefreshLayout;
import com.frame.compiler.widget.banner.listener.OnBannerClickListener;
import com.frame.compiler.widget.banner.manager.GlideAppSimpleImageManager_round;
import com.frame.compiler.widget.banner.widget.BannerLayout;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.toast.ToastUtil;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeRecyclerView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.base.MyWebViewActivity_html;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.H5JsonCallBack;
import com.jrdz.zhyb_android.pos.CPayDevice;
import com.jrdz.zhyb_android.ui.home.activity.MainActivity;
import com.jrdz.zhyb_android.ui.home.activity.NewsDetailActivity;
import com.jrdz.zhyb_android.ui.home.adapter.QueryComFunctAdapter;
import com.jrdz.zhyb_android.ui.home.model.BannerDataModel;
import com.jrdz.zhyb_android.ui.home.model.QueryComFunctModel;
import com.jrdz.zhyb_android.ui.insured.activity.AgencyQueryActivity;
import com.jrdz.zhyb_android.ui.insured.activity.BeautyMiZhiActivity;
import com.jrdz.zhyb_android.ui.insured.activity.ChronicDiseasesHandleActivity;
import com.jrdz.zhyb_android.ui.insured.activity.EmployeeClinicActivity;
import com.jrdz.zhyb_android.ui.insured.activity.FamilyAssistanceListActivity;
import com.jrdz.zhyb_android.ui.insured.activity.FingerprintloginActivity;
import com.jrdz.zhyb_android.ui.insured.activity.InsuranceTransferListActivity;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredCataManaActivity;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredDiseCataActivity;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredLoginActivity;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredMainActivity;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredOutpatientSettlementListActivity;
import com.jrdz.zhyb_android.ui.insured.activity.MechanisQueryActitiy;
import com.jrdz.zhyb_android.ui.insured.activity.OffLocationHandleActivity;
import com.jrdz.zhyb_android.ui.insured.activity.PersonalAccountQueryActivity;
import com.jrdz.zhyb_android.ui.insured.activity.PersonalAccountQueryListActivity;
import com.jrdz.zhyb_android.ui.insured.activity.PersonalInsuInfoDownloadActivity;
import com.jrdz.zhyb_android.ui.insured.activity.PersonalInsuInfoListActivity;
import com.jrdz.zhyb_android.ui.insured.activity.PersonalInsuInfoQueryActivity;
import com.jrdz.zhyb_android.ui.insured.activity.ScanPayActivity;
import com.jrdz.zhyb_android.ui.insured.activity.SlowCarePharmacyActitiy;
import com.jrdz.zhyb_android.ui.insured.activity.SpecialDrugHandleActivity;
import com.jrdz.zhyb_android.ui.insured.activity.WantDoActivity;
import com.jrdz.zhyb_android.ui.insured.activity.WantQueryActivity;
import com.jrdz.zhyb_android.ui.insured.activity.WantSeeActivity;
import com.jrdz.zhyb_android.ui.insured.model.EpaymentUrlModel;
import com.jrdz.zhyb_android.ui.insured.model.GetAPPAuditModel;
import com.jrdz.zhyb_android.ui.insured.model.WechatForYuLinLoginDataModel;
import com.jrdz.zhyb_android.ui.insured.model.WechatForYuLinLoginModel;
import com.jrdz.zhyb_android.ui.login.activity.LoginActivity;
import com.jrdz.zhyb_android.ui.mine.activity.HwScanActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.InquiryInfoActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.SmartPhaMainActivity_user;
import com.jrdz.zhyb_android.utils.AESUtils;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.H5RequestUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginSmrzUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MMKVUtils;
import com.jrdz.zhyb_android.utils.WXUtils;
import com.library.constantStorage.ConstantStorage;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.constant.RefreshState;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/08
 * 描    述：参保人首页
 * ================================================
 */
public class InsuredHomeFragment extends InsuredHomePermissionFragment implements OnBannerClickListener, OnRefreshListener {
    protected CustRefreshLayout mRefreshLayout;
    private NestedScrollView mScrollview;
    private TextView mTvUserPhone,mTvBusinessProcessingMore,mTvBusinessQueryMore,mTvPublicServiceMore;
    private BannerLayout mBanner;
    private LinearLayout mLlHead, mLlLargeFontVersion, mLlZfb,mLlScanPay;
    private ImageView mIvHead,mIvSpecialdrugHandle,mIvSmartPharmacy,mIvInsuInfo,mIvPersonUsageRecords,mIvAccidentalInjuryReg,mIvChronicDiseasesHandle;
    private ShapeRecyclerView mSrlBusinessProcessing,mSrlBusinessQuery,mSrlPublicService;

    private int bannerHeight;
    private QueryComFunctAdapter queryComFunctAdapter, businessProcessingComFunctAdapter,queryOtherComFunctAdapter;
    private int requestTag = 0;
    public String urlText01 = "alipays://platformapi/startapp?appId=2021001123625885&page=pages%2findex%2findex%3fprovideId%3d2088241201533517%26chInfo%3dXSyibaochaxunqianzhi%26";
    public String urlText02 = "https://ds.alipay.com/?scheme=alipays://platformapi/startapp?appId=2021001123625885&page=pages%2findex%2findex%3fprovideId%3d2088241201533517%26chInfo%3dXSyibaochaxunqianzhi%26returnUrl%3dhttps%253a%252f%252fwww.alipay.com";
    private CustomerDialogUtils doctorUpdateTipDialog;
    private InsuredLoginSmrzUtils insuredLoginSmrzUtils;
    private H5RequestUtils h5RequestUtils;

    //登录成功信息
    private ObserverWrapper<Integer> mLoginObserve = new ObserverWrapper<Integer>() {
        @Override
        public void onChanged(@Nullable Integer value) {
            //设置机构、用户信息
            if (mTvUserPhone != null) {
                switch (value) {
                    case 0://退出登录
                        mIvHead.setImageResource(R.drawable.ic_insured_head);
                        mTvUserPhone.setText("登录");
//                        if ("1".equals(InsuredLoginUtils.getIsEnterpriseFundPay())){
//                            mLlScanPay.setVisibility(View.VISIBLE);
//                        }else {
//                            mLlScanPay.setVisibility(View.GONE);
//                        }
                        break;
                    case 1://登录成功
                        GlideUtils.loadImg(InsuredLoginUtils.getAccessoryUrl(), mIvHead, R.drawable.ic_insured_head, new CircleCrop());
                        mTvUserPhone.setText(EmptyUtils.isEmpty(InsuredLoginUtils.getPhone()) ? "登录" : StringUtils.encryptionPhone(InsuredLoginUtils.getPhone()));
//                        if ("1".equals(InsuredLoginUtils.getIsEnterpriseFundPay())){
//                            mLlScanPay.setVisibility(View.VISIBLE);
//                        }else {
//                            mLlScanPay.setVisibility(View.GONE);
//                        }
                        break;
                }

                setSmartMedicalEntranceVisibility();
            }
        }
    };

    //后台切换前台监听
    private ObserverWrapper<Boolean> mBackgroundObserve = new ObserverWrapper<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean value) {
            if (value) {
                showWaitDialog();
                onRefresh(mRefreshLayout);
            }
        }
    };
    //更新参保人手机号
    private ObserverWrapper<String> mUpdateInsuredPhoneObserve = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            switch (value) {
                case "1"://更新手机号
                    mTvUserPhone.setText(StringUtils.encryptionPhone(InsuredLoginUtils.getPhone()));
                    break;
                case "2"://更新用户头像
                    GlideUtils.loadImg(InsuredLoginUtils.getAccessoryUrl(), mIvHead, R.drawable.ic_insured_head, new CircleCrop());
                    break;
            }
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.fragment_insured_home;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mRefreshLayout = view.findViewById(R.id.refreshLayout);
        mScrollview = view.findViewById(R.id.scrollview);
        mLlHead = view.findViewById(R.id.ll_head);
        mIvHead = view.findViewById(R.id.iv_head);
        mTvUserPhone = view.findViewById(R.id.tv_user_phone);
        mBanner = view.findViewById(R.id.banner);
        mLlLargeFontVersion = view.findViewById(R.id.ll_large_font_version);
        mLlZfb = view.findViewById(R.id.ll_zfb);
        mLlScanPay = view.findViewById(R.id.ll_scan_pay);

        mIvSpecialdrugHandle= view.findViewById(R.id.iv_specialdrug_handle);
        mIvSmartPharmacy= view.findViewById(R.id.iv_smart_pharmacy);
        mIvInsuInfo= view.findViewById(R.id.iv_insu_info);
        mIvPersonUsageRecords= view.findViewById(R.id.iv_person_usage_records);
        mIvAccidentalInjuryReg= view.findViewById(R.id.iv_accidental_injury_reg);
        mIvChronicDiseasesHandle= view.findViewById(R.id.iv_chronic_diseases_handle);

        mTvBusinessProcessingMore= view.findViewById(R.id.tv_business_processing_more);
        mSrlBusinessProcessing= view.findViewById(R.id.srl_business_processing);
        mTvBusinessQueryMore= view.findViewById(R.id.tv_business_query_more);
        mSrlBusinessQuery = view.findViewById(R.id.srl_business_query);
        mTvPublicServiceMore= view.findViewById(R.id.tv_public_service_more);
        mSrlPublicService = view.findViewById(R.id.srl_public_service);

        hideLeftView();
        ImmersionBar.setTitleBar(this, mTitleBar);
        mTitleBar.setLineVisible(false);
        mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                , ContextCompat.getColor(getContext(), R.color.colorAccent), 0));
        mTitleBar.setTitleColor(getResources().getColor(R.color.white));
        mTitleBar.setTitle("");
    }

    @Override
    protected void getHomePageData() {
        super.getHomePageData();
        MsgBus.sendInsuredLoginStatus().observe(this, mLoginObserve);
        MsgBus.sendIsBackground().observe(this, mBackgroundObserve);
        MsgBus.updateInsuredInfo().observe(this, mUpdateInsuredPhoneObserve);

        //获取app是否在应用市场审核
        if (Constants.Configure.IS_APP_MARKET){
            getAPPAudit();
        }else {
            setSmartMedicalEntranceVisibility();
//            if ("1".equals(InsuredLoginUtils.getIsEnterpriseFundPay())){
//                mLlScanPay.setVisibility(View.VISIBLE);
//            }else {
//                mLlScanPay.setVisibility(View.GONE);
//            }
        }

        //设置下拉刷新
        setRefreshInfo();
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setEnableLoadMore(false);

        bannerHeight = getResources().getDimensionPixelOffset(R.dimen.dp_232);
        //设置用户头像,手机号码
        GlideUtils.loadImg(InsuredLoginUtils.getAccessoryUrl(), mIvHead, R.drawable.ic_insured_head, new CircleCrop());
        mTvUserPhone.setText(EmptyUtils.isEmpty(InsuredLoginUtils.getPhone()) ? "登录" : StringUtils.encryptionPhone(InsuredLoginUtils.getPhone()));

        //业务办理
        mSrlBusinessProcessing.setHasFixedSize(true);
        mSrlBusinessProcessing.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
        businessProcessingComFunctAdapter = new QueryComFunctAdapter();
        mSrlBusinessProcessing.setAdapter(businessProcessingComFunctAdapter);
        businessProcessingComFunctAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getBusinessProcessingData());
        //业务查询
        mSrlBusinessQuery.setHasFixedSize(true);
        mSrlBusinessQuery.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
        queryOtherComFunctAdapter = new QueryComFunctAdapter();
        mSrlBusinessQuery.setAdapter(queryOtherComFunctAdapter);
        queryOtherComFunctAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getBusinessQueryData());
        //公共服务
        mSrlPublicService.setHasFixedSize(true);
        mSrlPublicService.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
        queryComFunctAdapter = new QueryComFunctAdapter();
        mSrlPublicService.setAdapter(queryComFunctAdapter);
        queryComFunctAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getPublicServiceData());
        //获取首页跑马灯资讯
        showWaitDialog();
        getBannerData();

        if (Constants.Configure.IS_POS) {
            getPossn();
        }
    }

    //获取app的审核状态
    private void getAPPAudit() {
        GetAPPAuditModel.sendGetAPPAuditRequest(TAG, new CustomerJsonCallBack<GetAPPAuditModel>() {
            @Override
            public void onRequestError(GetAPPAuditModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GetAPPAuditModel returnData) {
                hideWaitDialog();
                GetAPPAuditModel.DataBean info = returnData.getData();
                //1.当版本号小于后台返回的版本号 那么就不需要隐藏
                if (info != null && RxTool.getVersionCode(getContext())>=info.getInteriorVersionNum()) {
                    //（0未审核 1已审核）
                    if (0==info.getAPPApprovalStatus()){
//                        mLlSmartMedicalEntrance.setVisibility(View.GONE);
//                        mLlScanPay.setVisibility(View.GONE);
                    }else {
                        setSmartMedicalEntranceVisibility();
//                        if ("1".equals(InsuredLoginUtils.getIsEnterpriseFundPay())){
//                            mLlScanPay.setVisibility(View.VISIBLE);
//                        }else {
//                            mLlScanPay.setVisibility(View.GONE);
//                        }
                    }
                }else {
                    setSmartMedicalEntranceVisibility();
//                    if ("1".equals(InsuredLoginUtils.getIsEnterpriseFundPay())){
//                        mLlScanPay.setVisibility(View.VISIBLE);
//                    }else {
//                        mLlScanPay.setVisibility(View.GONE);
//                    }
                }
            }
        });
    }

    //获取pos的sn码
    private void getPossn() {
        Log.e("666666", "00001104" + CPayDevice.getCPaySystem().readSerialNum());


//        SwitchUtils.wifiSwitch(getContext(),false);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mLlHead.setOnClickListener(this);
        mLlLargeFontVersion.setOnClickListener(this);
        mLlZfb.setOnClickListener(this);
        mLlScanPay.setOnClickListener(this);

        mIvSpecialdrugHandle.setOnClickListener(this);
        mIvSmartPharmacy.setOnClickListener(this);
        mIvInsuInfo.setOnClickListener(this);
        mIvPersonUsageRecords.setOnClickListener(this);
        mIvAccidentalInjuryReg.setOnClickListener(this);
        mIvChronicDiseasesHandle.setOnClickListener(this);

        mTvBusinessProcessingMore.setOnClickListener(this);
        mTvBusinessQueryMore.setOnClickListener(this);
        mTvPublicServiceMore.setOnClickListener(this);
        businessProcessingComFunctAdapter.setOnItemClickListener(mOnFunctListener);
        queryComFunctAdapter.setOnItemClickListener(mOnFunctListener);
        queryOtherComFunctAdapter.setOnItemClickListener(mOnFunctListener);

        mScrollview.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY <= 0) {
                    mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                            , ContextCompat.getColor(getContext(), R.color.colorAccent), 0));
                    mTitleBar.setTitle("");
                } else if (scrollY <= bannerHeight) {
                    float alpha = (float) scrollY / bannerHeight;
                    mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                            , ContextCompat.getColor(getContext(), R.color.colorAccent), alpha));
                    mTitleBar.setTitle("");
                } else {
                    mTitleBar.setBackgroundColor(ColorUtils.blendARGB(Color.TRANSPARENT
                            , ContextCompat.getColor(getContext(), R.color.colorAccent), 1));
                    mTitleBar.setTitle("米脂智慧医保服务平台");
                }
            }
        });
    }

    //设置SmartRefreshLayout的刷新 加载样式
    public void setRefreshInfo() {
        mRefreshLayout.setPrimaryColorsId(R.color.windowbackground, R.color.txt_color_666);
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
        mRefreshLayout.setHeaderMaxDragRate(5);//最大显示下拉高度/Header标准高度  头部下拉高度的比例
    }

    //设置医药机构登录 是否显示
    private void setSmartMedicalEntranceVisibility() {
//        if (mLlSmartMedicalEntrance==null)return;
//        if (InsuredLoginUtils.isLogin()){
//            mLlSmartMedicalEntrance.setVisibility(View.GONE);
//        }else {
//            mLlSmartMedicalEntrance.setVisibility(View.VISIBLE);
//        }
    }

    //获取轮播图数据
    private void getBannerData() {
        BannerDataModel.sendInsuredBannerDataRequest(TAG, new CustomerJsonCallBack<BannerDataModel>() {
            @Override
            public void onRequestError(BannerDataModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BannerDataModel returnData) {
                dissWaitDailog();
                List<BannerDataModel.DataBean> bannerDatas = returnData.getData();
                int banner_delay_time = new BigDecimal(EmptyUtils.strEmptyToText(returnData.getTimeInterval(),"3")).multiply(new BigDecimal("1000")).intValue();
                if (bannerDatas != null&&!bannerDatas.isEmpty()) {
                    if (bannerDatas.size() == 1) {
                        mBanner.initTips(false, false, false)
                                .setImageLoaderManager(new GlideAppSimpleImageManager_round())
                                .setPageNumViewMargin(12, 12, 12, 12)
                                .setViewPagerTouchMode(true)
                                .setDelayTime(banner_delay_time)
                                .initListResources(bannerDatas)
                                .switchBanner(false)
                                .setOnBannerClickListener(InsuredHomeFragment.this);
                    } else {
                        mBanner.initTips(false, true, false)
                                .setImageLoaderManager(new GlideAppSimpleImageManager_round())
                                .setPageNumViewMargin(12, 12, 12, 12)
                                .setViewPagerTouchMode(false)
                                .setDelayTime(banner_delay_time)
                                .initListResources(bannerDatas)
                                .switchBanner(true)
                                .setOnBannerClickListener(InsuredHomeFragment.this);
                    }
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.ll_head://判断是否登录 已登录 跳转个人信息页面 未登录 跳转登录页面
                if (!InsuredLoginUtils.isLogin()) {
                    showShortToast("请先登录");
                    if (EmptyUtils.isEmpty(MMKVUtils.getString("finger_login_key", ""))) {//关闭指纹登录
                        InsuredLoginActivity.newIntance(getContext(), 0);
                    } else {
                        FingerprintloginActivity.newIntance(getContext(), 0);
                    }
                    return;
                }
                ((InsuredMainActivity) getActivity()).setCurrentTab(2);
                break;
//            case R.id.ll_smart_medical_entrance://进入榆林智慧医保
//                //判断用户是否已激活
////                if (!MechanismInfoUtils.isActivate()) {//未激活 跳转智慧医保激活页面
////                    ActivateDeviceActivity.newIntance(getContext());
////                } else
//                if (LoginUtils.isLogin()) {//已激活 判断是否已登录智慧医保
//                    MainActivity.newIntance(getContext(), 0);
//                } else {
//                    LoginActivity.newIntance(getContext());
//                }
//                break;
            case R.id.ll_zfb://跳转支付宝 电子凭证
                if (IsInstallUtils.getInstance().checkAliPayInstalled(getContext())) {
                    Intent intent = new Intent();
                    intent.setData(Uri.parse(urlText01));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    showShortToast("请安装支付宝软件");

                    //如果是pos机 跳转pos机自带的应用市场
                    if (Constants.Configure.IS_POS) {
                        String packageName = "com.centerm.cpay.applicationshop";
                        String className = "com.centerm.cpay.applicationshop.activity.AppDetailActivity";
                        Intent intent = new Intent();
                        //这里改成你的应用包名
                        intent.putExtra("packageName", "com.eg.android.AlipayGphone");
                        ComponentName componentName = new ComponentName(packageName, className);
                        intent.setComponent(componentName);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent();
                        intent.setAction("android.intent.action.VIEW");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        Uri url = Uri.parse(urlText02);
                        intent.setData(url);
                        startActivity(intent);
                    }
                }
                break;
            case R.id.ll_scan_pay://扫码支付
                scanPay();
                break;
            case R.id.ll_large_font_version://大字版
                showShortToast("暂未开放");
                break;
            case R.id.iv_specialdrug_handle://特殊药品申请备案
                if (insuredLoginSmrzUtils == null) {
                    insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
                }

                if (insuredLoginSmrzUtils.isLoginSmrz(getContext())) {
                    SpecialDrugHandleActivity.newIntance(getContext());
                }
                break;
            case R.id.iv_smart_pharmacy://在线购药
                SmartPhaMainActivity_user.newIntance(getContext(), 0);
                break;
            case R.id.iv_insu_info://参保凭证下载
                if (insuredLoginSmrzUtils == null) {
                    insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
                }

                if (insuredLoginSmrzUtils.isLoginSmrz(getContext())) {
                    PersonalInsuInfoDownloadActivity.newIntance(getContext());
                }
                break;
            case R.id.iv_person_usage_records://个人账户使用记录
                showShortToast("暂未开放");
                break;
            case R.id.iv_accidental_injury_reg://意外伤害登记
                showShortToast("暂未开放");
                break;
            case R.id.iv_chronic_diseases_handle://门诊慢病申报
                ChronicDiseasesHandleActivity.newIntance(getContext());
                break;
            case R.id.tv_business_processing_more://我要办--更多
                WantDoActivity.newIntance(getContext());
                break;
            case R.id.tv_business_query_more://我要查--更多
                WantQueryActivity.newIntance(getContext());
                break;
            case R.id.tv_public_service_more://我要看--更多
                WantSeeActivity.newIntance(getContext(),0);
                break;
        }
    }

    //扫码支付
    private void scanPay() {
        startActivityForResult(new Intent(getContext(), HwScanActivity.class), new OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == -1) {
                    String scanResult = data.getStringExtra("scanResult");
                    ScanPayActivity.newIntance(getContext(),scanResult);
                }
            }
        });
    }

    //常用功能查询  其他功能查询
    private BaseQuickAdapter.OnItemClickListener mOnFunctListener = new BaseQuickAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                return;
            }
            QueryComFunctModel sortItem = ((QueryComFunctAdapter) adapter).getItem(position);
            switch (sortItem.getId()) {
                case "1101"://政策宣传
                    WantSeeActivity.newIntance(getContext(),0);
                    break;
                case "1102"://最新通知
                    WantSeeActivity.newIntance(getContext(),1);
                    break;
                case "1103"://投诉建议
                    WantSeeActivity.newIntance(getContext(),2);
                    break;
                case "1104"://大美米脂
                    BeautyMiZhiActivity.newIntance(getContext());
                    break;
                case "1201"://个人账户查询
                    PersonalAccountQueryListActivity.newIntance(getContext());
                    break;
                case "1202"://医保目录查询
                    InsuredCataManaActivity.newIntance(getContext());
                    break;
                case "1203"://参保信息查询
//                    PersonalInsuInfoListActivity.newIntance(getContext());
                    goH5Pager("参保信息查询", Constants.H5URL.H5_TRANSFERPROOF_URL);
                    break;
                case "1204"://定点医药机构
//                    InsuredDiseCataActivity.newIntance(getContext(), "1");
                    MechanisQueryActitiy.newIntance(getContext());
                    break;
                case "1205"://定点慢保药店
                    SlowCarePharmacyActitiy.newIntance(getContext());
                    break;
                case "1206"://职工门诊统筹定点医药机构
                    EmployeeClinicActivity.newIntance(getContext());
                    break;
                case "1301"://城乡居民医保缴费
//                    getEpaymentUrl();
//                    if (insuredLoginSmrzUtils == null) {
//                        insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
//                    }
//
//                    if (insuredLoginSmrzUtils.isLoginSmrz(getContext())) {
//                        if (IsInstallUtils.getInstance().checkAliPayInstalled(getContext())) {
//                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.WebUrl.ALIPAYS_SXSBJF_URL));
//                            startActivity(intent);
//                        } else {
//                            showShortToast("请安装支付宝软件");
//
//                            //如果是pos机 跳转pos机自带的应用市场
//                            if (Constants.Configure.IS_POS) {
//                                String packageName = "com.centerm.cpay.applicationshop";
//                                String className = "com.centerm.cpay.applicationshop.activity.AppDetailActivity";
//                                Intent intent = new Intent();
//                                //这里改成你的应用包名
//                                intent.putExtra("packageName", "com.eg.android.AlipayGphone");
//                                ComponentName componentName = new ComponentName(packageName, className);
//                                intent.setComponent(componentName);
//                                startActivity(intent);
//                            } else {
//                                Intent intent = new Intent();
//                                intent.setAction("android.intent.action.VIEW");
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                Uri url = Uri.parse(urlText02);
//                                intent.setData(url);
//                                startActivity(intent);
//                            }
//                        }
//                    }
//                    goWxOpenUrl();
                    MyWebViewActivity.newIntance(getContext(), "城乡居民医保缴费", Constants.WebUrl.SXYBGF_SXSBJF_URL2, true, false);
                    break;
                case "1302"://门诊慢病申报
//                    ChronicDiseasesHandleActivity.newIntance(getContext());
//                    MyWebViewActivity.newIntance(getContext(), "门诊慢病申报", Constants.H5URL.H5_CHRONICDISEASESHANDLE_URL, true, false);
//                    MyWebViewActivity.newIntance(getContext(), "门诊慢病申报", "http://113.135.194.23:3083/medicare/index.html", true, false);
                    WXUtils.goSmallRoutine(getContext(), Constants.Configure.EBCX_APPID, "");
                    break;
                case "1303"://异地就医备案
                    OffLocationHandleActivity.newIntance(getContext());
                    break;
                case "1304"://特殊药品备案
                    if (insuredLoginSmrzUtils == null) {
                        insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
                    }

                    if (insuredLoginSmrzUtils.isLoginSmrz(getContext())) {
                        SpecialDrugHandleActivity.newIntance(getContext());
                    }
                    break;
                case "1305"://医药机构结算
//                    InsuredOutpatientSettlementListActivity.newIntance(getContext());
                    //判断用户是否已激活
                    if (LoginUtils.isLogin()) {//已激活 判断是否已登录智慧医保
                        MainActivity.newIntance(getContext(), 0);
                    } else {
                        LoginActivity.newIntance(getContext());
                    }
                    break;
                case "1306"://外伤信息登记
                    MyWebViewActivity.newIntance(getContext(), "外伤信息登记", Constants.H5URL.H5_TRAUMAINFOREG_URL, true, false);
                    break;
                case "1307"://家庭共济绑定
                    FamilyAssistanceListActivity.newIntance(getContext());
                    break;
                case "1308"://医保转移申请
                    InsuranceTransferListActivity.newIntance(getContext());
                    break;
            }
        }
    };

    //跳转微信打开链接
    private void goWxOpenUrl() {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("weixin://dl/business/?t="+Uri.encode("https://www.baidu.com/")));
            startActivity(intent); // 启动 Intent

//            intent.setPackage("com.tencent.mm"); // 设置目标包名为微信
//            getContext().startActivity(intent);
//            Intent intent =getContext().getPackageManager().getLaunchIntentForPackage("com.tencent.mm");
//            intent.setData(Uri.parse(Constants.WebUrl.GH_SXSBJF_URL));
//            startActivity(intent);
        }catch (Exception e) {
            ToastUtil.show("打开微信失败，请先检查微信是否安装");
            e.printStackTrace();
        }
    }

    //获取E缴费医保缴费页面地址
    private void getEpaymentUrl() {
        showWaitDialog();
        EpaymentUrlModel.sendEpaymentUrlRequest(TAG, EmptyUtils.isEmpty(InsuredLoginUtils.getPhone()) ? EmptyUtils.strEmpty(InsuredLoginUtils.getIdCardNo()) : InsuredLoginUtils.getPhone(), new CustomerJsonCallBack<EpaymentUrlModel>() {
            @Override
            public void onRequestError(EpaymentUrlModel returnData, String msg) {
                hideWaitDialog();
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(EpaymentUrlModel returnData) {
                hideWaitDialog();
                String data = returnData.getData();
                if (!EmptyUtils.isEmpty(data)){
                    MyWebViewActivity_html.newIntance(getContext(), "城乡居民医保缴费", data, true, false);
                }else {
                    showTipDialog("网页数据为空，请重新点击按钮获取");
                }
            }
        });
    }

    @Override
    public void onBannerClick(View view, int position, Object model) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            return;
        }
        if (model instanceof BannerDataModel.DataBean) {
            BannerDataModel.DataBean dataBean = (BannerDataModel.DataBean) model;
            if (dataBean != null) {
                //轮播图type 1代表纯广告 2跳外链 3跳公告 跳公告的id我放在url里面
                switch (dataBean.getTypeX()) {
                    case "1":
                        break;
                    case "2":
                        MyWebViewActivity.newIntance(getContext(), dataBean.getTitle(), dataBean.getUrlX(), true, false);
                        break;
                    case "3":
                        NewsDetailActivity.newIntance(getContext(), dataBean.getUrlX());
                        break;
                }
            }
        }
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        requestTag = 0;

        getBannerData();
    }

    //隐藏加载框
    public void dissWaitDailog() {
        requestTag++;
        if (requestTag >= 1) {
            hideRefreshView();
        }
    }

    //关闭刷新的view
    public void hideRefreshView() {
        if (mRefreshLayout.getState() == RefreshState.Refreshing) {
            mRefreshLayout.finishRefresh();
        } else {
            hideWaitDialog();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mBanner != null) {
            mBanner.start();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mBanner != null) {
            mBanner.stop();
        }
    }

    //跳转H5链接
    protected void goH5Pager(String title, String h5Url) {
        if (insuredLoginSmrzUtils == null) {
            insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
        }

        if (insuredLoginSmrzUtils.isLoginSmrz(getContext())) {
            if (h5RequestUtils==null){
                h5RequestUtils=new H5RequestUtils();
            }

            h5RequestUtils.goPager(getContext(), TAG, title, h5Url, new H5RequestUtils.IRequestListener() {
                @Override
                public void onShowWaitDialog() {
                    showWaitDialog();
                }

                @Override
                public void onHideWaitDialog() {
                    hideWaitDialog();
                }

                @Override
                public void onFail(String msg) {
                    showTipDialog(msg);
                }
            });
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (doctorUpdateTipDialog != null) {
            doctorUpdateTipDialog.onclean();
            doctorUpdateTipDialog = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBanner != null) {
            mBanner.destroy();
            mBanner = null;
        }

        if (insuredLoginSmrzUtils != null) {
            insuredLoginSmrzUtils.cleanData();
        }
    }

    public static InsuredHomeFragment newIntance() {
        InsuredHomeFragment homeFragment = new InsuredHomeFragment();
        return homeFragment;
    }
}
