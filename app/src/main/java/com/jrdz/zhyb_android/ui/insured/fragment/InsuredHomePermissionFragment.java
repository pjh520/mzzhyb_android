package com.jrdz.zhyb_android.ui.insured.fragment;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.frame.compiler.utils.RxTool;
import com.jrdz.zhyb_android.base.BaseFragment;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.model.CheckVersionUpdateModel;
import com.jrdz.zhyb_android.utils.MMKVUtils;
import com.jrdz.zhyb_android.widget.pop.UpdatePop;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.ui.main.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/12/12 0012
 * 描    述：首页权限数据抽取
 * ================================================
 */
public abstract class InsuredHomePermissionFragment extends BaseFragment {
    private UpdatePop updateAgreementPop;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public void initData() {
        super.initData();

        getHomePageData();
    }

    protected void getHomePageData() {
        //检测更新
        checkForUpdate();
    }

    //获取是否有新版本的信息
    private void checkForUpdate() {
        CheckVersionUpdateModel.sendCheckVersionUpdateRequest(TAG, new CustomerJsonCallBack<CheckVersionUpdateModel>() {
            @Override
            public void onRequestError(CheckVersionUpdateModel returnData, String msg) {
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(CheckVersionUpdateModel returnData) {
                CheckVersionUpdateModel.DataBean info = returnData.getData();

                //模拟需要更新的数据
//                info.setContent("模拟需要更新的数据");
//                info.setExternalVersionNum("1.3.0");
//                info.setInteriorVersionNum(17);
//                info.setIsConstraint("0");
//                info.setTitle("模拟需要更新的数据");
//                info.setUrl("");
                //1.当版本号小于后台返回的版本号 那么就需要更新
                if (info != null && RxTool.getVersionCode(getContext()) < info.getInteriorVersionNum()) {
                    showUpdatePop(info.getContent(), info.getUrl(), info.getIsConstraint(), info.getInteriorVersionNum());
                }
            }
        });
    }

    //显示更新弹框
    private void showUpdatePop(String content, String downloadurl, String enforce, int versionNo) {
        String noCommonUpdated = MMKVUtils.getString("noCommonUpdated", "");
        if ("0".equals(enforce) && String.valueOf(versionNo).equals(noCommonUpdated)) {//普通更新的情况下
            return;
        }
        updateAgreementPop = new UpdatePop(getContext(), content, "1".equals(enforce) ? true : false, new UpdatePop.IOptionListener() {
            @Override
            public void onAgree() {
                if (Constants.Configure.IS_POS){
                    goPosMarket();
                }else {
                    goBrowser(downloadurl);
                }
            }

            @Override
            public void onCancle() {
                MMKVUtils.putString("noCommonUpdated", String.valueOf(versionNo));
            }
        });
        updateAgreementPop.showPopupWindow();
    }

    //跳转pos机的应用市场更新app
    private void goPosMarket() {
        String packageName="com.centerm.cpay.applicationshop";
        String className="com.centerm.cpay.applicationshop.activity.AppDetailActivity";
        Intent intent=new Intent();
        //这里改成你的应用包名
        intent.putExtra("packageName", "com.jrdz.zhyb_android");
        ComponentName componentName=new ComponentName(packageName, className);
        intent.setComponent(componentName);
        startActivity(intent);
    }

    //跳转浏览器下载app
    private void goBrowser(String downloadurl) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(downloadurl));
        // 注意此处的判断intent.resolveActivity()可以返回显示该Intent的Activity对应的组件名
        // 官方解释 : Name of the component implementing an activity that can display the intent
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
//            final ComponentName componentName = intent.resolveActivity(getContext().getPackageManager());
            //  LogUtil.d("suyan = " + componentName.getClassName());
            startActivity(Intent.createChooser(intent, "请选择浏览器"));
        } else {
            showShortToast("链接错误或无浏览器");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (updateAgreementPop != null) {
            updateAgreementPop.onCleanListener();
            updateAgreementPop.dismiss();
            updateAgreementPop = null;
        }
    }
}
