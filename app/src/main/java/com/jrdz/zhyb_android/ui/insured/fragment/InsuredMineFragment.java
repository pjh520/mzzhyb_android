package com.jrdz.zhyb_android.ui.insured.fragment;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.baidu.mapapi.search.geocode.GeoCoder;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.FileUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeRelativeLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.model.CheckVersionUpdateModel;
import com.jrdz.zhyb_android.ui.insured.activity.FingerprintloginActivity;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredLoginActivity;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredPersonalActivity;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredSettingActivity;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredSharedActivity;
import com.jrdz.zhyb_android.ui.insured.model.SmrzResultModel;
import com.jrdz.zhyb_android.ui.mine.activity.AboutUsActivity;
import com.jrdz.zhyb_android.ui.mine.activity.AgreementActivtiy;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.PhaSortMineAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortMineModel;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.FreeBackActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.InsuredAccountActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.MyCollectionActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.MyTracksActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.OrderListActivity_user;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.DES3;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.MMKVUtils;
import com.library.constantStorage.ConstantStorage;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/20
 * 描    述：我的页面
 * ================================================
 */
public class InsuredMineFragment extends BaseFragment {
    private LinearLayout mLlMyTransaction,mLlMyCollection,mLlMyTracks,mLlFeedbackIcon,mLlAgreement,mLlShared,mLlAboutus,mLlMySetting;
    private ShapeRelativeLayout mRlPersonal;
    private ImageView mIvAccount;
    private TextView mTvIdentity, mTvPhone, mTvExit;

    private CustomerDialogUtils customerDialogUtils;
    private CustomeRecyclerView mSrlOrderSort;
    private PhaSortMineAdapter orderSortAdapter;//我的订单 适配器

    //登录成功信息
    private ObserverWrapper<Integer> mLoginObserve = new ObserverWrapper<Integer>() {
        @Override
        public void onChanged(@Nullable Integer value) {
            //设置机构、用户信息
            if (mTvIdentity != null && mTvPhone != null) {
                switch (value){
                    case 0://退出登录
                        mIvAccount.setImageResource(R.drawable.ic_insured_head);
                        mTvIdentity.setText("请先登录");
                        mTvPhone.setText("--");
                        break;
                    case 1://登录成功
                        GlideUtils.loadImg(InsuredLoginUtils.getAccessoryUrl(), mIvAccount,R.drawable.ic_insured_head,new CircleCrop());
                        mTvIdentity.setText(StringUtils.encryptionName(InsuredLoginUtils.getName()));
                        mTvPhone.setText(StringUtils.encryptionPhone(InsuredLoginUtils.getPhone()));
                        break;
                }
            }
        }
    };

    //更新参保人手机号
    private ObserverWrapper<String> mUpdateInsuredPhoneObserve = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            switch (value){
                case "1"://更新手机号
                    mTvPhone.setText(StringUtils.encryptionPhone(InsuredLoginUtils.getPhone()));
                    break;
                case "2"://更新用户头像
                    GlideUtils.loadImg(InsuredLoginUtils.getAccessoryUrl(), mIvAccount,R.drawable.ic_insured_head,new CircleCrop());
                    break;
                case "3"://更新用户姓名
                    mTvIdentity.setText(StringUtils.encryptionName(InsuredLoginUtils.getName()));
                    break;
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_insured_mine;
    }

    @Override
    public void initView(View view) {
        super.initView(view);

        mRlPersonal= view.findViewById(R.id.rl_personal);
        mIvAccount= view.findViewById(R.id.iv_account);
        mTvIdentity = view.findViewById(R.id.tv_identity);
        mTvPhone = view.findViewById(R.id.tv_phone);
        mSrlOrderSort = view.findViewById(R.id.srl_order_sort);

        mLlMyTransaction= view.findViewById(R.id.ll_my_transaction);
        mLlMyCollection= view.findViewById(R.id.ll_my_collection);
        mLlMyTracks= view.findViewById(R.id.ll_my_tracks);
        mLlFeedbackIcon= view.findViewById(R.id.ll_feedback_icon);
        mLlAgreement = view.findViewById(R.id.ll_agreement);
        mLlShared = view.findViewById(R.id.ll_shared);
        mLlAboutus = view.findViewById(R.id.ll_about_us);
        mLlMySetting = view.findViewById(R.id.ll_my_setting);
        mTvExit= view.findViewById(R.id.tv_exit);
    }

    @Override
    public void initData() {
        super.initData();
        MsgBus.sendInsuredLoginStatus().observe(this, mLoginObserve);
        MsgBus.updateInsuredInfo().observe(this, mUpdateInsuredPhoneObserve);


        //设置用户信息
        GlideUtils.loadImg(InsuredLoginUtils.getAccessoryUrl(), mIvAccount,R.drawable.ic_insured_head,new CircleCrop());
        mTvIdentity.setText(StringUtils.encryptionName(InsuredLoginUtils.getName()));
        mTvPhone.setText(StringUtils.encryptionPhone(InsuredLoginUtils.getPhone()));
        //我的订单
        mSrlOrderSort.setHasFixedSize(true);
        mSrlOrderSort.setLayoutManager(new GridLayoutManager(getContext(), 4, GridLayoutManager.VERTICAL, false));
        orderSortAdapter = new PhaSortMineAdapter();
        mSrlOrderSort.setAdapter(orderSortAdapter);
        orderSortAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getOrderSortData());
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mRlPersonal.setOnClickListener(this);

        mLlMyTransaction.setOnClickListener(this);
        mLlMyCollection.setOnClickListener(this);
        mLlMyTracks.setOnClickListener(this);
        mLlFeedbackIcon.setOnClickListener(this);
        mLlAgreement.setOnClickListener(this);
        mLlShared.setOnClickListener(this);
        mLlAboutus.setOnClickListener(this);
        mLlMySetting.setOnClickListener(this);
        mTvExit.setOnClickListener(this);
        //我的订单 item点击事件
        orderSortAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                PhaSortMineModel itemData = orderSortAdapter.getItem(i);
                switch (itemData.getId()) {
                    case "4001"://全部
                        OrderListActivity_user.newIntance(getContext(), 0);
                        break;
                    case "4002"://待付款
                        OrderListActivity_user.newIntance(getContext(), 1);
                        break;
                    case "4003"://待发货
                        OrderListActivity_user.newIntance(getContext(), 2);
                        break;
                    case "4004"://待收货
                        OrderListActivity_user.newIntance(getContext(), 3);
                        break;
                    case "4005"://已完成
                        OrderListActivity_user.newIntance(getContext(), 4);
                        break;
                    case "4006"://已取消
                        OrderListActivity_user.newIntance(getContext(), 5);
                        break;
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.rl_personal://进入个人信息页面
                InsuredPersonalActivity.newIntance(getContext());
                break;
            case R.id.ll_my_transaction://我的交易
                InsuredAccountActivity.newIntance(getContext());
                break;
            case R.id.ll_my_collection://我的收藏
                MyCollectionActivity.newIntance(getContext());
                break;
            case R.id.ll_my_tracks://我的足迹
                MyTracksActivity.newIntance(getContext());
                break;
            case R.id.ll_feedback_icon://意见反馈
                FreeBackActivity.newIntance(getContext());
                break;
            case R.id.ll_agreement://应用协议
                AgreementActivtiy.newIntance(getContext());
                break;
            case R.id.ll_shared://分享给好友
                InsuredSharedActivity.newIntance(getContext());
                break;
            case R.id.ll_about_us://关于我们
                AboutUsActivity.newIntance(getContext());
                break;
            case R.id.ll_my_setting://设置
                InsuredSettingActivity.newIntance(getContext());
                break;
            case R.id.tv_exit://退出登录
                exit();
                break;
        }
    }

    //退出登录
    private void exit() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(getContext(), "提示", "是否确认退出登录?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        showWaitDialog();
                        BaseModel.sendLogoutRequest_user(TAG, new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                if (EmptyUtils.isEmpty(MMKVUtils.getString("finger_login_key", ""))) {//关闭指纹登录
                                    InsuredLoginActivity.newIntance(getContext(),1);
                                }else {
                                    FingerprintloginActivity.newIntance(getContext(),1);
                                }
                            }
                        });
                    }
                });
    }

//    OnGetGeoCoderResultListener listener = new OnGetGeoCoderResultListener() {
//        @Override
//        public void onGetGeoCodeResult(GeoCodeResult geoCodeResult) {
//            if (null != geoCodeResult && null != geoCodeResult.getLocation()) {
//                if (geoCodeResult == null || geoCodeResult.error != SearchResult.ERRORNO.NO_ERROR) {
//                    hideWaitDialog();
//                    //没有检索到结果
//                    showShortToast("该地址不合法，暂未检测到地址经纬度");
//                } else {
//                    Log.e(TAG, "onGetGeoCodeResult:latitude==== "+geoCodeResult.getLocation().latitude+"=====longitude==== "+geoCodeResult.getLocation().longitude);
//                }
//            }
//        }
//
//        @Override
//        public void onGetReverseGeoCodeResult(ReverseGeoCodeResult reverseGeoCodeResult) {
//        }
//    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static InsuredMineFragment newIntance() {
        InsuredMineFragment mineFragment = new InsuredMineFragment();
        return mineFragment;
    }
}
