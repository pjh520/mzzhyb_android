package com.jrdz.zhyb_android.ui.insured.fragment;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.NewsDetailActivity;
import com.jrdz.zhyb_android.ui.home.adapter.NewsAdapter;
import com.jrdz.zhyb_android.ui.home.model.NotifyModel;
import com.jrdz.zhyb_android.ui.insured.activity.BeautyMiZhiActivity;
import com.jrdz.zhyb_android.ui.insured.activity.MizhiNewsDetailActivity;
import com.jrdz.zhyb_android.ui.insured.adapter.InsuredNewsAdapter;
import com.jrdz.zhyb_android.ui.insured.model.FixmedinsListModel;
import com.jrdz.zhyb_android.ui.insured.model.InsuredNewsModel;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/26
 * 描    述： 政策宣传fragment
 * ================================================
 */
public class InsuredNewsFragment extends BaseRecyclerViewFragment {
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_insured_news;
    }

    @Override
    public void initAdapter() {
        mAdapter=new InsuredNewsAdapter("2");
    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        super.getData();

        InsuredNewsModel.sendInsuredNewsRequest(TAG, String.valueOf(mPageNum), "10", "2", new CustomerJsonCallBack<InsuredNewsModel>() {
            @Override
            public void onRequestError(InsuredNewsModel returnData, String msg) {
                hideRefreshView();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(InsuredNewsModel returnData) {
                hideRefreshView();
                List<InsuredNewsModel.DataBean> infos=returnData.getData();
                if (mAdapter != null && infos != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(infos);
                        if (infos.size() <= 0) {
                            mAdapter.isUseEmpty(true);
                        }
                    } else {
                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter,view,position);
        InsuredNewsModel.DataBean itemData = ((InsuredNewsAdapter) adapter).getItem(position);
        if (itemData!=null){
            if (EmptyUtils.isEmpty(itemData.getNewsLink())){
                MizhiNewsDetailActivity.newIntance(getContext(),itemData.getNewsId());
            }else {
                MyWebViewActivity.newIntance(getContext(), itemData.getTitle(),itemData.getNewsLink(),true,false);
            }
        }
    }

    public static InsuredNewsFragment newIntance() {
        InsuredNewsFragment insuredNewsFragment = new InsuredNewsFragment();
        return insuredNewsFragment;
    }
}
