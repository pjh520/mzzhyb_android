package com.jrdz.zhyb_android.ui.insured.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.model.WestCataListModel;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredCataManaActivity;
import com.jrdz.zhyb_android.ui.insured.adapter.InsuredWestCataAdapter;
import com.zhy.http.okhttp.OkHttpUtils;

import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/15
 * 描    述：参保人西药目录
 * ================================================
 */
public class InsuredWestCataManaFragment extends BaseRecyclerViewFragment {
    protected TextView mTvNumName,mTvNum;

    protected String listType,listTypeName;
    protected InsuredCataManaActivity catalogueManageActivity;

    //刷新数据
    private ObserverWrapper<String> mObserver = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String tag) {
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.fragment_cata_mana;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mTvNumName= view.findViewById(R.id.tv_num_name);
        mTvNum = view.findViewById(R.id.tv_num);
    }

    @Override
    public void initAdapter() {
        mAdapter = new InsuredWestCataAdapter();
    }

    @Override
    public void initData() {
        listType = getArguments().getString("listType");
        listTypeName= getArguments().getString("listTypeName");
        super.initData();
        MsgBus.sendCataRefresh().observe(this, mObserver);
        catalogueManageActivity = (InsuredCataManaActivity) getActivity();

        mTvNumName.setText("医保目录-"+listTypeName);
        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        WestCataListModel.sendCatalogueRequest(TAG + listType, String.valueOf(mPageNum), "10", catalogueManageActivity == null ? "" : catalogueManageActivity.getSearchText(), new CustomerJsonCallBack<WestCataListModel>() {
            @Override
            public void onRequestError(WestCataListModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                if (mPageNum == 0) {
                    if (mTvNum!=null){
                        mTvNum.setText("0条");
                    }
                }
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(WestCataListModel returnData) {
                hideRefreshView();
                if (mPageNum == 0) {
                    if (mTvNum!=null){
                        mTvNum.setText(returnData.getTotalItems() + "条");
                    }
                }
                List<WestCataListModel.DataBean> infos = returnData.getData();
                if (mAdapter!=null&&infos != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(infos);
                        if (infos.size() <= 0) {
                            mAdapter.isUseEmpty(true);
                        }
                    } else {
                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void requestCancle() {
        OkHttpUtils.getInstance().cancelTag(TAG+listType);//取消以Activity.this作为tag的请求
    }

    public static InsuredWestCataManaFragment newIntance(String listType, String listTypeName) {
        InsuredWestCataManaFragment westCataManaFragment = new InsuredWestCataManaFragment();
        Bundle bundle = new Bundle();
        bundle.putString("listType", listType);
        bundle.putString("listTypeName", listTypeName);
        westCataManaFragment.setArguments(bundle);
        return westCataManaFragment;
    }
}
