package com.jrdz.zhyb_android.ui.insured.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredDiseCataActivity;
import com.jrdz.zhyb_android.ui.insured.adapter.InsuredManteCataAdapter;
import com.jrdz.zhyb_android.ui.insured.adapter.InsuredZYJBCataAdapter;
import com.jrdz.zhyb_android.ui.insured.model.InsuredManteCataModel;
import com.jrdz.zhyb_android.ui.insured.model.InsuredZYJBCataModel;

import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.catalogue.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/23
 * 描    述：参保人中医疾病目录
 * ================================================
 */
public class InsuredZYJBCataFragment extends BaseRecyclerViewFragment {
    protected TextView mTvNumName,mTvNum;

    private String from;
    protected InsuredDiseCataActivity diseCataActivity;

    //刷新数据
    private ObserverWrapper<String> mObserver = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String tag) {
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.fragment_cata_mana;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mTvNumName= view.findViewById(R.id.tv_num_name);
        mTvNum = view.findViewById(R.id.tv_num);

        mTvNumName.setText("目录条数");
    }

    @Override
    public void initAdapter() {
        mAdapter = new InsuredZYJBCataAdapter();
    }

    @Override
    public void initData() {
        from = getArguments().getString("from", "");
        super.initData();
        MsgBus.sendDiseRefresh().observe(this, mObserver);
        diseCataActivity = (InsuredDiseCataActivity) getActivity();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        super.getData();

        InsuredZYJBCataModel.sendDiseCataRequest(TAG , String.valueOf(mPageNum), "20", diseCataActivity == null ? "" : diseCataActivity.getSearchText(),
                new CustomerJsonCallBack<InsuredZYJBCataModel>() {
                    @Override
                    public void onRequestError(InsuredZYJBCataModel returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        if (mPageNum == 0) {
                            if (mTvNum!=null){
                                mTvNum.setText("0条");
                            }
                        }
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(InsuredZYJBCataModel returnData) {
                        hideRefreshView();
                        if (mPageNum == 0) {
                            if (mTvNum!=null){
                                mTvNum.setText(returnData.getTotalItems() + "条");
                            }
                        }
                        List<InsuredZYJBCataModel.DataBean> infos = returnData.getData();
                        if (mAdapter!=null&&infos != null) {
                            if (mPageNum == 0) {
                                mAdapter.setNewData(infos);
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        if ("2".equals(from)) {//返回该项数据
            Intent intent = new Intent();
            intent.putExtra("selectDatas", ((InsuredZYJBCataAdapter) adapter).getItem(position));
            ((InsuredDiseCataActivity) getActivity()).setResult(Activity.RESULT_OK, intent);
            ((InsuredDiseCataActivity) getActivity()).finish();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public static InsuredZYJBCataFragment newIntance(String from) {
        InsuredZYJBCataFragment diseCataFragment = new InsuredZYJBCataFragment();
        Bundle bundle = new Bundle();
        bundle.putString("from", from);
        diseCataFragment.setArguments(bundle);
        return diseCataFragment;
    }
}
