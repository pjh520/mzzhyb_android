package com.jrdz.zhyb_android.ui.insured.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;

import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.route.BaiduMapRoutePlan;
import com.baidu.mapapi.utils.route.RouteParaOption;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.model.KVModel;
import com.jrdz.zhyb_android.ui.insured.adapter.MechanisQueryAdapter;
import com.jrdz.zhyb_android.ui.insured.model.GetAPPAuditModel;
import com.jrdz.zhyb_android.ui.insured.model.MechanisQueryModel;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.ShopDetailActivity;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.location.LocationTool;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-02-14
 * 描    述：机构列表信息
 * ================================================
 */
public class MechanisMedicalQueryFragment extends BaseRecyclerViewFragment {
    protected AppCompatEditText mEtSearch;
    protected FrameLayout mFlClean;
    protected TextView mTvSearch;
    protected LinearLayout mLlDistance;
    protected TextView mTvDistance;
    protected LinearLayout mLlGrade;
    protected TextView mTvGrade;
    protected ImageView mIvGrade;
    protected LinearLayout mLlZoning;
    protected TextView mTvZoning, mTvFindFixmedinsNum;
    protected ImageView mIvZoning;
    protected WheelUtils wheelUtils;
    protected int requestTag = 0;
    protected boolean isCanclick=true;

    protected int choosePos1 = 0, choosePos2 = 0, choosePos3 = 0;
    protected double lat = 0, lon = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public int getLayoutId() {
        return R.layout.actitiy_mechanis_query;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mEtSearch = view.findViewById(R.id.et_search);
        mFlClean = view.findViewById(R.id.fl_clean);
        mTvSearch = view.findViewById(R.id.tv_search);
        mLlDistance = view.findViewById(R.id.ll_distance);
        mTvDistance = view.findViewById(R.id.tv_distance);
        mLlGrade = view.findViewById(R.id.ll_grade);
        mTvGrade = view.findViewById(R.id.tv_grade);
        mIvGrade = view.findViewById(R.id.iv_grade);
        mLlZoning = view.findViewById(R.id.ll_zoning);
        mTvZoning = view.findViewById(R.id.tv_zoning);
        mIvZoning = view.findViewById(R.id.iv_zoning);
        mTvFindFixmedinsNum = view.findViewById(R.id.tv_find_fixmedins_num);
    }

    @Override
    public void initAdapter() {
        mAdapter = new MechanisQueryAdapter();
    }

    @Override
    public void initData() {
        lat=getArguments().getDouble("lat", 0);
        lon=getArguments().getDouble("lon", 0);
        super.initData();
        mTvDistance.setTag("1");

//        List<DataDicModel> datas = LitePal.select("value", "label").where("type=?", "HOSP_LV").order("sort asc").find(DataDicModel.class);
//        Log.e("66666", "initData: "+datas.toString());
        showWaitDialog();
        getAPPAudit();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                    return true;
                }
                return false;
            }
        });

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                }
            }
        });

        mTvSearch.setOnClickListener(this);
        mLlDistance.setOnClickListener(this);
        mLlGrade.setOnClickListener(this);
        mLlZoning.setOnClickListener(this);
        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        super.getData();
        MechanisQueryModel.sendMechanisQueryRequest(TAG, String.valueOf(mPageNum), "10", null == mTvZoning.getTag() ? "" : String.valueOf(mTvZoning.getTag()),
                null == mTvGrade.getTag() ? "" : String.valueOf(mTvGrade.getTag()), mEtSearch.getText().toString(), String.valueOf(lat), String.valueOf(lon),
                null == mTvDistance.getTag() ? "1" : String.valueOf(mTvDistance.getTag()), new CustomerJsonCallBack<MechanisQueryModel>() {
                    @Override
                    public void onRequestError(MechanisQueryModel returnData, String msg) {
                        dissWaitDailog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(MechanisQueryModel returnData) {
                        dissWaitDailog();
                        List<MechanisQueryModel.DataBean> infos = returnData.getData();
                        if (mAdapter!=null&&infos != null) {
                            String hospLvText;
                            for (MechanisQueryModel.DataBean info : infos) {
                                hospLvText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("HOSP_LV", info.getHosp_lv());

                                info.setHospLvText(EmptyUtils.strEmpty(hospLvText));
                            }

                            if (mPageNum == 0) {
                                if (mTvFindFixmedinsNum!=null){
                                    mTvFindFixmedinsNum.setText(EmptyUtils.strEmptyToText(returnData.getTotalItems(), "0"));
                                }
                                mAdapter.setNewData(infos);
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_search://搜索
                if (EmptyUtils.isEmpty(mEtSearch.getText().toString())) {
                    showShortToast("请输入搜索关键词");
                    return;
                }
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.ll_distance://排序
                KeyboardUtils.hideSoftInput(getActivity());
                if (wheelUtils == null) {
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(getContext(), "", choosePos3,
                        CommonlyUsedDataUtils.getInstance().getFixmedinsSort(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                choosePos3 = choosePos;
                                mTvDistance.setTag(dateInfo.getCode());
                                mTvDistance.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                                showWaitDialog();
                                onRefresh(mRefreshLayout);
                            }
                        });
                break;
            case R.id.ll_grade://机构等级
                KeyboardUtils.hideSoftInput(getActivity());
                if (wheelUtils == null) {
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(getContext(), "", choosePos1,
                        CommonlyUsedDataUtils.getInstance().getFixmedinsGrade(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                choosePos1 = choosePos;
                                mTvGrade.setTag(dateInfo.getCode());
                                mTvGrade.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                                showWaitDialog();
                                onRefresh(mRefreshLayout);
                            }
                        });
                break;
            case R.id.ll_zoning://医保区划
                KeyboardUtils.hideSoftInput(getActivity());
                if (wheelUtils == null) {
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(getContext(), "", choosePos2,
                        CommonlyUsedDataUtils.getInstance().getFixmedinsZoning(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                choosePos2 = choosePos;
                                mTvZoning.setTag(dateInfo.getCode());
                                mTvZoning.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                                showWaitDialog();
                                onRefresh(mRefreshLayout);
                            }
                        });
                break;
        }
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        MechanisQueryModel.DataBean itemData = ((MechanisQueryAdapter) adapter).getItem(position);
        //获取app是否在应用市场审核
        if (isCanclick){
            if ("5".equals(itemData.getApproveStatus())) {
                ShopDetailActivity.newIntance(getContext(), itemData.getFixmedins_code());
            }
        }
    }

    //获取app的审核状态
    private void getAPPAudit() {
        GetAPPAuditModel.sendGetAPPAuditRequest(TAG, new CustomerJsonCallBack<GetAPPAuditModel>() {
            @Override
            public void onRequestError(GetAPPAuditModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GetAPPAuditModel returnData) {
                dissWaitDailog();
                GetAPPAuditModel.DataBean info = returnData.getData();
                //1.当版本号小于后台返回的版本号 那么就不需要隐藏
                if (info != null && RxTool.getVersionCode(getContext())>=info.getInteriorVersionNum()) {
                    //（0未审核 1已审核）
                    if (0==info.getAPPApprovalStatus()){
                        isCanclick=false;
                    }else {
                        isCanclick=true;
                    }
                }else {
                    isCanclick=true;
                }
            }
        });
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        MechanisQueryModel.DataBean itemData = ((MechanisQueryAdapter) adapter).getItem(position);
        switch (view.getId()) {
            case R.id.tv_fixmedins_map:
                goBaiduMap(itemData);
                break;
        }
    }

    //跳转百度map
    private void goBaiduMap(MechanisQueryModel.DataBean itemData) {
        //定义起终点坐标（天安门和百度大厦）
        LatLng startPoint = new LatLng(lat, lon);
        LatLng endPoint = new LatLng(itemData.getLatitude(), itemData.getLongitude());
//        LatLng endPoint = new LatLng(37.954322, 109.375443);

        //构建RouteParaOption参数以及策略
        //也可以通过startName和endName来构造
        RouteParaOption paraOption = new RouteParaOption()
                .startPoint(startPoint)
                .endPoint(endPoint)
                .busStrategyType(RouteParaOption.EBusStrategyType.bus_recommend_way);
        //调起百度地图
        try {
            BaiduMapRoutePlan.openBaiduMapDrivingRoute(paraOption, getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //隐藏加载框
    public void dissWaitDailog() {
        requestTag++;
        if (requestTag >= 2) {
            hideRefreshView();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (wheelUtils != null) {
            wheelUtils.dissWheel();
        }

        LocationTool.getInstance().unRegisterLocation();
        //调起结束时及时调用finish方法以释放相关资源
        BaiduMapRoutePlan.finish(getContext());
    }

    public static MechanisMedicalQueryFragment newIntance(double lat, double lon) {
        MechanisMedicalQueryFragment mechanisQueryFragment = new MechanisMedicalQueryFragment();
        Bundle bundle = new Bundle();
        bundle.putDouble("lat", lat);
        bundle.putDouble("lon", lon);
        mechanisQueryFragment.setArguments(bundle);
        return mechanisQueryFragment;
    }
}
