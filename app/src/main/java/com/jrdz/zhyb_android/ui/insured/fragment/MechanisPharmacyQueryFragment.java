package com.jrdz.zhyb_android.ui.insured.fragment;

import android.os.Bundle;

import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.model.MechanisQueryModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-05-21
 * 描    述：
 * ================================================
 */
public class MechanisPharmacyQueryFragment extends MechanisMedicalQueryFragment{

    @Override
    public void getData() {
        MechanisQueryModel.sendAllPharmacyListRequest(TAG, String.valueOf(mPageNum), "10", null == mTvZoning.getTag() ? "" : String.valueOf(mTvZoning.getTag()),
                null == mTvGrade.getTag() ? "" : String.valueOf(mTvGrade.getTag()), mEtSearch.getText().toString(), String.valueOf(lat), String.valueOf(lon),
                null == mTvDistance.getTag() ? "1" : String.valueOf(mTvDistance.getTag()), new CustomerJsonCallBack<MechanisQueryModel>() {
                    @Override
                    public void onRequestError(MechanisQueryModel returnData, String msg) {
                        dissWaitDailog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(MechanisQueryModel returnData) {
                        dissWaitDailog();
                        List<MechanisQueryModel.DataBean> infos = returnData.getData();
                        if (mAdapter!=null&&infos != null) {
                            String hospLvText;
                            for (MechanisQueryModel.DataBean info : infos) {
                                hospLvText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("HOSP_LV", info.getHosp_lv());

                                info.setHospLvText(EmptyUtils.strEmpty(hospLvText));
                            }

                            if (mPageNum == 0) {
                                if (mTvFindFixmedinsNum!=null){
                                    mTvFindFixmedinsNum.setText(EmptyUtils.strEmptyToText(returnData.getTotalItems(), "0"));
                                }
                                mAdapter.setNewData(infos);
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    public static MechanisPharmacyQueryFragment newIntance(double lat, double lon) {
        MechanisPharmacyQueryFragment mechanisQueryFragment = new MechanisPharmacyQueryFragment();
        Bundle bundle = new Bundle();
        bundle.putDouble("lat", lat);
        bundle.putDouble("lon", lon);
        mechanisQueryFragment.setArguments(bundle);
        return mechanisQueryFragment;
    }
}
