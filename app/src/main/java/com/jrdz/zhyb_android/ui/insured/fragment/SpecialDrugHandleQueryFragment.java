package com.jrdz.zhyb_android.ui.insured.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.activity.SpecialDrugRegQueryActivity;
import com.jrdz.zhyb_android.ui.insured.adapter.SpecialDrugRegQueryListAdapter;
import com.jrdz.zhyb_android.ui.insured.model.SpecialDrugUnRegListModel;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.OrderListAdapter_user;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.OrderListFragment_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OrderListModel_user;
import com.zhy.http.okhttp.OkHttpUtils;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-12
 * 描    述：特殊药品备案查询
 * ================================================
 */
public class SpecialDrugHandleQueryFragment extends BaseRecyclerViewFragment {
    private String filingStatus;
    private CustomerDialogUtils customerDialogUtils02;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public void enableDataInitialized() {
        isDataInitialized = false;
    }

    @Override
    public void initAdapter() {
        mAdapter = new SpecialDrugRegQueryListAdapter();
    }

    @Override
    public void initView(View view) {
        super.initView(view);

        mTitleBar.setVisibility(View.GONE);
    }

    @Override
    public void initData() {
        filingStatus = getArguments().getString("FilingStatus", "");
        super.initData();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void getData() {
        super.getData();

        SpecialDrugUnRegListModel.sendSpecialDrugUnRegListRequest(TAG + filingStatus, String.valueOf(mPageNum), "10", filingStatus, new CustomerJsonCallBack<SpecialDrugUnRegListModel>() {
            @Override
            public void onRequestError(SpecialDrugUnRegListModel returnData, String msg) {
                hideRefreshView();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(SpecialDrugUnRegListModel returnData) {
                hideRefreshView();
                List<SpecialDrugUnRegListModel.DataBean> infos = returnData.getData();
                if (mAdapter != null && infos != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(infos);
                        if (infos.size() <= 0) {
                            mAdapter.isUseEmpty(true);
                        }
                    } else {
                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        SpecialDrugUnRegListModel.DataBean itemData = ((SpecialDrugRegQueryListAdapter) adapter).getItem(position);
        switch (view.getId()) {
            case R.id.tv_cancle://撤销
                onCancle(itemData.getId_card_no(),itemData.getSpecialDrugFilingId(), position);
                break;
            case R.id.tv_detail://详情
                Intent intent=new Intent(getContext(),SpecialDrugRegQueryActivity.class);
                intent.putExtra("specialDrugFilingId", itemData.getSpecialDrugFilingId());
                startActivityForResult(intent, new OnActivityCallback() {
                    @Override
                    public void onActivityResult(int resultCode, @Nullable Intent data) {
                        if (resultCode==-1){
                            String specialDrugFilingId=data.getStringExtra("specialDrugFilingId");
                            deleteItem(specialDrugFilingId);
                        }
                    }
                });
                break;
        }
    }

    //删除指定的列表item
    private void deleteItem(String specialDrugFilingId) {
        for (int i = 0, size = mAdapter.getData().size(); i < size; i++) {
            SpecialDrugUnRegListModel.DataBean data = ((SpecialDrugRegQueryListAdapter) mAdapter).getData().get(i);
            if (specialDrugFilingId.equals(data.getSpecialDrugFilingId())) {
                mAdapter.remove(i);
                return;
            }
        }
    }

    //撤销
    private void onCancle(String id_card_no,String SpecialDrugFilingId, int pos) {
        if (customerDialogUtils02 == null) {
            customerDialogUtils02 = new CustomerDialogUtils();
        }
        customerDialogUtils02.showDialog(getContext(), "提示", "是否确认撤销特药备案?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        showWaitDialog();
                        BaseModel.sendRevokeSpecialDrugRequest(TAG, EmptyUtils.strEmpty(id_card_no), SpecialDrugFilingId, new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showTipDialog(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                showShortToast(returnData.getMsg());
                                mAdapter.remove(pos);
                            }
                        });
                    }
                });
    }

    @Override
    protected void requestCancle() {
        OkHttpUtils.getInstance().cancelTag(TAG + filingStatus);//取消以Activity.this作为tag的请求
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (customerDialogUtils02 != null) {
            customerDialogUtils02.onclean();
            customerDialogUtils02 = null;
        }
    }

    public static SpecialDrugHandleQueryFragment newIntance(String filingStatus) {
        SpecialDrugHandleQueryFragment specialDrugHandleQueryFragment = new SpecialDrugHandleQueryFragment();
        Bundle bundle = new Bundle();
        bundle.putString("FilingStatus", filingStatus);
        specialDrugHandleQueryFragment.setArguments(bundle);
        return specialDrugHandleQueryFragment;
    }
}
