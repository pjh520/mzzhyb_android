package com.jrdz.zhyb_android.ui.insured.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-02-17
 * 描    述：
 * ================================================
 */
public class AgencyQueryModel {

    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-02-14 16:23:08
     * data : [{"AccessoryUrl":"","uni_code":"b17f167dd59e45a58c94179b8d83b9f3","phoneType":1,"deviceBrand":"HUAWEI","systemModel":"ANE-AL00","systemVersion":"9","phone":"18098092396","ApproveStatus":0,"EntryMode":0,"MedinsinfoId":17,"admdvs":"610803","fixmedins_code":"H61080300942","fixmedins_name":"横山区塔湾镇小豆湾村卫生室","fixmedins_phone":"18098092396","fixmedins_type":"1","hosp_lv":"11","medinsLv":"9","uscc":"92610823MA70D1513H","CreateDT":"2023-02-02 00:13:10","UpdateDT":"2023-02-02 00:13:10","CreateUser":"admin","UpdateUser":"admin","accessoryId":"2c4d7a84-e071-41f7-a8f6-15eda9486fd4","ElectronicPrescription":0,"OutpatientMdtrtinfo":0,"Unions":"3","VoiceNotification":0}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        private String AgencyName;
        private String admdvs;
        private String Address;
        private String Telephone;
        private String WorkTime;
        private String Remark;

        public String getAgencyName() {
            return AgencyName;
        }

        public void setAgencyName(String agencyName) {
            AgencyName = agencyName;
        }

        public String getAdmdvs() {
            return admdvs;
        }

        public void setAdmdvs(String admdvs) {
            this.admdvs = admdvs;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getTelephone() {
            return Telephone;
        }

        public void setTelephone(String telephone) {
            Telephone = telephone;
        }

        public String getWorkTime() {
            return WorkTime;
        }

        public void setWorkTime(String workTime) {
            WorkTime = workTime;
        }

        public String getRemark() {
            return Remark;
        }

        public void setRemark(String remark) {
            Remark = remark;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.AgencyName);
            dest.writeString(this.admdvs);
            dest.writeString(this.Address);
            dest.writeString(this.Telephone);
            dest.writeString(this.WorkTime);
            dest.writeString(this.Remark);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.AgencyName = in.readString();
            this.admdvs = in.readString();
            this.Address = in.readString();
            this.Telephone = in.readString();
            this.WorkTime = in.readString();
            this.Remark = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //疾病目录列表
    public static void sendAgencyQueryRequest(final String TAG, String pageindex, String pagesize, String admdvs, String name,
                                                final CustomerJsonCallBack<AgencyQueryModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("admdvs", admdvs);//区划
        jsonObject.put("name", name);//搜索关键词

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_QUERYAGENCYLIST_URL, jsonObject.toJSONString(), callback);
    }
}
