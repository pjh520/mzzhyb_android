package com.jrdz.zhyb_android.ui.insured.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-09-30
 * 描    述：
 * ================================================
 */
public class EpaymentUrlModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2024-09-30 11:15:27
     * data : <form name="auto_submit_form" method="post" action="https://gw.open.icbc.com.cn/ui/epayment/project/V1?app_id=10000000000003191244&sign_type=RSA2&charset=UTF-8&format=json&msg_id=3ac21acdab38441998e893725a0c4493&timestamp=2024-09-30+11%3a15%3a27&sign=lQNy3CtSV8pneTXb6ssFzb0Kb16oyJvvKxSBjqHijuleB2UslTSz1dpip6lOMVneU3Gwa1qvZ1oNEfiXe%2f3seqYdAZm1mXGCCdnfv%2bTI%2f4kUBSB85xeufSw3te1axp16SiSQ%2b8Tt%2flimfFcqFd0vNseZLzvuORI%2bJmW2Q1CVgRCzVARL78gcTuVQwyijEs0MiNbuKUY5SP%2fSt5uoGRbFc6vj3XFlUJmSQZnRCLnCZLEkCIYjeOM8hbn6PwPEDsl%2fTo%2bFNr9wvqlSdOF3wpOUQhAyc1CQKBgi7iZTGI85iVmwglfIzdfDdCFL%2bFUdgTdD5%2bXW1JuHy5VcIRiAbSgnJw%3d%3d">
     <input type="hidden" name="biz_content" value="{&quot;computeId&quot;:&quot;22&quot;,&quot;areaCode&quot;:&quot;2610&quot;,&quot;userId&quot;:&quot;111120240930111527&quot;,&quot;payitemCode&quot;:&quot;PJ172017221ASSP03700&quot;,&quot;apiTip&quot;:&quot;&quot;,&quot;returnUrl&quot;:&quot;&quot;,&quot;customInfo&quot;:&quot;&quot;}">
     <input type="submit" value="立刻提交" style="display:none" >
     </form>
     <script>document.forms[0].submit();</script>
     */

    private String code;
    private String msg;
    private String server_time;
    private String data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }


    //获取E缴费医保缴费页面地址
    public static void sendEpaymentUrlRequest(final String TAG, String UserId,final CustomerJsonCallBack<EpaymentUrlModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("computeId", "22");//（客户端标志。21-ios客户端，22-android客户端，10-pc端）
        jsonObject.put("areaCode", "2610");//（榆林是2610）
        jsonObject.put("UserId", UserId);//（手机号或者身份证号）

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_GETEPAYMENTURL_URL, jsonObject.toJSONString(), callback);
    }
}
