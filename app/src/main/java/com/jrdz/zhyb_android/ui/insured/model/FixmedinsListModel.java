package com.jrdz.zhyb_android.ui.insured.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-16
 * 描    述：
 * ================================================
 */
public class FixmedinsListModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2024-04-17 15:45:50
     * data : [{"fixmedins_name":"陕西怡悦大药房连锁有限公司西安西影路连锁二店","accessoryUrl":"","accessoryUrl1":"","thrAccessoryUrl":"","doctorId":3134,"hosp_dept_codg":"","dr_code":"10020115029","dr_name":"贾春利","dr_pwd":"670B14728AD9902AECBA32E22FA4F6BD","dr_type":0,"type":1,"dr_type_name":"","dr_phone":"029-83521221","fixmedins_code":"10020115029","CreateDT":"2024-04-01 11:35:01","UpdateDT":"2024-04-01 11:35:01","CreateUser":"P61083000321","UpdateUser":"P61083000321","accessoryId":"b8ed8e45-3b5a-487d-9286-540432574196","subAccessoryId":"a7a22717-fe4e-4cdb-a4bd-a60d62800d09","IDNumber":"","ApproveStatus":3,"IsEnable":1,"thrAccessoryId":"16ee7603-a75e-4a69-b227-e1bff8c81711","RegistrationId":"","UserID":"","IsOTO":1,"IsOnline":0}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * fixmedins_name : 陕西怡悦大药房连锁有限公司西安西影路连锁二店
         * accessoryUrl :
         * accessoryUrl1 :
         * thrAccessoryUrl :
         * doctorId : 3134
         * hosp_dept_codg :
         * dr_code : 10020115029
         * dr_name : 贾春利
         * dr_pwd : 670B14728AD9902AECBA32E22FA4F6BD
         * dr_type : 0
         * type : 1
         * dr_type_name :
         * dr_phone : 029-83521221
         * fixmedins_code : 10020115029
         * CreateDT : 2024-04-01 11:35:01
         * UpdateDT : 2024-04-01 11:35:01
         * CreateUser : P61083000321
         * UpdateUser : P61083000321
         * accessoryId : b8ed8e45-3b5a-487d-9286-540432574196
         * subAccessoryId : a7a22717-fe4e-4cdb-a4bd-a60d62800d09
         * IDNumber :
         * ApproveStatus : 3
         * IsEnable : 1
         * thrAccessoryId : 16ee7603-a75e-4a69-b227-e1bff8c81711
         * RegistrationId :
         * UserID :
         * IsOTO : 1
         * IsOnline : 0
         */
        private String fixmedins_code;
        private String fixmedins_name;
        private String hosp_dept_codg;
        private String hosp_dept_name;
        private String doctorId;
        private String dr_code;
        private String dr_name;
        private String ProfessionalTitle;

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getFixmedins_name() {
            return fixmedins_name;
        }

        public void setFixmedins_name(String fixmedins_name) {
            this.fixmedins_name = fixmedins_name;
        }

        public String getDoctorId() {
            return doctorId;
        }

        public void setDoctorId(String doctorId) {
            this.doctorId = doctorId;
        }

        public String getHosp_dept_codg() {
            return hosp_dept_codg;
        }

        public void setHosp_dept_codg(String hosp_dept_codg) {
            this.hosp_dept_codg = hosp_dept_codg;
        }

        public String getHosp_dept_name() {
            return hosp_dept_name;
        }

        public void setHosp_dept_name(String hosp_dept_name) {
            this.hosp_dept_name = hosp_dept_name;
        }

        public String getDr_code() {
            return dr_code;
        }

        public void setDr_code(String dr_code) {
            this.dr_code = dr_code;
        }

        public String getDr_name() {
            return dr_name;
        }

        public void setDr_name(String dr_name) {
            this.dr_name = dr_name;
        }

        public String getProfessionalTitle() {
            return ProfessionalTitle;
        }

        public void setProfessionalTitle(String professionalTitle) {
            ProfessionalTitle = professionalTitle;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.fixmedins_code);
            dest.writeString(this.fixmedins_name);
            dest.writeString(this.hosp_dept_codg);
            dest.writeString(this.hosp_dept_name);
            dest.writeString(this.doctorId);
            dest.writeString(this.dr_code);
            dest.writeString(this.dr_name);
            dest.writeString(this.ProfessionalTitle);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.fixmedins_code = in.readString();
            this.fixmedins_name = in.readString();
            this.hosp_dept_codg = in.readString();
            this.hosp_dept_name = in.readString();
            this.doctorId = in.readString();
            this.dr_code = in.readString();
            this.dr_name = in.readString();
            this.ProfessionalTitle = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //特药药品库列表
    public static void sendFixmedinsListRequest(final String TAG, String pageindex,String pagesize,String name, final CustomerJsonCallBack<FixmedinsListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", name);//搜索名称
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_DOCTORLIST_URL, jsonObject.toJSONString(), callback);
    }
}
