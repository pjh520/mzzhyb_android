package com.jrdz.zhyb_android.ui.insured.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023/3/19
 * 描    述：
 * ================================================
 */
public class GetAPPAuditModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-03-19 11:29:29
     * data : {"title":"app更新","content":"新功能更新","externalVersionNum":"V1.0.7","interiorVersionNum":"8","isConstraint":"0","url":"http://113.135.194.23:3080/appinstall/zhyb_sign.apk","APPApprovalStatus":0}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : app更新
         * content : 新功能更新
         * externalVersionNum : V1.0.7
         * interiorVersionNum : 8
         * isConstraint : 0
         * url : http://113.135.194.23:3080/appinstall/zhyb_sign.apk
         * APPApprovalStatus : 0
         */

        private int interiorVersionNum;//版本数
        private int APPApprovalStatus;//（0未审核 1已审核）

        public int getInteriorVersionNum() {
            return interiorVersionNum;
        }

        public void setInteriorVersionNum(int interiorVersionNum) {
            this.interiorVersionNum = interiorVersionNum;
        }

        public int getAPPApprovalStatus() {
            return APPApprovalStatus;
        }

        public void setAPPApprovalStatus(int APPApprovalStatus) {
            this.APPApprovalStatus = APPApprovalStatus;
        }
    }

    //获取app审核状态
    public static void sendGetAPPAuditRequest(final String TAG, final CustomerJsonCallBack<GetAPPAuditModel> callback) {
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_GETAPPVER_URL, "", callback);
    }
}
