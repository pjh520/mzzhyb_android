package com.jrdz.zhyb_android.ui.insured.model;

import com.alibaba.fastjson.JSONObject;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-06-15
 * 描    述：
 * ================================================
 */
public class InsuredAccountModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-06-15 09:58:16
     * totalItems : 0
     * data : {"EnterpriseFundAmount":"100"}
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * EnterpriseFundAmount : 100
         */

        private String EnterpriseFundAmount;

        public String getEnterpriseFundAmount() {
            return EnterpriseFundAmount;
        }

        public void setEnterpriseFundAmount(String EnterpriseFundAmount) {
            this.EnterpriseFundAmount = EnterpriseFundAmount;
        }
    }

    public static void sendInsuredAccountRequest(final String TAG, String IDNumber,
                                                 final CustomerJsonCallBack<InsuredAccountModel> callback) {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("IDNumber", EmptyUtils.strEmpty(IDNumber));

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_GETENTERPRISEFUND_URL, jsonObject.toJSONString(), callback);
    }
}
