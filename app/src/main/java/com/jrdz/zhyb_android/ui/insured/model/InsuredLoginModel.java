package com.jrdz.zhyb_android.ui.insured.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/8
 * 描    述：
 * ================================================
 */
public class InsuredLoginModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-08-10 09:42:59
     * data : {"insuredId":"1","phone":"15060338985","name":"彭俊鸿","isFace":"0","id_card_no":"350321199211272655","accessoryId":"8787a2ce-f3cf-46e7-9606-28b89d182238","accessoryUrl":""}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * insuredId : 1
         * phone : 15060338985
         * name : 彭俊鸿
         * isFace : 0
         * id_card_no : 350321199211272655
         * accessoryId : 8787a2ce-f3cf-46e7-9606-28b89d182238
         * accessoryUrl :
         */

        private String insuredId;
        private String phone;
        private String pwd;
        private String name;
        private String isFace;
        private String id_card_no;
        private String accessoryId;
        private String accessoryUrl;
        private String nickname;
        private String AssociatedDiseases;
        private String sex;
        //是否设置支付密码（0未设置 1设置）
        private String IsSetPaymentPwd;
        //（1是0否）是否支持企业基金支付
        private String IsEnterpriseFundPay;
        private String UserID;//用户的userid

        public String getInsuredId() {
            return insuredId;
        }

        public void setInsuredId(String insuredId) {
            this.insuredId = insuredId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPwd() {
            return pwd;
        }

        public void setPwd(String pwd) {
            this.pwd = pwd;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIsFace() {
            return isFace;
        }

        public void setIsFace(String isFace) {
            this.isFace = isFace;
        }

        public String getId_card_no() {
            return id_card_no;
        }

        public void setId_card_no(String id_card_no) {
            this.id_card_no = id_card_no;
        }

        public String getAccessoryId() {
            return accessoryId;
        }

        public void setAccessoryId(String accessoryId) {
            this.accessoryId = accessoryId;
        }

        public String getAccessoryUrl() {
            return accessoryUrl;
        }

        public void setAccessoryUrl(String accessoryUrl) {
            this.accessoryUrl = accessoryUrl;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getAssociatedDiseases() {
            return AssociatedDiseases;
        }

        public void setAssociatedDiseases(String associatedDiseases) {
            AssociatedDiseases = associatedDiseases;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getIsSetPaymentPwd() {
            return IsSetPaymentPwd;
        }

        public void setIsSetPaymentPwd(String isSetPaymentPwd) {
            IsSetPaymentPwd = isSetPaymentPwd;
        }

        public String getIsEnterpriseFundPay() {
            return IsEnterpriseFundPay;
        }

        public void setIsEnterpriseFundPay(String isEnterpriseFundPay) {
            IsEnterpriseFundPay = isEnterpriseFundPay;
        }

        public String getUserID() {
            return UserID;
        }

        public void setUserID(String userID) {
            UserID = userID;
        }
    }
    //账号密码登录
    public static void sendInsuredLoginRequest(final String TAG, String phone, String pwd,String RegistrationId,
                                               final CustomerJsonCallBack<InsuredLoginModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("phone", phone);
        jsonObject.put("pwd", pwd);
        jsonObject.put("RegistrationId", RegistrationId);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSUREDLOGIN_URL, jsonObject.toJSONString(), callback);
    }


    //验证码登录
    public static void sendInsuredSmsCodeLoginRequest(final String TAG, String phone, String verificationCode,String RegistrationId,
                                                      final CustomerJsonCallBack<InsuredLoginModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("phone", phone);//账号
        jsonObject.put("verificationCode", verificationCode);//验证码
        jsonObject.put("RegistrationId", RegistrationId);//注册号

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSUREDSMSCODELOGIN_URL, jsonObject.toJSONString(), callback);
    }
}
