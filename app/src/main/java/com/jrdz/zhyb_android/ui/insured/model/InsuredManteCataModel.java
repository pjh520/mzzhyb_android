package com.jrdz.zhyb_android.ui.insured.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.catalogue.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/23
 * 描    述：
 * ================================================
 */
public class InsuredManteCataModel {
    /**
     * code : 1
     * msg : 请求成功
     * obj : {"currentPage":1,"resultObj":[{"contents_id":"75593","diag_code":"A33.x00","diag_name":"新生儿破伤风","diag_type":"1"}]}
     */

    private String code;
    private String msg;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {

        /**
         * CatalogueId : 2
         * chr_por_dis_listcode : 998
         * name : 偏执型精神分裂症
         * code : BF20000
         * diag_type : 1
         * diag_typename : 西医主要诊断
         */

        private String CatalogueId;
        private String chr_por_dis_listcode;
        private String name;
        private String code;
        private String diag_type;
        private String diag_typename;
        private String field2;//大类名称
        private String field3;//细分类名称

        public String getCatalogueId() {
            return CatalogueId;
        }

        public void setCatalogueId(String CatalogueId) {
            this.CatalogueId = CatalogueId;
        }

        public String getChr_por_dis_listcode() {
            return chr_por_dis_listcode;
        }

        public void setChr_por_dis_listcode(String chr_por_dis_listcode) {
            this.chr_por_dis_listcode = chr_por_dis_listcode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDiag_type() {
            return diag_type;
        }

        public void setDiag_type(String diag_type) {
            this.diag_type = diag_type;
        }

        public String getDiag_typename() {
            return diag_typename;
        }

        public void setDiag_typename(String diag_typename) {
            this.diag_typename = diag_typename;
        }

        public String getField2() {
            return field2;
        }

        public void setField2(String field2) {
            this.field2 = field2;
        }

        public String getField3() {
            return field3;
        }

        public void setField3(String field3) {
            this.field3 = field3;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.CatalogueId);
            dest.writeString(this.chr_por_dis_listcode);
            dest.writeString(this.name);
            dest.writeString(this.code);
            dest.writeString(this.diag_type);
            dest.writeString(this.diag_typename);
            dest.writeString(this.field2);
            dest.writeString(this.field3);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.CatalogueId = in.readString();
            this.chr_por_dis_listcode = in.readString();
            this.name = in.readString();
            this.code = in.readString();
            this.diag_type = in.readString();
            this.diag_typename = in.readString();
            this.field2 = in.readString();
            this.field3 = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //疾病目录列表
    public static void sendDiseCataRequest(final String TAG, String pageindex, String pagesize, String name,
                                           final CustomerJsonCallBack<InsuredManteCataModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", name);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_OPSPDISECATALOGUE_URL, jsonObject.toJSONString(), callback);
    }
}
