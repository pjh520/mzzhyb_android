package com.jrdz.zhyb_android.ui.insured.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-07-15
 * 描    述：
 * ================================================
 */
public class InsuredNewsModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2024-07-17 10:22:20
     * data : [{"NoticeId":2,"title":"智慧医保App上线啦","url":"","imgurl":"http://113.135.194.23:3081/banner.png","typename":"医保资讯","type":"2","CreateDT":"2022-03-15 00:00:00"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        //唯一标识
        public String NewsId;
        //标题
        public String Title;
        //文章来源
        public String ArticleSource;
        //新闻类型（1、大美米脂 2、政策宣传 3、最新通知）
        public String NewsType;
        //内容
        public String NewsContent;
        //附件标识
        public String NewsAccessoryId;
        //创建日期
        public String CreateDT;
        //创建人
        public String CreateUser;
        //关键字
        public String Keyword;
        //链接
        public String NewsLink;
        //图片地址
        public String NewsUrl;

        public String getNewsId() {
            return NewsId;
        }

        public void setNewsId(String newsId) {
            NewsId = newsId;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getArticleSource() {
            return ArticleSource;
        }

        public void setArticleSource(String articleSource) {
            ArticleSource = articleSource;
        }

        public String getNewsType() {
            return NewsType;
        }

        public void setNewsType(String newsType) {
            NewsType = newsType;
        }

        public String getNewsContent() {
            return NewsContent;
        }

        public void setNewsContent(String newsContent) {
            NewsContent = newsContent;
        }

        public String getNewsAccessoryId() {
            return NewsAccessoryId;
        }

        public void setNewsAccessoryId(String newsAccessoryId) {
            NewsAccessoryId = newsAccessoryId;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String createDT) {
            CreateDT = createDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String createUser) {
            CreateUser = createUser;
        }

        public String getKeyword() {
            return Keyword;
        }

        public void setKeyword(String keyword) {
            Keyword = keyword;
        }

        public String getNewsLink() {
            return NewsLink;
        }

        public void setNewsLink(String newsLink) {
            NewsLink = newsLink;
        }

        public String getNewsUrl() {
            return NewsUrl;
        }

        public void setNewsUrl(String newsUrl) {
            NewsUrl = newsUrl;
        }
    }

    //米脂资讯列表
    public static void sendInsuredNewsRequest(final String TAG, String pageindex,String pagesize,String NewsType, final CustomerJsonCallBack<InsuredNewsModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("NewsType", NewsType);//新闻类型（1、大美米脂 2、政策宣传 3、最新通知）
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_NEWSLIST_URL, jsonObject.toJSONString(), callback);
    }
}
