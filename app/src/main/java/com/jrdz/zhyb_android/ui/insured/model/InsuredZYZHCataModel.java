package com.jrdz.zhyb_android.ui.insured.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.catalogue.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/23
 * 描    述：
 * ================================================
 */
public class InsuredZYZHCataModel {
    /**
     * code : 1
     * msg : 请求成功
     * obj : {"currentPage":1,"resultObj":[{"contents_id":"75593","diag_code":"A33.x00","diag_name":"新生儿破伤风","diag_type":"1"}]}
     */

    private String code;
    private String msg;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {

        /**
         * TCMSyndromeCatalogueId : 20
         * tcm_syndrome : cb9110181515b16801b0ba89571e953b
         * name : 湿邪内蕴证(可选词：里湿证；内湿证)
         * code : B02.05.01.02.
         * diag_type : 4
         * diag_typename : 中医主证诊断
         */

        private String TCMSyndromeCatalogueId;
        private String tcm_syndrome;//中医证候ID
        private String name;// 病种分类名称
        private String code;
        private String diag_type;
        private String diag_typename;
        private String field3;//科别类目名称
        private String field20;//唯一记录号

        public String getTCMSyndromeCatalogueId() {
            return TCMSyndromeCatalogueId;
        }

        public void setTCMSyndromeCatalogueId(String TCMSyndromeCatalogueId) {
            this.TCMSyndromeCatalogueId = TCMSyndromeCatalogueId;
        }

        public String getTcm_syndrome() {
            return tcm_syndrome;
        }

        public void setTcm_syndrome(String tcm_syndrome) {
            this.tcm_syndrome = tcm_syndrome;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDiag_type() {
            return diag_type;
        }

        public void setDiag_type(String diag_type) {
            this.diag_type = diag_type;
        }

        public String getDiag_typename() {
            return diag_typename;
        }

        public void setDiag_typename(String diag_typename) {
            this.diag_typename = diag_typename;
        }

        public String getField3() {
            return field3;
        }

        public void setField3(String field3) {
            this.field3 = field3;
        }

        public String getField20() {
            return field20;
        }

        public void setField20(String field20) {
            this.field20 = field20;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.TCMSyndromeCatalogueId);
            dest.writeString(this.tcm_syndrome);
            dest.writeString(this.name);
            dest.writeString(this.code);
            dest.writeString(this.diag_type);
            dest.writeString(this.diag_typename);
            dest.writeString(this.field3);
            dest.writeString(this.field20);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.TCMSyndromeCatalogueId = in.readString();
            this.tcm_syndrome = in.readString();
            this.name = in.readString();
            this.code = in.readString();
            this.diag_type = in.readString();
            this.diag_typename = in.readString();
            this.field3 = in.readString();
            this.field20 = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //疾病目录列表
    public static void sendDiseCataRequest(final String TAG, String pageindex, String pagesize, String name,
                                           final CustomerJsonCallBack<InsuredZYZHCataModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", name);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_TCMSYNDROMECATALOGUE_URL, jsonObject.toJSONString(), callback);
    }
}
