package com.jrdz.zhyb_android.ui.insured.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-02-14
 * 描    述：
 * ================================================
 */
public class MechanisQueryModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-02-14 16:23:08
     * data : [{"AccessoryUrl":"","uni_code":"b17f167dd59e45a58c94179b8d83b9f3","phoneType":1,"deviceBrand":"HUAWEI","systemModel":"ANE-AL00","systemVersion":"9","phone":"18098092396","ApproveStatus":0,"EntryMode":0,"MedinsinfoId":17,"admdvs":"610803","fixmedins_code":"H61080300942","fixmedins_name":"横山区塔湾镇小豆湾村卫生室","fixmedins_phone":"18098092396","fixmedins_type":"1","hosp_lv":"11","medinsLv":"9","uscc":"92610823MA70D1513H","CreateDT":"2023-02-02 00:13:10","UpdateDT":"2023-02-02 00:13:10","CreateUser":"admin","UpdateUser":"admin","accessoryId":"2c4d7a84-e071-41f7-a8f6-15eda9486fd4","ElectronicPrescription":0,"OutpatientMdtrtinfo":0,"Unions":"3","VoiceNotification":0}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * AccessoryUrl :
         * uni_code : b17f167dd59e45a58c94179b8d83b9f3
         * phoneType : 1
         * deviceBrand : HUAWEI
         * systemModel : ANE-AL00
         * systemVersion : 9
         * phone : 18098092396
         * ApproveStatus : 0
         * EntryMode : 0
         * MedinsinfoId : 17
         * admdvs : 610803
         * fixmedins_code : H61080300942
         * fixmedins_name : 横山区塔湾镇小豆湾村卫生室
         * fixmedins_phone : 18098092396
         * fixmedins_type : 1
         * hosp_lv : 11
         * medinsLv : 9
         * uscc : 92610823MA70D1513H
         * CreateDT : 2023-02-02 00:13:10
         * UpdateDT : 2023-02-02 00:13:10
         * CreateUser : admin
         * UpdateUser : admin
         * accessoryId : 2c4d7a84-e071-41f7-a8f6-15eda9486fd4
         * ElectronicPrescription : 0
         * OutpatientMdtrtinfo : 0
         * Unions : 3
         * VoiceNotification : 0
         */

        private String AccessoryUrl;
        private String uni_code;
        private String phoneType;
        private String deviceBrand;
        private String systemModel;
        private String systemVersion;
        private String phone;
        private String ApproveStatus;
        private String EntryMode;
        private String MedinsinfoId;
        private String admdvs;
        private String fixmedins_code;
        private String fixmedins_name;
        private String fixmedins_phone;
        private String fixmedins_type;
        private String hosp_lv;
        private String hospLvText;
        private String medinsLv;
        private String uscc;
        private String CreateDT;
        private String UpdateDT;
        private String CreateUser;
        private String UpdateUser;
        private String accessoryId;
        private String ElectronicPrescription;
        private String OutpatientMdtrtinfo;
        private String Unions;
        private String VoiceNotification;
        private String Address;
        private double Latitude;
        private double Longitude;
        private double Distance;

        public String getAccessoryUrl() {
            return AccessoryUrl;
        }

        public void setAccessoryUrl(String AccessoryUrl) {
            this.AccessoryUrl = AccessoryUrl;
        }

        public String getUni_code() {
            return uni_code;
        }

        public void setUni_code(String uni_code) {
            this.uni_code = uni_code;
        }

        public String getPhoneType() {
            return phoneType;
        }

        public void setPhoneType(String phoneType) {
            this.phoneType = phoneType;
        }

        public String getDeviceBrand() {
            return deviceBrand;
        }

        public void setDeviceBrand(String deviceBrand) {
            this.deviceBrand = deviceBrand;
        }

        public String getSystemModel() {
            return systemModel;
        }

        public void setSystemModel(String systemModel) {
            this.systemModel = systemModel;
        }

        public String getSystemVersion() {
            return systemVersion;
        }

        public void setSystemVersion(String systemVersion) {
            this.systemVersion = systemVersion;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getApproveStatus() {
            return ApproveStatus;
        }

        public void setApproveStatus(String ApproveStatus) {
            this.ApproveStatus = ApproveStatus;
        }

        public String getEntryMode() {
            return EntryMode;
        }

        public void setEntryMode(String EntryMode) {
            this.EntryMode = EntryMode;
        }

        public String getMedinsinfoId() {
            return MedinsinfoId;
        }

        public void setMedinsinfoId(String MedinsinfoId) {
            this.MedinsinfoId = MedinsinfoId;
        }

        public String getAdmdvs() {
            return admdvs;
        }

        public void setAdmdvs(String admdvs) {
            this.admdvs = admdvs;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getFixmedins_name() {
            return fixmedins_name;
        }

        public void setFixmedins_name(String fixmedins_name) {
            this.fixmedins_name = fixmedins_name;
        }

        public String getFixmedins_phone() {
            return fixmedins_phone;
        }

        public void setFixmedins_phone(String fixmedins_phone) {
            this.fixmedins_phone = fixmedins_phone;
        }

        public String getFixmedins_type() {
            return fixmedins_type;
        }

        public void setFixmedins_type(String fixmedins_type) {
            this.fixmedins_type = fixmedins_type;
        }

        public String getHosp_lv() {
            return hosp_lv;
        }

        public void setHosp_lv(String hosp_lv) {
            this.hosp_lv = hosp_lv;
        }

        public String getHospLvText() {
            return hospLvText;
        }

        public void setHospLvText(String hospLvText) {
            this.hospLvText = hospLvText;
        }

        public String getMedinsLv() {
            return medinsLv;
        }

        public void setMedinsLv(String medinsLv) {
            this.medinsLv = medinsLv;
        }

        public String getUscc() {
            return uscc;
        }

        public void setUscc(String uscc) {
            this.uscc = uscc;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getUpdateDT() {
            return UpdateDT;
        }

        public void setUpdateDT(String UpdateDT) {
            this.UpdateDT = UpdateDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String CreateUser) {
            this.CreateUser = CreateUser;
        }

        public String getUpdateUser() {
            return UpdateUser;
        }

        public void setUpdateUser(String UpdateUser) {
            this.UpdateUser = UpdateUser;
        }

        public String getAccessoryId() {
            return accessoryId;
        }

        public void setAccessoryId(String accessoryId) {
            this.accessoryId = accessoryId;
        }

        public String getElectronicPrescription() {
            return ElectronicPrescription;
        }

        public void setElectronicPrescription(String ElectronicPrescription) {
            this.ElectronicPrescription = ElectronicPrescription;
        }

        public String getOutpatientMdtrtinfo() {
            return OutpatientMdtrtinfo;
        }

        public void setOutpatientMdtrtinfo(String OutpatientMdtrtinfo) {
            this.OutpatientMdtrtinfo = OutpatientMdtrtinfo;
        }

        public String getUnions() {
            return Unions;
        }

        public void setUnions(String Unions) {
            this.Unions = Unions;
        }

        public String getVoiceNotification() {
            return VoiceNotification;
        }

        public void setVoiceNotification(String VoiceNotification) {
            this.VoiceNotification = VoiceNotification;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public double getLatitude() {
            return Latitude;
        }

        public void setLatitude(double latitude) {
            Latitude = latitude;
        }

        public double getLongitude() {
            return Longitude;
        }

        public void setLongitude(double longitude) {
            Longitude = longitude;
        }

        public double getDistance() {
            return Distance;
        }

        public void setDistance(double distance) {
            Distance = distance;
        }
    }

    //获取所有医疗机构接口
    public static void sendMechanisQueryRequest(final String TAG, String pageindex, String pagesize, String admdvs, String hosp_lv, String name,
                                                String latitude,String longitude,String sort,
                                                final CustomerJsonCallBack<MechanisQueryModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("admdvs", admdvs);//区划
        jsonObject.put("hosp_lv", hosp_lv);//医院等级
        jsonObject.put("name", name);//搜索关键词
        jsonObject.put("Latitude", latitude);//纬度
        jsonObject.put("Longitude", longitude);//经度
        jsonObject.put("sort", sort);//(1按距离2按机构）

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_QUERYMEDINSINFOLIST_URL, jsonObject.toJSONString(), callback);
    }

    //获取所有药店接口
    public static void sendAllPharmacyListRequest(final String TAG, String pageindex, String pagesize, String admdvs, String hosp_lv, String name,
                                                  String latitude,String longitude,String sort,
                                                  final CustomerJsonCallBack<MechanisQueryModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("admdvs", admdvs);//区划
        jsonObject.put("hosp_lv", hosp_lv);//医院等级
        jsonObject.put("name", name);//搜索关键词
        jsonObject.put("Latitude", latitude);//纬度
        jsonObject.put("Longitude", longitude);//经度
        jsonObject.put("sort", sort);//(1按距离2按机构）

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_QUERYALLPHARMACYLIST_URL, jsonObject.toJSONString(), callback);
    }
}
