package com.jrdz.zhyb_android.ui.insured.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-07-26
 * 描    述：
 * ================================================
 */
public class PersonalInsuInfoQueryModel {
    /**
     * code : 1
     * msg : 请求成功
     * obj : {"opspDise":{},"perInfo":{"baseinfo":{"age":55.3,"brdy":"1966-09-21","certno":"612726196609210011","gend":"1","naty":"01","psn_cert_type":"01","psn_name":"宋怀春","psn_no":"61000006000000000010866022"},"idetinfo":[],"insuinfo":[{"balc":6336.9,"cvlserv_flag":"1","emp_name":"定边县农村合作医疗管理办公室","insuplc_admdvs":"610825","insutype":"310","psn_insu_date":"2020-01-01","psn_insu_stas":"1","psn_type":"1101"}]}}
     */

    private String code;
    private String msg;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * baseinfo : {"age":45.8,"brdy":"1976-11-11","certno":"61272419761111061X","gend":"1","naty":"01","psn_cert_type":"01","psn_name":"陈子平","psn_no":"61000006000000000017740912"}
         * idetinfo : [{"begntime":"2021-12-07 00:00:00","endtime":"2015-12-31 00:00:00","memo":"matIdetCode","psn_idet_type":"336036","psn_type_lv":null}]
         * insuinfo : [{"balc":0,"cvlserv_flag":"0","emp_name":"小豆湾村委会","insuplc_admdvs":"610803","insutype":"390","paus_insu_date":null,"psn_insu_date":"2021-01-01","psn_insu_stas":"1","psn_type":"1501"}]
         */

        private BaseinfoBean baseinfo;
        private List<IdetinfoBean> idetinfo;
        private List<InsuinfoBean> insuinfo;

        public BaseinfoBean getBaseinfo() {
            return baseinfo;
        }

        public void setBaseinfo(BaseinfoBean baseinfo) {
            this.baseinfo = baseinfo;
        }

        public List<IdetinfoBean> getIdetinfo() {
            return idetinfo;
        }

        public void setIdetinfo(List<IdetinfoBean> idetinfo) {
            this.idetinfo = idetinfo;
        }

        public List<InsuinfoBean> getInsuinfo() {
            return insuinfo;
        }

        public void setInsuinfo(List<InsuinfoBean> insuinfo) {
            this.insuinfo = insuinfo;
        }

        public static class BaseinfoBean implements Parcelable {
            /**
             * age : 45.8
             * brdy : 1976-11-11
             * certno : 61272419761111061X
             * gend : 1
             * naty : 01
             * psn_cert_type : 01
             * psn_name : 陈子平
             * psn_no : 61000006000000000017740912
             */

            private String age;
            private String brdy;
            private String certno;
            private String gend;
            private String naty;
            private String psn_cert_type;
            private String psn_name;
            private String psn_no;

            public String getAge() {
                return age;
            }

            public void setAge(String age) {
                this.age = age;
            }

            public String getBrdy() {
                return brdy;
            }

            public void setBrdy(String brdy) {
                this.brdy = brdy;
            }

            public String getCertno() {
                return certno;
            }

            public void setCertno(String certno) {
                this.certno = certno;
            }

            public String getGend() {
                return gend;
            }

            public void setGend(String gend) {
                this.gend = gend;
            }

            public String getNaty() {
                return naty;
            }

            public void setNaty(String naty) {
                this.naty = naty;
            }

            public String getPsn_cert_type() {
                return psn_cert_type;
            }

            public void setPsn_cert_type(String psn_cert_type) {
                this.psn_cert_type = psn_cert_type;
            }

            public String getPsn_name() {
                return psn_name;
            }

            public void setPsn_name(String psn_name) {
                this.psn_name = psn_name;
            }

            public String getPsn_no() {
                return psn_no;
            }

            public void setPsn_no(String psn_no) {
                this.psn_no = psn_no;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.age);
                dest.writeString(this.brdy);
                dest.writeString(this.certno);
                dest.writeString(this.gend);
                dest.writeString(this.naty);
                dest.writeString(this.psn_cert_type);
                dest.writeString(this.psn_name);
                dest.writeString(this.psn_no);
            }

            public BaseinfoBean() {
            }

            protected BaseinfoBean(Parcel in) {
                this.age = in.readString();
                this.brdy = in.readString();
                this.certno = in.readString();
                this.gend = in.readString();
                this.naty = in.readString();
                this.psn_cert_type = in.readString();
                this.psn_name = in.readString();
                this.psn_no = in.readString();
            }

            public static final Parcelable.Creator<BaseinfoBean> CREATOR = new Parcelable.Creator<BaseinfoBean>() {
                @Override
                public BaseinfoBean createFromParcel(Parcel source) {
                    return new BaseinfoBean(source);
                }

                @Override
                public BaseinfoBean[] newArray(int size) {
                    return new BaseinfoBean[size];
                }
            };
        }

        public static class IdetinfoBean implements Parcelable {
            /**
             * begntime : 2021-12-07 00:00:00
             * endtime : 2015-12-31 00:00:00
             * memo : matIdetCode
             * psn_idet_type : 336036
             * psn_type_lv : null
             */

            private String begntime;
            private String endtime;
            private String memo;
            private String psn_idet_type;
            private String psn_type_lv;

            public String getBegntime() {
                return begntime;
            }

            public void setBegntime(String begntime) {
                this.begntime = begntime;
            }

            public String getEndtime() {
                return endtime;
            }

            public void setEndtime(String endtime) {
                this.endtime = endtime;
            }

            public String getMemo() {
                return memo;
            }

            public void setMemo(String memo) {
                this.memo = memo;
            }

            public String getPsn_idet_type() {
                return psn_idet_type;
            }

            public void setPsn_idet_type(String psn_idet_type) {
                this.psn_idet_type = psn_idet_type;
            }

            public String getPsn_type_lv() {
                return psn_type_lv;
            }

            public void setPsn_type_lv(String psn_type_lv) {
                this.psn_type_lv = psn_type_lv;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.begntime);
                dest.writeString(this.endtime);
                dest.writeString(this.memo);
                dest.writeString(this.psn_idet_type);
                dest.writeString(this.psn_type_lv);
            }

            public IdetinfoBean() {
            }

            protected IdetinfoBean(Parcel in) {
                this.begntime = in.readString();
                this.endtime = in.readString();
                this.memo = in.readString();
                this.psn_idet_type = in.readString();
                this.psn_type_lv = in.readString();
            }

            public static final Parcelable.Creator<IdetinfoBean> CREATOR = new Parcelable.Creator<IdetinfoBean>() {
                @Override
                public IdetinfoBean createFromParcel(Parcel source) {
                    return new IdetinfoBean(source);
                }

                @Override
                public IdetinfoBean[] newArray(int size) {
                    return new IdetinfoBean[size];
                }
            };
        }

        public static class InsuinfoBean implements Parcelable {
            /**
             * balc : 0
             * cvlserv_flag : 0
             * emp_name : 小豆湾村委会
             * insuplc_admdvs : 610803
             * insutype : 390
             * paus_insu_date : null
             * psn_insu_date : 2021-01-01
             * psn_insu_stas : 1
             * psn_type : 1501
             */

            private String balc;
            private String cvlserv_flag;
            private String emp_name;
            private String insuplc_admdvs;
            private String insutype;
            private String paus_insu_date;
            private String psn_insu_date;
            private String psn_insu_stas;
            private String psn_type;
            private boolean isOpen=true;

            public String getBalc() {
                return balc;
            }

            public void setBalc(String balc) {
                this.balc = balc;
            }

            public String getCvlserv_flag() {
                return cvlserv_flag;
            }

            public void setCvlserv_flag(String cvlserv_flag) {
                this.cvlserv_flag = cvlserv_flag;
            }

            public String getEmp_name() {
                return emp_name;
            }

            public void setEmp_name(String emp_name) {
                this.emp_name = emp_name;
            }

            public String getInsuplc_admdvs() {
                return insuplc_admdvs;
            }

            public void setInsuplc_admdvs(String insuplc_admdvs) {
                this.insuplc_admdvs = insuplc_admdvs;
            }

            public String getInsutype() {
                return insutype;
            }

            public void setInsutype(String insutype) {
                this.insutype = insutype;
            }

            public String getPaus_insu_date() {
                return paus_insu_date;
            }

            public void setPaus_insu_date(String paus_insu_date) {
                this.paus_insu_date = paus_insu_date;
            }

            public String getPsn_insu_date() {
                return psn_insu_date;
            }

            public void setPsn_insu_date(String psn_insu_date) {
                this.psn_insu_date = psn_insu_date;
            }

            public String getPsn_insu_stas() {
                return psn_insu_stas;
            }

            public void setPsn_insu_stas(String psn_insu_stas) {
                this.psn_insu_stas = psn_insu_stas;
            }

            public String getPsn_type() {
                return psn_type;
            }

            public void setPsn_type(String psn_type) {
                this.psn_type = psn_type;
            }

            public boolean isOpen() {
                return isOpen;
            }

            public void setOpen(boolean open) {
                isOpen = open;
            }


            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.balc);
                dest.writeString(this.cvlserv_flag);
                dest.writeString(this.emp_name);
                dest.writeString(this.insuplc_admdvs);
                dest.writeString(this.insutype);
                dest.writeString(this.paus_insu_date);
                dest.writeString(this.psn_insu_date);
                dest.writeString(this.psn_insu_stas);
                dest.writeString(this.psn_type);
                dest.writeByte(this.isOpen ? (byte) 1 : (byte) 0);
            }

            public InsuinfoBean() {
            }

            protected InsuinfoBean(Parcel in) {
                this.balc = in.readString();
                this.cvlserv_flag = in.readString();
                this.emp_name = in.readString();
                this.insuplc_admdvs = in.readString();
                this.insutype = in.readString();
                this.paus_insu_date = in.readString();
                this.psn_insu_date = in.readString();
                this.psn_insu_stas = in.readString();
                this.psn_type = in.readString();
                this.isOpen = in.readByte() != 0;
            }

            public static final Parcelable.Creator<InsuinfoBean> CREATOR = new Parcelable.Creator<InsuinfoBean>() {
                @Override
                public InsuinfoBean createFromParcel(Parcel source) {
                    return new InsuinfoBean(source);
                }

                @Override
                public InsuinfoBean[] newArray(int size) {
                    return new InsuinfoBean[size];
                }
            };
        }
    }

    //疾病目录列表
    public static void sendPersonalInsuInfoQueryRequest(final String TAG, String mdtrt_cert_type, String mdtrt_cert_no,
                                           final CustomerJsonCallBack<PersonalInsuInfoQueryModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("mdtrt_cert_type", mdtrt_cert_type);
        jsonObject.put("mdtrt_cert_no", mdtrt_cert_no);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_PERSONNELINFO_URL, jsonObject.toJSONString(), callback);
    }
}
