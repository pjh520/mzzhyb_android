package com.jrdz.zhyb_android.ui.insured.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-05-14
 * 描    述：
 * ================================================
 */
public class SlowCarePharmacyModel {


    /**
     * code : 1
     * msg : 成功
     * server_time : 2024-05-14 15:25:21
     * totalItems : 81
     * data : [{"Distance":12905218,"OpspDisePharmacyId":1,"admdvsName":"市管","fixmedins_name":"西沙大药房","fixmedins_phone":"15529937999","Address":"人民中路三中大门西侧102号","Batch":"第一批","Latitude":0,"Longitude":0}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * Distance : 12905218
         * OpspDisePharmacyId : 1
         * admdvsName : 市管
         * fixmedins_name : 西沙大药房
         * fixmedins_phone : 15529937999
         * Address : 人民中路三中大门西侧102号
         * Batch : 第一批
         * Latitude : 0
         * Longitude : 0
         */

        private double Distance;
        private String OpspDisePharmacyId;
        private String admdvsName;
        private String fixmedins_name;
        private String fixmedins_phone;
        private String Address;
        private String Batch;
        private String Latitude;
        private String Longitude;

        public double getDistance() {
            return Distance;
        }

        public void setDistance(double Distance) {
            this.Distance = Distance;
        }

        public String getOpspDisePharmacyId() {
            return OpspDisePharmacyId;
        }

        public void setOpspDisePharmacyId(String OpspDisePharmacyId) {
            this.OpspDisePharmacyId = OpspDisePharmacyId;
        }

        public String getAdmdvsName() {
            return admdvsName;
        }

        public void setAdmdvsName(String admdvsName) {
            this.admdvsName = admdvsName;
        }

        public String getFixmedins_name() {
            return fixmedins_name;
        }

        public void setFixmedins_name(String fixmedins_name) {
            this.fixmedins_name = fixmedins_name;
        }

        public String getFixmedins_phone() {
            return fixmedins_phone;
        }

        public void setFixmedins_phone(String fixmedins_phone) {
            this.fixmedins_phone = fixmedins_phone;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getBatch() {
            return Batch;
        }

        public void setBatch(String Batch) {
            this.Batch = Batch;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }
    }

    //慢保药店查询
    public static void sendSlowCarePharmacyRequest(final String TAG, String pageindex, String pagesize, String name, String sort, String Batch, String admdvsName, String latitude, String longitude,
                                                   final CustomerJsonCallBack<SlowCarePharmacyModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", name);//搜索关键词
        jsonObject.put("sort", sort);//（1、远到近 2 近到远）
        jsonObject.put("Batch", Batch);//定点批次
        jsonObject.put("admdvsName", admdvsName);//区划
        jsonObject.put("Latitude", latitude);//纬度
        jsonObject.put("Longitude", longitude);//经度

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_QUERYOPSPDISEPHARMACYLIST_URL, jsonObject.toJSONString(), callback);
    }

    //门诊统筹定点机构查询
    public static void sendQueryCoordinationMedinsListRequest(final String TAG, String pageindex, String pagesize, String name, String sort,String admdvsName, String latitude, String longitude,
                                                   final CustomerJsonCallBack<SlowCarePharmacyModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", name);//搜索关键词
        jsonObject.put("sort", sort);//（1、远到近 2 近到远）
        jsonObject.put("admdvsName", admdvsName);//区划
        jsonObject.put("Latitude", latitude);//纬度
        jsonObject.put("Longitude", longitude);//经度

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_QUERYCOORDINATIONMEDINSLIST_URL, jsonObject.toJSONString(), callback);
    }

    //门诊统筹定点药店查询
    public static void sendQueryCoordinationPharmacyListRequest(final String TAG, String pageindex, String pagesize, String name, String sort,String latitude, String longitude,
                                                              final CustomerJsonCallBack<SlowCarePharmacyModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", name);//搜索关键词
        jsonObject.put("sort", sort);//（1、远到近 2 近到远）
        jsonObject.put("Latitude", latitude);//纬度
        jsonObject.put("Longitude", longitude);//经度

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_QUERYCOORDINATIONPHARMACYLIST_URL, jsonObject.toJSONString(), callback);
    }
}
