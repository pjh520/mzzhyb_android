package com.jrdz.zhyb_android.ui.insured.model;
import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;
import com.jrdz.zhyb_android.net.RequestData;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredPersonalActivity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-08-16
 * 描    述：
 * ================================================
 */
public class SmrzInitDataModel {

    private String code;
    private String msg;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private String certify_id;
        private String certify_url;

        public String getCertify_id() {
            return certify_id;
        }

        public void setCertify_id(String certify_id) {
            this.certify_id = certify_id;
        }

        public String getCertify_url() {
            return certify_url;
        }

        public void setCertify_url(String certify_url) {
            this.certify_url = certify_url;
        }
    }


    //实名认证初始化数据
    public static void sendSmrzInitDataRequest(final String TAG, String cert_name, String cert_no,
                                                        final CustomerJsonCallBack<SmrzInitDataModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("biz_code", "DATA_DIGITAL_BIZ_CODE_FACE_VERIFICATION");
        jsonObject.put("cert_name", cert_name);
        jsonObject.put("cert_no", cert_no);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_FACECERTIFY_URL, jsonObject.toJSONString(), callback);
    }
}
