package com.jrdz.zhyb_android.ui.insured.model;
import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredPersonalActivity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-08-16
 * 描    述：
 * ================================================
 */
public class SmrzResultModel {

    private String code;
    private String msg;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private String passed;

        public String getPassed() {
            return passed;
        }

        public void setPassed(String passed) {
            this.passed = passed;
        }
    }


    //实名认证结果查询
    public static void sendSmrzResultRequest(final String TAG, String certify_id, final CustomerJsonCallBack<SmrzResultModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("certify_id", certify_id);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_QUERYFACECERTIFY_URL, jsonObject.toJSONString(), callback);
    }
}
