package com.jrdz.zhyb_android.ui.insured.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-18
 * 描    述：
 * ================================================
 */
public class SpecialDrugDetailModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2024-04-18 08:54:46
     * data : {"SpecialDrugFilingId":1,"dr_code":"10020115029","dr_name":"贾春利","fixmedins_code":"10020115029","fixmedins_name":"陕西怡悦大药房连锁有限公司西安西影路连锁二店","phone":"15060338985","name":"宋怀春","id_card_no":"612726196609210011","MIC_Code":"X6100000000000000000001","ItemName":"苯丙酮尿症替代食品","begintime":"2024-04-17 00:00:00","endtime":"2024-06-17 00:00:00","ApplicationFormAccessoryId1":"0c1a5a33-5d85-4b77-bd9b-a3d81e9b6c19","PrescriptionAccessoryId1":"1912febd-038c-4472-a8a5-b0691f89a32f","DiagnosisAccessoryId1":"5cd37a1f-c9e1-4c30-b188-b199538ed75f","CaseAccessoryId1":"a7417861-3cf4-4537-96bf-a21b234be874","CreateDT":"2024-04-17 17:18:12","FilingUpdateDT":"2024-04-17 17:18:12","CreateUser":"15060338985","FilingUpdateUser":"15060338985","FilingStatus":1,"RevokeStatus":0,"RevokeReason":"","ReviewComments":"","RevokeUpdateDT":"2024-04-17 17:18:12","RevokeUpdateUser":"15060338985","FilingType":1,"RevokeReviewComments":"","ApplicationFormAccessoryUrl1":[{"AccessoryUrl":"~/App_Upload/Storage/Medicare_Ticket/0c1a5a33-5d85-4b77-bd9b-a3d81e9b6c19/62458a99-4e3c-462e-98e9-dbde9b3a71ab.jpg","AccessoryVerId":"62458a99-4e3c-462e-98e9-dbde9b3a71ab"}],"PrescriptionAccessoryUrl1":[{"AccessoryUrl":"~/App_Upload/Storage/Medicare_Ticket/1912febd-038c-4472-a8a5-b0691f89a32f/62dd8f59-c749-4577-a7dc-cb8b40602cef.jpg","AccessoryVerId":"62dd8f59-c749-4577-a7dc-cb8b40602cef"}],"DiagnosisAccessoryUrl1":[{"AccessoryUrl":"~/App_Upload/Storage/Medicare_Ticket/5cd37a1f-c9e1-4c30-b188-b199538ed75f/0a53a2d1-4815-4779-8c7b-2a7e79822669.jpg","AccessoryVerId":"0a53a2d1-4815-4779-8c7b-2a7e79822669"}],"CaseAccessoryUrl1":[]}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * SpecialDrugFilingId : 1
         * dr_code : 10020115029
         * dr_name : 贾春利
         * fixmedins_code : 10020115029
         * fixmedins_name : 陕西怡悦大药房连锁有限公司西安西影路连锁二店
         * phone : 15060338985
         * name : 宋怀春
         * id_card_no : 612726196609210011
         * MIC_Code : X6100000000000000000001
         * ItemName : 苯丙酮尿症替代食品
         * begintime : 2024-04-17 00:00:00
         * endtime : 2024-06-17 00:00:00
         * ApplicationFormAccessoryId1 : 0c1a5a33-5d85-4b77-bd9b-a3d81e9b6c19
         * PrescriptionAccessoryId1 : 1912febd-038c-4472-a8a5-b0691f89a32f
         * DiagnosisAccessoryId1 : 5cd37a1f-c9e1-4c30-b188-b199538ed75f
         * CaseAccessoryId1 : a7417861-3cf4-4537-96bf-a21b234be874
         * CreateDT : 2024-04-17 17:18:12
         * FilingUpdateDT : 2024-04-17 17:18:12
         * CreateUser : 15060338985
         * FilingUpdateUser : 15060338985
         * FilingStatus : 1
         * RevokeStatus : 0
         * RevokeReason :
         * ReviewComments :
         * RevokeUpdateDT : 2024-04-17 17:18:12
         * RevokeUpdateUser : 15060338985
         * FilingType : 1
         * RevokeReviewComments :
         * ApplicationFormAccessoryUrl1 : [{"AccessoryUrl":"~/App_Upload/Storage/Medicare_Ticket/0c1a5a33-5d85-4b77-bd9b-a3d81e9b6c19/62458a99-4e3c-462e-98e9-dbde9b3a71ab.jpg","AccessoryVerId":"62458a99-4e3c-462e-98e9-dbde9b3a71ab"}]
         * PrescriptionAccessoryUrl1 : [{"AccessoryUrl":"~/App_Upload/Storage/Medicare_Ticket/1912febd-038c-4472-a8a5-b0691f89a32f/62dd8f59-c749-4577-a7dc-cb8b40602cef.jpg","AccessoryVerId":"62dd8f59-c749-4577-a7dc-cb8b40602cef"}]
         * DiagnosisAccessoryUrl1 : [{"AccessoryUrl":"~/App_Upload/Storage/Medicare_Ticket/5cd37a1f-c9e1-4c30-b188-b199538ed75f/0a53a2d1-4815-4779-8c7b-2a7e79822669.jpg","AccessoryVerId":"0a53a2d1-4815-4779-8c7b-2a7e79822669"}]
         * CaseAccessoryUrl1 : []
         */

        private String SpecialDrugFilingId;
        private String dr_code;
        private String dr_name;
        private String fixmedins_code;
        private String fixmedins_name;
        private String phone;
        private String name;
        private String id_card_no;
        private String MIC_Code;
        private String ItemName;
        private String ApplicationFormAccessoryId1;
        private String PrescriptionAccessoryId1;
        private String DiagnosisAccessoryId1;
        private String CaseAccessoryId1;
        private String CreateDT;
        private String FilingUpdateDT;
        private String CreateUser;
        private String FilingUpdateUser;
        private String FilingStatus;
        private String RevokeStatus;
        private String RevokeReason;
        private String ReviewComments;
        private String RevokeUpdateDT;
        private String RevokeUpdateUser;
        private String FilingType;
        private String RevokeReviewComments;
        private String admdvsName;
        private String CommitmentLetterAccessoryId;
        private String CommitmentLetterAccessoryUrl;
        private List<SpecialDrugPicBean> ApplicationFormAccessoryUrl1;
        private List<SpecialDrugPicBean> PrescriptionAccessoryUrl1;
        private List<SpecialDrugPicBean> DiagnosisAccessoryUrl1;
        private List<SpecialDrugPicBean> CaseAccessoryUrl1;
        private List<MedInsuDirListBean> med_insu_dir_list;

        public String getSpecialDrugFilingId() {
            return SpecialDrugFilingId;
        }

        public void setSpecialDrugFilingId(String SpecialDrugFilingId) {
            this.SpecialDrugFilingId = SpecialDrugFilingId;
        }

        public String getDr_code() {
            return dr_code;
        }

        public void setDr_code(String dr_code) {
            this.dr_code = dr_code;
        }

        public String getDr_name() {
            return dr_name;
        }

        public void setDr_name(String dr_name) {
            this.dr_name = dr_name;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getFixmedins_name() {
            return fixmedins_name;
        }

        public void setFixmedins_name(String fixmedins_name) {
            this.fixmedins_name = fixmedins_name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId_card_no() {
            return id_card_no;
        }

        public void setId_card_no(String id_card_no) {
            this.id_card_no = id_card_no;
        }

        public String getMIC_Code() {
            return MIC_Code;
        }

        public void setMIC_Code(String MIC_Code) {
            this.MIC_Code = MIC_Code;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String ItemName) {
            this.ItemName = ItemName;
        }

        public String getApplicationFormAccessoryId1() {
            return ApplicationFormAccessoryId1;
        }

        public void setApplicationFormAccessoryId1(String ApplicationFormAccessoryId1) {
            this.ApplicationFormAccessoryId1 = ApplicationFormAccessoryId1;
        }

        public String getPrescriptionAccessoryId1() {
            return PrescriptionAccessoryId1;
        }

        public void setPrescriptionAccessoryId1(String PrescriptionAccessoryId1) {
            this.PrescriptionAccessoryId1 = PrescriptionAccessoryId1;
        }

        public String getDiagnosisAccessoryId1() {
            return DiagnosisAccessoryId1;
        }

        public void setDiagnosisAccessoryId1(String DiagnosisAccessoryId1) {
            this.DiagnosisAccessoryId1 = DiagnosisAccessoryId1;
        }

        public String getCaseAccessoryId1() {
            return CaseAccessoryId1;
        }

        public void setCaseAccessoryId1(String CaseAccessoryId1) {
            this.CaseAccessoryId1 = CaseAccessoryId1;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getFilingUpdateDT() {
            return FilingUpdateDT;
        }

        public void setFilingUpdateDT(String FilingUpdateDT) {
            this.FilingUpdateDT = FilingUpdateDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String CreateUser) {
            this.CreateUser = CreateUser;
        }

        public String getFilingUpdateUser() {
            return FilingUpdateUser;
        }

        public void setFilingUpdateUser(String FilingUpdateUser) {
            this.FilingUpdateUser = FilingUpdateUser;
        }

        public String getFilingStatus() {
            return FilingStatus;
        }

        public void setFilingStatus(String FilingStatus) {
            this.FilingStatus = FilingStatus;
        }

        public String getRevokeStatus() {
            return RevokeStatus;
        }

        public void setRevokeStatus(String RevokeStatus) {
            this.RevokeStatus = RevokeStatus;
        }

        public String getRevokeReason() {
            return RevokeReason;
        }

        public void setRevokeReason(String RevokeReason) {
            this.RevokeReason = RevokeReason;
        }

        public String getReviewComments() {
            return ReviewComments;
        }

        public void setReviewComments(String ReviewComments) {
            this.ReviewComments = ReviewComments;
        }

        public String getRevokeUpdateDT() {
            return RevokeUpdateDT;
        }

        public void setRevokeUpdateDT(String RevokeUpdateDT) {
            this.RevokeUpdateDT = RevokeUpdateDT;
        }

        public String getRevokeUpdateUser() {
            return RevokeUpdateUser;
        }

        public void setRevokeUpdateUser(String RevokeUpdateUser) {
            this.RevokeUpdateUser = RevokeUpdateUser;
        }

        public String getFilingType() {
            return FilingType;
        }

        public void setFilingType(String FilingType) {
            this.FilingType = FilingType;
        }

        public String getRevokeReviewComments() {
            return RevokeReviewComments;
        }

        public void setRevokeReviewComments(String RevokeReviewComments) {
            this.RevokeReviewComments = RevokeReviewComments;
        }

        public String getAdmdvsName() {
            return admdvsName;
        }

        public void setAdmdvsName(String admdvsName) {
            this.admdvsName = admdvsName;
        }

        public String getCommitmentLetterAccessoryId() {
            return CommitmentLetterAccessoryId;
        }

        public void setCommitmentLetterAccessoryId(String commitmentLetterAccessoryId) {
            CommitmentLetterAccessoryId = commitmentLetterAccessoryId;
        }

        public String getCommitmentLetterAccessoryUrl() {
            return CommitmentLetterAccessoryUrl;
        }

        public void setCommitmentLetterAccessoryUrl(String commitmentLetterAccessoryUrl) {
            CommitmentLetterAccessoryUrl = commitmentLetterAccessoryUrl;
        }

        public List<SpecialDrugPicBean> getApplicationFormAccessoryUrl1() {
            return ApplicationFormAccessoryUrl1;
        }

        public void setApplicationFormAccessoryUrl1(List<SpecialDrugPicBean> ApplicationFormAccessoryUrl1) {
            this.ApplicationFormAccessoryUrl1 = ApplicationFormAccessoryUrl1;
        }

        public List<SpecialDrugPicBean> getPrescriptionAccessoryUrl1() {
            return PrescriptionAccessoryUrl1;
        }

        public void setPrescriptionAccessoryUrl1(List<SpecialDrugPicBean> PrescriptionAccessoryUrl1) {
            this.PrescriptionAccessoryUrl1 = PrescriptionAccessoryUrl1;
        }

        public List<SpecialDrugPicBean> getDiagnosisAccessoryUrl1() {
            return DiagnosisAccessoryUrl1;
        }

        public void setDiagnosisAccessoryUrl1(List<SpecialDrugPicBean> DiagnosisAccessoryUrl1) {
            this.DiagnosisAccessoryUrl1 = DiagnosisAccessoryUrl1;
        }

        public List<SpecialDrugPicBean> getCaseAccessoryUrl1() {
            return CaseAccessoryUrl1;
        }

        public void setCaseAccessoryUrl1(List<SpecialDrugPicBean> CaseAccessoryUrl1) {
            this.CaseAccessoryUrl1 = CaseAccessoryUrl1;
        }

        public List<MedInsuDirListBean> getMed_insu_dir_list() {
            return med_insu_dir_list;
        }

        public void setMed_insu_dir_list(List<MedInsuDirListBean> med_insu_dir_list) {
            this.med_insu_dir_list = med_insu_dir_list;
        }

        public static class SpecialDrugPicBean {
            /**
             * AccessoryUrl : ~/App_Upload/Storage/Medicare_Ticket/0c1a5a33-5d85-4b77-bd9b-a3d81e9b6c19/62458a99-4e3c-462e-98e9-dbde9b3a71ab.jpg
             * AccessoryVerId : 62458a99-4e3c-462e-98e9-dbde9b3a71ab
             */

            private String AccessoryUrl;
            private String AccessoryId;

            public String getAccessoryUrl() {
                return AccessoryUrl;
            }

            public void setAccessoryUrl(String AccessoryUrl) {
                this.AccessoryUrl = AccessoryUrl;
            }

            public String getAccessoryId() {
                return AccessoryId;
            }

            public void setAccessoryId(String accessoryId) {
                AccessoryId = accessoryId;
            }
        }

        public static class MedInsuDirListBean{
            /**
             * SpecialDrugFilingSubId : 0
             * dr_code : D610802004964
             * dr_name : 刘怀勤
             * fixmedins_code : H61080200137
             * hosp_dept_name : 口腔科
             * fixmedins_name : 榆林市第一医院（榆林）
             * ProfessionalTitle : 主任医师
             * MIC_Code : XL01EXA350A001020183503
             * ItemName : 阿伐替尼片
             * cnt : 100
             * cnt_prcunt :
             */
            private String SpecialDrugFilingSubId;
            //fixmedins_code
            public String fixmedins_code;
            //fixmedins_name
            public String fixmedins_name;
            //hosp_dept_name
            public String hosp_dept_name;
            //医师编码
            public String dr_code;
            //医师名字
            public String dr_name;
            //职称
            public String ProfessionalTitle;
            //药品编码
            public String MIC_Code;
            //药品名称
            public String ItemName;
            //药品数量
            public String cnt;
            //药品单位
            public String cnt_prcunt;
            //开始时间
            public String begintime;
            //结束时间
            public String endtime;

            public MedInsuDirListBean() {}

            public MedInsuDirListBean(String fixmedins_code, String fixmedins_name, String hosp_dept_name, String dr_code, String dr_name,
                                      String professionalTitle, String MIC_Code, String itemName, String cnt, String cnt_prcunt, String begintime, String endtime) {
                this.fixmedins_code = fixmedins_code;
                this.fixmedins_name = fixmedins_name;
                this.hosp_dept_name = hosp_dept_name;
                this.dr_code = dr_code;
                this.dr_name = dr_name;
                ProfessionalTitle = professionalTitle;
                this.MIC_Code = MIC_Code;
                ItemName = itemName;
                this.cnt = cnt;
                this.cnt_prcunt = cnt_prcunt;
                this.begintime = begintime;
                this.endtime = endtime;
            }

            public String getSpecialDrugFilingSubId() {
                return SpecialDrugFilingSubId;
            }

            public void setSpecialDrugFilingSubId(String specialDrugFilingSubId) {
                SpecialDrugFilingSubId = specialDrugFilingSubId;
            }

            public String getDr_code() {
                return dr_code;
            }

            public void setDr_code(String dr_code) {
                this.dr_code = dr_code;
            }

            public String getDr_name() {
                return dr_name;
            }

            public void setDr_name(String dr_name) {
                this.dr_name = dr_name;
            }

            public String getFixmedins_code() {
                return fixmedins_code;
            }

            public void setFixmedins_code(String fixmedins_code) {
                this.fixmedins_code = fixmedins_code;
            }

            public String getHosp_dept_name() {
                return hosp_dept_name;
            }

            public void setHosp_dept_name(String hosp_dept_name) {
                this.hosp_dept_name = hosp_dept_name;
            }

            public String getFixmedins_name() {
                return fixmedins_name;
            }

            public void setFixmedins_name(String fixmedins_name) {
                this.fixmedins_name = fixmedins_name;
            }

            public String getProfessionalTitle() {
                return ProfessionalTitle;
            }

            public void setProfessionalTitle(String professionalTitle) {
                ProfessionalTitle = professionalTitle;
            }

            public String getMIC_Code() {
                return MIC_Code;
            }

            public void setMIC_Code(String MIC_Code) {
                this.MIC_Code = MIC_Code;
            }

            public String getItemName() {
                return ItemName;
            }

            public void setItemName(String itemName) {
                ItemName = itemName;
            }

            public String getCnt() {
                return cnt;
            }

            public void setCnt(String cnt) {
                this.cnt = cnt;
            }

            public String getCnt_prcunt() {
                return cnt_prcunt;
            }

            public void setCnt_prcunt(String cnt_prcunt) {
                this.cnt_prcunt = cnt_prcunt;
            }

            public String getBegintime() {
                return begintime;
            }

            public void setBegintime(String begintime) {
                this.begintime = begintime;
            }

            public String getEndtime() {
                return endtime;
            }

            public void setEndtime(String endtime) {
                this.endtime = endtime;
            }
        }
    }

    //特药备案详情
    public static void sendSpecialDrugDetailRequest(final String TAG, String SpecialDrugFilingId,final CustomerJsonCallBack<SpecialDrugDetailModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("SpecialDrugFilingId", SpecialDrugFilingId);//特药备案唯一标识

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_SPECIALDRUGDETAIL_URL, jsonObject.toJSONString(), callback);
    }
}
