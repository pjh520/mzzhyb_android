package com.jrdz.zhyb_android.ui.insured.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-15
 * 描    述：
 * ================================================
 */
public class SpecialDrugListModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2024-04-17 15:14:01
     * totalItems : 197303
     * data : [{"InstructionsAccessoryUrl":"","DetailAccessoryUrl1":"","DetailAccessoryUrl2":"","DetailAccessoryUrl3":"","DetailAccessoryUrl4":"","DetailAccessoryUrl5":"","BannerAccessoryUrl1":"","BannerAccessoryUrl2":"","BannerAccessoryUrl3":"","DrugDataId":6,"MIC_Code":"X6100000000000000000001","ItemCode":"X6100000000000000000001","ItemName":"苯丙酮尿症替代食品","ItemType":0,"ApprovalNumber":"国药准字","RegDosform":"null","Spec":"null","EnterpriseName":"null","ExpiryDateCount":36,"InventoryQuantity":10,"Price":1,"InstructionsAccessoryId":"b24f6d95-2228-4d1e-b592-0081ac208a87","DetailAccessoryId1":"ed819970-8951-470e-ba54-78d334f90aa9","DetailAccessoryId2":"8f976543-fba0-439f-b7d4-6dea54a593f1","DetailAccessoryId3":"af430ab2-90e6-49a0-bbf9-07c141db88ef","DetailAccessoryId4":"42b800a6-7e01-4bd8-9b29-faebcbb03a8b","DetailAccessoryId5":"7ca5ac00-c656-4743-ac2a-a8e0dc92800e","IsPlatformDrug":1,"CreateDT":"2023-01-06 12:18:22","UpdateDT":"2023-02-11 23:58:22","DrugsClassification":1,"DrugsClassificationName":"西药中成药","barCode":"","pyjmCode":"BBTNZTDSP","isEnable":1,"AssociatedDiagnostics":"","BannerAccessoryId1":"00000000-0000-0000-0000-000000000000","BannerAccessoryId2":"00000000-0000-0000-0000-000000000000","BannerAccessoryId3":"00000000-0000-0000-0000-000000000000"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * InstructionsAccessoryUrl :
         * DetailAccessoryUrl1 :
         * DetailAccessoryUrl2 :
         * DetailAccessoryUrl3 :
         * DetailAccessoryUrl4 :
         * DetailAccessoryUrl5 :
         * BannerAccessoryUrl1 :
         * BannerAccessoryUrl2 :
         * BannerAccessoryUrl3 :
         * DrugDataId : 6
         * MIC_Code : X6100000000000000000001
         * ItemCode : X6100000000000000000001
         * ItemName : 苯丙酮尿症替代食品
         * ItemType : 0
         * ApprovalNumber : 国药准字
         * RegDosform : null
         * Spec : null
         * EnterpriseName : null
         * ExpiryDateCount : 36
         * InventoryQuantity : 10
         * Price : 1
         * InstructionsAccessoryId : b24f6d95-2228-4d1e-b592-0081ac208a87
         * DetailAccessoryId1 : ed819970-8951-470e-ba54-78d334f90aa9
         * DetailAccessoryId2 : 8f976543-fba0-439f-b7d4-6dea54a593f1
         * DetailAccessoryId3 : af430ab2-90e6-49a0-bbf9-07c141db88ef
         * DetailAccessoryId4 : 42b800a6-7e01-4bd8-9b29-faebcbb03a8b
         * DetailAccessoryId5 : 7ca5ac00-c656-4743-ac2a-a8e0dc92800e
         * IsPlatformDrug : 1
         * CreateDT : 2023-01-06 12:18:22
         * UpdateDT : 2023-02-11 23:58:22
         * DrugsClassification : 1
         * DrugsClassificationName : 西药中成药
         * barCode :
         * pyjmCode : BBTNZTDSP
         * isEnable : 1
         * AssociatedDiagnostics :
         * BannerAccessoryId1 : 00000000-0000-0000-0000-000000000000
         * BannerAccessoryId2 : 00000000-0000-0000-0000-000000000000
         * BannerAccessoryId3 : 00000000-0000-0000-0000-000000000000
         */
        private String DrugDataId;
        private String MIC_Code;
        private String ItemCode;
        private String ItemName;
        private String ApprovalNumber;
        private String RegDosform;
        private String Spec;
        private String EnterpriseName;
        //2023版甲乙类
        public String ClassAB;
        //编号
        public String DrugNo;
        //药品企业
        public String DrugCompany;
        //最小包装单位
        public String mpu;
        //医保药品名称
        public String MIC_Name;
        //最小包装数量
        public String mpq;
        //备注
        public String Remark;

        public String getDrugDataId() {
            return DrugDataId;
        }

        public void setDrugDataId(String DrugDataId) {
            this.DrugDataId = DrugDataId;
        }

        public String getMIC_Code() {
            return MIC_Code;
        }

        public void setMIC_Code(String MIC_Code) {
            this.MIC_Code = MIC_Code;
        }

        public String getItemCode() {
            return ItemCode;
        }

        public void setItemCode(String ItemCode) {
            this.ItemCode = ItemCode;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String ItemName) {
            this.ItemName = ItemName;
        }

        public String getApprovalNumber() {
            return ApprovalNumber;
        }

        public void setApprovalNumber(String ApprovalNumber) {
            this.ApprovalNumber = ApprovalNumber;
        }

        public String getRegDosform() {
            return RegDosform;
        }

        public void setRegDosform(String RegDosform) {
            this.RegDosform = RegDosform;
        }

        public String getSpec() {
            return Spec;
        }

        public void setSpec(String Spec) {
            this.Spec = Spec;
        }

        public String getEnterpriseName() {
            return EnterpriseName;
        }

        public void setEnterpriseName(String EnterpriseName) {
            this.EnterpriseName = EnterpriseName;
        }

        public String getClassAB() {
            return ClassAB;
        }

        public void setClassAB(String classAB) {
            ClassAB = classAB;
        }

        public String getDrugNo() {
            return DrugNo;
        }

        public void setDrugNo(String drugNo) {
            DrugNo = drugNo;
        }

        public String getDrugCompany() {
            return DrugCompany;
        }

        public void setDrugCompany(String drugCompany) {
            DrugCompany = drugCompany;
        }

        public String getMpu() {
            return mpu;
        }

        public void setMpu(String mpu) {
            this.mpu = mpu;
        }

        public String getMIC_Name() {
            return MIC_Name;
        }

        public void setMIC_Name(String MIC_Name) {
            this.MIC_Name = MIC_Name;
        }

        public String getMpq() {
            return mpq;
        }

        public void setMpq(String mpq) {
            this.mpq = mpq;
        }

        public String getRemark() {
            return Remark;
        }

        public void setRemark(String remark) {
            Remark = remark;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.DrugDataId);
            dest.writeString(this.MIC_Code);
            dest.writeString(this.ItemCode);
            dest.writeString(this.ItemName);
            dest.writeString(this.ApprovalNumber);
            dest.writeString(this.RegDosform);
            dest.writeString(this.Spec);
            dest.writeString(this.EnterpriseName);
            dest.writeString(this.ClassAB);
            dest.writeString(this.DrugNo);
            dest.writeString(this.DrugCompany);
            dest.writeString(this.mpu);
            dest.writeString(this.MIC_Name);
            dest.writeString(this.mpq);
            dest.writeString(this.Remark);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.DrugDataId = in.readString();
            this.MIC_Code = in.readString();
            this.ItemCode = in.readString();
            this.ItemName = in.readString();
            this.ApprovalNumber = in.readString();
            this.RegDosform = in.readString();
            this.Spec = in.readString();
            this.EnterpriseName = in.readString();
            this.ClassAB = in.readString();
            this.DrugNo = in.readString();
            this.DrugCompany = in.readString();
            this.mpu = in.readString();
            this.MIC_Name = in.readString();
            this.mpq = in.readString();
            this.Remark = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //特药药品库列表
    public static void sendSpecialDrugListRequest(final String TAG, String pageindex,String pagesize,String name, final CustomerJsonCallBack<SpecialDrugListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", name);//搜索名称
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_SPECIALDRUGLIST_URL, jsonObject.toJSONString(), callback);
    }
}
