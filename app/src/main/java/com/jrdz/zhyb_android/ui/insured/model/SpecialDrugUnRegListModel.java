package com.jrdz.zhyb_android.ui.insured.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-16
 * 描    述：
 * ================================================
 */
public class SpecialDrugUnRegListModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2024-04-17 17:46:47
     * data : [{"SpecialDrugFilingId":1,"dr_code":"10020115029","dr_name":"贾春利","fixmedins_code":"10020115029","fixmedins_name":"陕西怡悦大药房连锁有限公司西安西影路连锁二店","phone":"15060338985","name":"宋怀春","id_card_no":"612726196609210011","MIC_Code":"X6100000000000000000001","ItemName":"苯丙酮尿症替代食品","begintime":"2024-04-17 00:00:00","endtime":"2024-06-17 00:00:00","ApplicationFormAccessoryId1":"0c1a5a33-5d85-4b77-bd9b-a3d81e9b6c19","ApplicationFormAccessoryId2":"00000000-0000-0000-0000-000000000000","ApplicationFormAccessoryId3":"00000000-0000-0000-0000-000000000000","PrescriptionAccessoryId1":"1912febd-038c-4472-a8a5-b0691f89a32f","PrescriptionAccessoryId2":"00000000-0000-0000-0000-000000000000","PrescriptionAccessoryId3":"00000000-0000-0000-0000-000000000000","DiagnosisAccessoryId1":"5cd37a1f-c9e1-4c30-b188-b199538ed75f","DiagnosisAccessoryId2":"00000000-0000-0000-0000-000000000000","DiagnosisAccessoryId3":"00000000-0000-0000-0000-000000000000","CaseAccessoryId1":"a7417861-3cf4-4537-96bf-a21b234be874","CaseAccessoryId2":"00000000-0000-0000-0000-000000000000","CaseAccessoryId3":"00000000-0000-0000-0000-000000000000","ApplicationFormAccessoryUrl1":"~/App_Upload/Storage/Medicare_Ticket/0c1a5a33-5d85-4b77-bd9b-a3d81e9b6c19/62458a99-4e3c-462e-98e9-dbde9b3a71ab.jpg","ApplicationFormAccessoryUrl2":"","ApplicationFormAccessoryUrl3":"","PrescriptionAccessoryUrl1":"~/App_Upload/Storage/Medicare_Ticket/1912febd-038c-4472-a8a5-b0691f89a32f/62dd8f59-c749-4577-a7dc-cb8b40602cef.jpg","PrescriptionAccessoryUrl2":"","PrescriptionAccessoryUrl3":"","DiagnosisAccessoryUrl1":"~/App_Upload/Storage/Medicare_Ticket/5cd37a1f-c9e1-4c30-b188-b199538ed75f/0a53a2d1-4815-4779-8c7b-2a7e79822669.jpg","DiagnosisAccessoryUrl2":"","DiagnosisAccessoryUrl3":"","CaseAccessoryUrl1":"~/App_Upload/Storage/Medicare_Ticket/a7417861-3cf4-4537-96bf-a21b234be874/91256d08-4e2c-446a-b667-2a0c2c8109e4.jpg","CaseAccessoryUrl2":"","CaseAccessoryUrl3":"","CreateDT":"2024-04-17 17:18:12","FilingUpdateDT":"2024-04-17 17:18:12","CreateUser":"15060338985","FilingUpdateUser":"15060338985","FilingStatus":1,"RevokeStatus":0,"RevokeReason":"","ReviewComments":"","RevokeUpdateDT":"2024-04-17 17:18:12","RevokeUpdateUser":"15060338985","FilingType":1,"RevokeReviewComments":""}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * SpecialDrugFilingId : 1
         * dr_code : 10020115029
         * dr_name : 贾春利
         * fixmedins_code : 10020115029
         * fixmedins_name : 陕西怡悦大药房连锁有限公司西安西影路连锁二店
         * phone : 15060338985
         * name : 宋怀春
         * id_card_no : 612726196609210011
         * MIC_Code : X6100000000000000000001
         * ItemName : 苯丙酮尿症替代食品
         * begintime : 2024-04-17 00:00:00
         * endtime : 2024-06-17 00:00:00
         * ApplicationFormAccessoryId1 : 0c1a5a33-5d85-4b77-bd9b-a3d81e9b6c19
         * ApplicationFormAccessoryId2 : 00000000-0000-0000-0000-000000000000
         * ApplicationFormAccessoryId3 : 00000000-0000-0000-0000-000000000000
         * PrescriptionAccessoryId1 : 1912febd-038c-4472-a8a5-b0691f89a32f
         * PrescriptionAccessoryId2 : 00000000-0000-0000-0000-000000000000
         * PrescriptionAccessoryId3 : 00000000-0000-0000-0000-000000000000
         * DiagnosisAccessoryId1 : 5cd37a1f-c9e1-4c30-b188-b199538ed75f
         * DiagnosisAccessoryId2 : 00000000-0000-0000-0000-000000000000
         * DiagnosisAccessoryId3 : 00000000-0000-0000-0000-000000000000
         * CaseAccessoryId1 : a7417861-3cf4-4537-96bf-a21b234be874
         * CaseAccessoryId2 : 00000000-0000-0000-0000-000000000000
         * CaseAccessoryId3 : 00000000-0000-0000-0000-000000000000
         * ApplicationFormAccessoryUrl1 : ~/App_Upload/Storage/Medicare_Ticket/0c1a5a33-5d85-4b77-bd9b-a3d81e9b6c19/62458a99-4e3c-462e-98e9-dbde9b3a71ab.jpg
         * ApplicationFormAccessoryUrl2 :
         * ApplicationFormAccessoryUrl3 :
         * PrescriptionAccessoryUrl1 : ~/App_Upload/Storage/Medicare_Ticket/1912febd-038c-4472-a8a5-b0691f89a32f/62dd8f59-c749-4577-a7dc-cb8b40602cef.jpg
         * PrescriptionAccessoryUrl2 :
         * PrescriptionAccessoryUrl3 :
         * DiagnosisAccessoryUrl1 : ~/App_Upload/Storage/Medicare_Ticket/5cd37a1f-c9e1-4c30-b188-b199538ed75f/0a53a2d1-4815-4779-8c7b-2a7e79822669.jpg
         * DiagnosisAccessoryUrl2 :
         * DiagnosisAccessoryUrl3 :
         * CaseAccessoryUrl1 : ~/App_Upload/Storage/Medicare_Ticket/a7417861-3cf4-4537-96bf-a21b234be874/91256d08-4e2c-446a-b667-2a0c2c8109e4.jpg
         * CaseAccessoryUrl2 :
         * CaseAccessoryUrl3 :
         * CreateDT : 2024-04-17 17:18:12
         * FilingUpdateDT : 2024-04-17 17:18:12
         * CreateUser : 15060338985
         * FilingUpdateUser : 15060338985
         * FilingStatus : 1
         * RevokeStatus : 0
         * RevokeReason :
         * ReviewComments :
         * RevokeUpdateDT : 2024-04-17 17:18:12
         * RevokeUpdateUser : 15060338985
         * FilingType : 1
         * RevokeReviewComments :
         */

        private String SpecialDrugFilingId;
        private String dr_code;
        private String dr_name;
        private String fixmedins_code;
        private String fixmedins_name;
        private String phone;
        private String name;
        private String id_card_no;
        private String MIC_Code;
        private String ItemName;
        private String begintime;
        private String endtime;
        private String ApplicationFormAccessoryId1;
        private String ApplicationFormAccessoryId2;
        private String ApplicationFormAccessoryId3;
        private String PrescriptionAccessoryId1;
        private String PrescriptionAccessoryId2;
        private String PrescriptionAccessoryId3;
        private String DiagnosisAccessoryId1;
        private String DiagnosisAccessoryId2;
        private String DiagnosisAccessoryId3;
        private String CaseAccessoryId1;
        private String CaseAccessoryId2;
        private String CaseAccessoryId3;
        private String ApplicationFormAccessoryUrl1;
        private String ApplicationFormAccessoryUrl2;
        private String ApplicationFormAccessoryUrl3;
        private String PrescriptionAccessoryUrl1;
        private String PrescriptionAccessoryUrl2;
        private String PrescriptionAccessoryUrl3;
        private String DiagnosisAccessoryUrl1;
        private String DiagnosisAccessoryUrl2;
        private String DiagnosisAccessoryUrl3;
        private String CaseAccessoryUrl1;
        private String CaseAccessoryUrl2;
        private String CaseAccessoryUrl3;
        private String CreateDT;
        private String FilingUpdateDT;
        private String CreateUser;
        private String FilingUpdateUser;
        private String FilingStatus;
        private String RevokeStatus;
        private String RevokeReason;
        private String ReviewComments;
        private String RevokeUpdateDT;
        private String RevokeUpdateUser;
        private String FilingType;
        private String RevokeReviewComments;

        public String getSpecialDrugFilingId() {
            return SpecialDrugFilingId;
        }

        public void setSpecialDrugFilingId(String SpecialDrugFilingId) {
            this.SpecialDrugFilingId = SpecialDrugFilingId;
        }

        public String getDr_code() {
            return dr_code;
        }

        public void setDr_code(String dr_code) {
            this.dr_code = dr_code;
        }

        public String getDr_name() {
            return dr_name;
        }

        public void setDr_name(String dr_name) {
            this.dr_name = dr_name;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getFixmedins_name() {
            return fixmedins_name;
        }

        public void setFixmedins_name(String fixmedins_name) {
            this.fixmedins_name = fixmedins_name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId_card_no() {
            return id_card_no;
        }

        public void setId_card_no(String id_card_no) {
            this.id_card_no = id_card_no;
        }

        public String getMIC_Code() {
            return MIC_Code;
        }

        public void setMIC_Code(String MIC_Code) {
            this.MIC_Code = MIC_Code;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String ItemName) {
            this.ItemName = ItemName;
        }

        public String getBegintime() {
            return begintime;
        }

        public void setBegintime(String begintime) {
            this.begintime = begintime;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getApplicationFormAccessoryId1() {
            return ApplicationFormAccessoryId1;
        }

        public void setApplicationFormAccessoryId1(String ApplicationFormAccessoryId1) {
            this.ApplicationFormAccessoryId1 = ApplicationFormAccessoryId1;
        }

        public String getApplicationFormAccessoryId2() {
            return ApplicationFormAccessoryId2;
        }

        public void setApplicationFormAccessoryId2(String ApplicationFormAccessoryId2) {
            this.ApplicationFormAccessoryId2 = ApplicationFormAccessoryId2;
        }

        public String getApplicationFormAccessoryId3() {
            return ApplicationFormAccessoryId3;
        }

        public void setApplicationFormAccessoryId3(String ApplicationFormAccessoryId3) {
            this.ApplicationFormAccessoryId3 = ApplicationFormAccessoryId3;
        }

        public String getPrescriptionAccessoryId1() {
            return PrescriptionAccessoryId1;
        }

        public void setPrescriptionAccessoryId1(String PrescriptionAccessoryId1) {
            this.PrescriptionAccessoryId1 = PrescriptionAccessoryId1;
        }

        public String getPrescriptionAccessoryId2() {
            return PrescriptionAccessoryId2;
        }

        public void setPrescriptionAccessoryId2(String PrescriptionAccessoryId2) {
            this.PrescriptionAccessoryId2 = PrescriptionAccessoryId2;
        }

        public String getPrescriptionAccessoryId3() {
            return PrescriptionAccessoryId3;
        }

        public void setPrescriptionAccessoryId3(String PrescriptionAccessoryId3) {
            this.PrescriptionAccessoryId3 = PrescriptionAccessoryId3;
        }

        public String getDiagnosisAccessoryId1() {
            return DiagnosisAccessoryId1;
        }

        public void setDiagnosisAccessoryId1(String DiagnosisAccessoryId1) {
            this.DiagnosisAccessoryId1 = DiagnosisAccessoryId1;
        }

        public String getDiagnosisAccessoryId2() {
            return DiagnosisAccessoryId2;
        }

        public void setDiagnosisAccessoryId2(String DiagnosisAccessoryId2) {
            this.DiagnosisAccessoryId2 = DiagnosisAccessoryId2;
        }

        public String getDiagnosisAccessoryId3() {
            return DiagnosisAccessoryId3;
        }

        public void setDiagnosisAccessoryId3(String DiagnosisAccessoryId3) {
            this.DiagnosisAccessoryId3 = DiagnosisAccessoryId3;
        }

        public String getCaseAccessoryId1() {
            return CaseAccessoryId1;
        }

        public void setCaseAccessoryId1(String CaseAccessoryId1) {
            this.CaseAccessoryId1 = CaseAccessoryId1;
        }

        public String getCaseAccessoryId2() {
            return CaseAccessoryId2;
        }

        public void setCaseAccessoryId2(String CaseAccessoryId2) {
            this.CaseAccessoryId2 = CaseAccessoryId2;
        }

        public String getCaseAccessoryId3() {
            return CaseAccessoryId3;
        }

        public void setCaseAccessoryId3(String CaseAccessoryId3) {
            this.CaseAccessoryId3 = CaseAccessoryId3;
        }

        public String getApplicationFormAccessoryUrl1() {
            return ApplicationFormAccessoryUrl1;
        }

        public void setApplicationFormAccessoryUrl1(String ApplicationFormAccessoryUrl1) {
            this.ApplicationFormAccessoryUrl1 = ApplicationFormAccessoryUrl1;
        }

        public String getApplicationFormAccessoryUrl2() {
            return ApplicationFormAccessoryUrl2;
        }

        public void setApplicationFormAccessoryUrl2(String ApplicationFormAccessoryUrl2) {
            this.ApplicationFormAccessoryUrl2 = ApplicationFormAccessoryUrl2;
        }

        public String getApplicationFormAccessoryUrl3() {
            return ApplicationFormAccessoryUrl3;
        }

        public void setApplicationFormAccessoryUrl3(String ApplicationFormAccessoryUrl3) {
            this.ApplicationFormAccessoryUrl3 = ApplicationFormAccessoryUrl3;
        }

        public String getPrescriptionAccessoryUrl1() {
            return PrescriptionAccessoryUrl1;
        }

        public void setPrescriptionAccessoryUrl1(String PrescriptionAccessoryUrl1) {
            this.PrescriptionAccessoryUrl1 = PrescriptionAccessoryUrl1;
        }

        public String getPrescriptionAccessoryUrl2() {
            return PrescriptionAccessoryUrl2;
        }

        public void setPrescriptionAccessoryUrl2(String PrescriptionAccessoryUrl2) {
            this.PrescriptionAccessoryUrl2 = PrescriptionAccessoryUrl2;
        }

        public String getPrescriptionAccessoryUrl3() {
            return PrescriptionAccessoryUrl3;
        }

        public void setPrescriptionAccessoryUrl3(String PrescriptionAccessoryUrl3) {
            this.PrescriptionAccessoryUrl3 = PrescriptionAccessoryUrl3;
        }

        public String getDiagnosisAccessoryUrl1() {
            return DiagnosisAccessoryUrl1;
        }

        public void setDiagnosisAccessoryUrl1(String DiagnosisAccessoryUrl1) {
            this.DiagnosisAccessoryUrl1 = DiagnosisAccessoryUrl1;
        }

        public String getDiagnosisAccessoryUrl2() {
            return DiagnosisAccessoryUrl2;
        }

        public void setDiagnosisAccessoryUrl2(String DiagnosisAccessoryUrl2) {
            this.DiagnosisAccessoryUrl2 = DiagnosisAccessoryUrl2;
        }

        public String getDiagnosisAccessoryUrl3() {
            return DiagnosisAccessoryUrl3;
        }

        public void setDiagnosisAccessoryUrl3(String DiagnosisAccessoryUrl3) {
            this.DiagnosisAccessoryUrl3 = DiagnosisAccessoryUrl3;
        }

        public String getCaseAccessoryUrl1() {
            return CaseAccessoryUrl1;
        }

        public void setCaseAccessoryUrl1(String CaseAccessoryUrl1) {
            this.CaseAccessoryUrl1 = CaseAccessoryUrl1;
        }

        public String getCaseAccessoryUrl2() {
            return CaseAccessoryUrl2;
        }

        public void setCaseAccessoryUrl2(String CaseAccessoryUrl2) {
            this.CaseAccessoryUrl2 = CaseAccessoryUrl2;
        }

        public String getCaseAccessoryUrl3() {
            return CaseAccessoryUrl3;
        }

        public void setCaseAccessoryUrl3(String CaseAccessoryUrl3) {
            this.CaseAccessoryUrl3 = CaseAccessoryUrl3;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getFilingUpdateDT() {
            return FilingUpdateDT;
        }

        public void setFilingUpdateDT(String FilingUpdateDT) {
            this.FilingUpdateDT = FilingUpdateDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String CreateUser) {
            this.CreateUser = CreateUser;
        }

        public String getFilingUpdateUser() {
            return FilingUpdateUser;
        }

        public void setFilingUpdateUser(String FilingUpdateUser) {
            this.FilingUpdateUser = FilingUpdateUser;
        }

        public String getFilingStatus() {
            return FilingStatus;
        }

        public void setFilingStatus(String FilingStatus) {
            this.FilingStatus = FilingStatus;
        }

        public String getRevokeStatus() {
            return RevokeStatus;
        }

        public void setRevokeStatus(String RevokeStatus) {
            this.RevokeStatus = RevokeStatus;
        }

        public String getRevokeReason() {
            return RevokeReason;
        }

        public void setRevokeReason(String RevokeReason) {
            this.RevokeReason = RevokeReason;
        }

        public String getReviewComments() {
            return ReviewComments;
        }

        public void setReviewComments(String ReviewComments) {
            this.ReviewComments = ReviewComments;
        }

        public String getRevokeUpdateDT() {
            return RevokeUpdateDT;
        }

        public void setRevokeUpdateDT(String RevokeUpdateDT) {
            this.RevokeUpdateDT = RevokeUpdateDT;
        }

        public String getRevokeUpdateUser() {
            return RevokeUpdateUser;
        }

        public void setRevokeUpdateUser(String RevokeUpdateUser) {
            this.RevokeUpdateUser = RevokeUpdateUser;
        }

        public String getFilingType() {
            return FilingType;
        }

        public void setFilingType(String FilingType) {
            this.FilingType = FilingType;
        }

        public String getRevokeReviewComments() {
            return RevokeReviewComments;
        }

        public void setRevokeReviewComments(String RevokeReviewComments) {
            this.RevokeReviewComments = RevokeReviewComments;
        }
    }

    //特药药品库列表
    public static void sendSpecialDrugUnRegListRequest(final String TAG, String pageindex, String pagesize, String FilingStatus, final CustomerJsonCallBack<SpecialDrugUnRegListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", "");//搜索名称
        jsonObject.put("FilingStatus", FilingStatus);//备案审核状态（1、备案中 2、备案成功 3、备案失败 4.正在备案中 5.已撤销）
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_INSURED_QUERYSPECIALDRUG_URL, jsonObject.toJSONString(), callback);
    }
}
