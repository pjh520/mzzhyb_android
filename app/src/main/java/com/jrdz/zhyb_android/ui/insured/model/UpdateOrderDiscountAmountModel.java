package com.jrdz.zhyb_android.ui.insured.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-12-29
 * 描    述：
 * ================================================
 */
public class UpdateOrderDiscountAmountModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-12-29 17:08:12
     * data : {"TotalAmount":"0.88"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * TotalAmount : 0.88
         */

        private String TotalAmount;

        public String getTotalAmount() {
            return TotalAmount;
        }

        public void setTotalAmount(String TotalAmount) {
            this.TotalAmount = TotalAmount;
        }
    }

    //订单修改优惠价格
    public static void sendUpdateOrderDiscountAmountRequest(final String TAG, String OrderNo,String DiscountAmount,String Discount,
                                                            final CustomerJsonCallBack<UpdateOrderDiscountAmountModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);
        jsonObject.put("DiscountAmount", DiscountAmount);
        jsonObject.put("Discount", Discount);
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MERCHANT_UPDATEORDERDISCOUNTAMOUNT_URL, jsonObject.toJSONString(), callback);
    }

}
