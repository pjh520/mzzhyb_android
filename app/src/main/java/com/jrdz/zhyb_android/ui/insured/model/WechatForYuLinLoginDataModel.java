package com.jrdz.zhyb_android.ui.insured.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-06-27
 * 描    述：
 * ================================================
 */
public class WechatForYuLinLoginDataModel {
    /**
     * mobile : 18966873288
     * accessToken : !TOrs5OA8KzO-FP1iAw1j4SSo0J6Nfrvs4XkI1SGqdytPf_--FOFIvlkLn_EFD-cNiSu-0Y4T6zC5_BmN5Uf2fRcQCWJP1xy_g3vFHOvMdQIF7BPOfVt6HSXlU-yXuyNDTYOIHmR4PXTGspmVHfK_IXuxedVEV6xxDkY5G1HcKKbrM=
     * refreshToken : 8f33aa9a6e82444c993fd914136ead36
     */

    private String mobile;
    private String accessToken;
    private String refreshToken;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
