package com.jrdz.zhyb_android.ui.insured.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.H5JsonCallBack;
import com.jrdz.zhyb_android.net.H5RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-06-27
 * 描    述：
 * ================================================
 */
public class WechatForYuLinLoginModel {
    /**
     * code : 0
     * type : success
     * message : 成功
     * data : xsgVxCd26iO/p9XHj49eOb80lyZaB/pEt7T+LuZLw0/XrCWX2y1mOF3r4EsXV+kvnNvPnXEceF18Z5V5YKco60N1ZGrZebO0UsYK/qTTjlu0Cjc/oukGuq5WGOtT0Ms6WfMVrf/d3hd/tKld2PFTD/3NglvxyvJXKWBt78Za29RdDJCYZofFyD9lOMhLhEjkNUD4JwD5CWN/GFCdLNqjB9ij/5YBoJOX1e6SJcWWUYEYWNxJuHh2hfk3FUFLhVbjwpJ4W64juAVqwy/HlrXXW37y1h53jPX2Fecpi59eHmU7ru1WVLM9rafe8hZGcuBTmlowGLtxYLLX94MNqHY4HvACxo6+5vV4WpuhfUqWNXo=
     */

    private int code;
    private String type;
    private String message;
    private String data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    //获取token
    public static void sendWechatForYuLinLoginRequest(final String TAG, String encryData,
                                                      final H5JsonCallBack<WechatForYuLinLoginModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("encryData", encryData);

        H5RequestData.requesNetWork_Json(TAG, Constants.H5URL.H5_WECHATFORYULINLOGIN_URL, jsonObject.toJSONString(), callback);
    }
}
