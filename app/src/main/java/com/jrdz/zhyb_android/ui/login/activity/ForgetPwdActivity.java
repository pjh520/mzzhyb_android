package com.jrdz.zhyb_android.ui.login.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.widget.countDownTime.CustomCountDownTimer;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.login.model.QueryPhoneModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.login.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-28
 * 描    述：忘记密码页面
 * ================================================
 */
public class ForgetPwdActivity extends BaseActivity {
    private EditText mEtPhone;
    private EditText mEtNewPwd;
    private EditText mEtAgainPwd;
    private LinearLayout mLlNewpwd;
    private View mLineNewpwd;
    private TextView mTvNewpwdDescribe;
    private LinearLayout mLlAgainPwd;
    private View mLineAgainPwd;
    private TextView mTvDescribe,mTvSendPhone;
    private LinearLayout mLlSendPhone;
    private View mLineSendPhone;
    private EditText mEtVerifCode;
    private TextView mTvTime;
    private ShapeTextView mTvQueryPhone,mTvUpdate;
    private CustomCountDownTimer mCustomCountDownTimer;

    String phone="";
    private int from;

    @Override
    public int getLayoutId() {
        return R.layout.activity_forget_pwd;
    }

    @Override
    public void initView() {
        super.initView();
        mEtPhone = findViewById(R.id.et_phone);
        mTvDescribe= findViewById(R.id.tv_describe);
        mTvQueryPhone= findViewById(R.id.tv_query_phone);
        mLlNewpwd = findViewById(R.id.ll_newpwd);
        mLineNewpwd = findViewById(R.id.line_newpwd);
        mTvNewpwdDescribe = findViewById(R.id.tv_newpwd_describe);
        mEtNewPwd = findViewById(R.id.et_new_pwd);
        mLlAgainPwd = findViewById(R.id.ll_again_pwd);
        mLineAgainPwd = findViewById(R.id.line_again_pwd);
        mEtAgainPwd = findViewById(R.id.et_again_pwd);
        mTvSendPhone = findViewById(R.id.tv_send_phone);
        mLlSendPhone = findViewById(R.id.ll_send_phone);
        mLineSendPhone = findViewById(R.id.line_send_phone);
        mEtVerifCode = findViewById(R.id.et_verif_code);
        mTvTime = findViewById(R.id.tv_time);
        mTvUpdate = findViewById(R.id.tv_update);
    }

    @Override
    public void initData() {
        from=getIntent().getIntExtra("from",1);
        super.initData();

        if (1==from){
            mTvDescribe.setText("提示:账号为机构编码,例如:H620879754。");
        }else {
            mTvDescribe.setText("提示:账号为新增人员时填写的人员编码,例如:D610800000000。");
        }

    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvQueryPhone.setOnClickListener(this);
        mTvTime.setOnClickListener(this);
        mTvUpdate.setOnClickListener(this);

        mEtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                if (View.VISIBLE==mTvSendPhone.getVisibility()){
                    setPhoneGone();
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_query_phone://查询
                onQueryPhone();
                break;
            case R.id.tv_time://获取短信验证码
                // 2022-07-25 先请求接口 接口返回发送短信验证码成功 然后开始倒计时
                sendSms();
                break;
            case R.id.tv_update://确认修改
                onUpdate();
                break;
        }
    }

    //根据填写的账户 查询绑定的手机号
    private void onQueryPhone() {
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString())){
            if (1==from){
                showShortToast("请输入您的机构编码");
            }else {
                showShortToast("请输入您的人员编码");
            }

            return;
        }
        showWaitDialog();
        QueryPhoneModel.sendQueryPhoneRequest(TAG, mEtPhone.getText().toString(), new CustomerJsonCallBack<QueryPhoneModel>() {
            @Override
            public void onRequestError(QueryPhoneModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
                setPhoneGone();
            }

            @Override
            public void onRequestSuccess(QueryPhoneModel returnData) {
                hideWaitDialog();
                QueryPhoneModel.DataBean data = returnData.getData();
                if (data!=null){
                    phone=data.getPhone();
                    if (!EmptyUtils.isEmpty(phone)){
                        mTvSendPhone.setText("我们将发送验证码到您的手机号"+ StringUtils.encryptionPhone(phone) +"上，请注意查收");

                        mLlNewpwd.setVisibility(View.VISIBLE);
                        mLineNewpwd.setVisibility(View.VISIBLE);
                        mTvNewpwdDescribe.setVisibility(View.VISIBLE);
                        mLlAgainPwd.setVisibility(View.VISIBLE);
                        mLineAgainPwd.setVisibility(View.VISIBLE);
                        mTvSendPhone.setVisibility(View.VISIBLE);
                        mLlSendPhone.setVisibility(View.VISIBLE);
                        mLineSendPhone.setVisibility(View.VISIBLE);
                        mTvUpdate.setVisibility(View.VISIBLE);
                    }else {
                        setPhoneGone();
                    }
                }else {
                    setPhoneGone();
                }
            }
        });
    }

    //将手机号 验证码隐藏
    private void setPhoneGone() {
        mLlNewpwd.setVisibility(View.GONE);
        mLineNewpwd.setVisibility(View.GONE);
        mTvNewpwdDescribe.setVisibility(View.GONE);
        mLlAgainPwd.setVisibility(View.GONE);
        mLineAgainPwd.setVisibility(View.GONE);
        mTvSendPhone.setVisibility(View.GONE);
        mLlSendPhone.setVisibility(View.GONE);
        mLineSendPhone.setVisibility(View.GONE);
        mTvUpdate.setVisibility(View.GONE);
        mEtVerifCode.setText("");
    }

    //发送短信
    private void sendSms() {
        if (EmptyUtils.isEmpty(phone) || phone.length() < 11) {
            showShortToast("手机号码格式不正确!");
            return;
        }
        showWaitDialog();
        BaseModel.sendSendsmsRequest(TAG, phone, "9", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();

                //发送短信验证码成功
                mTvTime.setEnabled(false);
                showShortToast("验证码已发送");
                mTvTime.setTextColor(getResources().getColor(R.color.color_ff0202));
                setTimeStart(60000);
            }
        });
    }

    //开启定时器  millisecond:倒计时的时间
    private void setTimeStart(long millisecond) {
        //防止内存泄漏
        if (null != mCustomCountDownTimer) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        mCustomCountDownTimer = new CustomCountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long S = millisUntilFinished / 1000;
                mTvTime.setText(S < 10 ? "0" + S + "s" : S + "s");
            }

            @Override
            public void onFinish() {
                mTvTime.setEnabled(true);
                mTvTime.setTextColor(getResources().getColor(R.color.color_4870e0));
                mTvTime.setText("获取验证码");
            }
        };

        mCustomCountDownTimer.start();
    }

    //确认修改
    private void onUpdate() {
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString().trim())) {
            showShortToast("请输入您的账号!");
            return;
        }
        if (EmptyUtils.isEmpty(mEtNewPwd.getText().toString())||mEtNewPwd.getText().toString().length() < 6) {
            showShortToast("密码由6~12位数字组成!");
            return;
        }
        if (!mEtNewPwd.getText().toString().trim().equals(mEtAgainPwd.getText().toString().trim())) {
            showShortToast("两次密码输入不一样，请重新输入");
            return;
        }
        if (EmptyUtils.isEmpty(mEtVerifCode.getText().toString().trim())) {
            showShortToast("请输入验证码!");
            return;
        }
        // 2022-12-28 请求接口 修改密码
        showWaitDialog();
        BaseModel.sendForgetPasswordRequest(TAG, mEtPhone.getText().toString(), mEtNewPwd.getText().toString(), mEtVerifCode.getText().toString(), phone, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("密码修改成功");
                LoginActivity.newIntance(ForgetPwdActivity.this);
                goFinish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mCustomCountDownTimer != null) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
    }

    //from 1:查找管理员密码 2.查找除了管理员之外人员的密码
    public static void newIntance(Context context,int from) {
        Intent intent = new Intent(context, ForgetPwdActivity.class);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }
}
