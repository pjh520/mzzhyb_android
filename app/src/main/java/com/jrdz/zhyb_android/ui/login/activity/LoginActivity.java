package com.jrdz.zhyb_android.ui.login.activity;

import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.fastjson.JSON;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.MD5Util;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.permission.PermissionHelper;
import com.frame.compiler.widget.tabLayout.CommonTabLayout;
import com.frame.compiler.widget.tabLayout.listener.OnTabSelectListener;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.database.MechanismInfoModel;
import com.jrdz.zhyb_android.database.UserInfoModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.MainActivity;
import com.jrdz.zhyb_android.ui.home.model.DoctorManageModel;
import com.jrdz.zhyb_android.ui.index.activity.ActivateDeviceActivity;
import com.jrdz.zhyb_android.ui.index.model.ActivateDeviceModel;
import com.jrdz.zhyb_android.ui.login.model.LoginModel;
import com.jrdz.zhyb_android.utils.AppManager_Acivity;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.DeviceID;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MMKVUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.utils.textutils.TextLightUtils;
import com.jrdz.zhyb_android.widget.pop.DropDownDataPop;
import com.jrdz.zhyb_android.widget.pop.DropDownLoadMorePop;
import com.tencent.bugly.crashreport.CrashReport;

import java.util.ArrayList;
import java.util.List;

import cn.jpush.android.api.JPushInterface;

public class LoginActivity extends BaseActivity {
    private FrameLayout mFlClose;
    private CommonTabLayout mStbTab;
    private ShapeLinearLayout mSllPhone;
    private EditText mEtPhone;
    private EditText mEtPwd;
    private TextView mTvForgetPwd, mTvRegist, mTvLogin;
    private TextView mTvAccountList, mTvDescribe, mTvAgreeRule;
    private FrameLayout flCb;
    private CheckBox cb;

    private PermissionHelper permissionHelper;
    private String guid = "";
    private CustomerDialogUtils customerDialogUtils;
    private DropDownLoadMorePop dropDownDataPop;
    private String type = "1";
    private TextLightUtils textLightUtils;
    private String fixmedins_code="",fixmedins_name="";


    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void initView() {
        super.initView();
        mFlClose = findViewById(R.id.fl_close);
        mStbTab = findViewById(R.id.stb_tab);
        mSllPhone = findViewById(R.id.sll_phone);
        mEtPhone = findViewById(R.id.et_phone);
        mTvAccountList = findViewById(R.id.tv_account_list);
        mTvDescribe = findViewById(R.id.tv_describe);
        mEtPwd = findViewById(R.id.et_pwd);
        mTvForgetPwd = findViewById(R.id.tv_forget_pwd);
        mTvRegist = findViewById(R.id.tv_regist);
        mTvLogin = findViewById(R.id.tv_login);
        mTvAgreeRule = findViewById(R.id.tv_agree_rule);
        flCb = findViewById(R.id.fl_cb);
        cb = findViewById(R.id.cb);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(false, 0.2f)
                .titleBarMarginTop(mFlClose);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        super.initData();
        //1.清空数据库存储的机构信息
        MechanismInfoUtils.clearAccount();
        //2.清空数据库存储的用户信息
        LoginUtils.clearAccount(false);
        //3.申请存储权限
        getPermission();
        //初始化tablayout
        mStbTab.setTabData(CommonlyUsedDataUtils.getInstance().getLoginTabData());

        textLightUtils = new TextLightUtils();
        textLightUtils.setHighlightColor(mTvAgreeRule, new TextLightUtils.IClickableSpan() {
            @Override
            public void onClickable(String title, String url) {
                MyWebViewActivity.newIntance(LoginActivity.this, title, url,
                        true, false);
            }
        });
        //模拟管理员登录
//        mEtPhone.setText("H61080200145");
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mStbTab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                dropDownDataPop=null;
                switch (position) {
                    case 0://管理员
                        type = "1";
                        mTvDescribe.setText("提示:账号为机构编码，例如:H620879754。");
                        mTvRegist.setVisibility(View.VISIBLE);
                        break;
                    case 1://其他人员
                        //fixmedins_code + "&" + fixmedins_name
                        String zhyf_account_history_other = MMKVUtils.getString("zhyf_account_history_other");
                        if (!EmptyUtils.isEmpty(zhyf_account_history_other)){
                            String[] mechanismInfo = zhyf_account_history_other.split("&");
                            fixmedins_code=mechanismInfo[0];
                            fixmedins_name=mechanismInfo[1];
                        }

                        type = "2";
                        mTvDescribe.setText("提示:账号为新增人员时填写的人员编码。");
                        mTvRegist.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onTabReselect(int position) {
            }
        });

        mFlClose.setOnClickListener(this);
        mTvForgetPwd.setOnClickListener(this);
        mTvRegist.setOnClickListener(this);
        mTvLogin.setOnClickListener(this);
        flCb.setOnClickListener(this);
        mTvAccountList.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close://关闭
                AppManager_Acivity.getInstance().finishAllActivity();
                break;
            case R.id.tv_account_list://账号列表
                getHistoryData();
                break;
            case R.id.tv_forget_pwd://忘记密码
                ForgetPwdActivity.newIntance(this, "1".equals(type) ? 1 : 2);
                break;
            case R.id.tv_regist://注册激活
                ActivateDeviceActivity.newIntance(LoginActivity.this);
                break;
            case R.id.fl_cb:
                if (cb.isChecked()) {
                    cb.setChecked(false);
                } else {
                    cb.setChecked(true);
                }
                break;
            case R.id.tv_login://登录
                login();
                break;
        }
    }

    //申请存储权限
    private void getPermission() {
        permissionHelper = new PermissionHelper();
        permissionHelper.requestPermission(this, new PermissionHelper.onPermissionListener() {
            @Override
            public void onSuccess() {
                getUniId();
            }

            @Override
            public void onNoAllSuccess(String noAllSuccessText) {

            }

            @Override
            public void onFail(String failText) {
                showShortToast("权限申请失败，app将不能登录");
            }
        }, "存储权限:登录的时候,需要存储app的唯一码到手机", Permission.MANAGE_EXTERNAL_STORAGE);
    }

    //获取唯一码
    private void getUniId() {
        showWaitDialog();
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    guid = DeviceID.getAndroidUniId();//f9665089da844c75ab11442860d1f2de
                    // 也可以通过CrashReport类设置，适合无法在初始化sdk时获取到deviceId的场景，context和deviceId不能为空（或空字符串）
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!EmptyUtils.isEmpty(guid)) {
                                CrashReport.setDeviceId(LoginActivity.this, guid);
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                hideWaitDialog();
            }
        });
    }

    //登录
    private void login() {
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString())) {
            showShortToast("请输入账号");
            return;
        }

        if (EmptyUtils.isEmpty(mEtPwd.getText().toString())) {
            showShortToast("请输入密码");
            return;
        }

        if (EmptyUtils.isEmpty(guid)) {
            showShortToast("请到系统设置页面手动授予权限");
            // 如果是被永久拒绝就跳转到应用权限系统设置页面
            XXPermissions.startPermissionActivity(LoginActivity.this, Permission.Group.STORAGE);
            return;
        }

        if (!cb.isChecked()) {
            if (customerDialogUtils == null) {
                customerDialogUtils = new CustomerDialogUtils();
            }
            customerDialogUtils.showDialog(LoginActivity.this, "提示", "登录代表同意《用户协议》和《隐私政策》", "取消", "同意并授权",
                    new CustomerDialogUtils.IDialogListener() {

                        @Override
                        public void onBtn01Click() {
                        }

                        @Override
                        public void onBtn02Click() {
                            cb.setChecked(true);
                        }
                    });
            return;
        }

        showWaitDialog();
        LoginModel.sendLoginRequest(TAG, type, mEtPhone.getText().toString(), mEtPwd.getText().toString(),
                EmptyUtils.strEmpty(JPushInterface.getRegistrationID(this)), new CustomerJsonCallBack<LoginModel>() {
                    @Override
                    public void onRequestError(LoginModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(LoginModel returnData) {
                        LoginModel.DataBean data = returnData.getData();
                        //登录成功 保存登录的账户
                        if (data != null) {
                            UserInfoModel userInfoModel = new UserInfoModel(EmptyUtils.strEmpty(data.getType()), EmptyUtils.strEmpty(data.getDr_type()), EmptyUtils.strEmpty(data.getDr_type_name()), EmptyUtils.strEmpty(data.getHosp_dept_codg()), EmptyUtils.strEmpty(data.getHosp_dept_name()),
                                    EmptyUtils.strEmpty(data.getDr_code()), EmptyUtils.strEmpty(data.getDr_name()), MD5Util.up32(mEtPwd.getText().toString()), EmptyUtils.strEmpty(data.getDr_phone()), EmptyUtils.strEmpty(data.getCaty()),
                                    EmptyUtils.strEmpty(data.getAccessoryId()), EmptyUtils.strEmpty(data.getAccessoryUrl()),
                                    EmptyUtils.strEmpty(data.getSubAccessoryId()), EmptyUtils.strEmpty(data.getAccessoryUrl1()), EmptyUtils.strEmpty(data.getIDNumber()),
                                    EmptyUtils.strEmpty(data.getApproveStatus()), EmptyUtils.strEmpty(data.getThrAccessoryUrl()), EmptyUtils.strEmpty(data.getThrAccessoryId()));

                            userInfoModel.save();
                            //记录登录账户的 对应的机构信息
                            saveHistoryData(mEtPhone.getText().toString(), data.getFixmedins_code(), data.getFixmedins_name());
                            //登录成功之后 需要获取机构信息
                            getMechanismInfo(data.getFixmedins_code(), data.getFixmedins_name());
                        } else {
                            hideWaitDialog();
                            showShortToast("数据有误，请重新登录");
                        }
                    }
                });
    }

    //获取机构信息
    private void getMechanismInfo(String fixmedins_code, String fixmedins_name) {
        ActivateDeviceModel.sendQueryMedinsinfoRequest(TAG, fixmedins_code, fixmedins_name, new CustomerJsonCallBack<ActivateDeviceModel>() {
            @Override
            public void onRequestError(ActivateDeviceModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(ActivateDeviceModel returnData) {
                ActivateDeviceModel.DataBean data = returnData.getData();
                if (data != null) {
                    //存储定点机构信息到数据库
                    MechanismInfoModel mechanismInfoModel = new MechanismInfoModel(data.getFixmedins_code(), data.getFixmedins_name(), data.getAdmdvs(),
                            data.getFixmedins_type(), data.getHosp_lv(), data.getMedinsLv(), data.getUscc(), data.getUnions(), data.getPhone(),
                            data.getElectronicPrescription(), data.getOutpatientMdtrtinfo(), data.getAccessoryId(), data.getAccessoryUrl(),
                            data.getApproveStatus(), data.getEntryMode(), data.getVoiceNotification(),data.getIsEnterpriseFundPay(),data.getIsOTO());
                    mechanismInfoModel.save();
                    onLoginSuccess();
                } else {
                    hideWaitDialog();
                    showShortToast("请求返回数据有误");
                }
            }
        });
    }

    //登录成功
    private void onLoginSuccess() {
        MsgBus.sendLoginStatus().post(1);
        hideWaitDialog();
        goMainPage();
    }

    //跳转首页
    private void goMainPage() {
        MainActivity.newIntance(LoginActivity.this, 0);
        overridePendingTransition(R.anim.screen_zoom_in, R.anim.screen_zoom_out);
        finish();
    }

    //保存搜索数据
    private void saveHistoryData(String accountText, String fixmedins_code, String fixmedins_name) {
        if ("1".equals(type)) {//管理员登录
            //获取历史搜索本地记录
            List<String> shopHistoryDatas = JSON.parseArray(MMKVUtils.getString("zhyf_account_history_mana"), String.class);
            if (shopHistoryDatas == null) {
                shopHistoryDatas = new ArrayList<>();
            } else {
                if (shopHistoryDatas.contains(accountText)) {
                    shopHistoryDatas.remove(accountText);
                }

                if (shopHistoryDatas.size() >= 5) {
                    shopHistoryDatas.remove(4);
                }
            }
            shopHistoryDatas.add(0, accountText);
            MMKVUtils.putString("zhyf_account_history_mana", JSON.toJSONString(shopHistoryDatas));
        }
        MMKVUtils.putString("zhyf_account_history_other", fixmedins_code + "&" + fixmedins_name);
    }

    //获取历史搜索记录
    private void getHistoryData() {
        if ("1".equals(type)) {//管理员登录
            //获取历史搜索本地记录
            List<String> shopHistoryDatas = JSON.parseArray(MMKVUtils.getString("zhyf_account_history_mana"), String.class);
            showAccountPop(shopHistoryDatas);
        } else {//其他人员登录
            if (dropDownDataPop == null) {
                if (!EmptyUtils.isEmpty(fixmedins_code)&&!EmptyUtils.isEmpty(fixmedins_name)){
                    showWaitDialog();
                    getDoctorData(0);
                }else {
                    showAccountPop(null);
                }
            } else {
                if (!dropDownDataPop.isShowing()) {
                    dropDownDataPop.showPopupWindow(mSllPhone);
                }
            }
        }
    }

    //获取医师账号数据
    public void getDoctorData(int mPageNum) {
        DoctorManageModel.sendDoctorManageModelRequest2(TAG,String.valueOf(mPageNum),"10",fixmedins_code,fixmedins_name, new CustomerJsonCallBack<DoctorManageModel>() {
            @Override
            public void onRequestError(DoctorManageModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(DoctorManageModel returnData) {
                hideWaitDialog();
                if (returnData.getData() != null ) {
                    List<DoctorManageModel.DataBean> infos = returnData.getData();
                    ArrayList<String> doctorDatas=new ArrayList<>();
                    for (DoctorManageModel.DataBean info : infos) {
                        doctorDatas.add(info.getDr_code());
                    }
                    showAccountPop(doctorDatas);
                }
            }
        });
    }

    private void showAccountPop(List<String> datas) {
        if (dropDownDataPop == null) {
            dropDownDataPop = new DropDownLoadMorePop(LoginActivity.this, datas,"1".equals(type)?false:true);

            dropDownDataPop.setOnListener(new DropDownLoadMorePop.IOptionListener() {
                @Override
                public void onItemClick(String item) {
                    mEtPhone.setText(EmptyUtils.strEmpty(item));
                }

                @Override
                public void onLoadMore(int mPageNum) {
                    getDoctorData(mPageNum);
                }
            });
        } else {
            if ("2".equals(type)){
                dropDownDataPop.setData(datas);
            }
        }
        if (!dropDownDataPop.isShowing()) {
            dropDownDataPop.showPopupWindow(mSllPhone);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == XXPermissions.REQUEST_CODE) {
            if (XXPermissions.isGranted(this, Permission.Group.STORAGE)) {
                getUniId();
            }
        }
    }

    //Activity屏蔽物理返回按钮
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AppManager_Acivity.getInstance().finishAllActivity();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        if (textLightUtils != null) {
            textLightUtils.onDestoryListener();
        }
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }

        if (dropDownDataPop != null) {
            dropDownDataPop.onCleanListener();
            dropDownDataPop.onDestroy();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }
}
