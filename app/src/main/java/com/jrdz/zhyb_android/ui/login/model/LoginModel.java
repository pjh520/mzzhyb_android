package com.jrdz.zhyb_android.ui.login.model;

import com.alibaba.fastjson.JSONObject;
import com.frame.compiler.utils.MD5Util;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/8
 * 描    述：
 * ================================================
 */
public class LoginModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-01-19 09:53:00
     * data : {"type":1,"drType":"","drTypeName":"","deptId":"","deptIdName":""}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * type : 0
         * dr_type : 1
         * dr_type_name : 医师
         * hosp_dept_codg : 22
         * hosp_dept_name : 肾内科
         * dr_code : 123456
         * dr_name : 王医生
         */
        private String type;
        private String dr_type;
        private String dr_type_name;
        private String hosp_dept_codg;
        private String hosp_dept_name;
        private String dr_code;
        private String dr_name;
        private String dr_phone;
        private String caty;
        private String accessoryId;
        private String subAccessoryId;
        private String thrAccessoryId;
        private String accessoryUrl;
        private String accessoryUrl1;
        private String thrAccessoryUrl;//头像
        private String IDNumber;//身份证号码

        private String fixmedins_code;
        private String fixmedins_name;
        //智慧药房版本-新增店铺审核状态
        private String ApproveStatus;//（0、从未提交审核1、审核中2、审核未通过3、审核通过4、禁止审核）


        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDr_type() {
            return dr_type;
        }

        public void setDr_type(String dr_type) {
            this.dr_type = dr_type;
        }

        public String getDr_type_name() {
            return dr_type_name;
        }

        public void setDr_type_name(String dr_type_name) {
            this.dr_type_name = dr_type_name;
        }

        public String getHosp_dept_codg() {
            return hosp_dept_codg;
        }

        public void setHosp_dept_codg(String hosp_dept_codg) {
            this.hosp_dept_codg = hosp_dept_codg;
        }

        public String getHosp_dept_name() {
            return hosp_dept_name;
        }

        public void setHosp_dept_name(String hosp_dept_name) {
            this.hosp_dept_name = hosp_dept_name;
        }

        public String getDr_code() {
            return dr_code;
        }

        public void setDr_code(String dr_code) {
            this.dr_code = dr_code;
        }

        public String getDr_name() {
            return dr_name;
        }

        public void setDr_name(String dr_name) {
            this.dr_name = dr_name;
        }

        public String getDr_phone() {
            return dr_phone;
        }

        public void setDr_phone(String dr_phone) {
            this.dr_phone = dr_phone;
        }

        public String getCaty() {
            return caty;
        }

        public void setCaty(String caty) {
            this.caty = caty;
        }

        public String getAccessoryId() {
            return accessoryId;
        }

        public void setAccessoryId(String accessoryId) {
            this.accessoryId = accessoryId;
        }

        public String getSubAccessoryId() {
            return subAccessoryId;
        }

        public void setSubAccessoryId(String subAccessoryId) {
            this.subAccessoryId = subAccessoryId;
        }

        public String getAccessoryUrl() {
            return accessoryUrl;
        }

        public void setAccessoryUrl(String accessoryUrl) {
            this.accessoryUrl = accessoryUrl;
        }

        public String getAccessoryUrl1() {
            return accessoryUrl1;
        }

        public void setAccessoryUrl1(String accessoryUrl1) {
            this.accessoryUrl1 = accessoryUrl1;
        }

        public String getThrAccessoryId() {
            return thrAccessoryId;
        }

        public void setThrAccessoryId(String thrAccessoryId) {
            this.thrAccessoryId = thrAccessoryId;
        }

        public String getThrAccessoryUrl() {
            return thrAccessoryUrl;
        }

        public void setThrAccessoryUrl(String thrAccessoryUrl) {
            this.thrAccessoryUrl = thrAccessoryUrl;
        }

        public String getIDNumber() {
            return IDNumber;
        }

        public void setIDNumber(String IDNumber) {
            this.IDNumber = IDNumber;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getFixmedins_name() {
            return fixmedins_name;
        }

        public void setFixmedins_name(String fixmedins_name) {
            this.fixmedins_name = fixmedins_name;
        }

        public String getApproveStatus() {
            return ApproveStatus;
        }

        public void setApproveStatus(String approveStatus) {
            ApproveStatus = approveStatus;
        }
    }

    public static void sendLoginRequest(final String TAG,String type, String username, String password,String RegistrationId,
                                        final CustomerJsonCallBack<LoginModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", type);
        jsonObject.put("RegistrationId", RegistrationId);
        RequestData.requesNetWork_Json3(TAG,username, MD5Util.up32(password),Constants.BASE_URL+ Constants.Api.GET_LOGIN_URL,jsonObject.toJSONString(), callback);
    }
}
