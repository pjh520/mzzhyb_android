package com.jrdz.zhyb_android.ui.login.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.login.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-02-16
 * 描    述：
 * ================================================
 */
public class QueryPhoneModel {
    /**
     * code : 1
     * msg : 成功!
     * server_time : 2023-02-16 16:45:09
     * data : {"phone":"15060338985"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * phone : 15060338985
         */

        private String phone;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }

    public static void sendQueryPhoneRequest(final String TAG, String username, final CustomerJsonCallBack<QueryPhoneModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", username);
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL+ Constants.Api.GET_PHONEBYACCOUNT_URL,jsonObject.toJSONString(), callback);
    }
}
