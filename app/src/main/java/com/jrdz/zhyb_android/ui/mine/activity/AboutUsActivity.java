package com.jrdz.zhyb_android.ui.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.RxTool;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.constant.Constants;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.mine.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/1/22
 * 描    述：关于我们
 * ================================================
 */
public class AboutUsActivity extends BaseActivity {
    private TextView mTvVersion;
    private TextView mTvUserRule;
    private TextView mTvPrivacyPolicy, mTvBeian;

    @Override
    public int getLayoutId() {
        return R.layout.activity_aboutus;
    }

    @Override
    public void initView() {
        super.initView();
        mTvVersion = findViewById(R.id.tv_version);
        mTvUserRule = findViewById(R.id.tv_user_rule);
        mTvPrivacyPolicy = findViewById(R.id.tv_privacy_policy);
        mTvBeian = findViewById(R.id.tv_beian);
    }

    @Override
    public void initData() {
        super.initData();

        mTvVersion.setText("版本号：" + RxTool.getVersion(this) + Constants.Configure.VERSIONNAME_TAG);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvUserRule.setOnClickListener(this);
        mTvPrivacyPolicy.setOnClickListener(this);
        mTvBeian.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_user_rule://用户协议
                MyWebViewActivity.newIntance(AboutUsActivity.this, "用户协议", Constants.BASE_URL+Constants.WebUrl.USER_AGREEMENT_URL,true, false);
                break;
            case R.id.tv_privacy_policy://隐私政策
                MyWebViewActivity.newIntance(AboutUsActivity.this, "隐私政策", Constants.BASE_URL + Constants.WebUrl.PRIVACY_POLICY_URL, true, false);
                break;
            case R.id.tv_beian://icp备案
                MyWebViewActivity.newIntance(AboutUsActivity.this, "ICP备案查询", Constants.WebUrl.ICP_BEIAN_URL, true, false);
                break;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, AboutUsActivity.class);
        context.startActivity(intent);
    }
}
