package com.jrdz.zhyb_android.ui.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.IsInstallUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.ui.login.activity.LoginActivity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.mine.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/9
 * 描    述：应用协议
 * ================================================
 */
public class AgreementActivtiy extends BaseActivity {
    private TextView mYhysButton;
    private TextView mFwzcButton;

    @Override
    public int getLayoutId() {
        return R.layout.activity_service_agreement;
    }

    @Override
    public void initView() {
        super.initView();
        mYhysButton = findViewById(R.id.yhysButton);
        mFwzcButton = findViewById(R.id.fwzcButton);
    }

    @Override
    public void initData() {
        super.initData();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mYhysButton.setOnClickListener(this);
        mFwzcButton.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.yhysButton://用户协议
                MyWebViewActivity.newIntance(AgreementActivtiy.this, "用户协议", Constants.BASE_URL+Constants.WebUrl.USER_AGREEMENT_URL,true, false);
                break;
            case R.id.fwzcButton://隐私协议
                MyWebViewActivity.newIntance(AgreementActivtiy.this, "隐私政策", Constants.BASE_URL + Constants.WebUrl.PRIVACY_POLICY_URL, true, false);
                break;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, AgreementActivtiy.class);
        context.startActivity(intent);
    }
}
