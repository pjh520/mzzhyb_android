package com.jrdz.zhyb_android.ui.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.FileUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.MMKVUtils;
import com.jrdz.zhyb_android.widget.pop.UpdatePathPop;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.mine.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-05-20
 * 描    述：下载路径设置
 * ================================================
 */
public class DownloadPathActivity extends BaseActivity {
    private TextView mTvLogPath;
    private ShapeTextView mStvLogClean,mStvLogPath;
    private TextView mTvPrescrPath;
    private ShapeTextView mStvPrescrClean,mStvPrescrPath;
    private UpdatePathPop updatePathPop;
    private CustomerDialogUtils customerDialogUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_download_path;
    }

    @Override
    public void initView() {
        super.initView();
        mTvLogPath = findViewById(R.id.tv_log_path);
        mStvLogClean= findViewById(R.id.stv_log_clean);
        mStvLogPath = findViewById(R.id.stv_log_path);

        mTvPrescrPath = findViewById(R.id.tv_prescr_path);
        mStvPrescrClean= findViewById(R.id.stv_prescr_clean);
        mStvPrescrPath = findViewById(R.id.stv_prescr_path);
    }

    @Override
    public void initData() {
        super.initData();

        mTvLogPath.setText(CommonlyUsedDataUtils.getInstance().getOutpatLogDir());
        mTvPrescrPath.setText(CommonlyUsedDataUtils.getInstance().getElecPrescrDir());
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mStvLogClean.setOnClickListener(this);
        mStvLogPath.setOnClickListener(this);

        mStvPrescrClean.setOnClickListener(this);
        mStvPrescrPath.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.stv_log_clean://清除门诊日志
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(DownloadPathActivity.this, "提示", "确定要清除门诊日志么？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {}

                    @Override
                    public void onBtn02Click() {
                        cleanLog();
                    }
                });
                break;
            case R.id.stv_log_path://门诊日志地址
                if (updatePathPop==null){
                    updatePathPop=new UpdatePathPop(DownloadPathActivity.this);
                }
                updatePathPop.setParentPath(BaseGlobal.suitBaseDir());
                updatePathPop.setContent(MMKVUtils.getString("OutpatLogDir", "/门诊日志/"));
                updatePathPop.setOnListener(new UpdatePathPop.IOptionListener() {
                    @Override
                    public void onAgree(String data) {
                        MMKVUtils.putString("OutpatLogDir", data);

                        mTvLogPath.setText(CommonlyUsedDataUtils.getInstance().getOutpatLogDir());
                    }

                    @Override
                    public void onCancle() {}
                });

                updatePathPop.showPopupWindow();
                break;
            case R.id.stv_prescr_clean://清除电子处方
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(DownloadPathActivity.this, "提示", "确定要清除电子处方么？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {}

                    @Override
                    public void onBtn02Click() {
                        cleanPrescr();
                    }
                });
                break;
            case R.id.stv_prescr_path://电子处方地址
                if (updatePathPop==null){
                    updatePathPop=new UpdatePathPop(DownloadPathActivity.this);
                }
                updatePathPop.setParentPath(BaseGlobal.suitBaseDir());
                updatePathPop.setContent(MMKVUtils.getString("ElecPrescrDir", "/电子处方/"));
                updatePathPop.setOnListener(new UpdatePathPop.IOptionListener() {
                    @Override
                    public void onAgree(String data) {
                        MMKVUtils.putString("ElecPrescrDir", data);
                        mTvPrescrPath.setText(CommonlyUsedDataUtils.getInstance().getElecPrescrDir());
                    }

                    @Override
                    public void onCancle() {}
                });

                updatePathPop.showPopupWindow();
                break;
        }
    }

    //清除门诊日志
    private void cleanLog() {
        showWaitDialog();
        ThreadUtils.executeBySingle(new ThreadUtils.SimpleTask<Void>() {
            @Nullable
            @Override
            public Void doInBackground() throws Throwable {
                FileUtils.deleteAllInDir(CommonlyUsedDataUtils.getInstance().getOutpatLogDir());
                return null;
            }

            @Override
            public void onSuccess(@Nullable Void result) {
                hideWaitDialog();
                showShortToast("清除成功");
            }
        });
    }

    //清除电子处方
    private void cleanPrescr() {
        showWaitDialog();
        ThreadUtils.executeBySingle(new ThreadUtils.SimpleTask<Void>() {
            @Nullable
            @Override
            public Void doInBackground() throws Throwable {
                FileUtils.deleteAllInDir(CommonlyUsedDataUtils.getInstance().getElecPrescrDir());
                return null;
            }

            @Override
            public void onSuccess(@Nullable Void result) {
                hideWaitDialog();
                showShortToast("清除成功");
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (updatePathPop != null) {
            updatePathPop.onCleanListener();
            updatePathPop = null;
        }
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, DownloadPathActivity.class);
        context.startActivity(intent);
    }
}
