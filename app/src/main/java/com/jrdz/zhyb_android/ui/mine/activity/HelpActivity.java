package com.jrdz.zhyb_android.ui.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.mine.adapter.HelpAdapter;
import com.jrdz.zhyb_android.ui.mine.model.HelpListModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.PdfViewActivity;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.mine.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/3/6
 * 描    述： 帮助中心
 * ================================================
 */
public class HelpActivity extends BaseRecyclerViewActivity {

    private CustomerDialogUtils customerDialogUtils;

    @Override
    public void initAdapter() {
        mAdapter = new HelpAdapter();
    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        super.getData();

        HelpListModel.sendHelpListRequest(TAG, String.valueOf(mPageNum), "20", new CustomerJsonCallBack<HelpListModel>() {
            @Override
            public void onRequestError(HelpListModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(HelpListModel returnData) {
                hideRefreshView();
                List<HelpListModel.DataBean> infos = returnData.getData();
                if (mAdapter!=null&&infos != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(infos);
                    } else {
                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        HelpListModel.DataBean data = ((HelpAdapter) adapter).getItem(position);
        switch (data.getType()) {
            // 1：电话2：文本内容3：网页链接4：视频文件5: PDF文件
            case "1"://电话
                showPhone(data.getLink());
                break;
            case "2"://文本内容
                HelpDetailActivity.newIntance(HelpActivity.this, data.getLink());
                break;
            case "3"://网页链接
                MyWebViewActivity.newIntance(HelpActivity.this, data.getTitle(), data.getLink(), true, false);
                break;
            case "4"://视频文件
                VideoPlaySingleActivity.newIntance(HelpActivity.this, false,data.getLink());
                break;
            case "5"://PDF文件
                //http://113.135.194.23:3081/App_Upload/Storage/HelpCenter/榆林智慧医保APP使用操作文档V1.0.0.pdf"
                if (EmptyUtils.isEmpty(data.getLink()))return;
                PdfViewActivity.newIntance(HelpActivity.this, data.getLink());
                break;
        }
    }

    //显示电话
    private void showPhone(String phone) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(HelpActivity.this, "提示", "是否拨打客服电话" + phone + "?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        RxTool.takePhone(HelpActivity.this, phone);
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, HelpActivity.class);
        context.startActivity(intent);
    }
}
