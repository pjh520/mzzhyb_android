package com.jrdz.zhyb_android.ui.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.NewsDetailActivity;
import com.jrdz.zhyb_android.ui.home.model.NewsDetailModel;
import com.jrdz.zhyb_android.ui.mine.model.HelpDetailModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.mine.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/3/9
 * 描    述： 帮助详情
 * ================================================
 */
public class HelpDetailActivity extends BaseActivity {
    private TextView mTvTitle;
    private TextView mTvTime;
    private TextView mTvContent;

    private String id="";

    @Override
    public int getLayoutId() {
        return R.layout.activity_help_detail;
    }

    @Override
    public void initView() {
        super.initView();
        mTvTitle = findViewById(R.id.tv_title);
        mTvTime = findViewById(R.id.tv_time);
        mTvContent = findViewById(R.id.tv_content);
    }

    @Override
    public void initData() {
        id=getIntent().getStringExtra("id");
        super.initData();

        showWaitDialog();
        getData();
    }

    //获取数据
    private void getData() {
        HelpDetailModel.sendHelpDetailRequest(TAG, id, new CustomerJsonCallBack<HelpDetailModel>() {
            @Override
            public void onRequestError(HelpDetailModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(HelpDetailModel returnData) {
                hideWaitDialog();
                HelpDetailModel.DataBean data = returnData.getData();
                if (data != null ) {
                    setTitle(EmptyUtils.strEmpty(data.getTitle()));
                    mTvTitle.setText(EmptyUtils.strEmpty(data.getTitle()));
                    mTvTime.setText(EmptyUtils.strEmpty(data.getCreateDT()));
                    mTvContent.setText(EmptyUtils.strEmpty(data.getContent()));
                }
            }
        });
    }

    public static void newIntance(Context context, String id) {
        Intent intent = new Intent(context, HelpDetailActivity.class);
        intent.putExtra("id", id);
        context.startActivity(intent);
    }
}
