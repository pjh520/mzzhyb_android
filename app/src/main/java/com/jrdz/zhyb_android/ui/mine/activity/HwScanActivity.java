package com.jrdz.zhyb_android.ui.mine.activity;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.annotation.Nullable;
import com.frame.compiler.widget.toast.ToastUtil;
import com.frame.compiler.widget.permission.PermissionHelper;
import com.hjq.permissions.Permission;
import com.huawei.hms.hmsscankit.OnLightVisibleCallBack;
import com.huawei.hms.hmsscankit.OnResultCallback;
import com.huawei.hms.hmsscankit.RemoteView;
import com.huawei.hms.ml.scan.HmsScan;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.widget.scan.ViewfinderView;

/**
 * ================================================
 * 项目名称：My Application
 * 包    名：com.example.myapplication.activity
 * 版    本：1.0
 * 创建日期：2021/10/13 0013
 * 描    述：华为扫码自定义页面
 * ================================================
 */
public class HwScanActivity extends BaseActivity{
    private FrameLayout mFlPreview;
    private ViewfinderView mViewfinderView;
    private ImageView mIvFlush;
    private FrameLayout mFlFlush;

    private RemoteView remoteView;
    private PermissionHelper permissionHelper;
    
    @Override
    protected void afterSetcontentView() {
        getWindow().setBackgroundDrawable(null);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_hwscan;
    }

    @Override
    public void initView() {
        super.initView();
        mFlPreview = findViewById(R.id.fl_preview);
        mViewfinderView = findViewById(R.id.viewfinderView);
        mIvFlush = findViewById(R.id.iv_flush);
        mFlFlush = findViewById(R.id.fl_flush);
        mTitleBar = findViewById(R.id.title_bar);
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        super.initData(savedInstanceState);

        initCamera(savedInstanceState);
        //获取相机权限
        permissionHelper = new PermissionHelper();
        permissionHelper.requestPermission(this, new PermissionHelper.onPermissionListener() {
            @Override
            public void onSuccess() {}

            @Override
            public void onNoAllSuccess(String noAllSuccessText) {}

            @Override
            public void onFail(String failText) {
                ToastUtil.show("相机权限申请失败");
            }
        }, "相机权限:扫码时,需要该权限,不然不能调用摄像头扫码", Permission.CAMERA);
    }

    @Override
    protected void initWaterMark() {

    }

    //初始化相机预览以及扫码参数
    private void initCamera(@Nullable Bundle savedInstanceState) {
        //1. 获取屏幕参数
        DisplayMetrics dm = getResources().getDisplayMetrics();
        int mScreenWidth = dm.widthPixels;
        int mScreenHeight = dm.heightPixels;
        //2.当前扫码框的宽高是dp_480。
        int scanFrameSize = getResources().getDimensionPixelSize(R.dimen.dp_750);
        //3.计算扫码区域矩形的坐标
        Rect rect = new Rect();
        rect.left = mScreenWidth / 2 - scanFrameSize / 2;
        rect.right = mScreenWidth / 2 + scanFrameSize / 2;
        rect.top = mScreenHeight / 2 - scanFrameSize / 2;
        rect.bottom = mScreenHeight / 2 + scanFrameSize / 2;

        //初始化RemoteView（相机预览view），并通过如下方法设置参数:setContext()（必选）传入context、
        //setBoundingBox()设置扫描区域、setFormat()设置识别码制式，设置完毕调用build()方法完成创建。
        //通过setContinuouslyScan（可选）方法设置非连续扫码模式。
        remoteView = new RemoteView.Builder().setContext(this)
                .setBoundingBox(rect)
                .setFormat(HmsScan.ALL_SCAN_TYPE)
                .setContinuouslyScan(false)
                .build();
        //当灯光变暗时，将调用此API以显示手电筒开关。
        remoteView.setOnLightVisibleCallback(new OnLightVisibleCallBack() {
            @Override
            public void onVisibleChanged(boolean visible) {
                if (visible) {
                    mFlFlush.setVisibility(View.VISIBLE);
                }
            }
        });
        //监听扫码回调事件
        remoteView.setOnResultCallback(new OnResultCallback() {
            @Override
            public void onResult(HmsScan[] result) {
                //获取到扫码结果HmsScan
                if (result != null && result.length > 0 && result[0] != null && !TextUtils.isEmpty(result[0].getOriginalValue())) {
                    showVibrator();
                    Intent intent = new Intent();
                    intent.putExtra("scanResult", result[0].getOriginalValue());
                    setResult(RESULT_OK, intent);
                    HwScanActivity.this.finish();
                }
            }
        });

        // Load the customized view to the activity.
        remoteView.onCreate(savedInstanceState);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        mFlPreview.addView(remoteView, params);

        setFlashOperation();
    }

    //设置震动
    private void showVibrator() {
        Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        vibrator.vibrate(300);
    }

    //设置闪光灯的开关
    private void setFlashOperation() {
        mFlFlush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIvFlush.setImageResource(remoteView.getLightStatus() ? R.drawable.flashlight_off : R.drawable.flashlight_on);
                remoteView.switchLight();
            }
        });
    }

    //remoteView 绑定页面的生命周期
    @Override
    protected void onStart() {
        super.onStart();
        remoteView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        remoteView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        remoteView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        remoteView.onStop();
    }

    @Override
    protected void onDestroy() {
        mViewfinderView.destroyView();
        super.onDestroy();
        if (mFlPreview!=null){
            mFlPreview.removeView(remoteView);
        }
        remoteView.onDestroy();
    }
}
