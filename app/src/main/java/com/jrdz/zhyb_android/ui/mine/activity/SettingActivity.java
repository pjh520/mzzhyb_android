package com.jrdz.zhyb_android.ui.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.switchButton.SwitchButton;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.UpdatePwdActivity;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_insured;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.mine.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/1/22
 * 描    述：设置
 * ================================================
 */
public class SettingActivity extends BaseActivity implements CompressUploadSinglePicUtils.IChoosePic {
    private ImageView mIvHead;
    private TextView mTvPhone;
    private LinearLayout mLlHead,mLlUpdatePwd,mLlDownloadPath,mLlElectPrescr,mLlOutpatientLog,mLlUpdateUnions;
    private SwitchButton mSbElectPrescr,mSbOutpatientLog;

    private String electronicPrescription="1",outpatientMdtrtinfo="1";
    private CompressUploadSinglePicUtils compressUploadSinglePicUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_setting;
    }

    @Override
    public void initView() {
        super.initView();
        mIvHead = findViewById(R.id.iv_head);
        mLlHead = findViewById(R.id.ll_head);
        mTvPhone = findViewById(R.id.tv_phone);
        mLlUpdatePwd = findViewById(R.id.ll_update_pwd);
        mLlDownloadPath= findViewById(R.id.ll_download_path);
        mLlElectPrescr = findViewById(R.id.ll_elect_prescr);
        mSbElectPrescr = findViewById(R.id.sb_elect_prescr);
        mLlOutpatientLog = findViewById(R.id.ll_outpatient_log);
        mSbOutpatientLog = findViewById(R.id.sb_outpatient_log);
        mLlUpdateUnions= findViewById(R.id.ll_update_unions);
    }

    @Override
    public void initData() {
        super.initData();
        if ("1".equals(MechanismInfoUtils.getFixmedinsType())){//定点医院
            mLlDownloadPath.setVisibility(View.VISIBLE);
            if (LoginUtils.isManage()){
                mLlElectPrescr.setVisibility(View.VISIBLE);
                mLlOutpatientLog.setVisibility(View.VISIBLE);
                if (Constants.Configure.IS_POS){
                    mLlUpdateUnions.setVisibility(View.VISIBLE);
                }else {
                    mLlUpdateUnions.setVisibility(View.GONE);
                }
            }else {
                mLlElectPrescr.setVisibility(View.GONE);
                mLlOutpatientLog.setVisibility(View.GONE);
                mLlUpdateUnions.setVisibility(View.GONE);
            }
        }else {//定点药店
            mLlDownloadPath.setVisibility(View.GONE);
            mLlElectPrescr.setVisibility(View.GONE);
            mLlOutpatientLog.setVisibility(View.GONE);
            mLlUpdateUnions.setVisibility(View.GONE);
        }
        GlideUtils.loadImg(LoginUtils.getThrAccessoryUrl(), mIvHead,R.drawable.ic_insured_head,new CircleCrop());
        mTvPhone.setText(StringUtils.encryptionPhone(LoginUtils.getDr_phone()));

        //设置电子处方是否开启
        electronicPrescription=MechanismInfoUtils.getElectronicPrescription();
        mSbElectPrescr.setCheckedImmediatelyNoEvent("1".equals(electronicPrescription) ? true : false);

        //设置门诊日志是否开启
        outpatientMdtrtinfo=MechanismInfoUtils.getOutpatientMdtrtinfo();
        mSbOutpatientLog.setCheckedImmediatelyNoEvent("1".equals(outpatientMdtrtinfo) ? true : false);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mLlHead.setOnClickListener(this);
        mLlUpdatePwd.setOnClickListener(this);
        mLlDownloadPath.setOnClickListener(this);
        mLlUpdateUnions.setOnClickListener(this);
        mSbElectPrescr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                electronicPrescription=b?"1":"0";
                setSystem();
            }
        });

        mSbOutpatientLog.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                outpatientMdtrtinfo=b?"1":"0";
                setSystem();
            }
        });
    }

    //设置系统数据
    private void setSystem(){
        showWaitDialog();
        BaseModel.sendSetSystemRequest(TAG, electronicPrescription, outpatientMdtrtinfo, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();

                MechanismInfoUtils.setElectronicPrescription(electronicPrescription);
                MechanismInfoUtils.setOutpatientMdtrtinfo(outpatientMdtrtinfo);
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.ll_head://选择图片上传头像
                if (compressUploadSinglePicUtils==null){
                    compressUploadSinglePicUtils = new CompressUploadSinglePicUtils();
                    compressUploadSinglePicUtils.initChoosePic(this, true, SettingActivity.this);
                }

                compressUploadSinglePicUtils.showChoosePicTypeDialog(CompressUploadSinglePicUtils_insured.PIC_HEAD_MANA_TAG);
                break;
            case R.id.ll_update_pwd://密码修改
                UpdatePwdActivity.newIntance(SettingActivity.this);
                break;
            case R.id.ll_download_path://下载路径
                DownloadPathActivity.newIntance(SettingActivity.this);
                break;
            case R.id.ll_update_unions://修改打印联数
                UpdateUnionsActivity.newIntance(SettingActivity.this);
                break;
        }
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            GlideUtils.loadImg(url, mIvHead,R.drawable.ic_insured_head,new CircleCrop());
            //2022-09-29 更新到本地数据库 并通知其他页面更新头像
            LoginUtils.setThrAccessoryId(accessoryId);
            LoginUtils.setThrAccessoryUrl(url);
            MsgBus.updateManaInfo().post("2");
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();

    }

    @Override
    public void onHideWait() {
        hideWaitDialog();

    }
    //--------------------------选择图片----------------------------------------------------

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, SettingActivity.class);
        context.startActivity(intent);
    }
}
