package com.jrdz.zhyb_android.ui.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.mine.adapter.UnionsAdapter;
import com.jrdz.zhyb_android.ui.mine.model.UnionsModel;
import com.jrdz.zhyb_android.ui.mine.model.UpdateUnionsModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.mine.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-08-26
 * 描    述：修改打印联数
 * ================================================
 */
public class UpdateUnionsActivity extends BaseActivity implements BaseQuickAdapter.OnItemClickListener {
    private CustomeRecyclerView mCrlUnions;
    private ShapeTextView mTvUpdate;

    private UnionsAdapter unionsAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_unions;
    }

    @Override
    public void initView() {
        super.initView();
        mCrlUnions= findViewById(R.id.crl_unions);
        mTvUpdate = findViewById(R.id.tv_update);
    }

    @Override
    public void initData() {
        super.initData();

        mCrlUnions.setHasFixedSize(true);
        mCrlUnions.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
        unionsAdapter=new UnionsAdapter();
        mCrlUnions.setAdapter(unionsAdapter);
        unionsAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getUnionsData(MechanismInfoUtils.getUnions()));
        unionsAdapter.setOnItemClickListener(this);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvUpdate.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()) {
            case R.id.tv_update://更新联数
                if (unionsAdapter.getCurrentUnionsModel()==null){
                    showShortToast("请选择联数");
                    return;
                }
                showWaitDialog();
                UpdateUnionsModel.sendUpdateUnionsRequest(TAG, unionsAdapter.getCurrentUnionsModel().getText(), new CustomerJsonCallBack<UpdateUnionsModel>() {
                    @Override
                    public void onRequestError(UpdateUnionsModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(UpdateUnionsModel returnData) {
                        hideWaitDialog();

                        UpdateUnionsModel.DataBean data = returnData.getData();
                        if (data != null) {
                            MechanismInfoUtils.setUnions(EmptyUtils.strEmpty(data.getUnions()));
                            showShortToast("修改打印联数成功！");
                        }else {
                            showShortToast("返回数据有误，请重新修改！");
                        }
                    }
                });
                break;
        }
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            return;
        }
        UnionsAdapter unionsAdapter=(UnionsAdapter) baseQuickAdapter;
        UnionsModel item = unionsAdapter.getItem(i);
        if (!item.isSelect()) {
            if (unionsAdapter.getCurrentUnionsModel() != null) {
                unionsAdapter.getCurrentUnionsModel().setSelect(false);
            }

            if (unionsAdapter.getCurrentView() != null) {
                unionsAdapter.getCurrentView().getShapeDrawableBuilder().setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();
            }
            ShapeTextView tvItem=view.findViewById(R.id.tv_item);
            unionsAdapter.setCurrentView(tvItem);
            unionsAdapter.setCurrentUnionsModel(item);

            item.setSelect(true);
            tvItem.getShapeDrawableBuilder().setStrokeColor(getResources().getColor(R.color.ps_color_ff572e)).intoBackground();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, UpdateUnionsActivity.class);
        context.startActivity(intent);
    }
}
