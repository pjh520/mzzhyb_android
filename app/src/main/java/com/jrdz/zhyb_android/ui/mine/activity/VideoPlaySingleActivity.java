package com.jrdz.zhyb_android.ui.mine.activity;

import android.content.Context;
import android.content.Intent;
import com.danikula.videocache.HttpProxyCacheServer;
import com.frame.video_library.cache.ProxyVideoCacheManager;
import com.frame.video_library.controller.StandardVideoController2;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import xyz.doikki.videoplayer.player.VideoView;


/**
 * ================================================
 * 项目名称：AndroidFrameNew
 * 包    名：com.frame.video_library.actvity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/5/9 0009
 * 描    述：单一视频播放页面(网络)
 * ================================================
 */
public class VideoPlaySingleActivity extends BaseActivity {
    private VideoView mVideoView;
    
    private String path;
    private boolean isLocal;
    
    @Override
    protected void afterSetcontentView() {
        getWindow().setBackgroundDrawable(null);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f);
        mImmersionBar.init();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_videoplay_single;
    }

    @Override
    public void initView() {
        super.initView();

        mVideoView = findViewById(R.id.video_view);
    }

    @Override
    public void initData() {
        path = getIntent().getStringExtra("path");
        isLocal = getIntent().getBooleanExtra("isLocal", false);
        super.initData();
        String proxyUrl = path;
        if (!isLocal) {
            HttpProxyCacheServer cacheServer = ProxyVideoCacheManager.getProxy(getApplicationContext());
            proxyUrl = cacheServer.getProxyUrl(path);
        }

        mVideoView.setUrl(proxyUrl);
        StandardVideoController2 controller = new StandardVideoController2(this);
        controller.addDefaultControlComponent("视频", false);
        mVideoView.setVideoController(controller);
        mVideoView.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mVideoView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mVideoView.resume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mVideoView.release();
    }

    @Override
    public void onBackPressed() {
        if (!mVideoView.onBackPressed()) {
            super.onBackPressed();
        }
    }

    public static void newIntance(Context context, boolean isLocal, String path) {
        Intent intent = new Intent(context, VideoPlaySingleActivity.class);
        intent.putExtra("isLocal", isLocal);
        intent.putExtra("path", path);
        context.startActivity(intent);
    }
}
