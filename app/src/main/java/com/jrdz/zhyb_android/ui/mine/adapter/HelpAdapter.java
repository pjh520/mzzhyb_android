package com.jrdz.zhyb_android.ui.mine.adapter;

import androidx.annotation.NonNull;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.mine.model.HelpListModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.mine.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/3/6
 * 描    述：
 * ================================================
 */
public class HelpAdapter extends BaseQuickAdapter<HelpListModel.DataBean, BaseViewHolder> {
    public HelpAdapter() {
        super(R.layout.layout_help_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, HelpListModel.DataBean helpModel) {
        baseViewHolder.setText(R.id.tv_text, EmptyUtils.strEmpty(helpModel.getTitle()));
    }
}
