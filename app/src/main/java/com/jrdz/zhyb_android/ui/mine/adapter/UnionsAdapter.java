package com.jrdz.zhyb_android.ui.mine.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.mine.model.UnionsModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.mine.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-08-29
 * 描    述：
 * ================================================
 */
public class UnionsAdapter extends BaseQuickAdapter<UnionsModel, BaseViewHolder> {
    private UnionsModel currentUnionsModel;
    private ShapeTextView currentView;

    public UnionsAdapter() {
        super(R.layout.layout_unions_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, UnionsModel unionsModel) {
        ShapeTextView tvItem=baseViewHolder.getView(R.id.tv_item);

        tvItem.setText(unionsModel.getText());
        if (unionsModel.isSelect()){
            tvItem.getShapeDrawableBuilder().setStrokeColor(mContext.getResources().getColor(R.color.ps_color_ff572e)).intoBackground();
            currentUnionsModel=unionsModel;
            currentView=tvItem;
        }else {
            tvItem.getShapeDrawableBuilder().setStrokeColor(mContext.getResources().getColor(R.color.txt_color_666)).intoBackground();
        }
    }

    public UnionsModel getCurrentUnionsModel() {
        return currentUnionsModel;
    }

    public void setCurrentUnionsModel(UnionsModel currentUnionsModel) {
        this.currentUnionsModel = currentUnionsModel;
    }

    public ShapeTextView getCurrentView() {
        return currentView;
    }

    public void setCurrentView(ShapeTextView currentView) {
        this.currentView = currentView;
    }
}
