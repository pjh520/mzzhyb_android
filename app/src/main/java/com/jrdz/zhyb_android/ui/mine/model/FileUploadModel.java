package com.jrdz.zhyb_android.ui.mine.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.utils.DeviceID;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.zhy.http.okhttp.OkHttpUtils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.mine.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-05-15
 * 描    述：
 * ================================================
 */
public class FileUploadModel {
    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private String accessoryId;
        private String accessoryUrl;

        public String getAccessoryId() {
            return accessoryId;
        }

        public void setAccessoryId(String accessoryId) {
            this.accessoryId = accessoryId;
        }

        public String getAccessoryUrl() {
            return accessoryUrl;
        }

        public void setAccessoryUrl(String accessoryUrl) {
            this.accessoryUrl = accessoryUrl;
        }
    }

    //上传图片
    public static void sendFileUploadRequest(final String TAG, String type, File stFile, final CustomerJsonCallBack<FileUploadModel> callback) {
        try {
            OkHttpUtils.post()
                    .url(Constants.BASE_URL + Constants.Api.GET_ATTACHMENTUPLOAD_URL)
                    .tag(TAG)
                    .addHeader("fixmedins_code", MechanismInfoUtils.getFixmedinsCode())
                    .addHeader("fixmedins_name", URLEncoder.encode(MechanismInfoUtils.getFixmedinsName(), "UTF-8"))
                    .addHeader("username", LoginUtils.getUserId())
                    .addHeader("password", LoginUtils.getPwd())
                    .addHeader("uni_code", DeviceID.getDeviceIDByMMKV())
                    .addHeader("insuplc_admdvs", "")
                    .addHeader("phoneType", Constants.Configure.PHONE_TYPE)//Android传1 ios传2  pos传3 后续可能会有其他的
                    .addFile("file", stFile.getName(), stFile)//
                    .addParams("type", type)
                    .build()
                    .execute(callback);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    //上传图片
    public static void sendFileUploadRequest_insured(final String TAG, String type, File stFile, final CustomerJsonCallBack<FileUploadModel> callback) {
        OkHttpUtils.post()
                .url(Constants.BASE_URL + Constants.Api.GET_ATTACHMENTUPLOAD_URL)
                .tag(TAG)
                .addHeader("username", InsuredLoginUtils.getPhone())
                .addHeader("password", InsuredLoginUtils.getPwd())
                .addHeader("uni_code", DeviceID.getDeviceIDByMMKV())
                .addHeader("phoneType", Constants.Configure.PHONE_TYPE)//Android传1 ios传2  pos传3 后续可能会有其他的
                .addFile("file", stFile.getName(), stFile)//
                .addParams("type", type)
                .build()
                .execute(callback);
    }
}
