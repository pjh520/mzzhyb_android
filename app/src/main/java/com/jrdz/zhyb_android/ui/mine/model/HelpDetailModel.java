package com.jrdz.zhyb_android.ui.mine.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.mine.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/3/9
 * 描    述：
 * ================================================
 */
public class HelpDetailModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-03-09 14:21:03
     * data : {"title":"文本内容测试","content":"操作帮助文档详情，内容待完善","CreateDT":"2022-03-09 00:00"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : 文本内容测试
         * content : 操作帮助文档详情，内容待完善
         * CreateDT : 2022-03-09 00:00
         */

        private String title;
        private String content;
        private String CreateDT;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }
    }

    //帮助列表
    public static void sendHelpDetailRequest(final String TAG, String id,final CustomerJsonCallBack<HelpDetailModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_HELPCENTER_DETAIL_URL, jsonObject.toJSONString(), callback);
    }
}
