package com.jrdz.zhyb_android.ui.mine.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.mine.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/3/6
 * 描    述：
 * ================================================
 */
public class HelpListModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-03-09 13:44:46
     * data : [{"title":"联系客服","type":3,"link":"13709291044"},{"title":"卫生室操作视频","type":2,"link":"https://www.baidu.com/"},{"title":"药店操作视频","type":2,"link":"https://www.163.com/"},{"title":"PDF帮助文档","type":4,"link":"  http://113.135.194.23:3080/ylybhelp.pdf"},{"title":"操作帮助文档","type":1,"link":"4"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : 联系客服
         * type : 3
         * link : 13709291044
         */

        private String title;
        private String type;
        private String link;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }

    //帮助列表
    public static void sendHelpListRequest(final String TAG, String pageindex, String pagesize,
                                           final CustomerJsonCallBack<HelpListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_HELPCENTER_URL, jsonObject.toJSONString(), callback);
    }
}
