package com.jrdz.zhyb_android.ui.mine.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.mine.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-05-17
 * 描    述：
 * ================================================
 */
public class SystemSetModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-05-19 09:23:43
     * data : {"ElectronicPrescription":"0","OutpatientMdtrtinfo":"0"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * ElectronicPrescription : 0
         * OutpatientMdtrtinfo : 0
         */

        private String ElectronicPrescription;
        private String OutpatientMdtrtinfo;

        public String getElectronicPrescription() {
            return ElectronicPrescription;
        }

        public void setElectronicPrescription(String ElectronicPrescription) {
            this.ElectronicPrescription = ElectronicPrescription;
        }

        public String getOutpatientMdtrtinfo() {
            return OutpatientMdtrtinfo;
        }

        public void setOutpatientMdtrtinfo(String OutpatientMdtrtinfo) {
            this.OutpatientMdtrtinfo = OutpatientMdtrtinfo;
        }
    }

    //系统设置获取
    public static void sendSystemSetRequest(final String TAG, final CustomerJsonCallBack<SystemSetModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_SYSTEMSET_URL,"", callback);
    }
}
