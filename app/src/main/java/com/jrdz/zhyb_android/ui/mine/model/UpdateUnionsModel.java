package com.jrdz.zhyb_android.ui.mine.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.mine.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-08-26
 * 描    述：
 * ================================================
 */
public class UpdateUnionsModel {
    /**
     * code : 1
     * msg : 修改打印联数成功！
     * server_time : 2022-08-26 16:08:44
     * data : {"uni_code":"a2c6fc55e299497d92a8cdbde1c895c0","phoneType":1,"deviceBrand":"HUAWEI","systemModel":"VKY-AL00","systemVersion":"9","phone":"13859005801","MedinsinfoId":13,"admdvs":"610802","fixmedins_code":"H61080200249","fixmedins_name":"榆林颈肩腰腿痛康复医院","fixmedins_phone":"13859005801","fixmedins_type":"1","hosp_lv":"06","medinsLv":"2","uscc":"52610800681598641T","CreateDT":"2022-05-20 15:16:31","UpdateDT":"2022-05-20 15:16:31","CreateUser":"admin","UpdateUser":"admin","accessoryId":"00964039-a93a-4d29-b907-2948def83e3d","ElectronicPrescription":1,"OutpatientMdtrtinfo":1,"Unions":"2"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * uni_code : a2c6fc55e299497d92a8cdbde1c895c0
         * phoneType : 1
         * deviceBrand : HUAWEI
         * systemModel : VKY-AL00
         * systemVersion : 9
         * phone : 13859005801
         * MedinsinfoId : 13
         * admdvs : 610802
         * fixmedins_code : H61080200249
         * fixmedins_name : 榆林颈肩腰腿痛康复医院
         * fixmedins_phone : 13859005801
         * fixmedins_type : 1
         * hosp_lv : 06
         * medinsLv : 2
         * uscc : 52610800681598641T
         * CreateDT : 2022-05-20 15:16:31
         * UpdateDT : 2022-05-20 15:16:31
         * CreateUser : admin
         * UpdateUser : admin
         * accessoryId : 00964039-a93a-4d29-b907-2948def83e3d
         * ElectronicPrescription : 1
         * OutpatientMdtrtinfo : 1
         * Unions : 2
         */
        private String Unions;

        public String getUnions() {
            return Unions;
        }

        public void setUnions(String Unions) {
            this.Unions = Unions;
        }
    }

    //更新联数
    public static void sendUpdateUnionsRequest(final String TAG, String unions,final CustomerJsonCallBack<UpdateUnionsModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("unions", unions);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_APPUNIONSUPDATE_URL, jsonObject.toJSONString(), callback);
    }
}
