package com.jrdz.zhyb_android.ui.settlement.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ImageUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.ThreadUtils;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.layout.ShapeRelativeLayout;
import com.hjq.shape.view.ShapeEditText;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.database.CatalogueModel;
import com.jrdz.zhyb_android.database.UserInfoModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.activity.SelectChinaCataManageActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.SelectWestCataManageActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.UpdateWestCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.model.ChinaPrescrCataModel;
import com.jrdz.zhyb_android.ui.catalogue.model.WestCataListModel;
import com.jrdz.zhyb_android.ui.catalogue.model.WestPrescrCataModel;
import com.jrdz.zhyb_android.ui.home.activity.ScanCataListActivtiy;
import com.jrdz.zhyb_android.ui.home.model.ScanOrganModel;
import com.jrdz.zhyb_android.ui.mine.activity.HwScanActivity;
import com.jrdz.zhyb_android.ui.mine.activity.ZbarScanActivity;
import com.jrdz.zhyb_android.ui.mine.model.FileUploadModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils;
import com.jrdz.zhyb_android.widget.pop.DropDownDataPop;
import com.jrdz.zhyb_android.widget.pop.HandWritePop;
import com.jrdz.zhyb_android.widget.pop.UpdateNumPop;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/5/8
 * 描    述： 中药处方
 * ================================================
 */
public class ChinaPrescrActivity extends BaseActivity implements HandWritePop.IOptionListener {
    private ShapeEditText mEtDiagResult;
    private TextView mTvSetlectDrugs;
    private TextView mTvScanSetlectDrugs,mTvNumCompany,mTvUsage;
    private ShapeTextView mTvEmptyText;
    private LinearLayout mLlDrugsContain;
    private ShapeLinearLayout mSllNumCompany,mSllUsage, mSllSignature;
    private RelativeLayout mRlNumInfo;
    private FrameLayout mFlReduce,mFlAdd;
    private ShapeTextView mEtNum;
    private ShapeEditText mEtRemarks;
    private ShapeRelativeLayout mSllHejiPrice;
    private TextView mTvHejiPrice, mTvSignatureStatus;
    private ShapeTextView mTvSure;

    private UpdateNumPop updateNumPop;
    private ChinaPrescrCataModel chinaPrescrCataModel;//存储西药中成药处方数据
    private ArrayList<ChinaPrescrCataModel.DataBean> catalogueListDatas;//存储西药中成药处方药品数据
    private DropDownDataPop dropDownDataPop;
    private HandWritePop handWritePop;
    String imagPath = BaseGlobal.getPicShotDir() + "doctor_signature.png";//存储签名的路径
    private UserInfoModel userInfo;
    private boolean isUpdate;

    @Override
    public int getLayoutId() {
        return R.layout.activity_china_prescr;
    }

    @Override
    public void initView() {
        super.initView();
        mEtDiagResult = findViewById(R.id.et_diag_result);
        mTvSetlectDrugs = findViewById(R.id.tv_setlect_drugs);
        mTvScanSetlectDrugs = findViewById(R.id.tv_scan_setlect_drugs);
        mTvEmptyText = findViewById(R.id.tv_empty_text);
        mLlDrugsContain = findViewById(R.id.ll_drugs_contain);
        mRlNumInfo= findViewById(R.id.rl_num_info);
        mFlReduce = findViewById(R.id.fl_reduce);
        mEtNum = findViewById(R.id.et_num);
        mFlAdd = findViewById(R.id.fl_add);
        mSllUsage = findViewById(R.id.sll_usage);
        mTvUsage = findViewById(R.id.tv_usage);
        mEtRemarks = findViewById(R.id.et_remarks);
        mSllHejiPrice = findViewById(R.id.sll_heji_price);
        mTvHejiPrice = findViewById(R.id.tv_heji_price);
        mSllSignature = findViewById(R.id.sll_signature);
        mTvSignatureStatus = findViewById(R.id.tv_signature_status);
        mTvSure = findViewById(R.id.tv_sure);
    }

    @Override
    public void initData() {
        chinaPrescrCataModel=getIntent().getParcelableExtra("chinaPrescrCataModel");
        super.initData();
        setRightTitleView("预览处方");
        userInfo = LoginUtils.getUserinfo();
        mTvSignatureStatus.setText(EmptyUtils.isEmpty(userInfo.getAccessoryUrl1())?"未签":"已签");

        if (chinaPrescrCataModel==null){
            chinaPrescrCataModel = new ChinaPrescrCataModel();
        }else {
            initPageData();
        }

        catalogueListDatas = chinaPrescrCataModel.getData();
        mTvUsage.setText(EmptyUtils.strEmpty(chinaPrescrCataModel.getUsage()));
        bpttomVisibily();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvSetlectDrugs.setOnClickListener(this);
        mTvScanSetlectDrugs.setOnClickListener(this);
        mFlReduce.setOnClickListener(this);
        mEtNum.setOnClickListener(this);
        mFlAdd.setOnClickListener(this);
        mSllUsage.setOnClickListener(this);
        mSllSignature.setOnClickListener(this);
        mTvSure.setOnClickListener(this);
    }

    //初始化页面数据
    private void initPageData() {
        isUpdate=true;
        mEtDiagResult.setText(EmptyUtils.strEmpty(chinaPrescrCataModel.getDiagResult()));
        mTvHejiPrice.setText("¥" + chinaPrescrCataModel.getHejiPrice());
        mEtNum.setText(String.valueOf(chinaPrescrCataModel.getNum()));
        mEtRemarks.setText(EmptyUtils.strEmpty(chinaPrescrCataModel.getRemarks()));
        for (ChinaPrescrCataModel.DataBean datum : chinaPrescrCataModel.getData()) {
            addDragView(datum);
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_setlect_drugs://选择药品
                SelectChinaCataManageActivity.newIntance(ChinaPrescrActivity.this, "102", "中药饮片", Constants.RequestCode.SELECT_CHINAPRESCR_CATA_CODE);
                break;
            case R.id.tv_scan_setlect_drugs://扫码选择药品
                if (Constants.Configure.IS_POS) {
                    goPosScan();
                } else {
                    goHwScan();
                }
                break;
            case R.id.fl_reduce://数量减
                showWaitDialog();
                if (EmptyUtils.isEmpty(mEtNum.getText().toString())) {
                    mEtNum.setText("1");
                } else if (new BigDecimal(mEtNum.getText().toString()).intValue() > 1) {
                    mEtNum.setText(new BigDecimal(mEtNum.getText().toString()).subtract(new BigDecimal("1")).toPlainString());
                } else {
                    showShortToast("药品数量最低值为1");
                    hideWaitDialog();
                    return;
                }

                chinaPrescrCataModel.setNum(EmptyUtils.isEmpty(mEtNum.getText().toString()) ? 1 : Integer.valueOf(mEtNum.getText().toString()));
                setTotalPrice();
                hideWaitDialog();
                break;
            case R.id.et_num://数量
                if (updateNumPop == null) {
                    updateNumPop = new UpdateNumPop(ChinaPrescrActivity.this);
                }
                updateNumPop.setOnListener(new UpdateNumPop.IOptionListener() {
                    @Override
                    public void onAgree(String data) {
                        int num = new BigDecimal(data).intValue();
                        if (num < 1) {
                            showShortToast("请输入正确的数量");
                            return;
                        }
                        mEtNum.setText(String.valueOf(num));
                        chinaPrescrCataModel.setNum(num);
                        setTotalPrice();
                    }

                    @Override
                    public void onCancle() {
                    }
                });
                updateNumPop.setInputType(InputType.TYPE_CLASS_NUMBER);
                updateNumPop.setContent(String.valueOf(chinaPrescrCataModel.getNum()));
                updateNumPop.setOutSideDismiss(false).showPopupWindow();
                KeyboardUtils.showSoftInput(ChinaPrescrActivity.this);
                break;
            case R.id.fl_add://数量加
                showWaitDialog();
                if (EmptyUtils.isEmpty(mEtNum.getText().toString())) {
                    mEtNum.setText("1");
                } else {
                    mEtNum.setText(new BigDecimal(mEtNum.getText().toString()).add(new BigDecimal("1")).toPlainString());
                }

                chinaPrescrCataModel.setNum(EmptyUtils.isEmpty(mEtNum.getText().toString()) ? 1 : Integer.valueOf(mEtNum.getText().toString()));
                setTotalPrice();
                hideWaitDialog();
                break;
            case R.id.sll_usage://用法
                KeyboardUtils.hideSoftInput(ChinaPrescrActivity.this);
                mSllUsage.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (dropDownDataPop == null) {
                            dropDownDataPop = new DropDownDataPop(ChinaPrescrActivity.this, CommonlyUsedDataUtils.getInstance().getUsageData_china());
                        } else {
                            dropDownDataPop.setData(CommonlyUsedDataUtils.getInstance().getUsageData_china());
                        }

                        dropDownDataPop.setOnListener(new DropDownDataPop.IOptionListener() {
                            @Override
                            public void onItemClick(String item) {
                                chinaPrescrCataModel.setUsage(item);
                                mTvUsage.setText(EmptyUtils.strEmpty(item));
                            }
                        });

                        dropDownDataPop.showPopupWindow(mSllUsage);
                    }
                }, 250);

                break;
            case R.id.sll_signature://医师签名
                if (handWritePop == null) {
                    handWritePop = new HandWritePop(ChinaPrescrActivity.this, this);
                }

                handWritePop.showPopupWindow();
                break;
            case R.id.tv_sure://确定
                sure("2");
                break;
        }
    }

    @Override
    public void rightTitleViewClick() {
        sure("1");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RequestCode.SELECT_CHINAPRESCR_CATA_CODE && resultCode == RESULT_OK) {//选择药品信息回调
            CatalogueModel catalogueModels = data.getParcelableExtra("catalogueModels");
            if (catalogueModels != null) {
                setSelectCataLayout(catalogueModels);
            }
        } else if (requestCode == Constants.RequestCode.REQUEST_SCANCATALIST_CODE && resultCode == RESULT_OK) {
            CatalogueModel catalogueModel = data.getParcelableExtra("catalogueModels");
            if (catalogueModel != null) {
                setSelectCataLayout(catalogueModel);
            }
        }else if (requestCode == Constants.RequestCode.PRESCR_UPLOAD_CODE && resultCode == RESULT_OK){//处方已上传完毕
            setResult(RESULT_OK, data);
            goFinish();
        }
    }

    //--------------------------------------药品信息-----------------------------------------------------------
    //设置选中的药品列表
    private void setSelectCataLayout(CatalogueModel catalogueModels) {
        boolean isSame = false;
        int pos = 0;
        if (!catalogueListDatas.isEmpty()) {
            ChinaPrescrCataModel.DataBean data;
            for (int i = 0, size = catalogueListDatas.size(); i < size; i++) {
                data = catalogueListDatas.get(i);
                if (data.getCatalogueModel().getFixmedins_hilist_id().equals(catalogueModels.getFixmedins_hilist_id())) {
                    //选择的药品已经在列表中，那么就需要直接该药品数量+1
                    data.setWeight(data.getWeight() + 1);
                    isSame = true;
                    pos = i;
                    break;
                }
            }
        }

        if (isSame) {
            updateDragView(pos, catalogueListDatas.get(pos));
        } else {
            ChinaPrescrCataModel.DataBean chinaPrescrCataModel = new ChinaPrescrCataModel.DataBean();
            chinaPrescrCataModel.setCatalogueModel(catalogueModels);
            catalogueListDatas.add(chinaPrescrCataModel);
            addDragView(chinaPrescrCataModel);
        }

        setTotalPrice();
        bpttomVisibily();
    }

    //增加药品项
    private void addDragView(ChinaPrescrCataModel.DataBean chinaPrescrCataData) {
        CatalogueModel cataLogueData = chinaPrescrCataData.getCatalogueModel();
        if (cataLogueData == null) {
            showShortToast("数据有误，请重新选择");
            return;
        }
        View view = LayoutInflater.from(this).inflate(R.layout.layout_chinaprescr_selectcata_item, mLlDrugsContain, false);

        TextView tvRegNam = view.findViewById(R.id.tv_reg_nam);
        FrameLayout flDelete = view.findViewById(R.id.fl_delete);
        FrameLayout flReduceWeight = view.findViewById(R.id.fl_reduce_weight);
        ShapeTextView etNumWeight = view.findViewById(R.id.et_num_weight);
        FrameLayout flAddWeight = view.findViewById(R.id.fl_add_weight);
        ShapeLinearLayout sllWeightCompany = view.findViewById(R.id.sll_weight_company);
        TextView tvWeightCompany = view.findViewById(R.id.tv_weight_company);
        ShapeTextView tvUnitPrice = view.findViewById(R.id.tv_unit_price);
        TextView tvSingleTotalPrice = view.findViewById(R.id.tv_single_total_price);


        tvRegNam.setText("药品名称:  " + cataLogueData.getFixmedins_hilist_name());
        etNumWeight.setText(String.valueOf(chinaPrescrCataData.getWeight()));
        tvWeightCompany.setText(EmptyUtils.strEmpty(chinaPrescrCataData.getWeightCompany()));
        tvUnitPrice.setText(EmptyUtils.strEmpty(cataLogueData.getPrice()));
        setSingTotalPrice(tvSingleTotalPrice, cataLogueData.getPrice(), etNumWeight.getText().toString());

        //删除选择
        flDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLlDrugsContain.removeView(view);
                catalogueListDatas.remove(chinaPrescrCataData);

                setTotalPrice();
                bpttomVisibily();
            }
        });
        //监听重量修改
        etNumWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }

                if (updateNumPop == null) {
                    updateNumPop = new UpdateNumPop(ChinaPrescrActivity.this);
                }
                updateNumPop.setOnListener(new UpdateNumPop.IOptionListener() {
                    @Override
                    public void onAgree(String data) {
                        int num = new BigDecimal(data).intValue();
                        if (num < 1) {
                            showShortToast("请输入正确的重量");
                            return;
                        }
                        etNumWeight.setText(String.valueOf(num));
                        chinaPrescrCataData.setWeight(num);
                        setSingTotalPrice(tvSingleTotalPrice, cataLogueData.getPrice(), etNumWeight.getText().toString());
                        setTotalPrice();
                    }

                    @Override
                    public void onCancle() {}
                });
                updateNumPop.setInputType(InputType.TYPE_CLASS_NUMBER);
                updateNumPop.setContent(String.valueOf(chinaPrescrCataData.getWeight()));
                updateNumPop.setOutSideDismiss(false).showPopupWindow();
                KeyboardUtils.showSoftInput(ChinaPrescrActivity.this);
            }
        });
        //重量减
        flReduceWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWaitDialog();
                if (EmptyUtils.isEmpty(etNumWeight.getText().toString())) {
                    etNumWeight.setText("1");
                } else if (new BigDecimal(etNumWeight.getText().toString()).intValue() > 1) {
                    etNumWeight.setText(new BigDecimal(etNumWeight.getText().toString()).subtract(new BigDecimal("1")).toPlainString());
                } else {
                    showShortToast("每次用量最低值为1");
                    hideWaitDialog();
                    return;
                }

                chinaPrescrCataData.setWeight(EmptyUtils.isEmpty(etNumWeight.getText().toString()) ? 1 : Integer.valueOf(etNumWeight.getText().toString()));
                setSingTotalPrice(tvSingleTotalPrice, cataLogueData.getPrice(), etNumWeight.getText().toString());
                setTotalPrice();
                hideWaitDialog();
            }
        });
        //重量加
        flAddWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                if (EmptyUtils.isEmpty(etNumWeight.getText().toString())) {
                    etNumWeight.setText("1");
                } else {
                    etNumWeight.setText(new BigDecimal(etNumWeight.getText().toString()).add(new BigDecimal("1")).toPlainString());
                }

                chinaPrescrCataData.setWeight(EmptyUtils.isEmpty(etNumWeight.getText().toString()) ? 1 : Integer.valueOf(etNumWeight.getText().toString()));
                setSingTotalPrice(tvSingleTotalPrice, cataLogueData.getPrice(), etNumWeight.getText().toString());
                setTotalPrice();
                hideWaitDialog();
            }
        });
        //选择重量单位
        sllWeightCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                KeyboardUtils.hideSoftInput(ChinaPrescrActivity.this);

                sllWeightCompany.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (dropDownDataPop == null) {
                            dropDownDataPop = new DropDownDataPop(ChinaPrescrActivity.this, CommonlyUsedDataUtils.getInstance().getWeightData());
                        } else {
                            dropDownDataPop.setData(CommonlyUsedDataUtils.getInstance().getWeightData());
                        }

                        dropDownDataPop.setOnListener(new DropDownDataPop.IOptionListener() {
                            @Override
                            public void onItemClick(String item) {
                                chinaPrescrCataData.setWeightCompany(item);
                                tvWeightCompany.setText(EmptyUtils.strEmpty(item));
                            }
                        });

                        dropDownDataPop.showPopupWindow(sllWeightCompany);
                    }
                }, 250);
            }
        });
        //单价修改按钮点击事件
        tvUnitPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }

                if (updateNumPop == null) {
                    updateNumPop = new UpdateNumPop(ChinaPrescrActivity.this);
                }
                updateNumPop.setOnListener(new UpdateNumPop.IOptionListener() {
                    @Override
                    public void onAgree(String data) {
                        if (!RxTool.isPrice(data)) {
                            showShortToast("请输入正确的金额");
                            return;
                        }
                        showWaitDialog();
                        editContents(cataLogueData, data,String.valueOf(chinaPrescrCataData.getWeight()),tvUnitPrice, tvSingleTotalPrice);
                    }

                    @Override
                    public void onCancle() {}
                });
                updateNumPop.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
                updateNumPop.setContent(EmptyUtils.strEmpty(cataLogueData.getPrice()));
                updateNumPop.setOutSideDismiss(false).showPopupWindow();
                KeyboardUtils.showSoftInput(ChinaPrescrActivity.this);
            }
        });
        mLlDrugsContain.addView(view);
    }

    //修改目录价格（不上传省平台）
    private void editContents(CatalogueModel cataData, String Price,String numWeight, TextView tvUnitPrice, TextView tvSingleTotalPrice) {
        BaseModel.sendContentsEditRequest(TAG, cataData.getList_type(), cataData.getFixmedins_hilist_id(), cataData.getFixmedins_hilist_name(),
                cataData.getMed_list_codg(), Price, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        cataData.setPrice(Price);
                        tvUnitPrice.setText(Price);
                        setSingTotalPrice(tvSingleTotalPrice, cataData.getPrice(), numWeight);
                        setTotalPrice();
                        showShortToast("修改成功");
                    }
                });
    }

    //更新药品项
    private void updateDragView(int pos, ChinaPrescrCataModel.DataBean chinaPrescrCataData) {
        CatalogueModel cataLogueData = chinaPrescrCataData.getCatalogueModel();
        if (cataLogueData == null) {
            showShortToast("数据有误，请重新选择");
            return;
        }
        View view = mLlDrugsContain.getChildAt(pos);
        ShapeTextView etNumWeight = view.findViewById(R.id.et_num_weight);
        TextView tvSingleTotalPrice = view.findViewById(R.id.tv_single_total_price);

        etNumWeight.setText(String.valueOf(chinaPrescrCataData.getWeight()));
        setSingTotalPrice(tvSingleTotalPrice, EmptyUtils.strEmpty(cataLogueData.getPrice()), etNumWeight.getText().toString());
    }

    //设置单个药品的总价
    private void setSingTotalPrice(TextView tv, String price, String num) {
        if (tv != null) {
            String singleTotalPrice = new BigDecimal(price).multiply(new BigDecimal(num)).toPlainString();
            tv.setText("¥" + singleTotalPrice);
        }
    }

    //底部控件是否显示
    private void bpttomVisibily() {
        if (catalogueListDatas.isEmpty()) {
            mTvEmptyText.setVisibility(View.VISIBLE);
            mRlNumInfo.setVisibility(View.GONE);
            mSllHejiPrice.setVisibility(View.GONE);
        } else {
            mTvEmptyText.setVisibility(View.GONE);
            mRlNumInfo.setVisibility(View.VISIBLE);
            mSllHejiPrice.setVisibility(View.VISIBLE);
        }
    }

    //计算全部药品的价格
    private void setTotalPrice() {
        String hejiPrice = "0";
        CatalogueModel cataLogueData;
        for (ChinaPrescrCataModel.DataBean westPrescrCataData : catalogueListDatas) {
            cataLogueData = westPrescrCataData.getCatalogueModel();
            String singleTotalPrice = new BigDecimal(cataLogueData.getPrice()).multiply(new BigDecimal(westPrescrCataData.getWeight())).toPlainString();
            hejiPrice = new BigDecimal(hejiPrice).add(new BigDecimal(singleTotalPrice)).toPlainString();
        }
        hejiPrice=new BigDecimal(hejiPrice).multiply(new BigDecimal(String.valueOf(chinaPrescrCataModel.getNum()))).toPlainString();
        chinaPrescrCataModel.setHejiPrice(hejiPrice);
        mTvHejiPrice.setText("¥" + hejiPrice);
    }
    //--------------------------------------药品信息-----------------------------------------------------------

    //使用pos机自带的扫码
    private void goPosScan() {
        startActivityForResult(new Intent(this, ZbarScanActivity.class), new OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == -1) {
                    String scanResult = data.getStringExtra("scanResult");
                    showWaitDialog();
                    scanOrgan(scanResult);
                }
            }
        });
    }

    //手机端使用华为扫码
    private void goHwScan() {
        startActivityForResult(HwScanActivity.class, new OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == RESULT_OK) {
                    String scanResult = data.getStringExtra("scanResult");
                    showWaitDialog();
                    scanOrgan(scanResult);
                }
            }
        });
    }

    //扫机构
    private void scanOrgan(String barCode) {
        ScanOrganModel.sendScanOrganRequest(TAG, barCode, new CustomerJsonCallBack<ScanOrganModel>() {
            @Override
            public void onRequestError(ScanOrganModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(ScanOrganModel returnData) {
                hideWaitDialog();
                String type = returnData.getType();
                if ("1".equals(type)) {//机构目录
                    ArrayList<CatalogueModel> data1 = returnData.getData1();
                    if (data1 != null && !data1.isEmpty()) {
                        if (data1.size() == 1) {
                            setSelectCataLayout(data1.get(0));
                        } else {
                            ScanCataListActivtiy.newIntance(ChinaPrescrActivity.this, "101", "2", data1, Constants.RequestCode.REQUEST_SCANCATALIST_CODE);
                        }
                    }
                } else if ("2".equals(type)) {//医保目录
                    List<WestCataListModel.DataBean> data2 = returnData.getData2();
                    if (data2 != null && !data2.isEmpty()) {
                        WestCataListModel.DataBean dataBean = data2.get(0);
                        UpdateWestCataActivity.newIntance(ChinaPrescrActivity.this, dataBean.getListType(), dataBean);
                    }
                }
            }
        });
    }

    @Override
    public void commit(Bitmap mBitmap) {
        showWaitDialog("图片处理中");

        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                boolean isSaveSuccess = ImageUtils.save(mBitmap, imagPath, Bitmap.CompressFormat.PNG, true);
                if (isSaveSuccess) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            upSignPic();
                        }
                    });
                } else {
                    hideWaitDialog();
                    showShortToast("图片保存失败，请重新保存");
                }
            }
        });
    }

    //上传签名
    private void upSignPic() {
        FileUploadModel.sendFileUploadRequest(TAG, CompressUploadSinglePicUtils.PIC_DOCTOR_SIGN_TAG, new File(imagPath), new CustomerJsonCallBack<FileUploadModel>() {
            @Override
            public void onRequestError(FileUploadModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(FileUploadModel returnData) {
                hideWaitDialog();
                FileUploadModel.DataBean dataBean = returnData.getData();
                if (dataBean!=null){
                    showShortToast("上传成功");

                    mTvSignatureStatus.setText("已签");

                    if (userInfo!=null){
                        userInfo.setSubAccessoryId(dataBean.getAccessoryId());
                        userInfo.setAccessoryUrl1(dataBean.getAccessoryUrl());
                        userInfo.update(userInfo.getId());
                    }
                }else {
                    showShortToast("图片上传失败，请重新上传");
                }
            }
        });
    }

    //确认按钮 from 1从预览处方进去 不带上传按钮 2确定进去 需要强制上传处方截图
    private void sure(String from) {
        if (EmptyUtils.isEmpty(mEtDiagResult.getText().toString())) {
            showShortToast("请填写诊断结果");
            return;
        }
        if (mLlDrugsContain.getChildCount() <= 0) {
            //若未选药品 则直接返回 相当于删除处方
            if (isUpdate){
                showShortToast("删除中药处方成功");
            }
            setResult(RESULT_OK,new Intent());
            goFinish();
            return;
        }

        //2022-05-10 还得判断是否签名
        if (EmptyUtils.isEmpty(userInfo.getAccessoryUrl1())){
            showShortToast("请先签名");
            return;
        }

        chinaPrescrCataModel.setDiagResult(EmptyUtils.strEmpty(mEtDiagResult.getText().toString()));
        chinaPrescrCataModel.setNum(EmptyUtils.isEmpty(mEtNum.getText().toString()) ? 1 : Integer.valueOf(mEtNum.getText().toString()));
        chinaPrescrCataModel.setUsage(EmptyUtils.strEmpty(mTvUsage.getText().toString()));
        chinaPrescrCataModel.setRemarks(EmptyUtils.strEmpty(mEtRemarks.getText().toString()));

        PreChinaPrescActivity.newIntance(this,chinaPrescrCataModel,getIntent().getBundleExtra("baseInfo"),from,Constants.RequestCode.PRESCR_UPLOAD_CODE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handWritePop != null) {
            handWritePop.onClean();
        }

        if (updateNumPop != null) {
            updateNumPop.onCleanListener();
            updateNumPop.onDestroy();
        }
        if (dropDownDataPop != null) {
            dropDownDataPop.onCleanListener();
            dropDownDataPop.onDestroy();
        }
    }

    public static void newIntance(Activity activity,String ipt_otp_no,String name,String sex,String age,String address,String phone,
                                  ChinaPrescrCataModel chinaPrescrCataModel, int requestCode) {
        Intent intent = new Intent(activity, ChinaPrescrActivity.class);
        Bundle bundle=new Bundle();
        bundle.putString("ipt_otp_no", ipt_otp_no);
        bundle.putString("name", name);
        bundle.putString("sex", sex);
        bundle.putString("age", age);
        bundle.putString("address", address);
        bundle.putString("phone", phone);
        intent.putExtra("baseInfo", bundle);
        intent.putExtra("chinaPrescrCataModel", chinaPrescrCataModel);
        activity.startActivityForResult(intent, requestCode);
    }
}
