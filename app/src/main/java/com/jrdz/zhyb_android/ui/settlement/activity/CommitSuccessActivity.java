package com.jrdz.zhyb_android.ui.settlement.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.home.activity.MainActivity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-05-10
 * 描    述： 提交成功页面
 * ================================================
 */
public class CommitSuccessActivity extends BaseActivity {
    private ShapeTextView mTvGoHome;
    private ShapeTextView mTvOutpatientRecords;

    @Override
    public int getLayoutId() {
        return R.layout.activity_commit_success;
    }

    @Override
    public void initView() {
        super.initView();
        mTvGoHome = findViewById(R.id.tv_go_home);
        mTvOutpatientRecords = findViewById(R.id.tv_outpatient_records);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvGoHome.setOnClickListener(this);
        mTvOutpatientRecords.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_go_home://返回首页
                MainActivity.newIntance(CommitSuccessActivity.this, 0);
                break;
            case R.id.tv_outpatient_records://查看门诊记录
                OutpatRegListActivtiy.newIntance(CommitSuccessActivity.this);
                goFinish();
                break;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, CommitSuccessActivity.class);
        context.startActivity(intent);
    }
}
