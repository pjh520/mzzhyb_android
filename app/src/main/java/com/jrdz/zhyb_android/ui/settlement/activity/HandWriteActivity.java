package com.jrdz.zhyb_android.ui.settlement.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.base.BaseGlobal;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.widget.HandWrite;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/17
 * 描    述：手写签名
 * ================================================
 */
public class HandWriteActivity extends BaseActivity {
    private HandWrite mHandWrite;
    private TextView mTvRewrite;

    @Override
    public int getLayoutId() {
        return R.layout.activity_handwrite;
    }

    @Override
    public void initView() {
        super.initView();
        mHandWrite=findViewById(R.id.handWrite);
        mTvRewrite=findViewById(R.id.tv_rewrite);
    }

    @Override
    public void initData() {
        super.initData();

        setRightTitleView("保存");
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvRewrite.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_rewrite:
                mHandWrite.reset();
                break;
        }
    }

    @Override
    public void rightTitleViewClick() {
        Bitmap mBitmap =mHandWrite.buildAreaBitmap(true);
        if (mBitmap!=null){
            try {
                Intent intent = new Intent();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                /*下面方法表示压缩图片，中间的值越小，压缩比例越大，失真也约厉害，100表示不压缩*/
                mBitmap.compress(Bitmap.CompressFormat.PNG,100,baos);
                byte[] byteArray = baos.toByteArray();
                intent.putExtra("bitmap",byteArray);
                setResult(RESULT_OK,intent);

                baos.flush();
                baos.close();
                if (mBitmap!=null){
                    mBitmap.recycle();
                    mBitmap=null;
                }

                finish();
            } catch (IOException e) {
                e.printStackTrace();
                showShortToast("数据有误，请重新进入页面签名");
            }
        }else {
            showShortToast("数据有误，请重新进入页面签名");
        }
    }

    public void SaveImage(){
        /*在这里保存图片纯粹是为了方便,保存图片进行验证*/
        String fileUrl = BaseGlobal.getImageDir() + "/android/data/test.png";
        try {
            Bitmap mBitmap =mHandWrite.buildAreaBitmap(true);
            if( mBitmap== null) return;
            FileOutputStream fos = new FileOutputStream(new File(fileUrl));
            mBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandWrite.release();
    }

    public static void newIntance(Activity activity, int requestCode) {
        Intent intent = new Intent(activity, HandWriteActivity.class);
        activity.startActivityForResult(intent,requestCode);
    }
}
