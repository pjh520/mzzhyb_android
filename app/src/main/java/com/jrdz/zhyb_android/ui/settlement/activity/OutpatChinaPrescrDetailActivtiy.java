package com.jrdz.zhyb_android.ui.settlement.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.DateUtil;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ImageUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.layout.ShapeRecyclerView;
import com.hjq.shape.view.ShapeView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.database.UserInfoModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.model.ChinaPrescrCataModel;
import com.jrdz.zhyb_android.ui.catalogue.model.WestPrescrCataModel;
import com.jrdz.zhyb_android.ui.mine.model.FileUploadModel;
import com.jrdz.zhyb_android.ui.settlement.adapter.ChinaPrescDragDetailAdapter;
import com.jrdz.zhyb_android.ui.settlement.adapter.PreChinaPrescDragAdapter;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatPrescrDetailModel;
import com.jrdz.zhyb_android.ui.settlement.model.SelectOutpatPrescrModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.DownLoadUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.utils.ScreenshotUtil;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/29
 * 描    述：电子处方列表
 * ================================================
 */
public class OutpatChinaPrescrDetailActivtiy extends BaseActivity {
    private NestedScrollView mSvContain;
    private TextView mTvPrescNum,mTvStatus;
    private TextView mTvMdtrtId;
    private TextView mTvName;
    private TextView mTvSex;
    private TextView mTvAge;
    private TextView mTvDept;
    private TextView mTvDate;
    private TextView mTvAddress;
    private TextView mTvPhone,mTvDiagResult;
    private ShapeRecyclerView mSrlDragContain;
    private ImageView mIvPhysician;
    private TextView mTvRemarks,mTvPrice;
    private TextView mTvReviewPharm;
    private TextView mTvDispePharm;
    private FrameLayout mFlDownload;
    private TextView mTvNuclearPharm,mTvDownload;

    private UserInfoModel userInfo;
    private String electronicPrescriptionNo;
    private OutpatPrescrDetailModel.DataBean.ElectronicPrescriptionsBean electronicPrescriptions;

    @Override
    public int getLayoutId() {
        return R.layout.activity_chinapresc_detail;
    }

    @Override
    public void initView() {
        super.initView();
        mSvContain = findViewById(R.id.sv_contain);
        mTvPrescNum = findViewById(R.id.tv_presc_num);
        mTvStatus = findViewById(R.id.tv_status);
        mTvMdtrtId = findViewById(R.id.tv_mdtrt_id);
        mTvName = findViewById(R.id.tv_name);
        mTvSex = findViewById(R.id.tv_sex);
        mTvAge = findViewById(R.id.tv_age);
        mTvDept = findViewById(R.id.tv_dept);
        mTvDate = findViewById(R.id.tv_date);
        mTvAddress = findViewById(R.id.tv_address);
        mTvPhone = findViewById(R.id.tv_phone);
        mTvDiagResult= findViewById(R.id.tv_diagResult);
        mSrlDragContain = findViewById(R.id.srl_drag_contain);
        mTvRemarks= findViewById(R.id.tv_remarks);
        mIvPhysician = findViewById(R.id.iv_physician);
        mTvPrice = findViewById(R.id.tv_price);
        mTvReviewPharm = findViewById(R.id.tv_review_pharm);
        mTvDispePharm = findViewById(R.id.tv_dispe_pharm);
        mTvNuclearPharm = findViewById(R.id.tv_nuclear_pharm);
        mFlDownload = findViewById(R.id.fl_download);
        mTvDownload = findViewById(R.id.tv_download);
    }

    @Override
    public void initData() {
        electronicPrescriptionNo = getIntent().getStringExtra("electronicPrescriptionNo");
        super.initData();
        userInfo = LoginUtils.getUserinfo();

        setTitle(MechanismInfoUtils.getFixmedinsName()+"电子处方笺");

        if (Constants.Configure.IS_POS){
            mFlDownload.setVisibility(View.GONE);
        }else {
            mFlDownload.setVisibility(View.VISIBLE);
        }

        showWaitDialog();
        getPageData();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvDownload.setOnClickListener(this);
    }

    private void getPageData() {
        OutpatPrescrDetailModel.sendOutpatPrescrDetailRequest(TAG, electronicPrescriptionNo, new CustomerJsonCallBack<OutpatPrescrDetailModel>() {
            @Override
            public void onRequestError(OutpatPrescrDetailModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(OutpatPrescrDetailModel returnData) {
                hideWaitDialog();
                if (isFinishing())return;
                OutpatPrescrDetailModel.DataBean data = returnData.getData();

                if (data!=null){
                    electronicPrescriptions = data.getElectronicPrescriptions();
                    List<OutpatPrescrDetailModel.DataBean.FeedetailsBean> feedetails = data.getFeedetails();
                    OutpatPrescrDetailModel.DataBean.OtherinfoBean otherinfo = data.getOtherinfo();
                    if (electronicPrescriptions!=null){
                        mTvPrescNum.setText(EmptyUtils.strEmptyToText(electronicPrescriptions.getElectronicPrescriptionNo(),"--"));
                        mTvMdtrtId.setText(EmptyUtils.strEmptyToText(electronicPrescriptions.getIpt_otp_no(),"--"));
                        mTvName.setText(EmptyUtils.strEmptyToText(electronicPrescriptions.getPsn_name(),"--"));
                        mTvSex.setText("1".equals(electronicPrescriptions.getSex())?"男":"女");
                        mTvAge.setText(EmptyUtils.strEmptyToText(electronicPrescriptions.getAge(),"--"));
                        mTvDept.setText(EmptyUtils.strEmptyToText(electronicPrescriptions.getDept_name(),"--"));
                        mTvDate.setText(EmptyUtils.strEmptyToText(electronicPrescriptions.getCreateDT(),"--"));
                        mTvAddress.setText(EmptyUtils.strEmptyToText(electronicPrescriptions.getAddress(),"--"));
                        mTvPhone.setText(EmptyUtils.strEmptyToText(electronicPrescriptions.getContact(),"--"));
                        mTvDiagResult.setText(EmptyUtils.strEmpty(EmptyUtils.strEmptyToText(electronicPrescriptions.getCmDiagResults(),"--")));
                        mTvPrice.setText(EmptyUtils.strEmptyToText(electronicPrescriptions.getCmAmount(),"--"));
                        mTvRemarks.setText("配"+electronicPrescriptions.getCmDrugNum()+"付 "
                                +electronicPrescriptions.getCmused_mtd()+" "+electronicPrescriptions.getCmRemark());
                    }
                    if (feedetails!=null){
                        setDragList(feedetails);
                    }
                    if (otherinfo!=null){
                        GlideUtils.getImageWidHeig(OutpatChinaPrescrDetailActivtiy.this, EmptyUtils.strEmpty(otherinfo.getDoctorurl()), new GlideUtils.IGetImageData() {
                            @Override
                            public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mIvPhysician.getLayoutParams();
                                if (radio >= 2.875) {
                                    layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_230));
                                    layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_230))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                                } else {
                                    layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_80) * radio);
                                    layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_80));
                                }

                                mIvPhysician.setLayoutParams(layoutParams);
                                mIvPhysician.setImageBitmap(resource);
                            }
                        });
                    }
                }
            }
        });
    }

    //设置药品数据
    private void setDragList(List<OutpatPrescrDetailModel.DataBean.FeedetailsBean> datas) {
        if (datas == null || datas.isEmpty()) {
            showShortToast("药品数据有误");
            return;
        }

        mSrlDragContain.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL,false));
        ChinaPrescDragDetailAdapter chinaPrescDragDetailAdapter=new ChinaPrescDragDetailAdapter();
        mSrlDragContain.setAdapter(chinaPrescDragDetailAdapter);

        chinaPrescDragDetailAdapter.setNewData(datas);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_download://下载
                if(electronicPrescriptions!=null){
                    showWaitDialog();
                    download();
                }
                break;
        }
    }

    private void download(){
        DownLoadUtils.downLoadPic(electronicPrescriptions.getAccessoryUrl(), TAG, CommonlyUsedDataUtils.getInstance().getElecPrescrDir(),
                EmptyUtils.strEmpty(electronicPrescriptions.getPsn_name()+"("+electronicPrescriptions.getElectronicPrescriptionNo()+")")+".png", new DownLoadUtils.IVideoDownLoad() {
            @Override
            public void onDownLoadSuccess(File response, String tag) {
                hideWaitDialog();
                // 最后通知图库更新
                try {
                    MediaStore.Images.Media.insertImage(getContentResolver(), response.getAbsolutePath(), "title", "description");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                showTipDialog("下载成功,存储地址:"+ CommonlyUsedDataUtils.getInstance().getElecPrescrDir());
            }

            @Override
            public void onDownLoadFail(String errorText) {
                hideWaitDialog();
                showShortToast(errorText);
            }
        });
    }

    public static void newIntance(Context context, String electronicPrescriptionNo) {
        Intent intent = new Intent(context, OutpatChinaPrescrDetailActivtiy.class);
        intent.putExtra("electronicPrescriptionNo", electronicPrescriptionNo);
        context.startActivity(intent);
    }
}
