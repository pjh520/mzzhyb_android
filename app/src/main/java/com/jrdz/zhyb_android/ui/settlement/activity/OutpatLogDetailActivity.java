package com.jrdz.zhyb_android.ui.settlement.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.FileUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.settlement.adapter.OutpatLogListAdapter;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatLogDetailModel;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatLogListModel;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatRecordDetailModel;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatRegListModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.MMKVUtils;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-05-11
 * 描    述： 门诊日志详情
 * ================================================
 */
public class OutpatLogDetailActivity extends BaseActivity {
    private TextView mTvIptOtpNo;
    private TextView mTvMdtrtId;
    private TextView mTvHomeNo;
    private TextView mTvVisitDate;
    private TextView mTvPatientInfo;
    private TextView mTvSex;
    private TextView mTvAge;
    private TextView mTvOccupation;
    private TextView mTvAddress;
    private TextView mTvPhone;
    private TextView mTvOnsetDate;
    private TextView mTvDisecataContain;
    private TextView mTvStatus,mTvDrName;
    private FrameLayout mFlDownload;
    private TextView mTvRemarks, mTvDownload;

    private String MdtrtinfoId;
    private OutpatLogDetailModel.DataBean dataBean;

    @Override
    public int getLayoutId() {
        return R.layout.activity_outpat_log_detail;
    }

    @Override
    public void initView() {
        super.initView();
        mTvIptOtpNo = findViewById(R.id.tv_ipt_otp_no);
        mTvMdtrtId = findViewById(R.id.tv_mdtrt_id);
        mTvHomeNo = findViewById(R.id.tv_home_no);
        mTvVisitDate = findViewById(R.id.tv_visit_date);
        mTvPatientInfo = findViewById(R.id.tv_patient_info);
        mTvSex = findViewById(R.id.tv_sex);
        mTvAge = findViewById(R.id.tv_age);
        mTvOccupation = findViewById(R.id.tv_occupation);
        mTvAddress = findViewById(R.id.tv_address);
        mTvPhone = findViewById(R.id.tv_phone);
        mTvOnsetDate = findViewById(R.id.tv_onset_date);
        mTvDisecataContain = findViewById(R.id.tv_disecata_contain);
        mTvStatus= findViewById(R.id.tv_status);
        mTvDrName = findViewById(R.id.tv_dr_name);
        mTvRemarks = findViewById(R.id.tv_remarks);
        mFlDownload = findViewById(R.id.fl_download);
        mTvDownload = findViewById(R.id.tv_download);
    }

    @Override
    public void initData() {
        MdtrtinfoId = getIntent().getStringExtra("MdtrtinfoId");
        super.initData();

        if (Constants.Configure.IS_POS){
            mFlDownload.setVisibility(View.GONE);
        }else {
            mFlDownload.setVisibility(View.VISIBLE);
        }

        showWaitDialog();
        getData();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvDownload.setOnClickListener(this);
    }

    //获取数据
    private void getData() {
        OutpatLogDetailModel.sendOutpatLogDetailRequest(TAG, MdtrtinfoId, new CustomerJsonCallBack<OutpatLogDetailModel>() {
            @Override
            public void onRequestError(OutpatLogDetailModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(OutpatLogDetailModel returnData) {
                hideWaitDialog();
                dataBean = returnData.getData();
                if (dataBean != null) {
                    mTvIptOtpNo.setText("门" + getResources().getString(R.string.spaces_half) + "诊" + getResources().getString(R.string.spaces_half) + "号：" +
                            EmptyUtils.strEmptyToText(dataBean.getIpt_otp_no(), "--"));
                    mTvMdtrtId.setText("就" + getResources().getString(R.string.spaces_half) + "诊" + getResources().getString(R.string.spaces_half) + "ID：" +
                            EmptyUtils.strEmptyToText(dataBean.getMdtrt_id(), "--"));
                    mTvHomeNo.setText("户主姓名：" + EmptyUtils.strEmptyToText(dataBean.getOuseholder(), "--"));
                    mTvVisitDate.setText("就诊日期：" + EmptyUtils.strEmptyToText(dataBean.getCreateDT(), "--"));
                    mTvPatientInfo.setText("患者信息：" + dataBean.getPsn_name() + "(" + dataBean.getMdtrt_cert_no() + ")");
                    mTvSex.setText("性" + getResources().getString(R.string.spaces) + getResources().getString(R.string.spaces) + "别：" + ("1".equals(dataBean.getSex()) ? "男" : "女"));
                    mTvAge.setText("年" + getResources().getString(R.string.spaces) + getResources().getString(R.string.spaces) + "龄：" + EmptyUtils.strEmptyToText(dataBean.getAge(), "--"));
                    mTvOccupation.setText("职" + getResources().getString(R.string.spaces) + getResources().getString(R.string.spaces) + "业：" + EmptyUtils.strEmptyToText(dataBean.getOccupation(), "--"));
                    mTvAddress.setText("居住地址：" + EmptyUtils.strEmptyToText(dataBean.getAddress(), "--"));
                    mTvPhone.setText("联系方式：" + EmptyUtils.strEmptyToText(dataBean.getContact(), "--"));
                    mTvOnsetDate.setText("发病日期：" + EmptyUtils.strEmptyToText(dataBean.getOnsetdate(), "--"));
                    mTvDisecataContain.setText(EmptyUtils.strEmptyToText(dataBean.getDise_name(), "--"));
                    mTvStatus.setText("1".equals(dataBean.getIsInitialDiagnosis())?"初诊":"复诊");
                    mTvDrName.setText("医师姓名：" + EmptyUtils.strEmptyToText(dataBean.getDr_name(), "--"));
                    mTvRemarks.setText("备" + getResources().getString(R.string.spaces) + getResources().getString(R.string.spaces) + "注：" + EmptyUtils.strEmptyToText(dataBean.getRemark(), "--"));
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()) {
            case R.id.tv_download://下载
                //2022-05-11 限制最多只能选择100条数据
                if(dataBean==null){
                    showShortToast("数据有误，请重新进入页面下载");
                    return;
                }
                showWaitDialog("文件下载中");
                ThreadUtils.getSinglePool().execute(new Runnable() {
                    @Override
                    public void run() {
                        String fileName = "门诊日志-" + System.currentTimeMillis() + ".txt";
                        StringBuilder outpatLog = new StringBuilder();
                        outpatLog.append(dataBean.getPsn_name()).append("(").append(dataBean.getIpt_otp_no()).append(")").append("\n");
                        outpatLog.append("门诊号：").append(EmptyUtils.strEmptyToText(dataBean.getIpt_otp_no(), "--")).append("\n")
                                .append("就诊ID：").append(EmptyUtils.strEmptyToText(dataBean.getMdtrt_id(), "--")).append("\n")
                                .append("户主姓名：").append(EmptyUtils.strEmptyToText(dataBean.getOuseholder(), "--")).append("\n")
                                .append("就诊日期：").append(EmptyUtils.strEmptyToText(dataBean.getCreateDT(), "--")).append("\n")
                                .append("患者信息：").append(dataBean.getPsn_name()).append("(").append(dataBean.getMdtrt_cert_no()).append(")").append("\n")
                                .append("性别：").append("1".equals(dataBean.getSex()) ? "男" : "女").append("\n")
                                .append("年龄：").append(EmptyUtils.strEmptyToText(dataBean.getAge(), "--")).append("\n")
                                .append("职业：").append(EmptyUtils.strEmptyToText(dataBean.getOccupation(), "--")).append("\n")
                                .append("居住地址：").append(EmptyUtils.strEmptyToText(dataBean.getAddress(), "--")).append("\n")
                                .append("联系方式：").append(EmptyUtils.strEmptyToText(dataBean.getContact(), "--")).append("\n")
                                .append("发病日期：").append(EmptyUtils.strEmptyToText(dataBean.getOnsetdate(), "--")).append("\n")
                                .append("初步诊断：").append(EmptyUtils.strEmptyToText(dataBean.getDise_name(), "--")).append("\n")
                                .append("医师姓名：").append(EmptyUtils.strEmptyToText(dataBean.getDr_name(), "--")).append("\n")
                                .append("备注：").append(EmptyUtils.strEmptyToText(dataBean.getRemark(), "--")).append("\n\n\n\n");

                        String isSuccess = FileUtils.writeTxtToFile(outpatLog.toString(), CommonlyUsedDataUtils.getInstance().getOutpatLogDir(), fileName);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideWaitDialog();
                                if (EmptyUtils.isEmpty(isSuccess)) {
                                    showTipDialog("下载成功,存储地址:" + CommonlyUsedDataUtils.getInstance().getOutpatLogDir() + fileName);
                                } else {
                                    showTipDialog(isSuccess);
                                }
                            }
                        });
                    }
                });
                break;
        }
    }

    public static void newIntance(Context context, String MdtrtinfoId) {
        Intent intent = new Intent(context, OutpatLogDetailActivity.class);
        intent.putExtra("MdtrtinfoId", MdtrtinfoId);
        context.startActivity(intent);
    }
}
