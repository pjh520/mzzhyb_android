package com.jrdz.zhyb_android.ui.settlement.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.FileUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.utils.wheel.TimeWheelUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.database.CatalogueModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.adapter.WestCataEnaListAdapter;
import com.jrdz.zhyb_android.ui.settlement.adapter.OutpatLogListAdapter;
import com.jrdz.zhyb_android.ui.settlement.adapter.OutpatRegListAdapter;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatLogListModel;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatRegListModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.MMKVUtils;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/29
 * 描    述：门诊日志
 * ================================================
 */
public class OutpatLogListActivtiy extends BaseRecyclerViewActivity {
    private AppCompatEditText mEtSearch;
    private TextView mTvSearch, mTvTime, mTvSearchTime;
    private ShapeTextView mTvDownload;
    private ShapeLinearLayout mSllTime;
    private View mLine;
    private LinearLayout mLlBottom;
    private FrameLayout mFlCbAll;
    private CheckBox mCbAll;
    private TextView mTvAll;
    private ShapeTextView mTvDownloadAll;

    private boolean isEditStatus = false;//是否时编辑状态
    int selectNum = 0;//选中的药品数量
    private String key = "";//关键词搜索
    private String begntime = "";//时间搜索
    private TimeWheelUtils timeWheelUtils;
    private int maxChoose = 100;//最多可以下载多少条数据

    @Override
    public int getLayoutId() {
        return R.layout.activity_outpat_loglist;
    }

    @Override
    public void initView() {
        super.initView();

        mEtSearch = findViewById(R.id.et_search);
        mTvSearch = findViewById(R.id.tv_search);
        mSllTime = findViewById(R.id.sll_time);
        mTvTime = findViewById(R.id.tv_time);
        mTvSearchTime = findViewById(R.id.tv_search_time);
        mTvDownload = findViewById(R.id.tv_download);
        mLine = findViewById(R.id.line);
        mLlBottom = findViewById(R.id.ll_bottom);
        mFlCbAll = findViewById(R.id.fl_cb_all);
        mCbAll = findViewById(R.id.cb_all);
        mTvAll = findViewById(R.id.tv_all);
        mTvDownloadAll = findViewById(R.id.tv_download_all);
    }

    @Override
    public void initAdapter() {
        mAdapter = new OutpatLogListAdapter();
    }

    @Override
    public void initData() {
        super.initData();

        if (Constants.Configure.IS_POS){
            mTvDownload.setVisibility(View.GONE);
        }else {
            mTvDownload.setVisibility(View.VISIBLE);
        }

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    onRefresh(mRefreshLayout);
                    return true;
                }
                return false;
            }
        });
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    key="";
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                }
            }
        });
        mTvSearch.setOnClickListener(this);

        mSllTime.setOnClickListener(this);
        mTvSearchTime.setOnClickListener(this);
        mTvDownload.setOnClickListener(this);
        mFlCbAll.setOnClickListener(this);
        mTvDownloadAll.setOnClickListener(this);
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void getData() {
        super.getData();

        OutpatLogListModel.sendOutpatLogListRequest(TAG, key, begntime, String.valueOf(mPageNum), "20",
                new CustomerJsonCallBack<OutpatLogListModel>() {
                    @Override
                    public void onRequestError(OutpatLogListModel returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(OutpatLogListModel returnData) {
                        hideRefreshView();

                        List<OutpatLogListModel.DataBean> infos = returnData.getData();
                        if (mAdapter != null && infos != null) {
                            if (mPageNum == 0) {
                                if (mCbAll!=null&&mCbAll.isChecked()) {//全选状态，加载更多需要勾选加载的
                                    selectNum = infos.size();
                                    if (selectNum>=maxChoose){
                                        showShortToast("一次最多只能选中"+maxChoose+"条日志下载");
                                        List<OutpatLogListModel.DataBean> frontDatas =infos.subList(0,maxChoose);
                                        for (OutpatLogListModel.DataBean messageData : frontDatas) {
                                            messageData.setChoose(true);
                                        }
                                        selectNum =frontDatas.size();
                                    }else {
                                        for (OutpatLogListModel.DataBean messageData : infos) {
                                            messageData.setChoose(true);
                                        }
                                    }

                                    setTvSelected();
                                }

                                mAdapter.setNewData(infos);
                                if (infos.size() <= 0) {
                                    mAdapter.isUseEmpty(true);
                                }
                            } else {
                                if (mCbAll!=null&&mCbAll.isChecked()&&mAdapter.getData().size()<maxChoose) {//全选状态，加载更多需要勾选加载的
                                    selectNum = mAdapter.getData().size()+infos.size();
                                    if (selectNum>=maxChoose){
                                        showShortToast("一次最多只能选中"+maxChoose+"条日志下载");
                                        List<OutpatLogListModel.DataBean> frontDatas =infos.subList(0,maxChoose-mAdapter.getData().size());
                                        for (OutpatLogListModel.DataBean messageData : frontDatas) {
                                            messageData.setChoose(true);
                                        }
                                        selectNum = mAdapter.getData().size()+frontDatas.size();
                                    }else {
                                        for (OutpatLogListModel.DataBean messageData : infos) {
                                            messageData.setChoose(true);
                                        }
                                    }

                                    setTvSelected();
                                }

                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_search://关键词搜索
                if (EmptyUtils.isEmpty(mEtSearch.getText().toString())) {
                    showShortToast("请输入名字/身份证号搜索");
                    return;
                }

                key = mEtSearch.getText().toString();
                begntime = "";
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.sll_time://选择时间
                KeyboardUtils.hideSoftInput(OutpatLogListActivtiy.this);
                if (timeWheelUtils == null) {
                    timeWheelUtils = new TimeWheelUtils();
                    timeWheelUtils.isShowDay(true, true, true, true, true, true);
                }
                timeWheelUtils.showTimeWheel(OutpatLogListActivtiy.this, "发病日期", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mTvTime.setText(EmptyUtils.strEmpty(dateInfo));
                    }
                });
                break;
            case R.id.tv_search_time://时间搜索
                if (EmptyUtils.isEmpty(mTvTime.getText().toString())) {
                    showShortToast("请选择就诊日期");
                    return;
                }

                key = "";
                begntime = mTvTime.getText().toString();
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.tv_download://下载
                if (isEditStatus) {
                    mTvDownload.setText("下载");
                    mTvDownload.getShapeDrawableBuilder().setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent))
                            .setSolidColor(getResources().getColor(R.color.color_4970e0)).intoBackground();
                    mTvDownload.setTextColor(getResources().getColor(R.color.white));
                    mLine.setVisibility(View.GONE);
                    mLlBottom.setVisibility(View.GONE);

                    cleanChoose();
                    ((OutpatLogListAdapter) mAdapter).setIsEditStatus(false);

                    mCbAll.setChecked(false);
                    selectNum = 0;
                    setTvSelected();
                } else {
                    mTvDownload.setText("取消");
                    mTvDownload.getShapeDrawableBuilder().setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1)).setStrokeColor(getResources().getColor(R.color.color_4970e0))
                            .setSolidColor(getResources().getColor(R.color.bar_transparent)).intoBackground();
                    mTvDownload.setTextColor(getResources().getColor(R.color.color_4970e0));
                    mLine.setVisibility(View.VISIBLE);
                    mLlBottom.setVisibility(View.VISIBLE);

                    ((OutpatLogListAdapter) mAdapter).setIsEditStatus(true);
                }

                isEditStatus = !isEditStatus;
                break;
            case R.id.fl_cb_all:
                if (mCbAll.isChecked()) {//取消全选
                    mCbAll.setChecked(false);
                    cleanChoose();

                    selectNum = 0;
                    mAdapter.notifyDataSetChanged();
                } else {//全选
                    mCbAll.setChecked(true);
                    allChoose();
                    mAdapter.notifyDataSetChanged();
                }

                setTvSelected();
                break;
            case R.id.tv_download_all://下载全部
                //2022-05-11 限制最多只能选择100条数据
                if (selectNum==0){
                    showShortToast("请选择要下载的日志");
                    return;
                }

                showWaitDialog("文件下载中");
                ThreadUtils.getSinglePool().execute(new Runnable() {
                    @Override
                    public void run() {
                        String fileName="门诊日志-"+System.currentTimeMillis()+".txt";
                        StringBuilder outpatLog = new StringBuilder();
                        List<OutpatLogListModel.DataBean> outpatLogs = ((OutpatLogListAdapter) mAdapter).getData();
                        outpatLog.append(outpatLogs.get(0).getPsn_name()).append("(").append(outpatLogs.get(0).getIpt_otp_no()).append(")").append("\n");
                        for (OutpatLogListModel.DataBean datum : outpatLogs) {
                            if (datum.isChoose()){
                                outpatLog.append("门诊号：").append(EmptyUtils.strEmptyToText(datum.getIpt_otp_no(),"--")).append("\n")
                                        .append("就诊ID：").append(EmptyUtils.strEmptyToText(datum.getMdtrt_id(),"--")).append("\n")
                                        .append("户主姓名：").append(EmptyUtils.strEmptyToText(datum.getOuseholder(),"--")).append("\n")
                                        .append("就诊日期：").append(EmptyUtils.strEmptyToText(datum.getCreateDT(),"--")).append("\n")
                                        .append("患者信息：").append(datum.getPsn_name()).append("(").append(datum.getMdtrt_cert_no()).append(")").append("\n")
                                        .append("性别：").append("1".equals(datum.getSex())?"男":"女").append("\n")
                                        .append("年龄：").append(EmptyUtils.strEmptyToText(datum.getAge(),"--")).append("\n")
                                        .append("职业：").append(EmptyUtils.strEmptyToText(datum.getOccupation(),"--")).append("\n")
                                        .append("居住地址：").append(EmptyUtils.strEmptyToText(datum.getAddress(),"--")).append("\n")
                                        .append("联系方式：").append(EmptyUtils.strEmptyToText(datum.getContact(),"--")).append("\n")
                                        .append("发病日期：").append(EmptyUtils.strEmptyToText(datum.getOnsetdate(),"--")).append("\n")
                                        .append("初步诊断：").append(EmptyUtils.strEmptyToText(datum.getDise_name(),"--")).append("\n")
                                        .append("医师姓名：").append(EmptyUtils.strEmptyToText(datum.getDr_name(),"--")).append("\n")
                                        .append("备注：").append(EmptyUtils.strEmptyToText(datum.getRemark(),"--")).append("\n\n\n\n");
                            }
                        }

                        String isSuccess=FileUtils.writeTxtToFile(outpatLog.toString(),CommonlyUsedDataUtils.getInstance().getOutpatLogDir(),fileName);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideWaitDialog();
                                if (EmptyUtils.isEmpty(isSuccess)){
                                    showTipDialog("下载成功,存储地址:"+CommonlyUsedDataUtils.getInstance().getOutpatLogDir()+fileName);
                                }else {
                                    showTipDialog(isSuccess);
                                }
                            }
                        });
                    }
                });
                break;
        }
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        OutpatLogListModel.DataBean itemData = ((OutpatLogListAdapter) adapter).getItem(position);
        switch (view.getId()) {
            case R.id.tv_detail://详细信息
                OutpatLogDetailActivity.newIntance(OutpatLogListActivtiy.this, itemData.getMdtrtinfoId());
                break;
            case R.id.fl_cb://选中需要下载的数据
                CheckBox cb = view.findViewById(R.id.cb);
                if (cb.isChecked()) {//取消选中
                    cb.setChecked(false);
                    itemData.setChoose(false);
                    selectNum--;

                    //取消全选
                    mCbAll.setChecked(false);
                } else {//选中
                    if (selectNum==maxChoose){
                        showShortToast("一次最多只能选中"+maxChoose+"条日志下载");
                        return;
                    }

                    cb.setChecked(true);
                    itemData.setChoose(true);
                    selectNum++;

                    if (selectNum==maxChoose||selectNum==mAdapter.getData().size()){
                        //全选
                        mCbAll.setChecked(true);
                    }
                }

                setTvSelected();
                break;
        }
    }

    //清除选中
    private void cleanChoose() {
        for (OutpatLogListModel.DataBean messageData : ((OutpatLogListAdapter) mAdapter).getData()) {
            messageData.setChoose(false);
        }
    }

    //全选
    private void allChoose() {
        List<OutpatLogListModel.DataBean> datas = ((OutpatLogListAdapter) mAdapter).getData();
        if (datas.size()<=maxChoose){
            for (OutpatLogListModel.DataBean messageData : datas) {
                messageData.setChoose(true);
            }
            selectNum = mAdapter.getData().size();
        }else {
            showShortToast("一次最多只能选中"+maxChoose+"条日志下载");
            List<OutpatLogListModel.DataBean> front100Datas = datas.subList(0, maxChoose);
            List<OutpatLogListModel.DataBean> after100Datas = datas.subList(maxChoose, datas.size());
            for (OutpatLogListModel.DataBean messageData : front100Datas) {
                messageData.setChoose(true);
            }
            for (OutpatLogListModel.DataBean messageData : after100Datas) {
                messageData.setChoose(false);
            }
            selectNum = maxChoose;
        }
    }

    //设置选中的数据
    private void setTvSelected() {
        if (mTvAll!=null){
            mTvAll.setText("全选(" + selectNum + "条日志)");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timeWheelUtils != null) {
            timeWheelUtils.dissTimeWheel();
            timeWheelUtils = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, OutpatLogListActivtiy.class);
        context.startActivity(intent);
    }
}
