package com.jrdz.zhyb_android.ui.settlement.activity;

import android.content.Context;
import android.content.Intent;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatRecordDetailModel;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatRegListModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-05-11
 * 描    述： 门诊记录详情
 * ================================================
 */
public class OutpatRecordDetailActivity extends BaseActivity {
    private TextView mTvIptOtpNo;
    private TextView mTvMdtrtId;
    private TextView mTvPsnNo;
    private TextView mTvDeptName;
    private TextView mTvDrName;
    private TextView mTvCreateTime;
    private TextView mTvDisespec;
    private TextView mTvMainDesc;
    private TextView mTvDisecataContain;
    private TextView mTvCycleDays;
    private TextView mTvDrugsContain;

    private String OutpatientRregistrationId;

    @Override
    public int getLayoutId() {
        return R.layout.activity_outpat_record_detail;
    }

    @Override
    public void initView() {
        super.initView();
        mTvIptOtpNo = findViewById(R.id.tv_ipt_otp_no);
        mTvMdtrtId = findViewById(R.id.tv_mdtrt_id);
        mTvPsnNo = findViewById(R.id.tv_psn_no);
        mTvDeptName = findViewById(R.id.tv_dept_name);
        mTvDrName = findViewById(R.id.tv_dr_name);
        mTvCreateTime = findViewById(R.id.tv_create_time);
        mTvDisespec = findViewById(R.id.tv_disespec);
        mTvMainDesc = findViewById(R.id.tv_main_desc);
        mTvDisecataContain = findViewById(R.id.tv_disecata_contain);
        mTvCycleDays = findViewById(R.id.tv_cycle_days);
        mTvDrugsContain = findViewById(R.id.tv_drugs_contain);
    }

    @Override
    public void initData() {
        OutpatientRregistrationId = getIntent().getStringExtra("OutpatientRregistrationId");
        super.initData();

        showWaitDialog();
        getData();
    }

    //获取数据
    private void getData() {
        OutpatRecordDetailModel.sendOutpatRecordDetailRequest(TAG, OutpatientRregistrationId, new CustomerJsonCallBack<OutpatRecordDetailModel>() {
            @Override
            public void onRequestError(OutpatRecordDetailModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(OutpatRecordDetailModel returnData) {
                hideWaitDialog();
                OutpatRecordDetailModel.DataBean data = returnData.getData();
                if (data!=null){
                    OutpatRecordDetailModel.DataBean.OutpatientRregistrationsBean outpatientRregistrations = data.getOutpatientRregistrations();
                    List<OutpatRecordDetailModel.DataBean.DiseinfosBean> diseinfos = data.getDiseinfos();
                    List<OutpatRecordDetailModel.DataBean.FeedetailsBean> feedetails = data.getFeedetails();

                    if (outpatientRregistrations!=null){
                        mTvIptOtpNo.setText("门"+getResources().getString(R.string.spaces_half)+ "诊"+getResources().getString(R.string.spaces_half)+"号："+
                                EmptyUtils.strEmptyToText(outpatientRregistrations.getIpt_otp_no(), "--"));
                        mTvMdtrtId.setText("就"+getResources().getString(R.string.spaces_half)+"诊"+getResources().getString(R.string.spaces_half)+"ID："+
                                EmptyUtils.strEmptyToText(outpatientRregistrations.getMdtrt_id(), "--"));
                        mTvPsnNo.setText("人员信息："+outpatientRregistrations.getPsn_name()+"("+outpatientRregistrations.getMdtrt_cert_no()+")");
                        mTvDeptName.setText("科室名称："+EmptyUtils.strEmptyToText(outpatientRregistrations.getDept_name(), "--"));
                        mTvDrName.setText("医师姓名："+EmptyUtils.strEmptyToText(outpatientRregistrations.getDr_name(), "--"));
                        mTvCreateTime.setText("挂号时间："+ EmptyUtils.strEmptyToText(outpatientRregistrations.getCreateDT(), "--"));
                        mTvDisespec.setText("病种名称："+ EmptyUtils.strEmptyToText(outpatientRregistrations.getDise_name(), "--"));
                        mTvMainDesc.setText("病情描述："+ EmptyUtils.strEmptyToText(outpatientRregistrations.getMain_cond_dscr(), "--"));
                        mTvCycleDays.setText("用药周期天数："+EmptyUtils.strEmptyToText(outpatientRregistrations.getPrd_days(), "--") +"天");
                    }

                    String disecatas="";
                    if (diseinfos!=null&&!diseinfos.isEmpty()){
                        for (int i = 0,size=diseinfos.size(); i < size; i++) {
                            OutpatRecordDetailModel.DataBean.DiseinfosBean diseinfo = diseinfos.get(i);
                            String diag_type = CommonlyUsedDataUtils.getInstance().getDataDicLabel("DIAG_TYPE",diseinfo.getDiag_type());
                            if (i<size-1){
                                disecatas+=diseinfo.getDiag_name()+"("+diag_type+")\n";
                            }else {
                                disecatas+=diseinfo.getDiag_name()+"("+diag_type+")";
                            }
                        }
                    }
                    mTvDisecataContain.setText(disecatas);

                    String drugs="";
                    if (feedetails!=null&&!feedetails.isEmpty()){
                        for (int i = 0,size=feedetails.size(); i < size; i++) {
                            OutpatRecordDetailModel.DataBean.FeedetailsBean feedetail = feedetails.get(i);

                            if (i<size-1){
                                drugs+=feedetail.getMedins_list_name()+"   "+feedetail.getCnt()+"\n";
                            }else {
                                drugs+=feedetail.getMedins_list_name()+"   "+feedetail.getCnt();
                            }
                        }
                    }
                    mTvDrugsContain.setText(drugs);
                }
            }
        });
    }

    public static void newIntance(Context context,String OutpatientRregistrationId) {
        Intent intent = new Intent(context, OutpatRecordDetailActivity.class);
        intent.putExtra("OutpatientRregistrationId", OutpatientRregistrationId);
        context.startActivity(intent);
    }
}
