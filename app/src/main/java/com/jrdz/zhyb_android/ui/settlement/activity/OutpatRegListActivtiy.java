package com.jrdz.zhyb_android.ui.settlement.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.TreatmentActivity;
import com.jrdz.zhyb_android.ui.home.adapter.StmtInfoListAdapter;
import com.jrdz.zhyb_android.ui.home.model.StmtTotalListModel;
import com.jrdz.zhyb_android.ui.login.activity.LoginActivity;
import com.jrdz.zhyb_android.ui.settlement.adapter.OutpatRegListAdapter;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatRegListModel;
import com.jrdz.zhyb_android.ui.settlement.model.QueryPersonalInfoModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/29
 * 描    述：门诊记录
 * ================================================
 */
public class OutpatRegListActivtiy extends BaseRecyclerViewActivity {
    private LinearLayout mLlTitle;
    private FrameLayout mFlClose;
    private AppCompatEditText mEtSearch;
    private TextView mTvSearch;
    private CustomerDialogUtils customerDialogUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_outpat_reglist;
    }

    @Override
    public void initView() {
        super.initView();

        mLlTitle = findViewById(R.id.ll_title);
        mFlClose = findViewById(R.id.fl_close);
        mEtSearch = findViewById(R.id.et_search);
        mTvSearch = findViewById(R.id.tv_search);
    }

    @Override
    public void initAdapter() {
        mAdapter = new OutpatRegListAdapter();
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mLlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    onRefresh(mRefreshLayout);
                    return true;
                }
                return false;
            }
        });
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                }
            }
        });
        mFlClose.setOnClickListener(this);
        mTvSearch.setOnClickListener(this);
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void getData() {
        super.getData();

        OutpatRegListModel.sendOutpatRegListRequest(TAG, mEtSearch.getText().toString(), String.valueOf(mPageNum), "20",
                new CustomerJsonCallBack<OutpatRegListModel>() {
                    @Override
                    public void onRequestError(OutpatRegListModel returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(OutpatRegListModel returnData) {
                        hideRefreshView();
                        List<OutpatRegListModel.DataBean> infos = returnData.getData();
                        if (mAdapter != null && infos != null) {
                            if (mPageNum == 0) {
                                mAdapter.setNewData(infos);
                                if (infos.size() <= 0) {
                                    mAdapter.isUseEmpty(true);
                                }
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close://关闭
                goFinish();
                break;
            case R.id.tv_search://搜索
                if (EmptyUtils.isEmpty(mEtSearch.getText().toString())) {
                    showShortToast("请输入名字/身份证号搜索");
                    return;
                }

                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
        }
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        OutpatRegListModel.DataBean itemData = ((OutpatRegListAdapter) adapter).getItem(position);
        switch (view.getId()) {
            case R.id.tv_detail://详细信息
                if (itemData.getStep()>=4) {//已结算成功
                    OutpatRecordDetailActivity.newIntance(OutpatRegListActivtiy.this, itemData.getOutpatientRregistrationId());
                } else {//未结算成功 需要进入结算页面
                    if ("1".equals(itemData.getSettlementClassification())){//1医保结算
                        goOutpatSettlement(itemData.getMdtrt_cert_no(),itemData.getMdtrt_cert_type(),itemData.getMed_type(),
                                itemData.getMdtrt_id(),itemData.getIpt_otp_no(),itemData.getAddress(),itemData.getContact(),itemData.getStep());
                    }else {//2自费结算
                        OwnExpenseSettleActivity_incomplete.newIntance(OutpatRegListActivtiy.this,itemData.getIpt_otp_no(),itemData.getPsn_name(),
                                itemData.getMdtrt_cert_no(),itemData.getSex(),itemData.getAge(),itemData.getAddress(),itemData.getContact(),
                                itemData.getStep(),Constants.RequestCode.OWNEXPENSESETTLE_INCOMPLETE_CODE);
                    }
                }
                break;
            case R.id.tv_cancle://撤销按钮点击
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(OutpatRegListActivtiy.this, "确定撤销吗?", "(撤销后此条门诊记录将不在出现在您的门诊记录中)",
                        R.color.color_ff0202, 2, "取消", "确定",
                        new CustomerDialogUtils.IDialogListener() {

                            @Override
                            public void onBtn01Click() {
                            }

                            @Override
                            public void onBtn02Click() {
                                cancle(itemData.getPsn_no(), itemData.getIpt_otp_no(), itemData.getMdtrt_id(), position);
                            }
                        });
                break;
        }
    }

    //进入未结算完成的门诊结算页面
    private void goOutpatSettlement(String mdtrt_cert_no, String mdtrt_cert_type, String med_type,String mdtrt_id,String ipt_otp_no,
                                    String address,String phone,int step) {
        showWaitDialog();
        QueryPersonalInfoModel.sendQueryPersonalInfoRequest(TAG, mdtrt_cert_no, mdtrt_cert_type, med_type,
                new CustomerJsonCallBack<QueryPersonalInfoModel>() {
                    @Override
                    public void onRequestError(QueryPersonalInfoModel returnData, String msg) {
                        hideWaitDialog();
                        showTipDialog(msg);
                    }

                    @Override
                    public void onRequestSuccess(QueryPersonalInfoModel returnData) {
                        hideWaitDialog();
                        QueryPersonalInfoModel.DataBean data = returnData.getData();
                        if (data != null && data.getBaseinfo() != null && data.getInsuinfo() != null && !data.getInsuinfo().isEmpty()) {
                            OutpatSettlementActivity_incomplete.newIntance(OutpatRegListActivtiy.this, data.getBaseinfo(),
                                    data.getInsuinfo().get(0), mdtrt_cert_type, med_type,mdtrt_id,ipt_otp_no,address,phone, returnData.getPsnOpspReg(),
                                    step, Constants.RequestCode.OUTPATSETTLEMENT_INCOMPLETE_CODE);
                        } else {
                            showShortToast("数据有误，请重新查询");
                        }
                    }
                });
    }

    //撤销挂号
    private void cancle(String psn_no, String ipt_otp_no, String mdtrt_id, int pos) {
        showWaitDialog();
        BaseModel.sendoutpatRegCancelRequest(TAG, psn_no, ipt_otp_no, mdtrt_id, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                mAdapter.remove(pos);
                showShortToast("撤销成功");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constants.RequestCode.OUTPATSETTLEMENT_INCOMPLETE_CODE||requestCode == Constants.RequestCode.OWNEXPENSESETTLE_INCOMPLETE_CODE)
                && resultCode == RESULT_OK) {
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, OutpatRegListActivtiy.class);
        context.startActivity(intent);
    }
}
