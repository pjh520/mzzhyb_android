package com.jrdz.zhyb_android.ui.settlement.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.FileUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.model.StmtInfoListModel;
import com.jrdz.zhyb_android.ui.settlement.adapter.SettleRecordAdapter;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.io.File;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/29
 * 描    述：门诊结算列表
 * ================================================
 */
public class OutpatSettleRecordActivtiy extends BaseRecyclerViewActivity {
    private LinearLayout mLlTitle;
    private FrameLayout mFlClose;
    private AppCompatEditText mEtSearch;
    private TextView mTvSearch;

    @Override
    public int getLayoutId() {
        return R.layout.activity_outpat_reglist;
    }

    @Override
    public void initView() {
        super.initView();

        mLlTitle = findViewById(R.id.ll_title);
        mFlClose = findViewById(R.id.fl_close);
        mEtSearch = findViewById(R.id.et_search);
        mTvSearch = findViewById(R.id.tv_search);
    }

    @Override
    public void initAdapter() {
        mAdapter = new SettleRecordAdapter();
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mLlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    onRefresh(mRefreshLayout);
                    return true;
                }
                return false;
            }
        });
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())){
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                }
            }
        });
        mFlClose.setOnClickListener(this);
        mTvSearch.setOnClickListener(this);
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void getData() {
        super.getData();

        StmtInfoListModel.sendSettleInfoRequest(TAG,mEtSearch.getText().toString(), String.valueOf(mPageNum), "20", new CustomerJsonCallBack<StmtInfoListModel>() {
            @Override
            public void onRequestError(StmtInfoListModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(StmtInfoListModel returnData) {
                hideRefreshView();
                List<StmtInfoListModel.DataBean> infos = returnData.getData();
                if (mAdapter!=null&&infos != null) {
                    for (StmtInfoListModel.DataBean info : infos) {
                        for (DataDicModel insutypeDatum : CommonlyUsedDataUtils.getInstance().getInsutypeData()) {
                            if (info.getInsutype().equals(insutypeDatum.getValue())) {
                                info.setInsutype_name(insutypeDatum.getLabel());
                                break;
                            }
                        }

                        for (DataDicModel medTypeDatum : CommonlyUsedDataUtils.getInstance().getMedTypeData()) {
                            if (info.getMed_type().equals(medTypeDatum.getValue())) {
                                info.setMed_type_name(medTypeDatum.getLabel());
                                break;
                            }
                        }
                    }

                    if (mPageNum == 0) {
                        mAdapter.setNewData(infos);
                        if (infos.size() <= 0) {
                            mAdapter.isUseEmpty(true);
                        }
                    } else {
                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.fl_close://关闭
                goFinish();
                break;
            case R.id.tv_search://搜索
                if (EmptyUtils.isEmpty(mEtSearch.getText().toString())) {
                    showShortToast("请输入名字/身份证号搜索");
                    return;
                }

                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
        }
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter,view,position);
        StmtInfoListModel.DataBean itemData = ((SettleRecordAdapter) adapter).getItem(position);
        SettleRecordDetailActivtiy.newIntance(OutpatSettleRecordActivtiy.this,itemData.getSetl_id());
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter,view,position);

        switch (view.getId()) {
            case R.id.tv_cancle://撤销按钮点击
                StmtInfoListModel.DataBean itemData = ((SettleRecordAdapter) adapter).getItem(position);
                settletmentCancle(itemData.getIpt_otp_no(),itemData.getPsn_no(),itemData.getSetl_id(),itemData.getMdtrt_id(),position);
                break;
            case R.id.tv_up_smallticket://上传小票
                StmtInfoListModel.DataBean itemData02 = ((SettleRecordAdapter) adapter).getItem(position);
                //获取本地图片路径
                //判断本地图片是否存在 若存在直接上传图片
                //若不存在 则跳转小票页面
                String imagPath = BaseGlobal.getImageDir() + "smallTicket/" + itemData02.getSetl_id() + ".jpg";
                if (FileUtils.isFileExists2(imagPath)) {
                    showWaitDialog();
                    upSmallTicket(itemData02.getSetl_id(),imagPath);
                }else {
                    SmallTicketActivity.newIntance(OutpatSettleRecordActivtiy.this,itemData02.getSetl_id(), "2",position, Constants.RequestCode.GET_SMALLTICKET_CODE2);
                }
                break;
        }
    }

    //门诊结算撤销
    private void settletmentCancle(String ipt_otp_no,String psn_no, String setl_id, String mdtrt_id, int pos) {
        showWaitDialog();
        BaseModel.sendCancleSettletmentRequest(TAG,ipt_otp_no,psn_no, setl_id, mdtrt_id, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                showShortToast("召回成功");
                onRefresh(mRefreshLayout);
            }
        });
    }

    //上传小票
    private void upSmallTicket(String setlId,String imagPath) {
        BaseModel.sendUploadFileRequest(TAG, setlId, new File(imagPath), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("上传成功");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode== Constants.RequestCode.GET_SMALLTICKET_CODE2&&resultCode==RESULT_OK){
            int pos = data.getIntExtra("pos", -1);
            if (-1!=pos){
                ((SettleRecordAdapter)mAdapter).getData().get(pos).setIsUpload("1");
                mAdapter.notifyItemChanged(pos);
            }
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, OutpatSettleRecordActivtiy.class);
        context.startActivity(intent);
    }
}
