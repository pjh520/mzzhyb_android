package com.jrdz.zhyb_android.ui.settlement.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.centerm.cpay.ai.lib.IdInfo;
import com.centerm.cpay.ai.lib.OnResultCallbackListener;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.wheel.TimeWheelUtils;
import com.frame.compiler.widget.CustomViewPager;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.layout.ShapeRelativeLayout;
import com.hjq.shape.view.ShapeEditText;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.database.CatalogueModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.pos.CPayDevice;
import com.jrdz.zhyb_android.pos.CPayID;
import com.jrdz.zhyb_android.ui.catalogue.activity.DiseCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.SelectCataManageActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.UpdateWestCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.model.ChinaPrescrCataModel;
import com.jrdz.zhyb_android.ui.catalogue.model.DiseCataModel;
import com.jrdz.zhyb_android.ui.catalogue.model.WestCataListModel;
import com.jrdz.zhyb_android.ui.catalogue.model.WestPrescrCataModel;
import com.jrdz.zhyb_android.ui.home.activity.ScanCataListActivtiy;
import com.jrdz.zhyb_android.ui.home.model.ScanOrganModel;
import com.jrdz.zhyb_android.ui.mine.activity.HwScanActivity;
import com.jrdz.zhyb_android.ui.mine.activity.ZbarScanActivity;
import com.jrdz.zhyb_android.ui.settlement.adapter.OutpSettAdapter;
import com.jrdz.zhyb_android.ui.settlement.adapter.RlDiseAdapter;
import com.jrdz.zhyb_android.ui.settlement.model.FeedetailModel;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatFeelistUpModel;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatMdtrtUpAParmaModel;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatRegModel;
import com.jrdz.zhyb_android.ui.settlement.model.QueryPersonalInfoModel;
import com.jrdz.zhyb_android.ui.settlement.model.SaveSettleModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.widget.pop.DropDownDataPop;
import com.jrdz.zhyb_android.widget.pop.UpdateNumPop;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/22
 * 描    述：普通门诊页面
 * ================================================
 */
public class OwnExpenseSettleActivity extends BaseActivity {
    protected HorizontalScrollView mHscvTab;
    protected LinearLayout mLlOutpatReg;
    private TextView mTvOutpatReg;
    protected ImageView mIvOutpatReg;
    protected TextView mTvTagOutpatUpA;
    protected TextView mTvOutpatUpA;
    protected ImageView mIvOutpatUpA;
    protected LinearLayout mLlOutpatUpA;
    protected TextView mTvTagFeelistUp;
    protected TextView mTvFeelistUp;
    protected ImageView mIvFeelistUp;
    protected LinearLayout mLlFeelistUp;
    protected TextView mTvTagSave;
    protected TextView mTvSave;
    protected ImageView mIvSave;
    protected LinearLayout mLlSave;
    protected CustomViewPager mVp;
    //挂号需要的数据
    private LinearLayout mLlGend, mLlBrdy, mLlOccupation;
    private TextView mTvOccupation;
    private LinearLayout mLlOnsetDate;
    private TextView mTvOnsetDate;
    private RadioGroup mSrbGroup;
    private ShapeTextView mTvReadCard, mTvReg;
    protected TextView mTvGend, mTvBrdy;
    protected EditText mTvPsnName, mTvMdtrtCertNo, mTvAge, mEtHouseName, mEtAddress, mEtPhone, mEtRemarks;
    //门诊就诊信息上传需要的数据
    private LinearLayout mLlSettleUpa;
    private LinearLayout mLlSetlectDisespec;
    private TextView mTvSetlectDisespecTag, mTvSetlectDisespec;
    private View mLineSetlectDisespec;
    private ShapeEditText mEtMainDesc;
    private TextView mTvSetlectDise;
    private ShapeTextView mTvUpA;
    private RlDiseAdapter rlDiseAdapter;
    //门诊明细上传需要的数据
    private LinearLayout mLlSettleFeelist, mLlWestPrescr, mLlChinesePrescr;
    private ShapeFrameLayout mFlCycleDaysReduce, mflCycleDaysAdd;
    protected EditText mEtCycleDays;
    private TextView mTvSetlectDrugs, mTvScanSetlectDrugs;
    private TextView mTvEmptyText;
    protected ShapeLinearLayout mLlDrugsContain, mLlWestprescrDrugsContain, mLlChinaprescrDrugsContain, mLlChinaprescrDrugsInfo;
    private View mLineWestprescrDrugs, mLineChinaprescrDrugs;
    protected ShapeRelativeLayout mSllHejiPrice, mSllChinaprescrHejiPrice, mSllOutpatFeelistUp;
    protected TextView mTvChinaprescrDrugsNum, mTvChinaprescrDrugsRemarks, mTvChinaprescrHejiPrice, mTvHejiPrice;
    private TextView mTvTotalPrice;
    private ShapeTextView mTvOutpatFeelistUp;
    private TextView mTvFeelistTitle;
    private LinearLayout mLlFeelist;
    protected TextView mTvFeedetlSnTag, mTvFeedetlSn;
    protected TextView mTvChrgBchno;
    private TextView mTvCancle;
    private ShapeTextView mTvCommit, mTvFeelistNext;
    //结算view需要的数据
    private TextView mTvPsnSetlway;
    private ShapeTextView mTvSaveSettlePre, mTvSaveSettle;

    protected static final int normalColor = Color.parseColor("#999999");
    protected static final int selectColor = Color.parseColor("#333333");
    protected static final int dp_30 = RxTool.getContext().getResources().getDimensionPixelOffset(R.dimen.dp_30);

    protected String mdtrt_cert_type = "02";
    protected String ipt_otp_no;//门诊号
    protected String chrg_bchno;//收费批次号
    //页面集合
    List<View> views = new ArrayList<>();
    private String totalPrice = "0";
    protected ArrayList<CatalogueModel> catalogueListDatas = new ArrayList<>();//存储选中的药品目录
    private UpdateNumPop updateNumPop;
    private boolean isElectPrescr, isOutpatientLog;
    private TimeWheelUtils timeWheelUtils;
    protected WestPrescrCataModel mWestPrescrCata;//西药中成药处方数据
    protected ChinaPrescrCataModel mChinaPrescrCata;//中药处方数据
    private DropDownDataPop dropDownDataPop;
    private CPayID cPayID;

    @Override
    public int getLayoutId() {
        return R.layout.activity_outpat_settlement;
    }

    @Override
    public void initView() {
        super.initView();
        mHscvTab = findViewById(R.id.hscv_tab);
        mLlOutpatReg = findViewById(R.id.ll_outpat_reg);
        mTvOutpatReg = findViewById(R.id.tv_outpat_reg);
        mIvOutpatReg = findViewById(R.id.iv_outpat_reg);

        mTvTagOutpatUpA = findViewById(R.id.tv_tag_outpat_upA);
        mTvOutpatUpA = findViewById(R.id.tv_outpat_upA);
        mIvOutpatUpA = findViewById(R.id.iv_outpat_upA);
        mLlOutpatUpA = findViewById(R.id.ll_outpat_upA);

        mTvTagFeelistUp = findViewById(R.id.tv_tag_feelist_up);
        mTvFeelistUp = findViewById(R.id.tv_feelist_up);
        mIvFeelistUp = findViewById(R.id.iv_feelist_up);
        mLlFeelistUp = findViewById(R.id.ll_feelist_up);

        mTvTagSave = findViewById(R.id.tv_tag_save);
        mTvSave = findViewById(R.id.tv_save);
        mIvSave = findViewById(R.id.iv_save);
        mLlSave = findViewById(R.id.ll_save);

        mVp = findViewById(R.id.vp);
    }

    @Override
    public void initData() {
        super.initData();
        isElectPrescr = "1".equals(MechanismInfoUtils.getElectronicPrescription()) ? true : false;//是否开启电子处方
        isOutpatientLog = "1".equals(MechanismInfoUtils.getOutpatientMdtrtinfo()) ? true : false;//是否开启门诊日志

        initViewPager();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvReadCard.setOnClickListener(this);
        mLlGend.setOnClickListener(this);
        mLlBrdy.setOnClickListener(this);
        mLlOccupation.setOnClickListener(this);
        mLlOnsetDate.setOnClickListener(this);
        mLlOutpatReg.setOnClickListener(this);

        mLlOutpatUpA.setOnClickListener(this);
        mLlFeelistUp.setOnClickListener(this);
        mLlSave.setOnClickListener(this);
        mTvReg.setOnClickListener(this);
        mTvSetlectDise.setOnClickListener(this);
        mTvUpA.setOnClickListener(this);
        mLlWestPrescr.setOnClickListener(this);
        mLlChinesePrescr.setOnClickListener(this);
        mFlCycleDaysReduce.setOnClickListener(this);
        mflCycleDaysAdd.setOnClickListener(this);
        mTvSetlectDrugs.setOnClickListener(this);
        mTvScanSetlectDrugs.setOnClickListener(this);
        mTvOutpatFeelistUp.setOnClickListener(this);
        mTvCancle.setOnClickListener(this);
        mTvFeelistNext.setOnClickListener(this);
        mTvCommit.setOnClickListener(this);
        mTvSaveSettlePre.setOnClickListener(this);
        mTvSaveSettle.setOnClickListener(this);
        mLlSetlectDisespec.setOnClickListener(this);

        mTvMdtrtCertNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 18) {
                    Map<String, String> certNoinfo = CommonlyUsedDataUtils.getInstance().getBirthdayAgeSex(s.toString());
                    if (certNoinfo != null) {
                        mTvGend.setTag(certNoinfo.get("sex"));
                        mTvGend.setText("1".equals(String.valueOf(certNoinfo.get("sex"))) ? "男" : "女");
                        mTvBrdy.setText(certNoinfo.get("birthday"));
                        mTvAge.setText(certNoinfo.get("age"));
                    } else {
                        showShortToast("身份证不合法");
                    }
                }
            }
        });
    }

    //初始化viewpager
    private void initViewPager() {
        views.clear();
        views.add(initRegView());//门诊挂号view
        views.add(initUpa());//门诊就诊信息上传view
        views.add(initFeelistUp());//门诊明细上传view
        views.add(initSave());//结算view
        OutpSettAdapter outpSettAdapter = new OutpSettAdapter(views);
        mVp.setAdapter(outpSettAdapter);
    }

    //初始化门诊挂号view
    private View initRegView() {
        mImmersionBar.keyboardEnable(true).init();
        //门诊挂号view
        View view = LayoutInflater.from(this).inflate(R.layout.layout_ownexpense_settle_reg, null, false);
        LinearLayout mLlPsnName = view.findViewById(R.id.ll_psn_name);
        mTvPsnName = view.findViewById(R.id.tv_psn_name);
        LinearLayout mLlMdtrtCertNo = view.findViewById(R.id.ll_mdtrt_cert_no);
        mTvMdtrtCertNo = view.findViewById(R.id.tv_mdtrt_cert_no);
        mTvReadCard = view.findViewById(R.id.tv_read_card);
        View line06 = view.findViewById(R.id.line_06);
        TextView mTvMdtrtCertNoDesc= view.findViewById(R.id.tv_mdtrt_cert_no_desc);
        mLlGend = view.findViewById(R.id.ll_gend);
        mTvGend = view.findViewById(R.id.tv_gend);
        View line07 = view.findViewById(R.id.line_07);
        mLlBrdy = view.findViewById(R.id.ll_brdy);
        mTvBrdy = view.findViewById(R.id.tv_brdy);
        View line08 = view.findViewById(R.id.line_08);
        LinearLayout mLlage = view.findViewById(R.id.ll_age);
        mTvAge = view.findViewById(R.id.tv_age);
        View line09 = view.findViewById(R.id.line_09);
        LinearLayout mLlHouseName = view.findViewById(R.id.ll_house_name);
        mEtHouseName = view.findViewById(R.id.et_house_name);
        View line01 = view.findViewById(R.id.line_01);
        mLlOccupation = view.findViewById(R.id.ll_occupation);
        mTvOccupation = view.findViewById(R.id.tv_occupation);
        View line02 = view.findViewById(R.id.line_02);
        LinearLayout mLlAddress = view.findViewById(R.id.ll_address);
        TextView tvAddressTag= view.findViewById(R.id.tv_address_tag);
        mEtAddress = view.findViewById(R.id.et_address);
        View line03 = view.findViewById(R.id.line_03);
        LinearLayout mLlPhone = view.findViewById(R.id.ll_phone);
        TextView tvPhoneTag= view.findViewById(R.id.tv_phone_tag);
        mEtPhone = view.findViewById(R.id.et_phone);
        View line04 = view.findViewById(R.id.line_04);
        mLlOnsetDate = view.findViewById(R.id.ll_onset_date);
        mTvOnsetDate = view.findViewById(R.id.tv_onset_date);
        View line05 = view.findViewById(R.id.line_05);
        RelativeLayout mLlRemarks = view.findViewById(R.id.ll_remarks);
        mEtRemarks = view.findViewById(R.id.et_remarks);
        mSrbGroup = view.findViewById(R.id.srb_group);
        TextView mTvDoctor = view.findViewById(R.id.tv_doctor);
        mTvReg = view.findViewById(R.id.tv_reg);

        if (isOutpatientLog) {//开启门诊日志
            mLlMdtrtCertNo.setVisibility(View.VISIBLE);
            line06.setVisibility(View.VISIBLE);
            mTvMdtrtCertNoDesc.setVisibility(View.VISIBLE);
            mLlGend.setVisibility(View.VISIBLE);
            line07.setVisibility(View.VISIBLE);
            mLlBrdy.setVisibility(View.VISIBLE);
            line08.setVisibility(View.VISIBLE);
            mLlage.setVisibility(View.VISIBLE);
            line09.setVisibility(View.VISIBLE);

            mLlHouseName.setVisibility(View.VISIBLE);
            line01.setVisibility(View.VISIBLE);
            mLlOccupation.setVisibility(View.VISIBLE);
            line02.setVisibility(View.VISIBLE);
            mLlAddress.setVisibility(View.VISIBLE);
            tvAddressTag.setVisibility(View.VISIBLE);
            line03.setVisibility(View.VISIBLE);
            mLlPhone.setVisibility(View.VISIBLE);
            tvPhoneTag.setVisibility(View.VISIBLE);
            line04.setVisibility(View.VISIBLE);
            mLlOnsetDate.setVisibility(View.VISIBLE);
            line05.setVisibility(View.VISIBLE);
            mSrbGroup.setVisibility(View.VISIBLE);
        } else if (isElectPrescr) {//开启电子处方
            mLlMdtrtCertNo.setVisibility(View.GONE);
            line06.setVisibility(View.GONE);
            mTvMdtrtCertNoDesc.setVisibility(View.GONE);
            mLlGend.setVisibility(View.VISIBLE);
            mTvGend.setHint("请选择性别");
            line07.setVisibility(View.VISIBLE);
            mLlBrdy.setVisibility(View.GONE);
            line08.setVisibility(View.GONE);
            mLlage.setVisibility(View.VISIBLE);
            mTvAge.setHint("请输入年龄");
            line09.setVisibility(View.VISIBLE);

            mLlHouseName.setVisibility(View.GONE);
            line01.setVisibility(View.GONE);
            mLlOccupation.setVisibility(View.GONE);
            line02.setVisibility(View.GONE);
            mLlAddress.setVisibility(View.VISIBLE);
            tvAddressTag.setVisibility(View.INVISIBLE);
            line03.setVisibility(View.VISIBLE);
            mLlPhone.setVisibility(View.VISIBLE);
            tvPhoneTag.setVisibility(View.INVISIBLE);
            line04.setVisibility(View.VISIBLE);
            mLlOnsetDate.setVisibility(View.GONE);
            line05.setVisibility(View.GONE);
            mSrbGroup.setVisibility(View.GONE);
        } else {//都不开启
            mLlMdtrtCertNo.setVisibility(View.GONE);
            line06.setVisibility(View.GONE);
            mTvMdtrtCertNoDesc.setVisibility(View.GONE);
            mLlGend.setVisibility(View.GONE);
            line07.setVisibility(View.GONE);
            mLlBrdy.setVisibility(View.GONE);
            line08.setVisibility(View.GONE);
            mLlage.setVisibility(View.GONE);
            line09.setVisibility(View.GONE);

            mLlHouseName.setVisibility(View.GONE);
            line01.setVisibility(View.GONE);
            mLlOccupation.setVisibility(View.GONE);
            line02.setVisibility(View.GONE);
            mLlAddress.setVisibility(View.GONE);
            line03.setVisibility(View.GONE);
            mLlPhone.setVisibility(View.GONE);
            line04.setVisibility(View.GONE);
            mLlOnsetDate.setVisibility(View.GONE);
            line05.setVisibility(View.GONE);
            mSrbGroup.setVisibility(View.GONE);
        }

        if (Constants.Configure.IS_POS) {
            mTvReadCard.setText("读卡识别");
        } else {
            mTvReadCard.setText("证件识别");
        }

        mTvDoctor.setText(LoginUtils.getUserName());
        return view;
    }

    //初始化门诊就诊信息上传view
    private View initUpa() {
        View view = LayoutInflater.from(this).inflate(R.layout.layout_outpat_settle_upa, null, false);
        mLlSettleUpa = view.findViewById(R.id.ll_settle_upa);
        RecyclerView mRlDise = view.findViewById(R.id.rl_dise);
        mTvUpA = view.findViewById(R.id.tv_upA);

        mRlDise.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rlDiseAdapter = new RlDiseAdapter();
        rlDiseAdapter.setHeaderAndEmpty(true);
        mRlDise.setAdapter(rlDiseAdapter);

        //设置列表emptyview
        View emptyView = LayoutInflater.from(this).inflate(R.layout.layout_empty_wrap_trans_view, mRlDise, false);
        rlDiseAdapter.setEmptyView(emptyView);

        //设置头部view
        View headView = LayoutInflater.from(this).inflate(R.layout.layout_outpat_settle_upa_headview, mRlDise, false);
        //慢性病病种
        mLlSetlectDisespec = headView.findViewById(R.id.ll_setlect_disespec);
        mTvSetlectDisespecTag = headView.findViewById(R.id.tv_setlect_disespec_tag);
        mTvSetlectDisespec = headView.findViewById(R.id.tv_setlect_disespec);
        mLineSetlectDisespec = headView.findViewById(R.id.line_setlect_disespec);
        mEtMainDesc = headView.findViewById(R.id.et_main_desc);
        mTvSetlectDise = headView.findViewById(R.id.tv_setlect_dise);

        rlDiseAdapter.addHeaderView(headView);
        rlDiseAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                switch (view.getId()) {
                    case R.id.tv_delete:
                        adapter.remove(position);
                        break;
                }
            }
        });

        return view;
    }

    //初始化门诊明细上传view
    private View initFeelistUp() {
        View view = LayoutInflater.from(this).inflate(R.layout.layout_outpat_settle_feelist_up, null, false);
        ShapeLinearLayout mSllPrescr = view.findViewById(R.id.sll_prescr);
        //处方
        mLlSettleFeelist = view.findViewById(R.id.ll_settle_feelist);
        mLlWestPrescr = view.findViewById(R.id.ll_west_prescr);
        mLlChinesePrescr = view.findViewById(R.id.ll_chinese_prescr);
        //药品列表头部view
        mFlCycleDaysReduce = view.findViewById(R.id.fl_cycle_days_reduce);
        mEtCycleDays = view.findViewById(R.id.et_cycle_days);
        mflCycleDaysAdd = view.findViewById(R.id.fl_cycle_days_add);
        mTvSetlectDrugs = view.findViewById(R.id.tv_setlect_drugs);
        mTvScanSetlectDrugs = view.findViewById(R.id.tv_scan_setlect_drugs);
        //药品列表空view
        mTvEmptyText = view.findViewById(R.id.tv_empty_text);
        //药品列表
        mLlDrugsContain = view.findViewById(R.id.ll_drugs_contain);
        //西药中成药处方药品列表
        mLineWestprescrDrugs = view.findViewById(R.id.line_westprescr_drugs);
        mLlWestprescrDrugsContain = view.findViewById(R.id.ll_westprescr_drugs_contain);
        mSllHejiPrice = view.findViewById(R.id.sll_heji_price);
        mTvHejiPrice = view.findViewById(R.id.tv_heji_price);
        //中药处方药品列表
        mLineChinaprescrDrugs = view.findViewById(R.id.line_chinaprescr_drugs);
        mLlChinaprescrDrugsContain = view.findViewById(R.id.ll_chinaprescr_drugs_contain);

        mLlChinaprescrDrugsInfo = view.findViewById(R.id.ll_chinaprescr_drugs_info);
        mTvChinaprescrDrugsNum = view.findViewById(R.id.tv_chinaprescr_drugs_num);
        mTvChinaprescrDrugsRemarks = view.findViewById(R.id.tv_chinaprescr_drugs_remarks);
        mSllChinaprescrHejiPrice = view.findViewById(R.id.sll_chinaprescr_heji_price);
        mTvChinaprescrHejiPrice = view.findViewById(R.id.tv_chinaprescr_heji_price);
        //门诊明细上传按钮
        //计算总价格
        mTvTotalPrice = view.findViewById(R.id.tv_total_price);
        mSllOutpatFeelistUp = view.findViewById(R.id.sll_outpat_feelist_up);
        mTvOutpatFeelistUp = view.findViewById(R.id.tv_outpat_feelist_up);
        //提交
        mTvCommit = view.findViewById(R.id.tv_commit);
        //下一步
        mTvFeelistNext = view.findViewById(R.id.tv_feelist_next);

        if (isElectPrescr) {//开启电子处方
            mSllPrescr.setVisibility(View.VISIBLE);
        } else {//都不开启
            mSllPrescr.setVisibility(View.GONE);
        }

        return view;
    }

    //初始化结算view
    private View initSave() {
        View view = LayoutInflater.from(this).inflate(R.layout.layout_outpat_settle_save, null, false);
        //门诊明细上传列表
        mTvFeelistTitle = view.findViewById(R.id.tv_feelist_title);
        mLlFeelist = view.findViewById(R.id.ll_feelist);
        mTvFeedetlSnTag = view.findViewById(R.id.tv_feedetl_sn_tag);
        mTvFeedetlSn = view.findViewById(R.id.tv_feedetl_sn);
        mTvChrgBchno = view.findViewById(R.id.tv_chrg_bchno);
        mTvCancle = view.findViewById(R.id.tv_cancle);
        //个人结算
        mTvPsnSetlway = view.findViewById(R.id.tv_psn_setlway);
        mTvSaveSettlePre = view.findViewById(R.id.tv_save_settle_pre);
        mTvSaveSettle = view.findViewById(R.id.tv_save_settle);

        mTvFeedetlSnTag.setText("门诊号");

        mTvPsnSetlway.setTag("01");
        mTvPsnSetlway.setText("按项目结算");
        return view;
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_read_card:
                if (Constants.Configure.IS_POS) {
                    nfcIdCard();
                } else {
                    dist15CertNo(mTvMdtrtCertNo.getText().toString());
                }
                break;
            case R.id.ll_gend://性别
                selectGend();
                break;
            case R.id.ll_brdy://出生日期
                selectBrdy();
                break;
            case R.id.ll_occupation://职业
                selectOccupation();
                break;
            case R.id.ll_onset_date://发病日期
                selectOnsetDate();
                break;
            case R.id.tv_reg://挂号
                reg();
                break;
            case R.id.ll_setlect_disespec://选择病种
                DiseCataActivity.newIntance(OwnExpenseSettleActivity.this, "2", Constants.RequestCode.GET_SETLECT_CHRONIC_DISE_CODE2);
                break;
            case R.id.tv_setlect_dise://选择诊断信息
                DiseCataActivity.newIntance(OwnExpenseSettleActivity.this, "3", Constants.RequestCode.GET_SETLECT_DISE_CODE);
                break;
            case R.id.tv_upA://门诊信息上传
                upA();
                break;
            case R.id.ll_west_prescr://西药中成药处方
                if (!EmptyUtils.isEmpty(chrg_bchno)) {
                    showShortToast("请先撤销门诊费用明细，再进入西药中成药处方");
                    return;
                }
                WestPrescrActivity.newIntance(OwnExpenseSettleActivity.this, ipt_otp_no, mTvPsnName.getText().toString(), mTvGend.getText().toString(),
                        mTvAge.getText().toString(), mEtAddress.getText().toString(), mEtPhone.getText().toString(), mWestPrescrCata,
                        Constants.RequestCode.SELECT_WESTPRESCR_CODE);
                break;
            case R.id.ll_chinese_prescr://中药处方
                if (!EmptyUtils.isEmpty(chrg_bchno)) {
                    showShortToast("请先撤销门诊费用明细，再进入中药处方");
                    return;
                }
                ChinaPrescrActivity.newIntance(OwnExpenseSettleActivity.this, ipt_otp_no, mTvPsnName.getText().toString(), mTvGend.getText().toString(),
                        mTvAge.getText().toString(), mEtAddress.getText().toString(), mEtPhone.getText().toString(), mChinaPrescrCata,
                        Constants.RequestCode.SELECT_CHINAPRESCR_CODE);
                break;
            case R.id.fl_cycle_days_reduce://周期天数减
                if (!EmptyUtils.isEmpty(chrg_bchno)) {
                    showShortToast("请先撤销门诊费用明细，再操作周期天数");
                    return;
                }

                if (EmptyUtils.isEmpty(mEtCycleDays.getText().toString())) {
                    mEtCycleDays.setText("1");
                } else if (new BigDecimal(mEtCycleDays.getText().toString()).intValue() > 1) {
                    mEtCycleDays.setText(new BigDecimal(mEtCycleDays.getText().toString()).subtract(new BigDecimal("1")).toPlainString());
                } else {
                    showShortToast("数量最低值为1");
                    hideWaitDialog();
                    return;
                }
                RxTool.setEditTextCursorLocation(mEtCycleDays);
                break;
            case R.id.fl_cycle_days_add://周期天数加
                if (!EmptyUtils.isEmpty(chrg_bchno)) {
                    showShortToast("请先撤销门诊费用明细，再操作周期天数");
                    return;
                }
                if (!EmptyUtils.isEmpty(chrg_bchno)) {
                    showShortToast("请先撤销门诊费用明细，再选择药品");
                    return;
                }
                if (EmptyUtils.isEmpty(mEtCycleDays.getText().toString())) {
                    mEtCycleDays.setText("1");
                } else {
                    mEtCycleDays.setText(new BigDecimal(mEtCycleDays.getText().toString()).add(new BigDecimal("1")).toPlainString());
                }
                RxTool.setEditTextCursorLocation(mEtCycleDays);
                break;
            case R.id.tv_setlect_drugs://选择药品
                if (!EmptyUtils.isEmpty(chrg_bchno)) {
                    showShortToast("请先撤销门诊费用明细，再选择药品");
                    return;
                }
                SelectCataManageActivity.newIntance(OwnExpenseSettleActivity.this, "2", Constants.RequestCode.SELECT_CATAENALIST_CODE);
                break;
            case R.id.tv_scan_setlect_drugs://扫码选择药品
                if (!EmptyUtils.isEmpty(chrg_bchno)) {
                    showShortToast("请先撤销门诊费用明细，再选择药品");
                    return;
                }
                if (Constants.Configure.IS_POS) {
                    goPosScan();
                } else {
                    goHwScan();
                }
                break;
            case R.id.tv_outpat_feelist_up://门诊明细上传
//                if (!EmptyUtils.isEmpty(chrg_bchno)) {
//                    showShortToast("已上传门诊费用明细");
//                    return;
//                }
//                outpatFeelistUp();
                break;
            case R.id.tv_commit:
                outpatFeelistUp("1");
                break;
            case R.id.tv_feelist_next://下一步 --结算
                outpatFeelistUp("2");
                break;
            case R.id.tv_cancle://撤销门诊明细
                feelistCancle();
                break;
            case R.id.tv_save_settle_pre://门诊预结算
                preSaveSettle();
                break;
            case R.id.tv_save_settle://门诊结算
                if (EmptyUtils.isEmpty(ipt_otp_no)) {
                    showShortToast("请先挂号");
                    return;
                }
                if (EmptyUtils.isEmpty(chrg_bchno)) {
                    showShortToast("请先上传门诊费用明细信息");
                    return;
                }
                if (mTvPsnSetlway.getTag() == null || EmptyUtils.isEmpty(mTvPsnSetlway.getTag().toString())) {
                    showShortToast("请选择个人结算方式");
                    return;
                }

                saveSettle();
                break;
            case R.id.ll_outpat_reg://门诊挂号
//                setTvColor(0);
                break;
            case R.id.ll_outpat_upA://门诊就诊信息上传
//                setTvColor(1);
                break;
            case R.id.ll_feelist_up://门诊明细上传
//                setTvColor(2);
                break;
            case R.id.ll_save://结算
//                setTvColor(3);
                break;
        }
    }

    //性别
    private void selectGend() {
        KeyboardUtils.hideSoftInput(OwnExpenseSettleActivity.this);
        if (dropDownDataPop == null) {
            dropDownDataPop = new DropDownDataPop(OwnExpenseSettleActivity.this, CommonlyUsedDataUtils.getInstance().getGend());
        } else {
            dropDownDataPop.setData(CommonlyUsedDataUtils.getInstance().getGend());
        }

        dropDownDataPop.setOnListener(new DropDownDataPop.IOptionListener() {
            @Override
            public void onItemClick(String item) {
                mTvGend.setTag("男".equals(item) ? "1" : "2");
                mTvGend.setText(EmptyUtils.strEmpty(item));
            }
        });

        dropDownDataPop.showPopupWindow(mTvGend);
    }

    //选择出生日期
    private void selectBrdy() {
        KeyboardUtils.hideSoftInput(OwnExpenseSettleActivity.this);
        if (timeWheelUtils == null) {
            timeWheelUtils = new TimeWheelUtils();
            timeWheelUtils.isShowDay(true, true, true, true, true, true);
        }
        timeWheelUtils.showTimeWheel(OwnExpenseSettleActivity.this, "出生日期", new TimeWheelUtils.TimeWheelClickListener() {
            @Override
            public void onchooseDate(String dateInfo) {
                mTvBrdy.setText(EmptyUtils.strEmpty(dateInfo));
            }
        });
    }

    //职业
    private void selectOccupation() {
        KeyboardUtils.hideSoftInput(OwnExpenseSettleActivity.this);
        if (dropDownDataPop == null) {
            dropDownDataPop = new DropDownDataPop(OwnExpenseSettleActivity.this, CommonlyUsedDataUtils.getInstance().getOccupationData());
        } else {
            dropDownDataPop.setData(CommonlyUsedDataUtils.getInstance().getOccupationData());
        }

        dropDownDataPop.setOnListener(new DropDownDataPop.IOptionListener() {
            @Override
            public void onItemClick(String item) {
                mTvOccupation.setText(EmptyUtils.strEmpty(item));
            }
        });

        dropDownDataPop.showPopupWindow(mTvOccupation);
    }

    //选择发病日期
    private void selectOnsetDate() {
        KeyboardUtils.hideSoftInput(OwnExpenseSettleActivity.this);
        if (timeWheelUtils == null) {
            timeWheelUtils = new TimeWheelUtils();
            timeWheelUtils.isShowDay(true, true, true, true, true, true);
        }
        timeWheelUtils.showTimeWheel(OwnExpenseSettleActivity.this, "发病日期", new TimeWheelUtils.TimeWheelClickListener() {
            @Override
            public void onchooseDate(String dateInfo) {
                mTvOnsetDate.setText(EmptyUtils.strEmpty(dateInfo));
            }
        });
    }

    //识别15位身份证
    private void dist15CertNo(String certNo) {
        if (EmptyUtils.isEmpty(certNo)) {
            showShortToast("请输入身份证号码");
            return;
        }
        if (certNo.length() != 15 && certNo.length() != 18) {
            showShortToast("请输入正确的身份证号码");
            return;
        }

        Map<String, String> certNoinfo = CommonlyUsedDataUtils.getInstance().getBirthdayAgeSex(certNo);
        if (certNoinfo != null) {
            mTvGend.setTag(certNoinfo.get("sex"));
            mTvGend.setText("1".equals(String.valueOf(certNoinfo.get("sex"))) ? "男" : "女");
            mTvBrdy.setText(certNoinfo.get("birthday"));
            mTvAge.setText(certNoinfo.get("age"));
        } else {
            showShortToast("身份证不合法");
        }
    }

    //通过nfc扫描身份证
    private void nfcIdCard() {
        showWaitDialog("正在识别身份证…");

        if (cPayID==null){
            cPayID = CPayDevice.getCPayID();
        }
        cPayID.startDetect(new OnResultCallbackListener.Stub() {
            @Override
            public void onResult(int code, Bundle bundle) {
                // https://blog.csdn.net/Bettarwang/article/details/45315091?locationNum=4
                bundle.setClassLoader(getClass().getClassLoader());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (code == 15) {

                        } else if (code == 1) {
                            hideWaitDialog();
                            IdInfo idInfo = bundle.getParcelable("idInfo");
                            if (idInfo != null) {
                                showShortToast("身份证识别成功");
                                mTvMdtrtCertNo.setText(EmptyUtils.strEmpty(idInfo.idnum));
                                RxTool.setEditTextCursorLocation(mTvMdtrtCertNo);

                                if (EmptyUtils.strEmpty(idInfo.idnum).length()==15){
                                    dist15CertNo(EmptyUtils.strEmpty(idInfo.idnum));
                                }
                            }
                        } else {
                            hideWaitDialog();
                            showShortToast(String.format("读卡失败: %s", cPayID.errorInfo(code)));
                        }
                    }
                });
            }
        });
    }

    //挂号
    private void reg() {
        if (EmptyUtils.isEmpty(mTvPsnName.getText().toString())) {
            showShortToast("请填写姓名");
            return;
        }
        String classification = "";
        String isInitialDiagnosis = "";
        if (isOutpatientLog && isElectPrescr) {//开启电子处方和门诊日志
            classification = "1";
            if (EmptyUtils.isEmpty(mTvMdtrtCertNo.getText().toString())) {
                showShortToast("请输入证件号码");
                return;
            }

            if (mTvMdtrtCertNo.getText().length()!=15&&mTvMdtrtCertNo.getText().length()!=18) {
                showShortToast("身份证号码为15位或者18位");
                return;
            }

            if (EmptyUtils.isEmpty(mTvGend.getText().toString())) {
                showShortToast("请选择性别");
                return;
            }
            if (EmptyUtils.isEmpty(mTvBrdy.getText().toString())) {
                showShortToast("请选择出生日期");
                return;
            }
            if (EmptyUtils.isEmpty(mTvAge.getText().toString())) {
                showShortToast("请输入年龄");
                return;
            }

            double ageDiff = new BigDecimal(mTvAge.getText().toString()).subtract(new BigDecimal("14")).doubleValue();
            if (ageDiff <= 0) {
                if (EmptyUtils.isEmpty(mEtHouseName.getText().toString())) {
                    showShortToast("十四岁以下的儿童必须填写家长姓名");
                    return;
                }
            }
            if (EmptyUtils.isEmpty(mEtAddress.getText().toString())) {
                showShortToast("请填写家庭住址");
                return;
            }
            if (EmptyUtils.isEmpty(mEtPhone.getText().toString())) {
                showShortToast("请填写联系方式");
                return;
            }
            if (!"1".equals(String.valueOf(mEtPhone.getText().charAt(0))) || mEtPhone.getText().length() != 11) {
                showShortToast("请输入正确的手机号码");
                return;
            }
            if (EmptyUtils.isEmpty(mTvOnsetDate.getText().toString())) {
                showShortToast("请选择发病日期");
                return;
            }

            isInitialDiagnosis = "1";
            if (mSrbGroup.getCheckedRadioButtonId() == R.id.srb_followup_visit) {
                isInitialDiagnosis = "2";
            }
        } else if (isElectPrescr) {//开启电子处方
            classification = "2";

            if (EmptyUtils.isEmpty(mTvGend.getText().toString())) {
                showShortToast("请选择性别");
                return;
            }
            if (EmptyUtils.isEmpty(mTvAge.getText().toString())) {
                showShortToast("请输入年龄");
                return;
            }

            if (!EmptyUtils.isEmpty(mEtPhone.getText().toString())&&(!"1".equals(String.valueOf(mEtPhone.getText().charAt(0))) || mEtPhone.getText().length() != 11)) {
                showShortToast("请输入正确的手机号码");
                return;
            }
        } else if (isOutpatientLog) {//开启门诊日志
            classification = "3";
            if (EmptyUtils.isEmpty(mTvMdtrtCertNo.getText().toString())) {
                showShortToast("请输入证件号码");
                return;
            }
            if (mTvMdtrtCertNo.getText().length()!=15&&mTvMdtrtCertNo.getText().length()!=18) {
                showShortToast("身份证号码为15位或者18位");
                return;
            }
            if (EmptyUtils.isEmpty(mTvGend.getText().toString())) {
                showShortToast("请选择性别");
                return;
            }
            if (EmptyUtils.isEmpty(mTvBrdy.getText().toString())) {
                showShortToast("请选择出生日期");
                return;
            }
            if (EmptyUtils.isEmpty(mTvAge.getText().toString())) {
                showShortToast("请输入年龄");
                return;
            }

            double ageDiff = new BigDecimal(mTvAge.getText().toString()).subtract(new BigDecimal("14")).doubleValue();
            if (ageDiff <= 0) {
                if (EmptyUtils.isEmpty(mEtHouseName.getText().toString())) {
                    showShortToast("十四岁以下的儿童必须填写家长姓名");
                    return;
                }
            }
            if (EmptyUtils.isEmpty(mEtAddress.getText().toString())) {
                showShortToast("请填写家庭住址");
                return;
            }
            if (EmptyUtils.isEmpty(mEtPhone.getText().toString())) {
                showShortToast("请填写联系方式");
                return;
            }
            if (!"1".equals(String.valueOf(mEtPhone.getText().charAt(0))) || mEtPhone.getText().length() != 11) {
                showShortToast("请输入正确的手机号码");
                return;
            }
            if (EmptyUtils.isEmpty(mTvOnsetDate.getText().toString())) {
                showShortToast("请选择发病日期");
                return;
            }

            isInitialDiagnosis = "1";
            if (mSrbGroup.getCheckedRadioButtonId() == R.id.srb_followup_visit) {
                isInitialDiagnosis = "2";
            }
        } else {//都不开启
            classification = "4";
        }
        showWaitDialog();
        OutpatRegModel.sendOutpatRegRequest(TAG, "", mTvPsnName.getText().toString(), "", mdtrt_cert_type,
                mTvMdtrtCertNo.getText().toString(), LoginUtils.getUserId(), LoginUtils.getUserName(), LoginUtils.getDeptId(), LoginUtils.getDeptIdName(),
                LoginUtils.getCaty(), "", String.valueOf(mTvGend.getTag()), mTvAge.getText().toString(), mTvBrdy.getText().toString(),
                mEtHouseName.getText().toString(), mTvOccupation.getText().toString(), mEtAddress.getText().toString(), mEtPhone.getText().toString(),
                mTvOnsetDate.getText().toString(), mEtRemarks.getText().toString(), isInitialDiagnosis, classification, "2","",
                new CustomerJsonCallBack<OutpatRegModel>() {
                    @Override
                    public void onRequestError(OutpatRegModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(OutpatRegModel returnData) {
                        hideWaitDialog();
                        OutpatRegModel.DataBean obj = returnData.getData();
                        if (obj != null) {
                            ipt_otp_no = obj.getIpt_otp_no();
                            setTvColor(1);
                        } else {
                            showShortToast("挂号数据返回错误，请重新挂号");
                        }
                    }
                });
    }

    //门诊信息上传
    private void upA() {
        if (EmptyUtils.isEmpty(ipt_otp_no)) {
            showShortToast("请先挂号");
            return;
        }

        if (rlDiseAdapter.getData() == null || rlDiseAdapter.getData().isEmpty()) {
            showShortToast("请选择诊断信息");
            return;
        }
        showWaitDialog();

        OutpatMdtrtUpAParmaModel outpatMdtrtUpAParma = new OutpatMdtrtUpAParmaModel();

        OutpatMdtrtUpAParmaModel.MdtrtinfoBean mdtrtinfoBean = new OutpatMdtrtUpAParmaModel.MdtrtinfoBean(ipt_otp_no,ipt_otp_no, "", "",
                EmptyUtils.strEmpty(mEtMainDesc.getText().toString()), mTvSetlectDisespec.getTag() == null ? "" : mTvSetlectDisespec.getTag().toString(),
                EmptyUtils.strEmpty(mTvSetlectDisespec.getText().toString()), "", "2");

        outpatMdtrtUpAParma.setMdtrtinfo(mdtrtinfoBean);
        for (int i = 0, size = rlDiseAdapter.getData().size(); i < size; i++) {
            DiseCataModel.DataBean diseCataData = rlDiseAdapter.getData().get(i);
            OutpatMdtrtUpAParmaModel.DiseinfoBean diseinfoBean = new OutpatMdtrtUpAParmaModel.DiseinfoBean(diseCataData.getDiag_type(), String.valueOf(i + 1),
                    diseCataData.getCode(), diseCataData.getName(), LoginUtils.getDeptIdName(), LoginUtils.getUserId(), LoginUtils.getUserName(), "1");

            outpatMdtrtUpAParma.getDiseinfo().add(diseinfoBean);
        }

        BaseModel.sendOutpatMdtrtUpARequest(TAG, JSON.toJSONString(outpatMdtrtUpAParma), "", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                setTvColor(2);
                upASuccess();
            }
        });
    }

    //门诊明细上传
    private void outpatFeelistUp(String flag) {
        if (EmptyUtils.isEmpty(ipt_otp_no)) {
            showShortToast("请先挂号");
            return;
        }
        //判断 至少要选择西药中成药处方 中药处方 外部药品的一种
        if (mWestPrescrCata == null && mChinaPrescrCata == null && (catalogueListDatas == null || catalogueListDatas.isEmpty())) {
            showShortToast("请选择药品");
            return;
        }

        //封装西药中成药数据
        String isWMCPMPrescription = "0", wmcpmDiagResults = "", wmcpmAmount = "", accessoryId1 = "",electronicPrescriptionNo1="";
        ArrayList<FeedetailModel.WmcpmPrescriptionBean> wmcpmPrescriptionBeans = new ArrayList<>();
        if (mWestPrescrCata != null) {
            isWMCPMPrescription = "1";
            wmcpmDiagResults = mWestPrescrCata.getDiagResult();
            wmcpmAmount = mWestPrescrCata.getHejiPrice();
            accessoryId1 = mWestPrescrCata.getPrescr_accessoryId();
            electronicPrescriptionNo1= mWestPrescrCata.getPrescrNo();

            CatalogueModel westPrescrCatalogueModel;
            for (WestPrescrCataModel.DataBean datum : mWestPrescrCata.getData()) {
                westPrescrCatalogueModel = datum.getCatalogueModel();
                String singleTotalPrice = new BigDecimal(westPrescrCatalogueModel.getPrice()).multiply(new BigDecimal(datum.getNum())).toPlainString();
                FeedetailModel.WmcpmPrescriptionBean wmcpmPrescriptionBean = new FeedetailModel.WmcpmPrescriptionBean(ipt_otp_no, "", "", "0",
                        westPrescrCatalogueModel.getMed_list_codg(), westPrescrCatalogueModel.getFixmedins_hilist_id(), singleTotalPrice,
                        String.valueOf(datum.getNum()), westPrescrCatalogueModel.getPrice(), EmptyUtils.strEmpty(mEtCycleDays.getText().toString()),
                        LoginUtils.getDeptId(), LoginUtils.getDeptIdName(), LoginUtils.getUserId(), LoginUtils.getUserName(), "1",
                        datum.getNumCompany(), "每次" + datum.getConsump() + datum.getConsumpCompany(), datum.getFrequency(), datum.getUsage(),
                        datum.getRemarks(),westPrescrCatalogueModel.getDrug_type_name());

                wmcpmPrescriptionBeans.add(wmcpmPrescriptionBean);
            }
        }

        //封装中药数据
        String isCMPrescription = "0", cmDiagResults = "", cmDrugNum = "", unt = "", cmused_mtd = "", cmAmount = "", cmRemark = "", accessoryId2 = "",electronicPrescriptionNo2="";
        ArrayList<FeedetailModel.CmPrescriptionBean> cmPrescriptionBeans = new ArrayList<>();
        if (mChinaPrescrCata != null) {
            isCMPrescription = "1";
            cmDiagResults = mChinaPrescrCata.getDiagResult();
            cmDrugNum = String.valueOf(mChinaPrescrCata.getNum());
            unt = mChinaPrescrCata.getNumCompany();
            cmused_mtd = mChinaPrescrCata.getUsage();
            cmAmount = mChinaPrescrCata.getHejiPrice();
            cmRemark = mChinaPrescrCata.getRemarks();
            accessoryId2 = mChinaPrescrCata.getPrescr_accessoryId();
            electronicPrescriptionNo2= mChinaPrescrCata.getPrescrNo();

            CatalogueModel chinaPrescrCatalogueModel;
            for (ChinaPrescrCataModel.DataBean datum : mChinaPrescrCata.getData()) {
                chinaPrescrCatalogueModel = datum.getCatalogueModel();
                String singleTotalPrice = new BigDecimal(chinaPrescrCatalogueModel.getPrice()).multiply(new BigDecimal(datum.getWeight())).toPlainString();
                FeedetailModel.CmPrescriptionBean cmPrescriptionBean = new FeedetailModel.CmPrescriptionBean(ipt_otp_no, "", "", "0",
                        chinaPrescrCatalogueModel.getMed_list_codg(), chinaPrescrCatalogueModel.getFixmedins_hilist_id(), singleTotalPrice,
                        String.valueOf(datum.getWeight()), chinaPrescrCatalogueModel.getPrice(), EmptyUtils.strEmpty(mEtCycleDays.getText().toString()),
                        LoginUtils.getDeptId(), LoginUtils.getDeptIdName(), LoginUtils.getUserId(), LoginUtils.getUserName(), "1",
                        datum.getWeightCompany());

                cmPrescriptionBeans.add(cmPrescriptionBean);
            }
        }

        ArrayList<FeedetailModel.FeedetailBean> feedetailBeans = new ArrayList<>();

        CatalogueModel catalogueModel;
        for (int i = 0; i < catalogueListDatas.size(); i++) {
            catalogueModel = catalogueListDatas.get(i);
            View view = mLlDrugsContain.getChildAt(i);
            ShapeEditText etRemarks = view.findViewById(R.id.et_remarks);

            String singleTotalPrice = new BigDecimal(catalogueModel.getPrice()).multiply(new BigDecimal(catalogueModel.getNum())).toPlainString();
            FeedetailModel.FeedetailBean feedetailBean = new FeedetailModel.FeedetailBean(ipt_otp_no, "", "", "0", catalogueModel.getMed_list_codg(),
                    catalogueModel.getFixmedins_hilist_id(), singleTotalPrice, String.valueOf(catalogueModel.getNum()), catalogueModel.getPrice(), EmptyUtils.strEmpty(mEtCycleDays.getText().toString()),
                    LoginUtils.getDeptId(), LoginUtils.getDeptIdName(), LoginUtils.getUserId(), LoginUtils.getUserName(), "1", etRemarks.getText().toString());

            feedetailBeans.add(feedetailBean);
        }
        FeedetailModel feedetailModels = new FeedetailModel(mEtCycleDays.getText().toString(), isWMCPMPrescription, isCMPrescription,
                "2", wmcpmDiagResults, wmcpmAmount, cmDiagResults, cmDrugNum, unt, cmused_mtd, cmAmount, cmRemark,
                accessoryId1, accessoryId2,ipt_otp_no,electronicPrescriptionNo1,electronicPrescriptionNo2);
        feedetailModels.setWmcpmPrescription(wmcpmPrescriptionBeans);
        feedetailModels.setCmPrescription(cmPrescriptionBeans);
        feedetailModels.setFeedetail(feedetailBeans);

        showWaitDialog();
        OutpatFeelistUpModel.sendOutpatFeelistUpRequest(TAG, JSON.toJSONString(feedetailModels), "",
                new CustomerJsonCallBack<OutpatFeelistUpModel>() {
                    @Override
                    public void onRequestError(OutpatFeelistUpModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(OutpatFeelistUpModel returnData) {
                        hideWaitDialog();
                        chrg_bchno = returnData.getChrg_bchno();//收费批次号
                        if ("1".equals(flag)){
                            feelistCommit();
                        }else if ("2".equals(flag)){
                            showShortToast("门诊明细上传成功");
                            mTvFeedetlSn.setText(EmptyUtils.strEmpty(ipt_otp_no));
                            mTvChrgBchno.setText(EmptyUtils.strEmpty(chrg_bchno));
                            setTvColor(3);

                            outpatFeelistUpSuccess();
                        }
                    }
                });
    }

    //撤销门诊费用明细上传
    private void feelistCancle() {
        showWaitDialog();
        BaseModel.sendOutpatFeeListUpCancelRequest(TAG, "", ipt_otp_no, chrg_bchno, "2","", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("撤销门诊费用明细成功");
                feelistCancleSuccess();
                chrg_bchno = "";
                mTvFeedetlSn.setText("");
                mTvChrgBchno.setText("");
                setTvColor(2);
            }
        });
    }

    //门诊明细上传页面 提交按钮
    private void feelistCommit() {
        if (EmptyUtils.isEmpty(ipt_otp_no)) {
            showShortToast("请先挂号");
            return;
        }
        if (EmptyUtils.isEmpty(chrg_bchno)) {
            showShortToast("请先上传门诊费用明细信息");
            return;
        }
        showWaitDialog();
        BaseModel.sendFeelistCommitRequest(TAG, ipt_otp_no, ipt_otp_no, mTvPsnName.getText().toString(), "",
                "2", new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        feelistCommitSuccess();
                        showShortToast(EmptyUtils.strEmpty(returnData.getMsg()));
                        CommitSuccessActivity.newIntance(OwnExpenseSettleActivity.this);
                        goFinish();
                    }
                });
    }

    //门诊预结算
    private void preSaveSettle() {
        if (EmptyUtils.isEmpty(ipt_otp_no)) {
            showShortToast("请先挂号");
            return;
        }
        if (EmptyUtils.isEmpty(chrg_bchno)) {
            showShortToast("请先上传门诊费用明细信息");
            return;
        }
        if (mTvPsnSetlway.getTag() == null || EmptyUtils.isEmpty(mTvPsnSetlway.getTag().toString())) {
            showShortToast("请选择个人结算方式");
            return;
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("psn_no", "");
        jsonObject.put("mdtrt_cert_type", mdtrt_cert_type);
        jsonObject.put("mdtrt_cert_no", mTvMdtrtCertNo.getText().toString());
        jsonObject.put("med_type", "");
        jsonObject.put("medfee_sumamt", totalPrice);
        jsonObject.put("psn_setlway", String.valueOf(mTvPsnSetlway.getTag()));
        jsonObject.put("mdtrt_id", ipt_otp_no);
        jsonObject.put("chrg_bchno", chrg_bchno);
        jsonObject.put("acct_used_flag", "1");
        jsonObject.put("insutype", "");
        jsonObject.put("settlementClassification", "2");

        PreOutpatSettleActivity.newIntance(OwnExpenseSettleActivity.this, JSON.toJSONString(jsonObject), "");
    }

    //门诊结算
    private void saveSettle() {
        showWaitDialog();
        SaveSettleModel.sendSaveSettleRequest(TAG, ipt_otp_no,"", mdtrt_cert_type, mTvMdtrtCertNo.getText().toString(), "",
                totalPrice, String.valueOf(mTvPsnSetlway.getTag()), ipt_otp_no, chrg_bchno, "", "",
                mTvPsnName.getText().toString(), "", "2", "", new CustomerJsonCallBack<SaveSettleModel>() {
                    @Override
                    public void onRequestError(SaveSettleModel returnData, String msg) {
                        hideWaitDialog();
                        showTipDialog(msg);
                    }

                    @Override
                    public void onRequestSuccess(SaveSettleModel returnData) {
                        hideWaitDialog();
                        SaveSettleModel.DataBean obj = returnData.getData();
                        saveSettleSuccess();
                        if (obj != null && obj.getSetlinfo() != null) {
                            SaveSettleModel.DataBean.SetlinfoBean setlinfo = obj.getSetlinfo();
                            SmallTicketActivity.newIntance(OwnExpenseSettleActivity.this, setlinfo.getSetl_id(), "1");
                            goFinish();
                        }
                    }
                });
    }

    //门诊就诊信息上传成功
    protected void upASuccess() {}
    //门诊就诊明细上传成功
    protected void outpatFeelistUpSuccess() {}
    //门诊就诊明细提交成功
    protected void feelistCommitSuccess() {}
    //门诊就诊明细撤销成功
    protected void feelistCancleSuccess() {}
    //结算成功
    protected void saveSettleSuccess() {}

    //设置头部tab的 文字颜色以及底部的图片线条展示
    private void setTvColor(int pos) {
        mIvOutpatReg.setVisibility(View.GONE);
        mIvOutpatUpA.setVisibility(View.GONE);
        mIvFeelistUp.setVisibility(View.GONE);
        mIvSave.setVisibility(View.GONE);

        switch (pos) {
            case 0:
                mImmersionBar.keyboardEnable(true).init();
                mHscvTab.fling(0);
                mHscvTab.smoothScrollTo(mLlOutpatReg.getLeft() - dp_30, 0);
                mIvOutpatReg.setVisibility(View.VISIBLE);
                break;
            case 1://移动到门诊就诊信息上传tab
                mImmersionBar.keyboardEnable(false).init();
                mHscvTab.fling(0);
                mHscvTab.smoothScrollTo(mLlOutpatUpA.getLeft() - dp_30, 0);
                mIvOutpatUpA.setVisibility(View.VISIBLE);
                break;
            case 2://移动到门诊就诊信息上传tab
                mHscvTab.fling(0);
                mHscvTab.smoothScrollTo(mLlFeelistUp.getLeft() - dp_30, 0);
                mIvFeelistUp.setVisibility(View.VISIBLE);
                break;
            case 3://移动到门诊就诊信息上传tab
                mHscvTab.fling(0);
                mHscvTab.smoothScrollTo(mLlSave.getLeft() - dp_30, 0);
                mIvSave.setVisibility(View.VISIBLE);
                break;
        }

        mTvOutpatUpA.setTextColor(normalColor);
        mTvFeelistUp.setTextColor(normalColor);
        mTvSave.setTextColor(normalColor);

        mTvTagOutpatUpA.setTextColor(normalColor);
        mTvTagFeelistUp.setTextColor(normalColor);
        mTvTagSave.setTextColor(normalColor);

        if (pos > 0) {
            mTvOutpatUpA.setTextColor(selectColor);
            mTvTagOutpatUpA.setTextColor(selectColor);
        }

        if (pos > 1) {
            mTvFeelistUp.setTextColor(selectColor);
            mTvTagFeelistUp.setTextColor(selectColor);
        }

        if (pos > 2) {
            mTvSave.setTextColor(selectColor);
            mTvTagSave.setTextColor(selectColor);
        }

        mVp.setCurrentItem(pos, true);
    }

    //使用pos机自带的扫码
    private void goPosScan() {
        startActivityForResult(new Intent(this, ZbarScanActivity.class), new OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == -1) {
                    String scanResult = data.getStringExtra("scanResult");
                    showWaitDialog();
                    scanOrgan(scanResult);
                }
            }
        });
    }

    //手机端使用华为扫码
    private void goHwScan() {
        startActivityForResult(HwScanActivity.class, new OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == RESULT_OK) {
                    String scanResult = data.getStringExtra("scanResult");
                    showWaitDialog();
                    scanOrgan(scanResult);
                }
            }
        });
    }

    //扫机构
    private void scanOrgan(String barCode) {
        ScanOrganModel.sendScanOrganRequest(TAG, barCode, new CustomerJsonCallBack<ScanOrganModel>() {
            @Override
            public void onRequestError(ScanOrganModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(ScanOrganModel returnData) {
                hideWaitDialog();
                String type = returnData.getType();
                if ("1".equals(type)) {//机构目录
                    ArrayList<CatalogueModel> data1 = returnData.getData1();
                    if (data1 != null && !data1.isEmpty()) {
                        if (data1.size() == 1) {
                            setSelectCataLayout(data1.get(0));
                        } else {
                            ScanCataListActivtiy.newIntance(OwnExpenseSettleActivity.this, "101", "2", data1, Constants.RequestCode.REQUEST_SCANCATALIST_CODE);
                        }
                    }
                } else if ("2".equals(type)) {//医保目录
                    List<WestCataListModel.DataBean> data2 = returnData.getData2();
                    if (data2 != null && !data2.isEmpty()) {
                        WestCataListModel.DataBean dataBean = data2.get(0);
                        UpdateWestCataActivity.newIntance(OwnExpenseSettleActivity.this, dataBean.getListType(), dataBean);
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RequestCode.GET_SETLECT_DISE_CODE && resultCode == RESULT_OK) {//选择诊断信息回调
            DiseCataModel.DataBean selectDiseCatas = data.getParcelableExtra("selectDatas");
            if (selectDiseCatas != null) {
                setSelectDiseCataLayout(selectDiseCatas);
            }
        } else if (requestCode == Constants.RequestCode.SELECT_CATAENALIST_CODE && resultCode == RESULT_OK) {//选择药品信息回调
            CatalogueModel catalogueModels = data.getParcelableExtra("catalogueModels");
            if (catalogueModels != null) {
                setSelectCataLayout(catalogueModels);
            }
        } else if (requestCode == Constants.RequestCode.GET_SETLECT_CHRONIC_DISE_CODE && resultCode == RESULT_OK) {//慢性病选择病种
            QueryPersonalInfoModel.PsnOpspRegBean feedetailBean = data.getParcelableExtra("selectData");
            if (feedetailBean != null) {
                mTvSetlectDisespec.setTag(feedetailBean.getOpsp_dise_code());
                mTvSetlectDisespec.setText(EmptyUtils.strEmpty(feedetailBean.getOpsp_dise_name()));
            }
        } else if (requestCode == Constants.RequestCode.GET_SETLECT_CHRONIC_DISE_CODE2 && resultCode == RESULT_OK) {//普通疾病选择病种
            DiseCataModel.DataBean selectDiseCatas = data.getParcelableExtra("selectDatas");
            if (selectDiseCatas != null) {
                mTvSetlectDisespec.setTag(selectDiseCatas.getCode());
                mTvSetlectDisespec.setText(EmptyUtils.strEmpty(selectDiseCatas.getName()));
            }
        } else if (requestCode == Constants.RequestCode.REQUEST_SCANCATALIST_CODE && resultCode == RESULT_OK) {//扫码选药
            CatalogueModel catalogueModel = data.getParcelableExtra("catalogueModels");
            if (catalogueModel != null) {
                setSelectCataLayout(catalogueModel);
            }
        } else if (requestCode == Constants.RequestCode.SELECT_WESTPRESCR_CODE && resultCode == RESULT_OK) {//选择西药中成药处方
            WestPrescrCataModel westPrescrCataModel = data.getParcelableExtra("westPrescrCataModel");
            if (westPrescrCataModel != null) {
                setWestPrescrCataLayout(westPrescrCataModel);
            }else {
                mWestPrescrCata = null;
                mLlWestprescrDrugsContain.removeAllViews();
                mLlWestprescrDrugsContain.setVisibility(View.GONE);
                mSllHejiPrice.setVisibility(View.GONE);
                mTvHejiPrice.setText("¥0");
                setTotalPrice();
                bpttomVisibily();
            }
        } else if (requestCode == Constants.RequestCode.SELECT_CHINAPRESCR_CODE && resultCode == RESULT_OK) {//选择中药处方
            ChinaPrescrCataModel chinaPrescrCataModel = data.getParcelableExtra("chinaPrescrCataModel");
            if (chinaPrescrCataModel != null) {
                setChinaPrescrCataLayout(chinaPrescrCataModel);
            }else {
                mChinaPrescrCata = null;
                mLlChinaprescrDrugsContain.removeAllViews();
                mLlChinaprescrDrugsContain.setVisibility(View.GONE);
                mLlChinaprescrDrugsInfo.setVisibility(View.GONE);
                mSllChinaprescrHejiPrice.setVisibility(View.GONE);
                mTvChinaprescrHejiPrice.setText("¥0");
                setTotalPrice();
                bpttomVisibily();
            }
        }
    }

    //--------------------------------------诊断信息-----------------------------------------------------------
    //设置选择疾病目录
    private void setSelectDiseCataLayout(DiseCataModel.DataBean selectDiseCata) {
        if (rlDiseAdapter.getData().isEmpty()) {
            ArrayList<DiseCataModel.DataBean> selectDiseDatas = new ArrayList<>();
            selectDiseDatas.add(selectDiseCata);
            rlDiseAdapter.setNewData(selectDiseDatas);
        } else {
            boolean isSame = false;
            for (DiseCataModel.DataBean resultObjBean : rlDiseAdapter.getData()) {
                if (resultObjBean.getCode().equals(selectDiseCata.getCode())) {
                    //选择的疾病已经在列表中，那么就需要直接该疾病去除
                    isSame = true;
                    break;
                }
            }
            if (!isSame) {
                rlDiseAdapter.addData(selectDiseCata);
            }
        }
    }
    //--------------------------------------诊断信息-----------------------------------------------------------

    //--------------------------------------药品信息-----------------------------------------------------------
    //设置选中的药品列表
    private void setSelectCataLayout(CatalogueModel catalogueModels) {
        boolean isSame = false;
        int pos = 0;
        if (!catalogueListDatas.isEmpty()) {
            CatalogueModel data;
            for (int i = 0, size = catalogueListDatas.size(); i < size; i++) {
                data = catalogueListDatas.get(i);
                if (data.getFixmedins_hilist_id().equals(catalogueModels.getFixmedins_hilist_id())) {
                    //选择的药品已经在列表中，那么就需要直接该药品数量+1
                    data.setNum(data.getNum() + 1);
                    isSame = true;
                    pos = i;
                    break;
                }
            }
        }

        if (isSame) {
            updateDragView(pos, catalogueListDatas.get(pos));
        } else {
            catalogueListDatas.add(catalogueModels);
            addDragView(catalogueModels);
            updateAllLineVisibility();
        }

        setTotalPrice();
        bpttomVisibily();
    }

    //增加药品项
    protected void addDragView(CatalogueModel cataData) {
        View view = LayoutInflater.from(OwnExpenseSettleActivity.this).inflate(R.layout.layout_outpatselect_cata_item, mLlDrugsContain, false);
        View line = view.findViewById(R.id.line);
        TextView tvRegNam = view.findViewById(R.id.tv_reg_nam);
        FrameLayout flDelete = view.findViewById(R.id.fl_delete);
        FrameLayout flReduce = view.findViewById(R.id.fl_reduce);
        ShapeTextView etNum = view.findViewById(R.id.et_num);
        FrameLayout flAdd = view.findViewById(R.id.fl_add);
        ShapeTextView tvUnitPrice = view.findViewById(R.id.tv_unit_price);
        TextView tvSingleTotalPrice = view.findViewById(R.id.tv_single_total_price);
        ShapeEditText etRemarks = view.findViewById(R.id.et_remarks);

        String spec = "";
        if (!EmptyUtils.isEmpty(cataData.getDrug_type_name())) {
            spec = "(" + cataData.getDrug_type_name() + ")";
        } else if (!EmptyUtils.isEmpty(cataData.getDosforom())) {
            spec = "(" + cataData.getDosforom() + ")";
        } else if (!EmptyUtils.isEmpty(cataData.getSpec())) {
            spec = "(" + cataData.getSpec() + ")";
        }

        tvRegNam.setText("药品名称:  " + cataData.getFixmedins_hilist_name() + spec);
        tvUnitPrice.setText(EmptyUtils.strEmpty(cataData.getPrice()));
        etNum.setText(String.valueOf(cataData.getNum()));
        setSingTotalPrice(tvSingleTotalPrice, cataData.getPrice(), etNum.getText().toString());
        etRemarks.setText(EmptyUtils.strEmpty(cataData.getRemark()));
        //删除选择
        flDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!EmptyUtils.isEmpty(chrg_bchno)) {
                    showShortToast("请先撤销门诊费用明细，再选择药品");
                    return;
                }
                mLlDrugsContain.removeView(view);
                catalogueListDatas.remove(cataData);

                setTotalPrice();
                updateAllLineVisibility();
                bpttomVisibily();
            }
        });
        //监听数量修改
        etNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }

                if (!EmptyUtils.isEmpty(chrg_bchno)) {
                    showShortToast("请先撤销门诊费用明细，再选择药品");
                    return;
                }

                if (updateNumPop == null) {
                    updateNumPop = new UpdateNumPop(OwnExpenseSettleActivity.this);
                }
                updateNumPop.setOnListener(new UpdateNumPop.IOptionListener() {
                    @Override
                    public void onAgree(String data) {
                        int num = new BigDecimal(data).intValue();
                        if (num < 1) {
                            showShortToast("请输入正确的数量");
                            return;
                        }
                        etNum.setText(String.valueOf(num));
                        cataData.setNum(num);
                        setSingTotalPrice(tvSingleTotalPrice, cataData.getPrice(), String.valueOf(num));
                        setTotalPrice();
                    }

                    @Override
                    public void onCancle() {
                    }
                });
                updateNumPop.setInputType(InputType.TYPE_CLASS_NUMBER);
                updateNumPop.setContent(String.valueOf(cataData.getNum()));
                updateNumPop.setOutSideDismiss(false).showPopupWindow();
                KeyboardUtils.showSoftInput(OwnExpenseSettleActivity.this);
            }
        });
        //数量减
        flReduce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!EmptyUtils.isEmpty(chrg_bchno)) {
                    showShortToast("请先撤销门诊费用明细，再选择药品");
                    return;
                }
                showWaitDialog();
                if (EmptyUtils.isEmpty(etNum.getText().toString())) {
                    etNum.setText("1");
                } else if (new BigDecimal(etNum.getText().toString()).intValue() > 1) {
                    etNum.setText(new BigDecimal(etNum.getText().toString()).subtract(new BigDecimal("1")).toPlainString());
                } else {
                    showShortToast("数量最低值为1");
                    hideWaitDialog();
                    return;
                }

                cataData.setNum(EmptyUtils.isEmpty(etNum.getText().toString()) ? 1 : Integer.valueOf(etNum.getText().toString()));
                setSingTotalPrice(tvSingleTotalPrice, cataData.getPrice(), etNum.getText().toString());
                setTotalPrice();
                hideWaitDialog();
            }
        });
        //数量加
        flAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                if (!EmptyUtils.isEmpty(chrg_bchno)) {
                    showShortToast("请先撤销门诊费用明细，再选择药品");
                    return;
                }
                if (EmptyUtils.isEmpty(etNum.getText().toString())) {
                    etNum.setText("1");
                } else {
                    etNum.setText(new BigDecimal(etNum.getText().toString()).add(new BigDecimal("1")).toPlainString());
                }

                cataData.setNum(EmptyUtils.isEmpty(etNum.getText().toString()) ? 1 : Integer.valueOf(etNum.getText().toString()));
                setSingTotalPrice(tvSingleTotalPrice, cataData.getPrice(), etNum.getText().toString());
                setTotalPrice();
                hideWaitDialog();
            }
        });
        //单价修改按钮点击事件
        tvUnitPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!EmptyUtils.isEmpty(chrg_bchno)) {
                    showShortToast("请先撤销门诊费用明细，再选择药品");
                    return;
                }

                if (!RxTool.isPrice(tvUnitPrice.getText().toString())) {
                    showShortToast("请输入正确的金额");
                    return;
                }

                if (updateNumPop == null) {
                    updateNumPop = new UpdateNumPop(OwnExpenseSettleActivity.this);
                }
                updateNumPop.setOnListener(new UpdateNumPop.IOptionListener() {
                    @Override
                    public void onAgree(String data) {
                        if (!RxTool.isPrice(data)) {
                            showShortToast("请输入正确的金额");
                            return;
                        }

                        showWaitDialog();
                        editContents(cataData, data, tvUnitPrice, tvSingleTotalPrice);
                    }

                    @Override
                    public void onCancle() {
                    }
                });
                updateNumPop.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
                updateNumPop.setContent(EmptyUtils.strEmpty(cataData.getPrice()));
                updateNumPop.setOutSideDismiss(false).showPopupWindow();
                KeyboardUtils.showSoftInput(OwnExpenseSettleActivity.this);
            }
        });

        mLlDrugsContain.addView(view);
    }

    //修改目录价格（不上传省平台）
    private void editContents(CatalogueModel cataData, String Price, TextView tvUnitPrice, TextView tvSingleTotalPrice) {
        BaseModel.sendContentsEditRequest(TAG, cataData.getList_type(), cataData.getFixmedins_hilist_id(), cataData.getFixmedins_hilist_name(),
                cataData.getMed_list_codg(), Price, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        cataData.setPrice(Price);
                        tvUnitPrice.setText(Price);
                        setSingTotalPrice(tvSingleTotalPrice, cataData.getPrice(), String.valueOf(cataData.getNum()));
                        setTotalPrice();
                        showShortToast("修改成功");
                    }
                });
    }

    //更新药品项
    private void updateDragView(int pos, CatalogueModel cataData) {
        View view = mLlDrugsContain.getChildAt(pos);
        ShapeTextView etNum = view.findViewById(R.id.et_num);
        TextView tvSingleTotalPrice = view.findViewById(R.id.tv_single_total_price);

        etNum.setText(String.valueOf(cataData.getNum()));
        setSingTotalPrice(tvSingleTotalPrice, EmptyUtils.strEmpty(cataData.getPrice()), etNum.getText().toString());
    }

    //更新线条，移除第一个item的线条
    protected void updateAllLineVisibility() {
        if (mLlDrugsContain.getChildCount() > 0) {
            View view = mLlDrugsContain.getChildAt(0);
            View line = view.findViewById(R.id.line);
            line.setVisibility(View.GONE);
        }
    }

    //设置单个药品的总价
    private void setSingTotalPrice(TextView tv, String price, String num) {
        if (tv != null) {
            String singleTotalPrice = new BigDecimal(price).multiply(new BigDecimal(num)).toPlainString();
            tv.setText("¥" + singleTotalPrice);
        }
    }

    //--------------------------------------药品信息-----------------------------------------------------------
    //----------------------------------处方药品信息------------------------------------------------------
    //设置西药中成药处方药品数据
    private void setWestPrescrCataLayout(WestPrescrCataModel westPrescrCataModel) {
        mWestPrescrCata = westPrescrCataModel;
        mLlWestprescrDrugsContain.removeAllViews();
        ArrayList<WestPrescrCataModel.DataBean> westPrescrCatas = westPrescrCataModel.getData();
        if (westPrescrCatas != null && !westPrescrCatas.isEmpty()) {
            mLlWestprescrDrugsContain.setVisibility(View.VISIBLE);
            for (int i = 0; i < westPrescrCatas.size(); i++) {
                WestPrescrCataModel.DataBean westPrescrCata = westPrescrCatas.get(i);

                View view = LayoutInflater.from(OwnExpenseSettleActivity.this).inflate(R.layout.layout_selectcata_westprescr_item, mLlWestprescrDrugsContain, false);
                View line = view.findViewById(R.id.line);
                TextView tvRegNam = view.findViewById(R.id.tv_reg_nam);
                TextView tvSpec = view.findViewById(R.id.tv_spec);
                TextView tvConsump = view.findViewById(R.id.tv_consump);
                TextView tvFrequency = view.findViewById(R.id.tv_frequency);
                TextView tvNum = view.findViewById(R.id.tv_num);
                TextView tvUsage = view.findViewById(R.id.tv_usage);
                TextView tvRemarks = view.findViewById(R.id.tv_remarks);
                TextView tvUnitPrice = view.findViewById(R.id.tv_unit_price);
                TextView tvSingleTotalPrice = view.findViewById(R.id.tv_single_total_price);

                if (i == 0) {
                    line.setVisibility(View.GONE);
                } else {
                    line.setVisibility(View.VISIBLE);
                }
                tvRegNam.setText("药品名称:  " + westPrescrCata.getCatalogueModel().getFixmedins_hilist_name());
                tvSpec.setText(westPrescrCata.getCatalogueModel().getDrug_type_name());
                tvConsump.setText(westPrescrCata.getConsump() + westPrescrCata.getConsumpCompany());
                tvFrequency.setText(EmptyUtils.strEmpty(westPrescrCata.getFrequency()));
                tvNum.setText(westPrescrCata.getNum() + westPrescrCata.getNumCompany());
                tvUsage.setText(EmptyUtils.strEmpty(westPrescrCata.getUsage()));
                tvRemarks.setText(EmptyUtils.strEmpty(westPrescrCata.getRemarks()));
                tvUnitPrice.setText("¥" + westPrescrCata.getCatalogueModel().getPrice());
                String singleTotalPrice = new BigDecimal(westPrescrCata.getCatalogueModel().getPrice()).multiply(new BigDecimal(String.valueOf(westPrescrCata.getNum()))).toPlainString();
                tvSingleTotalPrice.setText("¥" + singleTotalPrice);

                mLlWestprescrDrugsContain.addView(view);
            }

            mSllHejiPrice.setVisibility(View.VISIBLE);
            mTvHejiPrice.setText("¥" + westPrescrCataModel.getHejiPrice());
            setTotalPrice();
            bpttomVisibily();
        }
    }

    //设置中药处方药品数据
    private void setChinaPrescrCataLayout(ChinaPrescrCataModel chinaPrescrCataModel) {
        mChinaPrescrCata = chinaPrescrCataModel;
        mLlChinaprescrDrugsContain.removeAllViews();
        ArrayList<ChinaPrescrCataModel.DataBean> chinaPrescrCatas = chinaPrescrCataModel.getData();
        if (chinaPrescrCatas != null && !chinaPrescrCatas.isEmpty()) {
            mLlChinaprescrDrugsContain.setVisibility(View.VISIBLE);
            mLlChinaprescrDrugsInfo.setVisibility(View.VISIBLE);
            for (int i = 0; i < chinaPrescrCatas.size(); i++) {
                ChinaPrescrCataModel.DataBean westPrescrCata = chinaPrescrCatas.get(i);

                View view = LayoutInflater.from(OwnExpenseSettleActivity.this).inflate(R.layout.layout_selectcata_chinaprescr_item, mLlChinaprescrDrugsContain, false);
                View line = view.findViewById(R.id.line);
                TextView tvRegNam = view.findViewById(R.id.tv_reg_nam);
                TextView tvWeight = view.findViewById(R.id.tv_weight);
                TextView tvUnitPrice = view.findViewById(R.id.tv_unit_price);
                TextView tvSingleTotalPrice = view.findViewById(R.id.tv_single_total_price);

                if (i == 0) {
                    line.setVisibility(View.GONE);
                } else {
                    line.setVisibility(View.VISIBLE);
                }
                tvRegNam.setText("药品名称:  " + westPrescrCata.getCatalogueModel().getFixmedins_hilist_name());
                tvWeight.setText(EmptyUtils.strEmpty(westPrescrCata.getWeight() + westPrescrCata.getWeightCompany()));
                tvUnitPrice.setText("¥" + westPrescrCata.getCatalogueModel().getPrice());
                String singleTotalPrice = new BigDecimal(westPrescrCata.getCatalogueModel().getPrice()).multiply(new BigDecimal(String.valueOf(westPrescrCata.getWeight()))).toPlainString();
                tvSingleTotalPrice.setText("¥" + singleTotalPrice);

                mLlChinaprescrDrugsContain.addView(view);
            }

            mTvChinaprescrDrugsNum.setText("药品数量:  " + chinaPrescrCataModel.getNum() + chinaPrescrCataModel.getNumCompany());
            mTvChinaprescrDrugsRemarks.setText("备" + getResources().getString(R.string.spaces) + getResources().getString(R.string.spaces) + "注:  " + chinaPrescrCataModel.getRemarks());

            mSllChinaprescrHejiPrice.setVisibility(View.VISIBLE);
            mTvChinaprescrHejiPrice.setText("¥" + chinaPrescrCataModel.getHejiPrice());
            setTotalPrice();
            bpttomVisibily();
        }
    }
    //----------------------------------处方药品信息-------------------------------------------------------

    //底部控件是否显示
    protected void bpttomVisibily() {
        if (mWestPrescrCata == null && mChinaPrescrCata == null && catalogueListDatas.isEmpty()) {
            mTvEmptyText.setVisibility(View.VISIBLE);
            mSllOutpatFeelistUp.setVisibility(View.GONE);
        } else {
            mTvEmptyText.setVisibility(View.GONE);
            mSllOutpatFeelistUp.setVisibility(View.VISIBLE);
        }

        if (mWestPrescrCata == null) {
            mLineWestprescrDrugs.setVisibility(View.GONE);
        } else {
            mLineWestprescrDrugs.setVisibility(View.VISIBLE);
        }

        if (mChinaPrescrCata == null) {
            mLineChinaprescrDrugs.setVisibility(View.GONE);
        } else {
            mLineChinaprescrDrugs.setVisibility(View.VISIBLE);
        }
    }

    //计算全部药品的价格
    protected void setTotalPrice() {
        totalPrice = "0";
        for (CatalogueModel catalogueListData : catalogueListDatas) {
            String singleTotalPrice = new BigDecimal(catalogueListData.getPrice()).multiply(new BigDecimal(catalogueListData.getNum())).toPlainString();
            totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(singleTotalPrice)).toPlainString();
        }

        if (mWestPrescrCata != null) {
            totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(mWestPrescrCata.getHejiPrice())).toPlainString();
        }

        if (mChinaPrescrCata != null) {
            totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(mChinaPrescrCata.getHejiPrice())).toPlainString();
        }
        mTvTotalPrice.setText("总计: ¥" + totalPrice);
    }

    @Override
    protected void onDestroy() {
        if (cPayID != null) {
            cPayID.stopDetect();
        }
        super.onDestroy();
        if (mLlSettleUpa != null) {
            mLlSettleUpa.removeAllViews();
            mLlSettleUpa = null;
        }
        if (rlDiseAdapter != null) {
            rlDiseAdapter.getData().clear();
            rlDiseAdapter.setOnItemClickListener(null);
            rlDiseAdapter.setOnItemChildClickListener(null);
            rlDiseAdapter.setOnLoadMoreListener(null, null);
            rlDiseAdapter = null;
        }

        if (mLlSettleFeelist != null) {
            mLlSettleFeelist.removeAllViews();
            mLlSettleFeelist = null;
        }
        if (mLlDrugsContain != null) {
            mLlDrugsContain.removeAllViews();
            mLlDrugsContain = null;
        }
        if (updateNumPop != null) {
            updateNumPop.onCleanListener();
            updateNumPop.onDestroy();
        }

        if (dropDownDataPop != null) {
            dropDownDataPop.onCleanListener();
            dropDownDataPop.onDestroy();
        }

        if (timeWheelUtils != null) {
            timeWheelUtils.dissTimeWheel();
            timeWheelUtils = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, OwnExpenseSettleActivity.class);
        context.startActivity(intent);
    }
}
