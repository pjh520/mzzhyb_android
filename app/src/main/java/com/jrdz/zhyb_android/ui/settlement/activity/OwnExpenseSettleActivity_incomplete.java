package com.jrdz.zhyb_android.ui.settlement.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.database.CatalogueModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.model.ChinaPrescrCataModel;
import com.jrdz.zhyb_android.ui.catalogue.model.WestPrescrCataModel;
import com.jrdz.zhyb_android.ui.settlement.model.QueryPersonalInfoModel;
import com.jrdz.zhyb_android.ui.settlement.model.UnsettledOutpatientInfoModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-09-01
 * 描    述：自费结算-步骤未完成时 跳转的页面
 * ================================================
 */
public class OwnExpenseSettleActivity_incomplete extends OwnExpenseSettleActivity{
    private String psnName,mdtrtCertNo,gend,age,address,phone;
    private int step;

    @Override
    public void initData() {
        ipt_otp_no= getIntent().getStringExtra("ipt_otp_no");
        psnName=getIntent().getStringExtra("psnName");
        mdtrtCertNo=getIntent().getStringExtra("mdtrtCertNo");
        gend=getIntent().getStringExtra("gend");
        age=getIntent().getStringExtra("age");
        address= getIntent().getStringExtra("address");
        phone= getIntent().getStringExtra("phone");
        step= getIntent().getIntExtra("step", 1);
        super.initData();

        mTvPsnName.setText(EmptyUtils.strEmpty(psnName));
        mTvMdtrtCertNo.setText(EmptyUtils.strEmpty(mdtrtCertNo));
        mTvGend.setTag(gend);
        mTvGend.setText("1".equals(gend) ? "男" : "女");
        mTvAge.setText(EmptyUtils.strEmpty(age));
        mEtAddress.setText(EmptyUtils.strEmpty(address));
        mEtPhone.setText(phone);

        if (step>=3){
            showWaitDialog();
            getUnsettledOutpatientInfo();
        }

        mTitleBar.post(new Runnable() {
            @Override
            public void run() {
                setTvColor02(step);
            }
        });
    }

    //获取未结算门诊信息
    private void getUnsettledOutpatientInfo() {
        UnsettledOutpatientInfoModel.sendUnsettledOutpatientInfoRequest_user(TAG, ipt_otp_no, new CustomerJsonCallBack<UnsettledOutpatientInfoModel>() {
            @Override
            public void onRequestError(UnsettledOutpatientInfoModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(UnsettledOutpatientInfoModel returnData) {
                hideWaitDialog();
                if (returnData!=null){
                    //设置处方头部信息
                    List<UnsettledOutpatientInfoModel.DataBean> data = returnData.getData();
                    if (data!=null){
                        for (UnsettledOutpatientInfoModel.DataBean datum : data) {
                            if ("1".equals(datum.getPrescriptionType())){//西药处方药
                                if (mWestPrescrCata==null){
                                    mWestPrescrCata=new WestPrescrCataModel();
                                }
                                mWestPrescrCata.setDiagResult(datum.getWmcpmDiagResults());
                                mWestPrescrCata.setHejiPrice(datum.getWmcpmAmount());
                                mWestPrescrCata.setPrescr_accessoryId(datum.getAccessoryId());
                                mWestPrescrCata.setPrescrNo(datum.getElectronicPrescriptionNo());
                            }
                            if ("2".equals(datum.getPrescriptionType())){//中药处方药
                                if (mChinaPrescrCata==null){
                                    mChinaPrescrCata=new ChinaPrescrCataModel();
                                }
                                mChinaPrescrCata.setDiagResult(datum.getCmDiagResults());
                                mChinaPrescrCata.setNum(datum.getCmDrugNum());
                                mChinaPrescrCata.setNumCompany(datum.getUnt());
                                mChinaPrescrCata.setUsage(datum.getCmused_mtd());
                                mChinaPrescrCata.setRemarks(datum.getCmRemark());
                                mChinaPrescrCata.setHejiPrice(datum.getCmAmount());
                                mChinaPrescrCata.setPrescr_accessoryId(datum.getAccessoryId());
                                mChinaPrescrCata.setPrescrNo(datum.getElectronicPrescriptionNo());
                            }
                        }
                    }
                    //设置药品信息
                    List<UnsettledOutpatientInfoModel.FeesdataBean> feesdata = returnData.getFeesdata();
                    if (feesdata!=null&&!feesdata.isEmpty()){
                        for (UnsettledOutpatientInfoModel.FeesdataBean feesdatum : feesdata) {
                            String feedetl_sn = feesdatum.getFeedetl_sn();
                            if (EmptyUtils.isEmpty(feedetl_sn)){
                                showShortToast("处方药信息有误，请重新进入页面");
                                return;
                            }
                            //100是非处方药  200是西药处方药  300是中药处方药
                            switch (feedetl_sn.substring(feedetl_sn.length()-3, feedetl_sn.length())){
                                case "100"://非处方药
                                    CatalogueModel catalogueModel=new CatalogueModel();
                                    catalogueModel.setMed_list_codg(feesdatum.getMed_list_codg());
                                    catalogueModel.setFixmedins_hilist_id(feesdatum.getMedins_list_codg());
                                    catalogueModel.setFixmedins_hilist_name(feesdatum.getMedins_list_name());
                                    try {
                                        catalogueModel.setNum(Integer.valueOf(feesdatum.getCnt()));
                                    }catch (Exception e){
                                        catalogueModel.setNum(1);
                                    }
                                    catalogueModel.setPrice(feesdatum.getPric());
                                    catalogueModel.setRemark(feesdatum.getRemark());

                                    catalogueListDatas.add(catalogueModel);
                                    addDragView(catalogueModel);
                                    break;
                                case "200"://西药处方药
                                    WestPrescrCataModel.DataBean westPrescrCataDataBean = new WestPrescrCataModel.DataBean();
                                    String sin_dos_dscr = feesdatum.getSin_dos_dscr();
                                    try {
                                        westPrescrCataDataBean.setConsump(Integer.valueOf(sin_dos_dscr.substring(2,sin_dos_dscr.length()-1)));
                                    }catch (Exception e){
                                        westPrescrCataDataBean.setConsump(1);
                                    }
                                    if (!EmptyUtils.isEmpty(sin_dos_dscr)){
                                        westPrescrCataDataBean.setConsumpCompany(sin_dos_dscr.substring(sin_dos_dscr.length()-1,sin_dos_dscr.length()));
                                    }else {
                                        westPrescrCataDataBean.setConsumpCompany("片");
                                    }

                                    westPrescrCataDataBean.setFrequency(feesdatum.getUsed_frqu_dscr());
                                    try {
                                        westPrescrCataDataBean.setNum(Integer.valueOf(feesdatum.getCnt()));
                                    }catch (Exception e){
                                        westPrescrCataDataBean.setNum(1);
                                    }

                                    westPrescrCataDataBean.setNumCompany(feesdatum.getUnt());
                                    westPrescrCataDataBean.setUsage(feesdatum.getUsed_mtd());
                                    westPrescrCataDataBean.setRemarks(feesdatum.getRemark());

                                    //药品信息
                                    CatalogueModel catalogueModel01=new CatalogueModel();
                                    catalogueModel01.setMed_list_codg(feesdatum.getMed_list_codg());
                                    catalogueModel01.setFixmedins_hilist_id(feesdatum.getMedins_list_codg());
                                    catalogueModel01.setFixmedins_hilist_name(feesdatum.getMedins_list_name());
                                    catalogueModel01.setPrice(feesdatum.getPric());
                                    catalogueModel01.setDrug_type_name(feesdatum.getSpec());
                                    westPrescrCataDataBean.setCatalogueModel(catalogueModel01);

                                    mWestPrescrCata.getData().add(westPrescrCataDataBean);
                                    break;
                                case "300"://中药处方药
                                    ChinaPrescrCataModel.DataBean ChinaPrescrCataDataBean=new ChinaPrescrCataModel.DataBean();
                                    try {
                                        ChinaPrescrCataDataBean.setWeight(Integer.valueOf(feesdatum.getCnt()));
                                    }catch (Exception e){
                                        ChinaPrescrCataDataBean.setWeight(1);
                                    }
                                    ChinaPrescrCataDataBean.setWeightCompany(feesdatum.getUnt());

                                    //药品信息
                                    CatalogueModel catalogueModel02=new CatalogueModel();
                                    catalogueModel02.setMed_list_codg(feesdatum.getMed_list_codg());
                                    catalogueModel02.setFixmedins_hilist_id(feesdatum.getMedins_list_codg());
                                    catalogueModel02.setFixmedins_hilist_name(feesdatum.getMedins_list_name());
                                    catalogueModel02.setPrice(feesdatum.getPric());
                                    ChinaPrescrCataDataBean.setCatalogueModel(catalogueModel02);

                                    mChinaPrescrCata.getData().add(ChinaPrescrCataDataBean);
                                    break;
                            }
                        }

                        UnsettledOutpatientInfoModel.FeesdataBean firstData = feesdata.get(0);
                        if (firstData!=null){
                            chrg_bchno=firstData.getChrg_bchno();
                            mEtCycleDays.setText(firstData.getPrd_days());

                            mTvFeedetlSn.setText(EmptyUtils.strEmpty(ipt_otp_no));
                            mTvChrgBchno.setText(EmptyUtils.strEmpty(chrg_bchno));
                        }
                    }

                    //设置页面数据
                    setWestPrescrCataLayout02(mWestPrescrCata);
                    setChinaPrescrCataLayout02(mChinaPrescrCata);

                    updateAllLineVisibility();
                    setTotalPrice();
                    bpttomVisibily();
                }
            }
        });
    }

    protected void setWestPrescrCataLayout02(WestPrescrCataModel westPrescrCataModel) {
        if (westPrescrCataModel==null)return;
        mLlWestprescrDrugsContain.removeAllViews();
        ArrayList<WestPrescrCataModel.DataBean> westPrescrCatas = westPrescrCataModel.getData();
        if (westPrescrCatas != null && !westPrescrCatas.isEmpty()) {
            mLlWestprescrDrugsContain.setVisibility(View.VISIBLE);
            for (int i = 0; i < westPrescrCatas.size(); i++) {
                WestPrescrCataModel.DataBean westPrescrCata = westPrescrCatas.get(i);

                View view = LayoutInflater.from(OwnExpenseSettleActivity_incomplete.this).inflate(R.layout.layout_selectcata_westprescr_item, mLlWestprescrDrugsContain, false);
                View line = view.findViewById(R.id.line);
                TextView tvRegNam = view.findViewById(R.id.tv_reg_nam);
                TextView tvSpec = view.findViewById(R.id.tv_spec);
                TextView tvConsump = view.findViewById(R.id.tv_consump);
                TextView tvFrequency = view.findViewById(R.id.tv_frequency);
                TextView tvNum = view.findViewById(R.id.tv_num);
                TextView tvUsage = view.findViewById(R.id.tv_usage);
                TextView tvRemarks = view.findViewById(R.id.tv_remarks);
                TextView tvUnitPrice = view.findViewById(R.id.tv_unit_price);
                TextView tvSingleTotalPrice = view.findViewById(R.id.tv_single_total_price);

                if (i == 0) {
                    line.setVisibility(View.GONE);
                } else {
                    line.setVisibility(View.VISIBLE);
                }
                tvRegNam.setText("药品名称:  " + westPrescrCata.getCatalogueModel().getFixmedins_hilist_name());
                tvSpec.setText(westPrescrCata.getCatalogueModel().getDrug_type_name());
                tvConsump.setText(westPrescrCata.getConsump() + westPrescrCata.getConsumpCompany());
                tvFrequency.setText(EmptyUtils.strEmpty(westPrescrCata.getFrequency()));
                tvNum.setText(westPrescrCata.getNum() + westPrescrCata.getNumCompany());
                tvUsage.setText(EmptyUtils.strEmpty(westPrescrCata.getUsage()));
                tvRemarks.setText(EmptyUtils.strEmpty(westPrescrCata.getRemarks()));
                tvUnitPrice.setText("¥" + westPrescrCata.getCatalogueModel().getPrice());
                String singleTotalPrice = new BigDecimal(westPrescrCata.getCatalogueModel().getPrice()).multiply(new BigDecimal(String.valueOf(westPrescrCata.getNum()))).toPlainString();
                tvSingleTotalPrice.setText("¥" + singleTotalPrice);

                mLlWestprescrDrugsContain.addView(view);
            }

            mSllHejiPrice.setVisibility(View.VISIBLE);
            mTvHejiPrice.setText("¥" + westPrescrCataModel.getHejiPrice());
        }
    }

    protected void setChinaPrescrCataLayout02(ChinaPrescrCataModel chinaPrescrCataModel) {
        if (chinaPrescrCataModel==null)return;
        mLlChinaprescrDrugsContain.removeAllViews();
        ArrayList<ChinaPrescrCataModel.DataBean> chinaPrescrCatas = chinaPrescrCataModel.getData();
        if (chinaPrescrCatas != null && !chinaPrescrCatas.isEmpty()) {
            mLlChinaprescrDrugsContain.setVisibility(View.VISIBLE);
            mLlChinaprescrDrugsInfo.setVisibility(View.VISIBLE);
            for (int i = 0; i < chinaPrescrCatas.size(); i++) {
                ChinaPrescrCataModel.DataBean westPrescrCata = chinaPrescrCatas.get(i);

                View view = LayoutInflater.from(OwnExpenseSettleActivity_incomplete.this).inflate(R.layout.layout_selectcata_chinaprescr_item, mLlChinaprescrDrugsContain, false);
                View line = view.findViewById(R.id.line);
                TextView tvRegNam = view.findViewById(R.id.tv_reg_nam);
                TextView tvWeight = view.findViewById(R.id.tv_weight);
                TextView tvUnitPrice = view.findViewById(R.id.tv_unit_price);
                TextView tvSingleTotalPrice = view.findViewById(R.id.tv_single_total_price);

                if (i == 0) {
                    line.setVisibility(View.GONE);
                } else {
                    line.setVisibility(View.VISIBLE);
                }
                tvRegNam.setText("药品名称:  " + westPrescrCata.getCatalogueModel().getFixmedins_hilist_name());
                tvWeight.setText(EmptyUtils.strEmpty(westPrescrCata.getWeight() + westPrescrCata.getWeightCompany()));
                tvUnitPrice.setText("¥" + westPrescrCata.getCatalogueModel().getPrice());
                String singleTotalPrice = new BigDecimal(westPrescrCata.getCatalogueModel().getPrice()).multiply(new BigDecimal(String.valueOf(westPrescrCata.getWeight()))).toPlainString();
                tvSingleTotalPrice.setText("¥" + singleTotalPrice);

                mLlChinaprescrDrugsContain.addView(view);
            }

            mTvChinaprescrDrugsNum.setText("药品数量:  " + chinaPrescrCataModel.getNum() + chinaPrescrCataModel.getNumCompany());
            mTvChinaprescrDrugsRemarks.setText("备" + getResources().getString(R.string.spaces) + getResources().getString(R.string.spaces) + "注:  " + chinaPrescrCataModel.getRemarks());

            mSllChinaprescrHejiPrice.setVisibility(View.VISIBLE);
            mTvChinaprescrHejiPrice.setText("¥" + chinaPrescrCataModel.getHejiPrice());
        }
    }

    protected void setTvColor02(int pos) {
        mIvOutpatReg.setVisibility(View.GONE);
        mIvOutpatUpA.setVisibility(View.GONE);
        mIvFeelistUp.setVisibility(View.GONE);
        mIvSave.setVisibility(View.GONE);

        switch (pos) {
            case 0:
                mImmersionBar.keyboardEnable(true).init();
//                mHscvTab.fling(0);
                mHscvTab.scrollTo(mLlOutpatReg.getLeft() - dp_30, 0);
                mIvOutpatReg.setVisibility(View.VISIBLE);
                break;
            case 1://移动到门诊就诊信息上传tab
                mImmersionBar.keyboardEnable(false).init();
//                mHscvTab.fling(0);
                mHscvTab.scrollTo(mLlOutpatUpA.getLeft() - dp_30, 0);
                mIvOutpatUpA.setVisibility(View.VISIBLE);
                break;
            case 2://移动到门诊就诊信息上传tab
//                mHscvTab.fling(0);
                mHscvTab.scrollTo(mLlFeelistUp.getLeft() - dp_30, 0);
                mIvFeelistUp.setVisibility(View.VISIBLE);
                break;
            case 3://移动到门诊就诊信息上传tab
//                mHscvTab.fling(0);
                mHscvTab.scrollTo(mLlSave.getLeft() - dp_30, 0);
                mIvSave.setVisibility(View.VISIBLE);
                break;
        }

        mTvOutpatUpA.setTextColor(normalColor);
        mTvFeelistUp.setTextColor(normalColor);
        mTvSave.setTextColor(normalColor);

        mTvTagOutpatUpA.setTextColor(normalColor);
        mTvTagFeelistUp.setTextColor(normalColor);
        mTvTagSave.setTextColor(normalColor);

        if (pos > 0) {
            mTvOutpatUpA.setTextColor(selectColor);
            mTvTagOutpatUpA.setTextColor(selectColor);
        }

        if (pos > 1) {
            mTvFeelistUp.setTextColor(selectColor);
            mTvTagFeelistUp.setTextColor(selectColor);
        }

        if (pos > 2) {
            mTvSave.setTextColor(selectColor);
            mTvTagSave.setTextColor(selectColor);
        }

        mVp.setCurrentItem(pos, false);
    }

    @Override
    protected void upASuccess() {
        setResult(RESULT_OK);
    }

    @Override
    protected void outpatFeelistUpSuccess() {
        setResult(RESULT_OK);
    }

    @Override
    protected void feelistCommitSuccess() {
        setResult(RESULT_OK);
    }

    @Override
    protected void feelistCancleSuccess() {
        setResult(RESULT_OK);
    }

    @Override
    protected void saveSettleSuccess() {
        setResult(RESULT_OK);
    }

    public static void newIntance(Activity activity, String ipt_otp_no, String psnName, String mdtrtCertNo, String gend,
                                  String age, String address, String phone, int step, int requestCode) {
        Intent intent = new Intent(activity, OwnExpenseSettleActivity_incomplete.class);
        intent.putExtra("ipt_otp_no", ipt_otp_no);
        intent.putExtra("psnName", psnName);
        intent.putExtra("mdtrtCertNo", mdtrtCertNo);
        intent.putExtra("gend", gend);
        intent.putExtra("age", age);
        intent.putExtra("address", address);
        intent.putExtra("phone", phone);
        intent.putExtra("step", step);
        activity.startActivityForResult(intent,requestCode);
    }
}
