package com.jrdz.zhyb_android.ui.settlement.activity;

import android.content.Context;
import android.content.Intent;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;

import com.alibaba.fastjson.JSON;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.frame.compiler.widget.swipeMenuLayout.SwipeMenuLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.database.CatalogueModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.activity.ChronicDiseListActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.SelectCataManageActivity;
import com.jrdz.zhyb_android.ui.catalogue.activity.UpdateWestCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.model.WestCataListModel;
import com.jrdz.zhyb_android.ui.home.activity.ScanCataListActivtiy;
import com.jrdz.zhyb_android.ui.home.activity.TreatmentActivity;
import com.jrdz.zhyb_android.ui.home.model.ScanOrganModel;
import com.jrdz.zhyb_android.ui.mine.activity.HwScanActivity;
import com.jrdz.zhyb_android.ui.mine.activity.ZbarScanActivity;
import com.jrdz.zhyb_android.ui.settlement.model.PharmacySettleModel;
import com.jrdz.zhyb_android.ui.settlement.model.PharmacySettleParmaModel;
import com.jrdz.zhyb_android.ui.settlement.model.QueryPersonalInfoModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.InputPwdUtils;
import com.jrdz.zhyb_android.widget.pop.UpdateNumPop;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/14
 * 描    述： 药店结算
 * ================================================
 */
public class PharmacySettlementActivity extends BaseActivity implements InputPwdUtils.InputPwdListener {
    private TextView mTvPsnName;
    private TextView mTvGend;
    private TextView mTvNaty;
    private TextView mTvBrdy;
    private TextView mTvAge;
    private TextView mTvPsnNo;
    private TextView mTvPsnCertType;
    private TextView mTvMdtrtCertNo;
    private TextView mTvBalc;
    private TextView mTvPsnType;
    private TextView mTvInsutype;
    private TextView mTvCvlservFlag;
    private TextView mTvPsnInsuStas;
    private TextView mTvInsuplcAdmdvs;
    private TextView mTvPsnInsuDate;
    private TextView mTvEmpName;
    private TextView mTvSetlectDrugs,mTvScanSetlectDrugs;
    private TextView mTvMedType;
    private LinearLayout mLlSetlectDisespec;
    private TextView mTvSetlectDisespec;
    private EditText mEtCycleDays;
    private TextView mTvEmptyText;
    private LinearLayout mLlDrugsContain;
    private TextView mTvTotalPrice;
    private ShapeTextView mTvPreSettle, mTvSettle;

    private QueryPersonalInfoModel.DataBean.BaseinfoBean baseinfoBean;
    private QueryPersonalInfoModel.DataBean.InsuinfoBean insuinfoBean;
    private String mdtrt_cert_type, medType, medTypeName;
    private ArrayList<QueryPersonalInfoModel.PsnOpspRegBean> feedetailBeanList;

    private WheelUtils wheelUtils;
    private ArrayList<CatalogueModel> catalogueListDatas = new ArrayList<>();//存储选中的药品目录
    private String totalPrice = "0";
    private InputPwdUtils inputPwdUtils;
    private UpdateNumPop updateNumPop;

    @Override
    public int getLayoutId() {
        return R.layout.activity_pharmacy_settlement;
    }

    @Override
    public void initView() {
        super.initView();
        mTvPsnName = findViewById(R.id.tv_psn_name);
        mTvGend = findViewById(R.id.tv_gend);
        mTvNaty = findViewById(R.id.tv_naty);
        mTvBrdy = findViewById(R.id.tv_brdy);
        mTvAge = findViewById(R.id.tv_age);
        mTvPsnNo = findViewById(R.id.tv_psn_no);
        mTvPsnCertType = findViewById(R.id.tv_psn_cert_type);
        mTvMdtrtCertNo = findViewById(R.id.tv_mdtrt_cert_no);

        mTvBalc = findViewById(R.id.tv_balc);
        mTvPsnType = findViewById(R.id.tv_psn_type);
        mTvInsutype = findViewById(R.id.tv_insutype);
        mTvCvlservFlag = findViewById(R.id.tv_cvlserv_flag);
        mTvPsnInsuStas = findViewById(R.id.tv_psn_insu_stas);
        mTvInsuplcAdmdvs = findViewById(R.id.tv_insuplc_admdvs);
        mTvPsnInsuDate = findViewById(R.id.tv_psn_insu_date);
        mTvEmpName = findViewById(R.id.tv_emp_name);
        //医疗类别
        mTvMedType = findViewById(R.id.tv_med_type);
        //慢性病病种
        mLlSetlectDisespec = findViewById(R.id.ll_setlect_disespec);
        mTvSetlectDisespec = findViewById(R.id.tv_setlect_disespec);
        //用药周期天数
        mEtCycleDays = findViewById(R.id.et_cycle_days);
        //药品列表空view
        mTvEmptyText = findViewById(R.id.tv_empty_text);
        //药品目录
        mTvSetlectDrugs = findViewById(R.id.tv_setlect_drugs);
        mTvScanSetlectDrugs = findViewById(R.id.tv_scan_setlect_drugs);
        mLlDrugsContain = findViewById(R.id.ll_drugs_contain);
        //计算总价格
        mTvTotalPrice = findViewById(R.id.tv_total_price);
        //结算
        mTvPreSettle = findViewById(R.id.tv_pre_settle);
        mTvSettle = findViewById(R.id.tv_settle);
    }

    @Override
    public void initData() {
        Intent intent = getIntent();
        baseinfoBean = intent.getParcelableExtra("baseinfoBean");
        insuinfoBean = intent.getParcelableExtra("insuinfoBean");
        mdtrt_cert_type = intent.getStringExtra("mdtrt_cert_type");
        medType = intent.getStringExtra("medType");
        medTypeName = intent.getStringExtra("medTypeName");
        feedetailBeanList = intent.getParcelableArrayListExtra("feedetailBeanList");
        super.initData();
        setRightTitleView("人员待遇查询");
        //初始化密码输入框
        inputPwdUtils=new InputPwdUtils();
        //获取性别
        String gendText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("GEND", baseinfoBean.getGend());
        //获取民族
        String natyText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("NATY", baseinfoBean.getNaty());
        //获取证件类型
        String psnCertTypeText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("PSN_CERT_TYPE", baseinfoBean.getPsn_cert_type());
        //获取人员类别
        String psnTypeText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("PSN_TYPE", insuinfoBean.getPsn_type());
        //获取险种类型
        String insutypeText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("INSUTYPE", insuinfoBean.getInsutype());
        //获取是否公务员
        String cvlservFlagText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("CVLSERV_FLAG", insuinfoBean.getCvlserv_flag());
        //获取参保状态
        String psnInsuStasText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("PSN_INSU_STAS", insuinfoBean.getPsn_insu_stas());
        //获取参保就医区划
        String insuplcAdmdvsText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("AREA_ADMVS", insuinfoBean.getInsuplc_admdvs());

        //基本信息
        mTvPsnName.setText(EmptyUtils.strEmpty(baseinfoBean.getPsn_name()));
        mTvGend.setText(gendText);
        mTvNaty.setText(natyText);
        mTvBrdy.setText(EmptyUtils.strEmpty(baseinfoBean.getBrdy()));
        mTvAge.setText(EmptyUtils.strEmpty(baseinfoBean.getAge()));
        mTvPsnNo.setText(EmptyUtils.strEmpty(baseinfoBean.getPsn_no()));
        mTvPsnCertType.setText(psnCertTypeText);
        mTvMdtrtCertNo.setText(EmptyUtils.strEmpty(baseinfoBean.getCertno()));
        //参保信息
        mTvBalc.setText(EmptyUtils.strEmpty(insuinfoBean.getBalc()));
        mTvPsnType.setText(psnTypeText);
        mTvInsutype.setText(insutypeText);
        mTvCvlservFlag.setText(cvlservFlagText);
        mTvPsnInsuStas.setText(psnInsuStasText);
        mTvInsuplcAdmdvs.setText(insuplcAdmdvsText);
        mTvPsnInsuDate.setText(EmptyUtils.strEmpty(insuinfoBean.getPsn_insu_date()));
        mTvEmpName.setText(EmptyUtils.strEmpty(insuinfoBean.getEmp_name()));
        //医疗类别
        mTvMedType.setText(EmptyUtils.strEmpty(medTypeName));
        //判断是否是慢性病
        if ("9929".equals(medType)) {
            mLlSetlectDisespec.setVisibility(View.VISIBLE);
            mTvSetlectDisespec.setVisibility(View.VISIBLE);

            //慢病人员 如果查询到的慢病只有1个时候 直接默认展示 多个的话 就进入页面选择。
            if (feedetailBeanList!=null&&!feedetailBeanList.isEmpty()&&feedetailBeanList.size()==1){
                mLlSetlectDisespec.setEnabled(false);
                mTvSetlectDisespec.setTag(feedetailBeanList.get(0).getOpsp_dise_code());
                mTvSetlectDisespec.setText(EmptyUtils.strEmpty(feedetailBeanList.get(0).getOpsp_dise_name()));
            }
        } else {
            mLlSetlectDisespec.setVisibility(View.GONE);
            mTvSetlectDisespec.setVisibility(View.GONE);
        }

        wheelUtils = new WheelUtils();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mLlSetlectDisespec.setOnClickListener(this);
        mTvSetlectDrugs.setOnClickListener(this);
        mTvScanSetlectDrugs.setOnClickListener(this);
        mTvPreSettle.setOnClickListener(this);
        mTvSettle.setOnClickListener(this);
    }

    @Override
    public void rightTitleViewClick() {
        TreatmentActivity.newIntance(PharmacySettlementActivity.this, baseinfoBean.getPsn_no(), insuinfoBean.getInsutype(), medType, insuinfoBean.getInsuplc_admdvs());
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.ll_setlect_disespec://选择病种
                ChronicDiseListActivity.newIntance(PharmacySettlementActivity.this, feedetailBeanList, Constants.RequestCode.GET_SETLECT_CHRONIC_DISE_CODE);
                break;
            case R.id.tv_setlect_drugs://选择药品
                SelectCataManageActivity.newIntance(PharmacySettlementActivity.this, "2", Constants.RequestCode.SELECT_CATAENALIST_CODE);
                break;
            case R.id.tv_scan_setlect_drugs://扫码选择药品
                if (Constants.Configure.IS_POS){
                    goPosScan();
                }else {
                    goHwScan();
                }
                break;
            case R.id.tv_pre_settle://预结算
                preSettle();
                break;
            case R.id.tv_settle://结算
                if ("9929".equals(medType) && mTvSetlectDisespec.getTag() == null) {
                    showShortToast("请选择病种");
                    return;
                }
                if (catalogueListDatas == null || catalogueListDatas.isEmpty()) {
                    showShortToast("请选择药品");
                    return;
                }
                inputPwdUtils.showPwdDialog(PharmacySettlementActivity.this,PharmacySettlementActivity.this);
                break;
        }
    }

    //使用pos机自带的扫码
    private void goPosScan() {
        startActivityForResult(new Intent(this, ZbarScanActivity.class),new BaseActivity.OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == -1) {
                    String scanResult = data.getStringExtra("scanResult");
                    showWaitDialog();
                    scanOrgan(scanResult);
                }
            }
        });
    }

    //手机端使用华为扫码
    private void goHwScan() {
        startActivityForResult(HwScanActivity.class, new BaseActivity.OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == RESULT_OK) {
                    String scanResult = data.getStringExtra("scanResult");
                    showWaitDialog();
                    scanOrgan(scanResult);
                }
            }
        });
    }

    //扫机构
    private void scanOrgan(String barCode) {
        ScanOrganModel.sendScanOrganRequest(TAG, barCode, new CustomerJsonCallBack<ScanOrganModel>() {
            @Override
            public void onRequestError(ScanOrganModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(ScanOrganModel returnData) {
                hideWaitDialog();
                String type = returnData.getType();
                if ("1".equals(type)){//机构目录
                    ArrayList<CatalogueModel> data1 = returnData.getData1();
                    if (data1!=null&&!data1.isEmpty()){
                        if (data1.size()==1){
                            setSelectCataLayout(data1.get(0));
                        }else {
                            ScanCataListActivtiy.newIntance(PharmacySettlementActivity.this,"101","2",data1,Constants.RequestCode.REQUEST_SCANCATALIST_CODE);
                        }
                    }
                }else if ("2".equals(type)){//医保目录
                    List<WestCataListModel.DataBean> data2 = returnData.getData2();
                    if (data2!=null&&!data2.isEmpty()){
                        WestCataListModel.DataBean dataBean=data2.get(0);
                        UpdateWestCataActivity.newIntance(PharmacySettlementActivity.this,dataBean.getListType(),dataBean);
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RequestCode.SELECT_CATAENALIST_CODE && resultCode == RESULT_OK) {
            CatalogueModel catalogueModel = data.getParcelableExtra("catalogueModels");
            if (catalogueModel != null) {
                setSelectCataLayout(catalogueModel);
            }
        } else if (requestCode == Constants.RequestCode.GET_SETLECT_CHRONIC_DISE_CODE && resultCode == RESULT_OK) {
            QueryPersonalInfoModel.PsnOpspRegBean feedetailBean = data.getParcelableExtra("selectData");
            if (feedetailBean != null) {
                mTvSetlectDisespec.setTag(feedetailBean.getOpsp_dise_code());
                mTvSetlectDisespec.setText(EmptyUtils.strEmpty(feedetailBean.getOpsp_dise_name()));
            }
        }else if (requestCode == Constants.RequestCode.REQUEST_SCANCATALIST_CODE && resultCode == RESULT_OK){
            CatalogueModel catalogueModel = data.getParcelableExtra("catalogueModels");
            if (catalogueModel != null) {
                setSelectCataLayout(catalogueModel);
            }
        }
    }

    //设置选中的药品列表
    private void setSelectCataLayout(CatalogueModel catalogueModel) {
        boolean isSame = false;
        int pos = 0;
        if (!catalogueListDatas.isEmpty()) {
            CatalogueModel data;
            for (int i = 0, size = catalogueListDatas.size(); i < size; i++) {
                data = catalogueListDatas.get(i);
                if (data.getFixmedins_hilist_id().equals(catalogueModel.getFixmedins_hilist_id())) {
                    //选择的药品已经在列表中，那么就需要直接该药品数量+1
                    data.setNum(data.getNum() + 1);
                    isSame = true;
                    pos = i;
                    break;
                }
            }
        }

        if (isSame) {
            updateDragView(pos, catalogueListDatas.get(pos));
        } else {
            catalogueListDatas.add(catalogueModel);
            addDragView(catalogueModel);
            updateAllLineVisibility();
        }

        setTotalPrice();
        bpttomVisibily();
    }

    //增加药品项
    private void addDragView(CatalogueModel cataData) {
        View view = LayoutInflater.from(PharmacySettlementActivity.this).inflate(R.layout.layout_select_cata_item, mLlDrugsContain, false);
        SwipeMenuLayout swipeDelete = view.findViewById(R.id.swipe_delete);
        TextView tvDelete = view.findViewById(R.id.tv_delete);
        TextView tvMedListCodg = view.findViewById(R.id.tv_med_list_codg);
        TextView tvRegNam = view.findViewById(R.id.tv_reg_nam);
        EditText tvUnitPrice = view.findViewById(R.id.tv_unit_price);
        ShapeTextView tvUnitPriceUpdate = view.findViewById(R.id.tv_unit_price_update);
        FrameLayout flReduce = view.findViewById(R.id.fl_reduce);
        ShapeTextView etNum = view.findViewById(R.id.et_num);
        FrameLayout flAdd = view.findViewById(R.id.fl_add);
        TextView tvSingleTotalPrice = view.findViewById(R.id.tv_single_total_price);
        View line = view.findViewById(R.id.line);

        tvMedListCodg.setText("药品编码:  " + cataData.getFixmedins_hilist_id());
        tvRegNam.setText("药品名称:  " + cataData.getFixmedins_hilist_name());
        tvUnitPrice.setText(EmptyUtils.strEmpty(cataData.getPrice()));
        etNum.setText(String.valueOf(cataData.getNum()));
        setSingTotalPrice(tvSingleTotalPrice, cataData.getPrice(), etNum.getText().toString());

        //监听数量修改
        etNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }

                if (updateNumPop==null){
                    updateNumPop=new UpdateNumPop(PharmacySettlementActivity.this);
                }
                updateNumPop.setOnListener(new UpdateNumPop.IOptionListener() {
                    @Override
                    public void onAgree(String data) {
                        int num = new BigDecimal(data).intValue();
                        if (num< 1) {
                            showShortToast("请输入正确的数量");
                            return;
                        }
                        etNum.setText(String.valueOf(num));
                        cataData.setNum(num);
                        setSingTotalPrice(tvSingleTotalPrice, cataData.getPrice(), String.valueOf(num));
                        setTotalPrice();
                    }

                    @Override
                    public void onCancle() {}
                });
                updateNumPop.setInputType(InputType.TYPE_CLASS_NUMBER);
                updateNumPop.setContent(String.valueOf(cataData.getNum()));
                updateNumPop.setOutSideDismiss(false).showPopupWindow();
                KeyboardUtils.showSoftInput(PharmacySettlementActivity.this);
            }
        });
        //数量减
        flReduce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWaitDialog();
                if (EmptyUtils.isEmpty(etNum.getText().toString())) {
                    etNum.setText("1");
                } else if (new BigDecimal(etNum.getText().toString()).intValue() > 1) {
                    etNum.setText(new BigDecimal(etNum.getText().toString()).subtract(new BigDecimal("1")).toPlainString());
                } else {
                    showShortToast("数量最低值为1");
                    hideWaitDialog();
                    return;
                }

                cataData.setNum(EmptyUtils.isEmpty(etNum.getText().toString()) ? 1 : Integer.valueOf(etNum.getText().toString()));
                setSingTotalPrice(tvSingleTotalPrice, cataData.getPrice(), etNum.getText().toString());
                setTotalPrice();
                hideWaitDialog();
            }
        });
        //数量加
        flAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                if (EmptyUtils.isEmpty(etNum.getText().toString())) {
                    etNum.setText("1");
                } else {
                    etNum.setText(new BigDecimal(etNum.getText().toString()).add(new BigDecimal("1")).toPlainString());
                }

                cataData.setNum(EmptyUtils.isEmpty(etNum.getText().toString()) ? 1 : Integer.valueOf(etNum.getText().toString()));
                setSingTotalPrice(tvSingleTotalPrice, cataData.getPrice(), etNum.getText().toString());
                setTotalPrice();
                hideWaitDialog();
            }
        });
        //删除选择
        tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLlDrugsContain.removeView(view);
                catalogueListDatas.remove(cataData);
                swipeDelete.quickClose();
                setTotalPrice();
                updateAllLineVisibility();
                bpttomVisibily();
            }
        });
        //监听单价输入框焦点变化
        tvUnitPrice.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && EmptyUtils.isEmpty(tvUnitPrice.getText().toString())) {//没有焦点时 判断输入框是否由内容 没有的话 直接使用原有的数值 有则不变
                    tvUnitPrice.setText(EmptyUtils.strEmpty(cataData.getPrice()));
                }
            }
        });
        //单价修改按钮点击事件
        tvUnitPriceUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!RxTool.isPrice(tvUnitPrice.getText().toString())) {
                    showShortToast("请输入正确的金额");
                    return;
                }

                showWaitDialog();
                editContents(cataData, tvUnitPrice.getText().toString(),tvSingleTotalPrice);
            }
        });
        mLlDrugsContain.addView(view);
    }

    //修改目录价格（不上传省平台）
    private void editContents(CatalogueModel cataData, String Price,TextView tvSingleTotalPrice) {
        BaseModel.sendContentsEditRequest(TAG, cataData.getList_type(), cataData.getFixmedins_hilist_id(), cataData.getFixmedins_hilist_name(),
                cataData.getMed_list_codg(), Price, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        cataData.setPrice(Price);

                        setSingTotalPrice(tvSingleTotalPrice, cataData.getPrice(), String.valueOf(cataData.getNum()));
                        setTotalPrice();
                        showShortToast("修改成功");
                    }
                });
    }

    //更新药品项
    private void updateDragView(int pos, CatalogueModel cataData) {
        View view = mLlDrugsContain.getChildAt(pos);
        EditText etNum = view.findViewById(R.id.et_num);
        TextView tvSingleTotalPrice = view.findViewById(R.id.tv_single_total_price);

        etNum.setText(String.valueOf(cataData.getNum()));
        setSingTotalPrice(tvSingleTotalPrice, cataData.getPrice(), etNum.getText().toString());
    }

    //更新线条，移除第一个item的线条
    private void updateAllLineVisibility() {
        if (mLlDrugsContain.getChildCount() > 0) {
            View view = mLlDrugsContain.getChildAt(0);
            View line = view.findViewById(R.id.line);
            line.setVisibility(View.GONE);
        }
    }

    //设置单个药品的总价
    private void setSingTotalPrice(TextView tv, String price, String num) {
        String singleTotalPrice = new BigDecimal(price).multiply(new BigDecimal(num)).toPlainString();
        tv.setText("¥" + singleTotalPrice);
    }

    //底部控件是否显示
    private void bpttomVisibily() {
        if (catalogueListDatas.isEmpty()) {
            mTvEmptyText.setVisibility(View.VISIBLE);
        } else {
            mTvEmptyText.setVisibility(View.GONE);
        }
    }

    //计算全部药品的价格
    private void setTotalPrice() {
        totalPrice = "0";
        for (CatalogueModel catalogueListData : catalogueListDatas) {
            String singleTotalPrice = new BigDecimal(catalogueListData.getPrice()).multiply(new BigDecimal(catalogueListData.getNum())).toPlainString();
            totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(singleTotalPrice)).toPlainString();
        }

        mTvTotalPrice.setText("¥" + totalPrice);
    }

    //预结算
    private void preSettle() {
        if ("9929".equals(medType) && mTvSetlectDisespec.getTag() == null) {
            showShortToast("请选择病种");
            return;
        }
        if (catalogueListDatas == null || catalogueListDatas.isEmpty()) {
            showShortToast("请选择药品");
            return;
        }

        PharmacySettleParmaModel pharmacySettleParmaModel = new PharmacySettleParmaModel(insuinfoBean.getInsuplc_admdvs(), baseinfoBean.getPsn_no(),
                mdtrt_cert_type, baseinfoBean.getCertno(), baseinfoBean.getCertno(), totalPrice, insuinfoBean.getInsutype(),
                medType, "9929".equals(medType) ? mTvSetlectDisespec.getTag().toString() : "", "9929".equals(medType) ? mTvSetlectDisespec.getText().toString() : "",
                baseinfoBean.getPsn_name(),baseinfoBean.getPsn_name(),"");

        for (CatalogueModel catalogueListData : catalogueListDatas) {
            String singleTotalPrice = new BigDecimal(catalogueListData.getPrice()).multiply(new BigDecimal(catalogueListData.getNum())).toPlainString();
            PharmacySettleParmaModel.DrugdetailBean drugdetailBean = new PharmacySettleParmaModel.DrugdetailBean(catalogueListData.getFixmedins_hilist_id(),
                    String.valueOf(catalogueListData.getNum()), "0", singleTotalPrice,EmptyUtils.strEmpty(mEtCycleDays.getText().toString()));

            pharmacySettleParmaModel.getDrugdetail().add(drugdetailBean);
        }
        PrePharmacySettleActivity.newIntance(PharmacySettlementActivity.this, JSON.toJSONString(pharmacySettleParmaModel), insuinfoBean.getInsuplc_admdvs());
    }

    @Override
    public void onComplete(String pwd) {
        settle(pwd);
    }

    //结算
    private void settle(String pwd) {
        PharmacySettleParmaModel pharmacySettleParmaModel = new PharmacySettleParmaModel(insuinfoBean.getInsuplc_admdvs(), baseinfoBean.getPsn_no(),
                mdtrt_cert_type, baseinfoBean.getCertno(), baseinfoBean.getCertno(), totalPrice, insuinfoBean.getInsutype(),
                medType, "9929".equals(medType) ? mTvSetlectDisespec.getTag().toString() : "", "9929".equals(medType) ? mTvSetlectDisespec.getText().toString() : "",
                baseinfoBean.getPsn_name(),baseinfoBean.getPsn_name(),pwd);

        for (CatalogueModel catalogueListData : catalogueListDatas) {
            String singleTotalPrice = new BigDecimal(catalogueListData.getPrice()).multiply(new BigDecimal(catalogueListData.getNum())).toPlainString();
            PharmacySettleParmaModel.DrugdetailBean drugdetailBean = new PharmacySettleParmaModel.DrugdetailBean(catalogueListData.getFixmedins_hilist_id(),
                    String.valueOf(catalogueListData.getNum()), "0", singleTotalPrice,EmptyUtils.strEmpty(mEtCycleDays.getText().toString()));

            pharmacySettleParmaModel.getDrugdetail().add(drugdetailBean);
        }

        showWaitDialog();
        PharmacySettleModel.sendPharmacySettleRequest(TAG, JSON.toJSONString(pharmacySettleParmaModel), insuinfoBean.getInsuplc_admdvs(), new CustomerJsonCallBack<PharmacySettleModel>() {
            @Override
            public void onRequestError(PharmacySettleModel returnData, String msg) {
                hideWaitDialog();
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(PharmacySettleModel returnData) {
                hideWaitDialog();
                PharmacySettleModel.DataBean obj = returnData.getData();
                Constants.AppStorage.QUERY_PERSONAL_CERTNO="";
                if (obj != null && obj.getSetlinfo() != null) {
                    PharmacySettleModel.DataBean.SetlinfoBean setlinfo = obj.getSetlinfo();
                    SmallTicketActivity.newIntance(PharmacySettlementActivity.this, setlinfo.getSetl_id(), "1");
                    goFinish();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (inputPwdUtils!=null){
            inputPwdUtils.dismissDialog();
        }

        if (wheelUtils != null) {
            wheelUtils.dissWheel();
        }

        if (mLlDrugsContain != null) {
            mLlDrugsContain.removeAllViews();
            mLlDrugsContain = null;
        }

        if (updateNumPop!=null){
            updateNumPop.onCleanListener();
            updateNumPop.onDestroy();
        }
    }

    public static void newIntance(Context context, QueryPersonalInfoModel.DataBean.BaseinfoBean baseinfoBean,
                                  QueryPersonalInfoModel.DataBean.InsuinfoBean insuinfoBean, String mdtrt_cert_type,
                                  String medType, String medTypeName, ArrayList<QueryPersonalInfoModel.PsnOpspRegBean> feedetailBeanList) {
        Intent intent = new Intent(context, PharmacySettlementActivity.class);
        intent.putExtra("baseinfoBean", baseinfoBean);
        intent.putExtra("insuinfoBean", insuinfoBean);
        intent.putExtra("mdtrt_cert_type", mdtrt_cert_type);
        intent.putExtra("medType", medType);
        intent.putExtra("medTypeName", medTypeName);
        intent.putParcelableArrayListExtra("feedetailBeanList", feedetailBeanList);
        context.startActivity(intent);
    }
}
