package com.jrdz.zhyb_android.ui.settlement.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.DateUtil;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ImageUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.layout.ShapeRecyclerView;
import com.hjq.shape.view.ShapeView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.database.UserInfoModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.model.ChinaPrescrCataModel;
import com.jrdz.zhyb_android.ui.catalogue.model.WestPrescrCataModel;
import com.jrdz.zhyb_android.ui.mine.model.FileUploadModel;
import com.jrdz.zhyb_android.ui.settlement.adapter.PreChinaPrescDragAdapter;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.utils.ScreenshotUtil;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Random;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-05-10
 * 描    述：预览中药处方
 * ================================================
 */
public class PreChinaPrescActivity extends BaseActivity {
    private NestedScrollView mSvContain;
    private TextView mTvPrescNum;
    private TextView mTvMdtrtId;
    private TextView mTvName;
    private TextView mTvSex;
    private TextView mTvAge;
    private TextView mTvDept;
    private TextView mTvDate;
    private TextView mTvAddress;
    private TextView mTvPhone,mTvDiagResult;
    private ShapeRecyclerView mSrlDragContain;
    private ImageView mIvPhysician;
    private TextView mTvRemarks,mTvPrice;
    private TextView mTvReviewPharm;
    private TextView mTvDispePharm;
    private TextView mTvNuclearPharm;

    private ChinaPrescrCataModel chinaPrescrCataModel;
    private UserInfoModel userInfo;
    private String from;

    @Override
    public int getLayoutId() {
        return R.layout.activity_pre_chinapresc;
    }

    @Override
    public void initView() {
        super.initView();
        mSvContain = findViewById(R.id.sv_contain);
        mTvPrescNum = findViewById(R.id.tv_presc_num);
        mTvMdtrtId = findViewById(R.id.tv_mdtrt_id);
        mTvName = findViewById(R.id.tv_name);
        mTvSex = findViewById(R.id.tv_sex);
        mTvAge = findViewById(R.id.tv_age);
        mTvDept = findViewById(R.id.tv_dept);
        mTvDate = findViewById(R.id.tv_date);
        mTvAddress = findViewById(R.id.tv_address);
        mTvPhone = findViewById(R.id.tv_phone);
        mTvDiagResult= findViewById(R.id.tv_diagResult);
        mSrlDragContain = findViewById(R.id.srl_drag_contain);
        mTvRemarks= findViewById(R.id.tv_remarks);
        mIvPhysician = findViewById(R.id.iv_physician);
        mTvPrice = findViewById(R.id.tv_price);
        mTvReviewPharm = findViewById(R.id.tv_review_pharm);
        mTvDispePharm = findViewById(R.id.tv_dispe_pharm);
        mTvNuclearPharm = findViewById(R.id.tv_nuclear_pharm);
    }

    @Override
    public void initData() {
        chinaPrescrCataModel = getIntent().getParcelableExtra("chinaPrescrCataModel");
        Bundle baseInfo = getIntent().getBundleExtra("baseInfo");
        from= getIntent().getStringExtra("from");
        if (chinaPrescrCataModel == null) {
            showShortToast("处方数据有误");
            finish();
            return;
        }
        super.initData();
        userInfo = LoginUtils.getUserinfo();

        setTitle(MechanismInfoUtils.getFixmedinsName()+"电子处方笺");
        if ("2".equals(from)){
            setRightTitleView("上传");
        }

        String prescNum=DateUtil.getStringDate("yyyyMMddHHmmss")+((int)(Math.random()*9000)+1000);
        chinaPrescrCataModel.setPrescrNo(prescNum);
        mTvPrescNum.setText(prescNum);
        mTvMdtrtId.setText(baseInfo.getString("ipt_otp_no", ""));
        mTvName.setText(baseInfo.getString("name", ""));
        mTvSex.setText(baseInfo.getString("sex", ""));
        mTvAge.setText(baseInfo.getString("age", ""));
        mTvDept.setText(EmptyUtils.strEmpty(LoginUtils.getDeptIdName()));
        mTvDate.setText(DateUtil.getStringDate("yyyy-MM-dd HH:mm:ss"));
        mTvAddress.setText(baseInfo.getString("address", ""));
        mTvPhone.setText(baseInfo.getString("phone", ""));
        mTvDiagResult.setText(EmptyUtils.strEmpty(chinaPrescrCataModel.getDiagResult()));
        mTvPrice.setText(EmptyUtils.strEmpty(chinaPrescrCataModel.getHejiPrice()));
        mTvRemarks.setText("配"+chinaPrescrCataModel.getNum()+chinaPrescrCataModel.getNumCompany()+" "
                +chinaPrescrCataModel.getUsage()+" "+chinaPrescrCataModel.getRemarks());
        GlideUtils.getImageWidHeig(PreChinaPrescActivity.this, userInfo.getAccessoryUrl1(), new GlideUtils.IGetImageData() {
            @Override
            public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mIvPhysician.getLayoutParams();
                if (radio>=2.875){
                    layoutParams.width= (int) (getResources().getDimension(R.dimen.dp_230));
                    layoutParams.height= new BigDecimal((int) (getResources().getDimension(R.dimen.dp_230))).divide(new BigDecimal(radio),1,BigDecimal.ROUND_HALF_UP).intValue();
                }else {
                    layoutParams.width= (int) (getResources().getDimension(R.dimen.dp_80)*radio);
                    layoutParams.height= (int) (getResources().getDimension(R.dimen.dp_80));
                }

                mIvPhysician.setLayoutParams(layoutParams);
                mIvPhysician.setImageBitmap(resource);
            }
        });

        setDragList(chinaPrescrCataModel.getData());
    }

    @Override
    public void initEvent() {
        super.initEvent();
    }

    //设置药品数据
    private void setDragList(ArrayList<ChinaPrescrCataModel.DataBean> datas) {
        if (datas == null || datas.isEmpty()) {
            showShortToast("药品数据有误");
            return;
        }

        mSrlDragContain.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL,false));
        PreChinaPrescDragAdapter preChinaPrescDragAdapter=new PreChinaPrescDragAdapter();
        mSrlDragContain.setAdapter(preChinaPrescDragAdapter);

        preChinaPrescDragAdapter.setNewData(datas);
    }

    @Override
    public void rightTitleViewClick() {
        showWaitDialog();
        changeTitleStytle("1");
        mTitleBar.post(new Runnable() {
            @Override
            public void run() {
                String imagPath = BaseGlobal.getPicShotDir() + "prescr.jpg";
                ImageUtils.save(ScreenshotUtil.compressImage(ScreenshotUtil.ScreenShot(mTitleBar,mSvContain)), imagPath, Bitmap.CompressFormat.JPEG);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideWaitDialog();
                        changeTitleStytle("2");
                        FileUploadModel.sendFileUploadRequest(TAG, CompressUploadSinglePicUtils.PIC_PRESCR_TAG, new File(imagPath), new CustomerJsonCallBack<FileUploadModel>() {
                            @Override
                            public void onRequestError(FileUploadModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(FileUploadModel returnData) {
                                hideWaitDialog();
                                FileUploadModel.DataBean dataBean = returnData.getData();
                                if (dataBean!=null){
                                    showShortToast("上传成功");

                                    chinaPrescrCataModel.setPrescr_accessoryId(dataBean.getAccessoryId());
                                    chinaPrescrCataModel.setPrescr_accessoryUrl(dataBean.getAccessoryUrl());

                                    Intent intent = new Intent();
                                    intent.putExtra("chinaPrescrCataModel", chinaPrescrCataModel);
                                    setResult(RESULT_OK,intent);
                                    goFinish();
                                }else {
                                    showShortToast("图片上传失败，请重新上传");
                                }
                            }
                        });
                    }
                });
            }
        });
    }

    //改变标题栏的样式 type  1.代表截图模式 2.代表普通模式
    private void changeTitleStytle(String type){
        if ("1".equals(type)){
            mTitleBar.getLeftView().setVisibility(View.GONE);
            mTitleBar.getRightView().setVisibility(View.GONE);
            mTitleBar.getTitleView().setSingleLine(false);
            mTitleBar.getTitleView().setGravity(Gravity.CENTER);
        }else {
            mTitleBar.getLeftView().setVisibility(View.VISIBLE);
            mTitleBar.getRightView().setVisibility(View.VISIBLE);
            mTitleBar.getTitleView().setSingleLine(true);
            mTitleBar.getTitleView().setGravity(Gravity.CENTER_VERTICAL);
        }
    }

    public static void newIntance(Activity activity, ChinaPrescrCataModel chinaPrescrCataModel, Bundle baseInfo, String from, int requestCode) {
        Intent intent = new Intent(activity, PreChinaPrescActivity.class);
        intent.putExtra("chinaPrescrCataModel", chinaPrescrCataModel);
        intent.putExtra("baseInfo", baseInfo);
        intent.putExtra("from", from);
        activity.startActivityForResult(intent,requestCode);
    }
}
