package com.jrdz.zhyb_android.ui.settlement.activity;

import android.content.Context;
import android.content.Intent;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.settlement.model.PreSettleModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/29
 * 描    述：药店预结算页面
 * ================================================
 */
public class PrePharmacySettleActivity extends BaseActivity {
    private TextView mTvMedfeeSumamt;
    private TextView mTvFundPaySumamt;
    private TextView mTvAcctPay;
    private TextView mTvPsnCashPay;
    private TextView mTvContent;

    private String parma, insuplc_admdvs;

    @Override
    public int getLayoutId() {
        return R.layout.layout_pre_pharmacysettle;
    }

    @Override
    public void initView() {
        super.initView();

        mTvMedfeeSumamt = findViewById(R.id.tv_medfee_sumamt);
        mTvFundPaySumamt = findViewById(R.id.tv_fund_pay_sumamt);
        mTvAcctPay = findViewById(R.id.tv_acct_pay);
        mTvPsnCashPay = findViewById(R.id.tv_psn_cash_pay);
        mTvContent= findViewById(R.id.tv_content);
    }

    @Override
    public void initData() {
        parma = getIntent().getStringExtra("parma");
        insuplc_admdvs = getIntent().getStringExtra("insuplc_admdvs");
        super.initData();

        showWaitDialog();
        getPrePharmacySettleData();
    }

    //获取药店预结算数据
    private void getPrePharmacySettleData() {
        PreSettleModel.sendPrePharmacySettleRequest(TAG, parma, insuplc_admdvs, new CustomerJsonCallBack<PreSettleModel>() {
            @Override
            public void onRequestError(PreSettleModel returnData, String msg) {
                hideWaitDialog();
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(PreSettleModel returnData) {
                hideWaitDialog();
                PreSettleModel.DataBean obj = returnData.getData();
                if (obj != null) {
                    setSetlinfo(obj);
                }

            }
        });
    }

    //设置结算信息
    private void setSetlinfo(PreSettleModel.DataBean setlinfo) {
        mTvMedfeeSumamt.setText(EmptyUtils.strEmpty(setlinfo.getMedfee_sumamt()));
        mTvFundPaySumamt.setText(EmptyUtils.strEmpty(setlinfo.getFund_pay_sumamt()));
        mTvAcctPay.setText(EmptyUtils.strEmpty(setlinfo.getAcct_pay()));
        mTvPsnCashPay.setText(EmptyUtils.strEmpty(setlinfo.getPsn_cash_pay()));
        mTvContent.setText(EmptyUtils.strEmpty(setlinfo.getContent()));
    }

    public static void newIntance(Context context, String parma, String insuplc_admdvs) {
        Intent intent = new Intent(context, PrePharmacySettleActivity.class);
        intent.putExtra("parma", parma);
        intent.putExtra("insuplc_admdvs", insuplc_admdvs);
        context.startActivity(intent);
    }
}
