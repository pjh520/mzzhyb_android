package com.jrdz.zhyb_android.ui.settlement.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import com.frame.compiler.utils.viewPage.BaseViewPagerAndTabsAdapter_new;
import com.frame.compiler.widget.CustomViewPager;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.settlement.fragment.NetPrescrListFragment;
import com.jrdz.zhyb_android.ui.settlement.fragment.OutpatPrescrListFragment;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-01-10
 * 描    述：电子处方
 * ================================================
 */
public class PrescrActivity extends BaseActivity {
    private LinearLayout mRlTitle;
    private FrameLayout mFlClose;
    private SlidingTabLayout mStbTab;
    private CustomViewPager mVp;

    ArrayList<String> titles = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_elect_presc2;
    }

    @Override
    public void initView() {
        super.initView();

        mRlTitle = findViewById(R.id.rl_title);
        mFlClose = findViewById(R.id.fl_close);
        mStbTab = findViewById(R.id.stb_tab);
        mVp = findViewById(R.id.vp);
    }

    @Override
    protected void initImmersionBar() {
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mRlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        super.initData();

        initTabLayout();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mFlClose.setOnClickListener(this);
    }

    //初始化tablayout 跟 viewpager
    private void initTabLayout() {
        BaseViewPagerAndTabsAdapter_new baseViewPagerAndTabsAdapter_new = new BaseViewPagerAndTabsAdapter_new(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0://智慧医保处方
                        return OutpatPrescrListFragment.newIntance();
                    case 1://互联网药店处方
                        return NetPrescrListFragment.newIntance();
                    default:
                        return OutpatPrescrListFragment.newIntance();
                }
            }
        };

        titles.clear();
        titles.add("智慧医保处方");
        titles.add("互联网药店处方");

        baseViewPagerAndTabsAdapter_new.setData(titles);
        mVp.setAdapter(baseViewPagerAndTabsAdapter_new);
        mStbTab.setViewPager(mVp);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()) {
            case R.id.fl_close:
                goFinish();
                break;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, PrescrActivity.class);
        context.startActivity(intent);
    }
}
