package com.jrdz.zhyb_android.ui.settlement.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.baidu.ocr.sdk.OCR;
import com.baidu.ocr.sdk.OnResultListener;
import com.baidu.ocr.sdk.exception.OCRError;
import com.baidu.ocr.sdk.model.IDCardParams;
import com.baidu.ocr.sdk.model.IDCardResult;
import com.baidu.ocr.ui.camera.CameraActivity;
import com.baidu.ocr.ui.camera.CameraNativeHelper;
import com.baidu.ocr.ui.camera.CameraView;
import com.centerm.cpay.ai.lib.IdInfo;
import com.centerm.cpay.ai.lib.OnResultCallbackListener;
import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.FileUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.LogUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.frame.compiler.widget.permission.PermissionHelper;
import com.hjq.permissions.Permission;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.pos.CPayDevice;
import com.jrdz.zhyb_android.pos.CPayID;
import com.jrdz.zhyb_android.ui.home.activity.TreatmentActivity;
import com.jrdz.zhyb_android.ui.settlement.model.QueryPersonalInfoModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.io.File;
import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/14
 * 描    述： 查询人员信息
 * ================================================
 */
public class QueryPersonalInfoActivity extends BaseActivity {
    private ShapeLinearLayout mSllScanIdcard, mSllMedType;
    private EditText mEtIdcard;
    private TextView mTvScanType,mTvMedType;
    private ShapeTextView mTvQuery;

    private String from;
    private int choosePos1 = 0;
    private WheelUtils wheelUtils;
    private ArrayList<DataDicModel> medTypeData;
    private PermissionHelper permissionHelper;
    private String mdtrt_cert_type = "02";
    private CPayID cPayID;

    @Override
    public int getLayoutId() {
        return R.layout.activity_query_personal_info2;
    }

    @Override
    public void initView() {
        super.initView();
        mTvScanType= findViewById(R.id.tv_scan_type);
        mSllScanIdcard = findViewById(R.id.sll_scan_idcard);
        mEtIdcard = findViewById(R.id.et_idcard);
        mSllMedType = findViewById(R.id.sll_med_type);
        mTvMedType = findViewById(R.id.tv_med_type);

        mTvQuery = findViewById(R.id.tv_query);

        mEtIdcard.setText(Constants.AppStorage.QUERY_PERSONAL_CERTNO);

        // TODO: 2022/2/16 代码填充测试数据
//        mEtIdcard.setText("612724194212020656");//610802200402251221孙常源  612726196609210011 612724196401050626张占娥  612701192312294626  612724195201100647
    }

    @Override
    public void initData() {
        from = getIntent().getStringExtra("from");
        super.initData();
        //创建图片缓存地址
        FileUtils.fileDirExis(BaseGlobal.getPhotoCacheDir(), "idCard.jpg");

        if (Constants.Configure.IS_POS) {
            initNfcIdCard();
        } else {
            initScanIdCard();
        }

        if ("1".equals(from)){
            setTitle("医保结算");
        }else {
            setTitle("人员信息查询");
        }

        permissionHelper = new PermissionHelper();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mSllScanIdcard.setOnClickListener(this);
        mSllMedType.setOnClickListener(this);
        mTvQuery.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_1000)) {
            return;
        }
        switch (v.getId()) {
            case R.id.sll_scan_idcard://识别身份证
                if (Constants.Configure.IS_POS) {
                    nfcIdCard();
                } else {
                    scanIdCard();
                }
                break;
            case R.id.sll_med_type://选择医疗类别
                KeyboardUtils.hideSoftInput(QueryPersonalInfoActivity.this);
                if ("1".equals(from)) {//从门诊结算
                    medTypeData = CommonlyUsedDataUtils.getInstance().getOutpatMedTypeData();
                } else if ("2".equals(from)) {//药店结算
                    medTypeData = CommonlyUsedDataUtils.getInstance().getPharmacyMedTypeData();
                } else {//待遇检查
                    medTypeData = CommonlyUsedDataUtils.getInstance().getMedTypeData();
                }

                if (wheelUtils==null){
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(QueryPersonalInfoActivity.this, "选择医疗类型", choosePos1, medTypeData,
                        new WheelUtils.WheelClickListener<DataDicModel>() {
                            @Override
                            public void onchooseDate(int choosePos, DataDicModel dateInfo) {
                                choosePos1 = choosePos;
                                mTvMedType.setTag(dateInfo.getValue());
                                mTvMedType.setText(EmptyUtils.strEmpty(dateInfo.getLabel()));
                            }
                        });
                break;
            case R.id.tv_query:
                query();
                break;

        }
    }

    //查询个人信息
    private void query() {
        if (EmptyUtils.isEmpty(mEtIdcard.getText().toString())) {
            showShortToast("请输入身份证");
            return;
        }

        if (mEtIdcard.getText().length()!=15&&mEtIdcard.getText().length()!=18) {
            showShortToast("身份证号码为15位或者18位");
            return;
        }

        if (mTvMedType.getTag() == null || EmptyUtils.isEmpty(mTvMedType.getTag().toString())) {
            showShortToast("请选择医疗类别");
            return;
        }

        showWaitDialog();
        QueryPersonalInfoModel.sendQueryPersonalInfoRequest(TAG, mEtIdcard.getText().toString(), mdtrt_cert_type, mTvMedType.getTag().toString(),
                new CustomerJsonCallBack<QueryPersonalInfoModel>() {
                    @Override
                    public void onRequestError(QueryPersonalInfoModel returnData, String msg) {
                        hideWaitDialog();
                        showTipDialog(msg);
                    }

                    @Override
                    public void onRequestSuccess(QueryPersonalInfoModel returnData) {
                        hideWaitDialog();
                        QueryPersonalInfoModel.DataBean data = returnData.getData();
                        if (data != null && data.getBaseinfo() != null && data.getInsuinfo() != null && !data.getInsuinfo().isEmpty()) {
                            if ("1".equals(from)) {
                                OutpatSettlementActivity.newIntance(QueryPersonalInfoActivity.this, data.getBaseinfo(),
                                        data.getInsuinfo().get(0), mdtrt_cert_type, mTvMedType.getTag().toString(),
                                        mTvMedType.getText().toString(), returnData.getPsnOpspReg());
                            } else if ("2".equals(from)) {
                                PharmacySettlementActivity.newIntance(QueryPersonalInfoActivity.this, data.getBaseinfo(),
                                        data.getInsuinfo().get(0), mdtrt_cert_type, mTvMedType.getTag().toString(),
                                        mTvMedType.getText().toString(), returnData.getPsnOpspReg());
                            } else {
                                TreatmentActivity.newIntance(QueryPersonalInfoActivity.this, data.getBaseinfo().getPsn_no(),
                                        data.getInsuinfo().get(0).getInsutype(), mTvMedType.getTag().toString(), data.getInsuinfo().get(0).getInsuplc_admdvs());
                            }
                            Constants.AppStorage.QUERY_PERSONAL_CERTNO=mEtIdcard.getText().toString();
//                            goFinish();
                        } else {
                            showShortToast("数据有误，请重新查询");
                        }
                    }
                });
    }

    //在pos机上面跟华为统一扫码冲突
    //初始化扫描识别身份证
    private void initScanIdCard() {
        mTvScanType.setText("扫描证件正面可自动识别号码");
        //  初始化本地质量控制模型,释放代码在onDestory中
        //  调用身份证扫描必须加上 intent.putExtra(CameraActivity.KEY_NATIVE_MANUAL, true); 关闭自动初始化和释放本地模型
        CameraNativeHelper.init(this, OCR.getInstance(this).getLicense(), new CameraNativeHelper.CameraNativeInitCallback() {
            @Override
            public void onError(int errorCode, Throwable e) {
                String msg;
                switch (errorCode) {
                    case CameraView.NATIVE_SOLOAD_FAIL:
                        msg = "加载so失败，请确保apk中存在ui部分的so";
                        break;
                    case CameraView.NATIVE_AUTH_FAIL:
                        msg = "授权本地质量控制token获取失败";
                        break;
                    case CameraView.NATIVE_INIT_FAIL:
                        msg = "本地质量控制";
                        break;
                    default:
                        msg = String.valueOf(errorCode);
                }
                Log.e("baiduOcr", "本地质量控制初始化错误，错误原因：" + msg);
            }
        });
    }
    //在pos机上面跟华为统一扫码冲突
    //扫描识别身份证
    private void scanIdCard() {
        permissionHelper.requestPermission(this, new PermissionHelper.onPermissionListener() {
            @Override
            public void onSuccess() {
                // 身份证正面扫描
                Intent intent = new Intent(QueryPersonalInfoActivity.this, CameraActivity.class);
                intent.putExtra(CameraActivity.KEY_OUTPUT_FILE_PATH, BaseGlobal.getPhotoCacheDir() + "idCard.jpg");
                intent.putExtra(CameraActivity.KEY_NATIVE_ENABLE, true);
                // KEY_NATIVE_MANUAL设置了之后CameraActivity中不再自动初始化和释放模型
                // 请手动使用CameraNativeHelper初始化和释放模型
                // 推荐这样做，可以避免一些activity切换导致的不必要的异常
                intent.putExtra(CameraActivity.KEY_NATIVE_MANUAL, true);
                intent.putExtra(CameraActivity.KEY_CONTENT_TYPE, CameraActivity.CONTENT_TYPE_ID_CARD_FRONT);
                startActivityForResult(intent, Constants.RequestCode.REQUEST_IDCARD_CAMERA_CODE);
            }

            @Override
            public void onNoAllSuccess(String noAllSuccessText) {

            }

            @Override
            public void onFail(String noAllSuccessText) {
                showShortToast("权限申请失败，app将不能扫描身份证获取信息");
            }
        }, "相机权限:扫码时,需要该权限,不然不能调用摄像头扫码",Permission.CAMERA);
    }

    //初始化nfc扫描身份证
    private void initNfcIdCard() {
        cPayID = CPayDevice.getCPayID();
        mTvScanType.setText("请将二代身份证紧贴于NFC读卡区域");
    }

    //通过nfc扫描身份证
    private void nfcIdCard() {
        showWaitDialog("正在识别身份证…");
        if (cPayID==null){
            cPayID = CPayDevice.getCPayID();
        }

        cPayID.startDetect(new OnResultCallbackListener.Stub() {
            @Override
            public void onResult(int code, Bundle bundle) {
                // https://blog.csdn.net/Bettarwang/article/details/45315091?locationNum=4
                bundle.setClassLoader(getClass().getClassLoader());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (code == 15) {

                        } else if (code == 1) {
                            hideWaitDialog();
                            IdInfo idInfo = bundle.getParcelable("idInfo");
                            if (idInfo != null) {
                                LogUtils.e("^_^ 读取到的身份证id: " + idInfo.idnum);
                                showShortToast("身份证识别成功");
                                mEtIdcard.setText(EmptyUtils.strEmpty(idInfo.idnum));
                                RxTool.setEditTextCursorLocation(mEtIdcard);
                            }
                        } else {
                            hideWaitDialog();
                            showShortToast(String.format("读卡失败: %s", cPayID.errorInfo(code)));
                        }
                    }
                });
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //在pos机上面跟华为统一扫码冲突(已做兼容 pos扫码使用pos机自带的)
        if (requestCode == Constants.RequestCode.REQUEST_IDCARD_CAMERA_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String contentType = data.getStringExtra(CameraActivity.KEY_CONTENT_TYPE);
                String filePath = BaseGlobal.getPhotoCacheDir() + "idCard.jpg";
                if (!TextUtils.isEmpty(contentType)) {
                    showWaitDialog("身份信息识别中...");
                    if (CameraActivity.CONTENT_TYPE_ID_CARD_FRONT.equals(contentType)) {
                        recIDCard(IDCardParams.ID_CARD_SIDE_FRONT, filePath);
                    } else if (CameraActivity.CONTENT_TYPE_ID_CARD_BACK.equals(contentType)) {
                        recIDCard(IDCardParams.ID_CARD_SIDE_BACK, filePath);
                    }
                }
            }
        }
    }

    private void recIDCard(String idCardSide, String filePath) {
        IDCardParams param = new IDCardParams();
        param.setImageFile(new File(filePath));
        // 设置身份证正反面
        param.setIdCardSide(idCardSide);
        // 设置方向检测
        param.setDetectDirection(true);
        // 设置图像参数压缩质量0-100, 越大图像质量越好但是请求时间越长。 不设置则默认值为20
        param.setImageQuality(90);

        OCR.getInstance(this).recognizeIDCard(param, new OnResultListener<IDCardResult>() {
            @Override
            public void onResult(IDCardResult result) {
                hideWaitDialog();
                if (result != null) {
                    showShortToast("身份证识别成功");
                    mEtIdcard.setText(EmptyUtils.strEmpty(result.getIdNumber() == null ? "" : result.getIdNumber().toString()));
                    RxTool.setEditTextCursorLocation(mEtIdcard);
                }
            }

            @Override
            public void onError(OCRError error) {
                hideWaitDialog();
                showShortToast("身份证识别失败:" + error.getMessage());
            }
        });
    }

    @Override
    protected void onDestroy() {
        //在pos机上面跟华为统一扫码冲突(已做兼容 pos扫码使用pos机自带的)
        //释放本地质量控制模型
        CameraNativeHelper.release();
        if (cPayID != null) {
            cPayID.stopDetect();
        }
        super.onDestroy();

        if (wheelUtils != null) {
            wheelUtils.dissWheel();
        }

        if (medTypeData != null) {
            medTypeData.clear();
            medTypeData = null;
        }
    }

    //from 1:从门诊结算 2.药店结算 3.待遇检查
    public static void newIntance(Context context, String from) {
        Intent intent = new Intent(context, QueryPersonalInfoActivity.class);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }
}
