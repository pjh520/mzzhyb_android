package com.jrdz.zhyb_android.ui.settlement.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.LogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.pos.CPayDevice;
import com.jrdz.zhyb_android.pos.CPayPrint;
import com.jrdz.zhyb_android.ui.settlement.model.SmallTicketModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/23
 * 描    述： 结算记录详情
 * ================================================
 */
public class SettleRecordDetailActivtiy extends BaseActivity {
    private TextView mTvContent;
    private ImageView mIvContent;

    private String setlId;
    private String imgurl;
    private String content,isOrder;

    @Override
    public int getLayoutId() {
        return R.layout.activity_settle_record_detail;
    }

    @Override
    public void initView() {
        super.initView();

        mTvContent = findViewById(R.id.tv_content);
        mIvContent = findViewById(R.id.iv_content);
    }

    @Override
    public void initData() {
        setlId = getIntent().getStringExtra("setl_id");
        super.initData();

        setRightTitleView("打印");
        if (Constants.Configure.IS_POS){
            mTitleBar.getRightView().setVisibility(View.VISIBLE);
        }else {
            mTitleBar.getRightView().setVisibility(View.GONE);
        }

        showWaitDialog();
        getSmallTicketData();
    }

    private void getSmallTicketData(){
        SmallTicketModel.sendSmallTicketRequest(TAG, setlId,new CustomerJsonCallBack<SmallTicketModel>() {
            @Override
            public void onRequestError(SmallTicketModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(SmallTicketModel returnData) {
                hideWaitDialog();

                imgurl=returnData.getData().getImgurl();
                content=returnData.getData().getContent();
                isOrder=returnData.getData().getIsOrder();

                if ("0".equals(isOrder)){
                    if (!EmptyUtils.isEmpty(imgurl)){
                        mIvContent.setVisibility(View.VISIBLE);
                        GlideUtils.getImageWidHeig(SettleRecordDetailActivtiy.this, imgurl, new GlideUtils.IGetImageData() {
                            @Override
                            public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mIvContent.getLayoutParams();
                                layoutParams.height = (int) (getResources().getDimensionPixelSize(R.dimen.dp_690) / radio);
                                mIvContent.setLayoutParams(layoutParams);

                                mIvContent.setImageBitmap(resource);
                            }
                        });
                    }else if (!EmptyUtils.isEmpty(content)){
                        mTvContent.setVisibility(View.VISIBLE);
                        mTvContent.setText(content);
                    }else {
                        showShortToast("返回小票信息有误");
                    }
                }else {
                    if (!EmptyUtils.isEmpty(content)){
                        mTvContent.setVisibility(View.VISIBLE);
                        mTvContent.setText(content);
                    }

                    if (!EmptyUtils.isEmpty(imgurl)){
                        mIvContent.setVisibility(View.VISIBLE);
                        GlideUtils.getImageWidHeig(SettleRecordDetailActivtiy.this, imgurl, new GlideUtils.IGetImageData() {
                            @Override
                            public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mIvContent.getLayoutParams();
                                layoutParams.height = (int) (getResources().getDimensionPixelSize(R.dimen.dp_690) / radio);
                                mIvContent.setLayoutParams(layoutParams);

                                mIvContent.setImageBitmap(resource);
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    public void rightTitleViewClick() {
        if ("0".equals(isOrder)){
            if (!EmptyUtils.isEmpty(imgurl)){
                printImg(imgurl);
            }else if (!EmptyUtils.isEmpty(content)){
                printText(content);
            }else {
                showShortToast("返回小票信息有误");
            }
        }else {
            if (!EmptyUtils.isEmpty(imgurl)&&!EmptyUtils.isEmpty(content)){
                printTextImg(content,imgurl);
            }else if (!EmptyUtils.isEmpty(content)){
                printText(content);
            }else if (!EmptyUtils.isEmpty(imgurl)){
                printImg(imgurl);
            }else {
                showShortToast("返回小票信息有误");
            }
        }
    }

    //打印文字图片
    private void printTextImg(String content,String imgurl) {
        GlideUtils.getImageWidHeig(SettleRecordDetailActivtiy.this, imgurl, new GlideUtils.IGetImageData() {
            @Override
            public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                showWaitDialog("正在打印...");
                CPayPrint printer = CPayDevice.getCPayPrint();

                try {
                    printer.printDatas_order(content,resource, new CPayPrint.PrintCallback() {
                        @Override
                        public void onCall(int code, String msg) {
                            hideWaitDialog();
                            if (code != 9999) {
                                showShortToast(msg);
                            }
                        }
                    });
                } catch (Exception e) {
                    hideWaitDialog();
                    showTipDialog("打印异常了:"+e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    //打印图片
    private void printImg(String imgurl) {
        GlideUtils.getImageWidHeig(SettleRecordDetailActivtiy.this, imgurl, new GlideUtils.IGetImageData() {
            @Override
            public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                showWaitDialog("正在打印...");
                CPayPrint printer = CPayDevice.getCPayPrint();

                try {
                    printer.print(resource, new CPayPrint.PrintCallback() {
                        @Override
                        public void onCall(int code, String msg) {
                            hideWaitDialog();
                            if (code != 9999) {
                                showShortToast(msg);
                            }
                        }
                    });
                } catch (Exception e) {
                    hideWaitDialog();
                    showTipDialog("打印异常了:"+e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    //打印文本
    private void printText(String content) {
        showWaitDialog("正在打印...");
        CPayPrint printer = CPayDevice.getCPayPrint();

        try {
            printer.printSingle(content, new CPayPrint.PrintCallback() {
                @Override
                public void onCall(int code, String msg) {
                    hideWaitDialog();
                    if (code != 9999) {
                        showShortToast(msg);
                    }
                }
            });
        } catch (Exception e) {
            LogUtils.e("=========================打印异常了"+e.getMessage());
            e.printStackTrace();
        }
    }

    public static void newIntance(Context context, String setl_id) {
        Intent intent = new Intent(context, SettleRecordDetailActivtiy.class);
        intent.putExtra("setl_id", setl_id);
        context.startActivity(intent);
    }
}
