package com.jrdz.zhyb_android.ui.settlement.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;

import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.home.activity.LargeVersionActivity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-05-12
 * 描    述：结算类型选择页面
 * ================================================
 */
public class SettleTypeActivity extends BaseActivity {
    private LinearLayout mLlMedInsuSettle;
    private LinearLayout mLlOwnExpenseSettle;

    @Override
    public int getLayoutId() {
        return R.layout.activity_settle_type;
    }

    @Override
    public void initView() {
        super.initView();
        mLlMedInsuSettle = findViewById(R.id.ll_med_insu_settle);
        mLlOwnExpenseSettle = findViewById(R.id.ll_own_expense_settle);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mLlMedInsuSettle.setOnClickListener(this);
        mLlOwnExpenseSettle.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()){
            case R.id.ll_med_insu_settle://医保结算
                QueryPersonalInfoActivity.newIntance(SettleTypeActivity.this, "1");
                goFinish();
                break;
            case R.id.ll_own_expense_settle://自费结算
                OwnExpenseSettleActivity.newIntance(SettleTypeActivity.this);
                goFinish();
                break;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, SettleTypeActivity.class);
        context.startActivity(intent);
    }
}
