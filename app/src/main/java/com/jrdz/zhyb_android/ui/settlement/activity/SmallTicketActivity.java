package com.jrdz.zhyb_android.ui.settlement.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.provider.MediaStore;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ImageUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.pos.CPayDevice;
import com.jrdz.zhyb_android.pos.CPayPrint;
import com.jrdz.zhyb_android.ui.settlement.model.SmallTicketModel;
import com.jrdz.zhyb_android.utils.ScreenshotUtil;
import com.jrdz.zhyb_android.widget.pop.HandWritePop;
import com.tencent.bugly.crashreport.CrashReport;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/18
 * 描    述： 小票页面
 * ================================================
 */
public class SmallTicketActivity extends BaseActivity implements HandWritePop.IOptionListener {
    private ScrollView mSvContain;
    private TextView mTvContent;
    private ShapeTextView mTvPrint;
    private ShapeFrameLayout mSllSignature;
    private TextView mTvSignature;
    private ImageView mIvSignatureTag,mIvSignature;

    private String setlId,from;
    private int pos;
    private HandWritePop handWritePop;
    private Bitmap scaleBitmap;

    @Override
    public int getLayoutId() {
        return R.layout.activity_small_ticket;
    }

    @Override
    public void initView() {
        super.initView();
        mSvContain = findViewById(R.id.sv_contain);
        mTvContent = findViewById(R.id.tv_content);
        mTvPrint= findViewById(R.id.tv_print);

        mSllSignature= findViewById(R.id.sll_signature);
        mIvSignatureTag= findViewById(R.id.iv_signature_tag);
        mTvSignature = findViewById(R.id.tv_signature);
        mIvSignature = findViewById(R.id.iv_signature);
    }

    @Override
    public void initData() {
        setlId = getIntent().getStringExtra("setl_id");
        from = getIntent().getStringExtra("from");
        pos= getIntent().getIntExtra("pos",0);
        super.initData();

        if (EmptyUtils.isEmpty(setlId)){
            showWaitDialog("数据获取失败");
            return;
        }

        setRightTitleView("上传");

        if (Constants.Configure.IS_POS){
            mTvPrint.setVisibility(View.VISIBLE);
        }else {
            mTvPrint.setVisibility(View.GONE);
        }

        showWaitDialog();
        getSmallTicketData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvPrint.setOnClickListener(this);
        mSllSignature.setOnClickListener(this);
    }

    //获取小票数据
    private void getSmallTicketData() {
        SmallTicketModel.sendSmallTicketRequest(TAG, setlId,new CustomerJsonCallBack<SmallTicketModel>() {
            @Override
            public void onRequestError(SmallTicketModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(SmallTicketModel returnData) {
                hideWaitDialog();
                String content=returnData.getData().getContent();
                if (!EmptyUtils.isEmpty(content)){
                    mTvContent.setText(content);
                }else {
                    showShortToast("返回小票信息有误");
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()) {
            case R.id.tv_print://打印结算单
                if (EmptyUtils.isEmpty(mTvContent.getText().toString())){
                    showShortToast("数据有误，请重新进入页面");
                    return;
                }
                if (mTvSignature.getVisibility() == View.VISIBLE) {
                    showShortToast("请先签名");
                    return;
                }
                printDatas();
                break;
            case R.id.sll_signature://填写签名
                if (handWritePop==null){
                    handWritePop=new HandWritePop(SmallTicketActivity.this, this);
                }

                handWritePop.showPopupWindow();
                break;
        }
    }

    @Override
    public void rightTitleViewClick() {
        if (mTvSignature.getVisibility() == View.VISIBLE) {
            showShortToast("请先签名");
            return;
        }
        showWaitDialog();
        if (Constants.Configure.IS_POS){
            mTvPrint.setVisibility(View.GONE);
        }
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                String imagPath = BaseGlobal.getImageDir() + "smallTicket/"+setlId + ".jpg";
                // 最后通知图库更新
                try {
                    boolean saveResult = ImageUtils.save(ScreenshotUtil.compressImage(ScreenshotUtil.scrollViewScreenShot(mSvContain, Color.WHITE)), imagPath, Bitmap.CompressFormat.JPEG);
                    if (saveResult){
                        MediaStore.Images.Media.insertImage(getContentResolver(), imagPath, "title", "description");
                    }else {
                        hideWaitDialog();
                        showShortToast("保存图片失败");
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    hideWaitDialog();
                    showShortToast("保存图片失败");
                    CrashReport.postCatchedException(e);  // bugly会将这个throwable上报
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (Constants.Configure.IS_POS){
                            mTvPrint.setVisibility(View.VISIBLE);
                        }
                        BaseModel.sendUploadFileRequest(TAG, setlId, new File(imagPath), new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                showShortToast("上传成功");

                                if ("1".equals(from)){//来自药店/门诊结算页面
                                    SettleSuccessActivity.newIntance(SmallTicketActivity.this);
                                }else {
                                    Intent intent=new Intent();
                                    intent.putExtra("pos", pos);
                                    setResult(RESULT_OK,intent);
                                }

                                finish();
                            }
                        });
                    }
                });
            }
        });
    }

    @Override
    public void commit(Bitmap mBitmap) {
        mIvSignatureTag.setVisibility(View.GONE);
        mTvSignature.setVisibility(View.GONE);
        mIvSignature.setVisibility(View.VISIBLE);

        mIvSignature.post(new Runnable() {
            @Override
            public void run() {
                if (isFinishing()||mBitmap==null)return;
                int imageWidth = mIvSignature.getWidth();// ImageView的宽
                /**
                 *  BitmapFactory提供了多种方式根据不同的图片源来创建Bitmap对象：
                 *   （1）Bitmap BitmapFactory.decodeFile(String pathName, Options opts)： 读取SD卡上的图片。
                 *   （2）Bitmap BitmapFactory.decodeResource(Resources res, int id, Options opts) ： 读取网络上的图片。
                 *   （3）Bitmap BitmapFactory.decodeResource(Resources res, int id, Options opts) ： 读取资源文件中的图片。
                 *   这三个函数默认会直接为bitmap分配内存，我们通过第二个参数Options来控制，让它不分配内存，同时可以得到图片的相关信息。具体参考：
                 *   BitmapFactory.Options options = new BitmapFactory.Options();
                 *   options.inJustDecodeBounds = true;
                 *   BitmapFactory.decodeFile(pathName，options);
                 *   int imageHeight = options.outHeight;
                 *   int imageWidth = options.outWidth;
                 */
                int bitmapWidth = mBitmap.getWidth();// Bitmap的宽
                int bitmapHeight = mBitmap.getHeight();// Bitmap的高
                //计算展示图片的高度
                int imageHeight = new BigDecimal(String.valueOf(imageWidth))
                        .divide(new BigDecimal(String.valueOf(bitmapWidth)), 2, BigDecimal.ROUND_HALF_UP)
                        .multiply(new BigDecimal(String.valueOf(bitmapHeight))).intValue();

                FrameLayout.LayoutParams parma = (FrameLayout.LayoutParams) mIvSignature.getLayoutParams();
                parma.height = imageHeight;
                mIvSignature.setLayoutParams(parma);

                scaleBitmap=createScaleBitmap(mBitmap, imageWidth, imageHeight);
                mIvSignature.setImageBitmap(scaleBitmap);
            }
        });
    }

    public Bitmap createScaleBitmap(Bitmap src, int dstWidth, int dstHeight) {
        if (dstHeight <= 0 || dstWidth <= 0) return null;
        /*如果是放大图片，filter决定是否平滑，如果是缩小图片，filter无影响，我们这里是缩小图片，所以直接设置为false*/
        Bitmap dst = Bitmap.createScaledBitmap(src, dstWidth, dstHeight, false);
        if (dst == null) {
            return src;
        }
        if (src != dst) { // 如果没有缩放，那么不回收
            src.recycle(); // 释放Bitmap的native像素数组
        }
        return dst;
    }

    //打印结算单
    private void printDatas() {
        showWaitDialog("正在打印...");
        CPayPrint printer = CPayDevice.getCPayPrint();

        try {
            printer.printDatas(mTvContent.getText().toString(),scaleBitmap, new CPayPrint.PrintCallback() {
                @Override
                public void onCall(int code, String msg) {
                    hideWaitDialog();
                    if (code != 9999) {
                        showShortToast(msg);
                    }
                }
            });
        } catch (Exception e) {
            hideWaitDialog();
            showTipDialog("打印异常了:"+e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if ("1".equals(from)){
            showShortToast("小票为必传内容，请上传小票");
        }else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        if (handWritePop!=null){
            handWritePop.onClean();
        }
        super.onDestroy();
    }

    //from 1:来自药店/门诊结算页面  2:来自药店/门诊结算列表页面
    public static void newIntance(Context context,String setl_id, String from) {
        Intent intent = new Intent(context, SmallTicketActivity.class);
        intent.putExtra("setl_id", setl_id);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }

    //from 1:来自药店/门诊结算页面  2:来自药店/门诊结算列表页面
    public static void newIntance(Activity activity, String setl_id, String from, int pos, int requestCode) {
        Intent intent = new Intent(activity, SmallTicketActivity.class);
        intent.putExtra("setl_id", setl_id);
        intent.putExtra("from", from);
        intent.putExtra("pos", pos);
        activity.startActivityForResult(intent,requestCode);
    }
}
