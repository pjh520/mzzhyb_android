package com.jrdz.zhyb_android.ui.settlement.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.catalogue.model.ChinaPrescrCataModel;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatPrescrDetailModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-05-16
 * 描    述：
 * ================================================
 */
public class ChinaPrescDragDetailAdapter extends BaseQuickAdapter<OutpatPrescrDetailModel.DataBean.FeedetailsBean, BaseViewHolder> {
    public ChinaPrescDragDetailAdapter() {
        super(R.layout.layout_pre_chinapresc_drag_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, OutpatPrescrDetailModel.DataBean.FeedetailsBean dataBean) {
        baseViewHolder.setText(R.id.tv_name, dataBean.getMedins_list_name()+"    "+dataBean.getCnt()+dataBean.getUnt());
    }
}
