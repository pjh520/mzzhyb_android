package com.jrdz.zhyb_android.ui.settlement.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatLogListModel;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatRegListModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/29
 * 描    述：
 * ================================================
 */
public class OutpatLogListAdapter extends BaseQuickAdapter<OutpatLogListModel.DataBean, BaseViewHolder> {
    private boolean isEditStatus;

    public OutpatLogListAdapter() {
        super(R.layout.layout_outpatloglist_item, null);
    }

    public void setIsEditStatus(boolean isEditStatus){
        this.isEditStatus=isEditStatus;
        notifyDataSetChanged();
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.tv_detail);
        baseViewHolder.addOnClickListener(R.id.fl_cb);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, OutpatLogListModel.DataBean resultObjBean) {
        FrameLayout flCb=baseViewHolder.getView(R.id.fl_cb);
        CheckBox cb=baseViewHolder.getView(R.id.cb);

        baseViewHolder.setText(R.id.tv_ipt_otp_no, "门"+mContext.getResources().getString(R.string.spaces_half)+ "诊"+mContext.getResources().getString(R.string.spaces_half)+"号："+resultObjBean.getIpt_otp_no());
        baseViewHolder.setText(R.id.tv_mdtrt_id, "就"+mContext.getResources().getString(R.string.spaces_half)+"诊"+ mContext.getResources().getString(R.string.spaces_half)+"ID："+EmptyUtils.strEmptyToText(resultObjBean.getMdtrt_id(),"--"));
        baseViewHolder.setText(R.id.tv_home_no, "户主姓名："+ EmptyUtils.strEmptyToText(resultObjBean.getOuseholder(),"--"));
        baseViewHolder.setText(R.id.tv_psn_no, "患者姓名："+resultObjBean.getPsn_name()+"("+resultObjBean.getMdtrt_cert_no()+")");
        baseViewHolder.setText(R.id.tv_create_time, "就诊日期："+ resultObjBean.getBegntime());

        flCb.setVisibility(isEditStatus? View.VISIBLE:View.GONE);
        cb.setChecked(resultObjBean.choose?true:false);
    }
}
