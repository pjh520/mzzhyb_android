package com.jrdz.zhyb_android.ui.settlement.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatLogListModel;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatPrescrListModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/29
 * 描    述：
 * ================================================
 */
public class OutpatPrescrListAdapter extends BaseQuickAdapter<OutpatPrescrListModel.DataBean, BaseViewHolder> {
    private boolean isEditStatus;

    public OutpatPrescrListAdapter() {
        super(R.layout.layout_outpatprescrlist_item, null);
    }

    public void setIsEditStatus(boolean isEditStatus){
        this.isEditStatus=isEditStatus;
        notifyDataSetChanged();
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.tv_detail);
        baseViewHolder.addOnClickListener(R.id.fl_cb);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, OutpatPrescrListModel.DataBean resultObjBean) {
        FrameLayout flCb=baseViewHolder.getView(R.id.fl_cb);
        CheckBox cb=baseViewHolder.getView(R.id.cb);
        ShapeTextView tvPrescrStatus=baseViewHolder.getView(R.id.tv_prescr_status);

        baseViewHolder.setText(R.id.tv_prescr_no, "处方编号："+ EmptyUtils.strEmptyToText(resultObjBean.getElectronicPrescriptionNo(),"--"));
        baseViewHolder.setText(R.id.tv_prescr_status, "已召回");
        baseViewHolder.setText(R.id.tv_name, "姓"+mContext.getResources().getString(R.string.spaces)+mContext.getResources().getString(R.string.spaces)+"名："+EmptyUtils.strEmptyToText(resultObjBean.getPsn_name(),"--"));
        baseViewHolder.setText(R.id.tv_sex, "性"+mContext.getResources().getString(R.string.spaces)+mContext.getResources().getString(R.string.spaces)+"别："+("1".equals(resultObjBean.getSex())?"男":"女"));
        baseViewHolder.setText(R.id.tv_age, "年"+mContext.getResources().getString(R.string.spaces)+mContext.getResources().getString(R.string.spaces)+"龄："+EmptyUtils.strEmptyToText(resultObjBean.getAge(),"--"));
        baseViewHolder.setText(R.id.tv_ipt_otp_no, "门"+mContext.getResources().getString(R.string.spaces_half)+"诊"+mContext.getResources().getString(R.string.spaces_half)+"号："+EmptyUtils.strEmptyToText(resultObjBean.getIpt_otp_no(),"--"));
        baseViewHolder.setText(R.id.tv_create_time, "开具日期："+EmptyUtils.strEmptyToText(resultObjBean.getCreateDT(),"--"));

        if ("已召回".equals(resultObjBean.getStatusName())){
            tvPrescrStatus.setVisibility(View.VISIBLE);
        }else {
            tvPrescrStatus.setVisibility(View.GONE);
        }
        flCb.setVisibility(isEditStatus? View.VISIBLE:View.GONE);
        cb.setChecked(resultObjBean.choose?true:false);
    }
}
