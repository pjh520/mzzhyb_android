package com.jrdz.zhyb_android.ui.settlement.adapter;

import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.DateUtil;
import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatRegListModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/29
 * 描    述：
 * ================================================
 */
public class OutpatRegListAdapter extends BaseQuickAdapter<OutpatRegListModel.DataBean, BaseViewHolder> {
    public OutpatRegListAdapter() {
        super(R.layout.layout_outpatreglist_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.tv_detail);
        baseViewHolder.addOnClickListener(R.id.tv_cancle);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, OutpatRegListModel.DataBean resultObjBean) {
        TextView tvStatus=baseViewHolder.getView(R.id.tv_status);
        ShapeTextView tvDetail=baseViewHolder.getView(R.id.tv_detail);

        baseViewHolder.setText(R.id.tv_ipt_otp_no, "门"+mContext.getResources().getString(R.string.spaces_half)+
                "诊"+mContext.getResources().getString(R.string.spaces_half)+"号："+resultObjBean.getIpt_otp_no());
        baseViewHolder.setText(R.id.tv_mdtrt_id, "就"+mContext.getResources().getString(R.string.spaces_half)+"诊"+
                mContext.getResources().getString(R.string.spaces_half)+"ID："+resultObjBean.getMdtrt_id());
        baseViewHolder.setText(R.id.tv_psn_no, "人员信息："+resultObjBean.getPsn_name()+"("+resultObjBean.getMdtrt_cert_no()+")");
        baseViewHolder.setText(R.id.tv_dept_name, "科室名称："+resultObjBean.getDept_name());
        baseViewHolder.setText(R.id.tv_dr_name, "医师姓名："+resultObjBean.getDr_name());
        baseViewHolder.setText(R.id.tv_create_time, "挂号时间："+ resultObjBean.getBegntime());

        if (resultObjBean.getStep()<4){
            tvStatus.setText("未结算");
            tvDetail.setText("继续结算");
        }else {
            tvStatus.setText("已结算");
            tvDetail.setText("详细信息");
        }
    }
}
