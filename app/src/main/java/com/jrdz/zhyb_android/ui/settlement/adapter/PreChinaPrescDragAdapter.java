package com.jrdz.zhyb_android.ui.settlement.adapter;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.catalogue.model.ChinaPrescrCataModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-05-16
 * 描    述：
 * ================================================
 */
public class PreChinaPrescDragAdapter extends BaseQuickAdapter<ChinaPrescrCataModel.DataBean, BaseViewHolder> {
    public PreChinaPrescDragAdapter() {
        super(R.layout.layout_pre_chinapresc_drag_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ChinaPrescrCataModel.DataBean dataBean) {
        baseViewHolder.setText(R.id.tv_name, dataBean.getCatalogueModel().getFixmedins_hilist_name()+"    "+dataBean.getWeight()+dataBean.getWeightCompany());
    }
}
