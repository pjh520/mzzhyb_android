package com.jrdz.zhyb_android.ui.settlement.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.catalogue.model.DiseCataModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/1/26
 * 描    述：
 * ================================================
 */
public class RlDiseAdapter extends BaseQuickAdapter<DiseCataModel.DataBean, BaseViewHolder> {
    public RlDiseAdapter() {
        super(R.layout.layout_select_disecata_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.tv_delete);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, DiseCataModel.DataBean item) {
        helper.setText(R.id.tv_diag_type,"诊断类别:" + item.getDiag_typename());
        helper.setText(R.id.tv_diag_name,"诊断名称:" + item.getName());
    }
}
