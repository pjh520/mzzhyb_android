package com.jrdz.zhyb_android.ui.settlement.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.DateUtil;
import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.hjq.shape.view.ShapeView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.StmtInfoListModel;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatRegListModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/29
 * 描    述：
 * ================================================
 */
public class SettleRecordAdapter extends BaseQuickAdapter<StmtInfoListModel.DataBean, BaseViewHolder> {
    public  SettleRecordAdapter() {
        super(R.layout.layout_settlerecord_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.tv_cancle);
        baseViewHolder.addOnClickListener(R.id.tv_up_smallticket);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, StmtInfoListModel.DataBean resultObjBean) {
        TextView tvSetlidRelat=baseViewHolder.getView(R.id.tv_setlid_relat);
        ShapeView line=baseViewHolder.getView(R.id.line);
        ShapeTextView tvCancle=baseViewHolder.getView(R.id.tv_cancle);
        ShapeTextView tvUpSmallticket=baseViewHolder.getView(R.id.tv_up_smallticket);

        baseViewHolder.setText(R.id.tv_setl_id, "结算编码："+resultObjBean.getSetl_id());
        baseViewHolder.setText(R.id.tv_setl_status, EmptyUtils.strEmptyToText(resultObjBean.getStatusName(),"--"));
        baseViewHolder.setText(R.id.tv_insutype, "险种类型："+EmptyUtils.strEmptyToText(resultObjBean.getInsutype_name(),"--"));
        baseViewHolder.setText(R.id.tv_psn_no, "人员信息："+EmptyUtils.strEmptyToText(resultObjBean.getPsn_name(),"--")+
                "("+EmptyUtils.strEmptyToText(resultObjBean.getMdtrt_cert_no(),"--")+")");
        baseViewHolder.setText(R.id.tv_med_type, "医疗类别："+EmptyUtils.strEmptyToText(resultObjBean.getMed_type_name(),"--"));
        baseViewHolder.setText(R.id.tv_medfee_sumamt, "医疗费总额："+EmptyUtils.strEmptyToText(resultObjBean.getMedfee_sumamt(),"--"));
        baseViewHolder.setText(R.id.tv_fund_pay_sumamt, "基金支付总额："+EmptyUtils.strEmptyToText(resultObjBean.getFund_pay_sumamt(),"--"));
        baseViewHolder.setText(R.id.tv_acct_pay, "个人账户支付："+EmptyUtils.strEmptyToText(resultObjBean.getAcct_pay(),"--"));
        baseViewHolder.setText(R.id.tv_psn_cash_pay_tag, "个人现金支付："+EmptyUtils.strEmptyToText(resultObjBean.getPsn_cash_pay(),"--"));
        baseViewHolder.setText(R.id.tv_setl_time, "结算时间："+ EmptyUtils.strEmptyToText(resultObjBean.getSetl_time(),"--"));

        if (EmptyUtils.isEmpty(resultObjBean.getOriginal_setl_id())){
            tvSetlidRelat.setVisibility(View.GONE);
        }else {
            tvSetlidRelat.setVisibility(View.VISIBLE);
            tvSetlidRelat.setText("召回结算编码："+ resultObjBean.getOriginal_setl_id());
        }

        //Status  0是处理中，1是正常，2是撤销退费
        switch (resultObjBean.getStatus()){
            case "0":
                line.setVisibility(View.GONE);
                tvCancle.setVisibility(View.GONE);
                tvUpSmallticket.setVisibility(View.GONE);
                break;
            case "1":
                line.setVisibility(View.VISIBLE);
                tvCancle.setVisibility(View.VISIBLE);

                //1代表已上传，0代表未上传小票
                if ("0".equals(resultObjBean.getIsUpload())){
                    tvUpSmallticket.setVisibility(View.VISIBLE);
                }else if ("1".equals(resultObjBean.getIsUpload())){
                    tvUpSmallticket.setVisibility(View.GONE);
                }
                break;
            case "2":
                line.setVisibility(View.GONE);
                tvCancle.setVisibility(View.GONE);
                tvUpSmallticket.setVisibility(View.GONE);
                break;
        }
    }
}
