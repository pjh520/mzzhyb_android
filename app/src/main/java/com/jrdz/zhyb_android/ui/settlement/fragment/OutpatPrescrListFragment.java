package com.jrdz.zhyb_android.ui.settlement.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.wheel.TimeWheelUtils;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.settlement.activity.OutpatChinaPrescrDetailActivtiy;
import com.jrdz.zhyb_android.ui.settlement.activity.OutpatWestPrescrDetailActivtiy;
import com.jrdz.zhyb_android.ui.settlement.adapter.OutpatPrescrListAdapter;
import com.jrdz.zhyb_android.ui.settlement.model.OutpatPrescrListModel;
import com.jrdz.zhyb_android.ui.settlement.model.SelectOutpatPrescrModel;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.ElectPrescFragment_image;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.DownLoadUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/29
 * 描    述：电子处方列表--智慧医保处方
 * ================================================
 */
public class OutpatPrescrListFragment extends BaseRecyclerViewFragment {
    private AppCompatEditText mEtSearch;
    private TextView mTvSearch,mTvTime,mTvSearchTime;
    private ShapeTextView mTvDownload;
    private ShapeLinearLayout mSllTime;
    private View mLine;
    private LinearLayout mLlBottom;
    private FrameLayout mFlCbAll;
    private CheckBox mCbAll;
    private TextView mTvAll;
    private ShapeTextView mTvDownloadAll;

    private boolean isEditStatus = false;//是否时编辑状态
    int selectNum=0;//选中的药品数量
    private String key = "";//关键词搜索
    private String begntime = "";//时间搜索
    private TimeWheelUtils timeWheelUtils;
    private int maxChoose = 100;//最多可以下载多少条数据
    private ArrayList<SelectOutpatPrescrModel> selectOutpatPrescrLists = new ArrayList<>();//存储选中的处方

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_outpat_prescrlist;
    }

    @Override
    public void initView(View view) {
        super.initView(view);

        mEtSearch = view.findViewById(R.id.et_search);
        mTvSearch = view.findViewById(R.id.tv_search);
        mSllTime = view.findViewById(R.id.sll_time);
        mTvTime = view.findViewById(R.id.tv_time);
        mTvSearchTime = view.findViewById(R.id.tv_search_time);
        mTvDownload = view.findViewById(R.id.tv_download);
        mLine = view.findViewById(R.id.line);
        mLlBottom = view.findViewById(R.id.ll_bottom);
        mFlCbAll = view.findViewById(R.id.fl_cb_all);
        mCbAll = view.findViewById(R.id.cb_all);
        mTvAll = view.findViewById(R.id.tv_all);
        mTvDownloadAll = view.findViewById(R.id.tv_download_all);
    }

    @Override
    public void initAdapter() {
        mAdapter = new OutpatPrescrListAdapter();
    }

    @Override
    public void initData() {
        super.initData();

        if (Constants.Configure.IS_POS){
            mTvDownload.setVisibility(View.GONE);
        }else {
            mTvDownload.setVisibility(View.VISIBLE);
        }

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    onRefresh(mRefreshLayout);
                    return true;
                }
                return false;
            }
        });
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    key="";
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                }
            }
        });
        mTvSearch.setOnClickListener(this);
        mSllTime.setOnClickListener(this);
        mTvSearchTime.setOnClickListener(this);
        mTvDownload.setOnClickListener(this);
        mFlCbAll.setOnClickListener(this);
        mTvDownloadAll.setOnClickListener(this);
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void getData() {
        super.getData();

        OutpatPrescrListModel.sendOutpatPrescrListRequest(TAG, key,begntime,String.valueOf(mPageNum), "20",
                new CustomerJsonCallBack<OutpatPrescrListModel>() {
                    @Override
                    public void onRequestError(OutpatPrescrListModel returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(OutpatPrescrListModel returnData) {
                        hideRefreshView();
                        List<OutpatPrescrListModel.DataBean> infos = returnData.getData();
                        if (mAdapter!=null&&infos != null) {
                            if (mPageNum == 0) {
                                if (mCbAll!=null&&mCbAll.isChecked()) {//全选状态，加载更多需要勾选加载的
                                    selectNum = infos.size();
                                    if (selectNum>=maxChoose){
                                        showShortToast("一次最多只能选中"+maxChoose+"条处方下载");
                                        List<OutpatPrescrListModel.DataBean> frontDatas =infos.subList(0,maxChoose);
                                        for (OutpatPrescrListModel.DataBean messageData : frontDatas) {
                                            messageData.setChoose(true);
                                        }
                                        selectNum =frontDatas.size();
                                    }else {
                                        for (OutpatPrescrListModel.DataBean messageData : infos) {
                                            messageData.setChoose(true);
                                        }
                                    }

                                    setTvSelected();
                                }

                                mAdapter.setNewData(infos);
                                if (infos.size() <= 0) {
                                    mAdapter.isUseEmpty(true);
                                }
                            } else {
                                if (mCbAll!=null&&mCbAll.isChecked()&&mAdapter.getData().size()<maxChoose) {//全选状态，加载更多需要勾选加载的
                                    selectNum = mAdapter.getData().size()+infos.size();
                                    if (selectNum>=maxChoose){
                                        showShortToast("一次最多只能选中"+maxChoose+"条处方下载");
                                        List<OutpatPrescrListModel.DataBean> frontDatas =infos.subList(0,maxChoose-mAdapter.getData().size());
                                        for (OutpatPrescrListModel.DataBean messageData : frontDatas) {
                                            messageData.setChoose(true);
                                        }
                                        selectNum = mAdapter.getData().size()+frontDatas.size();
                                    }else {
                                        for (OutpatPrescrListModel.DataBean messageData : infos) {
                                            messageData.setChoose(true);
                                        }
                                    }

                                    setTvSelected();
                                }

                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_search://关键词搜索
                if (EmptyUtils.isEmpty(mEtSearch.getText().toString())) {
                    showShortToast("输入处方编号/姓名/门诊号/电话");
                    return;
                }

                key=mEtSearch.getText().toString();
                begntime="";
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.sll_time://选择时间
                KeyboardUtils.hideSoftInput(getActivity());
                if (timeWheelUtils == null) {
                    timeWheelUtils = new TimeWheelUtils();
                    timeWheelUtils.isShowDay(true, true, true, true, true, true);
                }
                timeWheelUtils.showTimeWheel(getContext(), "开具日期", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mTvTime.setText(EmptyUtils.strEmpty(dateInfo));
                    }
                });
                break;
            case R.id.tv_search_time://时间搜索
                if (EmptyUtils.isEmpty(mTvTime.getText().toString())) {
                    showShortToast("选择开具日期");
                    return;
                }

                key="";
                begntime=mTvTime.getText().toString();
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.tv_download://下载
                if (isEditStatus) {
                    mTvDownload.setText("下载");
                    mTvDownload.getShapeDrawableBuilder().setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent))
                            .setSolidColor(getResources().getColor(R.color.color_4970e0)).intoBackground();
                    mTvDownload.setTextColor(getResources().getColor(R.color.white));
                    mLine.setVisibility(View.GONE);
                    mLlBottom.setVisibility(View.GONE);

                    cleanChoose();
                    ((OutpatPrescrListAdapter) mAdapter).setIsEditStatus(false);

                    mCbAll.setChecked(false);
                    selectNum=0;
                    setTvSelected();
                } else {
                    mTvDownload.setText("取消");
                    mTvDownload.getShapeDrawableBuilder().setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1)).setStrokeColor(getResources().getColor(R.color.color_4970e0))
                            .setSolidColor(getResources().getColor(R.color.bar_transparent)).intoBackground();
                    mTvDownload.setTextColor(getResources().getColor(R.color.color_4970e0));
                    mLine.setVisibility(View.VISIBLE);
                    mLlBottom.setVisibility(View.VISIBLE);

                    ((OutpatPrescrListAdapter) mAdapter).setIsEditStatus(true);
                }

                isEditStatus = !isEditStatus;
                break;
            case R.id.fl_cb_all:
                if (mCbAll.isChecked()) {//取消全选
                    mCbAll.setChecked(false);
                    cleanChoose();

                    selectNum=0;
                    mAdapter.notifyDataSetChanged();
                } else {//全选
                    mCbAll.setChecked(true);
                    allChoose();

                    selectNum=mAdapter.getData().size();
                    mAdapter.notifyDataSetChanged();
                }

                setTvSelected();
                break;
            case R.id.tv_download_all://下载全部
                //2022-05-11 限制最多只能选择100条数据
                selectOutpatPrescrLists.clear();
                for (OutpatPrescrListModel.DataBean datum : ((OutpatPrescrListAdapter) mAdapter).getData()) {
                    if (datum.isChoose()){
                        SelectOutpatPrescrModel selectOutpatPrescrModel=new SelectOutpatPrescrModel(EmptyUtils.strEmpty(datum.getAccessoryUrl()),
                                EmptyUtils.strEmpty(datum.getPsn_name()+"("+datum.getElectronicPrescriptionNo()+")"));
                        selectOutpatPrescrLists.add(selectOutpatPrescrModel);
                    }
                }
                if (selectOutpatPrescrLists.size()==0){
                    showShortToast("请选择要下载的处方");
                    return;
                }
                showWaitDialog("文件下载中");
                download(selectOutpatPrescrLists);
                break;
        }
    }

    private void download(ArrayList<SelectOutpatPrescrModel> selectOutpatPrescrLists){
        if (selectOutpatPrescrLists.isEmpty()){
            hideWaitDialog();
            showTipDialog("下载完成,存储地址:"+ CommonlyUsedDataUtils.getInstance().getElecPrescrDir());
            return;
        }

        SelectOutpatPrescrModel selectData = selectOutpatPrescrLists.get(0);
        DownLoadUtils.downLoadPic(selectData.getUrl(), TAG, CommonlyUsedDataUtils.getInstance().getElecPrescrDir(), selectData.getTitle()+".png", new DownLoadUtils.IVideoDownLoad() {
            @Override
            public void onDownLoadSuccess(File response, String tag) {
                // 最后通知图库更新
                try {
                    MediaStore.Images.Media.insertImage(getContext().getContentResolver(), response.getAbsolutePath(), "title", "description");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                selectOutpatPrescrLists.remove(0);
                download(selectOutpatPrescrLists);
            }

            @Override
            public void onDownLoadFail(String errorText) {
                showShortToast(errorText);
                selectOutpatPrescrLists.remove(0);
                download(selectOutpatPrescrLists);
            }
        });
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        OutpatPrescrListModel.DataBean itemData = ((OutpatPrescrListAdapter) adapter).getItem(position);
        switch (view.getId()) {
            case R.id.tv_detail://详细信息
                if ("1".equals(itemData.getPrescriptionType())){//西药中成药处方
                    OutpatWestPrescrDetailActivtiy.newIntance(getContext(),itemData.getElectronicPrescriptionId());
                }else if ("2".equals(itemData.getPrescriptionType())){//中药处方
                    OutpatChinaPrescrDetailActivtiy.newIntance(getContext(),itemData.getElectronicPrescriptionId());
                }
                break;
            case R.id.fl_cb://选中需要下载的数据
                CheckBox cb = view.findViewById(R.id.cb);
                if (cb.isChecked()) {
                    cb.setChecked(false);
                    itemData.setChoose(false);
                    selectNum--;

                    //取消全选
                    mCbAll.setChecked(false);
                } else {
                    if (selectNum==maxChoose){
                        showShortToast("一次最多只能选中"+maxChoose+"条日志下载");
                        return;
                    }

                    cb.setChecked(true);
                    itemData.setChoose(true);
                    selectNum++;

                    if (selectNum==maxChoose||selectNum==mAdapter.getData().size()){
                        //全选
                        mCbAll.setChecked(true);
                    }
                }

                setTvSelected();
                break;
        }
    }

    //清除选中
    private void cleanChoose() {
        for (OutpatPrescrListModel.DataBean messageData : ((OutpatPrescrListAdapter) mAdapter).getData()) {
            messageData.setChoose(false);
        }
    }

    //全选
    private void allChoose() {
        List<OutpatPrescrListModel.DataBean> datas = ((OutpatPrescrListAdapter) mAdapter).getData();
        if (datas.size()<=maxChoose){
            for (OutpatPrescrListModel.DataBean messageData : datas) {
                messageData.setChoose(true);
            }
            selectNum = mAdapter.getData().size();
        }else {
            showShortToast("一次最多只能选中"+maxChoose+"条处方下载");
            List<OutpatPrescrListModel.DataBean> front100Datas = datas.subList(0, maxChoose);
            List<OutpatPrescrListModel.DataBean> after100Datas = datas.subList(maxChoose, datas.size());
            for (OutpatPrescrListModel.DataBean messageData : front100Datas) {
                messageData.setChoose(true);
            }
            for (OutpatPrescrListModel.DataBean messageData : after100Datas) {
                messageData.setChoose(false);
            }
            selectNum = maxChoose;
        }
    }

    //设置选中的数据
    private void setTvSelected() {
        mTvAll.setText("全选("+selectNum+"条处方)");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timeWheelUtils != null) {
            timeWheelUtils.dissTimeWheel();
            timeWheelUtils = null;
        }
    }

    public static OutpatPrescrListFragment newIntance() {
        OutpatPrescrListFragment outpatPrescrListFragment = new OutpatPrescrListFragment();
        return outpatPrescrListFragment;
    }
}
