package com.jrdz.zhyb_android.ui.settlement.model;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/22
 * 描    述：
 * ================================================
 */
public class FeedetailModel {
    private String prd_days;
    private String isWMCPMPrescription;//是否西药处方1是0否
    private String isCMPrescription;//是否中药处方1是0否
    private String settlementClassification;//结算分类 1医保结算 2自费结算
    private String wmcpmDiagResults;//西药处方诊断结果
    private String wmcpmAmount;//西药处方合计
    private String cmDiagResults;//中药处方诊断结果
    private String cmDrugNum;//中药处方药品数量
    private String unt;//中药处方药品数量单位
    private String cmused_mtd;//中药处方用法
    private String cmAmount;//中药处方合计
    private String cmRemark;//中药处方备注
    private String accessoryId1;//西药中成药处方图片id
    private String accessoryId2;//中药处方图片id
    private String ipt_otp_no;//门诊号
    private String electronicPrescriptionNo1;//西药处方号
    private String electronicPrescriptionNo2;//中药处方号


    private ArrayList<FeedetailBean> feedetail;//外部药品
    private ArrayList<WmcpmPrescriptionBean> wmcpmPrescription;//西药处方
    private ArrayList<CmPrescriptionBean> cmPrescription;//中药处方

    public FeedetailModel(String prd_days, String isWMCPMPrescription, String isCMPrescription, String settlementClassification,
                          String wmcpmDiagResults, String wmcpmAmount, String cmDiagResults, String cmDrugNum, String unt, String cmused_mtd,
                          String cmAmount, String cmRemark, String accessoryId1, String accessoryId2, String ipt_otp_no, String electronicPrescriptionNo1,
                          String electronicPrescriptionNo2) {
        this.prd_days = prd_days;
        this.isWMCPMPrescription = isWMCPMPrescription;
        this.isCMPrescription = isCMPrescription;
        this.settlementClassification = settlementClassification;
        this.wmcpmDiagResults = wmcpmDiagResults;
        this.wmcpmAmount = wmcpmAmount;
        this.cmDiagResults = cmDiagResults;
        this.cmDrugNum = cmDrugNum;
        this.unt = unt;
        this.cmused_mtd = cmused_mtd;
        this.cmAmount = cmAmount;
        this.cmRemark = cmRemark;
        this.accessoryId1 = accessoryId1;
        this.accessoryId2 = accessoryId2;

        this.ipt_otp_no = ipt_otp_no;
        this.electronicPrescriptionNo1 = electronicPrescriptionNo1;
        this.electronicPrescriptionNo2 = electronicPrescriptionNo2;
    }

    public String getPrd_days() {
        return prd_days;
    }

    public void setPrd_days(String prd_days) {
        this.prd_days = prd_days;
    }

    public String getIsWMCPMPrescription() {
        return isWMCPMPrescription;
    }

    public void setIsWMCPMPrescription(String isWMCPMPrescription) {
        this.isWMCPMPrescription = isWMCPMPrescription;
    }

    public String getIsCMPrescription() {
        return isCMPrescription;
    }

    public void setIsCMPrescription(String isCMPrescription) {
        this.isCMPrescription = isCMPrescription;
    }

    public String getSettlementClassification() {
        return settlementClassification;
    }

    public void setSettlementClassification(String settlementClassification) {
        this.settlementClassification = settlementClassification;
    }

    public String getWmcpmDiagResults() {
        return wmcpmDiagResults;
    }

    public void setWmcpmDiagResults(String wmcpmDiagResults) {
        this.wmcpmDiagResults = wmcpmDiagResults;
    }

    public String getWmcpmAmount() {
        return wmcpmAmount;
    }

    public void setWmcpmAmount(String wmcpmAmount) {
        this.wmcpmAmount = wmcpmAmount;
    }

    public String getCmDiagResults() {
        return cmDiagResults;
    }

    public void setCmDiagResults(String cmDiagResults) {
        this.cmDiagResults = cmDiagResults;
    }

    public String getCmDrugNum() {
        return cmDrugNum;
    }

    public void setCmDrugNum(String cmDrugNum) {
        this.cmDrugNum = cmDrugNum;
    }

    public String getUnt() {
        return unt;
    }

    public void setUnt(String unt) {
        this.unt = unt;
    }

    public String getCmused_mtd() {
        return cmused_mtd;
    }

    public void setCmused_mtd(String cmused_mtd) {
        this.cmused_mtd = cmused_mtd;
    }

    public String getCmAmount() {
        return cmAmount;
    }

    public void setCmAmount(String cmAmount) {
        this.cmAmount = cmAmount;
    }

    public String getCmRemark() {
        return cmRemark;
    }

    public void setCmRemark(String cmRemark) {
        this.cmRemark = cmRemark;
    }

    public ArrayList<WmcpmPrescriptionBean> getWmcpmPrescription() {
        return wmcpmPrescription;
    }

    public void setWmcpmPrescription(ArrayList<WmcpmPrescriptionBean> wmcpmPrescription) {
        this.wmcpmPrescription = wmcpmPrescription;
    }

    public ArrayList<CmPrescriptionBean> getCmPrescription() {
        return cmPrescription;
    }

    public void setCmPrescription(ArrayList<CmPrescriptionBean> cmPrescription) {
        this.cmPrescription = cmPrescription;
    }

    public ArrayList<FeedetailBean> getFeedetail() {
        return feedetail;
    }

    public void setFeedetail(ArrayList<FeedetailBean> feedetail) {
        this.feedetail = feedetail;
    }

    public String getAccessoryId1() {
        return accessoryId1;
    }

    public void setAccessoryId1(String accessoryId1) {
        this.accessoryId1 = accessoryId1;
    }

    public String getAccessoryId2() {
        return accessoryId2;
    }

    public void setAccessoryId2(String accessoryId2) {
        this.accessoryId2 = accessoryId2;
    }

    public String getIpt_otp_no() {
        return ipt_otp_no;
    }

    public void setIpt_otp_no(String ipt_otp_no) {
        this.ipt_otp_no = ipt_otp_no;
    }

    public String getElectronicPrescriptionNo1() {
        return electronicPrescriptionNo1;
    }

    public void setElectronicPrescriptionNo1(String electronicPrescriptionNo1) {
        this.electronicPrescriptionNo1 = electronicPrescriptionNo1;
    }

    public String getElectronicPrescriptionNo2() {
        return electronicPrescriptionNo2;
    }

    public void setElectronicPrescriptionNo2(String electronicPrescriptionNo2) {
        this.electronicPrescriptionNo2 = electronicPrescriptionNo2;
    }

    public static class WmcpmPrescriptionBean {
        private String mdtrt_id;
        private String psn_no;
        private String dise_codg;
        private String rx_circ_flag;
        private String med_list_codg;
        private String medins_list_codg;
        private String det_item_fee_sumamt;
        private String cnt;
        private String pric;
        private String prd_days;
        private String bilg_dept_codg;
        private String bilg_dept_name;
        private String bilg_dr_codg;
        private String bilg_dr_name;
        private String hosp_appr_flag;

        private String unt;//药品数量
        private String sin_dos_dscr;//每次用量
        private String used_frqu_dscr;//频率
        private String used_mtd;//频率
        private String remark;//备注
        private String spec;//规格

        public WmcpmPrescriptionBean(String mdtrt_id, String psn_no, String dise_codg, String rx_circ_flag, String med_list_codg,
                                     String medins_list_codg, String det_item_fee_sumamt, String cnt, String pric, String prd_days,
                                     String bilg_dept_codg, String bilg_dept_name, String bilg_dr_codg, String bilg_dr_name,
                                     String hosp_appr_flag, String unt, String sin_dos_dscr, String used_frqu_dscr, String used_mtd,
                                     String remark,String spec) {
            this.mdtrt_id = mdtrt_id;
            this.psn_no = psn_no;
            this.dise_codg = dise_codg;
            this.rx_circ_flag = rx_circ_flag;
            this.med_list_codg = med_list_codg;
            this.medins_list_codg = medins_list_codg;
            this.det_item_fee_sumamt = det_item_fee_sumamt;
            this.cnt = cnt;
            this.pric = pric;
            this.prd_days = prd_days;
            this.bilg_dept_codg = bilg_dept_codg;
            this.bilg_dept_name = bilg_dept_name;
            this.bilg_dr_codg = bilg_dr_codg;
            this.bilg_dr_name = bilg_dr_name;
            this.hosp_appr_flag = hosp_appr_flag;

            this.unt = unt;
            this.sin_dos_dscr = sin_dos_dscr;
            this.used_frqu_dscr = used_frqu_dscr;
            this.used_mtd = used_mtd;
            this.remark = remark;
            this.spec = spec;
        }

        public String getMdtrt_id() {
            return mdtrt_id;
        }

        public void setMdtrt_id(String mdtrt_id) {
            this.mdtrt_id = mdtrt_id;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getDise_codg() {
            return dise_codg;
        }

        public void setDise_codg(String dise_codg) {
            this.dise_codg = dise_codg;
        }

        public String getRx_circ_flag() {
            return rx_circ_flag;
        }

        public void setRx_circ_flag(String rx_circ_flag) {
            this.rx_circ_flag = rx_circ_flag;
        }

        public String getMed_list_codg() {
            return med_list_codg;
        }

        public void setMed_list_codg(String med_list_codg) {
            this.med_list_codg = med_list_codg;
        }

        public String getMedins_list_codg() {
            return medins_list_codg;
        }

        public void setMedins_list_codg(String medins_list_codg) {
            this.medins_list_codg = medins_list_codg;
        }

        public String getDet_item_fee_sumamt() {
            return det_item_fee_sumamt;
        }

        public void setDet_item_fee_sumamt(String det_item_fee_sumamt) {
            this.det_item_fee_sumamt = det_item_fee_sumamt;
        }

        public String getCnt() {
            return cnt;
        }

        public void setCnt(String cnt) {
            this.cnt = cnt;
        }

        public String getPric() {
            return pric;
        }

        public void setPric(String pric) {
            this.pric = pric;
        }

        public String getPrd_days() {
            return prd_days;
        }

        public void setPrd_days(String prd_days) {
            this.prd_days = prd_days;
        }

        public String getBilg_dept_codg() {
            return bilg_dept_codg;
        }

        public void setBilg_dept_codg(String bilg_dept_codg) {
            this.bilg_dept_codg = bilg_dept_codg;
        }

        public String getBilg_dept_name() {
            return bilg_dept_name;
        }

        public void setBilg_dept_name(String bilg_dept_name) {
            this.bilg_dept_name = bilg_dept_name;
        }

        public String getBilg_dr_codg() {
            return bilg_dr_codg;
        }

        public void setBilg_dr_codg(String bilg_dr_codg) {
            this.bilg_dr_codg = bilg_dr_codg;
        }

        public String getBilg_dr_name() {
            return bilg_dr_name;
        }

        public void setBilg_dr_name(String bilg_dr_name) {
            this.bilg_dr_name = bilg_dr_name;
        }

        public String getHosp_appr_flag() {
            return hosp_appr_flag;
        }

        public void setHosp_appr_flag(String hosp_appr_flag) {
            this.hosp_appr_flag = hosp_appr_flag;
        }

        public String getUnt() {
            return unt;
        }

        public void setUnt(String unt) {
            this.unt = unt;
        }

        public String getSin_dos_dscr() {
            return sin_dos_dscr;
        }

        public void setSin_dos_dscr(String sin_dos_dscr) {
            this.sin_dos_dscr = sin_dos_dscr;
        }

        public String getUsed_frqu_dscr() {
            return used_frqu_dscr;
        }

        public void setUsed_frqu_dscr(String used_frqu_dscr) {
            this.used_frqu_dscr = used_frqu_dscr;
        }

        public String getUsed_mtd() {
            return used_mtd;
        }

        public void setUsed_mtd(String used_mtd) {
            this.used_mtd = used_mtd;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getSpec() {
            return spec;
        }

        public void setSpec(String spec) {
            this.spec = spec;
        }
    }

    public static class CmPrescriptionBean {
        private String mdtrt_id;
        private String psn_no;
        private String dise_codg;
        private String rx_circ_flag;
        private String med_list_codg;
        private String medins_list_codg;
        private String det_item_fee_sumamt;
        private String cnt;//重量
        private String pric;
        private String prd_days;
        private String bilg_dept_codg;
        private String bilg_dept_name;
        private String bilg_dr_codg;
        private String bilg_dr_name;
        private String hosp_appr_flag;

        private String unt;//药品数量

        public CmPrescriptionBean(String mdtrt_id, String psn_no, String dise_codg, String rx_circ_flag, String med_list_codg,
                                  String medins_list_codg, String det_item_fee_sumamt, String cnt, String pric, String prd_days,
                                  String bilg_dept_codg, String bilg_dept_name, String bilg_dr_codg, String bilg_dr_name,
                                  String hosp_appr_flag, String unt) {
            this.mdtrt_id = mdtrt_id;
            this.psn_no = psn_no;
            this.dise_codg = dise_codg;
            this.rx_circ_flag = rx_circ_flag;
            this.med_list_codg = med_list_codg;
            this.medins_list_codg = medins_list_codg;
            this.det_item_fee_sumamt = det_item_fee_sumamt;
            this.cnt = cnt;
            this.pric = pric;
            this.prd_days = prd_days;
            this.bilg_dept_codg = bilg_dept_codg;
            this.bilg_dept_name = bilg_dept_name;
            this.bilg_dr_codg = bilg_dr_codg;
            this.bilg_dr_name = bilg_dr_name;
            this.hosp_appr_flag = hosp_appr_flag;
            this.unt = unt;
        }

        public String getMdtrt_id() {
            return mdtrt_id;
        }

        public void setMdtrt_id(String mdtrt_id) {
            this.mdtrt_id = mdtrt_id;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getDise_codg() {
            return dise_codg;
        }

        public void setDise_codg(String dise_codg) {
            this.dise_codg = dise_codg;
        }

        public String getRx_circ_flag() {
            return rx_circ_flag;
        }

        public void setRx_circ_flag(String rx_circ_flag) {
            this.rx_circ_flag = rx_circ_flag;
        }

        public String getMed_list_codg() {
            return med_list_codg;
        }

        public void setMed_list_codg(String med_list_codg) {
            this.med_list_codg = med_list_codg;
        }

        public String getMedins_list_codg() {
            return medins_list_codg;
        }

        public void setMedins_list_codg(String medins_list_codg) {
            this.medins_list_codg = medins_list_codg;
        }

        public String getDet_item_fee_sumamt() {
            return det_item_fee_sumamt;
        }

        public void setDet_item_fee_sumamt(String det_item_fee_sumamt) {
            this.det_item_fee_sumamt = det_item_fee_sumamt;
        }

        public String getCnt() {
            return cnt;
        }

        public void setCnt(String cnt) {
            this.cnt = cnt;
        }

        public String getPric() {
            return pric;
        }

        public void setPric(String pric) {
            this.pric = pric;
        }

        public String getPrd_days() {
            return prd_days;
        }

        public void setPrd_days(String prd_days) {
            this.prd_days = prd_days;
        }

        public String getBilg_dept_codg() {
            return bilg_dept_codg;
        }

        public void setBilg_dept_codg(String bilg_dept_codg) {
            this.bilg_dept_codg = bilg_dept_codg;
        }

        public String getBilg_dept_name() {
            return bilg_dept_name;
        }

        public void setBilg_dept_name(String bilg_dept_name) {
            this.bilg_dept_name = bilg_dept_name;
        }

        public String getBilg_dr_codg() {
            return bilg_dr_codg;
        }

        public void setBilg_dr_codg(String bilg_dr_codg) {
            this.bilg_dr_codg = bilg_dr_codg;
        }

        public String getBilg_dr_name() {
            return bilg_dr_name;
        }

        public void setBilg_dr_name(String bilg_dr_name) {
            this.bilg_dr_name = bilg_dr_name;
        }

        public String getHosp_appr_flag() {
            return hosp_appr_flag;
        }

        public void setHosp_appr_flag(String hosp_appr_flag) {
            this.hosp_appr_flag = hosp_appr_flag;
        }

        public String getUnt() {
            return unt;
        }

        public void setUnt(String unt) {
            this.unt = unt;
        }
    }

    public static class FeedetailBean {
        private String mdtrt_id;
        private String psn_no;
        private String dise_codg;
        private String rx_circ_flag;
        private String med_list_codg;
        private String medins_list_codg;
        private String det_item_fee_sumamt;
        private String cnt;
        private String pric;
        private String prd_days;
        private String bilg_dept_codg;
        private String bilg_dept_name;
        private String bilg_dr_codg;
        private String bilg_dr_name;
        private String hosp_appr_flag;

        private String remark;//备注

        public FeedetailBean(String mdtrt_id, String psn_no, String dise_codg, String rx_circ_flag, String med_list_codg, String medins_list_codg,
                             String det_item_fee_sumamt, String cnt, String pric, String prd_days, String bilg_dept_codg, String bilg_dept_name, String bilg_dr_codg,
                             String bilg_dr_name, String hosp_appr_flag, String remark) {
            this.mdtrt_id = mdtrt_id;
            this.psn_no = psn_no;
            this.dise_codg = dise_codg;
            this.rx_circ_flag = rx_circ_flag;
            this.med_list_codg = med_list_codg;
            this.medins_list_codg = medins_list_codg;
            this.det_item_fee_sumamt = det_item_fee_sumamt;
            this.cnt = cnt;
            this.pric = pric;
            this.prd_days = prd_days;
            this.bilg_dept_codg = bilg_dept_codg;
            this.bilg_dept_name = bilg_dept_name;
            this.bilg_dr_codg = bilg_dr_codg;
            this.bilg_dr_name = bilg_dr_name;
            this.hosp_appr_flag = hosp_appr_flag;
            this.remark = remark;
        }

        public String getMdtrt_id() {
            return mdtrt_id;
        }

        public void setMdtrt_id(String mdtrt_id) {
            this.mdtrt_id = mdtrt_id;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getDise_codg() {
            return dise_codg;
        }

        public void setDise_codg(String dise_codg) {
            this.dise_codg = dise_codg;
        }

        public String getRx_circ_flag() {
            return rx_circ_flag;
        }

        public void setRx_circ_flag(String rx_circ_flag) {
            this.rx_circ_flag = rx_circ_flag;
        }

        public String getMed_list_codg() {
            return med_list_codg;
        }

        public void setMed_list_codg(String med_list_codg) {
            this.med_list_codg = med_list_codg;
        }

        public String getMedins_list_codg() {
            return medins_list_codg;
        }

        public void setMedins_list_codg(String medins_list_codg) {
            this.medins_list_codg = medins_list_codg;
        }

        public String getDet_item_fee_sumamt() {
            return det_item_fee_sumamt;
        }

        public void setDet_item_fee_sumamt(String det_item_fee_sumamt) {
            this.det_item_fee_sumamt = det_item_fee_sumamt;
        }

        public String getCnt() {
            return cnt;
        }

        public void setCnt(String cnt) {
            this.cnt = cnt;
        }

        public String getPric() {
            return pric;
        }

        public void setPric(String pric) {
            this.pric = pric;
        }

        public String getPrd_days() {
            return prd_days;
        }

        public void setPrd_days(String prd_days) {
            this.prd_days = prd_days;
        }

        public String getBilg_dept_codg() {
            return bilg_dept_codg;
        }

        public void setBilg_dept_codg(String bilg_dept_codg) {
            this.bilg_dept_codg = bilg_dept_codg;
        }

        public String getBilg_dept_name() {
            return bilg_dept_name;
        }

        public void setBilg_dept_name(String bilg_dept_name) {
            this.bilg_dept_name = bilg_dept_name;
        }

        public String getBilg_dr_codg() {
            return bilg_dr_codg;
        }

        public void setBilg_dr_codg(String bilg_dr_codg) {
            this.bilg_dr_codg = bilg_dr_codg;
        }

        public String getBilg_dr_name() {
            return bilg_dr_name;
        }

        public void setBilg_dr_name(String bilg_dr_name) {
            this.bilg_dr_name = bilg_dr_name;
        }

        public String getHosp_appr_flag() {
            return hosp_appr_flag;
        }

        public void setHosp_appr_flag(String hosp_appr_flag) {
            this.hosp_appr_flag = hosp_appr_flag;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }
    }
}
