package com.jrdz.zhyb_android.ui.settlement.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/29
 * 描    述：
 * ================================================
 */
public class NetPrescrListModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-01-28 11:21:22
     * data : [{"OutpatientRregistrationId":3,"ipt_otp_no":"20220127201033001","mdtrt_id":"148300004","psn_no":"61000006000000000010866022","dept_name":"全科医疗科10","dept_code":"1","dr_name":"王一","atddr_no":"001","insutype":"310","begntime":"2022-01-27 20:10:33","mdtrt_cert_type":"02","mdtrt_cert_no":"612726196609210011","caty":"A02","Status":1}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * ipt_otp_no : 202205171112204912
         * psn_name : 宋怀春
         * sex : 1
         * age : 55.3
         * ElectronicPrescriptionId : 18
         * mdtrt_id : 411000007
         * psn_no : 61000006000000000010866022
         * fixmedins_code : H61080200249
         * CreateDT : 2022-05-17 11:12:53
         * UpdateDT : 2022-05-17 11:12:53
         * CreateUser : 001
         * UpdateUser : 001
         * settlementClassification : 1
         * electronicPrescriptionNo : 202205171112538845
         * PrescriptionType : 2
         * wmcpmDiagResults :
         * wmcpmAmount : 0
         * cmDiagResults :
         * cmDrugNum : 0
         * cmused_mtd :
         * cmAmount : 0
         * cmRemark :
         * unt : 3
         */

        private String ipt_otp_no;
        private String psn_name;
        private String sex;
        private String age;
        private String ElectronicPrescriptionId;
        private String mdtrt_id;
        private String psn_no;
        private String fixmedins_code;
        private String CreateDT;
        private String UpdateDT;
        private String CreateUser;
        private String UpdateUser;
        private String settlementClassification;
        private String electronicPrescriptionNo;
        private String PrescriptionType;
        private String wmcpmDiagResults;
        private String wmcpmAmount;
        private String cmDiagResults;
        private String cmDrugNum;
        private String cmused_mtd;
        private String cmAmount;
        private String cmRemark;
        private String unt;
        private String AccessoryUrl;
        private String StatusName;
        private String OrderNo;
        //本地数据
        public boolean choose;

        public String getIpt_otp_no() {
            return ipt_otp_no;
        }

        public void setIpt_otp_no(String ipt_otp_no) {
            this.ipt_otp_no = ipt_otp_no;
        }

        public String getPsn_name() {
            return psn_name;
        }

        public void setPsn_name(String psn_name) {
            this.psn_name = psn_name;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getElectronicPrescriptionId() {
            return ElectronicPrescriptionId;
        }

        public void setElectronicPrescriptionId(String ElectronicPrescriptionId) {
            this.ElectronicPrescriptionId = ElectronicPrescriptionId;
        }

        public String getMdtrt_id() {
            return mdtrt_id;
        }

        public void setMdtrt_id(String mdtrt_id) {
            this.mdtrt_id = mdtrt_id;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getUpdateDT() {
            return UpdateDT;
        }

        public void setUpdateDT(String UpdateDT) {
            this.UpdateDT = UpdateDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String CreateUser) {
            this.CreateUser = CreateUser;
        }

        public String getUpdateUser() {
            return UpdateUser;
        }

        public void setUpdateUser(String UpdateUser) {
            this.UpdateUser = UpdateUser;
        }

        public String getSettlementClassification() {
            return settlementClassification;
        }

        public void setSettlementClassification(String settlementClassification) {
            this.settlementClassification = settlementClassification;
        }

        public String getElectronicPrescriptionNo() {
            return electronicPrescriptionNo;
        }

        public void setElectronicPrescriptionNo(String electronicPrescriptionNo) {
            this.electronicPrescriptionNo = electronicPrescriptionNo;
        }

        public String getPrescriptionType() {
            return PrescriptionType;
        }

        public void setPrescriptionType(String PrescriptionType) {
            this.PrescriptionType = PrescriptionType;
        }

        public String getWmcpmDiagResults() {
            return wmcpmDiagResults;
        }

        public void setWmcpmDiagResults(String wmcpmDiagResults) {
            this.wmcpmDiagResults = wmcpmDiagResults;
        }

        public String getWmcpmAmount() {
            return wmcpmAmount;
        }

        public void setWmcpmAmount(String wmcpmAmount) {
            this.wmcpmAmount = wmcpmAmount;
        }

        public String getCmDiagResults() {
            return cmDiagResults;
        }

        public void setCmDiagResults(String cmDiagResults) {
            this.cmDiagResults = cmDiagResults;
        }

        public String getCmDrugNum() {
            return cmDrugNum;
        }

        public void setCmDrugNum(String cmDrugNum) {
            this.cmDrugNum = cmDrugNum;
        }

        public String getCmused_mtd() {
            return cmused_mtd;
        }

        public void setCmused_mtd(String cmused_mtd) {
            this.cmused_mtd = cmused_mtd;
        }

        public String getCmAmount() {
            return cmAmount;
        }

        public void setCmAmount(String cmAmount) {
            this.cmAmount = cmAmount;
        }

        public String getCmRemark() {
            return cmRemark;
        }

        public void setCmRemark(String cmRemark) {
            this.cmRemark = cmRemark;
        }

        public String getUnt() {
            return unt;
        }

        public void setUnt(String unt) {
            this.unt = unt;
        }

        public String getAccessoryUrl() {
            return AccessoryUrl;
        }

        public void setAccessoryUrl(String accessoryUrl) {
            AccessoryUrl = accessoryUrl;
        }

        public String getStatusName() {
            return StatusName;
        }

        public void setStatusName(String statusName) {
            StatusName = statusName;
        }

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String orderNo) {
            OrderNo = orderNo;
        }

        public boolean isChoose() {
            return choose;
        }

        public void setChoose(boolean choose) {
            this.choose = choose;
        }
    }

    //门诊日志列表
    public static void sendOutpatPrescrListRequest(final String TAG, String key, String begntime, String pageindex, String pagesize,
                                                   final CustomerJsonCallBack<NetPrescrListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("key", key);
        jsonObject.put("begntime", begntime);
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STORE_ELECTRONICPRESCRIPTIONLIST_URL, jsonObject.toJSONString(), callback);
    }
}
