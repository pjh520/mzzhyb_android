package com.jrdz.zhyb_android.ui.settlement.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/22
 * 描    述：
 * ================================================
 */
public class OutpatFeelistUpModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-01-29 14:41:00
     * data : [{"bas_medn_flag":"0","chld_medc_flag":null,"chrgitm_lv":"03","cnt":1,"det_item_fee_sumamt":0.01,"drt_reim_flag":"0","feedetl_sn":"102","fulamt_ownpay_amt":0.01,"hi_nego_drug_flag":"0","inscp_scp_amt":0,"list_sp_item_flag":null,"lmt_used_flag":"0","med_chrgitm_type":"09","memo":null,"overlmt_amt":0,"preselfpay_amt":0,"pric":0.01,"pric_uplmt_amt":999999,"selfpay_prop":1}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String chrg_bchno;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getChrg_bchno() {
        return chrg_bchno;
    }

    public void setChrg_bchno(String chrg_bchno) {
        this.chrg_bchno = chrg_bchno;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * bas_medn_flag : 0
         * chld_medc_flag : null
         * chrgitm_lv : 03
         * cnt : 1
         * det_item_fee_sumamt : 0.01
         * drt_reim_flag : 0
         * feedetl_sn : 102
         * fulamt_ownpay_amt : 0.01
         * hi_nego_drug_flag : 0
         * inscp_scp_amt : 0
         * list_sp_item_flag : null
         * lmt_used_flag : 0
         * med_chrgitm_type : 09
         * memo : null
         * overlmt_amt : 0
         * preselfpay_amt : 0
         * pric : 0.01
         * pric_uplmt_amt : 999999
         * selfpay_prop : 1
         */

        private String bas_medn_flag;
        private String chld_medc_flag;
        private String chrgitm_lv;
        private String cnt;
        private String det_item_fee_sumamt;
        private String drt_reim_flag;
        private String feedetl_sn;
        private String fulamt_ownpay_amt;
        private String hi_nego_drug_flag;
        private String inscp_scp_amt;
        private String list_sp_item_flag;
        private String lmt_used_flag;
        private String med_chrgitm_type;
        private String memo;
        private String overlmt_amt;
        private String preselfpay_amt;
        private String pric;
        private String pric_uplmt_amt;
        private String selfpay_prop;

        public String getBas_medn_flag() {
            return bas_medn_flag;
        }

        public void setBas_medn_flag(String bas_medn_flag) {
            this.bas_medn_flag = bas_medn_flag;
        }

        public String getChld_medc_flag() {
            return chld_medc_flag;
        }

        public void setChld_medc_flag(String chld_medc_flag) {
            this.chld_medc_flag = chld_medc_flag;
        }

        public String getChrgitm_lv() {
            return chrgitm_lv;
        }

        public void setChrgitm_lv(String chrgitm_lv) {
            this.chrgitm_lv = chrgitm_lv;
        }

        public String getCnt() {
            return cnt;
        }

        public void setCnt(String cnt) {
            this.cnt = cnt;
        }

        public String getDet_item_fee_sumamt() {
            return det_item_fee_sumamt;
        }

        public void setDet_item_fee_sumamt(String det_item_fee_sumamt) {
            this.det_item_fee_sumamt = det_item_fee_sumamt;
        }

        public String getDrt_reim_flag() {
            return drt_reim_flag;
        }

        public void setDrt_reim_flag(String drt_reim_flag) {
            this.drt_reim_flag = drt_reim_flag;
        }

        public String getFeedetl_sn() {
            return feedetl_sn;
        }

        public void setFeedetl_sn(String feedetl_sn) {
            this.feedetl_sn = feedetl_sn;
        }

        public String getFulamt_ownpay_amt() {
            return fulamt_ownpay_amt;
        }

        public void setFulamt_ownpay_amt(String fulamt_ownpay_amt) {
            this.fulamt_ownpay_amt = fulamt_ownpay_amt;
        }

        public String getHi_nego_drug_flag() {
            return hi_nego_drug_flag;
        }

        public void setHi_nego_drug_flag(String hi_nego_drug_flag) {
            this.hi_nego_drug_flag = hi_nego_drug_flag;
        }

        public String getInscp_scp_amt() {
            return inscp_scp_amt;
        }

        public void setInscp_scp_amt(String inscp_scp_amt) {
            this.inscp_scp_amt = inscp_scp_amt;
        }

        public String getList_sp_item_flag() {
            return list_sp_item_flag;
        }

        public void setList_sp_item_flag(String list_sp_item_flag) {
            this.list_sp_item_flag = list_sp_item_flag;
        }

        public String getLmt_used_flag() {
            return lmt_used_flag;
        }

        public void setLmt_used_flag(String lmt_used_flag) {
            this.lmt_used_flag = lmt_used_flag;
        }

        public String getMed_chrgitm_type() {
            return med_chrgitm_type;
        }

        public void setMed_chrgitm_type(String med_chrgitm_type) {
            this.med_chrgitm_type = med_chrgitm_type;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        public String getOverlmt_amt() {
            return overlmt_amt;
        }

        public void setOverlmt_amt(String overlmt_amt) {
            this.overlmt_amt = overlmt_amt;
        }

        public String getPreselfpay_amt() {
            return preselfpay_amt;
        }

        public void setPreselfpay_amt(String preselfpay_amt) {
            this.preselfpay_amt = preselfpay_amt;
        }

        public String getPric() {
            return pric;
        }

        public void setPric(String pric) {
            this.pric = pric;
        }

        public String getPric_uplmt_amt() {
            return pric_uplmt_amt;
        }

        public void setPric_uplmt_amt(String pric_uplmt_amt) {
            this.pric_uplmt_amt = pric_uplmt_amt;
        }

        public String getSelfpay_prop() {
            return selfpay_prop;
        }

        public void setSelfpay_prop(String selfpay_prop) {
            this.selfpay_prop = selfpay_prop;
        }
    }

    //门诊明细上传
    public static void sendOutpatFeelistUpRequest(final String TAG, String dataParma,String insuplc_admdvs,final CustomerJsonCallBack<OutpatFeelistUpModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_OUTPATIENTFEELISTUP_URL, dataParma,insuplc_admdvs, callback);
    }
}
