package com.jrdz.zhyb_android.ui.settlement.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-05-13
 * 描    述：
 * ================================================
 */
public class OutpatLogDetailModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-05-17 16:40:38
     * data : {"MdtrtinfoId":246,"mdtrt_id":"411000007","psn_no":"61000006000000000010866022","med_type":"11","begntime":"2022-05-17 11:12:32","main_cond_dscr":"主要病情描述","dise_codg":"A00.000x001","dise_name":"古典生物型霍乱","birctrl_type":"","birctrl_matn_date":"","fixmedins_code":"H61080200249","CreateDT":"2022-05-17 11:12:32","UpdateDT":"2022-05-17 11:12:32","CreateUser":"001","UpdateUser":"001","settlementClassification":1}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * psn_name : 宋怀春
         * dr_name : 新医一
         * mdtrt_cert_no : 612726196609210011
         * sex : 1
         * age : 55.3
         * ouseholder :
         * occupation : 农民
         * address : 莆田市吼吼吼吼吼
         * contact : 15059005800
         * onsetdate : 2022-05-17
         * remark : 备注
         * isInitialDiagnosis : 1
         * MdtrtinfoId : 246
         * mdtrt_id : 411000007
         * psn_no : 61000006000000000010866022
         * med_type : 11
         * begntime : 2022-05-17 11:12:32
         * main_cond_dscr : 主要病情描述
         * dise_codg : A00.000x001
         * dise_name : 古典生物型霍乱
         * birctrl_type :
         * birctrl_matn_date :
         * fixmedins_code : H61080200249
         * CreateDT : 2022-05-17 11:12:32
         * UpdateDT : 2022-05-17 11:12:32
         * CreateUser : 001
         * UpdateUser : 001
         * settlementClassification : 1
         * ipt_otp_no : 202205171112204912
         */

        private String psn_name;
        private String dr_name;
        private String mdtrt_cert_no;
        private String sex;
        private String age;
        private String ouseholder;
        private String occupation;
        private String address;
        private String contact;
        private String onsetdate;
        private String remark;
        private String isInitialDiagnosis;
        private String MdtrtinfoId;
        private String mdtrt_id;
        private String psn_no;
        private String med_type;
        private String begntime;
        private String main_cond_dscr;
        private String dise_codg;
        private String dise_name;
        private String birctrl_type;
        private String birctrl_matn_date;
        private String fixmedins_code;
        private String CreateDT;
        private String UpdateDT;
        private String CreateUser;
        private String UpdateUser;
        private String settlementClassification;
        private String ipt_otp_no;
        private String DiagResults;

        public String getPsn_name() {
            return psn_name;
        }

        public void setPsn_name(String psn_name) {
            this.psn_name = psn_name;
        }

        public String getDr_name() {
            return dr_name;
        }

        public void setDr_name(String dr_name) {
            this.dr_name = dr_name;
        }

        public String getMdtrt_cert_no() {
            return mdtrt_cert_no;
        }

        public void setMdtrt_cert_no(String mdtrt_cert_no) {
            this.mdtrt_cert_no = mdtrt_cert_no;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getOuseholder() {
            return ouseholder;
        }

        public void setOuseholder(String ouseholder) {
            this.ouseholder = ouseholder;
        }

        public String getOccupation() {
            return occupation;
        }

        public void setOccupation(String occupation) {
            this.occupation = occupation;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getContact() {
            return contact;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }

        public String getOnsetdate() {
            return onsetdate;
        }

        public void setOnsetdate(String onsetdate) {
            this.onsetdate = onsetdate;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getIsInitialDiagnosis() {
            return isInitialDiagnosis;
        }

        public void setIsInitialDiagnosis(String isInitialDiagnosis) {
            this.isInitialDiagnosis = isInitialDiagnosis;
        }

        public String getMdtrtinfoId() {
            return MdtrtinfoId;
        }

        public void setMdtrtinfoId(String MdtrtinfoId) {
            this.MdtrtinfoId = MdtrtinfoId;
        }

        public String getMdtrt_id() {
            return mdtrt_id;
        }

        public void setMdtrt_id(String mdtrt_id) {
            this.mdtrt_id = mdtrt_id;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getMed_type() {
            return med_type;
        }

        public void setMed_type(String med_type) {
            this.med_type = med_type;
        }

        public String getBegntime() {
            return begntime;
        }

        public void setBegntime(String begntime) {
            this.begntime = begntime;
        }

        public String getMain_cond_dscr() {
            return main_cond_dscr;
        }

        public void setMain_cond_dscr(String main_cond_dscr) {
            this.main_cond_dscr = main_cond_dscr;
        }

        public String getDise_codg() {
            return dise_codg;
        }

        public void setDise_codg(String dise_codg) {
            this.dise_codg = dise_codg;
        }

        public String getDise_name() {
            return dise_name;
        }

        public void setDise_name(String dise_name) {
            this.dise_name = dise_name;
        }

        public String getBirctrl_type() {
            return birctrl_type;
        }

        public void setBirctrl_type(String birctrl_type) {
            this.birctrl_type = birctrl_type;
        }

        public String getBirctrl_matn_date() {
            return birctrl_matn_date;
        }

        public void setBirctrl_matn_date(String birctrl_matn_date) {
            this.birctrl_matn_date = birctrl_matn_date;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getUpdateDT() {
            return UpdateDT;
        }

        public void setUpdateDT(String UpdateDT) {
            this.UpdateDT = UpdateDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String CreateUser) {
            this.CreateUser = CreateUser;
        }

        public String getUpdateUser() {
            return UpdateUser;
        }

        public void setUpdateUser(String UpdateUser) {
            this.UpdateUser = UpdateUser;
        }

        public String getSettlementClassification() {
            return settlementClassification;
        }

        public void setSettlementClassification(String settlementClassification) {
            this.settlementClassification = settlementClassification;
        }

        public String getIpt_otp_no() {
            return ipt_otp_no;
        }

        public void setIpt_otp_no(String ipt_otp_no) {
            this.ipt_otp_no = ipt_otp_no;
        }

        public String getDiagResults() {
            return DiagResults;
        }

        public void setDiagResults(String diagResults) {
            DiagResults = diagResults;
        }
    }

    //门诊日志详情
    public static void sendOutpatLogDetailRequest(final String TAG, String MdtrtinfoId,final CustomerJsonCallBack<OutpatLogDetailModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("MdtrtinfoId", MdtrtinfoId);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_OUTPATIENTMDTRTINFODETAIL_URL, jsonObject.toJSONString(), callback);
    }
}
