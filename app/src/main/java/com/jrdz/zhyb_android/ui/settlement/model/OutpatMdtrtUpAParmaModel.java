package com.jrdz.zhyb_android.ui.settlement.model;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/22
 * 描    述：
 * ================================================
 */
public class OutpatMdtrtUpAParmaModel {
    private MdtrtinfoBean mdtrtinfo;

    private List<DiseinfoBean> diseinfo = new ArrayList<>();

    public MdtrtinfoBean getMdtrtinfo() {
        return mdtrtinfo;
    }

    public void setMdtrtinfo(MdtrtinfoBean mdtrtinfo) {
        this.mdtrtinfo = mdtrtinfo;
    }

    public List<DiseinfoBean> getDiseinfo() {
        return diseinfo;
    }

    public void setDiseinfo(List<DiseinfoBean> diseinfo) {
        this.diseinfo = diseinfo;
    }

    public static class MdtrtinfoBean {
        private String ipt_otp_no;
        private String mdtrt_id;
        private String psn_no;
        private String med_type;
        private String main_cond_dscr;
        private String dise_codg;
        private String dise_name;
        private String insuplc_admdvs;
        private String settlementClassification;

        public MdtrtinfoBean(String ipt_otp_no,String mdtrt_id, String psn_no, String med_type, String main_cond_dscr, String dise_codg, String dise_name,
                             String insuplc_admdvs,String settlementClassification) {
            this.ipt_otp_no = ipt_otp_no;
            this.mdtrt_id = mdtrt_id;
            this.psn_no = psn_no;
            this.med_type = med_type;
            this.main_cond_dscr = main_cond_dscr;
            this.dise_codg = dise_codg;
            this.dise_name = dise_name;
            this.insuplc_admdvs = insuplc_admdvs;
            this.settlementClassification = settlementClassification;
        }

        public String getIpt_otp_no() {
            return ipt_otp_no;
        }

        public void setIpt_otp_no(String ipt_otp_no) {
            this.ipt_otp_no = ipt_otp_no;
        }

        public String getMdtrt_id() {
            return mdtrt_id;
        }

        public void setMdtrt_id(String mdtrt_id) {
            this.mdtrt_id = mdtrt_id;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getMed_type() {
            return med_type;
        }

        public void setMed_type(String med_type) {
            this.med_type = med_type;
        }

        public String getMain_cond_dscr() {
            return main_cond_dscr;
        }

        public void setMain_cond_dscr(String main_cond_dscr) {
            this.main_cond_dscr = main_cond_dscr;
        }

        public String getDise_codg() {
            return dise_codg;
        }

        public void setDise_codg(String dise_codg) {
            this.dise_codg = dise_codg;
        }

        public String getDise_name() {
            return dise_name;
        }

        public void setDise_name(String dise_name) {
            this.dise_name = dise_name;
        }

        public String getInsuplc_admdvs() {
            return insuplc_admdvs;
        }

        public void setInsuplc_admdvs(String insuplc_admdvs) {
            this.insuplc_admdvs = insuplc_admdvs;
        }

        public String getSettlementClassification() {
            return settlementClassification;
        }

        public void setSettlementClassification(String settlementClassification) {
            this.settlementClassification = settlementClassification;
        }
    }

    public static class DiseinfoBean {
        private String diag_type;//诊断类别
        private String diag_srt_no;//诊断排序号
        private String diag_code;//诊断代码
        private String diag_name;//诊断名称
        private String diag_dept;//诊断科室
        private String dise_dor_no;//诊断医生编码
        private String dise_dor_name;//诊断医生姓名
        private String vali_flag;//有效标志

        public DiseinfoBean(String diag_type, String diag_srt_no, String diag_code, String diag_name, String diag_dept,
                            String dise_dor_no, String dise_dor_name, String vali_flag) {
            this.diag_type = diag_type;
            this.diag_srt_no = diag_srt_no;
            this.diag_code = diag_code;
            this.diag_name = diag_name;
            this.diag_dept = diag_dept;
            this.dise_dor_no = dise_dor_no;
            this.dise_dor_name = dise_dor_name;
            this.vali_flag = vali_flag;
        }

        public String getDiag_type() {
            return diag_type;
        }

        public void setDiag_type(String diag_type) {
            this.diag_type = diag_type;
        }

        public String getDiag_srt_no() {
            return diag_srt_no;
        }

        public void setDiag_srt_no(String diag_srt_no) {
            this.diag_srt_no = diag_srt_no;
        }

        public String getDiag_code() {
            return diag_code;
        }

        public void setDiag_code(String diag_code) {
            this.diag_code = diag_code;
        }

        public String getDiag_name() {
            return diag_name;
        }

        public void setDiag_name(String diag_name) {
            this.diag_name = diag_name;
        }

        public String getDiag_dept() {
            return diag_dept;
        }

        public void setDiag_dept(String diag_dept) {
            this.diag_dept = diag_dept;
        }

        public String getDise_dor_no() {
            return dise_dor_no;
        }

        public void setDise_dor_no(String dise_dor_no) {
            this.dise_dor_no = dise_dor_no;
        }

        public String getDise_dor_name() {
            return dise_dor_name;
        }

        public void setDise_dor_name(String dise_dor_name) {
            this.dise_dor_name = dise_dor_name;
        }

        public String getVali_flag() {
            return vali_flag;
        }

        public void setVali_flag(String vali_flag) {
            this.vali_flag = vali_flag;
        }
    }
}
