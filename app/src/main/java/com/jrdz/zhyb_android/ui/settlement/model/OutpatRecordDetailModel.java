package com.jrdz.zhyb_android.ui.settlement.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-05-13
 * 描    述：
 * ================================================
 */
public class OutpatRecordDetailModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-05-18 18:13:13
     * data : {"OutpatientRregistrations":{"dise_name":"古典生物型霍乱","main_cond_dscr":"主要病情描述","OutpatientRregistrationId":263,"ipt_otp_no":"202205171112204912","mdtrt_id":"411000007","psn_no":"61000006000000000010866022","dept_name":"全科医疗科","dept_code":"2001","dr_name":"新医一","atddr_no":"001","insutype":"310","begntime":"2022-05-17 11:12:20","mdtrt_cert_type":"02","mdtrt_cert_no":"612726196609210011","caty":"A02","Status":1,"fixmedins_code":"H61080200249","fixmedins_name":"榆林颈肩腰腿痛康复医院","CreateDT":"2022-05-17 11:12:20","UpdateDT":"2022-05-17 11:12:20","CreateUser":"001","UpdateUser":"001","psn_name":"宋怀春","sex":"1","age":"55.3","birthday":"1966-09-21","ouseholder":"","occupation":"农民","address":"莆田市吼吼吼吼吼","contact":"15059005800","onsetdate":"2022-05-17","remark":"备注","isInitialDiagnosis":1,"classification":1,"settlementClassification":1},"Feedetails":[{"FeedetailId":265,"ipt_otp_no":"202205171112204912","mdtrt_id":"411000007","feedetl_sn":"202205171112540200","psn_no":"61000006000000000010866022","chrg_bchno":"202205171112538845","dise_codg":"","rxno":"","rx_circ_flag":"0","fee_ocur_time":"2022-05-17 11:12:54","med_list_codg":"XJ01CAA040E001020103596","medins_list_codg":"XJ01CAA040E001020103596","det_item_fee_sumamt":"0.02","cnt":"1","pric":"0.02","sin_dos_dscr":"","used_frqu_dscr":"","prd_days":"2","medc_way_dscr":"","bilg_dept_codg":"2001","bilg_dept_name":"全科医疗科","bilg_dr_codg":"001","bilg_dr_name":"新医一","acord_dept_codg":"","acord_dept_name":"","orders_dr_code":"","orders_dr_name":"","hosp_appr_flag":"1","tcmdrug_used_way":"","etip_flag":"","etip_hosp_code":"","dscg_tkdrug_flag":"","matn_fee_flag":"","setl_id":"","medins_list_name":"","fixmedins_code":"H61080200249","CreateDT":"2022-05-17 11:12:54","UpdateDT":"2022-05-17 11:12:54","CreateUser":"001","UpdateUser":"001"}],"Diseinfos":[{"DiseinfoId":276,"ipt_otp_no":"202205171112204912","mdtrt_id":"411000007","diag_type":"1","diag_srt_no":"1","diag_code":"A00.000x001","diag_name":"古典生物型霍乱","diag_dept":"2001","dise_dor_no":"001","dise_dor_name":"新医一","diag_time":"2022-05-17 11:12:32","vali_flag":"1","fixmedins_code":"H61080200249"}]}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * OutpatientRregistrations : {"dise_name":"古典生物型霍乱","main_cond_dscr":"主要病情描述","OutpatientRregistrationId":263,"ipt_otp_no":"202205171112204912","mdtrt_id":"411000007","psn_no":"61000006000000000010866022","dept_name":"全科医疗科","dept_code":"2001","dr_name":"新医一","atddr_no":"001","insutype":"310","begntime":"2022-05-17 11:12:20","mdtrt_cert_type":"02","mdtrt_cert_no":"612726196609210011","caty":"A02","Status":1,"fixmedins_code":"H61080200249","fixmedins_name":"榆林颈肩腰腿痛康复医院","CreateDT":"2022-05-17 11:12:20","UpdateDT":"2022-05-17 11:12:20","CreateUser":"001","UpdateUser":"001","psn_name":"宋怀春","sex":"1","age":"55.3","birthday":"1966-09-21","ouseholder":"","occupation":"农民","address":"莆田市吼吼吼吼吼","contact":"15059005800","onsetdate":"2022-05-17","remark":"备注","isInitialDiagnosis":1,"classification":1,"settlementClassification":1}
         * Feedetails : [{"FeedetailId":265,"ipt_otp_no":"202205171112204912","mdtrt_id":"411000007","feedetl_sn":"202205171112540200","psn_no":"61000006000000000010866022","chrg_bchno":"202205171112538845","dise_codg":"","rxno":"","rx_circ_flag":"0","fee_ocur_time":"2022-05-17 11:12:54","med_list_codg":"XJ01CAA040E001020103596","medins_list_codg":"XJ01CAA040E001020103596","det_item_fee_sumamt":"0.02","cnt":"1","pric":"0.02","sin_dos_dscr":"","used_frqu_dscr":"","prd_days":"2","medc_way_dscr":"","bilg_dept_codg":"2001","bilg_dept_name":"全科医疗科","bilg_dr_codg":"001","bilg_dr_name":"新医一","acord_dept_codg":"","acord_dept_name":"","orders_dr_code":"","orders_dr_name":"","hosp_appr_flag":"1","tcmdrug_used_way":"","etip_flag":"","etip_hosp_code":"","dscg_tkdrug_flag":"","matn_fee_flag":"","setl_id":"","medins_list_name":"","fixmedins_code":"H61080200249","CreateDT":"2022-05-17 11:12:54","UpdateDT":"2022-05-17 11:12:54","CreateUser":"001","UpdateUser":"001"}]
         * Diseinfos : [{"DiseinfoId":276,"ipt_otp_no":"202205171112204912","mdtrt_id":"411000007","diag_type":"1","diag_srt_no":"1","diag_code":"A00.000x001","diag_name":"古典生物型霍乱","diag_dept":"2001","dise_dor_no":"001","dise_dor_name":"新医一","diag_time":"2022-05-17 11:12:32","vali_flag":"1","fixmedins_code":"H61080200249"}]
         */

        private OutpatientRregistrationsBean OutpatientRregistrations;
        private List<FeedetailsBean> Feedetails;
        private List<DiseinfosBean> Diseinfos;

        public OutpatientRregistrationsBean getOutpatientRregistrations() {
            return OutpatientRregistrations;
        }

        public void setOutpatientRregistrations(OutpatientRregistrationsBean OutpatientRregistrations) {
            this.OutpatientRregistrations = OutpatientRregistrations;
        }

        public List<FeedetailsBean> getFeedetails() {
            return Feedetails;
        }

        public void setFeedetails(List<FeedetailsBean> Feedetails) {
            this.Feedetails = Feedetails;
        }

        public List<DiseinfosBean> getDiseinfos() {
            return Diseinfos;
        }

        public void setDiseinfos(List<DiseinfosBean> Diseinfos) {
            this.Diseinfos = Diseinfos;
        }

        public static class OutpatientRregistrationsBean {
            /**
             * dise_name : 古典生物型霍乱
             * main_cond_dscr : 主要病情描述
             * OutpatientRregistrationId : 263
             * ipt_otp_no : 202205171112204912
             * mdtrt_id : 411000007
             * psn_no : 61000006000000000010866022
             * dept_name : 全科医疗科
             * dept_code : 2001
             * dr_name : 新医一
             * atddr_no : 001
             * insutype : 310
             * begntime : 2022-05-17 11:12:20
             * mdtrt_cert_type : 02
             * mdtrt_cert_no : 612726196609210011
             * caty : A02
             * Status : 1
             * fixmedins_code : H61080200249
             * fixmedins_name : 榆林颈肩腰腿痛康复医院
             * CreateDT : 2022-05-17 11:12:20
             * UpdateDT : 2022-05-17 11:12:20
             * CreateUser : 001
             * UpdateUser : 001
             * psn_name : 宋怀春
             * sex : 1
             * age : 55.3
             * birthday : 1966-09-21
             * ouseholder :
             * occupation : 农民
             * address : 莆田市吼吼吼吼吼
             * contact : 15059005800
             * onsetdate : 2022-05-17
             * remark : 备注
             * isInitialDiagnosis : 1
             * classification : 1
             * settlementClassification : 1
             */

            private String dise_name;
            private String main_cond_dscr;
            private int OutpatientRregistrationId;
            private String ipt_otp_no;
            private String mdtrt_id;
            private String psn_no;
            private String dept_name;
            private String dept_code;
            private String dr_name;
            private String atddr_no;
            private String insutype;
            private String begntime;
            private String mdtrt_cert_type;
            private String mdtrt_cert_no;
            private String caty;
            private int Status;
            private String fixmedins_code;
            private String fixmedins_name;
            private String CreateDT;
            private String UpdateDT;
            private String CreateUser;
            private String UpdateUser;
            private String psn_name;
            private String sex;
            private String age;
            private String birthday;
            private String ouseholder;
            private String occupation;
            private String address;
            private String contact;
            private String onsetdate;
            private String remark;
            private int isInitialDiagnosis;
            private int classification;
            private int settlementClassification;
            private String prd_days;

            public String getDise_name() {
                return dise_name;
            }

            public void setDise_name(String dise_name) {
                this.dise_name = dise_name;
            }

            public String getMain_cond_dscr() {
                return main_cond_dscr;
            }

            public void setMain_cond_dscr(String main_cond_dscr) {
                this.main_cond_dscr = main_cond_dscr;
            }

            public int getOutpatientRregistrationId() {
                return OutpatientRregistrationId;
            }

            public void setOutpatientRregistrationId(int OutpatientRregistrationId) {
                this.OutpatientRregistrationId = OutpatientRregistrationId;
            }

            public String getIpt_otp_no() {
                return ipt_otp_no;
            }

            public void setIpt_otp_no(String ipt_otp_no) {
                this.ipt_otp_no = ipt_otp_no;
            }

            public String getMdtrt_id() {
                return mdtrt_id;
            }

            public void setMdtrt_id(String mdtrt_id) {
                this.mdtrt_id = mdtrt_id;
            }

            public String getPsn_no() {
                return psn_no;
            }

            public void setPsn_no(String psn_no) {
                this.psn_no = psn_no;
            }

            public String getDept_name() {
                return dept_name;
            }

            public void setDept_name(String dept_name) {
                this.dept_name = dept_name;
            }

            public String getDept_code() {
                return dept_code;
            }

            public void setDept_code(String dept_code) {
                this.dept_code = dept_code;
            }

            public String getDr_name() {
                return dr_name;
            }

            public void setDr_name(String dr_name) {
                this.dr_name = dr_name;
            }

            public String getAtddr_no() {
                return atddr_no;
            }

            public void setAtddr_no(String atddr_no) {
                this.atddr_no = atddr_no;
            }

            public String getInsutype() {
                return insutype;
            }

            public void setInsutype(String insutype) {
                this.insutype = insutype;
            }

            public String getBegntime() {
                return begntime;
            }

            public void setBegntime(String begntime) {
                this.begntime = begntime;
            }

            public String getMdtrt_cert_type() {
                return mdtrt_cert_type;
            }

            public void setMdtrt_cert_type(String mdtrt_cert_type) {
                this.mdtrt_cert_type = mdtrt_cert_type;
            }

            public String getMdtrt_cert_no() {
                return mdtrt_cert_no;
            }

            public void setMdtrt_cert_no(String mdtrt_cert_no) {
                this.mdtrt_cert_no = mdtrt_cert_no;
            }

            public String getCaty() {
                return caty;
            }

            public void setCaty(String caty) {
                this.caty = caty;
            }

            public int getStatus() {
                return Status;
            }

            public void setStatus(int Status) {
                this.Status = Status;
            }

            public String getFixmedins_code() {
                return fixmedins_code;
            }

            public void setFixmedins_code(String fixmedins_code) {
                this.fixmedins_code = fixmedins_code;
            }

            public String getFixmedins_name() {
                return fixmedins_name;
            }

            public void setFixmedins_name(String fixmedins_name) {
                this.fixmedins_name = fixmedins_name;
            }

            public String getCreateDT() {
                return CreateDT;
            }

            public void setCreateDT(String CreateDT) {
                this.CreateDT = CreateDT;
            }

            public String getUpdateDT() {
                return UpdateDT;
            }

            public void setUpdateDT(String UpdateDT) {
                this.UpdateDT = UpdateDT;
            }

            public String getCreateUser() {
                return CreateUser;
            }

            public void setCreateUser(String CreateUser) {
                this.CreateUser = CreateUser;
            }

            public String getUpdateUser() {
                return UpdateUser;
            }

            public void setUpdateUser(String UpdateUser) {
                this.UpdateUser = UpdateUser;
            }

            public String getPsn_name() {
                return psn_name;
            }

            public void setPsn_name(String psn_name) {
                this.psn_name = psn_name;
            }

            public String getSex() {
                return sex;
            }

            public void setSex(String sex) {
                this.sex = sex;
            }

            public String getAge() {
                return age;
            }

            public void setAge(String age) {
                this.age = age;
            }

            public String getBirthday() {
                return birthday;
            }

            public void setBirthday(String birthday) {
                this.birthday = birthday;
            }

            public String getOuseholder() {
                return ouseholder;
            }

            public void setOuseholder(String ouseholder) {
                this.ouseholder = ouseholder;
            }

            public String getOccupation() {
                return occupation;
            }

            public void setOccupation(String occupation) {
                this.occupation = occupation;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getContact() {
                return contact;
            }

            public void setContact(String contact) {
                this.contact = contact;
            }

            public String getOnsetdate() {
                return onsetdate;
            }

            public void setOnsetdate(String onsetdate) {
                this.onsetdate = onsetdate;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public int getIsInitialDiagnosis() {
                return isInitialDiagnosis;
            }

            public void setIsInitialDiagnosis(int isInitialDiagnosis) {
                this.isInitialDiagnosis = isInitialDiagnosis;
            }

            public int getClassification() {
                return classification;
            }

            public void setClassification(int classification) {
                this.classification = classification;
            }

            public int getSettlementClassification() {
                return settlementClassification;
            }

            public void setSettlementClassification(int settlementClassification) {
                this.settlementClassification = settlementClassification;
            }

            public String getPrd_days() {
                return prd_days;
            }

            public void setPrd_days(String prd_days) {
                this.prd_days = prd_days;
            }
        }

        public static class FeedetailsBean {
            /**
             * FeedetailId : 265
             * ipt_otp_no : 202205171112204912
             * mdtrt_id : 411000007
             * feedetl_sn : 202205171112540200
             * psn_no : 61000006000000000010866022
             * chrg_bchno : 202205171112538845
             * dise_codg :
             * rxno :
             * rx_circ_flag : 0
             * fee_ocur_time : 2022-05-17 11:12:54
             * med_list_codg : XJ01CAA040E001020103596
             * medins_list_codg : XJ01CAA040E001020103596
             * det_item_fee_sumamt : 0.02
             * cnt : 1
             * pric : 0.02
             * sin_dos_dscr :
             * used_frqu_dscr :
             * prd_days : 2
             * medc_way_dscr :
             * bilg_dept_codg : 2001
             * bilg_dept_name : 全科医疗科
             * bilg_dr_codg : 001
             * bilg_dr_name : 新医一
             * acord_dept_codg :
             * acord_dept_name :
             * orders_dr_code :
             * orders_dr_name :
             * hosp_appr_flag : 1
             * tcmdrug_used_way :
             * etip_flag :
             * etip_hosp_code :
             * dscg_tkdrug_flag :
             * matn_fee_flag :
             * setl_id :
             * medins_list_name :
             * fixmedins_code : H61080200249
             * CreateDT : 2022-05-17 11:12:54
             * UpdateDT : 2022-05-17 11:12:54
             * CreateUser : 001
             * UpdateUser : 001
             */

            private int FeedetailId;
            private String ipt_otp_no;
            private String mdtrt_id;
            private String feedetl_sn;
            private String psn_no;
            private String chrg_bchno;
            private String dise_codg;
            private String rxno;
            private String rx_circ_flag;
            private String fee_ocur_time;
            private String med_list_codg;
            private String medins_list_codg;
            private String det_item_fee_sumamt;
            private String cnt;
            private String pric;
            private String sin_dos_dscr;
            private String used_frqu_dscr;
            private String prd_days;
            private String medc_way_dscr;
            private String bilg_dept_codg;
            private String bilg_dept_name;
            private String bilg_dr_codg;
            private String bilg_dr_name;
            private String acord_dept_codg;
            private String acord_dept_name;
            private String orders_dr_code;
            private String orders_dr_name;
            private String hosp_appr_flag;
            private String tcmdrug_used_way;
            private String etip_flag;
            private String etip_hosp_code;
            private String dscg_tkdrug_flag;
            private String matn_fee_flag;
            private String setl_id;
            private String medins_list_name;
            private String fixmedins_code;
            private String CreateDT;
            private String UpdateDT;
            private String CreateUser;
            private String UpdateUser;

            public int getFeedetailId() {
                return FeedetailId;
            }

            public void setFeedetailId(int FeedetailId) {
                this.FeedetailId = FeedetailId;
            }

            public String getIpt_otp_no() {
                return ipt_otp_no;
            }

            public void setIpt_otp_no(String ipt_otp_no) {
                this.ipt_otp_no = ipt_otp_no;
            }

            public String getMdtrt_id() {
                return mdtrt_id;
            }

            public void setMdtrt_id(String mdtrt_id) {
                this.mdtrt_id = mdtrt_id;
            }

            public String getFeedetl_sn() {
                return feedetl_sn;
            }

            public void setFeedetl_sn(String feedetl_sn) {
                this.feedetl_sn = feedetl_sn;
            }

            public String getPsn_no() {
                return psn_no;
            }

            public void setPsn_no(String psn_no) {
                this.psn_no = psn_no;
            }

            public String getChrg_bchno() {
                return chrg_bchno;
            }

            public void setChrg_bchno(String chrg_bchno) {
                this.chrg_bchno = chrg_bchno;
            }

            public String getDise_codg() {
                return dise_codg;
            }

            public void setDise_codg(String dise_codg) {
                this.dise_codg = dise_codg;
            }

            public String getRxno() {
                return rxno;
            }

            public void setRxno(String rxno) {
                this.rxno = rxno;
            }

            public String getRx_circ_flag() {
                return rx_circ_flag;
            }

            public void setRx_circ_flag(String rx_circ_flag) {
                this.rx_circ_flag = rx_circ_flag;
            }

            public String getFee_ocur_time() {
                return fee_ocur_time;
            }

            public void setFee_ocur_time(String fee_ocur_time) {
                this.fee_ocur_time = fee_ocur_time;
            }

            public String getMed_list_codg() {
                return med_list_codg;
            }

            public void setMed_list_codg(String med_list_codg) {
                this.med_list_codg = med_list_codg;
            }

            public String getMedins_list_codg() {
                return medins_list_codg;
            }

            public void setMedins_list_codg(String medins_list_codg) {
                this.medins_list_codg = medins_list_codg;
            }

            public String getDet_item_fee_sumamt() {
                return det_item_fee_sumamt;
            }

            public void setDet_item_fee_sumamt(String det_item_fee_sumamt) {
                this.det_item_fee_sumamt = det_item_fee_sumamt;
            }

            public String getCnt() {
                return cnt;
            }

            public void setCnt(String cnt) {
                this.cnt = cnt;
            }

            public String getPric() {
                return pric;
            }

            public void setPric(String pric) {
                this.pric = pric;
            }

            public String getSin_dos_dscr() {
                return sin_dos_dscr;
            }

            public void setSin_dos_dscr(String sin_dos_dscr) {
                this.sin_dos_dscr = sin_dos_dscr;
            }

            public String getUsed_frqu_dscr() {
                return used_frqu_dscr;
            }

            public void setUsed_frqu_dscr(String used_frqu_dscr) {
                this.used_frqu_dscr = used_frqu_dscr;
            }

            public String getPrd_days() {
                return prd_days;
            }

            public void setPrd_days(String prd_days) {
                this.prd_days = prd_days;
            }

            public String getMedc_way_dscr() {
                return medc_way_dscr;
            }

            public void setMedc_way_dscr(String medc_way_dscr) {
                this.medc_way_dscr = medc_way_dscr;
            }

            public String getBilg_dept_codg() {
                return bilg_dept_codg;
            }

            public void setBilg_dept_codg(String bilg_dept_codg) {
                this.bilg_dept_codg = bilg_dept_codg;
            }

            public String getBilg_dept_name() {
                return bilg_dept_name;
            }

            public void setBilg_dept_name(String bilg_dept_name) {
                this.bilg_dept_name = bilg_dept_name;
            }

            public String getBilg_dr_codg() {
                return bilg_dr_codg;
            }

            public void setBilg_dr_codg(String bilg_dr_codg) {
                this.bilg_dr_codg = bilg_dr_codg;
            }

            public String getBilg_dr_name() {
                return bilg_dr_name;
            }

            public void setBilg_dr_name(String bilg_dr_name) {
                this.bilg_dr_name = bilg_dr_name;
            }

            public String getAcord_dept_codg() {
                return acord_dept_codg;
            }

            public void setAcord_dept_codg(String acord_dept_codg) {
                this.acord_dept_codg = acord_dept_codg;
            }

            public String getAcord_dept_name() {
                return acord_dept_name;
            }

            public void setAcord_dept_name(String acord_dept_name) {
                this.acord_dept_name = acord_dept_name;
            }

            public String getOrders_dr_code() {
                return orders_dr_code;
            }

            public void setOrders_dr_code(String orders_dr_code) {
                this.orders_dr_code = orders_dr_code;
            }

            public String getOrders_dr_name() {
                return orders_dr_name;
            }

            public void setOrders_dr_name(String orders_dr_name) {
                this.orders_dr_name = orders_dr_name;
            }

            public String getHosp_appr_flag() {
                return hosp_appr_flag;
            }

            public void setHosp_appr_flag(String hosp_appr_flag) {
                this.hosp_appr_flag = hosp_appr_flag;
            }

            public String getTcmdrug_used_way() {
                return tcmdrug_used_way;
            }

            public void setTcmdrug_used_way(String tcmdrug_used_way) {
                this.tcmdrug_used_way = tcmdrug_used_way;
            }

            public String getEtip_flag() {
                return etip_flag;
            }

            public void setEtip_flag(String etip_flag) {
                this.etip_flag = etip_flag;
            }

            public String getEtip_hosp_code() {
                return etip_hosp_code;
            }

            public void setEtip_hosp_code(String etip_hosp_code) {
                this.etip_hosp_code = etip_hosp_code;
            }

            public String getDscg_tkdrug_flag() {
                return dscg_tkdrug_flag;
            }

            public void setDscg_tkdrug_flag(String dscg_tkdrug_flag) {
                this.dscg_tkdrug_flag = dscg_tkdrug_flag;
            }

            public String getMatn_fee_flag() {
                return matn_fee_flag;
            }

            public void setMatn_fee_flag(String matn_fee_flag) {
                this.matn_fee_flag = matn_fee_flag;
            }

            public String getSetl_id() {
                return setl_id;
            }

            public void setSetl_id(String setl_id) {
                this.setl_id = setl_id;
            }

            public String getMedins_list_name() {
                return medins_list_name;
            }

            public void setMedins_list_name(String medins_list_name) {
                this.medins_list_name = medins_list_name;
            }

            public String getFixmedins_code() {
                return fixmedins_code;
            }

            public void setFixmedins_code(String fixmedins_code) {
                this.fixmedins_code = fixmedins_code;
            }

            public String getCreateDT() {
                return CreateDT;
            }

            public void setCreateDT(String CreateDT) {
                this.CreateDT = CreateDT;
            }

            public String getUpdateDT() {
                return UpdateDT;
            }

            public void setUpdateDT(String UpdateDT) {
                this.UpdateDT = UpdateDT;
            }

            public String getCreateUser() {
                return CreateUser;
            }

            public void setCreateUser(String CreateUser) {
                this.CreateUser = CreateUser;
            }

            public String getUpdateUser() {
                return UpdateUser;
            }

            public void setUpdateUser(String UpdateUser) {
                this.UpdateUser = UpdateUser;
            }
        }

        public static class DiseinfosBean {
            /**
             * DiseinfoId : 276
             * ipt_otp_no : 202205171112204912
             * mdtrt_id : 411000007
             * diag_type : 1
             * diag_srt_no : 1
             * diag_code : A00.000x001
             * diag_name : 古典生物型霍乱
             * diag_dept : 2001
             * dise_dor_no : 001
             * dise_dor_name : 新医一
             * diag_time : 2022-05-17 11:12:32
             * vali_flag : 1
             * fixmedins_code : H61080200249
             */

            private int DiseinfoId;
            private String ipt_otp_no;
            private String mdtrt_id;
            private String diag_type;
            private String diag_srt_no;
            private String diag_code;
            private String diag_name;
            private String diag_dept;
            private String dise_dor_no;
            private String dise_dor_name;
            private String diag_time;
            private String vali_flag;
            private String fixmedins_code;

            public int getDiseinfoId() {
                return DiseinfoId;
            }

            public void setDiseinfoId(int DiseinfoId) {
                this.DiseinfoId = DiseinfoId;
            }

            public String getIpt_otp_no() {
                return ipt_otp_no;
            }

            public void setIpt_otp_no(String ipt_otp_no) {
                this.ipt_otp_no = ipt_otp_no;
            }

            public String getMdtrt_id() {
                return mdtrt_id;
            }

            public void setMdtrt_id(String mdtrt_id) {
                this.mdtrt_id = mdtrt_id;
            }

            public String getDiag_type() {
                return diag_type;
            }

            public void setDiag_type(String diag_type) {
                this.diag_type = diag_type;
            }

            public String getDiag_srt_no() {
                return diag_srt_no;
            }

            public void setDiag_srt_no(String diag_srt_no) {
                this.diag_srt_no = diag_srt_no;
            }

            public String getDiag_code() {
                return diag_code;
            }

            public void setDiag_code(String diag_code) {
                this.diag_code = diag_code;
            }

            public String getDiag_name() {
                return diag_name;
            }

            public void setDiag_name(String diag_name) {
                this.diag_name = diag_name;
            }

            public String getDiag_dept() {
                return diag_dept;
            }

            public void setDiag_dept(String diag_dept) {
                this.diag_dept = diag_dept;
            }

            public String getDise_dor_no() {
                return dise_dor_no;
            }

            public void setDise_dor_no(String dise_dor_no) {
                this.dise_dor_no = dise_dor_no;
            }

            public String getDise_dor_name() {
                return dise_dor_name;
            }

            public void setDise_dor_name(String dise_dor_name) {
                this.dise_dor_name = dise_dor_name;
            }

            public String getDiag_time() {
                return diag_time;
            }

            public void setDiag_time(String diag_time) {
                this.diag_time = diag_time;
            }

            public String getVali_flag() {
                return vali_flag;
            }

            public void setVali_flag(String vali_flag) {
                this.vali_flag = vali_flag;
            }

            public String getFixmedins_code() {
                return fixmedins_code;
            }

            public void setFixmedins_code(String fixmedins_code) {
                this.fixmedins_code = fixmedins_code;
            }
        }
    }


    //门诊记录详情
    public static void sendOutpatRecordDetailRequest(final String TAG, String OutpatientRregistrationId,final CustomerJsonCallBack<OutpatRecordDetailModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OutpatientRregistrationId", OutpatientRregistrationId);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_OUTPATIENTREGISTRATIONDETAIL_URL, jsonObject.toJSONString(), callback);
    }
}
