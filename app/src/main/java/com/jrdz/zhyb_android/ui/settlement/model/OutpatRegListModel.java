package com.jrdz.zhyb_android.ui.settlement.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/29
 * 描    述：
 * ================================================
 */
public class OutpatRegListModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-01-28 11:21:22
     * data : [{"OutpatientRregistrationId":3,"ipt_otp_no":"20220127201033001","mdtrt_id":"148300004","psn_no":"61000006000000000010866022","dept_name":"全科医疗科10","dept_code":"1","dr_name":"王一","atddr_no":"001","insutype":"310","begntime":"2022-01-27 20:10:33","mdtrt_cert_type":"02","mdtrt_cert_no":"612726196609210011","caty":"A02","Status":1}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * OutpatientRregistrationId : 3
         * ipt_otp_no : 20220127201033001
         * mdtrt_id : 148300004
         * psn_no : 61000006000000000010866022
         * dept_name : 全科医疗科10
         * dept_code : 1
         * dr_name : 王一
         * atddr_no : 001
         * insutype : 310
         * begntime : 2022-01-27 20:10:33
         * mdtrt_cert_type : 02
         * mdtrt_cert_no : 612726196609210011
         * caty : A02
         * Status : 1
         */

        private String OutpatientRregistrationId;
        private String ipt_otp_no;
        private String mdtrt_id;
        private String psn_no;
        private String dept_name;
        private String dept_code;
        private String dr_name;
        private String atddr_no;
        private String insutype;
        private String begntime;
        private String mdtrt_cert_type;
        private String mdtrt_cert_no;
        private String caty;
        private int Status;
        private String psn_name;
        private String sex;
        private String age;
        private String med_type;
        private String settlementClassification;
        private String address;
        private String contact;
        private int step;//1 挂号 2门诊就诊信息上传 3门诊明细上传 4结算成功 5门诊明细上传不结算
        //测试数据
        public boolean choose;

        public boolean isChoose() {
            return choose;
        }

        public void setChoose(boolean choose) {
            this.choose = choose;
        }

        public String getOutpatientRregistrationId() {
            return OutpatientRregistrationId;
        }

        public void setOutpatientRregistrationId(String OutpatientRregistrationId) {
            this.OutpatientRregistrationId = OutpatientRregistrationId;
        }

        public String getIpt_otp_no() {
            return ipt_otp_no;
        }

        public void setIpt_otp_no(String ipt_otp_no) {
            this.ipt_otp_no = ipt_otp_no;
        }

        public String getMdtrt_id() {
            return mdtrt_id;
        }

        public void setMdtrt_id(String mdtrt_id) {
            this.mdtrt_id = mdtrt_id;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getDept_name() {
            return dept_name;
        }

        public void setDept_name(String dept_name) {
            this.dept_name = dept_name;
        }

        public String getDept_code() {
            return dept_code;
        }

        public void setDept_code(String dept_code) {
            this.dept_code = dept_code;
        }

        public String getDr_name() {
            return dr_name;
        }

        public void setDr_name(String dr_name) {
            this.dr_name = dr_name;
        }

        public String getAtddr_no() {
            return atddr_no;
        }

        public void setAtddr_no(String atddr_no) {
            this.atddr_no = atddr_no;
        }

        public String getInsutype() {
            return insutype;
        }

        public void setInsutype(String insutype) {
            this.insutype = insutype;
        }

        public String getBegntime() {
            return begntime;
        }

        public void setBegntime(String begntime) {
            this.begntime = begntime;
        }

        public String getMdtrt_cert_type() {
            return mdtrt_cert_type;
        }

        public void setMdtrt_cert_type(String mdtrt_cert_type) {
            this.mdtrt_cert_type = mdtrt_cert_type;
        }

        public String getMdtrt_cert_no() {
            return mdtrt_cert_no;
        }

        public void setMdtrt_cert_no(String mdtrt_cert_no) {
            this.mdtrt_cert_no = mdtrt_cert_no;
        }

        public String getCaty() {
            return caty;
        }

        public void setCaty(String caty) {
            this.caty = caty;
        }

        public int getStatus() {
            return Status;
        }

        public void setStatus(int Status) {
            this.Status = Status;
        }

        public String getPsn_name() {
            return psn_name;
        }

        public void setPsn_name(String psn_name) {
            this.psn_name = psn_name;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getMed_type() {
            return med_type;
        }

        public void setMed_type(String med_type) {
            this.med_type = med_type;
        }

        public String getSettlementClassification() {
            return settlementClassification;
        }

        public void setSettlementClassification(String settlementClassification) {
            this.settlementClassification = settlementClassification;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getContact() {
            return contact;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }

        public int getStep() {
            return step;
        }

        public void setStep(int step) {
            this.step = step;
        }
    }

    //门诊挂号列表
    public static void sendOutpatRegListRequest(final String TAG, String key,String pageindex,String pagesize, final CustomerJsonCallBack<OutpatRegListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("key", key);
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_REGISTRATIONINFO_URL, jsonObject.toJSONString(), callback);
    }
}
