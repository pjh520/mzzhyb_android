package com.jrdz.zhyb_android.ui.settlement.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/22
 * 描    述：
 * ================================================
 */
public class OutpatRegModel {
    /**
     * code : 1
     * msg : 请求成功
     * obj : {"ipt_otp_no":"20211222112137","mdtrt_id":"4343006","psn_no":"61000006000000000010866022"}
     */

    private String code;
    private String msg;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * ipt_otp_no : 20211222112137
         * mdtrt_id : 4343006
         * psn_no : 61000006000000000010866022
         */
        private String ipt_otp_no;
        private String mdtrt_id;
        private String psn_no;

        public String getIpt_otp_no() {
            return ipt_otp_no;
        }

        public void setIpt_otp_no(String ipt_otp_no) {
            this.ipt_otp_no = ipt_otp_no;
        }

        public String getMdtrt_id() {
            return mdtrt_id;
        }

        public void setMdtrt_id(String mdtrt_id) {
            this.mdtrt_id = mdtrt_id;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }
    }

    //门诊挂号
    public static void sendOutpatRegRequest(final String TAG, String psn_no,String psn_name, String insutype, String mdtrt_cert_type,String mdtrt_cert_no,
                                            String atddr_no, String dr_name, String dept_code, String dept_name, String caty,String insuplc_admdvs,
                                            String sex, String age,String birthday,String ouseholder,String occupation, String address,String contact,
                                            String onsetdate,String remark,String isInitialDiagnosis, String classification,String settlementClassification,
                                            String med_type,final CustomerJsonCallBack<OutpatRegModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("psn_no", psn_no);
        jsonObject.put("psn_name", psn_name);
        jsonObject.put("insutype", insutype);
        jsonObject.put("mdtrt_cert_type", mdtrt_cert_type);
        jsonObject.put("mdtrt_cert_no", mdtrt_cert_no);
        jsonObject.put("atddr_no", atddr_no);//医师编码
        jsonObject.put("dr_name", dr_name);//医师姓名
        jsonObject.put("dept_code", dept_code);//科室编码
        jsonObject.put("dept_name", dept_name);//科室编码
        jsonObject.put("caty", caty);//科别

        jsonObject.put("sex", sex);//性别
        jsonObject.put("age", age);//年龄
        jsonObject.put("birthday", birthday);//出生日期
        jsonObject.put("ouseholder", ouseholder);//房主姓名
        jsonObject.put("occupation", occupation);//职业
        jsonObject.put("address", address);//居住地址
        jsonObject.put("contact", contact);//联系方式
        jsonObject.put("onsetdate", onsetdate);//发病日期
        jsonObject.put("remark", remark);//备注
        jsonObject.put("isInitialDiagnosis", isInitialDiagnosis);//1初诊2复诊
        jsonObject.put("classification", classification);//门诊分类 1、开启电子处方和门诊日志 2、电子处方 3、门诊日志 4、关闭电子处方和门诊日志
        jsonObject.put("settlementClassification", settlementClassification);//结算分类 1医保结算 2自费结算
        jsonObject.put("med_type", med_type);//医疗类别
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_OUTPATIENTRREGISTRATION_URL, jsonObject.toJSONString(), insuplc_admdvs,callback);
    }
}
