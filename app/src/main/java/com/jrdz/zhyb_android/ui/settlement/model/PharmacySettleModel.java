package com.jrdz.zhyb_android.ui.settlement.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;


/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/17
 * 描    述：
 * ================================================
 */
public class PharmacySettleModel {
    /**
     * code : 1
     * msg : 请求成功
     * obj : {"detlcutinfo":[{"bas_medn_flag":"0","chrgitm_lv":"01","cnt":1,"det_item_fee_sumamt":30,"drt_reim_flag":"0","feedetl_sn":"1639736152190","fulamt_ownpay_amt":0,"hi_nego_drug_flag":"0","inscp_scp_amt":30,"med_chrgitm_type":"11","overlmt_amt":0,"preselfpay_amt":0,"pric":30,"pric_uplmt_amt":999999,"selfpay_prop":0},{"bas_medn_flag":"0","chrgitm_lv":"03","cnt":1,"det_item_fee_sumamt":30,"drt_reim_flag":"0","feedetl_sn":"1639736152298","fulamt_ownpay_amt":30,"hi_nego_drug_flag":"0","inscp_scp_amt":0,"med_chrgitm_type":"09","overlmt_amt":0,"preselfpay_amt":0,"pric":30,"pric_uplmt_amt":999999,"selfpay_prop":1}],"setldetail":[{"fund_pay_type":"310200","fund_pay_type_name":"城镇职工基本医疗保险个人账户基金","fund_payamt":60,"inscp_scp_amt":30}],"setlinfo":{"acct_pay":60,"act_pay_dedc":0,"age":55,"balc":5906.9,"brdy":"1966-09-21","certno":"612726196609210011","clr_optins":"610825","clr_type":"41","clr_way":"1","cvlserv_flag":"1","cvlserv_pay":0,"fulamt_ownpay_amt":30,"fund_pay_sumamt":0,"gend":"1","hifes_pay":0,"hifmi_pay":0,"hifob_pay":0,"hifp_pay":0,"inscp_scp_amt":30,"insutype":"310","maf_pay":0,"mdtrt_cert_type":"02","mdtrt_id":"4165019","med_type":"41","medfee_sumamt":60,"medins_setl_id":"H61080200030202112171815521606","naty":"01","oth_pay":0,"overlmt_selfpay":0,"pool_prop_selfpay":0,"preselfpay_amt":0,"psn_cash_pay":0,"psn_cert_type":"01","psn_name":"宋怀春","psn_no":"61000006000000000010866022","psn_part_amt":60,"psn_type":"1101","setl_id":"3120018","setl_time":"2021-12-17 18:15:47"}}
     */

    private String code;
    private String msg;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * detlcutinfo : [{"bas_medn_flag":"0","chrgitm_lv":"01","cnt":1,"det_item_fee_sumamt":30,"drt_reim_flag":"0","feedetl_sn":"1639736152190","fulamt_ownpay_amt":0,"hi_nego_drug_flag":"0","inscp_scp_amt":30,"med_chrgitm_type":"11","overlmt_amt":0,"preselfpay_amt":0,"pric":30,"pric_uplmt_amt":999999,"selfpay_prop":0},{"bas_medn_flag":"0","chrgitm_lv":"03","cnt":1,"det_item_fee_sumamt":30,"drt_reim_flag":"0","feedetl_sn":"1639736152298","fulamt_ownpay_amt":30,"hi_nego_drug_flag":"0","inscp_scp_amt":0,"med_chrgitm_type":"09","overlmt_amt":0,"preselfpay_amt":0,"pric":30,"pric_uplmt_amt":999999,"selfpay_prop":1}]
         * setldetail : [{"fund_pay_type":"310200","fund_pay_type_name":"城镇职工基本医疗保险个人账户基金","fund_payamt":60,"inscp_scp_amt":30}]
         * setlinfo : {"acct_pay":60,"act_pay_dedc":0,"age":55,"balc":5906.9,"brdy":"1966-09-21","certno":"612726196609210011","clr_optins":"610825","clr_type":"41","clr_way":"1","cvlserv_flag":"1","cvlserv_pay":0,"fulamt_ownpay_amt":30,"fund_pay_sumamt":0,"gend":"1","hifes_pay":0,"hifmi_pay":0,"hifob_pay":0,"hifp_pay":0,"inscp_scp_amt":30,"insutype":"310","maf_pay":0,"mdtrt_cert_type":"02","mdtrt_id":"4165019","med_type":"41","medfee_sumamt":60,"medins_setl_id":"H61080200030202112171815521606","naty":"01","oth_pay":0,"overlmt_selfpay":0,"pool_prop_selfpay":0,"preselfpay_amt":0,"psn_cash_pay":0,"psn_cert_type":"01","psn_name":"宋怀春","psn_no":"61000006000000000010866022","psn_part_amt":60,"psn_type":"1101","setl_id":"3120018","setl_time":"2021-12-17 18:15:47"}
         */

        private SetlinfoBean setlinfo;
        private List<DetlcutinfoBean> detlcutinfo;
        private List<SetldetailBean> setldetail;

        public SetlinfoBean getSetlinfo() {
            return setlinfo;
        }

        public void setSetlinfo(SetlinfoBean setlinfo) {
            this.setlinfo = setlinfo;
        }

        public List<DetlcutinfoBean> getDetlcutinfo() {
            return detlcutinfo;
        }

        public void setDetlcutinfo(List<DetlcutinfoBean> detlcutinfo) {
            this.detlcutinfo = detlcutinfo;
        }

        public List<SetldetailBean> getSetldetail() {
            return setldetail;
        }

        public void setSetldetail(List<SetldetailBean> setldetail) {
            this.setldetail = setldetail;
        }

        public static class SetlinfoBean {
            /**
             * acct_pay : 60
             * act_pay_dedc : 0
             * age : 55
             * balc : 5906.9
             * brdy : 1966-09-21
             * certno : 612726196609210011
             * clr_optins : 610825
             * clr_type : 41
             * clr_way : 1
             * cvlserv_flag : 1
             * cvlserv_pay : 0
             * fulamt_ownpay_amt : 30
             * fund_pay_sumamt : 0
             * gend : 1
             * hifes_pay : 0
             * hifmi_pay : 0
             * hifob_pay : 0
             * hifp_pay : 0
             * inscp_scp_amt : 30
             * insutype : 310
             * maf_pay : 0
             * mdtrt_cert_type : 02
             * mdtrt_id : 4165019
             * med_type : 41
             * medfee_sumamt : 60
             * medins_setl_id : H61080200030202112171815521606
             * naty : 01
             * oth_pay : 0
             * overlmt_selfpay : 0
             * pool_prop_selfpay : 0
             * preselfpay_amt : 0
             * psn_cash_pay : 0
             * psn_cert_type : 01
             * psn_name : 宋怀春
             * psn_no : 61000006000000000010866022
             * psn_part_amt : 60
             * psn_type : 1101
             * setl_id : 3120018
             * setl_time : 2021-12-17 18:15:47
             */

            private String acct_pay;
            private String act_pay_dedc;
            private String age;
            private double balc;
            private String brdy;
            private String certno;
            private String clr_optins;
            private String clr_type;
            private String clr_way;
            private String cvlserv_flag;
            private String cvlserv_pay;
            private String fulamt_ownpay_amt;
            private String fund_pay_sumamt;
            private String gend;
            private String hifes_pay;
            private String hifmi_pay;
            private String hifob_pay;
            private String hifp_pay;
            private String inscp_scp_amt;
            private String insutype;
            private String maf_pay;
            private String mdtrt_cert_type;
            private String mdtrt_id;
            private String med_type;
            private String medfee_sumamt;
            private String medins_setl_id;
            private String naty;
            private String oth_pay;
            private String overlmt_selfpay;
            private String pool_prop_selfpay;
            private String preselfpay_amt;
            private String psn_cash_pay;
            private String psn_cert_type;
            private String psn_name;
            private String psn_no;
            private String psn_part_amt;
            private String psn_type;
            private String setl_id;
            private String setl_time;

            public String getAcct_pay() {
                return acct_pay;
            }

            public void setAcct_pay(String acct_pay) {
                this.acct_pay = acct_pay;
            }

            public String getAct_pay_dedc() {
                return act_pay_dedc;
            }

            public void setAct_pay_dedc(String act_pay_dedc) {
                this.act_pay_dedc = act_pay_dedc;
            }

            public String getAge() {
                return age;
            }

            public void setAge(String age) {
                this.age = age;
            }

            public double getBalc() {
                return balc;
            }

            public void setBalc(double balc) {
                this.balc = balc;
            }

            public String getBrdy() {
                return brdy;
            }

            public void setBrdy(String brdy) {
                this.brdy = brdy;
            }

            public String getCertno() {
                return certno;
            }

            public void setCertno(String certno) {
                this.certno = certno;
            }

            public String getClr_optins() {
                return clr_optins;
            }

            public void setClr_optins(String clr_optins) {
                this.clr_optins = clr_optins;
            }

            public String getClr_type() {
                return clr_type;
            }

            public void setClr_type(String clr_type) {
                this.clr_type = clr_type;
            }

            public String getClr_way() {
                return clr_way;
            }

            public void setClr_way(String clr_way) {
                this.clr_way = clr_way;
            }

            public String getCvlserv_flag() {
                return cvlserv_flag;
            }

            public void setCvlserv_flag(String cvlserv_flag) {
                this.cvlserv_flag = cvlserv_flag;
            }

            public String getCvlserv_pay() {
                return cvlserv_pay;
            }

            public void setCvlserv_pay(String cvlserv_pay) {
                this.cvlserv_pay = cvlserv_pay;
            }

            public String getFulamt_ownpay_amt() {
                return fulamt_ownpay_amt;
            }

            public void setFulamt_ownpay_amt(String fulamt_ownpay_amt) {
                this.fulamt_ownpay_amt = fulamt_ownpay_amt;
            }

            public String getFund_pay_sumamt() {
                return fund_pay_sumamt;
            }

            public void setFund_pay_sumamt(String fund_pay_sumamt) {
                this.fund_pay_sumamt = fund_pay_sumamt;
            }

            public String getGend() {
                return gend;
            }

            public void setGend(String gend) {
                this.gend = gend;
            }

            public String getHifes_pay() {
                return hifes_pay;
            }

            public void setHifes_pay(String hifes_pay) {
                this.hifes_pay = hifes_pay;
            }

            public String getHifmi_pay() {
                return hifmi_pay;
            }

            public void setHifmi_pay(String hifmi_pay) {
                this.hifmi_pay = hifmi_pay;
            }

            public String getHifob_pay() {
                return hifob_pay;
            }

            public void setHifob_pay(String hifob_pay) {
                this.hifob_pay = hifob_pay;
            }

            public String getHifp_pay() {
                return hifp_pay;
            }

            public void setHifp_pay(String hifp_pay) {
                this.hifp_pay = hifp_pay;
            }

            public String getInscp_scp_amt() {
                return inscp_scp_amt;
            }

            public void setInscp_scp_amt(String inscp_scp_amt) {
                this.inscp_scp_amt = inscp_scp_amt;
            }

            public String getInsutype() {
                return insutype;
            }

            public void setInsutype(String insutype) {
                this.insutype = insutype;
            }

            public String getMaf_pay() {
                return maf_pay;
            }

            public void setMaf_pay(String maf_pay) {
                this.maf_pay = maf_pay;
            }

            public String getMdtrt_cert_type() {
                return mdtrt_cert_type;
            }

            public void setMdtrt_cert_type(String mdtrt_cert_type) {
                this.mdtrt_cert_type = mdtrt_cert_type;
            }

            public String getMdtrt_id() {
                return mdtrt_id;
            }

            public void setMdtrt_id(String mdtrt_id) {
                this.mdtrt_id = mdtrt_id;
            }

            public String getMed_type() {
                return med_type;
            }

            public void setMed_type(String med_type) {
                this.med_type = med_type;
            }

            public String getMedfee_sumamt() {
                return medfee_sumamt;
            }

            public void setMedfee_sumamt(String medfee_sumamt) {
                this.medfee_sumamt = medfee_sumamt;
            }

            public String getMedins_setl_id() {
                return medins_setl_id;
            }

            public void setMedins_setl_id(String medins_setl_id) {
                this.medins_setl_id = medins_setl_id;
            }

            public String getNaty() {
                return naty;
            }

            public void setNaty(String naty) {
                this.naty = naty;
            }

            public String getOth_pay() {
                return oth_pay;
            }

            public void setOth_pay(String oth_pay) {
                this.oth_pay = oth_pay;
            }

            public String getOverlmt_selfpay() {
                return overlmt_selfpay;
            }

            public void setOverlmt_selfpay(String overlmt_selfpay) {
                this.overlmt_selfpay = overlmt_selfpay;
            }

            public String getPool_prop_selfpay() {
                return pool_prop_selfpay;
            }

            public void setPool_prop_selfpay(String pool_prop_selfpay) {
                this.pool_prop_selfpay = pool_prop_selfpay;
            }

            public String getPreselfpay_amt() {
                return preselfpay_amt;
            }

            public void setPreselfpay_amt(String preselfpay_amt) {
                this.preselfpay_amt = preselfpay_amt;
            }

            public String getPsn_cash_pay() {
                return psn_cash_pay;
            }

            public void setPsn_cash_pay(String psn_cash_pay) {
                this.psn_cash_pay = psn_cash_pay;
            }

            public String getPsn_cert_type() {
                return psn_cert_type;
            }

            public void setPsn_cert_type(String psn_cert_type) {
                this.psn_cert_type = psn_cert_type;
            }

            public String getPsn_name() {
                return psn_name;
            }

            public void setPsn_name(String psn_name) {
                this.psn_name = psn_name;
            }

            public String getPsn_no() {
                return psn_no;
            }

            public void setPsn_no(String psn_no) {
                this.psn_no = psn_no;
            }

            public String getPsn_part_amt() {
                return psn_part_amt;
            }

            public void setPsn_part_amt(String psn_part_amt) {
                this.psn_part_amt = psn_part_amt;
            }

            public String getPsn_type() {
                return psn_type;
            }

            public void setPsn_type(String psn_type) {
                this.psn_type = psn_type;
            }

            public String getSetl_id() {
                return setl_id;
            }

            public void setSetl_id(String setl_id) {
                this.setl_id = setl_id;
            }

            public String getSetl_time() {
                return setl_time;
            }

            public void setSetl_time(String setl_time) {
                this.setl_time = setl_time;
            }
        }

        public static class DetlcutinfoBean {
            /**
             * bas_medn_flag : 0
             * chrgitm_lv : 01
             * cnt : 1
             * det_item_fee_sumamt : 30
             * drt_reim_flag : 0
             * feedetl_sn : 1639736152190
             * fulamt_ownpay_amt : 0
             * hi_nego_drug_flag : 0
             * inscp_scp_amt : 30
             * med_chrgitm_type : 11
             * overlmt_amt : 0
             * preselfpay_amt : 0
             * pric : 30
             * pric_uplmt_amt : 999999
             * selfpay_prop : 0
             */

            private String bas_medn_flag;
            private String chrgitm_lv;
            private String cnt;
            private String det_item_fee_sumamt;
            private String drt_reim_flag;
            private String feedetl_sn;
            private String fulamt_ownpay_amt;
            private String hi_nego_drug_flag;
            private String inscp_scp_amt;
            private String med_chrgitm_type;
            private String overlmt_amt;
            private String preselfpay_amt;
            private String pric;
            private String pric_uplmt_amt;
            private String selfpay_prop;

            public String getBas_medn_flag() {
                return bas_medn_flag;
            }

            public void setBas_medn_flag(String bas_medn_flag) {
                this.bas_medn_flag = bas_medn_flag;
            }

            public String getChrgitm_lv() {
                return chrgitm_lv;
            }

            public void setChrgitm_lv(String chrgitm_lv) {
                this.chrgitm_lv = chrgitm_lv;
            }

            public String getCnt() {
                return cnt;
            }

            public void setCnt(String cnt) {
                this.cnt = cnt;
            }

            public String getDet_item_fee_sumamt() {
                return det_item_fee_sumamt;
            }

            public void setDet_item_fee_sumamt(String det_item_fee_sumamt) {
                this.det_item_fee_sumamt = det_item_fee_sumamt;
            }

            public String getDrt_reim_flag() {
                return drt_reim_flag;
            }

            public void setDrt_reim_flag(String drt_reim_flag) {
                this.drt_reim_flag = drt_reim_flag;
            }

            public String getFeedetl_sn() {
                return feedetl_sn;
            }

            public void setFeedetl_sn(String feedetl_sn) {
                this.feedetl_sn = feedetl_sn;
            }

            public String getFulamt_ownpay_amt() {
                return fulamt_ownpay_amt;
            }

            public void setFulamt_ownpay_amt(String fulamt_ownpay_amt) {
                this.fulamt_ownpay_amt = fulamt_ownpay_amt;
            }

            public String getHi_nego_drug_flag() {
                return hi_nego_drug_flag;
            }

            public void setHi_nego_drug_flag(String hi_nego_drug_flag) {
                this.hi_nego_drug_flag = hi_nego_drug_flag;
            }

            public String getInscp_scp_amt() {
                return inscp_scp_amt;
            }

            public void setInscp_scp_amt(String inscp_scp_amt) {
                this.inscp_scp_amt = inscp_scp_amt;
            }

            public String getMed_chrgitm_type() {
                return med_chrgitm_type;
            }

            public void setMed_chrgitm_type(String med_chrgitm_type) {
                this.med_chrgitm_type = med_chrgitm_type;
            }

            public String getOverlmt_amt() {
                return overlmt_amt;
            }

            public void setOverlmt_amt(String overlmt_amt) {
                this.overlmt_amt = overlmt_amt;
            }

            public String getPreselfpay_amt() {
                return preselfpay_amt;
            }

            public void setPreselfpay_amt(String preselfpay_amt) {
                this.preselfpay_amt = preselfpay_amt;
            }

            public String getPric() {
                return pric;
            }

            public void setPric(String pric) {
                this.pric = pric;
            }

            public String getPric_uplmt_amt() {
                return pric_uplmt_amt;
            }

            public void setPric_uplmt_amt(String pric_uplmt_amt) {
                this.pric_uplmt_amt = pric_uplmt_amt;
            }

            public String getSelfpay_prop() {
                return selfpay_prop;
            }

            public void setSelfpay_prop(String selfpay_prop) {
                this.selfpay_prop = selfpay_prop;
            }
        }

        public static class SetldetailBean {
            /**
             * fund_pay_type : 310200
             * fund_pay_type_name : 城镇职工基本医疗保险个人账户基金
             * fund_payamt : 60
             * inscp_scp_amt : 30
             */

            private String fund_pay_type;
            private String fund_pay_type_name;
            private String fund_payamt;
            private String inscp_scp_amt;
            private String crt_payb_lmt_amt;

            public String getFund_pay_type() {
                return fund_pay_type;
            }

            public void setFund_pay_type(String fund_pay_type) {
                this.fund_pay_type = fund_pay_type;
            }

            public String getFund_pay_type_name() {
                return fund_pay_type_name;
            }

            public void setFund_pay_type_name(String fund_pay_type_name) {
                this.fund_pay_type_name = fund_pay_type_name;
            }

            public String getFund_payamt() {
                return fund_payamt;
            }

            public void setFund_payamt(String fund_payamt) {
                this.fund_payamt = fund_payamt;
            }

            public String getInscp_scp_amt() {
                return inscp_scp_amt;
            }

            public void setInscp_scp_amt(String inscp_scp_amt) {
                this.inscp_scp_amt = inscp_scp_amt;
            }

            public String getCrt_payb_lmt_amt() {
                return crt_payb_lmt_amt;
            }

            public void setCrt_payb_lmt_amt(String crt_payb_lmt_amt) {
                this.crt_payb_lmt_amt = crt_payb_lmt_amt;
            }
        }
    }

    //药店结算
    public static void sendPharmacySettleRequest(final String TAG, String dataParams,String insuplc_admdvs, final CustomerJsonCallBack<PharmacySettleModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL+ Constants.Api.GET_FIXMEDINSETTLE_URL,dataParams,insuplc_admdvs, callback);
    }
}
