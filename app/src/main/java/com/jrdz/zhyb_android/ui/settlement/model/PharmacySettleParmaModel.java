package com.jrdz.zhyb_android.ui.settlement.model;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/17
 * 描    述：
 * ================================================
 */
public class PharmacySettleParmaModel {
    private String insuplc_admdvs;
    private String psn_no;
    private String mdtrt_cert_type;
    private String mdtrt_cert_no;
    private String identity;
    private String medfee_sumamt;
    private String insutype;
    private String med_type;
    private String dise_codg;
    private String dise_name;
    private String psn_name;
    private String name;
    private String password;

    private ArrayList<DrugdetailBean> drugdetail=new ArrayList<>();

    public PharmacySettleParmaModel(String insuplc_admdvs, String psn_no, String mdtrt_cert_type, String mdtrt_cert_no,
                                    String identity, String medfee_sumamt, String insutype, String med_type, String dise_codg,
                                    String dise_name,String psn_name,String name,String password) {
        this.insuplc_admdvs = insuplc_admdvs;
        this.psn_no = psn_no;
        this.mdtrt_cert_type = mdtrt_cert_type;
        this.mdtrt_cert_no = mdtrt_cert_no;
        this.identity = identity;
        this.medfee_sumamt = medfee_sumamt;
        this.insutype = insutype;
        this.med_type = med_type;
        this.dise_codg = dise_codg;
        this.dise_name = dise_name;
        this.psn_name=psn_name;
        this.name=name;
        this.password=password;
    }

    public String getInsuplc_admdvs() {
        return insuplc_admdvs;
    }

    public void setInsuplc_admdvs(String insuplc_admdvs) {
        this.insuplc_admdvs = insuplc_admdvs;
    }

    public String getPsn_no() {
        return psn_no;
    }

    public void setPsn_no(String psn_no) {
        this.psn_no = psn_no;
    }

    public String getMdtrt_cert_type() {
        return mdtrt_cert_type;
    }

    public void setMdtrt_cert_type(String mdtrt_cert_type) {
        this.mdtrt_cert_type = mdtrt_cert_type;
    }

    public String getMdtrt_cert_no() {
        return mdtrt_cert_no;
    }

    public void setMdtrt_cert_no(String mdtrt_cert_no) {
        this.mdtrt_cert_no = mdtrt_cert_no;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getMedfee_sumamt() {
        return medfee_sumamt;
    }

    public void setMedfee_sumamt(String medfee_sumamt) {
        this.medfee_sumamt = medfee_sumamt;
    }

    public String getInsutype() {
        return insutype;
    }

    public void setInsutype(String insutype) {
        this.insutype = insutype;
    }

    public String getMed_type() {
        return med_type;
    }

    public void setMed_type(String med_type) {
        this.med_type = med_type;
    }

    public ArrayList<DrugdetailBean> getDrugdetail() {
        return drugdetail;
    }

    public void setDrugdetail(ArrayList<DrugdetailBean> drugdetail) {
        this.drugdetail = drugdetail;
    }

    public String getDise_codg() {
        return dise_codg;
    }

    public void setDise_codg(String dise_codg) {
        this.dise_codg = dise_codg;
    }

    public String getDise_name() {
        return dise_name;
    }

    public void setDise_name(String dise_name) {
        this.dise_name = dise_name;
    }

    public String getPsn_name() {
        return psn_name;
    }

    public void setPsn_name(String psn_name) {
        this.psn_name = psn_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static class DrugdetailBean{
        private String local_contents_id;
        private String cnt;
        private String rx_circ_flag;
        private String det_item_fee_sumamt;
        private String prd_days;

        public DrugdetailBean(String local_contents_id, String cnt, String rx_circ_flag, String det_item_fee_sumamt,String prd_days) {
            this.local_contents_id = local_contents_id;
            this.cnt = cnt;
            this.rx_circ_flag = rx_circ_flag;
            this.det_item_fee_sumamt = det_item_fee_sumamt;
            this.prd_days = prd_days;
        }

        public String getLocal_contents_id() {
            return local_contents_id;
        }

        public void setLocal_contents_id(String local_contents_id) {
            this.local_contents_id = local_contents_id;
        }

        public String getCnt() {
            return cnt;
        }

        public void setCnt(String cnt) {
            this.cnt = cnt;
        }

        public String getRx_circ_flag() {
            return rx_circ_flag;
        }

        public void setRx_circ_flag(String rx_circ_flag) {
            this.rx_circ_flag = rx_circ_flag;
        }

        public String getDet_item_fee_sumamt() {
            return det_item_fee_sumamt;
        }

        public void setDet_item_fee_sumamt(String det_item_fee_sumamt) {
            this.det_item_fee_sumamt = det_item_fee_sumamt;
        }

        public String getPrd_days() {
            return prd_days;
        }

        public void setPrd_days(String prd_days) {
            this.prd_days = prd_days;
        }
    }
}
