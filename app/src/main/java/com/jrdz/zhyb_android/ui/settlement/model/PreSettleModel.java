package com.jrdz.zhyb_android.ui.settlement.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;


/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/17
 * 描    述：
 * ================================================
 */
public class PreSettleModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-02-23 16:10:36
     * data : {"mdtrt_id":"121000001","psn_no":"61000006000000000010866022","psn_name":"宋怀春","psn_type":"1101","setl_time":"2022-02-23 16:10:36","med_type":"41","medfee_sumamt":"0.02","fund_pay_sumamt":"0","psn_part_amt":"0.02","acct_pay":"0.02","psn_cash_pay":"0","hosp_part_amt":"","balc":"4550.53","clr_type":"41","content":"榆林市医疗保障结算单\n定点编号：H61080200030\n定点名称：榆阳区安定精神病医院\n参保区划：\n结算ID：\n个人编号：\n姓名：\n人员类别：\n医疗类别：\n结算病种：\n结算日期：\n总费用：\n统筹支付：\n个人账户支付：\n个人现金支付：\n账户余额：\n慢性病累计支付：\n操作人：\n-------------费用明细-----------\n项目名称     单价(元)     数量    金额(元)\n木香顺气丸   0.02   1   0.02\n"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * mdtrt_id : 121000001
         * psn_no : 61000006000000000010866022
         * psn_name : 宋怀春
         * psn_type : 1101
         * setl_time : 2022-02-23 16:10:36
         * med_type : 41
         * medfee_sumamt : 0.02
         * fund_pay_sumamt : 0
         * psn_part_amt : 0.02
         * acct_pay : 0.02
         * psn_cash_pay : 0
         * hosp_part_amt :
         * balc : 4550.53
         * clr_type : 41
         * content : 榆林市医疗保障结算单
         定点编号：H61080200030
         定点名称：榆阳区安定精神病医院
         参保区划：
         结算ID：
         个人编号：
         姓名：
         人员类别：
         医疗类别：
         结算病种：
         结算日期：
         总费用：
         统筹支付：
         个人账户支付：
         个人现金支付：
         账户余额：
         慢性病累计支付：
         操作人：
         -------------费用明细-----------
         项目名称     单价(元)     数量    金额(元)
         木香顺气丸   0.02   1   0.02

         */

        private String mdtrt_id;
        private String psn_no;
        private String psn_name;
        private String psn_type;
        private String setl_time;
        private String med_type;
        private String medfee_sumamt;
        private String fund_pay_sumamt;
        private String psn_part_amt;
        private String acct_pay;
        private String psn_cash_pay;
        private String hosp_part_amt;
        private String balc;
        private String clr_type;
        private String content;

        public String getMdtrt_id() {
            return mdtrt_id;
        }

        public void setMdtrt_id(String mdtrt_id) {
            this.mdtrt_id = mdtrt_id;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getPsn_name() {
            return psn_name;
        }

        public void setPsn_name(String psn_name) {
            this.psn_name = psn_name;
        }

        public String getPsn_type() {
            return psn_type;
        }

        public void setPsn_type(String psn_type) {
            this.psn_type = psn_type;
        }

        public String getSetl_time() {
            return setl_time;
        }

        public void setSetl_time(String setl_time) {
            this.setl_time = setl_time;
        }

        public String getMed_type() {
            return med_type;
        }

        public void setMed_type(String med_type) {
            this.med_type = med_type;
        }

        public String getMedfee_sumamt() {
            return medfee_sumamt;
        }

        public void setMedfee_sumamt(String medfee_sumamt) {
            this.medfee_sumamt = medfee_sumamt;
        }

        public String getFund_pay_sumamt() {
            return fund_pay_sumamt;
        }

        public void setFund_pay_sumamt(String fund_pay_sumamt) {
            this.fund_pay_sumamt = fund_pay_sumamt;
        }

        public String getPsn_part_amt() {
            return psn_part_amt;
        }

        public void setPsn_part_amt(String psn_part_amt) {
            this.psn_part_amt = psn_part_amt;
        }

        public String getAcct_pay() {
            return acct_pay;
        }

        public void setAcct_pay(String acct_pay) {
            this.acct_pay = acct_pay;
        }

        public String getPsn_cash_pay() {
            return psn_cash_pay;
        }

        public void setPsn_cash_pay(String psn_cash_pay) {
            this.psn_cash_pay = psn_cash_pay;
        }

        public String getHosp_part_amt() {
            return hosp_part_amt;
        }

        public void setHosp_part_amt(String hosp_part_amt) {
            this.hosp_part_amt = hosp_part_amt;
        }

        public String getBalc() {
            return balc;
        }

        public void setBalc(String balc) {
            this.balc = balc;
        }

        public String getClr_type() {
            return clr_type;
        }

        public void setClr_type(String clr_type) {
            this.clr_type = clr_type;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    //药店预结算
    public static void sendPrePharmacySettleRequest(final String TAG, String dataParams,String insuplc_admdvs, final CustomerJsonCallBack<PreSettleModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL+ Constants.Api.GET_FIXMEDINSETTLEPRE_URL,dataParams,insuplc_admdvs, callback);
    }

    //门诊预结算
    public static void sendPreSaveSettleRequest(final String TAG, String parma, String insuplc_admdvs, final CustomerJsonCallBack<PreSettleModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_SAVESETTLETMENTPRE_URL, parma, insuplc_admdvs, callback);
    }
}
