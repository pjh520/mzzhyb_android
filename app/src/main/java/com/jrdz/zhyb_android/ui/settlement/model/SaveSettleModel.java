package com.jrdz.zhyb_android.ui.settlement.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/22
 * 描    述：
 * ================================================
 */
public class SaveSettleModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-02-06 16:04:28
     * data : {"setldetail":[{"crt_payb_lmt_amt":null,"fund_pay_type":"310200","fund_pay_type_name":"城镇职工基本医疗保险个人账户基金","fund_payamt":0.01,"inscp_scp_amt":0,"setl_proc_info":null}],"setlinfo":{"acct_mulaid_pay":0,"acct_pay":0.01,"act_pay_dedc":0,"age":55,"balc":4734.81,"brdy":"1966-09-21","certno":"612726196609210011","clr_optins":"610825","clr_type":"11","clr_way":"1","cvlserv_flag":"1","cvlserv_pay":0,"fulamt_ownpay_amt":0.01,"fund_pay_sumamt":0,"gend":"1","hifes_pay":0,"hifmi_pay":0,"hifob_pay":0,"hifp_pay":0,"hosp_part_amt":0,"inscp_scp_amt":0,"insutype":"310","maf_pay":0,"mdtrt_cert_type":"02","mdtrt_id":"156600001","med_type":"11","medfee_sumamt":0.01,"medins_setl_id":"H61080200022202202061604240002","naty":"01","oth_pay":0,"overlmt_selfpay":0,"pool_prop_selfpay":0,"preselfpay_amt":0,"psn_cash_pay":0,"psn_cert_type":"01","psn_name":"宋怀春","psn_no":"61000006000000000010866022","psn_part_amt":0.01,"psn_type":"1101","setl_id":"147600003","setl_time":"2022-02-06 16:03:50"}}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * setldetail : [{"crt_payb_lmt_amt":null,"fund_pay_type":"310200","fund_pay_type_name":"城镇职工基本医疗保险个人账户基金","fund_payamt":0.01,"inscp_scp_amt":0,"setl_proc_info":null}]
         * setlinfo : {"acct_mulaid_pay":0,"acct_pay":0.01,"act_pay_dedc":0,"age":55,"balc":4734.81,"brdy":"1966-09-21","certno":"612726196609210011","clr_optins":"610825","clr_type":"11","clr_way":"1","cvlserv_flag":"1","cvlserv_pay":0,"fulamt_ownpay_amt":0.01,"fund_pay_sumamt":0,"gend":"1","hifes_pay":0,"hifmi_pay":0,"hifob_pay":0,"hifp_pay":0,"hosp_part_amt":0,"inscp_scp_amt":0,"insutype":"310","maf_pay":0,"mdtrt_cert_type":"02","mdtrt_id":"156600001","med_type":"11","medfee_sumamt":0.01,"medins_setl_id":"H61080200022202202061604240002","naty":"01","oth_pay":0,"overlmt_selfpay":0,"pool_prop_selfpay":0,"preselfpay_amt":0,"psn_cash_pay":0,"psn_cert_type":"01","psn_name":"宋怀春","psn_no":"61000006000000000010866022","psn_part_amt":0.01,"psn_type":"1101","setl_id":"147600003","setl_time":"2022-02-06 16:03:50"}
         */

        private SetlinfoBean setlinfo;
        private List<PharmacySettleModel.DataBean.SetldetailBean> setldetail;

        public SetlinfoBean getSetlinfo() {
            return setlinfo;
        }

        public void setSetlinfo(SetlinfoBean setlinfo) {
            this.setlinfo = setlinfo;
        }

        public List<PharmacySettleModel.DataBean.SetldetailBean> getSetldetail() {
            return setldetail;
        }

        public void setSetldetail(List<PharmacySettleModel.DataBean.SetldetailBean> setldetail) {
            this.setldetail = setldetail;
        }

        public static class SetlinfoBean {
            /**
             * acct_mulaid_pay : 0
             * acct_pay : 0.01
             * act_pay_dedc : 0
             * age : 55
             * balc : 4734.81
             * brdy : 1966-09-21
             * certno : 612726196609210011
             * clr_optins : 610825
             * clr_type : 11
             * clr_way : 1
             * cvlserv_flag : 1
             * cvlserv_pay : 0
             * fulamt_ownpay_amt : 0.01
             * fund_pay_sumamt : 0
             * gend : 1
             * hifes_pay : 0
             * hifmi_pay : 0
             * hifob_pay : 0
             * hifp_pay : 0
             * hosp_part_amt : 0
             * inscp_scp_amt : 0
             * insutype : 310
             * maf_pay : 0
             * mdtrt_cert_type : 02
             * mdtrt_id : 156600001
             * med_type : 11
             * medfee_sumamt : 0.01
             * medins_setl_id : H61080200022202202061604240002
             * naty : 01
             * oth_pay : 0
             * overlmt_selfpay : 0
             * pool_prop_selfpay : 0
             * preselfpay_amt : 0
             * psn_cash_pay : 0
             * psn_cert_type : 01
             * psn_name : 宋怀春
             * psn_no : 61000006000000000010866022
             * psn_part_amt : 0.01
             * psn_type : 1101
             * setl_id : 147600003
             * setl_time : 2022-02-06 16:03:50
             */

            private String acct_mulaid_pay;
            private String acct_pay;
            private String act_pay_dedc;
            private String age;
            private String balc;
            private String brdy;
            private String certno;
            private String clr_optins;
            private String clr_type;
            private String clr_way;
            private String cvlserv_flag;
            private String cvlserv_pay;
            private String fulamt_ownpay_amt;
            private String fund_pay_sumamt;
            private String gend;
            private String hifes_pay;
            private String hifmi_pay;
            private String hifob_pay;
            private String hifp_pay;
            private String hosp_part_amt;
            private String inscp_scp_amt;
            private String insutype;
            private String maf_pay;
            private String mdtrt_cert_type;
            private String mdtrt_id;
            private String med_type;
            private String medfee_sumamt;
            private String medins_setl_id;
            private String naty;
            private String oth_pay;
            private String overlmt_selfpay;
            private String pool_prop_selfpay;
            private String preselfpay_amt;
            private String psn_cash_pay;
            private String psn_cert_type;
            private String psn_name;
            private String psn_no;
            private String psn_part_amt;
            private String psn_type;
            private String setl_id;
            private String setl_time;

            public String getAcct_mulaid_pay() {
                return acct_mulaid_pay;
            }

            public void setAcct_mulaid_pay(String acct_mulaid_pay) {
                this.acct_mulaid_pay = acct_mulaid_pay;
            }

            public String getAcct_pay() {
                return acct_pay;
            }

            public void setAcct_pay(String acct_pay) {
                this.acct_pay = acct_pay;
            }

            public String getAct_pay_dedc() {
                return act_pay_dedc;
            }

            public void setAct_pay_dedc(String act_pay_dedc) {
                this.act_pay_dedc = act_pay_dedc;
            }

            public String getAge() {
                return age;
            }

            public void setAge(String age) {
                this.age = age;
            }

            public String getBalc() {
                return balc;
            }

            public void setBalc(String balc) {
                this.balc = balc;
            }

            public String getBrdy() {
                return brdy;
            }

            public void setBrdy(String brdy) {
                this.brdy = brdy;
            }

            public String getCertno() {
                return certno;
            }

            public void setCertno(String certno) {
                this.certno = certno;
            }

            public String getClr_optins() {
                return clr_optins;
            }

            public void setClr_optins(String clr_optins) {
                this.clr_optins = clr_optins;
            }

            public String getClr_type() {
                return clr_type;
            }

            public void setClr_type(String clr_type) {
                this.clr_type = clr_type;
            }

            public String getClr_way() {
                return clr_way;
            }

            public void setClr_way(String clr_way) {
                this.clr_way = clr_way;
            }

            public String getCvlserv_flag() {
                return cvlserv_flag;
            }

            public void setCvlserv_flag(String cvlserv_flag) {
                this.cvlserv_flag = cvlserv_flag;
            }

            public String getCvlserv_pay() {
                return cvlserv_pay;
            }

            public void setCvlserv_pay(String cvlserv_pay) {
                this.cvlserv_pay = cvlserv_pay;
            }

            public String getFulamt_ownpay_amt() {
                return fulamt_ownpay_amt;
            }

            public void setFulamt_ownpay_amt(String fulamt_ownpay_amt) {
                this.fulamt_ownpay_amt = fulamt_ownpay_amt;
            }

            public String getFund_pay_sumamt() {
                return fund_pay_sumamt;
            }

            public void setFund_pay_sumamt(String fund_pay_sumamt) {
                this.fund_pay_sumamt = fund_pay_sumamt;
            }

            public String getGend() {
                return gend;
            }

            public void setGend(String gend) {
                this.gend = gend;
            }

            public String getHifes_pay() {
                return hifes_pay;
            }

            public void setHifes_pay(String hifes_pay) {
                this.hifes_pay = hifes_pay;
            }

            public String getHifmi_pay() {
                return hifmi_pay;
            }

            public void setHifmi_pay(String hifmi_pay) {
                this.hifmi_pay = hifmi_pay;
            }

            public String getHifob_pay() {
                return hifob_pay;
            }

            public void setHifob_pay(String hifob_pay) {
                this.hifob_pay = hifob_pay;
            }

            public String getHifp_pay() {
                return hifp_pay;
            }

            public void setHifp_pay(String hifp_pay) {
                this.hifp_pay = hifp_pay;
            }

            public String getHosp_part_amt() {
                return hosp_part_amt;
            }

            public void setHosp_part_amt(String hosp_part_amt) {
                this.hosp_part_amt = hosp_part_amt;
            }

            public String getInscp_scp_amt() {
                return inscp_scp_amt;
            }

            public void setInscp_scp_amt(String inscp_scp_amt) {
                this.inscp_scp_amt = inscp_scp_amt;
            }

            public String getInsutype() {
                return insutype;
            }

            public void setInsutype(String insutype) {
                this.insutype = insutype;
            }

            public String getMaf_pay() {
                return maf_pay;
            }

            public void setMaf_pay(String maf_pay) {
                this.maf_pay = maf_pay;
            }

            public String getMdtrt_cert_type() {
                return mdtrt_cert_type;
            }

            public void setMdtrt_cert_type(String mdtrt_cert_type) {
                this.mdtrt_cert_type = mdtrt_cert_type;
            }

            public String getMdtrt_id() {
                return mdtrt_id;
            }

            public void setMdtrt_id(String mdtrt_id) {
                this.mdtrt_id = mdtrt_id;
            }

            public String getMed_type() {
                return med_type;
            }

            public void setMed_type(String med_type) {
                this.med_type = med_type;
            }

            public String getMedfee_sumamt() {
                return medfee_sumamt;
            }

            public void setMedfee_sumamt(String medfee_sumamt) {
                this.medfee_sumamt = medfee_sumamt;
            }

            public String getMedins_setl_id() {
                return medins_setl_id;
            }

            public void setMedins_setl_id(String medins_setl_id) {
                this.medins_setl_id = medins_setl_id;
            }

            public String getNaty() {
                return naty;
            }

            public void setNaty(String naty) {
                this.naty = naty;
            }

            public String getOth_pay() {
                return oth_pay;
            }

            public void setOth_pay(String oth_pay) {
                this.oth_pay = oth_pay;
            }

            public String getOverlmt_selfpay() {
                return overlmt_selfpay;
            }

            public void setOverlmt_selfpay(String overlmt_selfpay) {
                this.overlmt_selfpay = overlmt_selfpay;
            }

            public String getPool_prop_selfpay() {
                return pool_prop_selfpay;
            }

            public void setPool_prop_selfpay(String pool_prop_selfpay) {
                this.pool_prop_selfpay = pool_prop_selfpay;
            }

            public String getPreselfpay_amt() {
                return preselfpay_amt;
            }

            public void setPreselfpay_amt(String preselfpay_amt) {
                this.preselfpay_amt = preselfpay_amt;
            }

            public String getPsn_cash_pay() {
                return psn_cash_pay;
            }

            public void setPsn_cash_pay(String psn_cash_pay) {
                this.psn_cash_pay = psn_cash_pay;
            }

            public String getPsn_cert_type() {
                return psn_cert_type;
            }

            public void setPsn_cert_type(String psn_cert_type) {
                this.psn_cert_type = psn_cert_type;
            }

            public String getPsn_name() {
                return psn_name;
            }

            public void setPsn_name(String psn_name) {
                this.psn_name = psn_name;
            }

            public String getPsn_no() {
                return psn_no;
            }

            public void setPsn_no(String psn_no) {
                this.psn_no = psn_no;
            }

            public String getPsn_part_amt() {
                return psn_part_amt;
            }

            public void setPsn_part_amt(String psn_part_amt) {
                this.psn_part_amt = psn_part_amt;
            }

            public String getPsn_type() {
                return psn_type;
            }

            public void setPsn_type(String psn_type) {
                this.psn_type = psn_type;
            }

            public String getSetl_id() {
                return setl_id;
            }

            public void setSetl_id(String setl_id) {
                this.setl_id = setl_id;
            }

            public String getSetl_time() {
                return setl_time;
            }

            public void setSetl_time(String setl_time) {
                this.setl_time = setl_time;
            }
        }
    }

    //门诊结算
    public static void sendSaveSettleRequest(final String TAG,String ipt_otp_no, String psn_no, String mdtrt_cert_type, String mdtrt_cert_no, String med_type,
                                             String medfee_sumamt, String psn_setlway, String mdtrt_id, String chrg_bchno,
                                             String insutype, String invono,String name,String password,String settlementClassification,String insuplc_admdvs,
                                             final CustomerJsonCallBack<SaveSettleModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ipt_otp_no", ipt_otp_no);
        jsonObject.put("psn_no", psn_no);
        jsonObject.put("mdtrt_cert_type", mdtrt_cert_type);
        jsonObject.put("mdtrt_cert_no", mdtrt_cert_no);
        jsonObject.put("med_type", med_type);
        jsonObject.put("medfee_sumamt", medfee_sumamt);
        jsonObject.put("psn_setlway", psn_setlway);
        jsonObject.put("mdtrt_id", mdtrt_id);
        jsonObject.put("chrg_bchno", chrg_bchno);
        jsonObject.put("acct_used_flag", "1");
        jsonObject.put("insutype", insutype);
        jsonObject.put("invono", invono);
        jsonObject.put("name", name);
        jsonObject.put("password", password);
        jsonObject.put("settlementClassification", settlementClassification);
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_SAVESETTLETMENT_URL, jsonObject.toJSONString(),insuplc_admdvs, callback);
    }
}
