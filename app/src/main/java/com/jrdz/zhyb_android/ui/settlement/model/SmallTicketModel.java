package com.jrdz.zhyb_android.ui.settlement.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/27
 * 描    述：
 * ================================================
 */
public class SmallTicketModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-02-08 01:50:15
     * data : {"content":"门诊结算清单"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * content : 门诊结算清单
         * imgurl：""
         */

        private String content;
        private String imgurl;
        private String IsOrder;//是否订单（0 不是 1是）

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getImgurl() {
            return imgurl;
        }

        public void setImgurl(String imgurl) {
            this.imgurl = imgurl;
        }

        public String getIsOrder() {
            return IsOrder;
        }

        public void setIsOrder(String isOrder) {
            IsOrder = isOrder;
        }
    }

    //获取小票数据
    public static void sendSmallTicketRequest(final String TAG, String Setl_id,final CustomerJsonCallBack<SmallTicketModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Setl_id", Setl_id);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_SETTLERECEIPT_URL, jsonObject.toJSONString(), callback);
    }
}
