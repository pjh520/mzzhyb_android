package com.jrdz.zhyb_android.ui.settlement.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.settlement.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-08-31
 * 描    述：
 * ================================================
 */
public class UnsettledOutpatientInfoModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-08-31 18:10:11
     * data : [{"ElectronicPrescriptionId":2484,"ipt_otp_no":"202308311750051041","mdtrt_id":"476854306","psn_no":"61000006000000000017308180","fixmedins_code":"H61080300942","CreateDT":"2023-08-31 17:53:53","UpdateDT":"2023-08-31 17:53:53","CreateUser":"D610803001453","UpdateUser":"D610803001453","settlementClassification":1,"electronicPrescriptionNo":"202308311753236343","PrescriptionType":1,"wmcpmDiagResults":"感冒","wmcpmAmount":1,"cmDiagResults":"","cmDrugNum":0,"cmused_mtd":"","cmAmount":0,"cmRemark":"","unt":0,"accessoryId":"02783ee9-9f18-4e9a-9d92-6fce980547ce","IsSubmitted":0}]
     * feesdata : [{"FeedetailId":20376,"ipt_otp_no":"202308311750051041","mdtrt_id":"476854306","feedetl_sn":"202308311753530200","psn_no":"61000006000000000017308180","chrg_bchno":"202308311753529884","dise_codg":"","rxno":"","rx_circ_flag":"0","fee_ocur_time":"2023-08-31 17:53:53","med_list_codg":"ZF03AAX0537030102573","medins_list_codg":"ZF03AAX0537030102573","det_item_fee_sumamt":"1.0","cnt":"1","pric":"1.0","sin_dos_dscr":"每次1片","used_frqu_dscr":"每日一次","prd_days":"2","medc_way_dscr":"","bilg_dept_codg":"A02","bilg_dept_name":"全科医疗科","bilg_dr_codg":"D610803001453","bilg_dr_name":"陈子平","acord_dept_codg":"","acord_dept_name":"","orders_dr_code":"","orders_dr_name":"","hosp_appr_flag":"1","tcmdrug_used_way":"","etip_flag":"","etip_hosp_code":"","dscg_tkdrug_flag":"","matn_fee_flag":"","setl_id":"","medins_list_name":"小儿咽扁颗粒","fixmedins_code":"H61080300942","CreateDT":"2023-08-31 17:53:53","UpdateDT":"2023-08-31 17:53:53","CreateUser":"D610803001453","UpdateUser":"D610803001453","unt":"片","used_mtd":"饭后  口服","remark":"测试西药备注","electronicPrescriptionNo":"202308311753236343","spec":"每袋装8g"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;
    private List<FeesdataBean> feesdata;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public List<FeesdataBean> getFeesdata() {
        return feesdata;
    }

    public void setFeesdata(List<FeesdataBean> feesdata) {
        this.feesdata = feesdata;
    }

    public static class DataBean {
        /**
         * ElectronicPrescriptionId : 2484
         * ipt_otp_no : 202308311750051041
         * mdtrt_id : 476854306
         * psn_no : 61000006000000000017308180
         * fixmedins_code : H61080300942
         * CreateDT : 2023-08-31 17:53:53
         * UpdateDT : 2023-08-31 17:53:53
         * CreateUser : D610803001453
         * UpdateUser : D610803001453
         * settlementClassification : 1
         * electronicPrescriptionNo : 202308311753236343
         * PrescriptionType : 1
         * wmcpmDiagResults : 感冒
         * wmcpmAmount : 1
         * cmDiagResults :
         * cmDrugNum : 0
         * cmused_mtd :
         * cmAmount : 0
         * cmRemark :
         * unt : 0
         * accessoryId : 02783ee9-9f18-4e9a-9d92-6fce980547ce
         * IsSubmitted : 0
         */

        private int ElectronicPrescriptionId;
        private String ipt_otp_no;
        private String mdtrt_id;
        private String psn_no;
        private String fixmedins_code;
        private String CreateDT;
        private String UpdateDT;
        private String CreateUser;
        private String UpdateUser;
        private int settlementClassification;
        private String electronicPrescriptionNo;
        private String PrescriptionType;
        private String wmcpmDiagResults;
        private String wmcpmAmount;
        private String cmDiagResults;
        private int cmDrugNum;
        private String cmused_mtd;
        private String cmAmount;
        private String cmRemark;
        private String unt;
        private String accessoryId;
        private int IsSubmitted;

        public int getElectronicPrescriptionId() {
            return ElectronicPrescriptionId;
        }

        public void setElectronicPrescriptionId(int ElectronicPrescriptionId) {
            this.ElectronicPrescriptionId = ElectronicPrescriptionId;
        }

        public String getIpt_otp_no() {
            return ipt_otp_no;
        }

        public void setIpt_otp_no(String ipt_otp_no) {
            this.ipt_otp_no = ipt_otp_no;
        }

        public String getMdtrt_id() {
            return mdtrt_id;
        }

        public void setMdtrt_id(String mdtrt_id) {
            this.mdtrt_id = mdtrt_id;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getUpdateDT() {
            return UpdateDT;
        }

        public void setUpdateDT(String UpdateDT) {
            this.UpdateDT = UpdateDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String CreateUser) {
            this.CreateUser = CreateUser;
        }

        public String getUpdateUser() {
            return UpdateUser;
        }

        public void setUpdateUser(String UpdateUser) {
            this.UpdateUser = UpdateUser;
        }

        public int getSettlementClassification() {
            return settlementClassification;
        }

        public void setSettlementClassification(int settlementClassification) {
            this.settlementClassification = settlementClassification;
        }

        public String getElectronicPrescriptionNo() {
            return electronicPrescriptionNo;
        }

        public void setElectronicPrescriptionNo(String electronicPrescriptionNo) {
            this.electronicPrescriptionNo = electronicPrescriptionNo;
        }

        public String getPrescriptionType() {
            return PrescriptionType;
        }

        public void setPrescriptionType(String PrescriptionType) {
            this.PrescriptionType = PrescriptionType;
        }

        public String getWmcpmDiagResults() {
            return wmcpmDiagResults;
        }

        public void setWmcpmDiagResults(String wmcpmDiagResults) {
            this.wmcpmDiagResults = wmcpmDiagResults;
        }

        public String getWmcpmAmount() {
            return wmcpmAmount;
        }

        public void setWmcpmAmount(String wmcpmAmount) {
            this.wmcpmAmount = wmcpmAmount;
        }

        public String getCmDiagResults() {
            return cmDiagResults;
        }

        public void setCmDiagResults(String cmDiagResults) {
            this.cmDiagResults = cmDiagResults;
        }

        public int getCmDrugNum() {
            return cmDrugNum;
        }

        public void setCmDrugNum(int cmDrugNum) {
            this.cmDrugNum = cmDrugNum;
        }

        public String getCmused_mtd() {
            return cmused_mtd;
        }

        public void setCmused_mtd(String cmused_mtd) {
            this.cmused_mtd = cmused_mtd;
        }

        public String getCmAmount() {
            return cmAmount;
        }

        public void setCmAmount(String cmAmount) {
            this.cmAmount = cmAmount;
        }

        public String getCmRemark() {
            return cmRemark;
        }

        public void setCmRemark(String cmRemark) {
            this.cmRemark = cmRemark;
        }

        public String getUnt() {
            return unt;
        }

        public void setUnt(String unt) {
            this.unt = unt;
        }

        public String getAccessoryId() {
            return accessoryId;
        }

        public void setAccessoryId(String accessoryId) {
            this.accessoryId = accessoryId;
        }

        public int getIsSubmitted() {
            return IsSubmitted;
        }

        public void setIsSubmitted(int IsSubmitted) {
            this.IsSubmitted = IsSubmitted;
        }
    }

    public static class FeesdataBean {
        /**
         * FeedetailId : 20376
         * ipt_otp_no : 202308311750051041
         * mdtrt_id : 476854306
         * feedetl_sn : 202308311753530200
         * psn_no : 61000006000000000017308180
         * chrg_bchno : 202308311753529884
         * dise_codg :
         * rxno :
         * rx_circ_flag : 0
         * fee_ocur_time : 2023-08-31 17:53:53
         * med_list_codg : ZF03AAX0537030102573
         * medins_list_codg : ZF03AAX0537030102573
         * det_item_fee_sumamt : 1.0
         * cnt : 1
         * pric : 1.0
         * sin_dos_dscr : 每次1片
         * used_frqu_dscr : 每日一次
         * prd_days : 2
         * medc_way_dscr :
         * bilg_dept_codg : A02
         * bilg_dept_name : 全科医疗科
         * bilg_dr_codg : D610803001453
         * bilg_dr_name : 陈子平
         * acord_dept_codg :
         * acord_dept_name :
         * orders_dr_code :
         * orders_dr_name :
         * hosp_appr_flag : 1
         * tcmdrug_used_way :
         * etip_flag :
         * etip_hosp_code :
         * dscg_tkdrug_flag :
         * matn_fee_flag :
         * setl_id :
         * medins_list_name : 小儿咽扁颗粒
         * fixmedins_code : H61080300942
         * CreateDT : 2023-08-31 17:53:53
         * UpdateDT : 2023-08-31 17:53:53
         * CreateUser : D610803001453
         * UpdateUser : D610803001453
         * unt : 片
         * used_mtd : 饭后  口服
         * remark : 测试西药备注
         * electronicPrescriptionNo : 202308311753236343
         * spec : 每袋装8g
         */

        private int FeedetailId;
        private String ipt_otp_no;
        private String mdtrt_id;
        private String feedetl_sn;
        private String psn_no;
        private String chrg_bchno;
        private String dise_codg;
        private String rxno;
        private String rx_circ_flag;
        private String fee_ocur_time;
        private String med_list_codg;
        private String medins_list_codg;
        private String det_item_fee_sumamt;
        private String cnt;
        private String pric;
        private String sin_dos_dscr;
        private String used_frqu_dscr;
        private String prd_days;
        private String medc_way_dscr;
        private String bilg_dept_codg;
        private String bilg_dept_name;
        private String bilg_dr_codg;
        private String bilg_dr_name;
        private String acord_dept_codg;
        private String acord_dept_name;
        private String orders_dr_code;
        private String orders_dr_name;
        private String hosp_appr_flag;
        private String tcmdrug_used_way;
        private String etip_flag;
        private String etip_hosp_code;
        private String dscg_tkdrug_flag;
        private String matn_fee_flag;
        private String setl_id;
        private String medins_list_name;
        private String fixmedins_code;
        private String CreateDT;
        private String UpdateDT;
        private String CreateUser;
        private String UpdateUser;
        private String unt;
        private String used_mtd;
        private String remark;
        private String electronicPrescriptionNo;
        private String spec;

        public int getFeedetailId() {
            return FeedetailId;
        }

        public void setFeedetailId(int FeedetailId) {
            this.FeedetailId = FeedetailId;
        }

        public String getIpt_otp_no() {
            return ipt_otp_no;
        }

        public void setIpt_otp_no(String ipt_otp_no) {
            this.ipt_otp_no = ipt_otp_no;
        }

        public String getMdtrt_id() {
            return mdtrt_id;
        }

        public void setMdtrt_id(String mdtrt_id) {
            this.mdtrt_id = mdtrt_id;
        }

        public String getFeedetl_sn() {
            return feedetl_sn;
        }

        public void setFeedetl_sn(String feedetl_sn) {
            this.feedetl_sn = feedetl_sn;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getChrg_bchno() {
            return chrg_bchno;
        }

        public void setChrg_bchno(String chrg_bchno) {
            this.chrg_bchno = chrg_bchno;
        }

        public String getDise_codg() {
            return dise_codg;
        }

        public void setDise_codg(String dise_codg) {
            this.dise_codg = dise_codg;
        }

        public String getRxno() {
            return rxno;
        }

        public void setRxno(String rxno) {
            this.rxno = rxno;
        }

        public String getRx_circ_flag() {
            return rx_circ_flag;
        }

        public void setRx_circ_flag(String rx_circ_flag) {
            this.rx_circ_flag = rx_circ_flag;
        }

        public String getFee_ocur_time() {
            return fee_ocur_time;
        }

        public void setFee_ocur_time(String fee_ocur_time) {
            this.fee_ocur_time = fee_ocur_time;
        }

        public String getMed_list_codg() {
            return med_list_codg;
        }

        public void setMed_list_codg(String med_list_codg) {
            this.med_list_codg = med_list_codg;
        }

        public String getMedins_list_codg() {
            return medins_list_codg;
        }

        public void setMedins_list_codg(String medins_list_codg) {
            this.medins_list_codg = medins_list_codg;
        }

        public String getDet_item_fee_sumamt() {
            return det_item_fee_sumamt;
        }

        public void setDet_item_fee_sumamt(String det_item_fee_sumamt) {
            this.det_item_fee_sumamt = det_item_fee_sumamt;
        }

        public String getCnt() {
            return cnt;
        }

        public void setCnt(String cnt) {
            this.cnt = cnt;
        }

        public String getPric() {
            return pric;
        }

        public void setPric(String pric) {
            this.pric = pric;
        }

        public String getSin_dos_dscr() {
            return sin_dos_dscr;
        }

        public void setSin_dos_dscr(String sin_dos_dscr) {
            this.sin_dos_dscr = sin_dos_dscr;
        }

        public String getUsed_frqu_dscr() {
            return used_frqu_dscr;
        }

        public void setUsed_frqu_dscr(String used_frqu_dscr) {
            this.used_frqu_dscr = used_frqu_dscr;
        }

        public String getPrd_days() {
            return prd_days;
        }

        public void setPrd_days(String prd_days) {
            this.prd_days = prd_days;
        }

        public String getMedc_way_dscr() {
            return medc_way_dscr;
        }

        public void setMedc_way_dscr(String medc_way_dscr) {
            this.medc_way_dscr = medc_way_dscr;
        }

        public String getBilg_dept_codg() {
            return bilg_dept_codg;
        }

        public void setBilg_dept_codg(String bilg_dept_codg) {
            this.bilg_dept_codg = bilg_dept_codg;
        }

        public String getBilg_dept_name() {
            return bilg_dept_name;
        }

        public void setBilg_dept_name(String bilg_dept_name) {
            this.bilg_dept_name = bilg_dept_name;
        }

        public String getBilg_dr_codg() {
            return bilg_dr_codg;
        }

        public void setBilg_dr_codg(String bilg_dr_codg) {
            this.bilg_dr_codg = bilg_dr_codg;
        }

        public String getBilg_dr_name() {
            return bilg_dr_name;
        }

        public void setBilg_dr_name(String bilg_dr_name) {
            this.bilg_dr_name = bilg_dr_name;
        }

        public String getAcord_dept_codg() {
            return acord_dept_codg;
        }

        public void setAcord_dept_codg(String acord_dept_codg) {
            this.acord_dept_codg = acord_dept_codg;
        }

        public String getAcord_dept_name() {
            return acord_dept_name;
        }

        public void setAcord_dept_name(String acord_dept_name) {
            this.acord_dept_name = acord_dept_name;
        }

        public String getOrders_dr_code() {
            return orders_dr_code;
        }

        public void setOrders_dr_code(String orders_dr_code) {
            this.orders_dr_code = orders_dr_code;
        }

        public String getOrders_dr_name() {
            return orders_dr_name;
        }

        public void setOrders_dr_name(String orders_dr_name) {
            this.orders_dr_name = orders_dr_name;
        }

        public String getHosp_appr_flag() {
            return hosp_appr_flag;
        }

        public void setHosp_appr_flag(String hosp_appr_flag) {
            this.hosp_appr_flag = hosp_appr_flag;
        }

        public String getTcmdrug_used_way() {
            return tcmdrug_used_way;
        }

        public void setTcmdrug_used_way(String tcmdrug_used_way) {
            this.tcmdrug_used_way = tcmdrug_used_way;
        }

        public String getEtip_flag() {
            return etip_flag;
        }

        public void setEtip_flag(String etip_flag) {
            this.etip_flag = etip_flag;
        }

        public String getEtip_hosp_code() {
            return etip_hosp_code;
        }

        public void setEtip_hosp_code(String etip_hosp_code) {
            this.etip_hosp_code = etip_hosp_code;
        }

        public String getDscg_tkdrug_flag() {
            return dscg_tkdrug_flag;
        }

        public void setDscg_tkdrug_flag(String dscg_tkdrug_flag) {
            this.dscg_tkdrug_flag = dscg_tkdrug_flag;
        }

        public String getMatn_fee_flag() {
            return matn_fee_flag;
        }

        public void setMatn_fee_flag(String matn_fee_flag) {
            this.matn_fee_flag = matn_fee_flag;
        }

        public String getSetl_id() {
            return setl_id;
        }

        public void setSetl_id(String setl_id) {
            this.setl_id = setl_id;
        }

        public String getMedins_list_name() {
            return medins_list_name;
        }

        public void setMedins_list_name(String medins_list_name) {
            this.medins_list_name = medins_list_name;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getUpdateDT() {
            return UpdateDT;
        }

        public void setUpdateDT(String UpdateDT) {
            this.UpdateDT = UpdateDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String CreateUser) {
            this.CreateUser = CreateUser;
        }

        public String getUpdateUser() {
            return UpdateUser;
        }

        public void setUpdateUser(String UpdateUser) {
            this.UpdateUser = UpdateUser;
        }

        public String getUnt() {
            return unt;
        }

        public void setUnt(String unt) {
            this.unt = unt;
        }

        public String getUsed_mtd() {
            return used_mtd;
        }

        public void setUsed_mtd(String used_mtd) {
            this.used_mtd = used_mtd;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getElectronicPrescriptionNo() {
            return electronicPrescriptionNo;
        }

        public void setElectronicPrescriptionNo(String electronicPrescriptionNo) {
            this.electronicPrescriptionNo = electronicPrescriptionNo;
        }

        public String getSpec() {
            return spec;
        }

        public void setSpec(String spec) {
            this.spec = spec;
        }
    }

    //获取未结算门诊信息
    public static void sendUnsettledOutpatientInfoRequest_user(final String TAG,String ipt_otp_no,final CustomerJsonCallBack<UnsettledOutpatientInfoModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ipt_otp_no", ipt_otp_no);
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_UNSETTLEDOUTPATIENTINFO_URL, jsonObject.toJSONString(), callback);
    }
}
