package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.constant.Constants;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：智慧药房-商户-设置-关于我们
 * ================================================
 */
public class AboutUsActivity_ZhyfManage extends BaseActivity {
    private LinearLayout mLlYhxy;
    private LinearLayout mLlYszc;
    private LinearLayout mLlPhone;
    private TextView mTvPhone;

    String phone="13709291044";
    private CustomerDialogUtils customerDialogUtils;
    private int from;

    @Override
    public int getLayoutId() {
        return R.layout.activity_aboutus_zhyf_manage;
    }

    @Override
    public void initView() {
        super.initView();
        mLlYhxy = findViewById(R.id.ll_yhxy);
        mLlYszc = findViewById(R.id.ll_yszc);
        mLlPhone = findViewById(R.id.ll_phone);
        mTvPhone = findViewById(R.id.tv_phone);
    }

    @Override
    public void initData() {
        from=getIntent().getIntExtra("from", 1);
        super.initData();

        mTvPhone.setText(phone);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mLlYhxy.setOnClickListener(this);
        mLlYszc.setOnClickListener(this);
        mLlPhone.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.ll_yhxy://用户协议
                MyWebViewActivity.newIntance(AboutUsActivity_ZhyfManage.this, "用户协议",
                        Constants.BASE_URL+(1==from?Constants.WebUrl.STOREUSERAGREEMENT_URL:Constants.WebUrl.PLATFORMSERVICEAGREEMENT_URL),
                        true, false);
                break;
            case R.id.ll_yszc://隐私政策
                MyWebViewActivity.newIntance(AboutUsActivity_ZhyfManage.this, "隐私政策",
                        Constants.BASE_URL+Constants.WebUrl.STOREPRIVACYPOLICY_URL,
                        true, false);
                break;
            case R.id.ll_phone://联系电话
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(AboutUsActivity_ZhyfManage.this, "提示", "是否拨打电话" +phone + "?", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {}

                    @Override
                    public void onBtn02Click() {
                        RxTool.takePhone(AboutUsActivity_ZhyfManage.this,phone);
                    }
                });

                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    //from 1:来自用户端 2.来自商户端
    public static void newIntance(Context context,int from) {
        Intent intent = new Intent(context, AboutUsActivity_ZhyfManage.class);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }
}
