package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.Html;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.SettleInApplyResultModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-28
 * 描    述：账户注销
 * ================================================
 */
public class AccountCancellationActivity extends SettleInApplyActivity{

    @Override
    public void initData() {
        super.initData();

        mEtRealName.setEnabled(false);
        mEtPhone.setEnabled(false);
        mEtSettlBankcard.setEnabled(false);
        mEtSettlBank.setEnabled(false);

        mIvAddLicense01.setEnabled(false);
        mIvDelete01.setEnabled(false);

        mIvAddLicense02.setEnabled(false);
        mIvDelete02.setEnabled(false);

        mIvAddLicense03.setEnabled(false);
        mIvDelete03.setEnabled(false);

        mIvAddLicense04.setEnabled(false);
        mIvDelete04.setEnabled(false);

        mIvAddLicense05.setEnabled(false);
        mIvDelete05.setEnabled(false);

        mIvAddLicense06.setEnabled(false);
        mIvDelete06.setEnabled(false);

        mIvAddLicense07.setEnabled(false);
        mIvDelete07.setEnabled(false);

        mIvAddLicense08.setEnabled(false);
        mIvDelete08.setEnabled(false);

        mIvAddLicense09.setEnabled(false);
        mIvDelete09.setEnabled(false);

        mIvAddLicense10.setEnabled(false);
        mIvDelete10.setEnabled(false);

        mFlCb.setEnabled(false);

        mTvSubmit.setText("确认注销");

        showWaitDialog();
        getPagerData();
    }

    //获取管理员审核数据
    protected void getPagerData() {
        SettleInApplyResultModel.sendSettleInApplyResultRequest(TAG, new CustomerJsonCallBack<SettleInApplyResultModel>() {
            @Override
            public void onRequestError(SettleInApplyResultModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(SettleInApplyResultModel returnData) {
                hideWaitDialog();
                if (returnData.getData() != null && returnData.getData().get(0) != null) {
                    //2022-09-28 请求后台数据 获取审核的结果
                    setData(returnData.getData().get(0));
                }
            }
        });
    }

    //设置数据(管理员)
    protected void setData(SettleInApplyResultModel.DataBean dataBean) {
        mEtRealName.setText(EmptyUtils.strEmpty(dataBean.getDirector()));
        mEtPhone.setText(EmptyUtils.strEmpty(dataBean.getFixmedins_phone()));
        mEtSettlBankcard.setText(EmptyUtils.strEmpty(dataBean.getSettlementBankCard()));
        mEtSettlBank.setText(EmptyUtils.strEmpty(dataBean.getBank()));
        //法人身份证正面照片
        if (!EmptyUtils.isEmpty(dataBean.getFrontAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, dataBean.getFrontAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic01,mIvPic01,mIvAddLicense01,mIvAddPhoto01,dataBean.getFrontAccessoryId(), dataBean.getFrontAccessoryUrl(), width, height);
                }
            });
        }
        //法人身份证反面照片
        if (!EmptyUtils.isEmpty(dataBean.getBackAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, dataBean.getBackAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic02,mIvPic02,mIvAddLicense02,mIvAddPhoto02,dataBean.getBackAccessoryId(), dataBean.getBackAccessoryUrl(), width, height);
                }
            });
        }
        //上传医疗机构执业许可证或营业执照
        if (!EmptyUtils.isEmpty(dataBean.getBusinessLicenseAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, dataBean.getBusinessLicenseAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic03,mIvPic03,mIvAddLicense03,mIvAddPhoto03,dataBean.getBusinessLicenseAccessoryId(), dataBean.getBusinessLicenseAccessoryUrl(), width, height);
                }
            });
        }
        //药品经营许可证
        if (!EmptyUtils.isEmpty(dataBean.getDrugBLAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, dataBean.getDrugBLAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic04,mIvPic04,mIvAddLicense04,mIvAddPhoto04,dataBean.getDrugBLAccessoryId(), dataBean.getDrugBLAccessoryUrl(), width, height);
                }
            });
        }
        //医疗器械经营许可证
        if (!EmptyUtils.isEmpty(dataBean.getMedicalDeviceBLAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, dataBean.getMedicalDeviceBLAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic05,mIvPic05,mIvAddLicense05,mIvAddPhoto05,dataBean.getMedicalDeviceBLAccessoryId(), dataBean.getMedicalDeviceBLAccessoryUrl(), width, height);
                }
            });
        }
        //GS认证证书
        if (!EmptyUtils.isEmpty(dataBean.getGSCertificateAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, dataBean.getGSCertificateAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic06,mIvPic06,mIvAddLicense06,mIvAddPhoto06,dataBean.getGSCertificateAccessoryId(), dataBean.getGSCertificateAccessoryUrl(), width, height);
                }
            });
        }
        //开户许可证
        if (!EmptyUtils.isEmpty(dataBean.getAccountLicenceAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, dataBean.getAccountLicenceAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic07,mIvPic07,mIvAddLicense07,mIvAddPhoto07,dataBean.getAccountLicenceAccessoryId(), dataBean.getAccountLicenceAccessoryUrl(), width, height);
                }
            });
        }
        //门头照片
        if (!EmptyUtils.isEmpty(dataBean.getDoorHeaderAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, dataBean.getDoorHeaderAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic08,mIvPic08,mIvAddLicense08,mIvAddPhoto08,dataBean.getDoorHeaderAccessoryId(), dataBean.getDoorHeaderAccessoryUrl(), width, height);
                }
            });
        }
        //内景照片1
        if (!EmptyUtils.isEmpty(dataBean.getInteriorOneAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, dataBean.getInteriorOneAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic09,mIvPic09,mIvAddLicense09,mIvAddPhoto09,dataBean.getInteriorOneAccessoryId(), dataBean.getInteriorOneAccessoryUrl(), width, height);
                }
            });
        }
        //内景照片2
        if (!EmptyUtils.isEmpty(dataBean.getInteriorTwoAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, dataBean.getInteriorTwoAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic10,mIvPic10,mIvAddLicense10,mIvAddPhoto10,dataBean.getInteriorTwoAccessoryId(), dataBean.getInteriorTwoAccessoryUrl(), width, height);
                }
            });
        }
    }

    @Override
    protected void submit() {
        showTipDialog("注销在线购药请联系平台");
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, AccountCancellationActivity.class);
        context.startActivity(intent);
    }
}
