package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.AddDirModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.DirDefaultIconModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;
import com.jrdz.zhyb_android.utils.ChineseFilter;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;

import java.math.BigDecimal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-09-30
 * 描    述：添加目录
 * ================================================
 */
public class AddDirActivity extends BaseActivity implements CompressUploadSinglePicUtils_zhyf.IChoosePic {
    public final String TAG_SELECT_ADD_DIR = "1";
    public final String TAG_SELECT_ADD_HORIZONTAL = "2";

    private ImageView mIvAddPic;
    private EditText mEtDirName;
    private RelativeLayout mLlHorizontalPic;
    private ShapeFrameLayout mSflHorizontalPic;
    private LinearLayout mLlAddHorizontalPic;
    private FrameLayout mFlHorizontalPic;
    private ImageView mIvHorizontalPic;
    private ImageView mIvDelete;
    private ShapeTextView mTvSave;
    private ShapeTextView mTvSaveAddGoods;

    private CompressUploadSinglePicUtils_zhyf compressUploadSinglePicUtils;
    private int from;

    @Override
    public int getLayoutId() {
        return R.layout.activity_add_dir;
    }

    @Override
    public void initView() {
        super.initView();
        mIvAddPic = findViewById(R.id.iv_add_pic);
        mEtDirName = findViewById(R.id.et_dir_name);
        mLlHorizontalPic = findViewById(R.id.ll_horizontal_pic);
        mSflHorizontalPic = findViewById(R.id.sfl_horizontal_pic);
        mLlAddHorizontalPic = findViewById(R.id.ll_add_horizontal_pic);
        mFlHorizontalPic = findViewById(R.id.fl_horizontal_pic);
        mIvHorizontalPic = findViewById(R.id.iv_horizontal_pic);
        mIvDelete = findViewById(R.id.iv_delete);
        mTvSave = findViewById(R.id.tv_save);
        mTvSaveAddGoods = findViewById(R.id.tv_save_add_goods);
    }

    @Override
    public void initData() {
        from=getIntent().getIntExtra("from", 1);
        super.initData();

        if (1==from){
            mTvSave.setVisibility(View.VISIBLE);

            mTvSave.setText("保存");
            mTvSaveAddGoods.setText("保存并添加商品");
        }else {
            mTvSave.setVisibility(View.GONE);

            mTvSaveAddGoods.setText("添加");
        }

        mEtDirName.setFilters(new InputFilter[]{new ChineseFilter(), new InputFilter.LengthFilter(5)});

        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);

        showWaitDialog();
        getDefaultPic();
    }

    //获取目录的默认图片
    private void getDefaultPic() {
        DirDefaultIconModel.sendDefaultIconRequest(TAG, new CustomerJsonCallBack<DirDefaultIconModel>() {
            @Override
            public void onRequestError(DirDefaultIconModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(DirDefaultIconModel returnData) {
                hideWaitDialog();
                DirDefaultIconModel.DataBean data = returnData.getData();
                if (data!=null){
                    mIvAddPic.setTag(R.id.tag_1, data.getDefaultIconAccessoryId());
                    mIvAddPic.setTag(R.id.tag_2, data.getDefaultIconAccessoryUrl());
                    GlideUtils.loadImg(data.getDefaultIconAccessoryUrl(), mIvAddPic, R.drawable.ic_add_dir_bg, new CircleCrop());
                }
            }
        });
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mIvAddPic.setOnClickListener(this);
        mLlAddHorizontalPic.setOnClickListener(this);
        mIvDelete.setOnClickListener(this);
        mTvSave.setOnClickListener(this);
        mTvSaveAddGoods.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.iv_add_pic://添加目录图片
                KeyboardUtils.hideSoftInput(AddDirActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_ADD_DIR, CompressUploadSinglePicUtils_zhyf.PIC_APPLY_ADD_DIR_TAG);
                break;
            case R.id.ll_add_horizontal_pic://添加横版图
                KeyboardUtils.hideSoftInput(AddDirActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_ADD_HORIZONTAL, CompressUploadSinglePicUtils_zhyf.PIC_APPLY_ADD_HORIZONTAL_TAG);
                break;
            case R.id.iv_delete://删除横版图
                mLlAddHorizontalPic.setVisibility(View.VISIBLE);
                mLlAddHorizontalPic.setEnabled(true);

                mIvHorizontalPic.setTag(R.id.tag_1, "");
                mIvHorizontalPic.setTag(R.id.tag_2, "");
                mFlHorizontalPic.setVisibility(View.GONE);
                break;
            case R.id.tv_save://保存
                save(1);
                break;
            case R.id.tv_save_add_goods://保存并添加商品
                if (1==from){
                    save(2);
                }else {
                    save(1);
                }
                break;
        }
    }

    //保存  保存并添加商品
    private void save(int tag) {
        if (EmptyUtils.isEmpty(mEtDirName.getText().toString()) || mEtDirName.getText().length() < 2) {
            showShortToast("请输入目录名称，2~5个汉字");
            return;
        }

//        if (null == mIvHorizontalPic.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvHorizontalPic.getTag(R.id.tag_2)))) {
//            showShortToast("请上传横版图");
//            return;
//        }

        // 2022-09-30 请求接口 上传目录信息
        showWaitDialog();
        AddDirModel.sendAddCataRequest(TAG, mEtDirName.getText().toString(), mIvAddPic.getTag(R.id.tag_1), mIvHorizontalPic.getTag(R.id.tag_1),
                "1", "2", new CustomerJsonCallBack<AddDirModel>() {
                    @Override
                    public void onRequestError(AddDirModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(AddDirModel returnData) {
                        hideWaitDialog();
                        PhaSortModel.DataBean data = returnData.getData();
                        if ( data!= null) {
                            //上传目录成功
                            data.setSelfTag(1);
                            MsgBus.sendHomePhaSortRefresh().post(data);
                            if (2 == tag) {
                                //2022-09-30 跳转添加商品页面
                                SpecialAreaAddGoodsActivity.newIntance(AddDirActivity.this,"1",data.getCatalogueId(),data.getCatalogueName());
                            }
                            goFinish();
                        }
                    }
                });
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String mTag, String accessoryId, String url, int width, int height) {
        switch (mTag) {
            case TAG_SELECT_ADD_DIR://添加目录图片
                selectPicAddDirSuccess(accessoryId, url, width, height);
                break;
            case TAG_SELECT_ADD_HORIZONTAL://添加横版图
                selectPicAddHorizontalSuccess(accessoryId, url, width, height);
                break;
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }

    //添加目录图片
    private void selectPicAddDirSuccess(String accessoryId, String url, int width, int height) {
        mIvAddPic.setTag(R.id.tag_1, accessoryId);
        mIvAddPic.setTag(R.id.tag_2, url);
        GlideUtils.loadImg(url, mIvAddPic, R.drawable.ic_add_dir_bg, new CircleCrop());
    }

    //添加横版图
    private void selectPicAddHorizontalSuccess(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic.getLayoutParams();
            if (radio >= 2.125) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_510));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_510))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_240) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_240));
            }

            mIvHorizontalPic.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic);

            mLlAddHorizontalPic.setVisibility(View.GONE);
            mLlAddHorizontalPic.setEnabled(false);

            mIvHorizontalPic.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic.setTag(R.id.tag_2, url);
            mFlHorizontalPic.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }
    }

    //from 1来自首页 2来自店铺装修
    public static void newIntance(Context context,int from) {
        Intent intent = new Intent(context, AddDirActivity.class);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }
}
