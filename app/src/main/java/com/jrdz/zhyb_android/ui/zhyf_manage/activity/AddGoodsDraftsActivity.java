package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.frame.compiler.widget.customPop.CustomerBottomListPop;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.StoreStatusModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-24
 * 描    述：添加商品--草稿箱
 * ================================================
 */
public class AddGoodsDraftsActivity extends SpecialAreaAddGoodsActivity {
    protected TextView mTvPostion;
    protected TextView mTvSortName;
    //保存按钮
    protected ShapeTextView mTvSave;
    protected ShapeTextView mTvSavePull;

    private WheelUtils wheelUtils;
    private int choosePos1 = 0;
    protected List<PhaSortModel.DataBean> phaSortDatas,sortDatas;

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_goods;
    }

    @Override
    public void initView() {
        super.initView();
        mTvPostion = findViewById(R.id.tv_postion);
        mTvSortName = findViewById(R.id.tv_sort_name);

        mTvSave = findViewById(R.id.tv_save);
        mTvSavePull = findViewById(R.id.tv_save_pull);
    }

    @Override
    public void initData() {
        mTvBannerTime.setTag("3");
        mTvBannerTime.setText("3S");
        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvPostion.setOnClickListener(this);
        mTvSortName.setOnClickListener(this);
        //保存 保存并上架 点击事件
        mTvSave.setOnClickListener(this);
        mTvSavePull.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_postion://位置
                KeyboardUtils.hideSoftInput(AddGoodsDraftsActivity.this);
                if (customerBottomListPop == null) {
                    customerBottomListPop = new CustomerBottomListPop(AddGoodsDraftsActivity.this, AddGoodsDraftsActivity.this);
                }
                customerBottomListPop.setDatas("21", "", mTvBannerTime.getText().toString(), CommonlyUsedDataUtils.getInstance().getGoosPostionData());
                customerBottomListPop.showPopupWindow();
                break;
            case R.id.tv_sort_name://名称
                KeyboardUtils.hideSoftInput(AddGoodsDraftsActivity.this);
                if (mTvPostion.getTag() == null || EmptyUtils.isEmpty(String.valueOf(mTvPostion.getTag()))) {
                    showShortToast("请先选择位置");
                    return;
                }

                if (wheelUtils == null) {
                    wheelUtils = new WheelUtils();
                }
                wheelUtils.showWheel(AddGoodsDraftsActivity.this, "", choosePos1, "1".equals(String.valueOf(mTvPostion.getTag()))?phaSortDatas:sortDatas,
                        new WheelUtils.WheelClickListener<PhaSortModel.DataBean>() {
                            @Override
                            public void onchooseDate(int choosePos, PhaSortModel.DataBean dateInfo) {
                                choosePos1 = choosePos;
                                mTvSortName.setTag(dateInfo.getCatalogueId());
                                mTvSortName.setText(EmptyUtils.strEmpty(dateInfo.getCatalogueName()));
                            }
                        });
                break;
            case R.id.tv_save://保存
                onSave();
                break;
            case R.id.tv_save_pull://保存并上架
                onPull();
                break;
        }
    }

    @Override
    public void onItemClick(String tag, StoreStatusModel str) {
        super.onItemClick(tag, str);
        switch (tag) {
            case "21"://位置
                mTvPostion.setTag(str.getId());
                mTvPostion.setText(str.getText());
                mTvSortName.setText("");
                choosePos1=0;
                switch (str.getId()){
                    case "1":
                        if (phaSortDatas==null){
                            showWaitDialog();
                            getHomeSortData();
                        }
                        break;
                    case "2":
                        if (sortDatas==null){
                            showWaitDialog();
                            getSortData();
                        }
                        break;
                }
                break;
        }
    }
    //获取首页分类数据
    protected void getHomeSortData() {
        PhaSortModel.sendHomeSortRequest(TAG, "1", new CustomerJsonCallBack<PhaSortModel>() {
            @Override
            public void onRequestError(PhaSortModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(PhaSortModel returnData) {
                hideWaitDialog();
                phaSortDatas = returnData.getData();
            }
        });
    }
    //获取分类数据
    protected void getSortData() {
        PhaSortModel.sendSortRequest(TAG, "1", new CustomerJsonCallBack<PhaSortModel>() {
            @Override
            public void onRequestError(PhaSortModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(PhaSortModel returnData) {
                hideWaitDialog();
                sortDatas = returnData.getData();
            }
        });
    }

    //保存
    protected void onSave() {
        //2022-10-08 调用接口 成功之后跳转成功页面
        String enterpriseName=(null==itemData)?"":(("1".equals(itemData.getIsPlatformDrug())||"1".equals(itemData.getDrugsClassification()))?itemData.getEnterpriseName():mEtProdentpName.getText().toString());
        String storageConditions=(null==itemData)?"":(("2".equals(itemData.getDrugsClassification()))?itemData.getStorageConditions():mEtStorageConditions.getText().toString());
        Object ItemType=(null==itemData)?"":(("1".equals(itemData.getIsPlatformDrug())||"1".equals(itemData.getDrugsClassification()))?mTvType.getTag():itemData.getItemType());
        String efccAtd=(null==itemData)?"":(("1".equals(itemData.getIsPlatformDrug())||"1".equals(itemData.getDrugsClassification()))?mEtFunctions.getText().toString():itemData.getEfccAtd());
        String usualWay=(null==itemData)?"":(("1".equals(itemData.getIsPlatformDrug())||"1".equals(itemData.getDrugsClassification()))?mEtCommonUsage.getText().toString():itemData.getUsualWay());
        String associatedDiagnostics="";
        if (null!=itemData&&"1".equals(MechanismInfoUtils.getFixmedinsType()) && "1".equals(itemData.getIsPlatformDrug())) {//医院并且是平台药 才需要获取关联诊断
            associatedDiagnostics= JSON.toJSONString(relationDisetypeAdapter.getData());
        }
        String associatedDiseases="";
        if (null!=itemData&&"1".equals(itemData.getDrugsClassification())) {//只要是西药中成药 才需要获取关联疾病
            //关联疾病数据
            List<String> relationDiseDatas = relationDiseAdapter.getData();

            for (int i = 0, size = relationDiseDatas.size(); i < size; i++) {
                if (i == size - 1) {
                    associatedDiseases += relationDiseDatas.get(i);
                } else {
                    associatedDiseases += relationDiseDatas.get(i) + "∞#";
                }
            }
        }
        String brand=(null==itemData)?"":(("1".equals(itemData.getIsPlatformDrug())||"1".equals(itemData.getDrugsClassification()))?itemData.getBrand():mEtBrand.getText().toString());
        String packaging=(null==itemData)?"":(("1".equals(itemData.getIsPlatformDrug())||"1".equals(itemData.getDrugsClassification()))?itemData.getPackaging():mEtPacking.getText().toString());
        String applicableScope=(null==itemData)?"":(("2".equals(itemData.getDrugsClassification()))?mEtApplyRange.getText().toString():itemData.getApplicableScope());
        String usageDosage=(null==itemData)?"":(("2".equals(itemData.getDrugsClassification())||"4".equals(itemData.getDrugsClassification()))?mEtCommonUsage.getText().toString():itemData.getUsageDosage());
        String usagemethod=(null==itemData)?"":(("3".equals(itemData.getDrugsClassification()))?mEtCommonUsage.getText().toString():itemData.getUsagemethod());

        showWaitDialog();
        BaseModel.sendAddGoodsRequest(TAG, null == mTvPostion.getTag() ? "" : String.valueOf(mTvPostion.getTag()),
                null == mTvSortName.getTag() ? "" : String.valueOf(mTvSortName.getTag()), mTvSortName.getText().toString(),
                mIvHorizontalPic01.getTag(R.id.tag_1), mIvHorizontalPic02.getTag(R.id.tag_1), mIvHorizontalPic03.getTag(R.id.tag_1), mTvBannerTime.getTag(),
                null==itemData?"":mEtPromotion.getTag(), null==itemData?"":mEtNumPromotion.getText().toString(), null==itemData?"":mEtEstimatePrice.getText().toString(),
                null==itemData?"":mEtStoreSimilarity.getText().toString(), null==itemData?"":itemData.getIsPlatformDrug(), null==itemData?"":itemData.getDrugsClassification(),
                null==itemData?"":itemData.getDrugsClassificationName(), null==itemData?"":itemData.getMIC_Code(), null==itemData?"":mEtDrugName.getText().toString(),
                null==itemData?"":mEtDrugId.getText().toString(), null==itemData?"":mEtDrugProductName.getText().toString(), null==itemData?"":mEtSpecs.getText().toString(), enterpriseName, storageConditions,
                null==itemData?"":mEtValidity.getText().toString(), null==itemData?"":mEtNum.getText().toString(), null==itemData?"":mEtNumPrice.getText().toString(), null==itemData?"":mIvDrugManual.getTag(R.id.tag_1),
                null==itemData?"":mIvCommodityDetails01.getTag(R.id.tag_1), null==itemData?"":mIvCommodityDetails02.getTag(R.id.tag_1), null==itemData?"":mIvCommodityDetails03.getTag(R.id.tag_1),
                null==itemData?"":mIvCommodityDetails04.getTag(R.id.tag_1), null==itemData?"":mIvCommodityDetails05.getTag(R.id.tag_1), "0", ItemType,
                null==itemData?"":itemData.getApprovalNumber(), null==itemData?"":itemData.getRegDosform(), efccAtd, usualWay, associatedDiagnostics,
                associatedDiseases, brand, null==itemData?"":itemData.getRegistrationCertificateNo(), null==itemData?"":itemData.getProductionLicenseNo(),
                packaging, applicableScope, usageDosage, null==itemData?"":itemData.getPrecautions(), usagemethod,
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("添加商品成功");

                        MsgBus.sendAddGoodsRefresh().post("1");
                        AddGoodsSuccessActivity.newIntance(AddGoodsDraftsActivity.this,"1","","","");
                        goFinish();
                    }
                });
    }

    //保存并上架
    @Override
    protected void onPull() {
        if (EmptyUtils.isEmpty(mTvPostion.getText().toString())) {
            showShortToast("请选择位置");
            return;
        }
        if (EmptyUtils.isEmpty(mTvSortName.getText().toString())) {
            showShortToast("请选择名称");
            return;
        }
        super.onPull();
    }

    @Override
    protected void requestData() {
        if (itemData == null) {
            showShortToast("页面数据有误，请重新进入页面");
            return;
        }
        //2022-10-08 调用接口 成功之后跳转成功页面
        String enterpriseName = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? itemData.getEnterpriseName() : mEtProdentpName.getText().toString();
        String storageConditions = ("2".equals(itemData.getDrugsClassification())) ? itemData.getStorageConditions() : mEtStorageConditions.getText().toString();
        Object ItemType = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? mTvType.getTag() : itemData.getItemType();
        String efccAtd = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? mEtFunctions.getText().toString() : itemData.getEfccAtd();
        String usualWay = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? mEtCommonUsage.getText().toString() : itemData.getUsualWay();
        String associatedDiagnostics = "";
        if ("1".equals(MechanismInfoUtils.getFixmedinsType()) && "1".equals(itemData.getIsPlatformDrug())) {//医院并且是平台药 才需要获取关联诊断
            associatedDiagnostics = JSON.toJSONString(relationDisetypeAdapter.getData());
            Log.e("666666", "associatedDiagnostics:===" + associatedDiagnostics);
        }
        String associatedDiseases = "";
        if ("1".equals(itemData.getDrugsClassification())) {//只要是西药中成药 才需要获取关联疾病
            //关联疾病数据
            List<String> relationDiseDatas = relationDiseAdapter.getData();

            for (int i = 0, size = relationDiseDatas.size(); i < size; i++) {
                if (i == size - 1) {
                    associatedDiseases += relationDiseDatas.get(i);
                } else {
                    associatedDiseases += relationDiseDatas.get(i) + "∞#";
                }
            }
            Log.e("666666", "associatedDiagnostics:===" + associatedDiseases);
        }
        String brand = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? itemData.getBrand() : mEtBrand.getText().toString();
        String packaging = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? itemData.getPackaging() : mEtPacking.getText().toString();
        String applicableScope = ("2".equals(itemData.getDrugsClassification())) ? mEtApplyRange.getText().toString() : itemData.getApplicableScope();
        String usageDosage = ("2".equals(itemData.getDrugsClassification()) || "4".equals(itemData.getDrugsClassification())) ? mEtCommonUsage.getText().toString() : itemData.getUsageDosage();
        String usagemethod = ("3".equals(itemData.getDrugsClassification())) ? mEtCommonUsage.getText().toString() : itemData.getUsagemethod();

        showWaitDialog();
        BaseModel.sendAddGoodsRequest(TAG, null == mTvPostion.getTag() ? "" : String.valueOf(mTvPostion.getTag()),
                null == mTvSortName.getTag() ? "" : String.valueOf(mTvSortName.getTag()),mTvSortName.getText().toString(),
                mIvHorizontalPic01.getTag(R.id.tag_1), mIvHorizontalPic02.getTag(R.id.tag_1), mIvHorizontalPic03.getTag(R.id.tag_1),
                mTvBannerTime.getTag(), mEtPromotion.getTag(), mEtNumPromotion.getText().toString(), mEtEstimatePrice.getText().toString(),
                mEtStoreSimilarity.getText().toString(), itemData.getIsPlatformDrug(), itemData.getDrugsClassification(), itemData.getDrugsClassificationName(),
                itemData.getMIC_Code(), mEtDrugName.getText().toString(), mEtDrugId.getText().toString(), mEtDrugProductName.getText().toString(), mEtSpecs.getText().toString(),
                enterpriseName, storageConditions, mEtValidity.getText().toString(), mEtNum.getText().toString(), mEtNumPrice.getText().toString(), mIvDrugManual.getTag(R.id.tag_1),
                mIvCommodityDetails01.getTag(R.id.tag_1), mIvCommodityDetails02.getTag(R.id.tag_1), mIvCommodityDetails03.getTag(R.id.tag_1), mIvCommodityDetails04.getTag(R.id.tag_1),
                mIvCommodityDetails05.getTag(R.id.tag_1), "1", ItemType, itemData.getApprovalNumber(), itemData.getRegDosform(), efccAtd, usualWay, associatedDiagnostics,
                associatedDiseases, brand, itemData.getRegistrationCertificateNo(), itemData.getProductionLicenseNo(),
                packaging, applicableScope, usageDosage, itemData.getPrecautions(), usagemethod,
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("添加商品成功");

                        MsgBus.sendAddGoodsRefresh().post("1");
                        AddGoodsSuccessActivity.newIntance(AddGoodsDraftsActivity.this, "2","","","");
                        goFinish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (phaSortDatas!=null){
            phaSortDatas.clear();
            phaSortDatas=null;
        }
        if (sortDatas!=null){
            sortDatas.clear();
            sortDatas=null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, AddGoodsDraftsActivity.class);
        context.startActivity(intent);
    }
}
