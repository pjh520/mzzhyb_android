package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-08
 * 描    述：
 * ================================================
 */
public class AddGoodsSuccessActivity extends BaseActivity {
    private TextView mTvResult;
    private TextView mTvDescribe01;
    private ShapeTextView mTvContinueAddGoods;
    private String tag,GoodsLocation,catalogueId,catalogueName;

    @Override
    public int getLayoutId() {
        return R.layout.activity_add_goods_success;
    }

    @Override
    public void initView() {
        super.initView();
        mTvResult = findViewById(R.id.tv_result);
        mTvDescribe01 = findViewById(R.id.tv_describe01);
        mTvContinueAddGoods = findViewById(R.id.tv_continue_add_goods);
    }

    @Override
    public void initData() {
        tag=getIntent().getStringExtra("tag");
        GoodsLocation=getIntent().getStringExtra("GoodsLocation");
        catalogueId=getIntent().getStringExtra("catalogueId");
        catalogueName=getIntent().getStringExtra("catalogueName");
        super.initData();
        if ("1".equals(tag)){
            mTvResult.setText("保存成功");
            mTvDescribe01.setText("您的商品已保存进草稿箱");
            mTvContinueAddGoods.setText("返回");
        } else if ("2".equals(tag)){
            mTvResult.setText("保存并上架成功");
            mTvDescribe01.setText("您的商品已保存并上架店铺");
            mTvContinueAddGoods.setText("继续新增产品");
        }else if ("3".equals(tag)){
            mTvResult.setText("上架成功");
            mTvDescribe01.setText("您的商品已保存并上架店铺");
            mTvContinueAddGoods.setText("继续新增上架产品");
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvContinueAddGoods.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_continue_add_goods://继续添加商品
                switch (mTvContinueAddGoods.getText().toString()){
                    case "返回":
                        break;
                    case "继续新增产品":
                        AddGoodsDraftsActivity.newIntance(AddGoodsSuccessActivity.this);
                        break;
                    case "继续新增上架产品":
                        SpecialAreaAddGoodsActivity.newIntance(this,GoodsLocation,catalogueId,catalogueName);
                        break;
                }

                goFinish();
                break;
        }
    }

    //tag 1保存成功 2 保存并上架成功 3 上架成功
    public static void newIntance(Context context,String tag,String GoodsLocation,String catalogueId,String catalogueName) {
        Intent intent = new Intent(context, AddGoodsSuccessActivity.class);
        intent.putExtra("tag", tag);
        intent.putExtra("GoodsLocation", GoodsLocation);
        intent.putExtra("catalogueId", catalogueId);
        intent.putExtra("catalogueName", catalogueName);
        context.startActivity(intent);
    }
}
