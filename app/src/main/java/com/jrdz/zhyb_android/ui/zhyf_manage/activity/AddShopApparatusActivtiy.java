package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;

import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-19
 * 描    述：新增--店铺药品库--医疗器械
 * ================================================
 */
public class AddShopApparatusActivtiy extends AddShopWestDragActivtiy {
    protected EditText mEtBrand;
    protected EditText mEtRegcertNo;
    protected EditText mEtFilingNo;
    protected EditText mEtPacking;
    protected EditText mEtApplyRange;
    protected EditText mEtCareful;

    @Override
    public int getLayoutId() {
        return R.layout.activtiy_add_shop_apparatus;
    }

    @Override
    public void initView() {
        super.initView();
        mEtBrand = findViewById(R.id.et_brand);
        mEtRegcertNo = findViewById(R.id.et_regcert_no);
        mEtFilingNo = findViewById(R.id.et_filing_no);
        mEtPacking = findViewById(R.id.et_packing);
        mEtApplyRange = findViewById(R.id.et_apply_range);
        mEtCareful = findViewById(R.id.et_careful);

        //模拟新增数据
//        mEtDrugName.setText("医用棉签1");
//        mEtSpec.setText("100mg");
//        mEtProdentpName.setText("通药制药集团股份有限公司");
//        mEtStorageConditions.setText("贮存条件1111");
//
//        mEtBrand.setText("可可康");
//        mEtRegcertNo.setText("鲁青械备20160233号");
//        mEtFilingNo.setText("鲁青食药监械生产备20140004号");
//        mEtPacking.setText("10cm*50支");
//        mEtApplyRange.setText("适用范围1111");
    }

    @Override
    public void initData() {
        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);
    }

    //添加  继续添加
    @Override
    protected void onAdd(String tag) {
        if (EmptyUtils.isEmpty(mEtDrugName.getText().toString())) {
            showShortToast("请输入名称");
            return;
        }
        if (EmptyUtils.isEmpty(mEtBrand.getText().toString())) {
            showShortToast("请输入品牌");
            return;
        }
        if (EmptyUtils.isEmpty(mEtSpec.getText().toString())) {
            showShortToast("请输入规格");
            return;
        }
        if (EmptyUtils.isEmpty(mEtPacking.getText().toString())) {
            showShortToast("请输入包装");
            return;
        }
        if (EmptyUtils.isEmpty(mEtProdentpName.getText().toString())) {
            showShortToast("请输入生产企业");
            return;
        }
        if (EmptyUtils.isEmpty(mEtApplyRange.getText().toString())) {
            showShortToast("请输入适用范围");
            return;
        }
        if (EmptyUtils.isEmpty(mEtCommonUsage.getText().toString())) {
            showShortToast("请输入用法用量");
            return;
        }
        if (EmptyUtils.isEmpty(mEtValidity.getText().toString()) || "0".equals(mEtValidity.getText().toString())) {
            showShortToast("请输入正确的有效期");
            return;
        }
        if (EmptyUtils.isEmpty(mEtNum.getText().toString()) || "0".equals(mEtNum.getText().toString())) {
            showShortToast("请输入正确的库存数量");
            return;
        }
        if (EmptyUtils.isEmpty(mEtNumPrice.getText().toString()) || "0".equals(mEtNumPrice.getText().toString())) {
            showShortToast("请输入正确的价格");
            return;
        }

        if (null == mIvDrugManual.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvDrugManual.getTag(R.id.tag_2)))) {
            showShortToast("请上传产品说明书");
            return;
        }

        int selectCommodityDetailsPic = 0;
        if (null != mIvCommodityDetails01.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails01.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails02.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails02.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails03.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails03.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails04.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails04.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails05.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails05.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (selectCommodityDetailsPic < 3) {
            showShortToast("请上传最少三张，至多五张产品详情图片");
            return;
        }
        // 2022-10-08 调用接口 成功之后跳转成功页面
        showWaitDialog();
        BaseModel.sendAddShopDrugDataRequest(TAG, "2", "医疗器械", mEtDrugName.getText().toString(), mEtSpec.getText().toString(),
                mEtProdentpName.getText().toString(), mEtStorageConditions.getText().toString(), mEtValidity.getText().toString(), mEtNum.getText().toString(),
                mEtNumPrice.getText().toString(), mIvDrugManual.getTag(R.id.tag_1), mIvCommodityDetails01.getTag(R.id.tag_1), mIvCommodityDetails02.getTag(R.id.tag_1),
                mIvCommodityDetails03.getTag(R.id.tag_1), mIvCommodityDetails04.getTag(R.id.tag_1), mIvCommodityDetails05.getTag(R.id.tag_1),
                "", "", "", "", "", "", mEtBrand.getText().toString(), mEtRegcertNo.getText().toString(),
                mEtFilingNo.getText().toString(), mEtPacking.getText().toString(), mEtApplyRange.getText().toString(), mEtCommonUsage.getText().toString(),
                mEtCareful.getText().toString(), "", new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("添加成功");
                        if ("2".equals(tag)) {
                            cleanData();
                        }else {
                            Intent intent = new Intent();
                            intent.putExtra("option_type", "add");
                            setResult(RESULT_OK, intent);
                            goFinish();
                        }
                    }
                });
    }

    @Override
    protected void cleanData() {
        mEtDrugName.setText("");
        mEtBrand.setText("");
        mEtRegcertNo.setText("");
        mEtFilingNo.setText("");
        mEtSpec.setText("");
        mEtPacking.setText("");
        mEtProdentpName.setText("");
        mEtApplyRange.setText("");
        mEtCommonUsage.setText("");
        mEtStorageConditions.setText("");
        mEtCareful.setText("");

        mEtValidity.setText("12");
        mEtNum.setText("5");
        mEtNumPrice.setText("0");

        mLlDrugManual.setVisibility(View.VISIBLE);
        mLlDrugManual.setEnabled(true);
        mIvDrugManual.setTag(R.id.tag_1, "");
        mIvDrugManual.setTag(R.id.tag_2, "");
        mFlDrugManual.setVisibility(View.GONE);

        mLlCommodityDetails01.setVisibility(View.VISIBLE);
        mLlCommodityDetails01.setEnabled(true);
        mIvCommodityDetails01.setTag(R.id.tag_1, "");
        mIvCommodityDetails01.setTag(R.id.tag_2, "");
        mFlCommodityDetails01.setVisibility(View.GONE);

        mLlCommodityDetails02.setVisibility(View.VISIBLE);
        mLlCommodityDetails02.setEnabled(true);
        mIvCommodityDetails02.setTag(R.id.tag_1, "");
        mIvCommodityDetails02.setTag(R.id.tag_2, "");
        mFlCommodityDetails02.setVisibility(View.GONE);

        mLlCommodityDetails03.setVisibility(View.VISIBLE);
        mLlCommodityDetails03.setEnabled(true);
        mIvCommodityDetails03.setTag(R.id.tag_1, "");
        mIvCommodityDetails03.setTag(R.id.tag_2, "");
        mFlCommodityDetails03.setVisibility(View.GONE);

        mLlCommodityDetails04.setVisibility(View.VISIBLE);
        mLlCommodityDetails04.setEnabled(true);
        mIvCommodityDetails04.setTag(R.id.tag_1, "");
        mIvCommodityDetails04.setTag(R.id.tag_2, "");
        mFlCommodityDetails04.setVisibility(View.GONE);

        mLlCommodityDetails05.setVisibility(View.VISIBLE);
        mLlCommodityDetails05.setEnabled(true);
        mIvCommodityDetails05.setTag(R.id.tag_1, "");
        mIvCommodityDetails05.setTag(R.id.tag_2, "");
        mFlCommodityDetails05.setVisibility(View.GONE);
    }
}
