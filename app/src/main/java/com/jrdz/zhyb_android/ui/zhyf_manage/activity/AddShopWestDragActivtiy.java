package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Intent;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.customPop.CustomerBottomListPop;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.JustifyContent;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.RelationDiseAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.StoreStatusModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;
import com.jrdz.zhyb_android.widget.MyFlexboxLayoutManager;
import com.jrdz.zhyb_android.widget.pop.AddRelationDisePop;
import com.jrdz.zhyb_android.widget.pop.UpdateNumPop;

import java.math.BigDecimal;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-18
 * 描    述：新增--店铺药品库--西药中成药
 * ================================================
 */
public class AddShopWestDragActivtiy extends BaseActivity implements CompressUploadSinglePicUtils_zhyf.IChoosePic, CustomerBottomListPop.IBottomChooseListener<StoreStatusModel> {
    public final String TAG_SELECT_DRUG_MANUAL = "4";//药品说明书
    public final String TAG_SELECT_COMMODITY_DETAILS_01 = "5";//商品详情1
    public final String TAG_SELECT_COMMODITY_DETAILS_02 = "6";//商品详情2
    public final String TAG_SELECT_COMMODITY_DETAILS_03 = "7";//商品详情3
    public final String TAG_SELECT_COMMODITY_DETAILS_04 = "8";//商品详情4
    public final String TAG_SELECT_COMMODITY_DETAILS_05 = "9";//商品详情5

    protected EditText mEtDrugName;
    protected TextView mTvType;
    protected EditText mEtAprvnoBegndate;
    protected EditText mEtRegDosform;
    protected EditText mEtSpec;
    protected EditText mEtProdentpName;
    protected EditText mEtFuncMain;
    protected EditText mEtCommonUsage;
    protected EditText mEtStorageConditions;
    protected FrameLayout mFlAddRelationDise;
    protected CustomeRecyclerView mCrvRelationDise;
    protected ShapeFrameLayout mFlReduceValidity;
    protected ShapeTextView mEtValidity;
    protected ShapeFrameLayout mFlAddValidity;
    protected ShapeFrameLayout mFlReduceNum;
    protected ShapeTextView mEtNum;
    protected ShapeFrameLayout mFlAddNum;
    protected ShapeFrameLayout mFlReducePrice;
    protected ShapeTextView mEtNumPrice;
    protected ShapeFrameLayout mFlAddPrice;
    protected LinearLayout mLlDrugManual;
    protected FrameLayout mFlDrugManual;
    protected ImageView mIvDrugManual;
    protected ImageView mIvDrugManualDelete;
    protected LinearLayout mLlCommodityDetails01;
    protected FrameLayout mFlCommodityDetails01;
    protected ImageView mIvCommodityDetails01;
    protected ImageView mIvCommodityDetailsDelete01;
    protected LinearLayout mLlCommodityDetails02;
    protected FrameLayout mFlCommodityDetails02;
    protected ImageView mIvCommodityDetails02;
    protected ImageView mIvCommodityDetailsDelete02;
    protected LinearLayout mLlCommodityDetails03;
    protected FrameLayout mFlCommodityDetails03;
    protected ImageView mIvCommodityDetails03;
    protected ImageView mIvCommodityDetailsDelete03;
    protected LinearLayout mLlCommodityDetails04;
    protected FrameLayout mFlCommodityDetails04;
    protected ImageView mIvCommodityDetails04;
    protected ImageView mIvCommodityDetailsDelete04;
    protected LinearLayout mLlCommodityDetails05;
    protected FrameLayout mFlCommodityDetails05;
    protected ImageView mIvCommodityDetails05;
    protected ImageView mIvCommodityDetailsDelete05;
    protected ShapeTextView mTvAdd;
    protected ShapeTextView mTvContinueAdd;

    protected CompressUploadSinglePicUtils_zhyf compressUploadSinglePicUtils;
    protected RelationDiseAdapter relationDiseAdapter;//关联疾病适配器
    protected CustomerBottomListPop customerBottomListPop; //选择弹框
    protected CustomerDialogUtils customerDialogUtils;
    protected AddRelationDisePop addRelationDisePop;//添加关联疾病弹框
    protected UpdateNumPop updateNumPop;

    @Override
    public int getLayoutId() {
        return R.layout.activtiy_add_shop_westdrag;
    }

    @Override
    public void initView() {
        super.initView();
        mEtDrugName = findViewById(R.id.et_drug_name);
        mTvType = findViewById(R.id.tv_type);
        mEtAprvnoBegndate = findViewById(R.id.et_aprvno_begndate);
        mEtRegDosform = findViewById(R.id.et_reg_dosform);
        mEtSpec = findViewById(R.id.et_spec);
        mEtProdentpName = findViewById(R.id.et_prodentp_name);
        mEtFuncMain = findViewById(R.id.et_func_main);
        mEtCommonUsage = findViewById(R.id.et_common_usage);
        mEtStorageConditions = findViewById(R.id.et_storage_conditions);
        mFlAddRelationDise = findViewById(R.id.fl_add_relation_dise);
        mCrvRelationDise = findViewById(R.id.crv_relation_dise);
        mFlReduceValidity = findViewById(R.id.fl_reduce_validity);
        mEtValidity = findViewById(R.id.et_validity);
        mFlAddValidity = findViewById(R.id.fl_add_validity);
        mFlReduceNum = findViewById(R.id.fl_reduce_num);
        mEtNum = findViewById(R.id.et_num);
        mFlAddNum = findViewById(R.id.fl_add_num);
        mFlReducePrice = findViewById(R.id.fl_reduce_price);
        mEtNumPrice = findViewById(R.id.et_num_price);
        mFlAddPrice = findViewById(R.id.fl_add_price);
        mLlDrugManual = findViewById(R.id.ll_drug_manual);
        mFlDrugManual = findViewById(R.id.fl_drug_manual);
        mIvDrugManual = findViewById(R.id.iv_drug_manual);
        mIvDrugManualDelete = findViewById(R.id.iv_drug_manual_delete);
        mLlCommodityDetails01 = findViewById(R.id.ll_commodity_details01);
        mFlCommodityDetails01 = findViewById(R.id.fl_commodity_details01);
        mIvCommodityDetails01 = findViewById(R.id.iv_commodity_details01);
        mIvCommodityDetailsDelete01 = findViewById(R.id.iv_commodity_details_delete01);
        mLlCommodityDetails02 = findViewById(R.id.ll_commodity_details02);
        mFlCommodityDetails02 = findViewById(R.id.fl_commodity_details02);
        mIvCommodityDetails02 = findViewById(R.id.iv_commodity_details02);
        mIvCommodityDetailsDelete02 = findViewById(R.id.iv_commodity_details_delete02);
        mLlCommodityDetails03 = findViewById(R.id.ll_commodity_details03);
        mFlCommodityDetails03 = findViewById(R.id.fl_commodity_details03);
        mIvCommodityDetails03 = findViewById(R.id.iv_commodity_details03);
        mIvCommodityDetailsDelete03 = findViewById(R.id.iv_commodity_details_delete03);
        mLlCommodityDetails04 = findViewById(R.id.ll_commodity_details04);
        mFlCommodityDetails04 = findViewById(R.id.fl_commodity_details04);
        mIvCommodityDetails04 = findViewById(R.id.iv_commodity_details04);
        mIvCommodityDetailsDelete04 = findViewById(R.id.iv_commodity_details_delete04);
        mLlCommodityDetails05 = findViewById(R.id.ll_commodity_details05);
        mFlCommodityDetails05 = findViewById(R.id.fl_commodity_details05);
        mIvCommodityDetails05 = findViewById(R.id.iv_commodity_details05);
        mIvCommodityDetailsDelete05 = findViewById(R.id.iv_commodity_details_delete05);
        mTvAdd = findViewById(R.id.tv_add);
        mTvContinueAdd = findViewById(R.id.tv_continue_add);

        mEtAprvnoBegndate = findViewById(R.id.et_aprvno_begndate);
        mEtRegDosform = findViewById(R.id.et_reg_dosform);
        mEtSpec = findViewById(R.id.et_spec);
        mEtProdentpName = findViewById(R.id.et_prodentp_name);
        mEtFuncMain = findViewById(R.id.et_func_main);
        mEtCommonUsage = findViewById(R.id.et_common_usage);
        mEtStorageConditions = findViewById(R.id.et_storage_conditions);

        //模拟新增数据
//        mEtDrugName.setText("苯丙酮尿症1001");
//        mTvType.setTag("1");
//        mTvType.setText("非处方药（OTC）");
//        mEtAprvnoBegndate.setText("国药准字H20193280-1111");
//        mEtRegDosform.setText("胶囊剂");
//        mEtSpec.setText("100mg");
//        mEtProdentpName.setText("通药制药集团股份有限公司");
//        mEtFuncMain.setText("功能主治1111");
//        mEtCommonUsage.setText("常见用法1111");
//        mEtStorageConditions.setText("贮存条件1111");
    }

    @Override
    protected void initImmersionBar() {
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mTitleBar).keyboardEnable(true);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        super.initData();

//        setRightIcon(getResources().getDrawable(R.drawable.ic_scan02));

        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);
        //关联病种
        mCrvRelationDise.setHasFixedSize(true);
        mCrvRelationDise.setLayoutManager(getLayoutManager());
        relationDiseAdapter = new RelationDiseAdapter();
        mCrvRelationDise.setAdapter(relationDiseAdapter);
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        MyFlexboxLayoutManager flexboxLayoutManager = new MyFlexboxLayoutManager(this);
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        flexboxLayoutManager.setFlexWrap(FlexWrap.WRAP);
        flexboxLayoutManager.setJustifyContent(JustifyContent.FLEX_START);

        return flexboxLayoutManager;
    }

    @Override
    public void initEvent() {
        super.initEvent();
        if (mTvType != null) {
            mTvType.setOnClickListener(this);
        }
        if (mFlAddRelationDise != null) {
            mFlAddRelationDise.setOnClickListener(this);
        }
        //关联疾病 item点击
        if (relationDiseAdapter != null) {
            relationDiseAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                    if (customerDialogUtils == null) {
                        customerDialogUtils = new CustomerDialogUtils();
                    }
                    customerDialogUtils.showDialog(AddShopWestDragActivtiy.this, "提示", "确定要删除么？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                        @Override
                        public void onBtn01Click() {
                        }

                        @Override
                        public void onBtn02Click() {
                            baseQuickAdapter.getData().remove(i);
                            baseQuickAdapter.notifyDataSetChanged();
                        }
                    });
                }
            });
        }
        //有效期 减 设置 加 点击事件
        mFlReduceValidity.setOnClickListener(this);
        mEtValidity.setOnClickListener(this);
        mFlAddValidity.setOnClickListener(this);
        //库存数量 减 设置 加 点击事件
        mFlReduceNum.setOnClickListener(this);
        mEtNum.setOnClickListener(this);
        mFlAddNum.setOnClickListener(this);
        //药品价格 减 设置 加 点击事件
        mFlReducePrice.setOnClickListener(this);
        mEtNumPrice.setOnClickListener(this);
        mFlAddPrice.setOnClickListener(this);

        //药品说明书图片点击操作
        mLlDrugManual.setOnClickListener(this);
        mIvDrugManualDelete.setOnClickListener(this);
        //商品详情图片点击操作
        mLlCommodityDetails01.setOnClickListener(this);
        mIvCommodityDetailsDelete01.setOnClickListener(this);
        mLlCommodityDetails02.setOnClickListener(this);
        mIvCommodityDetailsDelete02.setOnClickListener(this);
        mLlCommodityDetails03.setOnClickListener(this);
        mIvCommodityDetailsDelete03.setOnClickListener(this);
        mLlCommodityDetails04.setOnClickListener(this);
        mIvCommodityDetailsDelete04.setOnClickListener(this);
        mLlCommodityDetails05.setOnClickListener(this);
        mIvCommodityDetailsDelete05.setOnClickListener(this);

        mIvDrugManual.setOnClickListener(this);
        mIvCommodityDetails01.setOnClickListener(this);
        mIvCommodityDetails02.setOnClickListener(this);
        mIvCommodityDetails03.setOnClickListener(this);
        mIvCommodityDetails04.setOnClickListener(this);
        mIvCommodityDetails05.setOnClickListener(this);

        //添加 继续添加 点击事件
        if (mTvAdd != null) {
            mTvAdd.setOnClickListener(this);
        }
        if (mTvContinueAdd != null) {
            mTvContinueAdd.setOnClickListener(this);
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_type://选择药品类型
                KeyboardUtils.hideSoftInput(AddShopWestDragActivtiy.this);
                if (customerBottomListPop == null) {
                    customerBottomListPop = new CustomerBottomListPop(AddShopWestDragActivtiy.this, AddShopWestDragActivtiy.this);
                }
                customerBottomListPop.setDatas("2", "", mTvType.getText().toString(), CommonlyUsedDataUtils.getInstance().getDrugTypeData());
                customerBottomListPop.showPopupWindow();
                break;

            case R.id.fl_add_relation_dise://添加关联疾病
                if (relationDiseAdapter == null) {
                    showShortToast("数据初始化错误,请重新进入页面");
                    return;
                }
                if (relationDiseAdapter.getData().size() >= 5) {
                    showShortToast("关联疾病最多可添加5个");
                    return;
                }

                if (addRelationDisePop == null) {
                    addRelationDisePop = new AddRelationDisePop(AddShopWestDragActivtiy.this, "添加关联疾病");
                    addRelationDisePop.setOnListener(new AddRelationDisePop.IOptionListener() {
                        @Override
                        public void onAgree(String data) {
                            if (EmptyUtils.isEmpty(data)) {
                                showShortToast("请输入疾病名称");
                                return;
                            }
                            addRelationDisePop.dismiss();
                            relationDiseAdapter.getData().add(data);
                            relationDiseAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancle() {
                        }
                    });
                }
                addRelationDisePop.cleanData();
                addRelationDisePop.setOutSideDismiss(false).showPopupWindow();
                break;

            case R.id.fl_reduce_validity://有效期 减
                reduceValidity();
                break;
            case R.id.et_validity://设置有效期
                setValidity();
                break;
            case R.id.fl_add_validity://有效期 加
                addValidity();
                break;

            case R.id.fl_reduce_num://库存数量 减
                reduceNum();
                break;
            case R.id.et_num://设置库存数量
                setNum();
                break;
            case R.id.fl_add_num://库存数量 加
                addNum();
                break;

            case R.id.fl_reduce_price://药品价格 减
                reducePrice();
                break;
            case R.id.et_num_price://设置药品价格
                setPrice();
                break;
            case R.id.fl_add_price://药品价格 加
                addPrice();
                break;

            case R.id.ll_drug_manual://药品说明书
                KeyboardUtils.hideSoftInput(AddShopWestDragActivtiy.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_DRUG_MANUAL, CompressUploadSinglePicUtils_zhyf.PIC_DRUG_MANUAL_TAG);
                break;
            case R.id.iv_drug_manual_delete://药品说明书 删除
                mLlDrugManual.setVisibility(View.VISIBLE);
                mLlDrugManual.setEnabled(true);

                mIvDrugManual.setTag(R.id.tag_1, "");
                mIvDrugManual.setTag(R.id.tag_2, "");
                mFlDrugManual.setVisibility(View.GONE);
                break;

            case R.id.ll_commodity_details01://商品详情1
                KeyboardUtils.hideSoftInput(AddShopWestDragActivtiy.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_COMMODITY_DETAILS_01, CompressUploadSinglePicUtils_zhyf.PIC_COMMODITY_DETAILS_TAG);
                break;
            case R.id.iv_commodity_details_delete01://商品详情1 删除
                mLlCommodityDetails01.setVisibility(View.VISIBLE);
                mLlCommodityDetails01.setEnabled(true);

                mIvCommodityDetails01.setTag(R.id.tag_1, "");
                mIvCommodityDetails01.setTag(R.id.tag_2, "");
                mFlCommodityDetails01.setVisibility(View.GONE);
                break;
            case R.id.ll_commodity_details02://商品详情2
                KeyboardUtils.hideSoftInput(AddShopWestDragActivtiy.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_COMMODITY_DETAILS_02, CompressUploadSinglePicUtils_zhyf.PIC_COMMODITY_DETAILS_TAG);
                break;
            case R.id.iv_commodity_details_delete02://商品详情2 删除
                mLlCommodityDetails02.setVisibility(View.VISIBLE);
                mLlCommodityDetails02.setEnabled(true);

                mIvCommodityDetails02.setTag(R.id.tag_1, "");
                mIvCommodityDetails02.setTag(R.id.tag_2, "");
                mFlCommodityDetails02.setVisibility(View.GONE);
                break;
            case R.id.ll_commodity_details03://商品详情3
                KeyboardUtils.hideSoftInput(AddShopWestDragActivtiy.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_COMMODITY_DETAILS_03, CompressUploadSinglePicUtils_zhyf.PIC_COMMODITY_DETAILS_TAG);
                break;
            case R.id.iv_commodity_details_delete03://商品详情3 删除
                mLlCommodityDetails03.setVisibility(View.VISIBLE);
                mLlCommodityDetails03.setEnabled(true);

                mIvCommodityDetails03.setTag(R.id.tag_1, "");
                mIvCommodityDetails03.setTag(R.id.tag_2, "");
                mFlCommodityDetails03.setVisibility(View.GONE);
                break;
            case R.id.ll_commodity_details04://商品详情4
                KeyboardUtils.hideSoftInput(AddShopWestDragActivtiy.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_COMMODITY_DETAILS_04, CompressUploadSinglePicUtils_zhyf.PIC_COMMODITY_DETAILS_TAG);
                break;
            case R.id.iv_commodity_details_delete04://商品详情4 删除
                mLlCommodityDetails04.setVisibility(View.VISIBLE);
                mLlCommodityDetails04.setEnabled(true);

                mIvCommodityDetails04.setTag(R.id.tag_1, "");
                mIvCommodityDetails04.setTag(R.id.tag_2, "");
                mFlCommodityDetails04.setVisibility(View.GONE);
                break;
            case R.id.ll_commodity_details05://商品详情5
                KeyboardUtils.hideSoftInput(AddShopWestDragActivtiy.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_COMMODITY_DETAILS_05, CompressUploadSinglePicUtils_zhyf.PIC_COMMODITY_DETAILS_TAG);
                break;
            case R.id.iv_commodity_details_delete05://商品详情5 删除
                mLlCommodityDetails05.setVisibility(View.VISIBLE);
                mLlCommodityDetails05.setEnabled(true);

                mIvCommodityDetails05.setTag(R.id.tag_1, "");
                mIvCommodityDetails05.setTag(R.id.tag_2, "");
                mFlCommodityDetails05.setVisibility(View.GONE);
                break;
            case R.id.iv_drug_manual:
                showBigPic(mIvDrugManual,(String)mIvDrugManual.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details01:
                showBigPic(mIvCommodityDetails01,(String)mIvCommodityDetails01.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details02:
                showBigPic(mIvCommodityDetails02,(String)mIvCommodityDetails02.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details03:
                showBigPic(mIvCommodityDetails03,(String)mIvCommodityDetails03.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details04:
                showBigPic(mIvCommodityDetails04,(String)mIvCommodityDetails04.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details05:
                showBigPic(mIvCommodityDetails05,(String)mIvCommodityDetails05.getTag(R.id.tag_2));
                break;

            case R.id.tv_add://添加
                onAdd("1");
                break;
            case R.id.tv_continue_add://继续添加
                onAdd("2");
                break;
        }
    }

    //------------------------有效期----------------------------------------
    //有效期 减 库存数量大于等于1，小于等于1时也点不了
    private void reduceValidity() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mEtValidity.getText().toString())) {
            mEtValidity.setText("1");
        } else if (new BigDecimal(mEtValidity.getText().toString()).doubleValue() > 1) {
            mEtValidity.setText(new BigDecimal(mEtValidity.getText().toString()).subtract(new BigDecimal("1")).toPlainString());
        } else {
            showShortToast("有效期不能为0");
            hideWaitDialog();
            return;
        }

        hideWaitDialog();
    }

    //设置有效期
    private void setValidity() {
        if (updateNumPop == null) {
            updateNumPop = new UpdateNumPop(AddShopWestDragActivtiy.this);
        }
        updateNumPop.setOnListener(new UpdateNumPop.IOptionListener() {
            @Override
            public void onAgree(String data) {
                int num = new BigDecimal(data).intValue();
                if (num < 1) {
                    showShortToast("请输入正确的有效期");
                    return;
                }
                mEtValidity.setText(data);
            }

            @Override
            public void onCancle() {
            }
        });
        updateNumPop.setInputType(InputType.TYPE_CLASS_NUMBER);
        updateNumPop.setContent(EmptyUtils.strEmptyToText(mEtValidity.getText().toString(), "0"));
        updateNumPop.setOutSideDismiss(false).showPopupWindow();
        KeyboardUtils.showSoftInput(AddShopWestDragActivtiy.this);
    }

    //有效期 加
    private void addValidity() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mEtValidity.getText().toString())) {
            mEtValidity.setText("1");
        } else {
            mEtValidity.setText(new BigDecimal(mEtValidity.getText().toString()).add(new BigDecimal("1")).toPlainString());
        }

        hideWaitDialog();
    }

    //------------------------库存数量----------------------------------------
    //库存数量 减 库存数量大于等于1，小于等于1时也点不了
    private void reduceNum() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mEtNum.getText().toString())) {
            mEtNum.setText("1");
        } else if (new BigDecimal(mEtNum.getText().toString()).doubleValue() > 1) {
            mEtNum.setText(new BigDecimal(mEtNum.getText().toString()).subtract(new BigDecimal("1")).toPlainString());
        } else {
            showShortToast("库存数量不能为0");
            hideWaitDialog();
            return;
        }

        hideWaitDialog();
    }

    //设置库存数量
    private void setNum() {
        if (updateNumPop == null) {
            updateNumPop = new UpdateNumPop(AddShopWestDragActivtiy.this);
        }
        updateNumPop.setOnListener(new UpdateNumPop.IOptionListener() {
            @Override
            public void onAgree(String data) {
                int num = new BigDecimal(data).intValue();
                if (num < 1) {
                    showShortToast("请输入正确的数量");
                    return;
                }
                mEtNum.setText(data);
            }

            @Override
            public void onCancle() {
            }
        });
        updateNumPop.setInputType(InputType.TYPE_CLASS_NUMBER);
        updateNumPop.setContent(EmptyUtils.strEmptyToText(mEtNum.getText().toString(), "0"));
        updateNumPop.setOutSideDismiss(false).showPopupWindow();
        KeyboardUtils.showSoftInput(AddShopWestDragActivtiy.this);
    }

    //库存数量 加
    private void addNum() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mEtNum.getText().toString())) {
            mEtNum.setText("1");
        } else {
            mEtNum.setText(new BigDecimal(mEtNum.getText().toString()).add(new BigDecimal("1")).toPlainString());
        }

        hideWaitDialog();
    }

    //------------------------药品价格----------------------------------------
    //药品价格 减 药品价格和促销折扣不能为0，当小于1的时候点不了“减号”
    private void reducePrice() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mEtNumPrice.getText().toString())) {
            mEtNumPrice.setText("0");
        } else if (new BigDecimal(mEtNumPrice.getText().toString()).doubleValue() > 1) {
            mEtNumPrice.setText(new BigDecimal(mEtNumPrice.getText().toString()).subtract(new BigDecimal("1")).toPlainString());
        } else {
            showShortToast("药品价格不能为0");
            hideWaitDialog();
            return;
        }

        hideWaitDialog();
    }

    //设置药品价格
    private void setPrice() {
        if (updateNumPop == null) {
            updateNumPop = new UpdateNumPop(AddShopWestDragActivtiy.this);
        }
        updateNumPop.setOnListener(new UpdateNumPop.IOptionListener() {
            @Override
            public void onAgree(String data) {
                if (!RxTool.isPrice(data)) {
                    showShortToast("请输入正确的金额");
                    return;
                }

                mEtNumPrice.setText(data);
            }

            @Override
            public void onCancle() {
            }
        });
        updateNumPop.setFilters(2);
        updateNumPop.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
        updateNumPop.setContent(EmptyUtils.strEmptyToText(mEtNumPrice.getText().toString(), "0"));
        updateNumPop.setOutSideDismiss(false).showPopupWindow();
        KeyboardUtils.showSoftInput(AddShopWestDragActivtiy.this);
    }

    //药品价格 加
    private void addPrice() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mEtNumPrice.getText().toString())) {
            mEtNumPrice.setText("0");
        } else {
            mEtNumPrice.setText(new BigDecimal(mEtNumPrice.getText().toString()).add(new BigDecimal("1")).toPlainString());
        }

        hideWaitDialog();
    }

    //添加  继续添加
    protected void onAdd(String tag) {
        if (EmptyUtils.isEmpty(mEtDrugName.getText().toString())) {
            showShortToast("请输入药品名称");
            return;
        }
        if (EmptyUtils.isEmpty(mTvType.getText().toString())) {
            showShortToast("请选择药品类型");
            return;
        }
        if (EmptyUtils.isEmpty(mEtSpec.getText().toString())) {
            showShortToast("请输入药品规格");
            return;
        }
        if (EmptyUtils.isEmpty(mEtFuncMain.getText().toString())) {
            showShortToast("请输入功能主治");
            return;
        }
        if (EmptyUtils.isEmpty(mEtCommonUsage.getText().toString())) {
            showShortToast("请输入常见用法");
            return;
        }
        if (EmptyUtils.isEmpty(mEtStorageConditions.getText().toString())) {
            showShortToast("请输入贮存条件");
            return;
        }
        if (relationDiseAdapter.getData().size() <= 0) {
            showShortToast("请关联疾病");
            return;
        }
        if (EmptyUtils.isEmpty(mEtValidity.getText().toString()) || "0".equals(mEtValidity.getText().toString())) {
            showShortToast("请输入正确的有效期");
            return;
        }
        if (EmptyUtils.isEmpty(mEtNum.getText().toString()) || "0".equals(mEtNum.getText().toString())) {
            showShortToast("请输入正确的库存数量");
            return;
        }
        if (EmptyUtils.isEmpty(mEtNumPrice.getText().toString()) || "0".equals(mEtNumPrice.getText().toString())) {
            showShortToast("请输入正确的药品价格");
            return;
        }
        if (null == mIvDrugManual.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvDrugManual.getTag(R.id.tag_2)))) {
            showShortToast("请上传药品说明书");
            return;
        }

        //关联疾病数据
        List<String> relationDiseDatas = relationDiseAdapter.getData();
        String associatedDiseases = "";
        for (int i = 0, size = relationDiseDatas.size(); i < size; i++) {
            if (i == size - 1) {
                associatedDiseases += relationDiseDatas.get(i);
            } else {
                associatedDiseases += relationDiseDatas.get(i) + "∞#";
            }
        }

        int selectCommodityDetailsPic = 0;
        if (null != mIvCommodityDetails01.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails01.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails02.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails02.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails03.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails03.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails04.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails04.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails05.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails05.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }

        if (selectCommodityDetailsPic < 3) {
            showShortToast("请上传最少三张，至多五张商品详情图片");
            return;
        }

        showWaitDialog();
        // 2022-10-08 调用接口 成功之后跳转成功页面
        BaseModel.sendAddShopDrugDataRequest(TAG, "1", "西药中成药", mEtDrugName.getText().toString(), mEtSpec.getText().toString(),
                mEtProdentpName.getText().toString(), mEtStorageConditions.getText().toString(), mEtValidity.getText().toString(), mEtNum.getText().toString(),
                mEtNumPrice.getText().toString(), mIvDrugManual.getTag(R.id.tag_1), mIvCommodityDetails01.getTag(R.id.tag_1), mIvCommodityDetails02.getTag(R.id.tag_1),
                mIvCommodityDetails03.getTag(R.id.tag_1), mIvCommodityDetails04.getTag(R.id.tag_1), mIvCommodityDetails05.getTag(R.id.tag_1),
                mTvType.getTag(), mEtAprvnoBegndate.getText().toString(), mEtRegDosform.getText().toString(), mEtFuncMain.getText().toString(),
                mEtCommonUsage.getText().toString(), associatedDiseases, "", "", "", "", "",
                "", "", "", new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("添加成功");
                        if ("2".equals(tag)) {
                            cleanData();
                        }else {
                            Intent intent = new Intent();
                            intent.putExtra("option_type", "add");
                            setResult(RESULT_OK, intent);
                            goFinish();
                        }
                    }
                });
    }

    //清楚所有数据
    protected void cleanData(){
        mEtDrugName.setText("");
        mTvType.setTag("");
        mTvType.setText("");
        mEtAprvnoBegndate.setText("");
        mEtRegDosform.setText("");
        mEtSpec.setText("");
        mEtProdentpName.setText("");
        mEtFuncMain.setText("");
        mEtCommonUsage.setText("");
        mEtStorageConditions.setText("");
        //关联疾病
        relationDiseAdapter.getData().clear();
        relationDiseAdapter.notifyDataSetChanged();

        mEtValidity.setText("12");
        mEtNum.setText("5");
        mEtNumPrice.setText("0");

        mLlDrugManual.setVisibility(View.VISIBLE);
        mLlDrugManual.setEnabled(true);
        mIvDrugManual.setTag(R.id.tag_1, "");
        mIvDrugManual.setTag(R.id.tag_2, "");
        mFlDrugManual.setVisibility(View.GONE);

        mLlCommodityDetails01.setVisibility(View.VISIBLE);
        mLlCommodityDetails01.setEnabled(true);
        mIvCommodityDetails01.setTag(R.id.tag_1, "");
        mIvCommodityDetails01.setTag(R.id.tag_2, "");
        mFlCommodityDetails01.setVisibility(View.GONE);

        mLlCommodityDetails02.setVisibility(View.VISIBLE);
        mLlCommodityDetails02.setEnabled(true);
        mIvCommodityDetails02.setTag(R.id.tag_1, "");
        mIvCommodityDetails02.setTag(R.id.tag_2, "");
        mFlCommodityDetails02.setVisibility(View.GONE);

        mLlCommodityDetails03.setVisibility(View.VISIBLE);
        mLlCommodityDetails03.setEnabled(true);
        mIvCommodityDetails03.setTag(R.id.tag_1, "");
        mIvCommodityDetails03.setTag(R.id.tag_2, "");
        mFlCommodityDetails03.setVisibility(View.GONE);

        mLlCommodityDetails04.setVisibility(View.VISIBLE);
        mLlCommodityDetails04.setEnabled(true);
        mIvCommodityDetails04.setTag(R.id.tag_1, "");
        mIvCommodityDetails04.setTag(R.id.tag_2, "");
        mFlCommodityDetails04.setVisibility(View.GONE);

        mLlCommodityDetails05.setVisibility(View.VISIBLE);
        mLlCommodityDetails05.setEnabled(true);
        mIvCommodityDetails05.setTag(R.id.tag_1, "");
        mIvCommodityDetails05.setTag(R.id.tag_2, "");
        mFlCommodityDetails05.setVisibility(View.GONE);
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String mTag, String accessoryId, String url, int width, int height) {
        switch (mTag) {
            case TAG_SELECT_DRUG_MANUAL://药品说明书
                selectDrugManualSuccess(accessoryId, url, width, height);
                break;
            case TAG_SELECT_COMMODITY_DETAILS_01://商品详情1
                selectCommodityDetails01Success(accessoryId, url, width, height);
                break;
            case TAG_SELECT_COMMODITY_DETAILS_02://商品详情2
                selectCommodityDetails02Success(accessoryId, url, width, height);
                break;
            case TAG_SELECT_COMMODITY_DETAILS_03://商品详情3
                selectCommodityDetails03Success(accessoryId, url, width, height);
                break;
            case TAG_SELECT_COMMODITY_DETAILS_04://商品详情4
                selectCommodityDetails04Success(accessoryId, url, width, height);
                break;
            case TAG_SELECT_COMMODITY_DETAILS_05://商品详情5
                selectCommodityDetails05Success(accessoryId, url, width, height);
                break;
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }

    //药品说明书
    private void selectDrugManualSuccess(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvDrugManual.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvDrugManual.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvDrugManual);

            mLlDrugManual.setVisibility(View.GONE);
            mLlDrugManual.setEnabled(false);

            mIvDrugManual.setTag(R.id.tag_1, accessoryId);
            mIvDrugManual.setTag(R.id.tag_2, url);
            mFlDrugManual.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //商品详情01
    private void selectCommodityDetails01Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails01.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvCommodityDetails01.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCommodityDetails01);

            mLlCommodityDetails01.setVisibility(View.GONE);
            mLlCommodityDetails01.setEnabled(false);

            mIvCommodityDetails01.setTag(R.id.tag_1, accessoryId);
            mIvCommodityDetails01.setTag(R.id.tag_2, url);
            mFlCommodityDetails01.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //商品详情02
    private void selectCommodityDetails02Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails02.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvCommodityDetails02.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCommodityDetails02);

            mLlCommodityDetails02.setVisibility(View.GONE);
            mLlCommodityDetails02.setEnabled(false);

            mIvCommodityDetails02.setTag(R.id.tag_1, accessoryId);
            mIvCommodityDetails02.setTag(R.id.tag_2, url);
            mFlCommodityDetails02.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //商品详情03
    private void selectCommodityDetails03Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails03.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvCommodityDetails03.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCommodityDetails03);

            mLlCommodityDetails03.setVisibility(View.GONE);
            mLlCommodityDetails03.setEnabled(false);

            mIvCommodityDetails03.setTag(R.id.tag_1, accessoryId);
            mIvCommodityDetails03.setTag(R.id.tag_2, url);
            mFlCommodityDetails03.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //商品详情04
    private void selectCommodityDetails04Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails04.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvCommodityDetails04.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCommodityDetails04);

            mLlCommodityDetails04.setVisibility(View.GONE);
            mLlCommodityDetails04.setEnabled(false);

            mIvCommodityDetails04.setTag(R.id.tag_1, accessoryId);
            mIvCommodityDetails04.setTag(R.id.tag_2, url);
            mFlCommodityDetails04.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //商品详情05
    private void selectCommodityDetails05Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails05.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvCommodityDetails05.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCommodityDetails05);

            mLlCommodityDetails05.setVisibility(View.GONE);
            mLlCommodityDetails05.setEnabled(false);

            mIvCommodityDetails05.setTag(R.id.tag_1, accessoryId);
            mIvCommodityDetails05.setTag(R.id.tag_2, url);
            mFlCommodityDetails05.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //--------------------------选择图片----------------------------------------------------

    //展示大图
    private void showBigPic(ImageView imageView,String imageUrl) {
        OpenImage.with(AddShopWestDragActivtiy.this)
                //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                .setClickImageView(imageView)
                //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                //RecyclerView的数据
                .setImageUrl(imageUrl, MediaType.IMAGE)
                //点击的ImageView所在数据的位置
                .setClickPosition(0)
                //开始展示大图
                .show();
    }

    @Override
    public void onItemClick(String tag, StoreStatusModel str) {
        switch (tag) {
            case "2"://选择药品类型
                mTvType.setTag(str.getId());
                mTvType.setText(EmptyUtils.strEmpty(str.getText()));
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }

        if (customerBottomListPop != null) {
            customerBottomListPop.dismiss();
            customerBottomListPop.onDestroy();
        }

        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }

        if (addRelationDisePop != null) {
            addRelationDisePop.onCleanListener();
            addRelationDisePop.onDestroy();
        }
    }
}
