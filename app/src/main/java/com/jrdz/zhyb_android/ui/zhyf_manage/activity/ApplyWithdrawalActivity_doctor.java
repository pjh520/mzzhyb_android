package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;

import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;

import java.math.BigDecimal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-14
 * 描    述：申请提现
 * ================================================
 */
public class ApplyWithdrawalActivity_doctor extends ApplyWithdrawalActivity {

    //提交
    @Override
    protected void onCommit() {
        if (EmptyUtils.isEmpty(mEtMoney.getText().toString())) {
            showShortToast("请输入要提现的金额");
            return;
        }

        double dis01 = new BigDecimal(EmptyUtils.strEmptyToText(balance,"0")).subtract(new BigDecimal(mEtMoney.getText().toString())).doubleValue();
        if (dis01 < 0) {
            showShortToast("账户余额不足");
            return;
        }

        double dis02 = new BigDecimal(mEtMoney.getText().toString()).doubleValue();
        if (dis02 <= 0) {
            showShortToast("请输入要提现的金额");
            return;
        }

        double dis03 = new BigDecimal(mEtMoney.getText().toString()).subtract(new BigDecimal("1000")).doubleValue();
        if (dis03 < 0) {
            showShortToast("单笔提现最低1000元");
            return;
        }

        if (null == mTvPayType.getTag() || EmptyUtils.isEmpty(String.valueOf(mTvPayType.getTag()))) {
            showShortToast("请选择支付方式");
            return;
        }

        if (EmptyUtils.isEmpty(mEtPayAccount.getText().toString())) {
            showShortToast("请输入收款账号");
            return;
        }

        if (EmptyUtils.isEmpty(mEtPayName.getText().toString())) {
            showShortToast("请输入收款人姓名");
            return;
        }
        if (EmptyUtils.isEmpty(mEtPayPhone.getText().toString())) {
            showShortToast("请输入联系人手机号");
            return;
        }

        if (!"1".equals(String.valueOf(mEtPayPhone.getText().charAt(0))) || mEtPayPhone.getText().length() != 11) {
            showShortToast("请输入正确的手机号码");
            return;
        }

        //2022-10-13 请求接口生成支付信息
        showWaitDialog();
        BaseModel.sendApplyWithdrawalRequest_doctor(TAG, String.valueOf(mTvPayType.getTag()), mEtPayAccount.getText().toString(),
                mEtPayName.getText().toString(), mEtPayPhone.getText().toString(), mEtMoney.getText().toString(), balance, "1",
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        //支付成功之后
                        SuccessActivity.newIntance(ApplyWithdrawalActivity_doctor.this,"申请成功!", "返回我的账户");
                        goFinish();
                    }
                });
    }

    public static void newIntance(Context context, String balance) {
        Intent intent = new Intent(context, ApplyWithdrawalActivity_doctor.class);
        intent.putExtra("balance", balance);
        context.startActivity(intent);
    }
}
