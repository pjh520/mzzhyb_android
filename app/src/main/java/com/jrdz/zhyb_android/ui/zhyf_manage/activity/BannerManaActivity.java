package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.customPop.CustomerBottomListPop;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.StoreStatusModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;

import java.math.BigDecimal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-08
 * 描    述：banner管理页面
 * ================================================
 */
public class BannerManaActivity extends BaseActivity implements CompressUploadSinglePicUtils_zhyf.IChoosePic, CustomerBottomListPop.IBottomChooseListener<StoreStatusModel> {
    public final String TAG_SELECT_BANNER_01 = "1";//banner 1号位
    public final String TAG_SELECT_BANNER_02 = "2";//banner 2号位
    public final String TAG_SELECT_BANNER_03 = "3";//banner 3号位

    private RelativeLayout mLlHorizontalPic;
    private TextView mTv01;
    private ShapeFrameLayout mSflHorizontalPic01;
    private LinearLayout mLlAddHorizontalPic01;
    private FrameLayout mFlHorizontalPic01;
    private ImageView mIvHorizontalPic01;
    private ImageView mIvDelete01;
    private TextView mTvBanner01;
    private ShapeFrameLayout mSflHorizontalPic02;
    private LinearLayout mLlAddHorizontalPic02;
    private FrameLayout mFlHorizontalPic02;
    private ImageView mIvHorizontalPic02;
    private ImageView mIvDelete02;
    private TextView mTvBanner02;
    private ShapeFrameLayout mSflHorizontalPic03;
    private LinearLayout mLlAddHorizontalPic03;
    private FrameLayout mFlHorizontalPic03;
    private ImageView mIvHorizontalPic03;
    private ImageView mIvDelete03;
    private TextView mTvBanner03;
    private ShapeLinearLayout mSllBannerTime;
    private TextView mTvBannerTime;
    private ShapeTextView mTvSavePull;

    private CompressUploadSinglePicUtils_zhyf compressUploadSinglePicUtils;
    private CustomerBottomListPop customerBottomListPop; //选择弹框

    @Override
    public int getLayoutId() {
        return R.layout.activity_banner_mana;
    }

    @Override
    public void initView() {
        super.initView();
        mLlHorizontalPic = findViewById(R.id.ll_horizontal_pic);
        mTv01 = findViewById(R.id.tv_01);
        mSflHorizontalPic01 = findViewById(R.id.sfl_horizontal_pic01);
        mLlAddHorizontalPic01 = findViewById(R.id.ll_add_horizontal_pic01);
        mFlHorizontalPic01 = findViewById(R.id.fl_horizontal_pic01);
        mIvHorizontalPic01 = findViewById(R.id.iv_horizontal_pic01);
        mIvDelete01 = findViewById(R.id.iv_delete01);
        mTvBanner01 = findViewById(R.id.tv_banner01);
        mSflHorizontalPic02 = findViewById(R.id.sfl_horizontal_pic02);
        mLlAddHorizontalPic02 = findViewById(R.id.ll_add_horizontal_pic02);
        mFlHorizontalPic02 = findViewById(R.id.fl_horizontal_pic02);
        mIvHorizontalPic02 = findViewById(R.id.iv_horizontal_pic02);
        mIvDelete02 = findViewById(R.id.iv_delete02);
        mTvBanner02 = findViewById(R.id.tv_banner02);
        mSflHorizontalPic03 = findViewById(R.id.sfl_horizontal_pic03);
        mLlAddHorizontalPic03 = findViewById(R.id.ll_add_horizontal_pic03);
        mFlHorizontalPic03 = findViewById(R.id.fl_horizontal_pic03);
        mIvHorizontalPic03 = findViewById(R.id.iv_horizontal_pic03);
        mIvDelete03 = findViewById(R.id.iv_delete03);
        mTvBanner03 = findViewById(R.id.tv_banner03);
        mSllBannerTime = findViewById(R.id.sll_banner_time);
        mTvBannerTime = findViewById(R.id.tv_banner_time);
        mTvSavePull = findViewById(R.id.tv_save_pull);
    }

    @Override
    public void initData() {
        super.initData();


        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mLlAddHorizontalPic01.setOnClickListener(this);
        mIvDelete01.setOnClickListener(this);
        mLlAddHorizontalPic02.setOnClickListener(this);
        mIvDelete02.setOnClickListener(this);
        mLlAddHorizontalPic03.setOnClickListener(this);
        mIvDelete03.setOnClickListener(this);

        mSllBannerTime.setOnClickListener(this);
        mTvSavePull.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.ll_add_horizontal_pic01://banner 1号位
                KeyboardUtils.hideSoftInput(BannerManaActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_BANNER_01, CompressUploadSinglePicUtils_zhyf.PIC_BANNER_01_TAG);
                break;
            case R.id.iv_delete01://banner 1号位 删除
                mLlAddHorizontalPic01.setVisibility(View.VISIBLE);
                mLlAddHorizontalPic01.setEnabled(true);

                mIvHorizontalPic01.setTag(R.id.tag_1, "");
                mIvHorizontalPic01.setTag(R.id.tag_2, "");
                mFlHorizontalPic01.setVisibility(View.GONE);
                break;
            case R.id.ll_add_horizontal_pic02://banner 2号位
                KeyboardUtils.hideSoftInput(BannerManaActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_BANNER_02, CompressUploadSinglePicUtils_zhyf.PIC_BANNER_02_TAG);
                break;
            case R.id.iv_delete02://banner 2号位 删除
                mLlAddHorizontalPic02.setVisibility(View.VISIBLE);
                mLlAddHorizontalPic02.setEnabled(true);

                mIvHorizontalPic02.setTag(R.id.tag_1, "");
                mIvHorizontalPic02.setTag(R.id.tag_2, "");
                mFlHorizontalPic02.setVisibility(View.GONE);
                break;
            case R.id.ll_add_horizontal_pic03://banner 3号位
                KeyboardUtils.hideSoftInput(BannerManaActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_BANNER_03, CompressUploadSinglePicUtils_zhyf.PIC_BANNER_03_TAG);
                break;
            case R.id.iv_delete03://banner 3号位 删除
                mLlAddHorizontalPic03.setVisibility(View.VISIBLE);
                mLlAddHorizontalPic03.setEnabled(true);

                mIvHorizontalPic03.setTag(R.id.tag_1, "");
                mIvHorizontalPic03.setTag(R.id.tag_2, "");
                mFlHorizontalPic03.setVisibility(View.GONE);
                break;
            case R.id.sll_banner_time://选择轮播间隔时间
                KeyboardUtils.hideSoftInput(BannerManaActivity.this);
                if (customerBottomListPop == null) {
                    customerBottomListPop = new CustomerBottomListPop(BannerManaActivity.this, BannerManaActivity.this);
                }
                customerBottomListPop.setDatas("1", "", mTvBannerTime.getText().toString(), CommonlyUsedDataUtils.getInstance().getBannerTimeData());
                customerBottomListPop.showPopupWindow();
                break;
            case R.id.tv_save_pull://保存
                savePull();
                break;
        }
    }

    //保存
    private void savePull() {
        if (null == mIvHorizontalPic01.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvHorizontalPic01.getTag(R.id.tag_2)))) {
            showShortToast("请上传banner 1号位");
            return;
        }
        if (null == mIvHorizontalPic02.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvHorizontalPic02.getTag(R.id.tag_2)))) {
            showShortToast("请上传banner 2号位");
            return;
        }
        if (null == mIvHorizontalPic03.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvHorizontalPic03.getTag(R.id.tag_2)))) {
            showShortToast("请上传banner 3号位");
            return;
        }
        if (EmptyUtils.isEmpty(mTvBannerTime.getText().toString()) ) {
            showShortToast("请选择轮播间隔时间");
            return;
        }

        // TODO: 2022-10-08 请求接口 保存成功 返回上一页

        goFinish();
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String mTag, String accessoryId, String url, int width, int height) {
        switch (mTag) {
            case TAG_SELECT_BANNER_01://banner 1号位
                selectBanner01Success(accessoryId, url, width, height);
                break;
            case TAG_SELECT_BANNER_02://banner 2号位
                selectBanner02Success(accessoryId, url, width, height);
                break;
            case TAG_SELECT_BANNER_03://banner 3号位
                selectBanner03Success(accessoryId, url, width, height);
                break;
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }

    //banner 1号位
    private void selectBanner01Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic01.getLayoutParams();
            if (radio >= 2.025) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_486));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_486))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_240) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_240));
            }

            mIvHorizontalPic01.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic01);

            mLlAddHorizontalPic01.setVisibility(View.GONE);
            mLlAddHorizontalPic01.setEnabled(false);

            mIvHorizontalPic01.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic01.setTag(R.id.tag_2, url);
            mFlHorizontalPic01.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //banner 2号位
    private void selectBanner02Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic02.getLayoutParams();
            if (radio >= 2.025) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_486));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_486))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_240) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_240));
            }

            mIvHorizontalPic02.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic02);

            mLlAddHorizontalPic02.setVisibility(View.GONE);
            mLlAddHorizontalPic02.setEnabled(false);

            mIvHorizontalPic02.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic02.setTag(R.id.tag_2, url);
            mFlHorizontalPic02.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //banner 3号位
    private void selectBanner03Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic03.getLayoutParams();
            if (radio >= 2.025) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_486));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_486))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_240) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_240));
            }

            mIvHorizontalPic03.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic03);

            mLlAddHorizontalPic03.setVisibility(View.GONE);
            mLlAddHorizontalPic03.setEnabled(false);

            mIvHorizontalPic03.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic03.setTag(R.id.tag_2, url);
            mFlHorizontalPic03.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //--------------------------选择图片----------------------------------------------------

    @Override
    public void onItemClick(String tag, StoreStatusModel str) {
        switch (tag) {
            case "1"://选择轮播间隔时间
                mTvBannerTime.setText(EmptyUtils.strEmpty(str.getText()));
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }
        if (customerBottomListPop != null) {
            customerBottomListPop.dismiss();
            customerBottomListPop.onDestroy();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, BannerManaActivity.class);
        context.startActivity(intent);
    }
}
