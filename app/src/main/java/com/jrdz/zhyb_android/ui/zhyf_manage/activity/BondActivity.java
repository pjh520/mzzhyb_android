package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.MyAccountModel_mana;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-14
 * 描    述：保证金页面
 * ================================================
 */
public class BondActivity extends BaseActivity {
    private TextView mTvBalanceAccount;
    private TextView mTvBalance;
    private ShapeTextView mTvImmediatelyPay;
    private ShapeTextView mTvApplyWithdrawal;

    private MyAccountModel_mana.DataBean pagerData;

    @Override
    public int getLayoutId() {
        return R.layout.activity_bond;
    }

    @Override
    public void initView() {
        super.initView();
        mTvBalanceAccount = findViewById(R.id.tv_balance_account);
        mTvBalance = findViewById(R.id.tv_balance);
        mTvImmediatelyPay = findViewById(R.id.tv_immediately_pay);
        mTvApplyWithdrawal = findViewById(R.id.tv_apply_withdrawal);
    }

    @Override
    public void initData() {
        super.initData();

        setRightTitleView("扣款记录");
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvImmediatelyPay.setOnClickListener(this);
        mTvApplyWithdrawal.setOnClickListener(this);
    }

    @Override
    public void rightTitleViewClick() {
        DeductionRecordActivity.newIntance(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        if (pagerData==null){
            showShortToast("数据有误，请重新进入页面");
            return;
        }
        switch (v.getId()){
            case R.id.tv_immediately_pay://立即缴纳
                BondRechargeActivity.newIntance(BondActivity.this,pagerData.getUserName());
                break;
            case R.id.tv_apply_withdrawal://申请提现
                BondWithdrawalActivity.newIntance(BondActivity.this,pagerData.getDepositBalance());
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPagerData();
    }

    //获取页面数据
    private void getPagerData() {
        MyAccountModel_mana.sendMyAccountRequest_mana(TAG, new CustomerJsonCallBack<MyAccountModel_mana>() {
            @Override
            public void onRequestError(MyAccountModel_mana returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(MyAccountModel_mana returnData) {
                hideWaitDialog();
                setPagerData(returnData.getData());
            }
        });
    }

    private void setPagerData(MyAccountModel_mana.DataBean data) {
        pagerData=data;
        if (data!=null){
            mTvBalance.setText(EmptyUtils.strEmpty(data.getDepositBalance()));
            mTvBalanceAccount.setText("账号:"+data.getUserName());
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, BondActivity.class);
        context.startActivity(intent);
    }
}
