package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.LogUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PayTypeModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.QueryRechargePayModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.RechargeModel_mercenary;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.OrderDetailActivity_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.PaySuccessActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.model.QueryOrderPayOnlineModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.widget.pop.SelectPayTypePop;
import com.kuaiqian.fusedpay.entity.FusedPayRequest;
import com.kuaiqian.fusedpay.entity.FusedPayResult;
import com.kuaiqian.fusedpay.sdk.FusedPayApiFactory;
import com.kuaiqian.fusedpay.sdk.IFusedPayApi;
import com.kuaiqian.fusedpay.sdk.IFusedPayEventHandler;

import java.math.BigDecimal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-14
 * 描    述：保证金充值
 * ================================================
 */
public class BondRechargeActivity extends BaseActivity implements SelectPayTypePop.IOptionListener, IFusedPayEventHandler {
    private ShapeTextView mStvPic;
    private TextView mTvTitle;
    private TextView mTvPriceTag;
    private TextView mTvRechargePrice;
    private FrameLayout mFlReduce;
    private TextView mTvNum;
    private FrameLayout mFlAdd;
    private View mViewLine;
    private LinearLayout mLlPayType;
    private TextView mTvPayType;
    private TextView mTvPrice;
    private ShapeTextView mTvRecharge;

    private String rechargeAccount;
    private SelectPayTypePop selectPayTypePop;
    private String serialNumber;

    private boolean isPaying=false;//当前操作是否是正在支付

    @Override
    public int getLayoutId() {
        return R.layout.activity_bond_recharge;
    }

    @Override
    public void initView() {
        super.initView();
        mStvPic = findViewById(R.id.stv_pic);
        mTvTitle = findViewById(R.id.tv_title);
        mTvPriceTag = findViewById(R.id.tv_price_tag);
        mTvRechargePrice = findViewById(R.id.tv_recharge_price);
        mFlReduce = findViewById(R.id.fl_reduce);
        mTvNum = findViewById(R.id.tv_num);
        mFlAdd = findViewById(R.id.fl_add);
        mViewLine = findViewById(R.id.view_line);
        mLlPayType = findViewById(R.id.ll_pay_type);
        mTvPayType = findViewById(R.id.tv_pay_type);
        mTvPrice = findViewById(R.id.tv_price);
        mTvRecharge = findViewById(R.id.tv_recharge);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(false, 0.2f)
                .titleBar(mTitleBar);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        rechargeAccount = getIntent().getStringExtra("rechargeAccount");
        super.initData();
        setRightTitleView("充值记录");
        setTotalPrice();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mFlReduce.setOnClickListener(this);
        mFlAdd.setOnClickListener(this);
        mLlPayType.setOnClickListener(this);
        mTvRecharge.setOnClickListener(this);
    }

    @Override
    public void rightTitleViewClick() {
        BondRecordActivity.newIntance(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_reduce://数量减
                reduceNum();
                break;
            case R.id.fl_add://数量加
                addNum();
                break;
            case R.id.ll_pay_type://选择支付方式
                if (selectPayTypePop == null) {
                    selectPayTypePop = new SelectPayTypePop(BondRechargeActivity.this,
                            CommonlyUsedDataUtils.getInstance().getSelectPayTypeData(), this);
                }

                selectPayTypePop.showPopupWindow();
                break;
            case R.id.tv_recharge://立即充值
                onRecharge();
                break;
        }
    }

    //数量减
    private void reduceNum() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mTvNum.getText().toString())) {
            mTvNum.setText("1");
        } else if (new BigDecimal(mTvNum.getText().toString()).doubleValue() > 1) {
            mTvNum.setText(new BigDecimal(mTvNum.getText().toString()).subtract(new BigDecimal("1")).toPlainString());
        } else {
            showShortToast("数量最少为1");
            hideWaitDialog();
            return;
        }

        hideWaitDialog();
        setTotalPrice();
    }

    //数量加
    private void addNum() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mTvNum.getText().toString())) {
            mTvNum.setText("1");
        } else {
            mTvNum.setText(new BigDecimal(mTvNum.getText().toString()).add(new BigDecimal("1")).toPlainString());
        }

        hideWaitDialog();
        setTotalPrice();
    }

    @Override
    public void onItemClick(PayTypeModel payTypeModel) {
        mTvPayType.setTag(payTypeModel.getId());
        mTvPayType.setText(EmptyUtils.strEmpty(payTypeModel.getText()));
    }

    //立即充值
    private void onRecharge() {
        if (null == mTvPayType.getTag() || EmptyUtils.isEmpty(String.valueOf(mTvPayType.getTag()))) {
            showShortToast("请选择支付方式");
            return;
        }

        //2022-10-13 请求接口生成支付信息
        showWaitDialog();
        RechargeModel_mercenary.sendRechargeRequest_mercenary(TAG, String.valueOf(mTvPayType.getTag()), rechargeAccount, "0",
                mTvPrice.getText().toString(), "3", new CustomerJsonCallBack<RechargeModel_mercenary>() {
                    @Override
                    public void onRequestError(RechargeModel_mercenary returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(RechargeModel_mercenary returnData) {
                        hideWaitDialog();
                        serialNumber = returnData.getSerialNumber();
                        RechargeModel_mercenary.DataBean payData = returnData.getData();
                        if (payData != null && payData.getMpayInfo() != null) {
                            //获取支付信息成功
                            switch (String.valueOf(mTvPayType.getTag())) {
                                case "1"://微信
                                    invokeFusedPaySDK("4", JSON.toJSONString(payData.getMpayInfo()));
                                    break;
                                case "2"://支付宝
                                    invokeFusedPaySDK("7", JSON.toJSONString(payData.getMpayInfo()));
                                    break;
                                case "3"://云闪付
                                    invokeFusedPaySDK("5", JSON.toJSONString(payData.getMpayInfo()));
                                    break;
                            }
                        }
                    }
                });
    }

    //设置总价
    private void setTotalPrice() {
        String singleTotalPrice = new BigDecimal("1000.00").multiply(new BigDecimal(mTvNum.getText().toString())).toPlainString();
        mTvPrice.setText(singleTotalPrice);
    }

    /**
     * 调起聚合支付sdk
     *
     * @param platform 支付平台 :1 --飞凡通支付   2 --支付宝支付     3 --微信支付  4.微信支付定制版 5.云闪付 7.支付宝支付定制版
     * @param mpayInfo 移动支付的信息
     */
    private void invokeFusedPaySDK(String platform, String mpayInfo) {
        FusedPayRequest payRequest = new FusedPayRequest();
        payRequest.setPlatform(platform);
        payRequest.setMpayInfo(mpayInfo);
        if ("5".equals(platform)) {
            payRequest.setUnionPayTestEnv(false);
        }
        // CallBackSchemeId可以自定义，自定义的结果页面需实现IKuaiqianEventHandler接口
        payRequest.setCallbackSchemeId("com.jrdz.zhyb_android.ui.zhyf_manage.activity.BondRechargeActivity");
        IFusedPayApi payApi = FusedPayApiFactory.createPayApi(BondRechargeActivity.this);
        payApi.pay(payRequest);

        isPaying=true;
    }

    //==============================================支付方法回调================================================
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        isPaying=false;
        IFusedPayApi api = FusedPayApiFactory.createPayApi(this);
        api.handleIntent(getIntent(), this);
    }

    @Override
    public void onResponse(FusedPayResult fusedPayResult) {
        LogUtils.e("onResponse", "支付结果：" + fusedPayResult);
        String payResultCode = fusedPayResult.getResultStatus();
        String payResultMessage = fusedPayResult.getResultMessage();

        if ("00".equals(payResultCode)) {//00支付返回成功
            //支付成功之后
            RechargeSuccessActivity.newIntance(BondRechargeActivity.this,serialNumber, "充值成功!", "返回我的账户",true);
            goFinish();
        } else {
            showTipDialog(payResultMessage);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isPaying){
            showWaitDialog();
            getOrderPayOnlineStatus();
        }
    }

    //获取订单支付状态
    private void getOrderPayOnlineStatus() {
        QueryRechargePayModel.sendQueryRechargePayRequest(TAG, serialNumber, new CustomerJsonCallBack<QueryRechargePayModel>() {
            @Override
            public void onRequestError(QueryRechargePayModel returnData, String msg) {
                hideWaitDialog();
                isPaying=false;
            }

            @Override
            public void onRequestSuccess(QueryRechargePayModel returnData) {
                hideWaitDialog();
//                IsPaid=1或者IsPaid=2
                QueryRechargePayModel.DataBean data = returnData.getData();
                if (data != null) {
                    if ("1".equals(data.getIsPaid()) || "2".equals(data.getIsPaid())) {//支付成功
                        //支付成功之后
                        RechargeSuccessActivity.newIntance(BondRechargeActivity.this,serialNumber, "充值成功!", "返回我的账户",false);
                        goFinish();
                    } else {
                        showTipDialog("充值失败!");
                    }
                }

                isPaying=false;
            }
        });
    }

    //==============================================支付方法回调================================================

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (selectPayTypePop != null) {
            selectPayTypePop.onClean();
        }
    }

    public static void newIntance(Context context, String rechargeAccount) {
        Intent intent = new Intent(context, BondRechargeActivity.class);
        intent.putExtra("rechargeAccount", rechargeAccount);
        context.startActivity(intent);
    }
}
