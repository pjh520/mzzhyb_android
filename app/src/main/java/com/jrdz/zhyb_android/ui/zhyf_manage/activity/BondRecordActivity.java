package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-14
 * 描    述：保证金充值记录
 * ================================================
 */
public class BondRecordActivity extends RechargeRecordActivity_ZhyfManage{


    public static void newIntance(Context context) {
        Intent intent = new Intent(context, BondRecordActivity.class);
        context.startActivity(intent);
    }
}
