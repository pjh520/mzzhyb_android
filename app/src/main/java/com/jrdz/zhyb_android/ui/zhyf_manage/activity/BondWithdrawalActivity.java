package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.SpanUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PayTypeModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.widget.pop.SelectPayTypePop;

import java.math.BigDecimal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-14
 * 描    述：保证金-申请提现
 * ================================================
 */
public class BondWithdrawalActivity extends BaseActivity implements SelectPayTypePop.IOptionListener {
    private TextView mTvBalance;
    private FrameLayout mFlClean;
    private TextView mTvAllBalance;
    private LinearLayout mLlPayType;
    private TextView mTvPayType;
    private EditText mEtPayAccount;
    private EditText mEtPayName;
    private EditText mEtPayPhone;
    private TextView mTvDescribe;
    private ShapeTextView mTvCommit;

    private String depositBalance;
    private EditText mEtMoney;
    private SelectPayTypePop selectPayTypePop;

    @Override
    public int getLayoutId() {
        return R.layout.activity_apply_withdrawal_bond;
    }

    @Override
    public void initView() {
        super.initView();
        mTvBalance = findViewById(R.id.tv_balance);
        mEtMoney = findViewById(R.id.et_money);
        mFlClean = findViewById(R.id.fl_clean);
        mTvAllBalance = findViewById(R.id.tv_all_balance);
        mLlPayType = findViewById(R.id.ll_pay_type);
        mTvPayType = findViewById(R.id.tv_pay_type);
        mEtPayAccount = findViewById(R.id.et_pay_account);
        mEtPayName = findViewById(R.id.et_pay_name);
        mEtPayPhone = findViewById(R.id.et_pay_phone);
        mTvDescribe = findViewById(R.id.tv_describe);
        mTvCommit = findViewById(R.id.tv_commit);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mTitleBar).keyboardEnable(true);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        depositBalance=getIntent().getStringExtra("depositBalance");
        super.initData();

        mTvBalance.setText(EmptyUtils.strEmpty(depositBalance));

        double disff=new BigDecimal(depositBalance).doubleValue();
        if (disff>0){
            mTvCommit.setEnabled(true);
        }else {
            mTvCommit.setEnabled(false);
        }
        //设置底部描述 提现说明
        String s1 = "注意：提交提现申请后，平台完成审核后在10个工作日内将款项划拨至您指定账户。请确保您提供的为您本人实名收款账户，且真实有效，否则将审核不通过无法提" +
                "现。如因您填写的信息有误导致平台给您转账错误所引起的一切后果由您本人承担。请仔细阅读";
        String s2 = "《提现说明》";
        SpannableStringBuilder str = new SpanUtils().append(s1)
                .append(s2).setForegroundColor(Color.parseColor("#4870E0"))
                .setClickSpan(Color.parseColor("#4870E0"), false, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MyWebViewActivity.newIntance(BondWithdrawalActivity.this, "提现说明", Constants.BASE_URL + Constants.WebUrl.CASHWITHDRAWALINSTRUCTIONS_URL, true, false);
                    }
                })
                .create();
        mTvDescribe.setMovementMethod(LinkMovementMethod.getInstance());
        mTvDescribe.setHighlightColor(ContextCompat.getColor(RxTool.getContext(), android.R.color.transparent));
        mTvDescribe.setText(str);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mEtMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    mFlClean.setVisibility(View.GONE);
                } else {
                    mFlClean.setVisibility(View.VISIBLE);
                }
            }
        });
        mFlClean.setOnClickListener(this);
        mTvAllBalance.setOnClickListener(this);
        mLlPayType.setOnClickListener(this);
        mTvCommit.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_clean://清空输入的金额
                mEtMoney.setText("");
                break;
            case R.id.tv_all_balance://全部提现
                mEtMoney.setText(mTvBalance.getText().toString());
                RxTool.setEditTextCursorLocation(mEtMoney);
                break;
            case R.id.ll_pay_type://选择收款方式
                if (selectPayTypePop == null) {
                    selectPayTypePop = new SelectPayTypePop(BondWithdrawalActivity.this,
                            CommonlyUsedDataUtils.getInstance().getSelectCollectionTypeData(), this);
                }

                selectPayTypePop.showPopupWindow();
                break;
            case R.id.tv_commit://提交
                onCommit();
                break;
        }
    }

    @Override
    public void onItemClick(PayTypeModel payTypeModel) {
        mTvPayType.setTag(payTypeModel.getId());
        mTvPayType.setText(EmptyUtils.strEmpty(payTypeModel.getText()));
    }

    //提交
    private void onCommit() {
        if (EmptyUtils.isEmpty(mEtMoney.getText().toString())) {
            showShortToast("请输入要提现的金额");
            return;
        }

        double dis01 = new BigDecimal(mEtMoney.getText().toString()).doubleValue();
        if (dis01 <= 0) {
            showShortToast("请输入要提现的金额");
            return;
        }

        if (null == mTvPayType.getTag() || EmptyUtils.isEmpty(String.valueOf(mTvPayType.getTag()))) {
            showShortToast("请选择支付方式");
            return;
        }

        if (EmptyUtils.isEmpty(mEtPayAccount.getText().toString())) {
            showShortToast("请输入收款账号");
            return;
        }

        if (EmptyUtils.isEmpty(mEtPayName.getText().toString())) {
            showShortToast("请输入收款人姓名");
            return;
        }
        if (EmptyUtils.isEmpty(mEtPayPhone.getText().toString())) {
            showShortToast("请输入联系人手机号");
            return;
        }

        if (!"1".equals(String.valueOf(mEtPayPhone.getText().charAt(0))) || mEtPayPhone.getText().length() != 11) {
            showShortToast("请输入正确的手机号码");
            return;
        }

        //2022-10-13 请求接口生成支付信息
        showWaitDialog();
        BaseModel.sendApplyWithdrawalRequest(TAG, String.valueOf(mTvPayType.getTag()), mEtPayAccount.getText().toString(),
                mEtPayName.getText().toString(), mEtPayPhone.getText().toString(), mEtMoney.getText().toString(), depositBalance, "2",
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();

                        //支付成功之后
                        SuccessActivity.newIntance(BondWithdrawalActivity.this,"提现成功!", "返回我的账户");
                        goFinish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (selectPayTypePop != null) {
            selectPayTypePop.onClean();
        }
    }

    public static void newIntance(Context context, String depositBalance) {
        Intent intent = new Intent(context, BondWithdrawalActivity.class);
        intent.putExtra("depositBalance", depositBalance);
        context.startActivity(intent);
    }
}
