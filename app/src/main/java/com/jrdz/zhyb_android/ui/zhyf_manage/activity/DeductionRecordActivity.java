package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;

import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.DeductionRecordAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.DeductionRecordModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-14
 * 描    述：扣款记录 页面
 * ================================================
 */
public class DeductionRecordActivity extends BaseRecyclerViewActivity {
    @Override
    public int getLayoutId() {
        return R.layout.activity_base_recyclerview_white;
    }

    @Override
    public void initAdapter() {
        mAdapter=new DeductionRecordAdapter();
    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        super.getData();

        DeductionRecordModel.sendDeductionRecordRequest(TAG, String.valueOf(mPageNum), "20", new CustomerJsonCallBack<DeductionRecordModel>() {
            @Override
            public void onRequestError(DeductionRecordModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(DeductionRecordModel returnData) {
                hideRefreshView();
                List<DeductionRecordModel.DataBean> datas = returnData.getData();
                if (mAdapter!=null&&datas != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(datas);
                    } else {
                        mAdapter.addData(datas);
                        mAdapter.loadMoreComplete();
                    }

                    if (datas.isEmpty()) {
                        if (mAdapter.getData().size() < 4) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, DeductionRecordActivity.class);
        context.startActivity(intent);
    }
}
