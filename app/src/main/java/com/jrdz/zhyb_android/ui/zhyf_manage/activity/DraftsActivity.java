package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.DraftsAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.utils.LoginUtils;

import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-17
 * 描    述：
 * ================================================
 */
public class DraftsActivity extends BaseRecyclerViewActivity {
    private EditText mEtSearch;
    private ShapeTextView mStvSearch;
    private CustomerDialogUtils customerDialogUtils;

    private ObserverWrapper<String> mAddGoodsObserver = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.activity_drafts;
    }

    @Override
    public void initView() {
        super.initView();
        mEtSearch = findViewById(R.id.et_search);
        mStvSearch = findViewById(R.id.stv_search);
    }

    @Override
    public void initData() {
        super.initData();
        MsgBus.sendAddGoodsRefresh().observe(this, mAddGoodsObserver);
//        if (LoginUtils.isManage()) {
//            setRightTitleView("新增");
//        }

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        return new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false);
    }

    @Override
    public void initAdapter() {
        mAdapter = new DraftsAdapter();
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(false, 0.2f)
                .titleBar(mTitleBar);
        mImmersionBar.init();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                }
            }
        });
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    search();
                    return true;
                }
                return false;
            }
        });

        setSmartHasRefreshOrLoadMore();
        setLoadMore();

        mStvSearch.setOnClickListener(this);
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void getData() {
        super.getData();
        GoodsModel.sendDraftsGoodsRequest(TAG, mEtSearch.getText().toString(), String.valueOf(mPageNum), "20", new CustomerJsonCallBack<GoodsModel>() {
            @Override
            public void onRequestError(GoodsModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GoodsModel returnData) {
                hideRefreshView();
                List<GoodsModel.DataBean> infos = returnData.getData();
                if (mAdapter != null && infos != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(infos);
                        if (infos.size() <= 0) {
                            mAdapter.isUseEmpty(true);
                        }
                    } else {
                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void rightTitleViewClick() {
        AddGoodsDraftsActivity.newIntance(this);
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        //只有管理员能编辑商品
        if (LoginUtils.isManage()) {
            GoodsModel.DataBean itemData = ((DraftsAdapter) adapter).getItem(position);
            Intent intent = new Intent(this, UpdateGoodsActivity.class);
            intent.putExtra("GoodsNo", itemData.getGoodsNo());
            intent.putExtra("isChoose", itemData.isChoose());
            intent.putExtra("from", "2");
            startActivityForResult(intent, new OnActivityCallback() {
                @Override
                public void onActivityResult(int resultCode, @Nullable Intent data) {
                    if (resultCode == RESULT_OK) {
                        GoodsModel.DataBean itemDataInside = data.getParcelableExtra("itemData");
                        ((DraftsAdapter) adapter).setData(position, itemDataInside);
                    }
                }
            });
        }
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        GoodsModel.DataBean itemData = ((DraftsAdapter) adapter).getItem(position);
        switch (view.getId()) {
            case R.id.stv_btn02://删除
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(DraftsActivity.this, "确定删除所选产品吗？",
                        "注意:删除后不可恢复,谨慎操作!",
                        "取消", "确定",
                        new CustomerDialogUtils.IDialogListener() {

                            @Override
                            public void onBtn01Click() {

                            }

                            @Override
                            public void onBtn02Click() {
                                onDelete(itemData.getGoodsNo(), position);
                            }
                        });
                break;
            case R.id.stv_btn01://上架
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(DraftsActivity.this, "确定上架所选产品吗？",
                        "注意:库存为0,信息不完整的商品不能上架!",
                        "取消", "确定",
                        new CustomerDialogUtils.IDialogListener() {

                            @Override
                            public void onBtn01Click() {

                            }

                            @Override
                            public void onBtn02Click() {
                                onGrounding(itemData.getGoodsNo(), position);
                            }
                        });
                break;
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.stv_search:
                search();
                break;
        }
    }

    //删除
    private void onDelete(String id, int position) {
        showWaitDialog();
        BaseModel.sendDraftsDelGoodsRequest(TAG, id, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("删除成功");
                mAdapter.remove(position);
            }
        });
    }

    //上架
    private void onGrounding(String id, int position) {
        Log.e("666666", "id===" + id);
        showWaitDialog();
        BaseModel.sendOnOffGoodsRequest(TAG, id, "1", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("上架成功");
                mAdapter.remove(position);
            }
        });
    }

    //搜索
    private void search() {
        if (EmptyUtils.isEmpty(mEtSearch.getText().toString())) {
            showShortToast("请输入药品名称");
            return;
        }
        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, DraftsActivity.class);
        context.startActivity(intent);
    }
}
