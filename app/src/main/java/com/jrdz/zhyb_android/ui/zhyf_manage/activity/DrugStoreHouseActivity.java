package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.viewPage.BaseViewPagerAndTabsAdapter_new;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.ui.mine.activity.HwScanActivity;
import com.jrdz.zhyb_android.ui.mine.activity.ZbarScanActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.fragment.PlatformDragStoreHouseFragment;
import com.jrdz.zhyb_android.ui.zhyf_manage.fragment.ShopDragStoreHouseFragment;

import java.util.Arrays;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-18
 * 描    述：药品库
 * ================================================
 */
public class DrugStoreHouseActivity extends BaseActivity {
    private LinearLayout mLlTitle;
    private FrameLayout mFlClose;
    private AppCompatEditText mEtSearch;
    private FrameLayout mFlClean;
    private FrameLayout mFlScan;
    private TextView mTvSearch;
    private SlidingTabLayout mStbExternal;
    private ViewPager mVpExternal;

    String[] titles = new String[]{"平台药品库(医保)","平台药品库(非医保)"};
    private int from;//0:代表首页药品库进来 1:代表选择药品
    private String scanResult="";

    @Override
    public int getLayoutId() {
        return R.layout.activity_drug_storehouse;
    }

    @Override
    public void initView() {
        super.initView();
        mLlTitle = findViewById(R.id.ll_title);
        mFlClose = findViewById(R.id.fl_close);
        mEtSearch = findViewById(R.id.et_search);
        mFlClean = findViewById(R.id.fl_clean);
        mFlScan = findViewById(R.id.fl_scan);
        mTvSearch = findViewById(R.id.tv_search);

        mStbExternal = findViewById(R.id.stb_external);
        mVpExternal = findViewById(R.id.vp_external);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mLlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        from=getIntent().getIntExtra("from", 0);
        super.initData();

        initTabLayout();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    search();
                    return true;
                }
                return false;
            }
        });
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    mFlClean.setVisibility(View.GONE);
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    search();
                } else {
                    mFlClean.setVisibility(View.VISIBLE);
                }
            }
        });

        mFlClose.setOnClickListener(this);
        mFlClean.setOnClickListener(this);
        mFlScan.setOnClickListener(this);
        mTvSearch.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close://关闭
                goFinish();
                break;
            case R.id.fl_clean://清除搜索数据
                mEtSearch.setText("");
                break;
            case R.id.fl_scan://扫码
                if (Constants.Configure.IS_POS) {
                    goPosScan();
                } else {
                    goHwScan();
                }
                break;
            case R.id.tv_search://搜索
                scanResult="";
                if (EmptyUtils.isEmpty(mEtSearch.getText().toString())){
                    showShortToast("请输入药品名称");
                    return;
                }
                search();
                break;
        }
    }

    //初始化tab  平台药品库 店铺药品库
    private void initTabLayout() {
        BaseViewPagerAndTabsAdapter_new baseViewPagerAndTabsAdapter_new=new BaseViewPagerAndTabsAdapter_new(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position){
                    case 0://平台药品库
                        return PlatformDragStoreHouseFragment.newIntance(from);
                    case 1://店铺药品库
                        return ShopDragStoreHouseFragment.newIntance(from);
                    default:
                        return PlatformDragStoreHouseFragment.newIntance(from);
                }
            }
        };

        baseViewPagerAndTabsAdapter_new.setData(Arrays.asList(titles));
        mVpExternal.setAdapter(baseViewPagerAndTabsAdapter_new);
        mStbExternal.setViewPager(mVpExternal);
    }

    //使用pos机自带的扫码
    private void goPosScan() {
        startActivityForResult(new Intent(DrugStoreHouseActivity.this, ZbarScanActivity.class), new BaseActivity.OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == -1) {
                    scanResult = data.getStringExtra("scanResult");
                    search();
                }
            }
        });
    }

    //手机端使用华为扫码
    private void goHwScan() {
        startActivityForResult(new Intent(DrugStoreHouseActivity.this, HwScanActivity.class), new BaseActivity.OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == -1) {
                    scanResult = data.getStringExtra("scanResult");
                    search();
                }
            }
        });
    }

    //搜索
    private void search() {
        MsgBus.sendDrugStoreHouseSearch().post("-1");
    }

    public String getSearchText(){
        return EmptyUtils.strEmpty(mEtSearch.getText().toString());
    }

    public String getBarCode(){
        return scanResult;
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, DrugStoreHouseActivity.class);
        context.startActivity(intent);
    }
}
