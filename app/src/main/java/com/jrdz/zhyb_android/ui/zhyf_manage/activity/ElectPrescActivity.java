package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.view.ShapeView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.ElectPrescModel;

import java.math.BigDecimal;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：电子处方
 * ================================================
 */
public class ElectPrescActivity extends BaseActivity {
    private TextView mTvPrescNum;
    private TextView mTvMdtrtId;
    private TextView mTvName;
    private TextView mTvSex;
    private TextView mTvAge;
    private TextView mTvPhone;
    private TextView mTvDiagResult;
    private LinearLayout mSllDragContain;
    private ImageView mIvPhysician;
    private TextView mTvPrice;
    private TextView mTvReviewPharm;
    private TextView mTvDispePharm;
    private TextView mTvNuclearPharm;

    protected String orderNo;

    @Override
    public int getLayoutId() {
        return R.layout.activity_elect_presc;
    }

    @Override
    public void initView() {
        super.initView();
        mTvPrescNum = findViewById(R.id.tv_presc_num);
        mTvMdtrtId = findViewById(R.id.tv_mdtrt_id);
        mTvName = findViewById(R.id.tv_name);
        mTvSex = findViewById(R.id.tv_sex);
        mTvAge = findViewById(R.id.tv_age);
        mTvPhone = findViewById(R.id.tv_phone);
        mTvDiagResult = findViewById(R.id.tv_diagResult);
        mSllDragContain = findViewById(R.id.sll_drag_contain);
        mIvPhysician = findViewById(R.id.iv_physician);
        mTvPrice = findViewById(R.id.tv_price);
        mTvReviewPharm = findViewById(R.id.tv_review_pharm);
        mTvDispePharm = findViewById(R.id.tv_dispe_pharm);
        mTvNuclearPharm = findViewById(R.id.tv_nuclear_pharm);
    }

    @Override
    public void initData() {
        orderNo= getIntent().getStringExtra("orderNo");
        super.initData();

        showWaitDialog();
        getPageData();
    }

    //获取页面数据
    public void getPageData() {
        // 2022-10-13 模拟获取电子处方的数据
        ElectPrescModel.sendElectPrescModelRequest(TAG, orderNo, new CustomerJsonCallBack<ElectPrescModel>() {
            @Override
            public void onRequestError(ElectPrescModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(ElectPrescModel returnData) {
                hideWaitDialog();
                setPageData(returnData.getData());
            }
        });
    }

    //设置页面数据
    public void setPageData(ElectPrescModel.DataBean data) {
        if (data==null)return;
        ElectPrescModel.DataBean.ElectronicPrescriptionsBean electronicPrescriptions = data.getElectronicPrescriptions();
        if (electronicPrescriptions!=null){
            mTvPrescNum.setText("处方编号:"+electronicPrescriptions.getElectronicPrescriptionNo());
            mTvMdtrtId.setText("开具日期:"+electronicPrescriptions.getCreateDT());
            mTvName.setText("姓名:"+electronicPrescriptions.getPsn_name());
            mTvSex.setText("性别:"+("1".equals(electronicPrescriptions.getSex())?"男":"女"));
            mTvAge.setText("年龄:"+electronicPrescriptions.getAge());
            mTvPhone.setText("电话:"+electronicPrescriptions.getUserName());

            mTvDiagResult.setText("诊断结果:\n"+electronicPrescriptions.getDiag_name());
            mTvPrice.setText("药品金额:"+electronicPrescriptions.getMedicareAmount());
        }
        List<ElectPrescModel.DataBean.OrderGoodsBean> orderGoods = data.getOrderGoods();
        if (orderGoods!=null){
            //获取数据成功
            mSllDragContain.removeAllViews();
            for (int i = 0,size=orderGoods.size(); i < size; i++) {
                ElectPrescModel.DataBean.OrderGoodsBean itemData = orderGoods.get(i);
                View view = LayoutInflater.from(this).inflate(R.layout.layout_elect_presc_drag_item, mSllDragContain, false);
                ShapeView mTopLine = view.findViewById(R.id.top_line);
                TextView tvDragName= view.findViewById(R.id.tv_drag_name);
                TextView tvUsageTag= view.findViewById(R.id.tv_usage_tag);
                TextView tvRemarks= view.findViewById(R.id.tv_remarks);

                if (i==0){
                    mTopLine.setVisibility(View.GONE);
                }else {
                    mTopLine.setVisibility(View.VISIBLE);
                }

                tvDragName.setText((i+1)+"."+itemData.getGoodsName()+"    "+itemData.getSpec()+"    "+"*"+itemData.getGoodsNum()+"盒"+"    "+"共"+itemData.getGoodsNum()+"盒");
                tvUsageTag.setText("用法用量:"+itemData.getUsageDosage());
                tvRemarks.setText("备注:"+itemData.getRemark());
                mSllDragContain.addView(view);
            }
        }
        ElectPrescModel.DataBean.OtherinfoBean otherinfo = data.getOtherinfo();
        if (otherinfo!=null){
            GlideUtils.getImageWidHeig(ElectPrescActivity.this, otherinfo.getDoctorurl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mIvPhysician.getLayoutParams();
                    if (radio>=4){
                        layoutParams.width= (int) (getResources().getDimension(R.dimen.dp_240));
                        layoutParams.height= new BigDecimal((int) (getResources().getDimension(R.dimen.dp_240))).divide(new BigDecimal(radio),1,BigDecimal.ROUND_HALF_UP).intValue();
                    }else {
                        layoutParams.width= (int) (getResources().getDimension(R.dimen.dp_60)*radio);
                        layoutParams.height= (int) (getResources().getDimension(R.dimen.dp_60));
                    }

                    mIvPhysician.setLayoutParams(layoutParams);
                    mIvPhysician.setImageBitmap(resource);
                }
            });
        }
    }

    public static void newIntance(Context context, String orderNo) {
        Intent intent = new Intent(context, ElectPrescActivity.class);
        intent.putExtra("orderNo", orderNo);
        context.startActivity(intent);
    }
}
