package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;

import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.ElectPrescActivity2_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ElectPrescModel2;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-01-11
 * 描    述：
 * ================================================
 */
public class ElectPrescActivity2 extends ElectPrescActivity2_user {

    @Override
    public void getPageData() {
        ElectPrescModel2.sendElectPrescModelRequest(TAG, orderNo, new CustomerJsonCallBack<ElectPrescModel2>() {
            @Override
            public void onRequestError(ElectPrescModel2 returnData, String msg) {
                hideWaitDialog();
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(ElectPrescModel2 returnData) {
                hideWaitDialog();
                pagerData = returnData.getData();
                if (pagerData != null) {
                    initTabLayout(pagerData.getElectronicPrescriptions());
                }else {
                    showShortToast("数据有误,请重新进入页面");
                }
            }
        });
    }

    public static void newIntance(Context context, String orderNo) {
        Intent intent = new Intent(context, ElectPrescActivity2.class);
        intent.putExtra("orderNo", orderNo);
        context.startActivity(intent);
    }
}
