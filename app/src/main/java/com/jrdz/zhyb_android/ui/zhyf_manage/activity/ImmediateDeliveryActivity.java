package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.frame.compiler.widget.customPop.CustomerBottomListPop;
import com.frame.compiler.widget.switchButton.SwitchButton;
import com.hjq.shape.view.ShapeEditText;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.AddDepartmentActivity;
import com.jrdz.zhyb_android.ui.home.model.KVModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.LogisDeliveManageModel;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.OnlineBuyDrugActivity;
import com.jrdz.zhyb_android.utils.LoginUtils;

import java.util.Arrays;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-12
 * 描    述：立即发货
 * ================================================
 */
public class ImmediateDeliveryActivity extends BaseActivity implements CustomerBottomListPop.IBottomChooseListener<String> {
    private ImageView mIvAddressSeize;
    private TextView mTvAddressInfo;
    private TextView mTvDeliveryType;
    private LinearLayout mLlDeliveryName;
    private EditText mTvDeliveryName;
    private LinearLayout mLlDeliveryPhone;
    private EditText mTvDeliveryPhone;
    private LinearLayout mLlLogisName;
    private TextView mTvLogisName;
    private LinearLayout mLlLogisId;
    private EditText mTvLogisId;
    private TextView mTvLogisDescribe;
    private ShapeTextView mTvSure;

    private String orderNo, isLogistics, fullName, phone, location, address;
    private LogisDeliveManageModel.DataBean pagerData;
    private CustomerBottomListPop customerBottomListPop;
    private List<String> expressCompany;

    @Override
    public int getLayoutId() {
        return R.layout.activity_immediate_delivery;
    }

    @Override
    public void initView() {
        super.initView();
        mIvAddressSeize = findViewById(R.id.iv_address_seize);
        mTvAddressInfo = findViewById(R.id.tv_address_info);
        mTvDeliveryType = findViewById(R.id.tv_delivery_type);
        mLlDeliveryName = findViewById(R.id.ll_delivery_name);
        mTvDeliveryName = findViewById(R.id.tv_delivery_name);
        mLlDeliveryPhone = findViewById(R.id.ll_delivery_phone);
        mTvDeliveryPhone = findViewById(R.id.tv_delivery_phone);
        mLlLogisName = findViewById(R.id.ll_logis_name);
        mTvLogisName = findViewById(R.id.tv_logis_name);
        mLlLogisId = findViewById(R.id.ll_logis_id);
        mTvLogisId = findViewById(R.id.tv_logis_id);
        mTvLogisDescribe = findViewById(R.id.tv_logis_describe);
        mTvSure = findViewById(R.id.tv_sure);
    }

    @Override
    public void initData() {
        //是否物流（0、客户自提1、商家自配 2、商家快递）
        Intent intent = getIntent();
        orderNo = intent.getStringExtra("orderNo");
        isLogistics = intent.getStringExtra("isLogistics");
        fullName = intent.getStringExtra("fullName");
        phone = intent.getStringExtra("phone");
        location = intent.getStringExtra("location");
        address = intent.getStringExtra("address");

        super.initData();
        switch (isLogistics) {
            case "0"://客户自提
                break;
            case "1"://商家自配
                mTvDeliveryType.setText("商家自配");
                mLlDeliveryName.setVisibility(View.VISIBLE);
                mLlDeliveryPhone.setVisibility(View.VISIBLE);

                mLlLogisName.setVisibility(View.GONE);
                mLlLogisId.setVisibility(View.GONE);
                mTvLogisDescribe.setVisibility(View.GONE);
                break;
            case "2"://商家快递
                mTvDeliveryType.setText("快递物流");
                mLlDeliveryName.setVisibility(View.GONE);
                mLlDeliveryPhone.setVisibility(View.GONE);

                mLlLogisName.setVisibility(View.VISIBLE);
                mLlLogisId.setVisibility(View.VISIBLE);
                mTvLogisDescribe.setVisibility(View.VISIBLE);
                break;
        }

        mTvAddressInfo.setText(fullName + "    " + StringUtils.encryptionPhone(phone) + "\n" + location + address);

        showWaitDialog();
        getLogisDeliveData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mLlLogisName.setOnClickListener(this);
        mTvSure.setOnClickListener(this);
    }

    //获取物流配置数据
    private void getLogisDeliveData() {
        LogisDeliveManageModel.sendQueryLogisCofigRequest(TAG, new CustomerJsonCallBack<LogisDeliveManageModel>() {
            @Override
            public void onRequestError(LogisDeliveManageModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(LogisDeliveManageModel returnData) {
                hideWaitDialog();

                if (returnData.getData() != null) {
                    setPagerData(returnData.getData());
                }
            }
        });
    }

    //设置页面数据
    private void setPagerData(LogisDeliveManageModel.DataBean data) {
        pagerData = data;

        String text = data.getExpressCompany();
        if (!EmptyUtils.isEmpty(text)) {
            expressCompany = Arrays.asList(text.split(","));
        }

        if ("1".equals(isLogistics)) {
            mTvDeliveryName.setText(EmptyUtils.strEmpty(data.getSelfDeliveryName()));
            mTvDeliveryPhone.setText(EmptyUtils.strEmpty(data.getSelfDeliveryPhone()));
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.ll_logis_name://选择快递
                KeyboardUtils.hideSoftInput(ImmediateDeliveryActivity.this);
                if (customerBottomListPop == null) {
                    customerBottomListPop = new CustomerBottomListPop(this, this);
                }
                if (expressCompany != null) {
                    customerBottomListPop.setDatas("1", "请选择快递", mTvLogisName.getText().toString(), expressCompany);
                    customerBottomListPop.showPopupWindow();
                } else {
                    showShortToast("请先设置快递数据");
                }
                break;
            case R.id.tv_sure://确定
                onSure();
                break;
        }
    }

    @Override
    public void onItemClick(String tag, String str) {
        mTvLogisName.setText(str);
    }

    //确定
    private void onSure() {
        if ("1".equals(isLogistics)) {//商家配送
            if (EmptyUtils.isEmpty(mTvDeliveryName.getText().toString())) {
                showShortToast("请输入配送人姓名");
                return;
            }

            if (EmptyUtils.isEmpty(mTvDeliveryPhone.getText().toString())) {
                showShortToast("请输入配送人联系电话");
                return;
            }

            if (!"1".equals(String.valueOf(mTvDeliveryPhone.getText().charAt(0))) || mTvDeliveryPhone.getText().length() != 11) {
                showShortToast("请输入正确的手机号码");
                return;
            }
        } else {//快递配送
            if (EmptyUtils.isEmpty(mTvLogisName.getText().toString())) {
                showShortToast("请选择物流公司");
                return;
            }
            if (EmptyUtils.isEmpty(mTvLogisId.getText().toString())) {
                showShortToast("请填写快递物流单号");
                return;
            }
        }

        showWaitDialog();
        BaseModel.sendImmediateDeliveryRequest(TAG, orderNo, "2", "商家配送", fullName, phone, location + address,
                mTvDeliveryName.getText().toString(), mTvDeliveryPhone.getText().toString(), mTvLogisName.getText().toString(), mTvLogisId.getText().toString(),
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("立即发货成功");
                        setResult(RESULT_OK);
                        goFinish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (customerBottomListPop != null) {
            customerBottomListPop.dismiss();
            customerBottomListPop.onDestroy();
        }
    }
}
