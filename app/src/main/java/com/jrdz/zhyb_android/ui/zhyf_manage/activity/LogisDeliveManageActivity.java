package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.switchButton.SwitchButton;
import com.hjq.shape.view.ShapeEditText;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.LogisDeliveManageModel;
import com.jrdz.zhyb_android.utils.LoginUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-12
 * 描    述：物流配送管理
 * ================================================
 */
public class LogisDeliveManageActivity extends BaseActivity {
    private SwitchButton mSbSelfConfig;
    private EditText mEtSelfName;
    private EditText mEtSelfPhone;
    private ShapeEditText mEtSelfDistance;
    private SwitchButton mSbLogisConfig;
    private LinearLayout mLlLogisConfigContain, mLlLogisContain;
    private ShapeEditText mEtLogis;
    private TextView mTvAddLogis;
    private LinearLayout mLlProvince;
    private LinearLayout mLlNortheast;
    private LinearLayout mLlEastChina;
    private LinearLayout mLlCentralChina;
    private LinearLayout mLlSouthChina;
    private LinearLayout mLlNorthChina;
    private LinearLayout mLlNorthwest;
    private LinearLayout mLlSouthwest;
    private ShapeTextView mTvSave;

    private CustomerDialogUtils customerDialogUtils;
    private LogisDeliveManageModel.DataBean pagerData;

    @Override
    public int getLayoutId() {
        return R.layout.activity_logis_delive_manage;
    }

    @Override
    public void initView() {
        super.initView();
        mSbSelfConfig = findViewById(R.id.sb_self_config);
        mEtSelfName = findViewById(R.id.et_self_name);
        mEtSelfPhone = findViewById(R.id.et_self_phone);
        mEtSelfDistance = findViewById(R.id.et_self_distance);
        mSbLogisConfig = findViewById(R.id.sb_logis_config);
        mLlLogisConfigContain = findViewById(R.id.ll_logis_config_contain);
        mLlLogisContain = findViewById(R.id.ll_logis_contain);
        mEtLogis = findViewById(R.id.et_logis);
        mTvAddLogis = findViewById(R.id.tv_add_logis);
        mLlProvince = findViewById(R.id.ll_province);
        mLlNortheast = findViewById(R.id.ll_northeast);
        mLlEastChina = findViewById(R.id.ll_east_china);
        mLlCentralChina = findViewById(R.id.ll_central_china);
        mLlSouthChina = findViewById(R.id.ll_south_china);
        mLlNorthChina = findViewById(R.id.ll_north_china);
        mLlNorthwest = findViewById(R.id.ll_northwest);
        mLlSouthwest = findViewById(R.id.ll_southwest);

        mTvSave = findViewById(R.id.tv_save);
    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        getPagerData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mSbSelfConfig.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                // 2022-10-12 关闭的时候 需要请求接口 开启的时候 需要点击保存按钮才能生效
                if (!b) {
                    if (customerDialogUtils == null) {
                        customerDialogUtils = new CustomerDialogUtils();
                    }
                    customerDialogUtils.showDialog(LogisDeliveManageActivity.this, "确认关闭吗？", "注意:关闭商家自配,用户下单后商家将不能自己配送;" +
                                    "关闭快递物流,用户下单后不能走快递物流配送,系统将不会产生快递物流费用;如果全部关闭,系统将不支持配送,用户只能选择自提,谨慎操作!",
                            "取消", "确定",
                            new CustomerDialogUtils.IDialogListener() {

                                @Override
                                public void onBtn01Click() {
                                    mSbSelfConfig.setCheckedImmediatelyNoEvent(true);
                                }

                                @Override
                                public void onBtn02Click() {
                                    onCloseSave(1);
                                }
                            });
                }
            }
        });

        mSbLogisConfig.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                // 2022-10-12 关闭的时候 需要请求接口 开启的时候 需要点击保存按钮才能生效
                if (b) {
                    mLlLogisConfigContain.setVisibility(View.VISIBLE);
                } else {
                    if (customerDialogUtils == null) {
                        customerDialogUtils = new CustomerDialogUtils();
                    }
                    customerDialogUtils.showDialog(LogisDeliveManageActivity.this, "确认关闭吗？", "注意:关闭商家自配,用户下单后商家将不能自己配送;" +
                                    "关闭快递物流,用户下单后不能走快递物流配送,系统将不会产生快递物流费用;如果全部关闭,系统将不支持配送,用户只能选择自提,谨慎操作!",
                            "取消", "确定",
                            new CustomerDialogUtils.IDialogListener() {

                                @Override
                                public void onBtn01Click() {
                                    mSbLogisConfig.setCheckedImmediatelyNoEvent(true);
                                }

                                @Override
                                public void onBtn02Click() {
                                    onCloseSave(2);
                                }
                            });
                }
            }
        });
        mTvAddLogis.setOnClickListener(this);
        mLlProvince.setOnClickListener(this);
        mLlNortheast.setOnClickListener(this);
        mLlEastChina.setOnClickListener(this);
        mLlCentralChina.setOnClickListener(this);
        mLlSouthChina.setOnClickListener(this);
        mLlNorthChina.setOnClickListener(this);
        mLlNorthwest.setOnClickListener(this);
        mLlSouthwest.setOnClickListener(this);
        mTvSave.setOnClickListener(this);
    }

    //获取页面数据
    private void getPagerData() {
        LogisDeliveManageModel.sendQueryLogisCofigRequest(TAG, new CustomerJsonCallBack<LogisDeliveManageModel>() {
            @Override
            public void onRequestError(LogisDeliveManageModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(LogisDeliveManageModel returnData) {
                hideWaitDialog();

                if (returnData.getData() != null) {
                    setPagerData(returnData.getData());
                }
            }
        });
    }

    //设置页面数据
    private void setPagerData(LogisDeliveManageModel.DataBean data) {
        pagerData = data;
        mSbSelfConfig.setCheckedImmediatelyNoEvent("1".equals(data.getIsSelfDelivery()) ? true : false);
        mEtSelfName.setText(EmptyUtils.strEmptyToText(data.getSelfDeliveryName(), LoginUtils.getUserName()));
        mEtSelfPhone.setText(EmptyUtils.strEmptyToText(data.getSelfDeliveryPhone(), LoginUtils.getDr_phone()));
        mEtSelfDistance.setText(EmptyUtils.strEmpty(data.getSelfDeliveryArea()));
        mSbLogisConfig.setCheckedImmediatelyNoEvent("1".equals(data.getIsExpress()) ? true : false);

        if ("1".equals(data.getIsExpress())) {
            mLlLogisConfigContain.setVisibility(View.VISIBLE);
        } else {
            mLlLogisConfigContain.setVisibility(View.GONE);
        }

        if (!EmptyUtils.isEmpty(data.getExpressCompany())) {
            String[] expressCompanys = data.getExpressCompany().split(",");
            mLlLogisContain.removeAllViews();
            String expressCompany;
            for (int i = 0,size=expressCompanys.length; i < size; i++) {
                expressCompany=expressCompanys[i];
                if (i==0){
                    mEtLogis.setText(expressCompany);
                }else {
                    View view = LayoutInflater.from(LogisDeliveManageActivity.this).inflate(R.layout.layout_logis_delive_manage_name_item, mLlLogisContain, false);
                    ShapeEditText etLogis = view.findViewById(R.id.et_logis);
                    FrameLayout flDelete = view.findViewById(R.id.fl_delete);

                    etLogis.setText(expressCompany);

                    flDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mLlLogisContain.removeView(view);
                        }
                    });

                    mLlLogisContain.addView(view);
                }
            }
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_add_logis://添加快递物流公司名称
                if (mLlLogisContain.getChildCount() >= 4) {
                    showShortToast("快递物流公司,最多设置五个");
                    return;
                }
                View view = LayoutInflater.from(LogisDeliveManageActivity.this).inflate(R.layout.layout_logis_delive_manage_name_item, mLlLogisContain, false);
                FrameLayout flDelete = view.findViewById(R.id.fl_delete);
                flDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mLlLogisContain.removeView(view);
                    }
                });
                mLlLogisContain.addView(view);
                break;
            case R.id.ll_province://省内（陕西省）
                LogisSettingActivity.newIntance(LogisDeliveManageActivity.this, "1", "省内（陕西省）");
                break;
            case R.id.ll_northeast://东北：黑龙江省，辽宁省，吉林省
                LogisSettingActivity.newIntance(LogisDeliveManageActivity.this, "2", "东北");
                break;
            case R.id.ll_east_china://华东：上海市，江苏省，浙江省，安徽省，江西省
                LogisSettingActivity.newIntance(LogisDeliveManageActivity.this, "3", "华东");
                break;
            case R.id.ll_central_china://华中：河南省，湖北省，湖南省
                LogisSettingActivity.newIntance(LogisDeliveManageActivity.this, "4", "华中");
                break;
            case R.id.ll_south_china://华南：福建省，广东省，广西壮族自治区，海南省
                LogisSettingActivity.newIntance(LogisDeliveManageActivity.this, "5", "华南");
                break;
            case R.id.ll_north_china://华北：北京市，天津市，河北省，山西省，内蒙古自治区，山东省
                LogisSettingActivity.newIntance(LogisDeliveManageActivity.this, "6", "华北");
                break;
            case R.id.ll_northwest://西北：甘肃省，青海省，宁夏回族自治区，新疆维吾尔自治区
                LogisSettingActivity.newIntance(LogisDeliveManageActivity.this, "8", "西北");
                break;
            case R.id.ll_southwest://西南：重庆市，四川省，贵州省，云南省，西藏自治区
                LogisSettingActivity.newIntance(LogisDeliveManageActivity.this, "7", "西南");
                break;
            case R.id.tv_save://保存
                onSave();
                break;
        }
    }

    //保存
    private void onSave() {
        //判断快递物流 保存需要的数据
        if (!mSbSelfConfig.isChecked() && !mSbLogisConfig.isChecked()) {
            showShortToast("暂无需要保存的数据");
            return;
        }
        //判断商家自配 保存需要的数据
        if (mSbSelfConfig.isChecked()) {
            if (EmptyUtils.isEmpty(mEtSelfName.getText().toString())) {
                showShortToast("请输入配送人姓名");
                return;
            }

            if (EmptyUtils.isEmpty(mEtSelfPhone.getText().toString())) {
                showShortToast("请输入配送人电话");
                return;
            }

            if (EmptyUtils.isEmpty(mEtSelfDistance.getText().toString())) {
                showShortToast("请输入自配区域");
                return;
            }
        }

        //判断快递物流 保存需要的数据
        String logiNames = pagerData.getExpressCompany();
        if (mSbLogisConfig.isChecked()){
            if (EmptyUtils.isEmpty(mEtLogis.getText().toString())) {
                showShortToast("请输入快递物流公司名称");
                return;
            }
            if (mLlLogisContain.getChildCount()==0){
                logiNames=mEtLogis.getText().toString();
            }else {
                logiNames=mEtLogis.getText().toString()+",";
            }

            //获取设置的快递列表
            for (int i = 0, size = mLlLogisContain.getChildCount(); i < size; i++) {
                View view =mLlLogisContain.getChildAt(i);
                ShapeEditText shapeEditText=view.findViewById(R.id.et_logis);
                if (shapeEditText != null) {
                    if (EmptyUtils.isEmpty(shapeEditText.getText().toString())) {
                        showShortToast("请输入快递物流公司名称");
                        return;
                    }

                    if (i == size - 1) {
                        logiNames += shapeEditText.getText().toString().trim();
                    } else {
                        logiNames += shapeEditText.getText().toString().trim() + ",";
                    }
                }
            }
            Log.e("6666666", "logiNames==="+logiNames);
        }

        showWaitDialog();
        BaseModel.sendSaveLogisCofigRequest(TAG, mSbSelfConfig.isChecked() ? "1" : "0", mEtSelfName.getText().toString(), mEtSelfPhone.getText().toString(), mEtSelfDistance.getText().toString(),
                mSbLogisConfig.isChecked() ? "1" : "0", logiNames, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();

                        showShortToast("保存成功");
                        goFinish();
                    }
                });
    }

    //关闭保存 tag1:关闭的是商家自配 2关闭快递物流
    private void onCloseSave(int tag) {
        if (pagerData == null) {
            showShortToast("页面数据有问题,请重新进入页面");
            return;
        }
        showWaitDialog();
        BaseModel.sendSaveLogisCofigRequest(TAG, 1==tag?"0":(mSbSelfConfig.isChecked()?"1":"0"),pagerData.getSelfDeliveryName(), pagerData.getSelfDeliveryPhone(), pagerData.getSelfDeliveryArea(),
                2==tag?"0":(mSbLogisConfig.isChecked()?"1":"0"), pagerData.getExpressCompany(), new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                        if (1==tag){
                            mSbSelfConfig.setCheckedImmediatelyNoEvent(true);
                        }else if (2==tag){
                            mSbLogisConfig.setCheckedImmediatelyNoEvent(true);
                        }
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("关闭成功");
                        if (2==tag){
                            mLlLogisConfigContain.setVisibility(View.GONE);
                        }
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }

        pagerData = null;
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, LogisDeliveManageActivity.class);
        context.startActivity(intent);
    }
}
