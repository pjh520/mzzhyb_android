package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.text.InputFilter;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONObject;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.text.MoneyValueFilter;
import com.hjq.shape.view.ShapeEditText;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.model.KVModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.ProvinceAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.QueryLogisticsSetModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-12
 * 描    述：物流设置页面
 * ================================================
 */
public class LogisSettingActivity extends BaseActivity implements BaseQuickAdapter.OnItemClickListener {
    private TextView mTvProvince;
    private LinearLayout mLlRegion;
    private ImageView mIvRegion;
    private TextView mTvRegion;
    private CustomeRecyclerView mCrlProvince;
    private ShapeEditText mEtFreight;
    private LinearLayout mLlTime;
    private TextView mTvTime;
    private ShapeTextView mTvSave;

    private String type,typeName;
    private ProvinceAdapter provinceAdapter;

    private WheelUtils wheelUtils;
    private int choosePos1=5;
    private boolean isAllRegion=true;//判断是否已经全选省
    private List<QueryLogisticsSetModel.DataBean> provinceData;//记录省内的数据

    @Override
    public int getLayoutId() {
        return R.layout.layout_logis_setting;
    }

    @Override
    public void initView() {
        super.initView();
        mTvProvince = findViewById(R.id.tv_province);
        mLlRegion = findViewById(R.id.ll_region);
        mIvRegion = findViewById(R.id.iv_region);
        mTvRegion = findViewById(R.id.tv_region);
        mCrlProvince = findViewById(R.id.crl_province);
        mEtFreight = findViewById(R.id.et_freight);
        mLlTime = findViewById(R.id.ll_time);
        mTvTime = findViewById(R.id.tv_time);
        mTvSave = findViewById(R.id.tv_save);
    }

    @Override
    public void initData() {
        type=getIntent().getStringExtra("type");
        typeName=getIntent().getStringExtra("typeName");
        super.initData();

        if ("1".equals(type)){//省内配送
            mTvProvince.setVisibility(View.VISIBLE);
            mLlRegion.setVisibility(View.GONE);
            mCrlProvince.setVisibility(View.GONE);
        }else {//省外
            mTvProvince.setVisibility(View.GONE);
            mLlRegion.setVisibility(View.VISIBLE);
            mCrlProvince.setVisibility(View.VISIBLE);

            mTvRegion.setText(EmptyUtils.strEmpty(typeName));
            //初始化省 数据选择列表
            mCrlProvince.setHasFixedSize(true);
            mCrlProvince.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL,false));
            provinceAdapter=new ProvinceAdapter();
            mCrlProvince.setAdapter(provinceAdapter);
            provinceAdapter.setOnItemClickListener(this);
        }
        mEtFreight.setFilters(new InputFilter[]{new MoneyValueFilter()});

        showWaitDialog();
        getPagerData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mLlRegion.setOnClickListener(this);
        mLlTime.setOnClickListener(this);
        mTvSave.setOnClickListener(this);
    }

    //获取物流设置数据
    private void getPagerData() {
        QueryLogisticsSetModel.sendQueryLogisticsSetRequest(TAG, type, new CustomerJsonCallBack<QueryLogisticsSetModel>() {
            @Override
            public void onRequestError(QueryLogisticsSetModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(QueryLogisticsSetModel returnData) {
                hideWaitDialog();
                if (isFinishing())return;
                if (returnData.getData()!=null){
                    setPagerData(returnData.getData());
                }
            }
        });
    }

    //设置页面数据
    private void setPagerData(List<QueryLogisticsSetModel.DataBean> datas) {
        if (!"1".equals(type)){
            provinceAdapter.setNewData(datas);
            //判断是否已经全选省
            isAllChoose();
        }else {
            provinceData=datas;
        }

        //遍历数组 获取是否有设置过的地区 若有 则直接使用该地区的运费 物流时效 若没有 则使用第一条
        int pos=0;
        QueryLogisticsSetModel.DataBean datum;
        for (int i = 0,size=datas.size(); i < size; i++) {
            datum=datas.get(i);
            if ("1".equals(datum.getIsOn())){
                pos=i;
                break;
            }
        }
        datum=datas.get(pos);

        mEtFreight.setText(EmptyUtils.strEmpty(datum.getLogisticsFee()));
        mTvTime.setTag(datum.getLogisticsTimeliness());
        if ("0".equals(datum.getLogisticsTimeliness())){
            mTvTime.setText("0（当日达）");
        }else if ("1".equals(datum.getLogisticsTimeliness())){
            mTvTime.setText("1（次日达）");
        }else if ("2".equals(datum.getLogisticsTimeliness())){
            mTvTime.setText("2（隔日达）");
        }else{
            mTvTime.setText(EmptyUtils.strEmpty(datum.getLogisticsTimeliness()));
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.ll_region://是否全选
                if (isAllRegion){//已全选 重置成全部未选
                    mIvRegion.setImageResource(R.drawable.ic_dagou_nor);

                    for (QueryLogisticsSetModel.DataBean datum : provinceAdapter.getData()) {
                        datum.setIsOn("0");
                    }
                    provinceAdapter.notifyDataSetChanged();

                }else {//未全选 重置成全部选中
                    mIvRegion.setImageResource(R.drawable.ic_dagou_pre);
                    for (QueryLogisticsSetModel.DataBean datum : provinceAdapter.getData()) {
                        datum.setIsOn("1");
                    }
                    provinceAdapter.notifyDataSetChanged();
                }

                isAllRegion=!isAllRegion;
                break;
            case R.id.ll_time://物流时效
                KeyboardUtils.hideSoftInput(LogisSettingActivity.this);
                if (wheelUtils==null){
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(LogisSettingActivity.this, "", choosePos1,
                        CommonlyUsedDataUtils.getInstance().getLogisTime(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                choosePos1 = choosePos;
                                mTvTime.setTag(dateInfo.getCode());
                                mTvTime.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                            }
                        });
                break;
            case R.id.tv_save://保存
                onSave();
                break;
        }
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        ImageView ivRegion02=view.findViewById(R.id.iv_region02);
        QueryLogisticsSetModel.DataBean itemData = ((ProvinceAdapter)baseQuickAdapter).getItem(i);

        ivRegion02.setImageResource("1".equals(itemData.getIsOn())?R.drawable.ic_dagou_nor:R.drawable.ic_dagou_pre);
        itemData.setIsOn("1".equals(itemData.getIsOn())?"0":"1");

        isAllChoose();
    }
    //判断是否已经全选省
    private void isAllChoose(){
        isAllRegion=true;
        for (QueryLogisticsSetModel.DataBean dataBean : provinceAdapter.getData()) {
            if ("0".equals(dataBean.getIsOn())){
                isAllRegion=false;
                break;
            }
        }

        mIvRegion.setImageResource(isAllRegion?R.drawable.ic_dagou_pre:R.drawable.ic_dagou_nor);
    }

    //保存
    private void onSave() {
        if (EmptyUtils.isEmpty(mEtFreight.getText().toString())) {
            showShortToast("请输入运费");
            return;
        }

        double fright=new BigDecimal(mEtFreight.getText().toString()).doubleValue();
        if (fright<=0) {
            showShortToast("运费必须大于0");
            return;
        }

        JSONObject jsonObject = new JSONObject();
        if ("1".equals(type)){//省内配送
            if (provinceData==null||provinceData.isEmpty()){
                showShortToast("数据有误，请重新进入页面");
                return;
            }
            QueryLogisticsSetModel.DataBean itemData = provinceData.get(0);
            itemData.setLogisticsFee(mEtFreight.getText().toString());
            itemData.setLogisticsTimeliness(String.valueOf(mTvTime.getTag()));
            itemData.setIsOn("1");
            jsonObject.put("AreaName",provinceData);
        }else {
            List<QueryLogisticsSetModel.DataBean> datas = provinceAdapter.getData();
            for (QueryLogisticsSetModel.DataBean data : datas) {
                data.setLogisticsFee(mEtFreight.getText().toString());
                data.setLogisticsTimeliness(String.valueOf(mTvTime.getTag()));
            }
            jsonObject.put("AreaName",provinceAdapter.getData());
        }

        showWaitDialog();
        BaseModel.sendSaveLogisticsSetRequest(TAG, jsonObject.toJSONString(), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("保存成功");
                goFinish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (wheelUtils != null) {
            wheelUtils.dissWheel();
        }
    }

    public static void newIntance(Context context, String type, String typeName) {
        Intent intent = new Intent(context, LogisSettingActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("typeName", typeName);
        context.startActivity(intent);
    }
}
