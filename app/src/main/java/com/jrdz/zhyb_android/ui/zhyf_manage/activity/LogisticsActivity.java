package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxClipboardTool;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.LogisticsModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-11
 * 描    述：查看物流页面
 * ================================================
 */
public class LogisticsActivity extends BaseActivity {
    private LinearLayout mLl01;
    private TextView mTvStatus01;
    private TextView mTvTime01;
    private TextView mTvLogisticsNum01;
    private TextView mTvLogisticsNumCopy01;
    private TextView mTvShipperName01;
    private TextView mTvShipperPhone01;
    private FrameLayout mFlShipperPhoneTake01;
    private LinearLayout mLl02;
    private TextView mTvStatus02;
    private LinearLayout mLl03;
    private TextView mTvStatus03;
    private TextView mTvTime03;
    private TextView mTvLogisticsNum03;
    private TextView mTvLogisticsNumCopy03;
    private TextView mTvShipperName03;
    private TextView mTvShipperPhone03;
    private FrameLayout mFlShipperPhoneTake03;

    private CustomerDialogUtils customerDialogUtils;
    private String orderNo;
    private LogisticsModel.DataBean pagerData;

    @Override
    public int getLayoutId() {
        return R.layout.activity_logistics;
    }

    @Override
    public void initView() {
        super.initView();
        mLl01 = findViewById(R.id.ll_01);
        mTvStatus01 = findViewById(R.id.tv_status_01);
        mTvTime01 = findViewById(R.id.tv_time_01);
        mTvLogisticsNum01 = findViewById(R.id.tv_logistics_num01);
        mTvLogisticsNumCopy01 = findViewById(R.id.tv_logistics_num_copy01);
        mTvShipperName01 = findViewById(R.id.tv_shipper_name_01);
        mTvShipperPhone01 = findViewById(R.id.tv_shipper_phone_01);
        mFlShipperPhoneTake01 = findViewById(R.id.fl_shipper_phone_take_01);
        mLl02 = findViewById(R.id.ll_02);
        mTvStatus02 = findViewById(R.id.tv_status_02);
        mLl03 = findViewById(R.id.ll_03);
        mTvStatus03 = findViewById(R.id.tv_status_03);
        mTvTime03 = findViewById(R.id.tv_time_03);
        mTvLogisticsNum03 = findViewById(R.id.tv_logistics_num_03);
        mTvLogisticsNumCopy03 = findViewById(R.id.tv_logistics_num_copy_03);
        mTvShipperName03 = findViewById(R.id.tv_shipper_name_03);
        mTvShipperPhone03 = findViewById(R.id.tv_shipper_phone_03);
        mFlShipperPhoneTake03 = findViewById(R.id.fl_shipper_phone_take_03);
    }

    @Override
    public void initData() {
        orderNo = getIntent().getStringExtra("orderNo");
        super.initData();

        showWaitDialog();
        getPagerData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvLogisticsNumCopy01.setOnClickListener(this);
        mTvLogisticsNumCopy03.setOnClickListener(this);

        mFlShipperPhoneTake01.setOnClickListener(this);
        mFlShipperPhoneTake03.setOnClickListener(this);
    }

    //获取页面数据
    private void getPagerData() {
        LogisticsModel.sendLogisticsRequest(TAG, orderNo, new CustomerJsonCallBack<LogisticsModel>() {
            @Override
            public void onRequestError(LogisticsModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(LogisticsModel returnData) {
                hideWaitDialog();
                setPagerData(returnData.getData());
            }
        });
    }

    //设置页面数据
    private void setPagerData(LogisticsModel.DataBean data) {
        if (data==null) {
            showShortToast("数据有误，请重新进入页面");
            return;
        }
        pagerData=data;
        if ("1".equals(data.getIsLogistics())){//商户自配
            mTvLogisticsNum01.setVisibility(View.GONE);
            mTvLogisticsNumCopy01.setVisibility(View.GONE);
            mTvLogisticsNum03.setVisibility(View.GONE);
            mTvLogisticsNumCopy03.setVisibility(View.GONE);

            if ("1".equals(data.getIsConfirm())){//用户确认收货
                mTvTime01.setVisibility(View.VISIBLE);
                mTvShipperName01.setVisibility(View.VISIBLE);
                mTvShipperPhone01.setVisibility(View.VISIBLE);
                mFlShipperPhoneTake01.setVisibility(View.VISIBLE);

                mTvStatus01.setText("确认收货");
                mTvTime01.setText(data.getConfirmationTime()+" "+data.getConfirmationInfo());
                mTvShipperName01.setText("配送人："+data.getSelfDeliveryName());
                mTvShipperPhone01.setText("配送人电话："+data.getSelfDeliveryPhone());
            }else {
                mTvTime01.setVisibility(View.GONE);
                mTvShipperName01.setVisibility(View.GONE);
                mTvShipperPhone01.setVisibility(View.GONE);
                mFlShipperPhoneTake01.setVisibility(View.GONE);

                mTvStatus01.setText("等待用户确认收货");
            }

            mTvShipperName03.setVisibility(View.VISIBLE);
            mTvShipperPhone03.setVisibility(View.VISIBLE);
            mFlShipperPhoneTake03.setVisibility(View.VISIBLE);
            mTvShipperName03.setText("配送人："+data.getSelfDeliveryName());
            mTvShipperPhone03.setText("配送人电话："+data.getSelfDeliveryPhone());
        } else {//快递配送
            if ("1".equals(data.getIsConfirm())){//用户确认收货
                mTvTime01.setVisibility(View.VISIBLE);
                mTvLogisticsNum01.setVisibility(View.VISIBLE);
                mTvLogisticsNumCopy01.setVisibility(View.VISIBLE);

                mTvStatus01.setText("确认收货");
                mTvTime01.setText(data.getConfirmationTime()+" "+data.getConfirmationInfo());
                mTvLogisticsNum01.setText(data.getExpressCompany()+"单号:  "+data.getExpressNo());
            }else {
                mTvTime01.setVisibility(View.GONE);
                mTvLogisticsNum01.setVisibility(View.GONE);
                mTvLogisticsNumCopy01.setVisibility(View.GONE);

                mTvStatus01.setText("等待用户确认收货");
            }
            mTvLogisticsNum03.setVisibility(View.VISIBLE);
            mTvLogisticsNumCopy03.setVisibility(View.VISIBLE);
            mTvLogisticsNum03.setText(data.getExpressCompany()+"单号:  "+data.getExpressNo());

            mTvShipperName01.setVisibility(View.GONE);
            mTvShipperPhone01.setVisibility(View.GONE);
            mFlShipperPhoneTake01.setVisibility(View.GONE);
            mTvShipperName03.setVisibility(View.GONE);
            mTvShipperPhone03.setVisibility(View.GONE);
            mFlShipperPhoneTake03.setVisibility(View.GONE);
        }
        mTvTime03.setText(data.getCreateDT()+" "+data.getLogisticsProgressInfo());
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_logistics_num_copy01:
                if (pagerData!=null){
                    RxClipboardTool.copyText(LogisticsActivity.this, pagerData.getExpressNo());
                }
                break;
            case R.id.tv_logistics_num_copy_03:
                if (pagerData!=null){
                    RxClipboardTool.copyText(LogisticsActivity.this, pagerData.getExpressNo());
                }
                break;
            case R.id.fl_shipper_phone_take_01:
                if (pagerData!=null&&!EmptyUtils.isEmpty(pagerData.getSelfDeliveryPhone())){
                    onTakePhone(pagerData.getSelfDeliveryPhone());
                }
                break;
            case R.id.fl_shipper_phone_take_03:
                if (pagerData!=null&&!EmptyUtils.isEmpty(pagerData.getSelfDeliveryPhone())){
                    onTakePhone(pagerData.getSelfDeliveryPhone());
                }
                break;
        }
    }

    //联系买家
    private void onTakePhone(String phone) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(LogisticsActivity.this, "提示", "是否拨打配送人电话" + phone + "?", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                RxTool.takePhone(LogisticsActivity.this, phone);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context, String orderNo) {
        Intent intent = new Intent(context, LogisticsActivity.class);
        intent.putExtra("orderNo", orderNo);
        context.startActivity(intent);
    }
}
