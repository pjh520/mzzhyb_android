package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSON;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.JustifyContent;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.model.DiseCataModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.RelationDiseAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.RelationDisetypeAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.DragStoreHouseModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodDetailModel;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.GoodDetailActivity;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.widget.MyFlexboxLayoutManager;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-22
 * 描    述：查看商品
 * ================================================
 */
public class LookGoodsActivity extends BaseActivity {
    private RelativeLayout mLlHorizontalPic;
    private TextView mTv01;
    private ShapeFrameLayout mSflHorizontalPic01;
    private LinearLayout mLlAddHorizontalPic01;
    private FrameLayout mFlHorizontalPic01;
    private ImageView mIvHorizontalPic01;
    private ImageView mIvDelete01;
    private TextView mTvBanner01;
    private ShapeFrameLayout mSflHorizontalPic02;
    private LinearLayout mLlAddHorizontalPic02;
    private FrameLayout mFlHorizontalPic02;
    private ImageView mIvHorizontalPic02;
    private ImageView mIvDelete02;
    private TextView mTvBanner02;
    private ShapeFrameLayout mSflHorizontalPic03;
    private LinearLayout mLlAddHorizontalPic03;
    private FrameLayout mFlHorizontalPic03;
    private ImageView mIvHorizontalPic03;
    private ImageView mIvDelete03;
    private TextView mTvBanner03;
    private ShapeTextView mTvBannerTime;
    private LinearLayout mLlDrugsInfoContain;
    private ShapeTextView mTvSave;

    //西药中成药特有
    protected EditText mEtBrand, mEtSpecs, mEtPacking, mEtProdentpName, mEtApplyRange, mEtSort;
    protected TextView mTvType;

    protected LinearLayout mLlRelationDisetype;
    protected View mLineRelationDisetype01;
    protected View mLineRelationDisetype02;

    protected CustomeRecyclerView mCrvRelationDisetype;
    protected CustomeRecyclerView mCrvRelationDise;
    protected EditText mEtFunctions;
    protected EditText mEtCommonUsage;
    protected EditText mEtStorageConditions;
    //药品共用
    protected EditText mEtDrugName, mEtDrugProductName;
    protected EditText mEtDrugId;
    protected TextView mEtNumPrice;
    protected TextView mEtPromotion;
    protected TextView mEtNumPromotion;
    protected TextView mEtEstimatePrice;
    protected TextView mEtNum;
    protected TextView mEtValidity;
    protected LinearLayout mLlDrugManual;
    protected FrameLayout mFlDrugManual;
    protected ImageView mIvDrugManual;
    protected ImageView mIvDrugManualDelete;
    protected EditText mEtStoreSimilarity;
    protected LinearLayout mLlCommodityDetails01;
    protected FrameLayout mFlCommodityDetails01;
    protected ImageView mIvCommodityDetails01;
    protected ImageView mIvCommodityDetailsDelete01;
    protected LinearLayout mLlCommodityDetails02;
    protected FrameLayout mFlCommodityDetails02;
    protected ImageView mIvCommodityDetails02;
    protected ImageView mIvCommodityDetailsDelete02;
    protected LinearLayout mLlCommodityDetails03;
    protected FrameLayout mFlCommodityDetails03;
    protected ImageView mIvCommodityDetails03;
    protected ImageView mIvCommodityDetailsDelete03;
    protected LinearLayout mLlCommodityDetails04;
    protected FrameLayout mFlCommodityDetails04;
    protected ImageView mIvCommodityDetails04;
    protected ImageView mIvCommodityDetailsDelete04;
    protected LinearLayout mLlCommodityDetails05;
    protected FrameLayout mFlCommodityDetails05;
    protected ImageView mIvCommodityDetails05;
    protected ImageView mIvCommodityDetailsDelete05;

    private String GoodsNo;
    private GoodDetailModel.DataBean pagerData;
    protected DragStoreHouseModel.DataBean itemData;//选中 的药品数据
    protected RelationDisetypeAdapter relationDisetypeAdapter;//关联病种与诊断信息适配器
    protected RelationDiseAdapter relationDiseAdapter;//关联疾病适配器
    protected CustomerDialogUtils customerDialogUtils;
    private boolean hasLowerShelfBtn=true;

    @Override
    public int getLayoutId() {
        return R.layout.activity_look_goods;
    }

    @Override
    public void initView() {
        super.initView();

        mLlHorizontalPic = findViewById(R.id.ll_horizontal_pic);
        mTv01 = findViewById(R.id.tv_01);
        mSflHorizontalPic01 = findViewById(R.id.sfl_horizontal_pic01);
        mLlAddHorizontalPic01 = findViewById(R.id.ll_add_horizontal_pic01);
        mFlHorizontalPic01 = findViewById(R.id.fl_horizontal_pic01);
        mIvHorizontalPic01 = findViewById(R.id.iv_horizontal_pic01);
        mIvDelete01 = findViewById(R.id.iv_delete01);
        mTvBanner01 = findViewById(R.id.tv_banner01);
        mSflHorizontalPic02 = findViewById(R.id.sfl_horizontal_pic02);
        mLlAddHorizontalPic02 = findViewById(R.id.ll_add_horizontal_pic02);
        mFlHorizontalPic02 = findViewById(R.id.fl_horizontal_pic02);
        mIvHorizontalPic02 = findViewById(R.id.iv_horizontal_pic02);
        mIvDelete02 = findViewById(R.id.iv_delete02);
        mTvBanner02 = findViewById(R.id.tv_banner02);
        mSflHorizontalPic03 = findViewById(R.id.sfl_horizontal_pic03);
        mLlAddHorizontalPic03 = findViewById(R.id.ll_add_horizontal_pic03);
        mFlHorizontalPic03 = findViewById(R.id.fl_horizontal_pic03);
        mIvHorizontalPic03 = findViewById(R.id.iv_horizontal_pic03);
        mIvDelete03 = findViewById(R.id.iv_delete03);
        mTvBanner03 = findViewById(R.id.tv_banner03);
        mTvBannerTime = findViewById(R.id.tv_banner_time);
        mLlDrugsInfoContain = findViewById(R.id.ll_drugs_info_contain);
        mTvSave = findViewById(R.id.tv_save);
    }

    @Override
    public void initData() {
        GoodsNo = getIntent().getStringExtra("GoodsNo");
        hasLowerShelfBtn = getIntent().getBooleanExtra("hasLowerShelfBtn",true);
        super.initData();

        if (hasLowerShelfBtn){
            mTvSave.setVisibility(View.VISIBLE);
        }else {
            mTvSave.setVisibility(View.GONE);
        }

        showWaitDialog();
        getPagerData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mIvHorizontalPic01.setOnClickListener(this);
        mIvHorizontalPic02.setOnClickListener(this);
        mIvHorizontalPic03.setOnClickListener(this);

        mTvSave.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.iv_horizontal_pic01://banner 1号位
                showBigPic(mIvHorizontalPic01,(String)mIvHorizontalPic01.getTag(R.id.tag_2));
                break;
            case R.id.iv_horizontal_pic02://banner 2号位
                showBigPic(mIvHorizontalPic02,(String)mIvHorizontalPic02.getTag(R.id.tag_2));
                break;
            case R.id.iv_horizontal_pic03://banner 3号位
                showBigPic(mIvHorizontalPic03,(String)mIvHorizontalPic03.getTag(R.id.tag_2));
                break;
            case R.id.iv_drug_manual:
                showBigPic(mIvDrugManual,(String)mIvDrugManual.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details01:
                showBigPic(mIvCommodityDetails01,(String)mIvCommodityDetails01.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details02:
                showBigPic(mIvCommodityDetails02,(String)mIvCommodityDetails02.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details03:
                showBigPic(mIvCommodityDetails03,(String)mIvCommodityDetails03.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details04:
                showBigPic(mIvCommodityDetails04,(String)mIvCommodityDetails04.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details05:
                showBigPic(mIvCommodityDetails05,(String)mIvCommodityDetails05.getTag(R.id.tag_2));
                break;
            case R.id.tv_save://下架
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(LookGoodsActivity.this, "确定下架所选产品吗？","注意:有未完成订单的产品的不能下架!",
                        "取消", "确定",
                        new CustomerDialogUtils.IDialogListener() {

                            @Override
                            public void onBtn01Click() {

                            }

                            @Override
                            public void onBtn02Click() {
                                onLowerShelf(GoodsNo);
                            }
                        });
                break;
        }
    }

    //获取页面数据
    private void getPagerData() {
        GoodDetailModel.sendGoodDetailRequest(TAG, GoodsNo, new CustomerJsonCallBack<GoodDetailModel>() {
            @Override
            public void onRequestError(GoodDetailModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GoodDetailModel returnData) {
                hideWaitDialog();
                if (isFinishing())return;
                GoodDetailModel.DataBean data = returnData.getData();
                if (data != null) {
                    setPagerData(data);
                }
            }
        });
    }

    //设置详情数据
    private void setPagerData(GoodDetailModel.DataBean data) {
        pagerData = data;
        //设置banner图片
        if (!EmptyUtils.isEmpty(data.getBannerAccessoryUrl1())) {
            GlideUtils.getImageWidHeig(this, data.getBannerAccessoryUrl1(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner01Success(data.getBannerAccessoryId1(), data.getBannerAccessoryUrl1(), width, height);
                }
            });
        }
        if (!EmptyUtils.isEmpty(data.getBannerAccessoryUrl2())) {
            GlideUtils.getImageWidHeig(this, data.getBannerAccessoryUrl2(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner02Success(data.getBannerAccessoryId2(), data.getBannerAccessoryUrl2(), width, height);
                }
            });
        }
        if (!EmptyUtils.isEmpty(data.getBannerAccessoryUrl3())) {
            GlideUtils.getImageWidHeig(this, data.getBannerAccessoryUrl3(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner03Success(data.getBannerAccessoryId3(), data.getBannerAccessoryUrl3(), width, height);
                }
            });
        }
        mTvBannerTime.setTag(data.getIntervaltime());
        mTvBannerTime.setText(data.getIntervaltime() + "S");
        //设置药品数据
        itemData = new DragStoreHouseModel.DataBean(data.getInstructionsAccessoryUrl(), data.getDetailAccessoryUrl1(), data.getDetailAccessoryUrl2(),
                data.getDetailAccessoryUrl3(), data.getDetailAccessoryUrl4(), data.getDetailAccessoryUrl5(), "", data.getMIC_Code(),
                data.getItemCode(), data.getItemName(),data.getGoodsName(),data.getItemType(), data.getApprovalNumber(), data.getRegDosform(), data.getSpec(),
                data.getEnterpriseName(), data.getEfccAtd(), data.getUsualWay(), data.getStorageConditions(), data.getExpiryDateCount(),
                data.getInventoryQuantity(), data.getPrice(), data.getInstructionsAccessoryId(), data.getDetailAccessoryId1(), data.getDetailAccessoryId2(),
                data.getDetailAccessoryId3(), data.getDetailAccessoryId4(), data.getDetailAccessoryId5(), data.getRegistrationCertificateNo(),
                data.getProductionLicenseNo(), data.getBrand(), data.getPackaging(), data.getApplicableScope(), data.getUsageDosage(), data.getPrecautions(),
                data.getUsagemethod(), data.getIsPlatformDrug(), data.getDrugsClassification(), data.getDrugsClassificationName(),
                data.getAssociatedDiseases(),data.getAssociatedDiagnostics(),data.getIsPromotion());

        if ("1".equals(data.getIsPlatformDrug())) {//平台药品库-西药中成药
            addPlatformWestDragInfo(itemData);
        } else {
            switch (data.getDrugsClassification()) {
                case "1"://店铺药品库-西药中成药
                    addShopWestDragInfo(itemData);
                    break;
                case "2"://店铺药品库-医疗器械
                    addShopApparatusDragInfo(itemData);
                    break;
                case "3"://店铺药品库-成人用品
                    addShopAdultproduceDragInfo(itemData);
                    break;
                case "4"://店铺药品库-个人用品
                    addShopPerproduceDragInfo(itemData);
                    break;
            }
        }
    }

    //展示大图
    private void showBigPic(ImageView imageView,String imageUrl) {
        OpenImage.with(LookGoodsActivity.this)
                //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                .setClickImageView(imageView)
                //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                //RecyclerView的数据
                .setImageUrl(imageUrl,MediaType.IMAGE)
                //点击的ImageView所在数据的位置
                .setClickPosition(0)
                //开始展示大图
                .show();
    }
    //下架
    private void onLowerShelf(String id) {
        Log.e("666666", "id===" + id);
        showWaitDialog();
        BaseModel.sendOnOffGoodsRequest(TAG, id, "2", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("下架成功");
                setResult(RESULT_OK);
                goFinish();
            }
        });
    }

    //banner 1号位
    protected void selectBanner01Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic01.getLayoutParams();
            if (radio >= 2.025) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_486));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_486))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_240) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_240));
            }

            mIvHorizontalPic01.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic01);

            mLlAddHorizontalPic01.setVisibility(View.GONE);
            mLlAddHorizontalPic01.setEnabled(false);

            mIvHorizontalPic01.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic01.setTag(R.id.tag_2, url);
            mFlHorizontalPic01.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //banner 2号位
    protected void selectBanner02Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic02.getLayoutParams();
            if (radio >= 2.025) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_486));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_486))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_240) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_240));
            }

            mIvHorizontalPic02.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic02);

            mLlAddHorizontalPic02.setVisibility(View.GONE);
            mLlAddHorizontalPic02.setEnabled(false);

            mIvHorizontalPic02.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic02.setTag(R.id.tag_2, url);
            mFlHorizontalPic02.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //banner 3号位
    protected void selectBanner03Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic03.getLayoutParams();
            if (radio >= 2.025) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_486));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_486))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_240) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_240));
            }

            mIvHorizontalPic03.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic03);

            mLlAddHorizontalPic03.setVisibility(View.GONE);
            mLlAddHorizontalPic03.setEnabled(false);

            mIvHorizontalPic03.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic03.setTag(R.id.tag_2, url);
            mFlHorizontalPic03.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //药品说明书
    protected void selectDrugManualSuccess(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvDrugManual.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvDrugManual.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvDrugManual);

            mLlDrugManual.setVisibility(View.GONE);
            mLlDrugManual.setEnabled(false);

            mIvDrugManual.setTag(R.id.tag_1, accessoryId);
            mIvDrugManual.setTag(R.id.tag_2, url);
            mFlDrugManual.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //商品详情01
    protected void selectCommodityDetails01Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails01.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvCommodityDetails01.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCommodityDetails01);

            mLlCommodityDetails01.setVisibility(View.GONE);
            mLlCommodityDetails01.setEnabled(false);

            mIvCommodityDetails01.setTag(R.id.tag_1, accessoryId);
            mIvCommodityDetails01.setTag(R.id.tag_2, url);
            mFlCommodityDetails01.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //商品详情02
    protected void selectCommodityDetails02Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails02.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvCommodityDetails02.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCommodityDetails02);

            mLlCommodityDetails02.setVisibility(View.GONE);
            mLlCommodityDetails02.setEnabled(false);

            mIvCommodityDetails02.setTag(R.id.tag_1, accessoryId);
            mIvCommodityDetails02.setTag(R.id.tag_2, url);
            mFlCommodityDetails02.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //商品详情03
    protected void selectCommodityDetails03Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails03.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvCommodityDetails03.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCommodityDetails03);

            mLlCommodityDetails03.setVisibility(View.GONE);
            mLlCommodityDetails03.setEnabled(false);

            mIvCommodityDetails03.setTag(R.id.tag_1, accessoryId);
            mIvCommodityDetails03.setTag(R.id.tag_2, url);
            mFlCommodityDetails03.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //商品详情04
    protected void selectCommodityDetails04Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails04.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvCommodityDetails04.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCommodityDetails04);

            mLlCommodityDetails04.setVisibility(View.GONE);
            mLlCommodityDetails04.setEnabled(false);

            mIvCommodityDetails04.setTag(R.id.tag_1, accessoryId);
            mIvCommodityDetails04.setTag(R.id.tag_2, url);
            mFlCommodityDetails04.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //商品详情05
    protected void selectCommodityDetails05Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails05.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvCommodityDetails05.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCommodityDetails05);

            mLlCommodityDetails05.setVisibility(View.GONE);
            mLlCommodityDetails05.setEnabled(false);

            mIvCommodityDetails05.setTag(R.id.tag_1, accessoryId);
            mIvCommodityDetails05.setTag(R.id.tag_2, url);
            mFlCommodityDetails05.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }


    //------------------------选择药品之后展示的布局----------------------------------------
    //添加药品  平台药品库-西药中成药
    protected void addPlatformWestDragInfo(DragStoreHouseModel.DataBean itemData) {
        mLlDrugsInfoContain.removeAllViews();
        LayoutInflater.from(this).inflate(R.layout.layout_look_goods_platform_west, mLlDrugsInfoContain, true);

        mEtSpecs = findViewById(R.id.et_specs);
        mTvType = findViewById(R.id.tv_type);

        mLlRelationDisetype = findViewById(R.id.ll_relation_disetype);
        mLineRelationDisetype01 = findViewById(R.id.line_relation_disetype01);
        mLineRelationDisetype02 = findViewById(R.id.line_relation_disetype02);
        mCrvRelationDisetype = findViewById(R.id.crv_relation_disetype);

        mCrvRelationDise = findViewById(R.id.crv_relation_dise);
        mEtFunctions = findViewById(R.id.et_functions);
        mEtCommonUsage = findViewById(R.id.et_common_usage);
        mEtStorageConditions = findViewById(R.id.et_storage_conditions);
        addDragPublicLayout(itemData);

        //设置数据
        mEtSpecs.setText(EmptyUtils.strEmpty(itemData.getSpec()));
        if (EmptyUtils.isEmpty(itemData.getItemType())||"0".equals(itemData.getItemType())){
            mTvType.setText("");
        }else {
            mTvType.setTag(itemData.getItemType());
            mTvType.setText("1".equals(itemData.getItemType()) ? "非处方药（OTC）" : "处方药");
        }
        mEtFunctions.setText(EmptyUtils.strEmpty(itemData.getEfccAtd()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsualWay()));
        mEtStorageConditions.setText(EmptyUtils.strEmpty(itemData.getStorageConditions()));

        if ("1".equals(MechanismInfoUtils.getFixmedinsType())) {//医院
            mLlRelationDisetype.setVisibility(View.VISIBLE);
            mLineRelationDisetype01.setVisibility(View.VISIBLE);
            mLineRelationDisetype02.setVisibility(View.VISIBLE);
            mCrvRelationDisetype.setVisibility(View.VISIBLE);
        } else if ("2".equals(MechanismInfoUtils.getFixmedinsType())) {//药店
            mLlRelationDisetype.setVisibility(View.GONE);
            mLineRelationDisetype01.setVisibility(View.GONE);
            mLineRelationDisetype02.setVisibility(View.GONE);
            mCrvRelationDisetype.setVisibility(View.GONE);
        }

        //关联病种与诊断信息
        mCrvRelationDisetype.setHasFixedSize(true);
        mCrvRelationDisetype.setLayoutManager(getLayoutManager());
        relationDisetypeAdapter = new RelationDisetypeAdapter();
        mCrvRelationDisetype.setAdapter(relationDisetypeAdapter);
        //设置关联诊断
        List<DiseCataModel.DataBean> dataBeans = JSON.parseArray(pagerData.getAssociatedDiagnostics(), DiseCataModel.DataBean.class);
        relationDisetypeAdapter.setNewData(dataBeans);

        //关联疾病
        mCrvRelationDise.setHasFixedSize(true);
        mCrvRelationDise.setLayoutManager(getLayoutManager());
        relationDiseAdapter = new RelationDiseAdapter();
        mCrvRelationDise.setAdapter(relationDiseAdapter);
        //关联疾病的数据
        if (!EmptyUtils.isEmpty(itemData.getAssociatedDiseases())){
            String[] associatedDiseases = itemData.getAssociatedDiseases().split("∞#");
            relationDiseAdapter.setNewData(Arrays.asList(associatedDiseases));
        }

        mTvType.setOnClickListener(this);
    }

    //添加药品 店铺药品库-西药中成药
    protected void addShopWestDragInfo(DragStoreHouseModel.DataBean itemData) {
        mLlDrugsInfoContain.removeAllViews();
        LayoutInflater.from(this).inflate(R.layout.layout_look_goods_shop_west, mLlDrugsInfoContain, true);

        mEtSpecs = findViewById(R.id.et_specs);
        mTvType = findViewById(R.id.tv_type);

        mCrvRelationDise = findViewById(R.id.crv_relation_dise);
        mEtFunctions = findViewById(R.id.et_functions);
        mEtCommonUsage = findViewById(R.id.et_common_usage);

        mEtStorageConditions = findViewById(R.id.et_storage_conditions);
        addDragPublicLayout(itemData);

        //设置数据
        mEtSpecs.setText(EmptyUtils.strEmpty(itemData.getSpec()));
        if (EmptyUtils.isEmpty(itemData.getItemType())||"0".equals(itemData.getItemType())){
            mTvType.setText("");
        }else {
            mTvType.setTag(itemData.getItemType());
            mTvType.setText("1".equals(itemData.getItemType()) ? "非处方药（OTC）" : "处方药");
        }

        mEtFunctions.setText(EmptyUtils.strEmpty(itemData.getEfccAtd()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsualWay()));
        mEtStorageConditions.setText(EmptyUtils.strEmpty(itemData.getStorageConditions()));

        //关联疾病
        mCrvRelationDise.setHasFixedSize(true);
        mCrvRelationDise.setLayoutManager(getLayoutManager());
        relationDiseAdapter = new RelationDiseAdapter();
        mCrvRelationDise.setAdapter(relationDiseAdapter);
        //关联疾病的数据
        String[] associatedDiseases = itemData.getAssociatedDiseases().split("∞#");
        relationDiseAdapter.setNewData(Arrays.asList(associatedDiseases));
    }

    //添加药品 店铺药品库-医疗器械
    protected void addShopApparatusDragInfo(DragStoreHouseModel.DataBean itemData) {
        mLlDrugsInfoContain.removeAllViews();
        LayoutInflater.from(this).inflate(R.layout.layout_look_goods_shop_apparatus, mLlDrugsInfoContain, true);

        mEtBrand = findViewById(R.id.et_brand);
        mEtSpecs = findViewById(R.id.et_specs);
        mEtPacking = findViewById(R.id.et_packing);
        mEtProdentpName = findViewById(R.id.et_prodentp_name);
        mEtApplyRange = findViewById(R.id.et_apply_range);
        mEtCommonUsage = findViewById(R.id.et_common_usage);

        addDragPublicLayout(itemData);

        //设置数据
        mEtBrand.setText(EmptyUtils.strEmpty(itemData.getBrand()));
        mEtSpecs.setText(EmptyUtils.strEmpty(itemData.getSpec()));
        mEtPacking.setText(EmptyUtils.strEmpty(itemData.getPackaging()));
        mEtProdentpName.setText(EmptyUtils.strEmpty(itemData.getEnterpriseName()));
        mEtApplyRange.setText(EmptyUtils.strEmpty(itemData.getApplicableScope()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsageDosage()));
    }

    //添加药品 店铺药品库-成人用品
    protected void addShopAdultproduceDragInfo(DragStoreHouseModel.DataBean itemData) {
        mLlDrugsInfoContain.removeAllViews();
        LayoutInflater.from(this).inflate(R.layout.layout_look_goods_shop_adultproduct, mLlDrugsInfoContain, true);

        mEtBrand = findViewById(R.id.et_brand);
        mEtSpecs = findViewById(R.id.et_specs);
        mEtPacking = findViewById(R.id.et_packing);
        mEtProdentpName = findViewById(R.id.et_prodentp_name);
        mEtCommonUsage = findViewById(R.id.et_common_usage);
        mEtStorageConditions = findViewById(R.id.et_storage_conditions);

        addDragPublicLayout(itemData);

        //设置数据
        mEtBrand.setText(EmptyUtils.strEmpty(itemData.getBrand()));
        mEtSpecs.setText(EmptyUtils.strEmpty(itemData.getSpec()));
        mEtPacking.setText(EmptyUtils.strEmpty(itemData.getPackaging()));
        mEtProdentpName.setText(EmptyUtils.strEmpty(itemData.getEnterpriseName()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsagemethod()));
        mEtStorageConditions.setText(EmptyUtils.strEmpty(itemData.getStorageConditions()));
    }

    //添加药品  店铺药品库-个人用品
    protected void addShopPerproduceDragInfo(DragStoreHouseModel.DataBean itemData) {
        mLlDrugsInfoContain.removeAllViews();
        LayoutInflater.from(this).inflate(R.layout.layout_look_goods_shop_perproduct, mLlDrugsInfoContain, true);

        mEtBrand = findViewById(R.id.et_brand);
        mEtSpecs = findViewById(R.id.et_specs);
        mEtPacking = findViewById(R.id.et_packing);
        mEtProdentpName = findViewById(R.id.et_prodentp_name);
        mEtCommonUsage = findViewById(R.id.et_common_usage);
        mEtStorageConditions = findViewById(R.id.et_storage_conditions);

        addDragPublicLayout(itemData);

        //设置数据
        mEtBrand.setText(EmptyUtils.strEmpty(itemData.getBrand()));
        mEtSpecs.setText(EmptyUtils.strEmpty(itemData.getSpec()));
        mEtPacking.setText(EmptyUtils.strEmpty(itemData.getPackaging()));
        mEtProdentpName.setText(EmptyUtils.strEmpty(itemData.getEnterpriseName()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsageDosage()));
        mEtStorageConditions.setText(EmptyUtils.strEmpty(itemData.getStorageConditions()));
    }

    //添加药品--公共的布局
    protected void addDragPublicLayout(DragStoreHouseModel.DataBean itemData) {
        mEtDrugName = findViewById(R.id.et_drug_name);
        mEtDrugId = findViewById(R.id.et_drug_id);
        mEtDrugProductName = findViewById(R.id.et_drug_product_name);
        mEtSort = findViewById(R.id.et_sort);
        //药品价格 价格
        mEtNumPrice = findViewById(R.id.et_num_price);
        //是否促销 促销折扣
        mEtPromotion = findViewById(R.id.et_promotion);
        mEtNumPromotion = findViewById(R.id.et_num_promotion);
        //预估到手价
        mEtEstimatePrice = findViewById(R.id.et_estimate_price);
        //库存数量
        mEtNum = findViewById(R.id.et_num);
        //有效期
        mEtValidity = findViewById(R.id.et_validity);
        //说明书 店内相似 商品详情
        mLlDrugManual = findViewById(R.id.ll_drug_manual);
        mFlDrugManual = findViewById(R.id.fl_drug_manual);
        mIvDrugManual = findViewById(R.id.iv_drug_manual);
        mIvDrugManualDelete = findViewById(R.id.iv_drug_manual_delete);
        mEtStoreSimilarity = findViewById(R.id.et_store_similarity);
        mLlCommodityDetails01 = findViewById(R.id.ll_commodity_details01);
        mFlCommodityDetails01 = findViewById(R.id.fl_commodity_details01);
        mIvCommodityDetails01 = findViewById(R.id.iv_commodity_details01);
        mIvCommodityDetailsDelete01 = findViewById(R.id.iv_commodity_details_delete01);
        mLlCommodityDetails02 = findViewById(R.id.ll_commodity_details02);
        mFlCommodityDetails02 = findViewById(R.id.fl_commodity_details02);
        mIvCommodityDetails02 = findViewById(R.id.iv_commodity_details02);
        mIvCommodityDetailsDelete02 = findViewById(R.id.iv_commodity_details_delete02);
        mLlCommodityDetails03 = findViewById(R.id.ll_commodity_details03);
        mFlCommodityDetails03 = findViewById(R.id.fl_commodity_details03);
        mIvCommodityDetails03 = findViewById(R.id.iv_commodity_details03);
        mIvCommodityDetailsDelete03 = findViewById(R.id.iv_commodity_details_delete03);
        mLlCommodityDetails04 = findViewById(R.id.ll_commodity_details04);
        mFlCommodityDetails04 = findViewById(R.id.fl_commodity_details04);
        mIvCommodityDetails04 = findViewById(R.id.iv_commodity_details04);
        mIvCommodityDetailsDelete04 = findViewById(R.id.iv_commodity_details_delete04);
        mLlCommodityDetails05 = findViewById(R.id.ll_commodity_details05);
        mFlCommodityDetails05 = findViewById(R.id.fl_commodity_details05);
        mIvCommodityDetails05 = findViewById(R.id.iv_commodity_details05);
        mIvCommodityDetailsDelete05 = findViewById(R.id.iv_commodity_details_delete05);

        //设置数据
        mEtDrugName.setText(EmptyUtils.strEmpty(itemData.getItemName()));
        mEtDrugId.setText(EmptyUtils.strEmpty(itemData.getItemCode()));
        mEtDrugProductName.setText(EmptyUtils.isEmpty(itemData.getGoodsName())?EmptyUtils.strEmpty(itemData.getItemName()):itemData.getGoodsName());
        mEtSort.setText(EmptyUtils.strEmpty(itemData.getDrugsClassificationName()));
        mEtNumPrice.setText(EmptyUtils.strEmpty(itemData.getPrice()));
        mEtPromotion.setTag(itemData.getIsPromotion());
        mEtPromotion.setText("1".equals(itemData.getIsPromotion())?"是":"否");
        //折扣
        mEtNumPromotion.setText("打"+pagerData.getPromotionDiscount()+"折");
        //计算预估到手价
        mEtEstimatePrice.setText(EmptyUtils.strEmpty(pagerData.getPreferentialPrice()));
        //库存数量
        mEtNum.setText(EmptyUtils.strEmpty(itemData.getInventoryQuantity()));
        //有效期
        mEtValidity.setText(EmptyUtils.strEmpty(itemData.getExpiryDateCount())+"个月");
        //说明书 商品详情
        if (!EmptyUtils.isEmpty(itemData.getInstructionsAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, itemData.getInstructionsAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectDrugManualSuccess(itemData.getInstructionsAccessoryId(), itemData.getInstructionsAccessoryUrl(), width, height);
                }
            });
        }
        mEtStoreSimilarity.setText(EmptyUtils.strEmpty(pagerData.getStoreSimilarity()));
        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl1())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl1(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectCommodityDetails01Success(itemData.getDetailAccessoryId1(), itemData.getDetailAccessoryUrl1(), width, height);
                }
            });
        }
        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl2())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl2(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectCommodityDetails02Success(itemData.getDetailAccessoryId2(), itemData.getDetailAccessoryUrl2(), width, height);
                }
            });
        }
        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl3())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl3(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectCommodityDetails03Success(itemData.getDetailAccessoryId3(), itemData.getDetailAccessoryUrl3(), width, height);
                }
            });
        }
        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl4())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl4(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectCommodityDetails04Success(itemData.getDetailAccessoryId4(), itemData.getDetailAccessoryUrl4(), width, height);
                }
            });
        }
        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl5())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl5(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectCommodityDetails05Success(itemData.getDetailAccessoryId5(), itemData.getDetailAccessoryUrl5(), width, height);
                }
            });
        }

        mIvDrugManual.setOnClickListener(this);
        mIvCommodityDetails01.setOnClickListener(this);
        mIvCommodityDetails02.setOnClickListener(this);
        mIvCommodityDetails03.setOnClickListener(this);
        mIvCommodityDetails04.setOnClickListener(this);
        mIvCommodityDetails05.setOnClickListener(this);
    }
    //------------------------选择药品之后展示的布局----------------------------------------

    public RecyclerView.LayoutManager getLayoutManager() {
        MyFlexboxLayoutManager flexboxLayoutManager = new MyFlexboxLayoutManager(this);
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        flexboxLayoutManager.setFlexWrap(FlexWrap.WRAP);
        flexboxLayoutManager.setJustifyContent(JustifyContent.FLEX_START);

        return flexboxLayoutManager;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }
}
