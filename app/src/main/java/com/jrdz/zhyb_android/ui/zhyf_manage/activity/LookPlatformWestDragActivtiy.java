package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSON;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.glide.GlideUtils;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.JustifyContent;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.catalogue.model.DiseCataModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.RelationDiseAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.RelationDisetypeAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.DragStoreHouseModel;
import com.jrdz.zhyb_android.widget.MyFlexboxLayoutManager;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-19
 * 描    述：查看--平台药品库--西药中成药
 * ================================================
 */
public class LookPlatformWestDragActivtiy extends BaseActivity {
    private TextView mTvDrugName;
    private TextView mTvDrugId;
    private TextView mTvType;
    private EditText mEtAprvnoBegndate;
    private EditText mEtRegDosform;
    private EditText mEtSpec;
    private EditText mEtProdentpName;
    private EditText mEtFuncMain;
    private EditText mEtCommonUsage;
    private EditText mEtStorageConditions;
    private CustomeRecyclerView mCrvRelationDisetype;
    private CustomeRecyclerView mCrvRelationDise;
    private TextView mTvValidity;
    private TextView mTvNum;
    private TextView mTvPrice;

    private ImageView mIvDrugManual;

    private ShapeFrameLayout mSflCommodityDetails01;
    private ImageView mIvCommodityDetails01;
    private ShapeFrameLayout mSflCommodityDetails02;
    private ImageView mIvCommodityDetails02;
    private ShapeFrameLayout mSflCommodityDetails03;
    private ImageView mIvCommodityDetails03;
    private ShapeFrameLayout mSflCommodityDetails04;
    private ImageView mIvCommodityDetails04;
    private ShapeFrameLayout mSflCommodityDetails05;
    private ImageView mIvCommodityDetails05;

    private DragStoreHouseModel.DataBean itemData;
    private RelationDisetypeAdapter relationDisetypeAdapter;
    private RelationDiseAdapter relationDiseAdapter;
    
    @Override
    public int getLayoutId() {
        return R.layout.activtiy_look_platfrom_westdrag;
    }

    @Override
    public void initView() {
        super.initView();
        mTvDrugName = findViewById(R.id.tv_drug_name);
        mTvDrugId = findViewById(R.id.tv_drug_id);
        mTvType = findViewById(R.id.tv_type);
        mEtAprvnoBegndate = findViewById(R.id.et_aprvno_begndate);
        mEtRegDosform = findViewById(R.id.et_reg_dosform);
        mEtSpec = findViewById(R.id.et_spec);
        mEtProdentpName = findViewById(R.id.et_prodentp_name);
        mEtFuncMain = findViewById(R.id.et_func_main);
        mEtCommonUsage = findViewById(R.id.et_common_usage);
        mEtStorageConditions = findViewById(R.id.et_storage_conditions);
        mCrvRelationDisetype = findViewById(R.id.crv_relation_disetype);
        mCrvRelationDise = findViewById(R.id.crv_relation_dise);
        mTvValidity = findViewById(R.id.tv_validity);
        mTvNum = findViewById(R.id.tv_num);
        mTvPrice = findViewById(R.id.tv_price);

        mIvDrugManual = findViewById(R.id.iv_drug_manual);

        mSflCommodityDetails01 = findViewById(R.id.sfl_commodity_details01);
        mIvCommodityDetails01 = findViewById(R.id.iv_commodity_details01);

        mSflCommodityDetails02 = findViewById(R.id.sfl_commodity_details02);
        mIvCommodityDetails02 = findViewById(R.id.iv_commodity_details02);

        mSflCommodityDetails03 = findViewById(R.id.sfl_commodity_details03);
        mIvCommodityDetails03 = findViewById(R.id.iv_commodity_details03);

        mSflCommodityDetails04 = findViewById(R.id.sfl_commodity_details04);
        mIvCommodityDetails04 = findViewById(R.id.iv_commodity_details04);

        mSflCommodityDetails05 = findViewById(R.id.sfl_commodity_details05);
        mIvCommodityDetails05 = findViewById(R.id.iv_commodity_details05);
    }

    @Override
    public void initData() {
        itemData=getIntent().getParcelableExtra("itemData");
        super.initData();

        //设置数据
        mTvDrugName.setText(EmptyUtils.strEmpty(itemData.getItemName()));
        mTvDrugId.setText(EmptyUtils.strEmpty(itemData.getItemCode()));

        if (EmptyUtils.isEmpty(itemData.getItemType())||"0".equals(itemData.getItemType())){
            mTvType.setText("");
        }else {
            mTvType.setTag(itemData.getItemType());
            mTvType.setText("1".equals(itemData.getItemType()) ? "非处方药（OTC）" : "处方药");
        }

        mEtAprvnoBegndate.setText(EmptyUtils.strEmpty(itemData.getApprovalNumber()));
        mEtRegDosform.setText(EmptyUtils.strEmpty(itemData.getRegDosform()));
        mEtSpec.setText(EmptyUtils.strEmpty(itemData.getSpec()));
        mEtProdentpName.setText(EmptyUtils.strEmpty(itemData.getEnterpriseName()));
        mEtFuncMain.setText(EmptyUtils.strEmpty(itemData.getEfccAtd()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsualWay()));
        mEtStorageConditions.setText(EmptyUtils.strEmpty(itemData.getStorageConditions()));

        mTvValidity.setText(EmptyUtils.strEmpty(itemData.getExpiryDateCount()));
        mTvNum.setText(EmptyUtils.strEmpty(itemData.getInventoryQuantity()));
        mTvPrice.setText(EmptyUtils.strEmpty(itemData.getPrice()));

        //关联病种与诊断信息
        mCrvRelationDisetype.setHasFixedSize(true);
        mCrvRelationDisetype.setLayoutManager(getLayoutManager());
        relationDisetypeAdapter = new RelationDisetypeAdapter();
        mCrvRelationDisetype.setAdapter(relationDisetypeAdapter);
        //关联病种与诊断信息的数据
        List<DiseCataModel.DataBean> dataBeans = JSON.parseArray(itemData.getAssociatedDiagnostics(), DiseCataModel.DataBean.class);
        relationDisetypeAdapter.setNewData(dataBeans);

        //关联疾病
        mCrvRelationDise.setHasFixedSize(true);
        mCrvRelationDise.setLayoutManager(getLayoutManager());
        relationDiseAdapter = new RelationDiseAdapter();
        mCrvRelationDise.setAdapter(relationDiseAdapter);
        //关联病种的数据
        if (!EmptyUtils.isEmpty(itemData.getAssociatedDiseases())){
            String[] relationDises=itemData.getAssociatedDiseases().split("∞#");
            relationDiseAdapter.setNewData(Arrays.asList(relationDises));
        }

        //设置药品说明书图片
        if (!EmptyUtils.isEmpty(itemData.getInstructionsAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, itemData.getInstructionsAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvDrugManual.getLayoutParams();
                    if (radio >= 2.029) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                    }

                    mIvDrugManual.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(itemData.getInstructionsAccessoryUrl(), mIvDrugManual);

                    mIvDrugManual.setTag(R.id.tag_1, itemData.getInstructionsAccessoryId());
                    mIvDrugManual.setTag(R.id.tag_2, itemData.getInstructionsAccessoryUrl());
                    mIvDrugManual.setVisibility(View.VISIBLE);
                }
            });
        }

        //设置商品详情图片
        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl1())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl1(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails01.getLayoutParams();
                    if (radio >= 2.029) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                    }

                    mIvCommodityDetails01.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(itemData.getDetailAccessoryUrl1(), mIvCommodityDetails01);

                    mIvCommodityDetails01.setTag(R.id.tag_1, itemData.getDetailAccessoryId1());
                    mIvCommodityDetails01.setTag(R.id.tag_2, itemData.getDetailAccessoryUrl1());
                    mIvCommodityDetails01.setVisibility(View.VISIBLE);
                }
            });
        }

        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl2())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl2(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails02.getLayoutParams();
                    if (radio >= 2.029) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                    }

                    mIvCommodityDetails02.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(itemData.getDetailAccessoryUrl2(), mIvCommodityDetails02);

                    mIvCommodityDetails02.setTag(R.id.tag_1, itemData.getDetailAccessoryId2());
                    mIvCommodityDetails02.setTag(R.id.tag_2, itemData.getDetailAccessoryUrl2());
                    mIvCommodityDetails02.setVisibility(View.VISIBLE);
                }
            });
        }

        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl3())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl3(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails03.getLayoutParams();
                    if (radio >= 2.029) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                    }

                    mIvCommodityDetails03.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(itemData.getDetailAccessoryUrl3(), mIvCommodityDetails03);

                    mIvCommodityDetails03.setTag(R.id.tag_1, itemData.getDetailAccessoryId3());
                    mIvCommodityDetails03.setTag(R.id.tag_2, itemData.getDetailAccessoryUrl3());
                    mIvCommodityDetails03.setVisibility(View.VISIBLE);
                }
            });
        }

        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl4())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl4(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails04.getLayoutParams();
                    if (radio >= 2.029) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                    }

                    mIvCommodityDetails04.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(itemData.getDetailAccessoryUrl4(), mIvCommodityDetails04);

                    mIvCommodityDetails04.setTag(R.id.tag_1, itemData.getDetailAccessoryId4());
                    mIvCommodityDetails04.setTag(R.id.tag_2, itemData.getDetailAccessoryUrl4());
                    mIvCommodityDetails04.setVisibility(View.VISIBLE);
                }
            });
        }

        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl5())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl5(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails05.getLayoutParams();
                    if (radio >= 2.029) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                    }

                    mIvCommodityDetails05.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(itemData.getDetailAccessoryUrl5(), mIvCommodityDetails05);

                    mIvCommodityDetails05.setTag(R.id.tag_1, itemData.getDetailAccessoryId5());
                    mIvCommodityDetails05.setTag(R.id.tag_2, itemData.getDetailAccessoryUrl5());
                    mIvCommodityDetails05.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mIvDrugManual.setOnClickListener(this);
        mIvCommodityDetails01.setOnClickListener(this);
        mIvCommodityDetails02.setOnClickListener(this);
        mIvCommodityDetails03.setOnClickListener(this);
        mIvCommodityDetails04.setOnClickListener(this);
        mIvCommodityDetails05.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.iv_drug_manual://banner 1号位
                showBigPic(mIvDrugManual,(String)mIvDrugManual.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details01:
                showBigPic(mIvCommodityDetails01,(String)mIvCommodityDetails01.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details02:
                showBigPic(mIvCommodityDetails02,(String)mIvCommodityDetails02.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details03:
                showBigPic(mIvCommodityDetails03,(String)mIvCommodityDetails03.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details04:
                showBigPic(mIvCommodityDetails04,(String)mIvCommodityDetails04.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details05:
                showBigPic(mIvCommodityDetails05,(String)mIvCommodityDetails05.getTag(R.id.tag_2));
                break;
        }
    }


    //展示大图
    private void showBigPic(ImageView imageView,String imageUrl) {
        OpenImage.with(LookPlatformWestDragActivtiy.this)
                //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                .setClickImageView(imageView)
                //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                //RecyclerView的数据
                .setImageUrl(imageUrl, MediaType.IMAGE)
                //点击的ImageView所在数据的位置
                .setClickPosition(0)
                //开始展示大图
                .show();
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        MyFlexboxLayoutManager flexboxLayoutManager = new MyFlexboxLayoutManager(this);
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        flexboxLayoutManager.setFlexWrap(FlexWrap.WRAP);
        flexboxLayoutManager.setJustifyContent(JustifyContent.FLEX_START);

        return flexboxLayoutManager;
    }

    public static void newIntance(Context context,DragStoreHouseModel.DataBean itemData) {
        Intent intent = new Intent(context, LookPlatformWestDragActivtiy.class);
        intent.putExtra("itemData", itemData);
        context.startActivity(intent);
    }
}
