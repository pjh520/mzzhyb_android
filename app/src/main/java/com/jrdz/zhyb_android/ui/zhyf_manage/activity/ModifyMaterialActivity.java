package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.SettleInApplyResultModel;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-09-28
 * 描    述：入驻申请 修改材料
 * ================================================
 */
public class ModifyMaterialActivity extends SettleInApplyActivity{
    private TextView mTvRefuse;

    private SettleInApplyResultModel.DataBean pagerData;

    @Override
    public int getLayoutId() {
        return R.layout.activity_modify_material;
    }

    @Override
    public void initView() {
        super.initView();
        mTvRefuse = findViewById(R.id.tv_refuse);
    }

    @Override
    public void initData() {
        pagerData=getIntent().getParcelableExtra("pagerData");
        super.initData();
        //设置数据
        mTvRefuse.setText("申请注册驳回原因："+pagerData.getRejectReason());

        mEtRealName.setText(EmptyUtils.strEmpty(pagerData.getDirector()));
        mEtPhone.setText(EmptyUtils.strEmpty(pagerData.getFixmedins_phone()));
        mEtSettlBankcard.setText(EmptyUtils.strEmpty(pagerData.getSettlementBankCard()));
        mEtSettlBank.setText(EmptyUtils.strEmpty(pagerData.getBank()));
        //法人身份证正面照片
        if (!EmptyUtils.isEmpty(pagerData.getFrontAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, pagerData.getFrontAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic01,mIvPic01,mIvAddLicense01,mIvAddPhoto01,pagerData.getFrontAccessoryId(), pagerData.getFrontAccessoryUrl(), width, height);
                }
            });
        }
        //法人身份证反面照片
        if (!EmptyUtils.isEmpty(pagerData.getBackAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, pagerData.getBackAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic02,mIvPic02,mIvAddLicense02,mIvAddPhoto02,pagerData.getBackAccessoryId(), pagerData.getBackAccessoryUrl(), width, height);
                }
            });
        }
        //上传医疗机构执业许可证或营业执照
        if (!EmptyUtils.isEmpty(pagerData.getBusinessLicenseAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, pagerData.getBusinessLicenseAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic03,mIvPic03,mIvAddLicense03,mIvAddPhoto03,pagerData.getBusinessLicenseAccessoryId(), pagerData.getBusinessLicenseAccessoryUrl(), width, height);
                }
            });
        }
        //药品经营许可证
        if (!EmptyUtils.isEmpty(pagerData.getDrugBLAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, pagerData.getDrugBLAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic04,mIvPic04,mIvAddLicense04,mIvAddPhoto04,pagerData.getDrugBLAccessoryId(), pagerData.getDrugBLAccessoryUrl(), width, height);
                }
            });
        }
        //医疗器械经营许可证
        if (!EmptyUtils.isEmpty(pagerData.getMedicalDeviceBLAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, pagerData.getMedicalDeviceBLAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic05,mIvPic05,mIvAddLicense05,mIvAddPhoto05,pagerData.getMedicalDeviceBLAccessoryId(), pagerData.getMedicalDeviceBLAccessoryUrl(), width, height);
                }
            });
        }
        //GS认证证书
        if (!EmptyUtils.isEmpty(pagerData.getGSCertificateAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, pagerData.getGSCertificateAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic06,mIvPic06,mIvAddLicense06,mIvAddPhoto06,pagerData.getGSCertificateAccessoryId(), pagerData.getGSCertificateAccessoryUrl(), width, height);
                }
            });
        }
        //开户许可证
        if (!EmptyUtils.isEmpty(pagerData.getAccountLicenceAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, pagerData.getAccountLicenceAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic07,mIvPic07,mIvAddLicense07,mIvAddPhoto07,pagerData.getAccountLicenceAccessoryId(), pagerData.getAccountLicenceAccessoryUrl(), width, height);
                }
            });
        }
        //门头照片
        if (!EmptyUtils.isEmpty(pagerData.getDoorHeaderAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, pagerData.getDoorHeaderAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic08,mIvPic08,mIvAddLicense08,mIvAddPhoto08,pagerData.getDoorHeaderAccessoryId(), pagerData.getDoorHeaderAccessoryUrl(), width, height);
                }
            });
        }
        //内景照片1
        if (!EmptyUtils.isEmpty(pagerData.getInteriorOneAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, pagerData.getInteriorOneAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic09,mIvPic09,mIvAddLicense09,mIvAddPhoto09,pagerData.getInteriorOneAccessoryId(), pagerData.getInteriorOneAccessoryUrl(), width, height);
                }
            });
        }
        //内景照片2
        if (!EmptyUtils.isEmpty(pagerData.getInteriorTwoAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, pagerData.getInteriorTwoAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic10,mIvPic10,mIvAddLicense10,mIvAddPhoto10,pagerData.getInteriorTwoAccessoryId(), pagerData.getInteriorTwoAccessoryUrl(), width, height);
                }
            });
        }
    }

    @Override
    protected void onSetPicCert() {
        if (!EmptyUtils.isEmpty(pagerData.getBusinessLicenseAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, pagerData.getBusinessLicenseAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic03,mIvPic03,mIvAddLicense03,mIvAddPhoto03,pagerData.getBusinessLicenseAccessoryId(), pagerData.getBusinessLicenseAccessoryUrl(), width, height);
                }
            });
        }
    }

    public static void newIntance(Context context, SettleInApplyResultModel.DataBean pagerData) {
        Intent intent = new Intent(context, ModifyMaterialActivity.class);
        intent.putExtra("pagerData", pagerData);
        context.startActivity(intent);
    }
}
