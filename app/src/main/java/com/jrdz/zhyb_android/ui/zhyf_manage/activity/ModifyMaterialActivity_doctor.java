package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.SettleInApplyResultModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-27
 * 描    述：入驻申请 修改材料--医师端
 * ================================================
 */
public class ModifyMaterialActivity_doctor extends SettleInApplyActivity_doctor{
    private TextView mTvRefuse;

    private SettleInApplyResultModel.DataBean pagerData;

    @Override
    public int getLayoutId() {
        return R.layout.activity_modify_material_doctor;
    }

    @Override
    public void initView() {
        super.initView();
        mTvRefuse = findViewById(R.id.tv_refuse);
    }

    @Override
    public void initData() {
        pagerData=getIntent().getParcelableExtra("pagerData");
        super.initData();

        //设置数据
        mTvRefuse.setText("申请注册驳回原因："+pagerData.getRejectReason());
        mEtLicenseNo.setText(EmptyUtils.strEmpty(pagerData.getQualificationCertificateNo()));
        //选择身份证人像正面照
        if (!EmptyUtils.isEmpty(pagerData.getFrontAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, pagerData.getFrontAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicFrontPhotoSuccess(pagerData.getFrontAccessoryId(), pagerData.getFrontAccessoryUrl(), width, height);
                }
            });
        }
        //选择身份证国徽反面照
        if (!EmptyUtils.isEmpty(pagerData.getBackAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, pagerData.getBackAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicNegativePhotoSuccess(pagerData.getBackAccessoryId(), pagerData.getBackAccessoryUrl(), width, height);
                }
            });
        }
        mEtCardno.setText(EmptyUtils.strEmpty(pagerData.getIDNumber()));
    }

    public static void newIntance(Context context, SettleInApplyResultModel.DataBean pagerData) {
        Intent intent = new Intent(context, ModifyMaterialActivity_doctor.class);
        intent.putExtra("pagerData", pagerData);
        context.startActivity(intent);
    }
}
