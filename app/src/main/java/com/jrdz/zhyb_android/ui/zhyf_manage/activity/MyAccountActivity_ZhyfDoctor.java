package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.adapter.MonthSettQueryAdapter;
import com.jrdz.zhyb_android.ui.home.model.MonthSettModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.MyAccountModel_doctor;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-26
 * 描    述：我的账户--商户-医师端
 * ================================================
 */
public class MyAccountActivity_ZhyfDoctor extends BaseActivity implements BaseQuickAdapter.OnItemClickListener {
    private FrameLayout mFlForward;
    private RecyclerView mRlDate;
    private FrameLayout mFlBackoff;
    private TextView mTvBalanceTag01;
    private TextView mTvAccount;
    private TextView mTvBalanceTag02;
    private TextView mTvBalance;
    private ShapeTextView mTvWithdrawal;
    private TextView mTvOpenNum;
    private TextView mTvOpenSinglePrice;
    private TextView mTvOpenTotalPrice;

    private LinearLayoutManager mLinearLayoutManager;
    private MonthSettQueryAdapter monthSettQueryAdapter;
    private MonthSettModel currentMonthSettModel;
    private ShapeTextView currentView;

    @Override
    public int getLayoutId() {
        return R.layout.activity_myaccount_zhyfdoctor;
    }

    @Override
    public void initView() {
        super.initView();
        mFlForward = findViewById(R.id.fl_forward);
        mRlDate = findViewById(R.id.rl_date);
        mFlBackoff = findViewById(R.id.fl_backoff);
        mTvBalanceTag01 = findViewById(R.id.tv_balance_tag01);
        mTvAccount = findViewById(R.id.tv_account);
        mTvBalanceTag02 = findViewById(R.id.tv_balance_tag02);
        mTvBalance = findViewById(R.id.tv_balance);
        mTvWithdrawal = findViewById(R.id.tv_Withdrawal);
        mTvOpenNum = findViewById(R.id.tv_open_num);
        mTvOpenSinglePrice = findViewById(R.id.tv_open_single_price);
        mTvOpenTotalPrice = findViewById(R.id.tv_open_total_price);
    }

    @Override
    public void initData() {
        super.initData();

        //设置列表
        mLinearLayoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        mRlDate.setLayoutManager(mLinearLayoutManager);
        monthSettQueryAdapter = new MonthSettQueryAdapter();
        mRlDate.setAdapter(monthSettQueryAdapter);
        monthSettQueryAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getDateData());
        currentMonthSettModel=monthSettQueryAdapter.getItem(0);
        mTvAccount.requestFocus();

        showWaitDialog();
        queryMonSetl();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        monthSettQueryAdapter.setOnItemClickListener(this);
        mTvWithdrawal.setOnClickListener(this);
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            return;
        }
        MonthSettModel item = ((MonthSettQueryAdapter) baseQuickAdapter).getItem(i);
        if (!item.isChoose()) {
            if (currentMonthSettModel != null) {
                currentMonthSettModel.setChoose(false);
            }else {
                ((MonthSettQueryAdapter) baseQuickAdapter).getItem(0).setChoose(false);
            }

            if (currentView != null) {
                currentView.getShapeDrawableBuilder().setStrokeColor(getResources().getColor(R.color.color_333333))
                        .setSolidColor(getResources().getColor(R.color.bar_transparent)).intoBackground();
                currentView.setTextColor(getResources().getColor(R.color.color_333333));
            }else {
                currentView = (ShapeTextView) baseQuickAdapter.getViewByPosition(mRlDate, 0,R.id.tv);
                currentView.getShapeDrawableBuilder().setStrokeColor(getResources().getColor(R.color.color_333333))
                        .setSolidColor(getResources().getColor(R.color.bar_transparent)).intoBackground();
                currentView.setTextColor(getResources().getColor(R.color.color_333333));
            }

            currentView = view.findViewById(R.id.tv);
            currentMonthSettModel = item;

            currentMonthSettModel.setChoose(true);
            currentView.getShapeDrawableBuilder().setStrokeColor(getResources().getColor(R.color.bar_transparent))
                    .setSolidColor(getResources().getColor(R.color.color_4970e0)).intoBackground();
            currentView.setTextColor(getResources().getColor(R.color.white));

            queryMonSetl();
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_Withdrawal://提现
                ApplyWithdrawalActivity_doctor.newIntance(MyAccountActivity_ZhyfDoctor.this,mTvBalance.getText().toString());
                break;
        }
    }

    //查询账户
    private void queryMonSetl() {
        MyAccountModel_doctor.sendMyAccountRequest_doctor(TAG, currentMonthSettModel.getTag(), new CustomerJsonCallBack<MyAccountModel_doctor>() {
            @Override
            public void onRequestError(MyAccountModel_doctor returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(MyAccountModel_doctor returnData) {
                hideWaitDialog();
                MyAccountModel_doctor.DataBean data = returnData.getData();
                if (data!=null){
                    mTvAccount.setText("(账号："+data.getDr_code()+")");
                    mTvBalance.setText(EmptyUtils.strEmpty(data.getAccountBalance()));
                    mTvOpenNum.setText("开具在线处方："+data.getOnlinePrescriptionCount()+"单");
                    mTvOpenSinglePrice.setText("单价（元）："+data.getOnlinePrescriptionPrice());
                    mTvOpenTotalPrice.setText("处方收入（元）："+data.getOnlinePrescriptionAmount());
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (currentMonthSettModel != null) {
            currentMonthSettModel = null;
        }

        if (currentView != null) {
            currentView = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, MyAccountActivity_ZhyfDoctor.class);
        context.startActivity(intent);
    }
}
