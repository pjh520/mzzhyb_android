package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.autoTextView.AutofitTextView;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.MyAccountModel_mana;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：我的账户--商户-管理端
 * ================================================
 */
public class MyAccountActivity_ZhyfMana extends BaseActivity {
    private TextView mTvBalance;
    private TextView mTvAccount;
    private TextView mTvRecharge;
    private TextView mTvWithdrawal;
    private AutofitTextView mAtvTotalPayment;
    private AutofitTextView mAtvMedicalTotalPayment;
    private AutofitTextView mAtvTotalEnterpriseFund,mAtvOnlineTotalPayment;

    private MyAccountModel_mana.DataBean pagerData;

    @Override
    public int getLayoutId() {
        return R.layout.activity_my_account_zhyfmana;
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(false, 0.2f)
                .titleBar(mTitleBar);
        mImmersionBar.init();
    }

    @Override
    public void initView() {
        super.initView();
        mTvBalance = findViewById(R.id.tv_balance);
        mTvAccount = findViewById(R.id.tv_account);
        mTvRecharge = findViewById(R.id.tv_recharge);
        mTvWithdrawal = findViewById(R.id.tv_withdrawal);
        mAtvTotalPayment = findViewById(R.id.atv_total_payment);
        mAtvMedicalTotalPayment = findViewById(R.id.atv_medical_total_payment);
        mAtvTotalEnterpriseFund= findViewById(R.id.atv_total_enterprise_fund);
        mAtvOnlineTotalPayment = findViewById(R.id.atv_online_total_payment);
    }

    @Override
    public void initData() {
        super.initData();
        mTitleBar.getRightView().setText("保证金");
        mTitleBar.getRightView().setEnabled(false);
        mTitleBar.getRightView().setTextColor(getResources().getColor(R.color.txt_color_999));
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvRecharge.setOnClickListener(this);
        mTvWithdrawal.setOnClickListener(this);
    }

    //获取页面数据
    private void getPagerData() {
        MyAccountModel_mana.sendMyAccountRequest_mana(TAG, new CustomerJsonCallBack<MyAccountModel_mana>() {
            @Override
            public void onRequestError(MyAccountModel_mana returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(MyAccountModel_mana returnData) {
                hideWaitDialog();
                setPagerData(returnData.getData());
            }
        });
    }

    //设置页面信息
    private void setPagerData(MyAccountModel_mana.DataBean data) {
        pagerData=data;
        if (data != null) {
            mTvBalance.setText(EmptyUtils.strEmpty(data.getAccountBalance()));
            mTvAccount.setText("账号:"+data.getUserName());
            mAtvTotalPayment.setText(EmptyUtils.strEmpty(data.getTotalAccumulatedIncome()));
            mAtvMedicalTotalPayment.setText(EmptyUtils.strEmpty(data.getTotalMedicareIncome()));
            mAtvOnlineTotalPayment.setText(EmptyUtils.strEmpty(data.getTotalOnlineIncome()));
            mAtvTotalEnterpriseFund.setText(EmptyUtils.strEmpty(data.getTotalEnterpriseFundAmount()));

            if ("1".equals(data.getIsDepositClick())){//充值按钮可点击
                mTitleBar.getRightView().setEnabled(true);
                mTitleBar.getRightView().setTextColor(getResources().getColor(R.color.white));
            }else {//充值按钮不可点击
                mTitleBar.getRightView().setEnabled(false);
                mTitleBar.getRightView().setTextColor(getResources().getColor(R.color.txt_color_999));
            }

            if ("2".equals(MechanismInfoUtils.getEntryMode())) {//管理服务模式
                if ("1".equals(data.getIsClick())){//充值按钮可点击
                    mTvRecharge.setEnabled(true);
                    mTvRecharge.setTextColor(getResources().getColor(R.color.color_4870e0));
                }else {//充值按钮不可点击
                    mTvRecharge.setEnabled(false);
                    mTvRecharge.setTextColor(getResources().getColor(R.color.txt_color_999));
                }
            }
        }
    }

    @Override
    public void rightTitleViewClick() {
        if (null==pagerData){
            showShortToast("数据返回有误，请重新进入页面");
            return;
        }
        BondActivity.newIntance(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_recharge://充值
                if (EmptyUtils.isEmpty(mTvAccount.getText().toString())){
                    showShortToast("数据返回有误，请重新进入页面");
                    return;
                }
                if ("1".equals(MechanismInfoUtils.getEntryMode())) {//固定抽佣模式
                    RechargeActivity_mercenary.newIntance(MyAccountActivity_ZhyfMana.this,pagerData.getUserName());
                } else if ("2".equals(MechanismInfoUtils.getEntryMode())){//管理服务模式
                    RechargeActivity_manage.newIntance(MyAccountActivity_ZhyfMana.this,pagerData.getUserName(), "1");
                }
                break;
            case R.id.tv_withdrawal://提现
                if (EmptyUtils.isEmpty(mTvBalance.getText().toString())){
                    showShortToast("数据返回有误，请重新进入页面");
                    return;
                }
                ApplyWithdrawalActivity.newIntance(MyAccountActivity_ZhyfMana.this,mTvBalance.getText().toString());
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPagerData();
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, MyAccountActivity_ZhyfMana.class);
        context.startActivity(intent);
    }
}
