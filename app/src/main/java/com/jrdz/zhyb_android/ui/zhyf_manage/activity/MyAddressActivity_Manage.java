package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-11
 * 描    述：我的收货地址
 * ================================================
 */
public class MyAddressActivity_Manage extends BaseActivity {
    private LinearLayout mLlUpdateAddress;

    @Override
    public int getLayoutId() {
        return R.layout.activity_myaddress_manage;
    }

    @Override
    public void initView() {
        super.initView();
        mLlUpdateAddress = findViewById(R.id.ll_update_address);
    }

    @Override
    public void initData() {
        super.initData();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mLlUpdateAddress.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.ll_update_address://编辑我的收货地址
                Intent intent = new Intent(MyAddressActivity_Manage.this, UpdateAddressActivtiy_manage.class);
                startActivityForResult(intent, new BaseActivity.OnActivityCallback() {
                    @Override
                    public void onActivityResult(int resultCode, @Nullable Intent data) {
                        if (resultCode == -1) {
                            // TODO: 2022-10-11 修改页面的收货地址信息

                            //TODO:当修改订单成功之后 前端需要通知我的地址页面修改地址

                        }
                    }
                });
                break;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, MyAddressActivity_Manage.class);
        context.startActivity(intent);
    }
}
