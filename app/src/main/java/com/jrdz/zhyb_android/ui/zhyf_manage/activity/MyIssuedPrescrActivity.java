package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.wheel.TimeWheelUtils;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.MyIussuedPrescrTabsAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.fragment.MyIssuedPrescrFragment;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.MyIssuedPrescrCountModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.MyIussedTabModel;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-26
 * 描    述：我开具的处方页面
 * ================================================
 */
public class MyIssuedPrescrActivity extends BaseActivity {
    private ShapeLinearLayout mSllStartTime;
    private TextView mTvStartTime;
    private ShapeLinearLayout mSllEndTime;
    private TextView mTvEndTime;
    private ShapeTextView mTvSearch;
    private SlidingTabLayout mStbPrescr;
    private ViewPager mVpPrescr;

    private TimeWheelUtils timeWheelUtils;
    private MyIussuedPrescrTabsAdapter baseViewPagerAndTabsAdapter_new;

    @Override
    public int getLayoutId() {
        return R.layout.activity_my_issued_prescr;
    }

    @Override
    public void initView() {
        super.initView();
        mSllStartTime = findViewById(R.id.sll_start_time);
        mTvStartTime = findViewById(R.id.tv_start_time);
        mSllEndTime = findViewById(R.id.sll_end_time);
        mTvEndTime = findViewById(R.id.tv_end_time);
        mTvSearch = findViewById(R.id.tv_search);
        mStbPrescr = findViewById(R.id.stb_prescr);
        mVpPrescr = findViewById(R.id.vp_prescr);
    }

    @Override
    public void initData() {
        super.initData();
        timeWheelUtils = new TimeWheelUtils();
        timeWheelUtils.isShowDay(true, true, true, true, true, true);

        showWaitDialog();
        getTabData();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mSllStartTime.setOnClickListener(this);
        mSllEndTime.setOnClickListener(this);
        mTvSearch.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.sll_start_time://开始日期
                timeWheelUtils.showTimeWheel(MyIssuedPrescrActivity.this, "开始日期", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mTvStartTime.setText(EmptyUtils.strEmpty(dateInfo));
                    }
                });
                break;
            case R.id.sll_end_time://结束日期
                timeWheelUtils.showTimeWheel(MyIssuedPrescrActivity.this, "结束日期", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mTvEndTime.setText(EmptyUtils.strEmpty(dateInfo));
                    }
                });
                break;
            case R.id.tv_search://搜索
                onSearch();
                break;
        }
    }

    //判断时间
    private void onSearch() {
        if (EmptyUtils.isEmpty(mTvStartTime.getText().toString())) {
            showShortToast("请选择开始日期");
            return;
        }

        if (EmptyUtils.isEmpty(mTvEndTime.getText().toString())) {
            showShortToast("请选择结束日期");
            return;
        }

        int dis=new BigDecimal(mTvStartTime.getText().toString().replaceAll("-", "")).subtract(
                new BigDecimal(mTvEndTime.getText().toString().replaceAll("-", ""))).intValue();

        if (dis>0){
            showShortToast("开始日期不能大于结束日期");
            return;
        }

        getTabData();
        MsgBus.sendMyIssuedPrescrRefresh().post("1");
    }

    //获取标签数据
    public void getTabData() {
        MyIssuedPrescrCountModel.sendDoctorElectronicPrescriptionCountRequest(TAG, getBeginDate(), getEndDate(), new CustomerJsonCallBack<MyIssuedPrescrCountModel>() {
            @Override
            public void onRequestError(MyIssuedPrescrCountModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(MyIssuedPrescrCountModel returnData) {
                hideWaitDialog();

                MyIssuedPrescrCountModel.DataBean myIssuedPrescrModel = returnData.getData();
                ArrayList<MyIussedTabModel> myIussedTabModels=new ArrayList<>();
                myIussedTabModels.add(new MyIussedTabModel("1", "店铺处方",myIssuedPrescrModel.getTotalItems1()));
                myIussedTabModels.add(new MyIussedTabModel("2", "在线处方", myIssuedPrescrModel.getTotalItems2()));

                initTablayout(myIussedTabModels);
            }
        });
    }

    //初始化tablayout 跟 viewpager
    private void initTablayout(ArrayList<MyIussedTabModel> myIussedTabModels) {
        if (baseViewPagerAndTabsAdapter_new==null){
            baseViewPagerAndTabsAdapter_new = new MyIussuedPrescrTabsAdapter(getSupportFragmentManager()) {
                @Override
                public Fragment getCustomeItem(String id,int position) {
                    switch (id) {
                        case "1"://店铺处方
                            return MyIssuedPrescrFragment.newIntance(id);
                        case "2"://在线处方
                            return MyIssuedPrescrFragment.newIntance(id);
                        default:
                            return MyIssuedPrescrFragment.newIntance(id);
                    }
                }
            };

            baseViewPagerAndTabsAdapter_new.setData(myIussedTabModels);

            mVpPrescr.setAdapter(baseViewPagerAndTabsAdapter_new);
            mStbPrescr.setViewPager(mVpPrescr);
        }else {
            baseViewPagerAndTabsAdapter_new.setData(myIussedTabModels);
            mStbPrescr.notifyDataSetChanged();
        }
    }

    public String getBeginDate(){
        return mTvStartTime.getText().toString();
    }

    public String getEndDate(){
        return mTvEndTime.getText().toString();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timeWheelUtils != null) {
            timeWheelUtils.dissTimeWheel();
            timeWheelUtils = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, MyIssuedPrescrActivity.class);
        context.startActivity(intent);
    }


}
