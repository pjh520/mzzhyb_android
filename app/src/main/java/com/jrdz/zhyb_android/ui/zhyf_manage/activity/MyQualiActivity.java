package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.MyQualiModel;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;

import java.math.BigDecimal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：我的资质 页面
 * ================================================
 */
public class MyQualiActivity extends BaseActivity implements CompressUploadSinglePicUtils_zhyf.IChoosePic {
    public final String TAG_QUALI_PIC_01 = "1";//营业执照
    public final String TAG_QUALI_PIC_02 = "2";//药品经营许可证
    public final String TAG_QUALI_PIC_03 = "3";//医疗器械经营许可证
    public final String TAG_QUALI_PIC_04 = "4";//GS认证证书

    private ShapeFrameLayout mSflHorizontalPic01;
    private LinearLayout mLlAddHorizontalPic01;
    private FrameLayout mFlHorizontalPic01;
    private ImageView mIvHorizontalPic01;
    private ImageView mIvDelete01;
    private ShapeFrameLayout mSflHorizontalPic02;
    private LinearLayout mLlAddHorizontalPic02;
    private FrameLayout mFlHorizontalPic02;
    private ImageView mIvHorizontalPic02;
    private ImageView mIvDelete02;
    private ShapeFrameLayout mSflHorizontalPic03;
    private LinearLayout mLlAddHorizontalPic03;
    private FrameLayout mFlHorizontalPic03;
    private ImageView mIvHorizontalPic03;
    private ImageView mIvDelete03;
    private ShapeFrameLayout mSflHorizontalPic04;
    private LinearLayout mLlAddHorizontalPic04;
    private FrameLayout mFlHorizontalPic04;
    private ImageView mIvHorizontalPic04;
    private ImageView mIvDelete04;

    private CompressUploadSinglePicUtils_zhyf compressUploadSinglePicUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_my_quali;
    }

    @Override
    public void initView() {
        super.initView();
        mSflHorizontalPic01 = findViewById(R.id.sfl_horizontal_pic01);
        mLlAddHorizontalPic01 = findViewById(R.id.ll_add_horizontal_pic01);
        mFlHorizontalPic01 = findViewById(R.id.fl_horizontal_pic01);
        mIvHorizontalPic01 = findViewById(R.id.iv_horizontal_pic01);
        mIvDelete01 = findViewById(R.id.iv_delete01);
        mSflHorizontalPic02 = findViewById(R.id.sfl_horizontal_pic02);
        mLlAddHorizontalPic02 = findViewById(R.id.ll_add_horizontal_pic02);
        mFlHorizontalPic02 = findViewById(R.id.fl_horizontal_pic02);
        mIvHorizontalPic02 = findViewById(R.id.iv_horizontal_pic02);
        mIvDelete02 = findViewById(R.id.iv_delete02);
        mSflHorizontalPic03 = findViewById(R.id.sfl_horizontal_pic03);
        mLlAddHorizontalPic03 = findViewById(R.id.ll_add_horizontal_pic03);
        mFlHorizontalPic03 = findViewById(R.id.fl_horizontal_pic03);
        mIvHorizontalPic03 = findViewById(R.id.iv_horizontal_pic03);
        mIvDelete03 = findViewById(R.id.iv_delete03);
        mSflHorizontalPic04 = findViewById(R.id.sfl_horizontal_pic04);
        mLlAddHorizontalPic04 = findViewById(R.id.ll_add_horizontal_pic04);
        mFlHorizontalPic04 = findViewById(R.id.fl_horizontal_pic04);
        mIvHorizontalPic04 = findViewById(R.id.iv_horizontal_pic04);
        mIvDelete04 = findViewById(R.id.iv_delete04);
    }

    @Override
    public void initData() {
        super.initData();
        setRightTitleView("保存");
        mTitleBar.getRightView().setTextColor(getResources().getColor(R.color.color_4870e0));

        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);

        showWaitDialog();
        getPagerData();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mLlAddHorizontalPic01.setOnClickListener(this);
        mIvDelete01.setOnClickListener(this);
        mLlAddHorizontalPic02.setOnClickListener(this);
        mIvDelete02.setOnClickListener(this);
        mLlAddHorizontalPic03.setOnClickListener(this);
        mIvDelete03.setOnClickListener(this);
        mLlAddHorizontalPic04.setOnClickListener(this);
        mIvDelete04.setOnClickListener(this);

        mIvHorizontalPic01.setOnClickListener(this);
        mIvHorizontalPic02.setOnClickListener(this);
        mIvHorizontalPic03.setOnClickListener(this);
        mIvHorizontalPic04.setOnClickListener(this);
    }

    //获取我的资质
    private void getPagerData() {
        MyQualiModel.sendMyQualiRequest(TAG, new CustomerJsonCallBack<MyQualiModel>() {
            @Override
            public void onRequestError(MyQualiModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(MyQualiModel returnData) {
                hideWaitDialog();
                MyQualiModel.DataBean data = returnData.getData();
                if (data != null) {
                    setPagerData(data);
                }
            }
        });
    }

    //设置页面数据
    private void setPagerData(MyQualiModel.DataBean data) {
        //营业执照
        if (!EmptyUtils.isEmpty(data.getBusinessLicenseAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, data.getBusinessLicenseAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectQualiPic01Success(data.getBusinessLicenseAccessoryId(), data.getBusinessLicenseAccessoryUrl(), width, height);
                }
            });
        }

        //药品经营许可证
        if (!EmptyUtils.isEmpty(data.getDrugBLAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, data.getBusinessLicenseAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectQualiPic02Success(data.getDrugBLAccessoryId(), data.getDrugBLAccessoryUrl(), width, height);
                }
            });
        }

        //医疗器械经营许可证
        if (!EmptyUtils.isEmpty(data.getMedicalDeviceBLAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, data.getBusinessLicenseAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectQualiPic03Success(data.getMedicalDeviceBLAccessoryId(), data.getMedicalDeviceBLAccessoryUrl(), width, height);
                }
            });
        }

        //GS认证证书
        if (!EmptyUtils.isEmpty(data.getGSCertificateAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, data.getBusinessLicenseAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectQualiPic04Success(data.getGSCertificateAccessoryId(), data.getGSCertificateAccessoryUrl(), width, height);
                }
            });
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.ll_add_horizontal_pic01://营业执照
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_QUALI_PIC_01, CompressUploadSinglePicUtils_zhyf.PIC_APPLY_CERT_TAG);
                break;
            case R.id.iv_delete01://营业执照 删除
                mLlAddHorizontalPic01.setVisibility(View.VISIBLE);
                mLlAddHorizontalPic01.setEnabled(true);

                mIvHorizontalPic01.setTag(R.id.tag_1, "");
                mIvHorizontalPic01.setTag(R.id.tag_2, "");
                mFlHorizontalPic01.setVisibility(View.GONE);
                break;
            case R.id.ll_add_horizontal_pic02://药品经营许可证
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_QUALI_PIC_02, CompressUploadSinglePicUtils_zhyf.PIC_QUALI_02_TAG);
                break;
            case R.id.iv_delete02://药品经营许可证 删除
                mLlAddHorizontalPic02.setVisibility(View.VISIBLE);
                mLlAddHorizontalPic02.setEnabled(true);

                mIvHorizontalPic02.setTag(R.id.tag_1, "");
                mIvHorizontalPic02.setTag(R.id.tag_2, "");
                mFlHorizontalPic02.setVisibility(View.GONE);
                break;
            case R.id.ll_add_horizontal_pic03://医疗器械经营许可证
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_QUALI_PIC_03, CompressUploadSinglePicUtils_zhyf.PIC_QUALI_03_TAG);
                break;
            case R.id.iv_delete03://医疗器械经营许可证 删除
                mLlAddHorizontalPic03.setVisibility(View.VISIBLE);
                mLlAddHorizontalPic03.setEnabled(true);

                mIvHorizontalPic03.setTag(R.id.tag_1, "");
                mIvHorizontalPic03.setTag(R.id.tag_2, "");
                mFlHorizontalPic03.setVisibility(View.GONE);
                break;
            case R.id.ll_add_horizontal_pic04://GS认证证书
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_QUALI_PIC_04, CompressUploadSinglePicUtils_zhyf.PIC_QUALI_04_TAG);
                break;
            case R.id.iv_delete04://GS认证证书 删除
                mLlAddHorizontalPic04.setVisibility(View.VISIBLE);
                mLlAddHorizontalPic04.setEnabled(true);

                mIvHorizontalPic04.setTag(R.id.tag_1, "");
                mIvHorizontalPic04.setTag(R.id.tag_2, "");
                mFlHorizontalPic04.setVisibility(View.GONE);
                break;
            case R.id.iv_horizontal_pic01://营业执照
                showBigPic(mIvHorizontalPic01,(String)mIvHorizontalPic01.getTag(R.id.tag_2));
                break;
            case R.id.iv_horizontal_pic02://药品经营许可证
                showBigPic(mIvHorizontalPic02,(String)mIvHorizontalPic02.getTag(R.id.tag_2));
                break;
            case R.id.iv_horizontal_pic03://医疗器械经营许可证
                showBigPic(mIvHorizontalPic03,(String)mIvHorizontalPic03.getTag(R.id.tag_2));
                break;
            case R.id.iv_horizontal_pic04://GS认证证书
                showBigPic(mIvHorizontalPic04,(String)mIvHorizontalPic04.getTag(R.id.tag_2));
                break;
        }
    }

    @Override
    public void rightTitleViewClick() {
        if (null == mIvHorizontalPic01.getTag(R.id.tag_1) || EmptyUtils.isEmpty(String.valueOf(mIvHorizontalPic01.getTag(R.id.tag_1)))) {
            showShortToast("请上传营业执照");
            return;
        }
        if (null == mIvHorizontalPic02.getTag(R.id.tag_1) || EmptyUtils.isEmpty(String.valueOf(mIvHorizontalPic02.getTag(R.id.tag_1)))) {
            showShortToast("请上传药品经营许可证");
            return;
        }

        showWaitDialog();
        BaseModel.sendUpdateStoreCredentialsRequest(TAG, String.valueOf(mIvHorizontalPic01.getTag(R.id.tag_1)), String.valueOf(mIvHorizontalPic02.getTag(R.id.tag_1)),
                mIvHorizontalPic03.getTag(R.id.tag_1), mIvHorizontalPic04.getTag(R.id.tag_1), new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();

                        showShortToast("保存成功");
                    }
                });
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String mTag, String accessoryId, String url, int width, int height) {
        switch (mTag) {
            case TAG_QUALI_PIC_01://营业执照
                selectQualiPic01Success(accessoryId, url, width, height);
                break;
            case TAG_QUALI_PIC_02://药品经营许可证
                selectQualiPic02Success(accessoryId, url, width, height);
                break;
            case TAG_QUALI_PIC_03://医疗器械经营许可证
                selectQualiPic03Success(accessoryId, url, width, height);
                break;
            case TAG_QUALI_PIC_04://GS认证证书
                selectQualiPic04Success(accessoryId, url, width, height);
                break;
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }

    //营业执照
    private void selectQualiPic01Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic01.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvHorizontalPic01.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic01);

            mLlAddHorizontalPic01.setVisibility(View.GONE);
            mLlAddHorizontalPic01.setEnabled(false);

            mIvHorizontalPic01.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic01.setTag(R.id.tag_2, url);
            mFlHorizontalPic01.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //药品经营许可证
    private void selectQualiPic02Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic02.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvHorizontalPic02.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic02);

            mLlAddHorizontalPic02.setVisibility(View.GONE);
            mLlAddHorizontalPic02.setEnabled(false);

            mIvHorizontalPic02.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic02.setTag(R.id.tag_2, url);
            mFlHorizontalPic02.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //医疗器械经营许可证
    private void selectQualiPic03Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic03.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvHorizontalPic03.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic03);

            mLlAddHorizontalPic03.setVisibility(View.GONE);
            mLlAddHorizontalPic03.setEnabled(false);

            mIvHorizontalPic03.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic03.setTag(R.id.tag_2, url);
            mFlHorizontalPic03.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //GS认证证书
    private void selectQualiPic04Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic04.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvHorizontalPic04.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic04);

            mLlAddHorizontalPic04.setVisibility(View.GONE);
            mLlAddHorizontalPic04.setEnabled(false);

            mIvHorizontalPic04.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic04.setTag(R.id.tag_2, url);
            mFlHorizontalPic04.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //--------------------------选择图片----------------------------------------------------
    //展示大图
    private void showBigPic(ImageView imageView,String imageUrl) {
        OpenImage.with(MyQualiActivity.this)
                //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                .setClickImageView(imageView)
                //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                //RecyclerView的数据
                .setImageUrl(imageUrl, MediaType.IMAGE)
                //点击的ImageView所在数据的位置
                .setClickPosition(0)
                //开始展示大图
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, MyQualiActivity.class);
        context.startActivity(intent);
    }
}
