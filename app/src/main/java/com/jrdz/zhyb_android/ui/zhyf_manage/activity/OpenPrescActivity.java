package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSON;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.DecimalFormatUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ImageUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.RatioImageView;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.JustifyContent;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeEditText;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.database.UserInfoModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.mine.model.FileUploadModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.OpenprescRelationDiseAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.OrderDetailModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.OrderListModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.QueryOnlineInquiryModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TagTextBean;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.CertPicAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.model.VisitorDiseaseHistoryModel;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils;
import com.jrdz.zhyb_android.widget.MyFlexboxLayoutManager;
import com.jrdz.zhyb_android.widget.TagTextView;
import com.jrdz.zhyb_android.widget.pop.HandWritePop;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-26
 * 描    述：开处方页面
 * ================================================
 */
public class OpenPrescActivity extends BaseActivity implements HandWritePop.IOptionListener {
    protected ShapeLinearLayout mSllProductInfo;
    private ImageView mIvShopHead;
    private TextView mTvShopName;
    private LinearLayout mLlOrderListContain;
    private TextView mTvPersonInfo;
    private ShapeTextView mStvIsOneself;
    private TextView mTvCertNo;
    private CustomeRecyclerView mCrvRelationDisetype;
    private CustomeRecyclerView mCrvRelationDise;
    private TextView mTvJudge01;
    private TextView mTvJudge02;
    private TextView mTvJudge03;
    private TextView mTvSymptom;
    private CustomeRecyclerView mCrlCertPic;
    private TextView mTvSync;
    private TextView mTvPersonName;
    private TextView mTvPersonSex;
    private TextView mTvPersonAge;
    private TextView mTvPersonPhone;
    private LinearLayout mLlDragContain;
    private TextView mTvPrice;
    private ShapeEditText mEtDiagResult;
    private ShapeLinearLayout mSllSignature;
    private TextView mTvSignatureStatus;
    private ShapeTextView mTvSure;

    private String orderNo,storeAccessoryUrl,storeName;
    private ArrayList<OrderDetailModel.DataBean.OrderGoodsBean> orderGoods;
    private QueryOnlineInquiryModel.DataBean pagerData;
    private OpenprescRelationDiseAdapter relationDiseTypeAdapter;
    private OpenprescRelationDiseAdapter relationDiseAdapter;
    private CertPicAdapter certPicAdapter;
    private HandWritePop handWritePop;
    String imagPath = BaseGlobal.getPicShotDir() + "doctor_signature.png";//存储签名的路径
    private UserInfoModel userInfo;
    List<TagTextBean> tags = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_openpresc;
    }

    @Override
    public void initView() {
        super.initView();
        mSllProductInfo = findViewById(R.id.sll_product_info);
        mIvShopHead = findViewById(R.id.iv_shop_head);
        mTvShopName = findViewById(R.id.tv_shop_name);
        mLlOrderListContain = findViewById(R.id.ll_order_list_contain);
        mTvPersonInfo = findViewById(R.id.tv_person_info);
        mStvIsOneself = findViewById(R.id.stv_is_oneself);
        mTvCertNo = findViewById(R.id.tv_cert_no);
        mCrvRelationDisetype = findViewById(R.id.crv_relation_disetype);
        mCrvRelationDise = findViewById(R.id.crv_relation_dise);
        mTvJudge01 = findViewById(R.id.tv_judge_01);
        mTvJudge02 = findViewById(R.id.tv_judge_02);
        mTvJudge03 = findViewById(R.id.tv_judge_03);
        mTvSymptom = findViewById(R.id.tv_symptom);
        mCrlCertPic = findViewById(R.id.crl_cert_pic);

        mTvSync = findViewById(R.id.tv_sync);
        mTvPersonName = findViewById(R.id.tv_person_name);
        mTvPersonSex = findViewById(R.id.tv_person_sex);
        mTvPersonAge = findViewById(R.id.tv_person_age);
        mTvPersonPhone = findViewById(R.id.tv_person_phone);
        mLlDragContain= findViewById(R.id.ll_drag_contain);
        mTvPrice = findViewById(R.id.tv_price);
        mEtDiagResult = findViewById(R.id.et_diag_result);
        mSllSignature = findViewById(R.id.sll_signature);
        mTvSignatureStatus = findViewById(R.id.tv_signature_status);
        mTvSure = findViewById(R.id.tv_sure);
    }

    @Override
    public void initData() {
        Intent intent=getIntent();
        orderNo = intent.getStringExtra("orderNo");
        storeAccessoryUrl=intent.getStringExtra("storeAccessoryUrl");
        storeName=intent.getStringExtra("storeName");
        orderGoods = intent.getParcelableArrayListExtra("orderGoods");

        super.initData();
        userInfo = LoginUtils.getUserinfo();
        GlideUtils.loadImg(storeAccessoryUrl,mIvShopHead,R.drawable.ic_placeholder_bg,new CircleCrop());
        mTvShopName.setText(EmptyUtils.strEmpty(storeName));
        mLlOrderListContain.removeAllViews();
        if (orderGoods!=null){
            for (int i = 0, size = orderGoods.size(); i < size; i++) {
                OrderDetailModel.DataBean.OrderGoodsBean orderGood = orderGoods.get(i);
                //设置商品信息
                View view = LayoutInflater.from(this).inflate(R.layout.layout_orderlist_product_item, mLlOrderListContain, false);
                ImageView ivPic = view.findViewById(R.id.iv_pic);
                ShapeTextView stvOtc = view.findViewById(R.id.stv_otc);
                ShapeTextView stvEnterpriseTag= view.findViewById(R.id.stv_enterprise_tag);
                TagTextView tvTitle = view.findViewById(R.id.tv_title);
                TextView tvEstimatePrice = view.findViewById(R.id.tv_estimate_price);
                TextView tvRealPrice = view.findViewById(R.id.tv_real_price);
                View lineItem = view.findViewById(R.id.line_item);

                GlideUtils.loadImg(orderGood.getBannerAccessoryUrl1(), ivPic, R.drawable.ic_placeholder_bg,
                        new RoundedCornersTransformation(getResources().getDimensionPixelSize(R.dimen.dp_16), 0));
                //判断是否是处方药
                if ("1".equals(orderGood.getItemType())) {
                    stvOtc.setText("OTC");
                    stvOtc.setVisibility(View.VISIBLE);
                } else if ("2".equals(orderGood.getItemType())) {
                    stvOtc.setText("处方药");
                    stvOtc.setVisibility(View.VISIBLE);
                } else {
                    stvOtc.setVisibility(View.GONE);
                }
                //判断是否是平台药 若是 需要展示医保
                tags.clear();
                if ("1".equals(orderGood.getIsPlatformDrug())){//是平台药
                    tags.add(new TagTextBean("医保",R.color.color_ee8734));
                    tvTitle.setContentAndTag(EmptyUtils.strEmpty(orderGood.getGoodsName()), tags);
                }else {
                    tvTitle.setContentAndTag(EmptyUtils.strEmpty(orderGood.getGoodsName()), tags);
                }

                //判断是否支持企业基金支付
                if ("1".equals(orderGood.getIsEnterpriseFundPay())){
                    stvEnterpriseTag.setVisibility(View.VISIBLE);
                }else {
                    stvEnterpriseTag.setVisibility(View.GONE);
                }
                tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(orderGood.getPrice())));
                tvRealPrice.setText("x" + orderGood.getGoodsNum());
                //2022-10-10 最后一个item的分割线需要隐藏
                if (i == size - 1) {
                    lineItem.setVisibility(View.GONE);
                } else {
                    lineItem.setVisibility(View.VISIBLE);
                }
                mLlOrderListContain.addView(view);
            }
        }

        mTvSignatureStatus.setText(EmptyUtils.isEmpty(userInfo.getAccessoryUrl1()) ? "未签" : "已签");
        //疾病史
        mCrvRelationDisetype.setHasFixedSize(true);
        mCrvRelationDisetype.setLayoutManager(getLayoutManager());
        relationDiseTypeAdapter = new OpenprescRelationDiseAdapter(2);
        mCrvRelationDisetype.setAdapter(relationDiseTypeAdapter);
        //已确诊的相关疾病
        mCrvRelationDise.setHasFixedSize(true);
        mCrvRelationDise.setLayoutManager(getLayoutManager());
        relationDiseAdapter = new OpenprescRelationDiseAdapter(1);
        mCrvRelationDise.setAdapter(relationDiseAdapter);
        //复诊凭证
        mCrlCertPic.setHasFixedSize(true);
        mCrlCertPic.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
        certPicAdapter = new CertPicAdapter();
        mCrlCertPic.setAdapter(certPicAdapter);

        showWaitDialog();
        getPagerData();
        updatePresStatus();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvSync.setOnClickListener(this);
        mSllSignature.setOnClickListener(this);
        mTvSure.setOnClickListener(this);

        certPicAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                RatioImageView ivCertPic=view.findViewById(R.id.iv_cert_pic);
                String itemData = certPicAdapter.getItem(i);
                //查看图片
                OpenImage.with(OpenPrescActivity.this)
                        //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                        .setClickImageView(ivCertPic)
                        //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                        .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                        //RecyclerView的数据
                        .setImageUrl(itemData, MediaType.IMAGE)
                        //点击的ImageView所在数据的位置
                        .setClickPosition(0)
                        //开始展示大图
                        .show();
            }
        });
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        MyFlexboxLayoutManager flexboxLayoutManager = new MyFlexboxLayoutManager(this);
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        flexboxLayoutManager.setFlexWrap(FlexWrap.WRAP);
        flexboxLayoutManager.setJustifyContent(JustifyContent.FLEX_START);

        return flexboxLayoutManager;
    }

    //获取页面数据
    private void getPagerData() {
        QueryOnlineInquiryModel.sendQueryOnlineInquiryRequest(TAG, orderNo, new CustomerJsonCallBack<QueryOnlineInquiryModel>() {
            @Override
            public void onRequestError(QueryOnlineInquiryModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(QueryOnlineInquiryModel returnData) {
                hideWaitDialog();
                if (isFinishing())return;
                pagerData = returnData.getData();
                if (pagerData != null) {
                    setPagerData();
                } else {
                    showShortToast("数据返回有误，请重新进入页面");
                }
            }
        });
    }

    //修改处方问诊状态
    private void updatePresStatus() {
        BaseModel.sendUpdatePresStatusRequest_mana(TAG, orderNo, LoginUtils.getUserId(), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {}
        });
    }

    //设置页面数据
    private void setPagerData() {
        String sex = "1".equals(pagerData.getSex()) ? "男" : "女";
        mTvPersonInfo.setText(StringUtils.encryptionName(pagerData.getPsn_name()) + " " + sex + " " + pagerData.getAge() + "岁");
        mStvIsOneself.setText("本人");
        mTvCertNo.setText(StringUtils.encryptionIDCard(pagerData.getMdtrt_cert_no()));
        //疾病史的数据
        ArrayList<String> relationTytpDises = new ArrayList<>();
        String AssociatedDiseases1 = pagerData.getAssociatedDiseases1();
        if (!EmptyUtils.isEmpty(AssociatedDiseases1)) {
            VisitorDiseaseHistoryModel visitorDiseaseHistoryModel = JSON.parseObject(AssociatedDiseases1, VisitorDiseaseHistoryModel.class);
            //过往病史
            String selectPastMedical = visitorDiseaseHistoryModel.getSelectPastMedical();
            String etPastMedical = visitorDiseaseHistoryModel.getEtPastMedical();
            if ("1".equals(visitorDiseaseHistoryModel.getIsSelectPastMedical())) {
                String pastMedicalShowText=getShowText(selectPastMedical,etPastMedical);
                relationTytpDises.add(EmptyUtils.isEmpty(pastMedicalShowText)?"过往病史":"过往病史:" + pastMedicalShowText);
            }
            //过敏史弹框
            String selectAllergy = visitorDiseaseHistoryModel.getSelectAllergy();
            String etAllergy = visitorDiseaseHistoryModel.getEtAllergy();
            if ("1".equals(visitorDiseaseHistoryModel.getIsSelectAllergy())) {
                String allergyShowText=getShowText(selectAllergy,etAllergy);
                relationTytpDises.add(EmptyUtils.isEmpty(allergyShowText)?"过敏史":"过敏史:" + allergyShowText);
            }
            //家族病史弹框
            String selectFamily = visitorDiseaseHistoryModel.getSelectFamily();
            String etFamily = visitorDiseaseHistoryModel.getEtFamily();
            if ("1".equals(visitorDiseaseHistoryModel.getIsSelectFamily())) {
                String familyShowText=getShowText(selectFamily,etFamily);
                relationTytpDises.add(EmptyUtils.isEmpty(familyShowText)?"家族病史":"家族病史:" + familyShowText);
            }
            //肝功能异常
            if ("1".equals(visitorDiseaseHistoryModel.getIsSelectLiver())) {
                relationTytpDises.add("肝功能异常");
            }
            //肾功能异常
            if ("1".equals(visitorDiseaseHistoryModel.getIsSelectKidney())) {
                relationTytpDises.add("肾功能异常");
            }
            //妊娠哺乳期
            if ("1".equals(visitorDiseaseHistoryModel.getIsSelectPregnancyLactation())) {
                relationTytpDises.add("妊娠哺乳期");
            }
        }

        relationDiseTypeAdapter.setNewData(relationTytpDises);
        //已确诊的相关疾病的数据
        relationDiseAdapter.setNewData(Arrays.asList(pagerData.getAssociatedDiseases().split("∞#")));

        //您是否服用过该药品且无相关禁忌症
        mTvJudge01.setText("1".equals(pagerData.getIsTake()) ? "是" : "否");
        //您是否服用过该药品且无相关禁忌症
        mTvJudge02.setText("1".equals(pagerData.getIsAllergy()) ? "是" : "否");
        //您是否服用过该药品且无相关禁忌症
        mTvJudge03.setText("1".equals(pagerData.getIsUntowardReaction()) ? "是" : "否");
        //描述确诊疾病症状:
        mTvSymptom.setText("描述确诊疾病症状:" + pagerData.getSymptom());

        //设置复诊凭证图片
        ArrayList<String> certPicDatas = new ArrayList<>();
        if (!EmptyUtils.isEmpty(pagerData.getMedicalRecordAccessoryUrl1())) {
            certPicDatas.add(pagerData.getMedicalRecordAccessoryUrl1());
        }
        if (!EmptyUtils.isEmpty(pagerData.getMedicalRecordAccessoryUrl2())) {
            certPicDatas.add(pagerData.getMedicalRecordAccessoryUrl2());
        }
        if (!EmptyUtils.isEmpty(pagerData.getMedicalRecordAccessoryUrl3())) {
            certPicDatas.add(pagerData.getMedicalRecordAccessoryUrl3());
        }
        if (!EmptyUtils.isEmpty(pagerData.getMedicalRecordAccessoryUrl4())) {
            certPicDatas.add(pagerData.getMedicalRecordAccessoryUrl4());
        }
        if (!EmptyUtils.isEmpty(pagerData.getMedicalRecordAccessoryUrl5())) {
            certPicDatas.add(pagerData.getMedicalRecordAccessoryUrl5());
        }
        certPicAdapter.setNewData(certPicDatas);
    }

    protected String getShowText(String selectDatas, String etContent) {
        String showText="";
        if (EmptyUtils.isEmpty(selectDatas)) {
            showText = etContent;
        } else {
            if (EmptyUtils.isEmpty(etContent)) {
                showText = selectDatas;
            } else {
                showText = selectDatas+","+ etContent;
            }
        }

        return showText;
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_sync://同步患者信息
                mTvPersonName.setText("姓名:"+StringUtils.encryptionName(pagerData.getPsn_name()));
                mTvPersonSex.setText("1".equals(pagerData.getSex()) ? "性别:男" : "性别:女");
                mTvPersonAge.setText("年龄:"+pagerData.getAge() + "岁");
                mTvPersonPhone.setText("电话:"+pagerData.getUserName());

                if (orderGoods!=null){
                    mLlDragContain.removeAllViews();
                    String totalPrice = "0";
                    for (OrderDetailModel.DataBean.OrderGoodsBean orderGood : orderGoods) {
                        View view= LayoutInflater.from(OpenPrescActivity.this).inflate(R.layout.layout_openpresc_drag_item, mLlDragContain, false);
                        TextView tvDragName=view.findViewById(R.id.tv_drag_name);
                        TextView tvDragSpec=view.findViewById(R.id.tv_drag_spec);
                        TextView tvDragNum=view.findViewById(R.id.tv_drag_num);
                        TextView tvDragUsageDosage=view.findViewById(R.id.tv_drag_usage_dosage);
                        TextView tvRemark=view.findViewById(R.id.tv_remark);

                        tvDragName.setText("药品名称:"+orderGood.getGoodsName());
                        tvDragSpec.setText("规格:"+orderGood.getSpec());
                        tvDragNum.setText("数量:"+orderGood.getGoodsNum());
                        tvDragUsageDosage.setText("用法用量:"+orderGood.getUsageDosage());
                        tvRemark.setText("备注:"+orderGood.getRemark());
                        mLlDragContain.addView(view);

                        totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(orderGood.getPrice())
                                .multiply(new BigDecimal(String.valueOf(orderGood.getGoodsNum())))).toPlainString();
                    }

                    mTvPrice.setText("¥"+totalPrice);
                }
                break;
            case R.id.sll_signature://医师签名
                if (handWritePop == null) {
                    handWritePop = new HandWritePop(OpenPrescActivity.this, this);
                }

                handWritePop.showPopupWindow();
                break;
            case R.id.tv_sure://提交
                onSure();
                break;
        }
    }

    @Override
    public void commit(Bitmap mBitmap) {
        showWaitDialog("图片处理中");

        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                boolean isSaveSuccess = ImageUtils.save(mBitmap, imagPath, Bitmap.CompressFormat.PNG, true);
                if (isSaveSuccess) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            upSignPic();
                        }
                    });
                } else {
                    hideWaitDialog();
                    showShortToast("图片保存失败，请重新保存");
                }
            }
        });
    }

    //上传签名
    private void upSignPic() {
        FileUploadModel.sendFileUploadRequest(TAG, CompressUploadSinglePicUtils.PIC_DOCTOR_SIGN_TAG, new File(imagPath), new CustomerJsonCallBack<FileUploadModel>() {
            @Override
            public void onRequestError(FileUploadModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(FileUploadModel returnData) {
                hideWaitDialog();
                FileUploadModel.DataBean dataBean = returnData.getData();
                if (dataBean != null) {
                    showShortToast("上传成功");

                    mTvSignatureStatus.setText("已签");

                    if (userInfo != null) {
                        userInfo.setSubAccessoryId(dataBean.getAccessoryId());
                        userInfo.setAccessoryUrl1(dataBean.getAccessoryUrl());
                        userInfo.update(userInfo.getId());
                    }
                } else {
                    showShortToast("图片上传失败，请重新上传");
                }
            }
        });
    }

    //提交
    private void onSure() {
        if (EmptyUtils.isEmpty(mEtDiagResult.getText().toString())) {
            showShortToast("请填写诊断结果");
            return;
        }
        //判断是否签名
        if (EmptyUtils.isEmpty(userInfo.getAccessoryUrl1())) {
            showShortToast("请先签名");
            return;
        }
        // 2022-10-26 请求接口 开具处方
        showWaitDialog();
        BaseModel.sendAddElectronicPrescriptionRequest(TAG, orderNo,mEtDiagResult.getText().toString(), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();

                //开具处方成功
                setResult(RESULT_OK);
                SuccessActivity.newIntance(OpenPrescActivity.this,"提交成功!", "稍后可查看处方");
                goFinish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handWritePop != null) {
            handWritePop.onClean();
        }
    }
}

