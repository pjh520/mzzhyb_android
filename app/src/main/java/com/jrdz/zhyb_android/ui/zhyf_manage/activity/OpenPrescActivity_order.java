package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.view.View;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-26
 * 描    述：开处方页面--订单详情页面
 * ================================================
 */
public class OpenPrescActivity_order extends OpenPrescActivity{

    @Override
    public void initData() {
        super.initData();
        mSllProductInfo.setVisibility(View.GONE);
    }
}

