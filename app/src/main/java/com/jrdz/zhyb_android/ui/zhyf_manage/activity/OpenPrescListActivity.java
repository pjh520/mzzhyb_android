package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.BroadCastReceiveUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.switchButton.SwitchButton;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.MainActivity;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredMainActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.OpenPrescAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.OpenPrescModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.QuerydoctorIsOnlineModel;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.OrderDetailActivity_user;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.library.jpush_library.PushMessageReceiver;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-25
 * 描    述：开处方列表页面
 * ================================================
 */
public class OpenPrescListActivity extends BaseRecyclerViewActivity {
    private SwitchButton mSbOnline;
    private EditText mEtSearch;
    private ShapeTextView mStvSearch;

    //接受推送消息
    private BroadCastReceiveUtils mPushReceiver = new BroadCastReceiveUtils() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra("type");
            String notificationExtras = intent.getStringExtra("extras");

            switch (type){
                case "onNotifyMessageArrived"://通知到达
                    try {
                        JSONObject jsonObject = JSON.parseObject(notificationExtras);
                        String type_2 = jsonObject.getString("type");//(0不语音1语音）
                        if ("6".equals(type_2)){
                            showWaitDialog();
                            onRefresh(mRefreshLayout);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };


    @Override
    public int getLayoutId() {
        return R.layout.activity_openpresc_list;
    }

    @Override
    public void initView() {
        super.initView();
        mSbOnline = findViewById(R.id.sb_online);
        mEtSearch = findViewById(R.id.et_search);
        mStvSearch = findViewById(R.id.stv_search);
    }

    @Override
    public void initAdapter() {
        mAdapter=new OpenPrescAdapter();
        mAdapter.setHeaderAndEmpty(true);
    }

    @Override
    public void initData() {
        super.initData();
        BroadCastReceiveUtils.registerLocalReceiver(this, PushMessageReceiver.SEND_PUSH_SPEECH_SOUNDS, mPushReceiver);
        showWaitDialog();
        getDoctorIsOnlineStates();
        onRefresh(mRefreshLayout);
    }

    //获取医师在线状态
    private void getDoctorIsOnlineStates() {
        QuerydoctorIsOnlineModel.sendQuerydoctorIsOnlineRequest(TAG, LoginUtils.getUserId(), new CustomerJsonCallBack<QuerydoctorIsOnlineModel>() {
            @Override
            public void onRequestError(QuerydoctorIsOnlineModel returnData, String msg) {
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(QuerydoctorIsOnlineModel returnData) {
                QuerydoctorIsOnlineModel.DataBean pagerData = returnData.getData();
                if (pagerData!=null){
                    mSbOnline.setCheckedImmediatelyNoEvent("1".equals(pagerData.getIsOnline()) ? true : false);
                }else {
                    showTipDialog("数据返回有误，请重新进入页面");
                }
            }
        });
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mSbOnline.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                showWaitDialog();
                setOnline(b?"1":"0");
            }
        });
        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }


    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void getData() {
        super.getData();
        OpenPrescModel.sendOrderWithPrescriptionlistRequest(TAG, String.valueOf(mPageNum), "10",mEtSearch.getText().toString(), new CustomerJsonCallBack<OpenPrescModel>() {
            @Override
            public void onRequestError(OpenPrescModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(OpenPrescModel returnData) {
                hideRefreshView();
                List<OpenPrescModel.DataBean> datas = returnData.getData();
                if (mAdapter!=null&&datas != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(datas);

                        if (datas == null || datas.isEmpty()) {
                            mAdapter.isUseEmpty(true);
                        }
                    } else {
                        mAdapter.addData(datas);
                        mAdapter.loadMoreComplete();
                    }

                    if (datas.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    //设置医师是否上下线
    private void setOnline(String isOnline) {
        BaseModel.sendEditdoctorIsOnlineRequest(TAG, LoginUtils.getUserId(), isOnline, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
            }
        });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        OpenPrescModel.DataBean itemData = ((OpenPrescAdapter) adapter).getItem(position);
        Intent intent=new Intent(OpenPrescListActivity.this,OpenPrescActivity.class);
        intent.putExtra("orderNo", itemData.getOrderNo());
        intent.putExtra("storeAccessoryUrl", itemData.getStoreAccessoryUrl());
        intent.putExtra("storeName", itemData.getStoreName());
        intent.putParcelableArrayListExtra("orderGoods", itemData.getOrderGoods());

        intent.putParcelableArrayListExtra("orderGoods", itemData.getOrderGoods());
        startActivityForResult(intent, new OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode==RESULT_OK){
                    mAdapter.remove(position);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BroadCastReceiveUtils.unregisterLocalReceiver(this, mPushReceiver);
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, OpenPrescListActivity.class);
        context.startActivity(intent);
    }
}
