package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.frame.compiler.utils.BroadCastReceiveUtils;
import com.frame.compiler.utils.DateUtil;
import com.frame.compiler.utils.DecimalFormatUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxClipboardTool;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.widget.countDownTime.CustomCountDownTimer;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.layout.ShapeRelativeLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.OrderDetailModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TagTextBean;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.ElectPrescActivity2_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.ElectPrescActivity_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.OrderDetailActivity_user;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.widget.TagTextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-10
 * 描    述：智慧药房--管理端--订单详情页面
 * ================================================
 */
public class OrderDetailActivity extends BaseActivity {
    private TextView mTvStatus, mTvTime;
    private TextView mTvDeliveryType;
    private View mLine01;
    private ImageView mIvAddressSeize;
    private TextView mTvAddressInfo;
    private ShapeRelativeLayout mSrlPrescrInfo;
    private TextView mTvLookPrescr;
    private TextView mTvOpenPrescrDescribe;

    private TextView mTvOrderId;
    private TextView mTvOrderRefundStatus;
    private LinearLayout mLlOrderListContain;
    private TextView mTvTotalNum;
    private TextView mTvTotalPrice;
    private TextView mTvOrderId02;
    private TextView mTvOrderIdCopy;
    private LinearLayout mLlRefundNo;
    private TextView mTvRefundNo;
    private TextView mTvRefundNoCopy;
    private TextView mTvBusinessPhone;
    private TextView mTvBusinessWx;
    private TextView mTvBusinessWxCopy;
    private TextView mTvBuyerWx;
    private TextView mTvBuyerWxCopy;
    private TextView mTvBuyerMsg;
    private TextView mTvOrderCreateTime;
    private LinearLayout mLlPayTime;
    private TextView mTvOrderPayTime;
    private LinearLayout mLlDeliverGoodsTime;
    private TextView mTvDeliverGoodsTime;
    private LinearLayout mLlFinishTime;
    private TextView mTvFinishTime;
    private LinearLayout mLlCancleTime;
    private TextView mTvOrderCancleTime;

    private ShapeLinearLayout mSllPreSettl;
    private TextView mTvSettleListTitle;
    private TextView mTvPsnNo;
    private TextView mTvPsnName;
    private TextView mTvCertType;
    private TextView mTvCertNo;
    private TextView mTvInsutypeName;
    private TextView mTvFeedetlSn;
    private TextView mTvMedfeeSumamt;
    private TextView mTvFundPaySumamt;
    private TextView mTvAcctPay;
    private TextView mTvPsnCashPay;
    private TextView mTvMoney;

    private ShapeLinearLayout mSllSerialno;
    private LinearLayout mLlIptOtpNo;
    private TextView mTvIptOtpNo;
    private LinearLayout mLlRefundSerialno;
    private TextView mTvRefundSerialno;
    private LinearLayout mLlOrderTotalPrice;
    private TextView mTvOrderTotalPrice;
    private LinearLayout mLlFreight;
    private TextView mTvFreight;
    private LinearLayout mLlMedicalPay;
    private TextView mTvMedicalPay;
    private LinearLayout mLlMedicalPersonPay;
    private TextView mTvMedicalPersonPay;
    private LinearLayout mLlEnterprisePay;
    private TextView mTvEnterprisePay;
    private LinearLayout mLlPsnCashPay02;
    private TextView mTvPsnCashPay02;

    private ShapeLinearLayout mSllWaitpayDetail;
    private LinearLayout mLlOnlinePrice;
    private TextView mTvOnlinePrice;
    private LinearLayout mLlFundPrice;
    private TextView mTvFundPrice;
    private LinearLayout mLlMedicalPersonPayMoney;
    private TextView mTvMedicalPersonPayMoney;
    private LinearLayout mLlEnterprisePayMoney;
    private TextView mTvEnterprisePayMoney;
    private LinearLayout mLlHasPrice;
    private TextView mTvHasPrice;

    private LinearLayout mLlBtn;
    private ShapeTextView mTv01;
    private ShapeTextView mTv02;
    private ShapeTextView mTv03,mTv04;

    List<TagTextBean> tags = new ArrayList<>();
    private String orderNo;
    private CustomerDialogUtils customerDialogUtils;
    private CustomCountDownTimer mCustomCountDownTimer;
    private OrderDetailModel.DataBean detailData;
    private boolean hasPrescr;//是否有处方药
    private String totalPrice;//总价
    private int totalNum = 0;//商品总数

    @Override
    public int getLayoutId() {
        return R.layout.activity_order_detail;
    }

    @Override
    public void initView() {
        super.initView();
        mTvStatus = findViewById(R.id.tv_status);
        mTvTime = findViewById(R.id.tv_time);
        mTvDeliveryType = findViewById(R.id.tv_delivery_type);
        mLine01 = findViewById(R.id.line_01);
        mIvAddressSeize = findViewById(R.id.iv_address_seize);
        mTvAddressInfo = findViewById(R.id.tv_address_info);
        mSrlPrescrInfo = findViewById(R.id.srl_prescr_info);
        mTvLookPrescr = findViewById(R.id.tv_look_prescr);
        mTvOpenPrescrDescribe = findViewById(R.id.tv_open_prescr_describe);

        mTvOrderId = findViewById(R.id.tv_order_id);
        mTvOrderRefundStatus = findViewById(R.id.tv_order_refund_status);
        mLlOrderListContain = findViewById(R.id.ll_order_list_contain);
        mTvTotalNum = findViewById(R.id.tv_total_num);
        mTvTotalPrice = findViewById(R.id.tv_total_price);

        mTvOrderId02 = findViewById(R.id.tv_order_id_02);
        mTvOrderIdCopy = findViewById(R.id.tv_order_id_copy);
        mLlRefundNo = findViewById(R.id.ll_refund_no);
        mTvRefundNo = findViewById(R.id.tv_refund_no);
        mTvRefundNoCopy = findViewById(R.id.tv_refund_no_copy);
        mTvBusinessPhone = findViewById(R.id.tv_business_phone);
        mTvBusinessWx = findViewById(R.id.tv_business_wx);
        mTvBusinessWxCopy = findViewById(R.id.tv_business_wx_copy);
        mTvBuyerWx = findViewById(R.id.tv_buyer_wx);
        mTvBuyerWxCopy = findViewById(R.id.tv_buyer_wx_copy);
        mTvBuyerMsg = findViewById(R.id.tv_buyer_msg);
        mTvOrderCreateTime = findViewById(R.id.tv_order_create_time);
        mLlPayTime = findViewById(R.id.ll_pay_time);
        mTvOrderPayTime = findViewById(R.id.tv_order_pay_time);
        mLlDeliverGoodsTime = findViewById(R.id.ll_deliver_goods_time);
        mTvDeliverGoodsTime = findViewById(R.id.tv_deliver_goods_time);
        mLlFinishTime = findViewById(R.id.ll_finish_time);
        mTvFinishTime = findViewById(R.id.tv_finish_time);
        mLlCancleTime = findViewById(R.id.ll_cancle_time);
        mTvOrderCancleTime = findViewById(R.id.tv_order_cancle_time);

        mSllPreSettl = findViewById(R.id.sll_pre_settl);
        mTvSettleListTitle = findViewById(R.id.tv_settle_list_title);
        mTvPsnNo = findViewById(R.id.tv_psn_no);
        mTvPsnName = findViewById(R.id.tv_psn_name);
        mTvCertType = findViewById(R.id.tv_cert_type);
        mTvCertNo = findViewById(R.id.tv_cert_no);
        mTvInsutypeName = findViewById(R.id.tv_insutype_name);
        mTvFeedetlSn = findViewById(R.id.tv_feedetl_sn);
        mTvMedfeeSumamt = findViewById(R.id.tv_medfee_sumamt);
        mTvFundPaySumamt = findViewById(R.id.tv_fund_pay_sumamt);
        mTvAcctPay = findViewById(R.id.tv_acct_pay);
        mTvPsnCashPay = findViewById(R.id.tv_psn_cash_pay);
        mTvMoney = findViewById(R.id.tv_money);

        mSllSerialno = findViewById(R.id.sll_serialno);
        mLlIptOtpNo = findViewById(R.id.ll_ipt_otp_no);
        mTvIptOtpNo = findViewById(R.id.tv_ipt_otp_no);
        mLlRefundSerialno = findViewById(R.id.ll_refund_serialno);
        mTvRefundSerialno = findViewById(R.id.tv_refund_serialno);
        mLlOrderTotalPrice = findViewById(R.id.ll_order_total_price);
        mTvOrderTotalPrice = findViewById(R.id.tv_order_total_price);
        mLlFreight = findViewById(R.id.ll_freight);
        mTvFreight = findViewById(R.id.tv_freight);
        mLlMedicalPay = findViewById(R.id.ll_medical_pay);
        mTvMedicalPay = findViewById(R.id.tv_medical_pay);
        mLlMedicalPersonPay = findViewById(R.id.ll_medical_person_pay);
        mTvMedicalPersonPay = findViewById(R.id.tv_medical_person_pay);
        mLlEnterprisePay = findViewById(R.id.ll_enterprise_pay);
        mTvEnterprisePay = findViewById(R.id.tv_enterprise_pay);
        mLlPsnCashPay02 = findViewById(R.id.ll_psn_cash_pay02);
        mTvPsnCashPay02 = findViewById(R.id.tv_psn_cash_pay02);

        mSllWaitpayDetail = findViewById(R.id.sll_waitpay_detail);
        mLlOnlinePrice = findViewById(R.id.ll_online_price);
        mTvOnlinePrice = findViewById(R.id.tv_online_price);
        mLlFundPrice = findViewById(R.id.ll_fund_price);
        mTvFundPrice = findViewById(R.id.tv_fund_price);
        mLlMedicalPersonPayMoney = findViewById(R.id.ll_medical_person_pay_money);
        mTvMedicalPersonPayMoney = findViewById(R.id.tv_medical_person_pay_money);
        mLlEnterprisePayMoney = findViewById(R.id.ll_enterprise_pay_money);
        mTvEnterprisePayMoney = findViewById(R.id.tv_enterprise_pay_money);
        mLlHasPrice = findViewById(R.id.ll_has_price);
        mTvHasPrice = findViewById(R.id.tv_has_price);

        mLlBtn = findViewById(R.id.ll_btn);
        mTv01 = findViewById(R.id.tv_01);
        mTv02 = findViewById(R.id.tv_02);
        mTv03 = findViewById(R.id.tv_03);
        mTv04 = findViewById(R.id.tv_04);
    }

    @Override
    public void initData() {
        orderNo = getIntent().getStringExtra("orderNo");
        super.initData();

        showWaitDialog();
        getDetailData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvLookPrescr.setOnClickListener(this);

        mTvOrderIdCopy.setOnClickListener(this);
        mTvRefundNoCopy.setOnClickListener(this);
        mTvBusinessWxCopy.setOnClickListener(this);
        mTvBuyerWxCopy.setOnClickListener(this);

        mTv01.setOnClickListener(this);
        mTv02.setOnClickListener(this);
        mTv03.setOnClickListener(this);
        mTv04.setOnClickListener(this);
    }

    //获取详情数据
    protected void getDetailData() {
        //2022-10-12 模拟请求数据
        OrderDetailModel.sendOrderDetailRequest(TAG, orderNo, new CustomerJsonCallBack<OrderDetailModel>() {
            @Override
            public void onRequestError(OrderDetailModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(OrderDetailModel returnData) {
                hideWaitDialog();
                detailData = returnData.getData();
                if (detailData != null) {
                    setDetailData(returnData.getServer_time());
                }
            }
        });
    }

    //设置页面数据
    protected void setDetailData(String server_time) {
        // 2022-10-31 判断是否有带处方药
        hasPrescr = false;
        mLlOrderListContain.removeAllViews();
        totalPrice = "0";
        totalNum = 0;
        List<OrderDetailModel.DataBean.OrderGoodsBean> orderGoods = detailData.getOrderGoods();
        for (int i = 0, size = orderGoods.size(); i < size; i++) {
            OrderDetailModel.DataBean.OrderGoodsBean orderGood = orderGoods.get(i);
            if ("2".equals(orderGood.getItemType())) {
                hasPrescr = true;
            }

            totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(orderGood.getPrice())
                    .multiply(new BigDecimal(String.valueOf(orderGood.getGoodsNum())))).toPlainString();
            totalNum += orderGood.getGoodsNum();

            //设置商品信息
            View view = LayoutInflater.from(this).inflate(R.layout.layout_orderlist_product_item, mLlOrderListContain, false);
            ImageView ivPic = view.findViewById(R.id.iv_pic);
            ShapeTextView stvOtc = view.findViewById(R.id.stv_otc);
            ShapeTextView stvEnterpriseTag= view.findViewById(R.id.stv_enterprise_tag);
            TagTextView tvTitle = view.findViewById(R.id.tv_title);
            TextView tvEstimatePrice = view.findViewById(R.id.tv_estimate_price);
            TextView tvRealPrice = view.findViewById(R.id.tv_real_price);
            View lineItem = view.findViewById(R.id.line_item);

            GlideUtils.loadImg(orderGood.getBannerAccessoryUrl1(), ivPic, R.drawable.ic_placeholder_bg,
                    new RoundedCornersTransformation(getResources().getDimensionPixelSize(R.dimen.dp_16), 0));
            //判断是否是处方药
            if ("1".equals(orderGood.getItemType())) {
                stvOtc.setText("OTC");
                stvOtc.setVisibility(View.VISIBLE);
            } else if ("2".equals(orderGood.getItemType())) {
                stvOtc.setText("处方药");
                stvOtc.setVisibility(View.VISIBLE);
            } else {
                stvOtc.setVisibility(View.GONE);
            }
            //判断是否是平台药 若是 需要展示医保
            tags.clear();
            if ("1".equals(orderGood.getIsPlatformDrug())){//是平台药
                tags.add(new TagTextBean("医保",R.color.color_ee8734));
                tvTitle.setContentAndTag(EmptyUtils.strEmpty(orderGood.getGoodsName()), tags);
            }else {
                tvTitle.setContentAndTag(EmptyUtils.strEmpty(orderGood.getGoodsName()), tags);
            }

            //判断是否支持企业基金支付
            if ("1".equals(orderGood.getIsEnterpriseFundPay())){
                stvEnterpriseTag.setVisibility(View.VISIBLE);
            }else {
                stvEnterpriseTag.setVisibility(View.GONE);
            }

            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(orderGood.getPrice())));
            tvRealPrice.setText("x" + orderGood.getGoodsNum());
            //2022-10-10 最后一个item的分割线需要隐藏
            if (i == size - 1) {
                lineItem.setVisibility(View.GONE);
            } else {
                lineItem.setVisibility(View.VISIBLE);
            }
            mLlOrderListContain.addView(view);
        }
        //需要区分是否是自提订单
        if ("1".equals(detailData.getShippingMethod())) {//自提
            mTvDeliveryType.setText("自提");
            mLine01.setVisibility(View.GONE);
            mIvAddressSeize.setVisibility(View.GONE);
            mTvAddressInfo.setVisibility(View.GONE);
        } else {//商家配送
            mTvDeliveryType.setText("商家配送");
            mLine01.setVisibility(View.VISIBLE);
            mIvAddressSeize.setVisibility(View.VISIBLE);
            mTvAddressInfo.setVisibility(View.VISIBLE);

            mTvAddressInfo.setText(detailData.getFullName() + "    " + StringUtils.encryptionPhone(detailData.getPhone()) + "\n" + detailData.getLocation() + detailData.getAddress());
        }

        mTvOrderId.setText("订单编号：" + detailData.getOrderNo());
        //退款状态 是否已退款(0、 未退款 1、已退款）
        if ("1".equals(detailData.getIsRefunded())){
            mTvOrderRefundStatus.setVisibility(View.VISIBLE);
        }else {
            mTvOrderRefundStatus.setVisibility(View.GONE);
        }
        mTvTotalNum.setText("共" + totalNum + "件商品");
        mTvTotalPrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(totalPrice)));

        //退款编号
        mLlRefundNo.setVisibility(EmptyUtils.isEmpty(detailData.getOrderRefundNo())?View.GONE:View.VISIBLE);

        mTvOrderId02.setText(EmptyUtils.strEmpty(detailData.getOrderNo()));
        mTvRefundNo.setText(EmptyUtils.strEmpty(detailData.getOrderRefundNo()));
        mTvBusinessPhone.setText(EmptyUtils.strEmpty(detailData.getTelephone()));
        mTvBusinessWx.setText(EmptyUtils.strEmpty(detailData.getWechat()));
        mTvBuyerWx.setText(EmptyUtils.strEmpty(detailData.getBuyerWeChat()));
        mTvBuyerMsg.setText(EmptyUtils.strEmpty(detailData.getBuyerMessage()));
        mTvOrderCreateTime.setText(EmptyUtils.strEmpty(detailData.getCreateDT()));

        //医保结算 费用明细
        mTvIptOtpNo.setText(EmptyUtils.strEmpty(detailData.getIpt_otp_no()));
        mTvRefundSerialno.setText(EmptyUtils.strEmpty(detailData.getOrderRefundSerialNo()));
        mTvOrderTotalPrice.setText(EmptyUtils.strEmpty(detailData.getTotalAmount()));//订单总额
        mTvFreight.setText(EmptyUtils.strEmpty(detailData.getLogisticsFee()));//运费
        mTvMedicalPay.setText(EmptyUtils.strEmptyToText(detailData.getFund_pay_sumamt(),"0.0"));//医保报销
        mTvMedicalPersonPay.setText(EmptyUtils.strEmptyToText(detailData.getAcct_pay(),"0.0"));//医保个人账户支付
        mTvEnterprisePay.setText(EmptyUtils.strEmptyToText(detailData.getEnterpriseFundPay(),"0.0"));//企业基金支付
        mTvPsnCashPay02.setText(EmptyUtils.strEmptyToText(detailData.getOnlineAmount(),"0.0"));//个人现金支付

        //2022-10-10 需要区分 是否自费订单
        if ("1".equals(detailData.getPurchasingDrugsMethod())) {//是自费订单
            mSllPreSettl.setVisibility(View.GONE);
        } else {//医保报销订单
            //获取证件类型
            String psnCertTypeText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("PSN_CERT_TYPE", EmptyUtils.strEmpty(detailData.getMdtrt_cert_type()));
            //获取险种类型
            String insutypeText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("INSUTYPE", EmptyUtils.strEmpty(detailData.getInsutype()));
            mSllPreSettl.setVisibility(View.VISIBLE);
            mTvPsnNo.setText(EmptyUtils.strEmpty(detailData.getPsn_no()));
            mTvPsnName.setText(EmptyUtils.isEmpty(detailData.getPsn_name())?"":StringUtils.encryptionName(detailData.getPsn_name()));
            mTvCertType.setText(psnCertTypeText);
            mTvCertNo.setText(EmptyUtils.isEmpty(detailData.getMdtrt_cert_no())?"":StringUtils.encryptionIDCard(detailData.getMdtrt_cert_no()));
            mTvInsutypeName.setText(insutypeText);
            mTvFeedetlSn.setText(EmptyUtils.strEmpty(detailData.getIpt_otp_no()));
            mTvMedfeeSumamt.setText(EmptyUtils.strEmpty(detailData.getMedfee_sumamt()));
            mTvFundPaySumamt.setText(EmptyUtils.strEmpty(detailData.getFund_pay_sumamt()));
            mTvAcctPay.setText(EmptyUtils.strEmpty(detailData.getAcct_pay()));
            mTvPsnCashPay.setText(EmptyUtils.strEmpty(detailData.getPsn_cash_pay()));
            mTvMoney.setText("****元");
        }

        //(0 不开处方 1、待开处方 2、已开处方3、待医师确认处方4、第三方开处方 5.未提交问诊信息）
        if ("2".equals(detailData.getIsPrescription())){
            mTv04.setVisibility(View.VISIBLE);
            mTv04.setText("查看处方");
        }else {
            mTv04.setVisibility(View.GONE);
        }

        //请求数据 成功
        switch (detailData.getOrderStatus()) {
            case "1"://待付款
                onWaitPay(server_time);
                break;
            case "2"://待发货
                waitDelivery();
                break;
            case "3"://待收货
                waitReceive();
                break;
            case "4"://已完成
                complete();
                break;
            case "5"://用户取消订单（未支付）
                cancle1();
                break;
            case "6"://系统取消订单（未支付完成）
                //医保结算状态(0、 未结算 1、结算成功）
                if ("1".equals(detailData.getSettlementStatus())){//系统取消订单(医保已支付，线上支付未完成）
                    cancle2();
                }else {//系统取消订单(医保支付未完成，线上支付未完成）
                    cancle1();
                }
                break;
            case "7"://系统取消订单（支付完成）
                cancle3();
                break;
            case "8"://商家取消订单（支付完成）
                cancle4();
                break;
        }
    }

    //待付款
    private void onWaitPay(String server_time) {
        setTitle("待付款");
        mTvStatus.setText("待买家付款，剩余");
        mTvTime.setVisibility(View.VISIBLE);
        mTvTime.setText("30:00");

        mTvSettleListTitle.setText("医保预结算清单");
        //最后付款时间
        mLlPayTime.setVisibility(View.GONE);
        //发货时间
        mLlDeliverGoodsTime.setVisibility(View.GONE);
        //成交时间
        mLlFinishTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.GONE);
        //费用明细
        mLlIptOtpNo.setVisibility(View.GONE);
        mLlRefundSerialno.setVisibility(View.GONE);
        mLlMedicalPay.setVisibility(View.GONE);
        mLlMedicalPersonPay.setVisibility(View.GONE);
        mLlEnterprisePay.setVisibility(View.GONE);
        mLlPsnCashPay02.setVisibility(View.GONE);

        //1.先判断是自费还是医保支付
        mSllWaitpayDetail.setVisibility(View.VISIBLE);
        mLlOnlinePrice.setVisibility(View.VISIBLE);
        mTvOnlinePrice.setText(EmptyUtils.strEmpty(detailData.getTotalAmount()));
        if ("1".equals(detailData.getPurchasingDrugsMethod())) {//1自助购药（自费结算）
            mLlFundPrice.setVisibility(View.GONE);
            mLlMedicalPersonPayMoney.setVisibility(View.GONE);
            if ("1".equals(detailData.getEnterpriseFundPayStatus())){//企业基金已支付
                mLlEnterprisePayMoney.setVisibility(View.VISIBLE);
                mTvEnterprisePayMoney.setText(EmptyUtils.strEmpty(detailData.getEnterpriseFundPay()));
            }else {//企业基金未支付
                mLlEnterprisePayMoney.setVisibility(View.GONE);
            }
            mLlHasPrice.setVisibility(View.GONE);
        } else {//2、城乡居民医保/职工医保购药（医保结算）3、慢特病购药（医保结算）4、两病购药（医保结算）
            mLlFundPrice.setVisibility(View.VISIBLE);
            if ("1".equals(detailData.getSettlementStatus())){//医保个人账户已支付
                mLlMedicalPersonPayMoney.setVisibility(View.VISIBLE);
                mTvMedicalPersonPayMoney.setText(EmptyUtils.strEmpty(detailData.getAcct_pay()));
            }else {//医保个人账户未支付
                mLlMedicalPersonPayMoney.setVisibility(View.GONE);
            }
            if ("1".equals(detailData.getEnterpriseFundPayStatus())){//企业基金已支付
                mLlEnterprisePayMoney.setVisibility(View.VISIBLE);
                mTvEnterprisePayMoney.setText(EmptyUtils.strEmpty(detailData.getEnterpriseFundPay()));
            }else {//企业基金未支付
                mLlEnterprisePayMoney.setVisibility(View.GONE);
            }
            mLlHasPrice.setVisibility(View.VISIBLE);

            mTvFundPrice.setText(EmptyUtils.strEmpty(detailData.getPre_fund_pay_sumamt()));
            mTvHasPrice.setText(EmptyUtils.strEmpty(detailData.getOnlineAmount()));
        }

        mTv01.setVisibility(View.GONE);
        mTv02.setVisibility(View.VISIBLE);
        mTv02.setText("联系买家");
        mTv03.setVisibility(View.VISIBLE);
        mTv03.setText("修改订单");
        //计算  定时器时间==30分钟-已过去多少时间
        long pastTimes = new BigDecimal(DateUtil.dateToStamp2(server_time)).subtract(new BigDecimal(DateUtil.dateToStamp2(detailData.getCreateDT()))).longValue();
        setTimeStart(30 * 60 * 1000 - pastTimes);
    }
    //待发货
    private void waitDelivery() {
        setTitle("待发货");
        mTvStatus.setText("待卖家发货");
        mTvTime.setVisibility(View.GONE);
        //关闭定时器
        if (mCustomCountDownTimer != null) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }

        mTvSettleListTitle.setText("医保结算清单");
        //最后付款时间
        mLlPayTime.setVisibility(View.VISIBLE);
        mTvOrderPayTime.setText(EmptyUtils.strEmpty(detailData.getLastPaytTime()));
        //发货时间
        mLlDeliverGoodsTime.setVisibility(View.GONE);
        //成交时间
        mLlFinishTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.GONE);
        //费用明细
        //费用明细
        if ("1".equals(detailData.getPurchasingDrugsMethod())){//自助购药
            mLlIptOtpNo.setVisibility(View.GONE);
            mLlMedicalPay.setVisibility(View.GONE);
            mLlMedicalPersonPay.setVisibility(View.GONE);
        }else {
            mLlIptOtpNo.setVisibility(View.VISIBLE);
            mLlMedicalPay.setVisibility(View.VISIBLE);
            mLlMedicalPersonPay.setVisibility(View.VISIBLE);
        }
        mLlRefundSerialno.setVisibility(View.GONE);
        mLlEnterprisePay.setVisibility(View.VISIBLE);
        mLlPsnCashPay02.setVisibility(View.VISIBLE);
        //待付款支付明细
        mSllWaitpayDetail.setVisibility(View.GONE);

        mTv01.setVisibility(View.VISIBLE);
        mTv01.setText("取消订单");
        mTv02.setVisibility(View.VISIBLE);
        mTv02.setText("联系买家");
        mTv03.setVisibility(View.VISIBLE);
        mTv03.setText("立即发货");
    }
    //待收货
    private void waitReceive() {
        setTitle("待收货");
        mTvStatus.setText("待买家确认收货");
        mTvTime.setVisibility(View.GONE);

        mTvSettleListTitle.setText("医保结算清单");
        //最后付款时间
        mLlPayTime.setVisibility(View.VISIBLE);
        mTvOrderPayTime.setText(EmptyUtils.strEmpty(detailData.getLastPaytTime()));
        //发货时间
        mLlDeliverGoodsTime.setVisibility(View.VISIBLE);
        mTvDeliverGoodsTime.setText(EmptyUtils.strEmpty(detailData.getDeliveryTime()));
        //成交时间
        mLlFinishTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.GONE);
        //费用明细
        if ("1".equals(detailData.getPurchasingDrugsMethod())){//自助购药
            mLlIptOtpNo.setVisibility(View.GONE);
            mLlMedicalPay.setVisibility(View.GONE);
            mLlMedicalPersonPay.setVisibility(View.GONE);
        }else {
            mLlIptOtpNo.setVisibility(View.VISIBLE);
            mLlMedicalPay.setVisibility(View.VISIBLE);
            mLlMedicalPersonPay.setVisibility(View.VISIBLE);
        }
        mLlRefundSerialno.setVisibility(View.GONE);
        mLlEnterprisePay.setVisibility(View.VISIBLE);
        mLlPsnCashPay02.setVisibility(View.VISIBLE);
        //待付款支付明细
        mSllWaitpayDetail.setVisibility(View.GONE);

        mTv01.setVisibility(View.VISIBLE);
        mTv01.setText("取消订单");
        mTv02.setVisibility(View.VISIBLE);
        mTv02.setText("联系买家");
        if ("1".equals(detailData.getShippingMethod())) {//自提
            mTv03.setVisibility(View.GONE);
        } else {//商家配送
            mTv03.setVisibility(View.VISIBLE);
            mTv03.setText("查看物流");
        }
    }
    //已完成
    private void complete() {
        setTitle("已完成");
        mTvStatus.setText("订单完成");
        mTvTime.setVisibility(View.GONE);

        mTvSettleListTitle.setText("医保结算清单");
        //最后付款时间
        mLlPayTime.setVisibility(View.VISIBLE);
        mTvOrderPayTime.setText(EmptyUtils.strEmpty(detailData.getLastPaytTime()));
        //发货时间
        mLlDeliverGoodsTime.setVisibility(View.VISIBLE);
        mTvDeliverGoodsTime.setText(EmptyUtils.strEmpty(detailData.getDeliveryTime()));
        //成交时间
        mLlFinishTime.setVisibility(View.VISIBLE);
        mTvFinishTime.setText(EmptyUtils.strEmpty(detailData.getConfirmTime()));
        //订单取消时间
        mLlCancleTime.setVisibility(View.GONE);
        //费用明细
        if ("1".equals(detailData.getPurchasingDrugsMethod())){//自助购药
            mLlIptOtpNo.setVisibility(View.GONE);
            mLlMedicalPay.setVisibility(View.GONE);
            mLlMedicalPersonPay.setVisibility(View.GONE);
        }else {
            mLlIptOtpNo.setVisibility(View.VISIBLE);
            mLlMedicalPay.setVisibility(View.VISIBLE);
            mLlMedicalPersonPay.setVisibility(View.VISIBLE);
        }
        mLlRefundSerialno.setVisibility(View.GONE);
        mLlEnterprisePay.setVisibility(View.VISIBLE);
        mLlPsnCashPay02.setVisibility(View.VISIBLE);
        //待付款支付明细
        mSllWaitpayDetail.setVisibility(View.GONE);

        mTv01.setVisibility(View.VISIBLE);
        mTv01.setText("取消订单");
        mTv02.setVisibility(View.VISIBLE);
        mTv02.setText("联系买家");
        if ("1".equals(detailData.getShippingMethod())) {//自提
            mTv03.setVisibility(View.GONE);
        } else {//商家配送
            mTv03.setVisibility(View.VISIBLE);
            mTv03.setText("查看物流");
        }
    }
    //用户取消订单（未支付）
    private void cancle1() {
        setTitle("已取消");
        mTvStatus.setText("已取消（交易关闭）");
        mTvTime.setVisibility(View.GONE);

        mTvSettleListTitle.setText("医保预结算清单");
        //最后付款时间
        mLlPayTime.setVisibility(View.GONE);
        //发货时间
        mLlDeliverGoodsTime.setVisibility(View.GONE);
        //成交时间
        mLlFinishTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.VISIBLE);
        mTvOrderCancleTime.setText(EmptyUtils.strEmpty(detailData.getCancelTime()));
        //费用明细
        mLlIptOtpNo.setVisibility(View.GONE);
        mLlRefundSerialno.setVisibility(View.GONE);
        mLlMedicalPay.setVisibility(View.GONE);
        mLlMedicalPersonPay.setVisibility(View.GONE);
        mLlEnterprisePay.setVisibility(View.GONE);
        mLlPsnCashPay02.setVisibility(View.GONE);
        //待付款支付明细
        mSllWaitpayDetail.setVisibility(View.GONE);

        mTv01.setVisibility(View.GONE);
        mTv02.setVisibility(View.VISIBLE);
        mTv02.setText("联系买家");
        mTv03.setVisibility(View.VISIBLE);
        mTv03.setText("删除订单");
    }
    //系统取消订单（未支付完成）
    private void cancle2() {
        setTitle("已取消");
        mTvStatus.setText("已取消（交易关闭）");
        mTvTime.setVisibility(View.GONE);

        mTvSettleListTitle.setText("医保结算清单");
        //最后付款时间
        mLlPayTime.setVisibility(View.GONE);
        //发货时间
        mLlDeliverGoodsTime.setVisibility(View.GONE);
        //成交时间
        mLlFinishTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.VISIBLE);
        mTvOrderCancleTime.setText(EmptyUtils.strEmpty(detailData.getCancelTime()));
        //费用明细
        //费用明细
        if ("1".equals(detailData.getPurchasingDrugsMethod())){//自助购药
            mLlIptOtpNo.setVisibility(View.GONE);
            mLlMedicalPay.setVisibility(View.GONE);
            mLlMedicalPersonPay.setVisibility(View.GONE);
        }else {
            mLlIptOtpNo.setVisibility(View.VISIBLE);
            mLlMedicalPay.setVisibility(View.VISIBLE);
            mLlMedicalPersonPay.setVisibility(View.VISIBLE);
        }
        mLlRefundSerialno.setVisibility(View.VISIBLE);
        mLlEnterprisePay.setVisibility(View.GONE);
        mLlPsnCashPay02.setVisibility(View.GONE);
        //待付款支付明细
        mSllWaitpayDetail.setVisibility(View.GONE);

        mTv01.setVisibility(View.GONE);
        mTv02.setVisibility(View.VISIBLE);
        mTv02.setText("联系商家");
        mTv03.setVisibility(View.VISIBLE);
        mTv03.setText("删除订单");
    }
    //系统取消订单（支付完成）
    private void cancle3() {
        setTitle("已取消");
        mTvStatus.setText("已取消（交易关闭）");
        mTvTime.setVisibility(View.GONE);

        mTvSettleListTitle.setText("医保结算清单");
        //最后付款时间
        mLlPayTime.setVisibility(View.VISIBLE);
        mTvOrderPayTime.setText(EmptyUtils.strEmpty(detailData.getLastPaytTime()));
        //发货时间
        if ("0".equals(detailData.getIsViewLogistics())){
            mLlDeliverGoodsTime.setVisibility(View.VISIBLE);
            mTvDeliverGoodsTime.setText(EmptyUtils.strEmpty(detailData.getDeliveryTime()));
        }else {
            mLlDeliverGoodsTime.setVisibility(View.GONE);
        }
        //成交时间
        mLlFinishTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.VISIBLE);
        mTvOrderCancleTime.setText(EmptyUtils.strEmpty(detailData.getCancelTime()));
        //费用明细
        //费用明细
        if ("1".equals(detailData.getPurchasingDrugsMethod())){//自助购药
            mLlIptOtpNo.setVisibility(View.GONE);
            mLlMedicalPay.setVisibility(View.GONE);
            mLlMedicalPersonPay.setVisibility(View.GONE);
        }else {
            mLlIptOtpNo.setVisibility(View.VISIBLE);
            mLlMedicalPay.setVisibility(View.VISIBLE);
            mLlMedicalPersonPay.setVisibility(View.VISIBLE);
        }
        mLlRefundSerialno.setVisibility(View.VISIBLE);
        mLlEnterprisePay.setVisibility(View.VISIBLE);
        mLlPsnCashPay02.setVisibility(View.VISIBLE);
        //待付款支付明细
        mSllWaitpayDetail.setVisibility(View.GONE);

        mTv01.setVisibility(View.VISIBLE);
        mTv01.setText("退款进度");
        mTv02.setVisibility(View.VISIBLE);
        mTv02.setText("联系商家");
        mTv03.setVisibility(View.VISIBLE);
        mTv03.setText("删除订单");
    }
    //商家取消订单（支付完成）
    private void cancle4() {
        setTitle("已取消");
        mTvStatus.setText("订单取消");
        mTvTime.setVisibility(View.GONE);

        mTvSettleListTitle.setText("医保结算清单");
        //最后付款时间
        mLlPayTime.setVisibility(View.VISIBLE);
        mTvOrderPayTime.setText(EmptyUtils.strEmpty(detailData.getLastPaytTime()));
        //发货时间
        if ("0".equals(detailData.getIsViewLogistics())){
            mLlDeliverGoodsTime.setVisibility(View.VISIBLE);
            mTvDeliverGoodsTime.setText(EmptyUtils.strEmpty(detailData.getDeliveryTime()));
        }else {
            mLlDeliverGoodsTime.setVisibility(View.GONE);
        }
        //成交时间
        mLlFinishTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.VISIBLE);
        mTvOrderCancleTime.setText(EmptyUtils.strEmpty(detailData.getCancelTime()));
        //费用明细
        if ("1".equals(detailData.getPurchasingDrugsMethod())){//自助购药
            mLlIptOtpNo.setVisibility(View.GONE);
            mLlMedicalPay.setVisibility(View.GONE);
            mLlMedicalPersonPay.setVisibility(View.GONE);
        }else {
            mLlIptOtpNo.setVisibility(View.VISIBLE);
            mLlMedicalPay.setVisibility(View.VISIBLE);
            mLlMedicalPersonPay.setVisibility(View.VISIBLE);
        }
        mLlRefundSerialno.setVisibility(View.VISIBLE);
        mLlEnterprisePay.setVisibility(View.VISIBLE);
        mLlPsnCashPay02.setVisibility(View.VISIBLE);
        //待付款支付明细
        mSllWaitpayDetail.setVisibility(View.GONE);

        mTv01.setVisibility(View.VISIBLE);
        mTv01.setText("退款进度");
        mTv02.setVisibility(View.VISIBLE);
        mTv02.setText("联系商家");
        if ("1".equals(detailData.getShippingMethod())){//自提
            mTv03.setVisibility(View.GONE);
        }else {//商家配送
            // 2023/6/10 此处还得加个判断 商户端那边取消的时候 订单是否是处在代发货的状态 如果是 那么查看物流就该隐藏
            if ("0".equals(detailData.getIsViewLogistics())){
                mTv03.setVisibility(View.VISIBLE);
                mTv03.setText("查看物流");
            }else {
                mTv03.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        if (R.id.tv_look_prescr == v.getId()) {//查看处方
            //  2022-10-26 判断是是否是医师
            // 《1》是医师 在判断是否开具处方完成 1.未开具处方 按钮显示开具处方   2已开具处方 按钮显示查看处方   处方开具完成可以查看处方才能点击“立即发货”按钮
            // 《2》是管理员 其他人员 在判断是否开具处方完成 1.未开具处方 点击按钮提示处方暂未开具   2已开具处方 按钮显示查看处方   处方开具完成可以查看处方才能点击“立即发货”按钮
            //模拟开具处方状态
            if ("2".equals(LoginUtils.getType())) {//医师身份
                //是否开处方(0 不开处方 1、待开处方 2、已开处方 3、待医师确认处方
                if ("2".equals(detailData.getIsPrescription())) {//已开具处方
                    ElectPrescActivity.newIntance(OrderDetailActivity.this, orderNo);
                }else if ("3".equals(detailData.getIsPrescription())){//未开具处方
                    Intent intent = new Intent(OrderDetailActivity.this, OpenPrescActivity_order.class);
                    intent.putExtra("orderNo", orderNo);
                    intent.putExtra("storeAccessoryUrl", detailData.getStoreAccessoryUrl());
                    intent.putExtra("storeName", detailData.getStoreName());
                    intent.putParcelableArrayListExtra("orderGoods", detailData.getOrderGoods());
                    startActivityForResult(intent, new OnActivityCallback() {
                        @Override
                        public void onActivityResult(int resultCode, @Nullable Intent data) {
                            if (resultCode == RESULT_OK) {
                                detailData.setIsPrescription("2");
                                mTvLookPrescr.setText("查看处方");
                            }
                        }
                    });
                }else {//等待用户提交问诊信息
                    showShortToast("等待用户提交问诊信息");
                }
            } else {//只要不是医师身份 都走这边的逻辑
                if ("2".equals(detailData.getIsPrescription())) {//已开具处方
                    ElectPrescActivity.newIntance(OrderDetailActivity.this, orderNo);
                } else {//未开具处方
                    showShortToast("未开具处方");
                }
            }
        }else if (R.id.tv_order_id_copy == v.getId()) {
            RxClipboardTool.copyText(OrderDetailActivity.this, mTvOrderId02.getText().toString());
        } else if (R.id.tv_refund_no_copy == v.getId()) {
            RxClipboardTool.copyText(OrderDetailActivity.this, mTvRefundNo.getText().toString());
        } else if (R.id.tv_business_wx_copy == v.getId()) {
            RxClipboardTool.copyText(OrderDetailActivity.this, mTvBusinessWx.getText().toString());
        } else if (R.id.tv_buyer_wx_copy == v.getId()) {
            RxClipboardTool.copyText(OrderDetailActivity.this, mTvBuyerWx.getText().toString());
        }  else {
            switch (((TextView) v).getText().toString()) {
                case "取消订单":
                    onCancle();
                    break;
                case "退款进度":
                    RefundProgressActivity.newIntance(OrderDetailActivity.this,detailData.getOrderNo(),detailData.getOnlineAmount(),detailData.getSettlementStatus());
                    break;
                case "联系买家":
                    onTakePhone(detailData.getPhone());
                    break;
                case "修改订单":
                    Intent intent = new Intent(OrderDetailActivity.this, UpdateOrderActivity.class);
                    intent.putExtra("OrderNo", detailData.getOrderNo());
                    intent.putExtra("ShippingMethod", detailData.getShippingMethod());
                    intent.putExtra("FullName", detailData.getFullName());
                    intent.putExtra("Phone", detailData.getPhone());
                    intent.putExtra("Location", detailData.getLocation());
                    intent.putExtra("Address", detailData.getAddress());
                    intent.putExtra("AreaName", detailData.getAreaName());
                    intent.putExtra("CityName", detailData.getCityName());
                    intent.putExtra("latitude", detailData.getLatitude());
                    intent.putExtra("longitude", detailData.getLongitude());
                    startActivityForResult(intent, new BaseActivity.OnActivityCallback() {
                        @Override
                        public void onActivityResult(int resultCode, @Nullable Intent data) {
                            if (resultCode == -1) {
                                showWaitDialog();
                                getDetailData();
                                //当修改订单成功之后 前端需要通知订单列表页面更新该订单的订单数据
                                Intent intent = new Intent();
                                intent.putExtra("OperationType", "updateOrder");
                                setResult(RESULT_OK, intent);
                            }
                        }
                    });
                    break;
                case "立即发货":
                    //开具处方状态
                    if ("0".equals(detailData.getIsPrescription())||"2".equals(detailData.getIsPrescription())) {//不需开具处方或者已开具处方
                        immediateDelivery();
                    } else {//需开具处方但未开具处方
                        showShortToast("未开具处方");
                    }
                    break;
                case "查看物流":
                    LogisticsActivity.newIntance(OrderDetailActivity.this,detailData.getOrderNo());
                    break;
                case "删除订单":
                    onDelete();
                    break;
                case "查看处方":
                    if ("1".equals(detailData.getIsThirdPartyPres())){
                        ElectPrescActivity2.newIntance(OrderDetailActivity.this, orderNo);
                    }else {
                        ElectPrescActivity.newIntance(OrderDetailActivity.this, orderNo);
                    }
                    break;
            }
        }
    }

    //开启定时器  millisecond:倒计时的时间 1:待付款倒计时
    protected void setTimeStart(long millisecond) {
        //防止内存泄漏
        if (null != mCustomCountDownTimer) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        mCustomCountDownTimer = new CustomCountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long M = (millisUntilFinished % (60 * 60 * 1000)) / (60 * 1000);
                long S = (millisUntilFinished % (60 * 1000)) / 1000;

                mTvTime.setText((M < 10 ? "0" + M : String.valueOf(M)) + ":" + (S < 10 ? "0" + S : String.valueOf(S)));
            }

            @Override
            public void onFinish() {
                mTvTime.setText("00:00");
            }
        };

        mCustomCountDownTimer.start();
    }

    //取消订单 按钮点击事件
    private void onCancle() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(OrderDetailActivity.this, "确定取消订单吗？", "注意：取消订单后医保支付部分将撤销,同时在线支付部分也将原路返回到个人账户。", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                //2022-10-10 请求接口取消该订单
                showWaitDialog();
                BaseModel.sendStoreCancelOrderRequest(TAG, orderNo, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast(returnData.getMsg());
                        //当订单取消成功之后 前端需要移除该订单的展示
                        Intent intent = new Intent();
                        intent.putExtra("OperationType", "cancle");
                        intent.putExtra("orderNo", orderNo);
                        setResult(RESULT_OK, intent);
                        goFinish();
                    }
                });
            }
        });
    }

    //联系买家
    private void onTakePhone(String phone) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(OrderDetailActivity.this, "提示", "是否拨打买家电话" + phone + "?", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                RxTool.takePhone(OrderDetailActivity.this, phone);
            }
        });
    }

    //立即发货
    private void immediateDelivery() {
        if ("0".equals(detailData.getIsLogistics())){
            showWaitDialog();
            BaseModel.sendImmediateDeliveryRequest(TAG, orderNo, "1", "自提", detailData.getFullName(), detailData.getPhone(),
                    detailData.getLocation() + detailData.getAddress(), "", "", "", "",
                    new CustomerJsonCallBack<BaseModel>() {
                        @Override
                        public void onRequestError(BaseModel returnData, String msg) {
                            hideWaitDialog();
                            showShortToast(msg);
                        }

                        @Override
                        public void onRequestSuccess(BaseModel returnData) {
                            hideWaitDialog();
                            //当订单立即发货成功之后 前端需要移除该订单的展示
                            Intent intent = new Intent(Constants.Action.SEND_REFRESH_DAILY_LOTTERY);
                            intent.putExtra("OperationType", "immediate_Delivery");
                            intent.putExtra("orderNo", orderNo);
                            intent.putExtra("status", "2");
                            BroadCastReceiveUtils.sendLocalBroadCast(OrderDetailActivity.this, intent);
                            showShortToast("立即发货成功");
                            // 2022-10-12 刷新页面
                            showWaitDialog();
                            getDetailData();
                        }
                    });
        }else {
            Intent intent = new Intent(OrderDetailActivity.this, ImmediateDeliveryActivity.class);
            intent.putExtra("orderNo",detailData.getOrderNo());
            intent.putExtra("isLogistics",detailData.getIsLogistics());
            intent.putExtra("fullName",detailData.getFullName());
            intent.putExtra("phone",detailData.getPhone());
            intent.putExtra("location",detailData.getLocation());
            intent.putExtra("address",detailData.getAddress());
            startActivityForResult(intent, new BaseActivity.OnActivityCallback() {
                @Override
                public void onActivityResult(int resultCode, @Nullable Intent data) {
                    if (resultCode == -1) {

                        //当订单立即发货成功之后 前端需要移除该订单的展示
                        Intent intent = new Intent(Constants.Action.SEND_REFRESH_DAILY_LOTTERY);
                        intent.putExtra("OperationType", "immediate_Delivery");
                        intent.putExtra("orderNo", orderNo);
                        intent.putExtra("status", "2");
                        BroadCastReceiveUtils.sendLocalBroadCast(OrderDetailActivity.this, intent);
                        showShortToast("立即发货成功");
                        // 2022-10-12 刷新页面
                        showWaitDialog();
                        getDetailData();
                    }
                }
            });
        }
    }

    //删除订单
    private void onDelete() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(OrderDetailActivity.this, "确定删除此订单吗？", "注意:订单删除后不可恢复,谨慎操作!", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                // 2022-10-10 请求接口删除该订单
                showWaitDialog();
                BaseModel.sendStoreDelOrderRequest(TAG, orderNo, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast(returnData.getMsg());
                        //当订单删除成功之后 前端需要移除该订单的展示
                        Intent intent = new Intent();
                        intent.putExtra("OperationType", "delete");
                        intent.putExtra("orderNo", orderNo);
                        setResult(RESULT_OK, intent);
                        goFinish();
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCustomCountDownTimer != null) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context, String orderNo) {
        Intent intent = new Intent(context, OrderDetailActivity.class);
        intent.putExtra("orderNo", orderNo);
        context.startActivity(intent);
    }
}
