package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.FileUtils;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.utils.DownLoadUtils;

import java.io.File;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023/3/18
 * 描    述：pdf 查看
 * ================================================
 */
public class PdfViewActivity extends BaseActivity implements OnLoadCompleteListener,OnPageChangeListener, OnPageErrorListener {
    private PDFView mPdfView;
    private String url;
    Integer pageNumber = 0;

    @Override
    public int getLayoutId() {
        return R.layout.activity_pdfview;
    }

    @Override
    public void initView() {
        super.initView();
        mPdfView = findViewById(R.id.pdfView);
    }

    @Override
    public void initData() {
        url=getIntent().getStringExtra("url");
        super.initData();

        int firstPos=url.lastIndexOf("/");
        String relativeFileName=url.substring(firstPos+1);
        String fileName= BaseGlobal.getFileDir() + relativeFileName;

        if (FileUtils.isFileExists(fileName)) {
            openPdf(new File(fileName));
        }else {
            showWaitDialog();
            downLoadPdf(relativeFileName);
        }
    }

    //下载pdf文档
    private void downLoadPdf(String relativeFileName) {
        DownLoadUtils.downLoadPdf(url, "down_doc", BaseGlobal.getFileDir(), relativeFileName, new DownLoadUtils.IVideoDownLoad() {
            @Override
            public void onDownLoadSuccess(File response, String tag) {
                hideWaitDialog();

                openPdf(response);
            }

            @Override
            public void onDownLoadFail(String errorText) {
                hideWaitDialog();
                showShortToast("文件下载失败");
            }
        });
    }

    //打开pdf
    private void openPdf(File response){
        mPdfView.fromFile(response)
                .defaultPage(pageNumber)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(PdfViewActivity.this))
                .spacing(10) // in dp
                .onPageError(this)
                .load();
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
    }

    @Override
    public void loadComplete(int nbPages) {}

    @Override
    public void onPageError(int page, Throwable t) {}

    public static void newIntance(Context context,String url) {
        Intent intent = new Intent(context, PdfViewActivity.class);
        intent.putExtra("url",url);
        context.startActivity(intent);
    }
}
