package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.utils.LoginUtils;

import java.math.BigDecimal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-08-11
 * 描    述：个人信息页面
 * ================================================
 */
public class PersonalActivity_Zhyf extends BaseActivity {
    private LinearLayout mLlHead;
    private ImageView mIvHead;
    private LinearLayout mLlPersonType;
    private TextView mTvPersonType;
    private LinearLayout mLlName;
    private TextView mTvName;
    private LinearLayout mLlPhone;
    private TextView mTvPhone;
    private LinearLayout mLlCertno;
    private TextView mTvCertno;

    private TextView mTvDoctorCertPic;
    private ShapeFrameLayout mSflDoctorCertPic;
    private ImageView mIvDoctorCertPic;

    @Override
    public int getLayoutId() {
        return R.layout.activity_personal_zhyf;
    }

    @Override
    public void initView() {
        super.initView();
        mLlHead = findViewById(R.id.ll_head);
        mIvHead = findViewById(R.id.iv_head);
        mLlPersonType = findViewById(R.id.ll_person_type);
        mTvPersonType = findViewById(R.id.tv_person_type);
        mLlName = findViewById(R.id.ll_name);
        mTvName = findViewById(R.id.tv_name);
        mLlPhone = findViewById(R.id.ll_phone);
        mTvPhone = findViewById(R.id.tv_phone);
        mLlCertno = findViewById(R.id.ll_certno);
        mTvCertno = findViewById(R.id.tv_certno);

        mTvDoctorCertPic = findViewById(R.id.tv_doctor_cert_pic);
        mSflDoctorCertPic = findViewById(R.id.sfl_doctor_cert_pic);
        mIvDoctorCertPic = findViewById(R.id.iv_doctor_cert_pic);
    }

    @Override
    public void initData() {
        super.initData();

        GlideUtils.loadImg(LoginUtils.getThrAccessoryUrl(), mIvHead,R.drawable.ic_insured_head,new CircleCrop());
        //不同身份的人进入首页 展现不一样
        switch (LoginUtils.getType()){
            case "1"://管理员
                mTvPersonType.setText("管理员");

                mTvDoctorCertPic.setVisibility(View.GONE);
                mSflDoctorCertPic.setVisibility(View.GONE);
                break;
            case "2"://医师
                mTvPersonType.setText("医师");
                mTvDoctorCertPic.setVisibility(View.VISIBLE);
                mSflDoctorCertPic.setVisibility(View.VISIBLE);
                //医师资格证
                if (!EmptyUtils.isEmpty(LoginUtils.getAccessoryUrl())) {
                    GlideUtils.getImageWidHeig(this, LoginUtils.getAccessoryUrl(), new GlideUtils.IGetImageData() {
                        @Override
                        public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvDoctorCertPic.getLayoutParams();
                            if (radio >= 2.029) {
                                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                            } else {
                                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                            }

                            mIvDoctorCertPic.setLayoutParams(layoutParams);
                            GlideUtils.loadImg(LoginUtils.getAccessoryUrl(), mIvDoctorCertPic);

                            mIvDoctorCertPic.setTag(R.id.tag_1, LoginUtils.getAccessoryId());
                            mIvDoctorCertPic.setTag(R.id.tag_2, LoginUtils.getAccessoryUrl());
                        }
                    });
                }
                break;
            case "3"://其他人员
                mTvPersonType.setText(EmptyUtils.strEmpty(LoginUtils.getDrTypeName()));
                mTvDoctorCertPic.setVisibility(View.VISIBLE);
                if (!EmptyUtils.isEmpty(LoginUtils.getAccessoryUrl())) {
                    GlideUtils.getImageWidHeig(this, LoginUtils.getAccessoryUrl(), new GlideUtils.IGetImageData() {
                        @Override
                        public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvDoctorCertPic.getLayoutParams();
                            if (radio >= 2.029) {
                                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                            } else {
                                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                            }

                            mIvDoctorCertPic.setLayoutParams(layoutParams);
                            GlideUtils.loadImg(LoginUtils.getAccessoryUrl(), mIvDoctorCertPic);

                            mIvDoctorCertPic.setTag(R.id.tag_1, LoginUtils.getAccessoryId());
                            mIvDoctorCertPic.setTag(R.id.tag_2, LoginUtils.getAccessoryUrl());
                        }
                    });
                }
                break;
        }

        mTvName.setText("管理员".equals(LoginUtils.getUserName())?"管理员":StringUtils.encryptionName(LoginUtils.getUserName()));
        mTvPhone.setText(StringUtils.encryptionPhone(LoginUtils.getDr_phone()));
        mTvCertno.setText(StringUtils.encryptionIDCard(LoginUtils.getIDNumber()));
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mIvDoctorCertPic.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.iv_doctor_cert_pic://查看医师凭证
                OpenImage.with(PersonalActivity_Zhyf.this)
                        //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                        .setClickImageView(mIvDoctorCertPic)
                        //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                        .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                        //RecyclerView的数据
                        .setImageUrl(String.valueOf(mIvDoctorCertPic.getTag(R.id.tag_2)), MediaType.IMAGE)
                        //点击的ImageView所在数据的位置
                        .setClickPosition(0)
                        //开始展示大图
                        .show();
                break;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, PersonalActivity_Zhyf.class);
        context.startActivity(intent);
    }
}
