package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.PhaSortSpecialAreaAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.DetailCatalogueModel;
import com.jrdz.zhyb_android.utils.LoginUtils;

import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-14
 * 描    述：药品专区 页面
 * ================================================
 */
public class PhaSortSpecialAreaActivity extends BaseRecyclerViewActivity {
    private EditText mEtSearch;
    private ShapeTextView mStvSearch;
    private View subTopPic;
    private ImageView ivTopPic;

    private String catalogueId,catalogueName;
    private CustomerDialogUtils customerDialogUtils;

    private ObserverWrapper<String> mAddGoodsObserver=new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    };


    @Override
    public int getLayoutId() {
        return R.layout.activity_phasort_specialarea;
    }

    @Override
    public void initView() {
        super.initView();
        mEtSearch = findViewById(R.id.et_search);
        mStvSearch = findViewById(R.id.stv_search);
    }

    @Override
    public void initData() {
        catalogueId = getIntent().getStringExtra("catalogueId");
        catalogueName = getIntent().getStringExtra("catalogueName");
        super.initData();
        MsgBus.sendAddGoodsRefresh().observe(this, mAddGoodsObserver);

        setTitle(catalogueName + "专区");
        if (LoginUtils.isManage()) {
            setRightTitleView("添加");
        }
        //初始化头部图片
        initHeadView();

        showWaitDialog();
        getTopPicData();
        onRefresh(mRefreshLayout);
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        return new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false);
    }

    @Override
    public void initAdapter() {
        mAdapter = new PhaSortSpecialAreaAdapter();
        mAdapter.setHeaderAndEmpty(true);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                }
            }
        });
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    search();
                    return true;
                }
                return false;
            }
        });
        mStvSearch.setOnClickListener(this);
    }

    private void initHeadView() {
        View headView = LayoutInflater.from(this).inflate(R.layout.layout_specialsort_headview, mRecyclerView, false);
        subTopPic= headView.findViewById(R.id.sub_top_pic);
        ivTopPic = headView.findViewById(R.id.iv_top_pic);
        mAdapter.addHeaderView(headView);
    }

    //获取顶部图片的数据
    private void getTopPicData() {
        DetailCatalogueModel.sendDetailCatalogueRequest_user(TAG, catalogueId, new CustomerJsonCallBack<DetailCatalogueModel>() {
            @Override
            public void onRequestError(DetailCatalogueModel returnData, String msg) {
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(DetailCatalogueModel returnData) {
                if (returnData.getData()!=null&&!EmptyUtils.isEmpty(returnData.getData().getSubAccessoryUrl())){
                    ivTopPic.setVisibility(View.VISIBLE);
                    GlideUtils.loadImg(returnData.getData().getSubAccessoryUrl(), ivTopPic, R.drawable.ic_placeholder_bg,
                            new RoundedCornersTransformation(getResources().getDimensionPixelSize(R.dimen.dp_16), 0));
                }else {
                    ivTopPic.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void rightTitleViewClick() {
        SpecialAreaAddGoodsActivity.newIntance(this,"1",catalogueId,catalogueName);
    }

    @Override
    public void getData() {
        GoodsModel.sendGoodsRequest(TAG, String.valueOf(mPageNum), "20", mEtSearch.getText().toString(),
                catalogueId, "", new CustomerJsonCallBack<GoodsModel>() {
                    @Override
                    public void onRequestError(GoodsModel returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(GoodsModel returnData) {
                        hideRefreshView();
                        List<GoodsModel.DataBean> infos = returnData.getData();
                        if (mAdapter!=null&&infos != null) {
                            if (mPageNum == 0) {
                                mAdapter.setNewData(infos);
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        GoodsModel.DataBean itemData = ((PhaSortSpecialAreaAdapter) adapter).getItem(position);
        Intent intent=new Intent(this,LookGoodsActivity.class);
        intent.putExtra("GoodsNo", itemData.getGoodsNo());
        startActivityForResult(intent, new OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode==RESULT_OK){
                    mAdapter.remove(position);
                }
            }
        });
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        GoodsModel.DataBean itemData = ((PhaSortSpecialAreaAdapter) adapter).getItem(position);
        switch (view.getId()) {
            case R.id.stv_lower_shelf://下架
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(PhaSortSpecialAreaActivity.this, "确定下架所选产品吗？","注意:有未完成订单的产品的不能下架!",
                        "取消", "确定",
                        new CustomerDialogUtils.IDialogListener() {

                            @Override
                            public void onBtn01Click() {

                            }

                            @Override
                            public void onBtn02Click() {
                                onLowerShelf(itemData.getGoodsNo(), position);
                            }
                        });
                break;
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.stv_search:
                search();
                break;
        }
    }

    //下架
    private void onLowerShelf(String id, int position) {
        Log.e("666666", "id===" + id);
        showWaitDialog();
        BaseModel.sendOnOffGoodsRequest(TAG, id, "2", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("下架成功");
                mAdapter.remove(position);
            }
        });
    }

    //搜索
    private void search() {
        if (EmptyUtils.isEmpty(mEtSearch.getText().toString())) {
            showShortToast("请输入药品名称");
            return;
        }
        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context,String catalogueId, String catalogueName) {
        Intent intent = new Intent(context, PhaSortSpecialAreaActivity.class);
        intent.putExtra("catalogueId", catalogueId);
        intent.putExtra("catalogueName", catalogueName);
        context.startActivity(intent);
    }
}
