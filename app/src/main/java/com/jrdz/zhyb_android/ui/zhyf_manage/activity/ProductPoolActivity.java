package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.ProductPoolAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.utils.LoginUtils;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-17
 * 描    述：产品池 页面
 * ================================================
 */
public class ProductPoolActivity extends BaseRecyclerViewActivity {
    private ShapeLinearLayout mSllSearch;
    private EditText mEtSearch;
    private ShapeTextView mStvSearch;
    private View mLine;
    private LinearLayout mLlBottom;
    private FrameLayout mFlCbAll;
    private CheckBox mCbAll;
    private ShapeTextView mStvBtn03;
    private ShapeTextView mStvBtn02;
    private ShapeTextView mStvBtn01;

    private boolean isEdit = false;//是否在管理状态
    int selectNum = 0;//选中的商品数量
    private CustomerDialogUtils customerDialogUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_product_pool;
    }

    @Override
    public void initView() {
        super.initView();
        mEtSearch = findViewById(R.id.et_search);
        mStvSearch = findViewById(R.id.stv_search);

        mLine = findViewById(R.id.line);
        mLlBottom = findViewById(R.id.ll_bottom);
        mFlCbAll = findViewById(R.id.fl_cb_all);
        mCbAll = findViewById(R.id.cb_all);

        mStvBtn03 = findViewById(R.id.stv_btn03);
        mStvBtn02 = findViewById(R.id.stv_btn02);
        mStvBtn01 = findViewById(R.id.stv_btn01);
    }

    @Override
    public void initData() {
        super.initData();
        setRightTitleView("管理");

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(false, 0.2f)
                .titleBar(mTitleBar);
        mImmersionBar.init();
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        return new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false);
    }

    @Override
    public void initAdapter() {
        mAdapter = new ProductPoolAdapter();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();

        mFlCbAll.setOnClickListener(this);
        mStvBtn03.setOnClickListener(this);
        mStvBtn02.setOnClickListener(this);
        mStvBtn01.setOnClickListener(this);

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                }
            }
        });
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    search();
                    return true;
                }
                return false;
            }
        });
        mStvSearch.setOnClickListener(this);
    }

    @Override
    public void rightTitleViewClick() {
        if (isEdit) {
            setRightTitleView("管理");
            isEdit = false;
            mLine.setVisibility(View.GONE);
            mLlBottom.setVisibility(View.GONE);

            cleanChoose();
            ((ProductPoolAdapter) mAdapter).setIsEditStatus(false);
            mCbAll.setChecked(false);
        } else {
            setRightTitleView("完成");
            isEdit = true;

            mLine.setVisibility(View.VISIBLE);
            mLlBottom.setVisibility(View.VISIBLE);

            ((ProductPoolAdapter) mAdapter).setIsEditStatus(true);
        }
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void getData() {
        super.getData();
        GoodsModel.sendGoodsRequest(TAG, String.valueOf(mPageNum), "20",mEtSearch.getText().toString(), "","", new CustomerJsonCallBack<GoodsModel>() {
            @Override
            public void onRequestError(GoodsModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GoodsModel returnData) {
                hideRefreshView();
                List<GoodsModel.DataBean> infos = returnData.getData();
                if (mAdapter!=null&&infos != null) {
                    if (mPageNum == 0) {
                        if (mCbAll!=null&&mCbAll.isChecked()) {//全选状态，加载更多需要勾选加载的
                            for (GoodsModel.DataBean messageData : infos) {
                                messageData.setChoose(true);
                            }
                            selectNum = infos.size();
                        }

                        mAdapter.setNewData(infos);

                        if (infos.size() <= 0) {
                            mAdapter.isUseEmpty(true);
                        }
                    } else {
                        if (mCbAll!=null&&mCbAll.isChecked()) {//全选状态，加载更多需要勾选加载的
                            for (GoodsModel.DataBean messageData : infos) {
                                messageData.setChoose(true);
                            }
                            selectNum = mAdapter.getData().size() + infos.size();
                        }

                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.stv_search:
                search();
                break;
            case R.id.stv_btn03://删除
                if (selectNum == 0) {
                    showShortToast("请选择需要删除的商品");
                    return;
                }
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(ProductPoolActivity.this, "确定删除所选产品吗？",
                        "注意:需没有此产品未完成的订单,删除后不可恢复,谨慎操作!",
                        "取消", "确定",
                        new CustomerDialogUtils.IDialogListener() {

                            @Override
                            public void onBtn01Click() {

                            }

                            @Override
                            public void onBtn02Click() {
                                onBatchDelete();
                            }
                        });
                break;
            case R.id.stv_btn02://上架
                if (selectNum == 0) {
                    showShortToast("请选择需要上架的商品");
                    return;
                }
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(ProductPoolActivity.this, "确定上架所选产品吗？",
                        "注意:库存为0,信息不完整的商品不能上架!",
                        "取消", "确定",
                        new CustomerDialogUtils.IDialogListener() {

                            @Override
                            public void onBtn01Click() {

                            }

                            @Override
                            public void onBtn02Click() {
                                onBatchGrounding();
                            }
                        });
                break;
            case R.id.stv_btn01://下架
                if (selectNum == 0) {
                    showShortToast("请选择需要下架的商品");
                    return;
                }
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(ProductPoolActivity.this, "确定下架所选产品吗？",
                        "注意:有未完成订单的产品的不能下架!",
                        "取消", "确定",
                        new CustomerDialogUtils.IDialogListener() {

                            @Override
                            public void onBtn01Click() {

                            }

                            @Override
                            public void onBtn02Click() {
                                onBatchLowerShelf();
                            }
                        });
                break;
            case R.id.fl_cb_all:
                if (mCbAll.isChecked()) {//取消全选
                    mCbAll.setChecked(false);
                    cleanChoose();

                    mAdapter.notifyDataSetChanged();
                } else {//全选
                    mCbAll.setChecked(true);
                    allChoose();

                    mAdapter.notifyDataSetChanged();
                }
                break;
        }
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        //判断是否是管理员 以及 商品是否下架 只有下架的商品 才能进入编辑页面
        GoodsModel.DataBean itemData = ((ProductPoolAdapter) adapter).getItem(position);
        if (LoginUtils.isManage()) {
            Intent intent = new Intent(this, UpdateGoodsActivity.class);
            intent.putExtra("GoodsNo", itemData.getGoodsNo());
            intent.putExtra("isChoose", itemData.isChoose());
            intent.putExtra("from", "1");
            startActivityForResult(intent, new OnActivityCallback() {
                @Override
                public void onActivityResult(int resultCode, @Nullable Intent data) {
                    if (resultCode == RESULT_OK) {
                        GoodsModel.DataBean itemDataInside = data.getParcelableExtra("itemData");
                        ((ProductPoolAdapter) adapter).setData(position, itemDataInside);
                    }
                }
            });
        }
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        GoodsModel.DataBean itemData = ((ProductPoolAdapter) adapter).getItem(position);
        switch (view.getId()) {
            case R.id.fl_product_select://选中需要下载的数据
                ImageView ivProductSelect = view.findViewById(R.id.iv_product_select);
                if (itemData.choose) {//取消选中
                    ivProductSelect.setImageResource(R.drawable.ic_product_select_nor);
                    itemData.setChoose(false);
                    selectNum--;
                    //取消全选
                    mCbAll.setChecked(false);
                } else {//选中
                    ivProductSelect.setImageResource(R.drawable.ic_product_select_pre);
                    itemData.setChoose(true);
                    selectNum++;
                    if (selectNum == mAdapter.getData().size()) {
                        //全选
                        mCbAll.setChecked(true);
                    }
                }
                break;
            case R.id.stv_btn03://删除
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(ProductPoolActivity.this, "确定删除所选产品吗？",
                        "注意:需没有此产品未完成的订单,删除后不可恢复,谨慎操作!",
                        "取消", "确定",
                        new CustomerDialogUtils.IDialogListener() {

                            @Override
                            public void onBtn01Click() {

                            }

                            @Override
                            public void onBtn02Click() {
                                onDelete(itemData.getGoodsNo(), position);
                            }
                        });
                break;
            case R.id.stv_btn02://上架
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(ProductPoolActivity.this, "确定上架所选产品吗？",
                        "注意:库存为0,信息不完整的商品不能上架!",
                        "取消", "确定",
                        new CustomerDialogUtils.IDialogListener() {

                            @Override
                            public void onBtn01Click() {

                            }

                            @Override
                            public void onBtn02Click() {
                                onGrounding(itemData, position);
                            }
                        });
                break;
            case R.id.stv_btn01://下架
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(ProductPoolActivity.this, "确定下架所选产品吗？",
                        "注意:有未完成订单的产品的不能下架!",
                        "取消", "确定",
                        new CustomerDialogUtils.IDialogListener() {

                            @Override
                            public void onBtn01Click() {

                            }

                            @Override
                            public void onBtn02Click() {
                                onLowerShelf(itemData, position);
                            }
                        });
                break;
        }
    }

    //删除
    private void onDelete(String id, int position) {
        showWaitDialog();
        BaseModel.sendDelGoodsRequest(TAG, id, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("删除成功");
                mAdapter.remove(position);
                MsgBus.sendAddGoodsRefresh().post("1");
            }
        });
    }

    //上架
    private void onGrounding(GoodsModel.DataBean dataBean, int position) {
        showWaitDialog();
        BaseModel.sendOnOffGoodsRequest(TAG, dataBean.getGoodsNo(), "1", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("上架成功");
                dataBean.setIsOnSale("1");
                mAdapter.notifyItemChanged(position);
                MsgBus.sendAddGoodsRefresh().post("1");
            }
        });
    }

    //下架
    private void onLowerShelf(GoodsModel.DataBean dataBean, int position) {
        showWaitDialog();
        BaseModel.sendOnOffGoodsRequest(TAG, dataBean.getGoodsNo(), "2", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("下架成功");
                dataBean.setIsOnSale("2");
                mAdapter.notifyItemChanged(position);
                MsgBus.sendAddGoodsRefresh().post("1");
            }
        });
    }

    //清除选中
    private void cleanChoose() {
        for (GoodsModel.DataBean messageData : ((ProductPoolAdapter) mAdapter).getData()) {
            messageData.setChoose(false);
        }

        selectNum = 0;
    }

    //全选
    private void allChoose() {
        for (GoodsModel.DataBean messageData : ((ProductPoolAdapter) mAdapter).getData()) {
            messageData.setChoose(true);
        }

        selectNum = mAdapter.getData().size();
    }

    //批量删除
    private void onBatchDelete() {
        List<GoodsModel.DataBean> datas = ((ProductPoolAdapter) mAdapter).getData();

        String deleteIds = "";
        for (int i = 0, size = datas.size(); i < size; i++) {
            GoodsModel.DataBean data = datas.get(i);
            if (data.choose && !"1".equals(data.getIsOnSale())) {
                deleteIds += data.getGoodsNo() + ",";
            }
        }

        if (EmptyUtils.isEmpty(deleteIds)) {
            showShortToast("请选择已下架的商品删除");
            return;
        }

        deleteIds = deleteIds.substring(0, deleteIds.length() - 1);

        Log.e("666666", "deleteIds===" + deleteIds);

        showWaitDialog();
        BaseModel.sendDelGoodsRequest(TAG, deleteIds, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("删除成功");
                showWaitDialog();
                onRefresh(mRefreshLayout);
                MsgBus.sendAddGoodsRefresh().post("1");
            }
        });
    }

    //批量上架
    private void onBatchGrounding() {
        List<GoodsModel.DataBean> datas = ((ProductPoolAdapter) mAdapter).getData();

        String groundingIds = "";
        for (int i = 0, size = datas.size(); i < size; i++) {
            GoodsModel.DataBean data = datas.get(i);
            if (data.choose && !"1".equals(data.getIsOnSale()) && data.getInventoryQuantity()!=0) {
                groundingIds += data.getGoodsNo() + ",";
            }
        }

        if (EmptyUtils.isEmpty(groundingIds)) {
            showShortToast("请选择已下架的商品上架");
            return;
        }
        groundingIds = groundingIds.substring(0, groundingIds.length() - 1);
        showWaitDialog();
        BaseModel.sendOnOffGoodsRequest(TAG, groundingIds, "1", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("上架成功");
                showWaitDialog();
                onRefresh(mRefreshLayout);
                MsgBus.sendAddGoodsRefresh().post("1");
            }
        });
    }

    //批量下架
    private void onBatchLowerShelf() {
        List<GoodsModel.DataBean> datas = ((ProductPoolAdapter) mAdapter).getData();

        String lowerShelfIds = "";
        for (int i = 0, size = datas.size(); i < size; i++) {
            GoodsModel.DataBean data = datas.get(i);
            if (data.choose && "1".equals(data.getIsOnSale())) {
                lowerShelfIds += data.getGoodsNo() + ",";
            }
        }

        if (EmptyUtils.isEmpty(lowerShelfIds)) {
            showShortToast("请选择已上架的商品下架");
            return;
        }
        lowerShelfIds = lowerShelfIds.substring(0, lowerShelfIds.length() - 1);
        Log.e("666666", "lowerShelfIds===" + lowerShelfIds);

        showWaitDialog();
        BaseModel.sendOnOffGoodsRequest(TAG, lowerShelfIds, "2", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("下架成功");
                showWaitDialog();
                onRefresh(mRefreshLayout);
                MsgBus.sendAddGoodsRefresh().post("1");
            }
        });
    }

    //搜索
    private void search() {
        if (EmptyUtils.isEmpty(mEtSearch.getText().toString())) {
            showShortToast("请输入药品名称");
            return;
        }
        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, ProductPoolActivity.class);
        context.startActivity(intent);
    }
}
