package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSON;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.LogUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.PriceInfoAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PayTypeModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PriceInfoModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.QueryRechargePayModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.RechargeModel_mercenary;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.OrderDetailActivity_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.PaySuccessActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OrderPayOnlineModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.QueryOrderPayOnlineModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.widget.pop.SelectPayTypePop;
import com.kuaiqian.fusedpay.entity.FusedPayRequest;
import com.kuaiqian.fusedpay.entity.FusedPayResult;
import com.kuaiqian.fusedpay.sdk.FusedPayApiFactory;
import com.kuaiqian.fusedpay.sdk.IFusedPayApi;
import com.kuaiqian.fusedpay.sdk.IFusedPayEventHandler;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：充值页面--平台抽佣模式
 * ================================================
 */
public class RechargeActivity_mercenary extends BaseActivity implements BaseQuickAdapter.OnItemClickListener, SelectPayTypePop.IOptionListener, IFusedPayEventHandler {
    private TextView mTvTitleDescribe;
    private CustomeRecyclerView mCrlPriceInfo;
    private LinearLayout mLlPayType;
    private TextView mTvPayType;
    private TextView mTvPrice;
    private ShapeTextView mTvRecharge;

    private String rechargeAccount;
    private PriceInfoAdapter priceInfoAdapter;
    private PriceInfoModel currentPriceInfoModel;//记录当前选择的价格条目
    private ShapeTextView currentView;
    private SelectPayTypePop selectPayTypePop;//选择支付方式弹框
    private String serialNumber;
    private boolean isPaying=false;//当前操作是否是正在支付

    @Override
    public int getLayoutId() {
        return R.layout.activity_recharge_zhyfmanage;
    }

    @Override
    public void initView() {
        super.initView();
        mTvTitleDescribe = findViewById(R.id.tv_title_describe);
        mCrlPriceInfo = findViewById(R.id.crl_price_info);
        mLlPayType = findViewById(R.id.ll_pay_type);
        mTvPayType = findViewById(R.id.tv_pay_type);
        mTvPrice = findViewById(R.id.tv_price);
        mTvRecharge = findViewById(R.id.tv_recharge);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(false, 0.2f)
                .titleBar(mTitleBar);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        rechargeAccount = getIntent().getStringExtra("rechargeAccount");
        super.initData();
        mTvTitleDescribe.setText("选择充值金额");
        setRightTitleView("充值记录");

        //模拟充值金额列表的数据
        mCrlPriceInfo.setHasFixedSize(true);
        mCrlPriceInfo.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
        priceInfoAdapter = new PriceInfoAdapter();
        mCrlPriceInfo.setAdapter(priceInfoAdapter);

        ArrayList<PriceInfoModel> priceInfoModels = new ArrayList<>();
        PriceInfoModel priceInfoModel01 = new PriceInfoModel("1", "100.00", false);
        PriceInfoModel priceInfoModel02 = new PriceInfoModel("2", "300.00", false);
        PriceInfoModel priceInfoModel03 = new PriceInfoModel("3", "500.00", false);
        PriceInfoModel priceInfoModel04 = new PriceInfoModel("4", "1000.00", false);
        PriceInfoModel priceInfoModel05 = new PriceInfoModel("5", "3000.00", false);
        PriceInfoModel priceInfoModel06 = new PriceInfoModel("6", "5000.00", false);
        PriceInfoModel priceInfoModel07 = new PriceInfoModel("7", "10000.00", false);
        PriceInfoModel priceInfoModel08 = new PriceInfoModel("8", "20000.00", false);
        priceInfoModels.add(priceInfoModel01);
        priceInfoModels.add(priceInfoModel02);
        priceInfoModels.add(priceInfoModel03);
        priceInfoModels.add(priceInfoModel04);
        priceInfoModels.add(priceInfoModel05);
        priceInfoModels.add(priceInfoModel06);
        priceInfoModels.add(priceInfoModel07);
        priceInfoModels.add(priceInfoModel08);

        priceInfoAdapter.setNewData(priceInfoModels);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        priceInfoAdapter.setOnItemClickListener(this);
        mLlPayType.setOnClickListener(this);
        mTvRecharge.setOnClickListener(this);
    }

    @Override
    public void rightTitleViewClick() {
        RechargeRecordActivity_ZhyfManage.newIntance(this);
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        PriceInfoModel itemData = priceInfoAdapter.getItem(i);
        if (!itemData.isChoose()) {
            ShapeTextView mChildPrice = view.findViewById(R.id.tv_price);
            mChildPrice.getShapeDrawableBuilder().setStrokeColor(getResources().getColor(R.color.color_ff0202))
                    .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1)).intoBackground();
            itemData.setChoose(true);

            if (currentPriceInfoModel != null) {
                currentPriceInfoModel.setChoose(false);
            }

            if (currentView != null) {
                currentView.getShapeDrawableBuilder().setStrokeColor(getResources().getColor(R.color.bar_transparent))
                        .setStrokeWidth(0).intoBackground();
            }

            currentPriceInfoModel = itemData;
            currentView = mChildPrice;

            //选中之后 设置价格
            mTvPrice.setText(currentPriceInfoModel.getPrice());
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.ll_pay_type://选择支付方式
                if (selectPayTypePop == null) {
                    selectPayTypePop = new SelectPayTypePop(RechargeActivity_mercenary.this,
                            CommonlyUsedDataUtils.getInstance().getSelectPayTypeData(), this);
                }

                selectPayTypePop.showPopupWindow();
                break;
            case R.id.tv_recharge://立即充值
                onRecharge();
                break;
        }
    }

    @Override
    public void onItemClick(PayTypeModel payTypeModel) {
        mTvPayType.setTag(payTypeModel.getId());
        mTvPayType.setText(EmptyUtils.strEmpty(payTypeModel.getText()));
    }

    //立即充值
    private void onRecharge() {
        if (currentPriceInfoModel == null) {
            showShortToast("请选择充值金额");
            return;
        }

        if (null == mTvPayType.getTag() || EmptyUtils.isEmpty(String.valueOf(mTvPayType.getTag()))) {
            showShortToast("请选择支付方式");
            return;
        }

        // 2022-10-13 请求接口生成支付信息
        showWaitDialog();
        RechargeModel_mercenary.sendRechargeRequest_mercenary(TAG, String.valueOf(mTvPayType.getTag()), rechargeAccount, "0",
                currentPriceInfoModel.getPrice(), MechanismInfoUtils.getEntryMode(), new CustomerJsonCallBack<RechargeModel_mercenary>() {
                    @Override
                    public void onRequestError(RechargeModel_mercenary returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(RechargeModel_mercenary returnData) {
                        hideWaitDialog();
                        serialNumber=returnData.getSerialNumber();
                        RechargeModel_mercenary.DataBean payData = returnData.getData();
                        if (payData != null && payData.getMpayInfo() != null) {
                            //获取支付信息成功
                            switch (String.valueOf(mTvPayType.getTag())) {
                                case "1"://微信
                                    invokeFusedPaySDK("4", JSON.toJSONString(payData.getMpayInfo()));
                                    break;
                                case "2"://支付宝
                                    invokeFusedPaySDK("7", JSON.toJSONString(payData.getMpayInfo()));
                                    break;
                                case "3"://云闪付
                                    invokeFusedPaySDK("5", JSON.toJSONString(payData.getMpayInfo()));
                                    break;
                            }
                        }
                    }
                });
    }

    /**
     * 调起聚合支付sdk
     *
     * @param platform 支付平台 :1 --飞凡通支付   2 --支付宝支付     3 --微信支付  4.微信支付定制版 5.云闪付 7.支付宝支付定制版
     * @param mpayInfo 移动支付的信息
     */
    private void invokeFusedPaySDK(String platform, String mpayInfo) {
        FusedPayRequest payRequest = new FusedPayRequest();
        payRequest.setPlatform(platform);
        payRequest.setMpayInfo(mpayInfo);
        if ("5".equals(platform)) {
            payRequest.setUnionPayTestEnv(false);
        }
        // CallBackSchemeId可以自定义，自定义的结果页面需实现IKuaiqianEventHandler接口
        payRequest.setCallbackSchemeId("com.jrdz.zhyb_android.ui.zhyf_manage.activity.RechargeActivity_mercenary");
        IFusedPayApi payApi = FusedPayApiFactory.createPayApi(RechargeActivity_mercenary.this);
        payApi.pay(payRequest);
        isPaying=true;
    }

    //==============================================支付方法回调================================================
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        isPaying=false;
        IFusedPayApi api = FusedPayApiFactory.createPayApi(this);
        api.handleIntent(getIntent(), this);
    }

    @Override
    public void onResponse(FusedPayResult fusedPayResult) {
        LogUtils.e("onResponse", "支付结果：" + fusedPayResult);
        String payResultCode = fusedPayResult.getResultStatus();
        String payResultMessage = fusedPayResult.getResultMessage();

        if ("00".equals(payResultCode)) {//00支付返回成功
            //支付成功之后
            RechargeSuccessActivity.newIntance(RechargeActivity_mercenary.this,serialNumber, "充值成功!", "返回我的账户",true);
            goFinish();
        } else {
            showTipDialog(payResultMessage);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isPaying){
            showWaitDialog();
            getOrderPayOnlineStatus();
        }
    }

    //获取订单支付状态
    private void getOrderPayOnlineStatus() {
        QueryRechargePayModel.sendQueryRechargePayRequest(TAG, serialNumber, new CustomerJsonCallBack<QueryRechargePayModel>() {
            @Override
            public void onRequestError(QueryRechargePayModel returnData, String msg) {
                hideWaitDialog();
                isPaying=false;
            }

            @Override
            public void onRequestSuccess(QueryRechargePayModel returnData) {
                hideWaitDialog();
//                IsPaid=1或者IsPaid=2
                QueryRechargePayModel.DataBean data = returnData.getData();
                if (data != null) {
                    if ("1".equals(data.getIsPaid()) || "2".equals(data.getIsPaid())) {//支付成功
                        //支付成功之后
                        RechargeSuccessActivity.newIntance(RechargeActivity_mercenary.this,serialNumber, "充值成功!", "返回我的账户",false);
                        goFinish();
                    } else {
                        showTipDialog("充值失败!");
                    }
                }

                isPaying=false;
            }
        });
    }

    //==============================================支付方法回调================================================

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (selectPayTypePop != null) {
            selectPayTypePop.onClean();
        }
        currentPriceInfoModel = null;
        currentView = null;
    }

    public static void newIntance(Context context, String rechargeAccount) {
        Intent intent = new Intent(context, RechargeActivity_mercenary.class);
        intent.putExtra("rechargeAccount", rechargeAccount);
        context.startActivity(intent);
    }
}
