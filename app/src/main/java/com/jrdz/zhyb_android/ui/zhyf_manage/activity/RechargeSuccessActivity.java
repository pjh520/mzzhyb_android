package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.QueryRechargePayModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-08
 * 描    述： 充值成功
 * ================================================
 */
public class RechargeSuccessActivity extends BaseActivity {
    private TextView mTvResult;
    private ShapeTextView mTvBackMyAccount;

    private String serialNumber,result,btnText,from;
    private boolean needRequest;
    private Handler handler;
    //是否调用接口验证支付转态
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            getOrderPayOnlineStatus();
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.activity_recharge_success;
    }

    @Override
    public void initView() {
        super.initView();
        mTvResult = findViewById(R.id.tv_result);
        mTvBackMyAccount = findViewById(R.id.tv_back_my_account);
    }

    @Override
    public void initData() {
        serialNumber=getIntent().getStringExtra("serialNumber");
        result=getIntent().getStringExtra("result");
        btnText=getIntent().getStringExtra("btnText");
        from=getIntent().getStringExtra("from");
        needRequest=getIntent().getBooleanExtra("needRequest",true);
        super.initData();

        if (needRequest){
            handler = new Handler(getMainLooper());
            showWaitDialog("支付结果查询中...");
            getOrderPayOnlineStatus();
        }else {
            setSuccessUi();
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvBackMyAccount.setOnClickListener(this);
    }

    //获取订单支付状态
    private void getOrderPayOnlineStatus() {
        QueryRechargePayModel.sendQueryRechargePayRequest(TAG, serialNumber, new CustomerJsonCallBack<QueryRechargePayModel>() {
            @Override
            public void onRequestError(QueryRechargePayModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(QueryRechargePayModel returnData) {
//                IsPaid=1或者IsPaid=2
                QueryRechargePayModel.DataBean data = returnData.getData();
                if (data != null) {
                    if ("1".equals(data.getIsPaid()) || "2".equals(data.getIsPaid())) {//支付成功
                        hideWaitDialog();
                        setSuccessUi();
                    } else {
                        handler.postDelayed(mRunnable, 2000);
                    }
                }
            }
        });
    }

    //设置成功的UI
    private void setSuccessUi() {
        mTvResult.setText(EmptyUtils.strEmpty(result));
        mTvBackMyAccount.setText(EmptyUtils.strEmpty(btnText));
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_back_my_account://返回我的账户
                if (!EmptyUtils.isEmpty(from)&&"2".equals(from)){
                    SmartPhaMainActivity.newIntance(RechargeSuccessActivity.this,0);
                }
                goFinish();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
    }

    public static void newIntance(Context context,String serialNumber,String result,String btnText,boolean needRequest) {
        Intent intent = new Intent(context, RechargeSuccessActivity.class);
        intent.putExtra("serialNumber", serialNumber);
        intent.putExtra("result", result);
        intent.putExtra("btnText", btnText);
        intent.putExtra("needRequest", needRequest);
        context.startActivity(intent);
    }

    public static void newIntance(Context context,String serialNumber,String result,String btnText,String from,boolean needRequest) {
        Intent intent = new Intent(context, RechargeSuccessActivity.class);
        intent.putExtra("serialNumber", serialNumber);
        intent.putExtra("result", result);
        intent.putExtra("btnText", btnText);
        intent.putExtra("from", from);
        intent.putExtra("needRequest", needRequest);
        context.startActivity(intent);
    }
}
