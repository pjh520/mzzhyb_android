package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.RefundProgressModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.RefundProgressDataModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.RefundProgressTopItemModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-11
 * 描    述：退款详情 页面
 * ================================================
 */
public class RefundProgressActivity extends BaseActivity {
    private TextView mTvStatus;
    private TextView mTvReceiptTime;
    private ShapeLinearLayout mSllTopContain;
    private ShapeLinearLayout mSllRefundContain;
    private LinearLayout mLlOrderListContain;
    private TextView mTvTotalNum;
    private TextView mTvTotalPrice;
    private TextView mTvOrderId,mTvRefundId,mTvRefundNo;
    private ShapeLinearLayout mSllPreSettl;
    private TextView mTvPsnNo;
    private TextView mTvPsnName;
    private TextView mTvCertType;
    private TextView mTvCertNo;
    private TextView mTvInsutypeName;
    private TextView mTvFeedetlSn;
    private TextView mTvMedfeeSumamt;
    private TextView mTvFundPaySumamt;
    private TextView mTvAcctPay;
    private TextView mTvPsnCashPay;
    private TextView mTvMoney,mTvOnlinePrice;

    private String orderNo,onlineAmount,settlementStatus;
    ArrayList<RefundProgressTopItemModel> refundProgressTopItemModels=new ArrayList<>();
    ArrayList<RefundProgressDataModel> refundProgressDataModels=new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_refund_progress;
    }

    @Override
    public void initView() {
        super.initView();
        mTvStatus = findViewById(R.id.tv_status);
        mTvReceiptTime = findViewById(R.id.tv_receipt_time);
        mSllTopContain=findViewById(R.id.sll_top_contain);
        mSllRefundContain = findViewById(R.id.sll_refund_contain);
        mLlOrderListContain = findViewById(R.id.ll_order_list_contain);
        mTvTotalNum = findViewById(R.id.tv_total_num);
        mTvTotalPrice = findViewById(R.id.tv_total_price);
        mTvOrderId = findViewById(R.id.tv_order_id);
        mTvRefundId = findViewById(R.id.tv_refund_id);
        mTvRefundNo = findViewById(R.id.tv_refund_no);

        mSllPreSettl = findViewById(R.id.sll_pre_settl);
        mTvPsnNo = findViewById(R.id.tv_psn_no);
        mTvPsnName = findViewById(R.id.tv_psn_name);
        mTvCertType = findViewById(R.id.tv_cert_type);
        mTvCertNo = findViewById(R.id.tv_cert_no);
        mTvInsutypeName = findViewById(R.id.tv_insutype_name);
        mTvFeedetlSn = findViewById(R.id.tv_feedetl_sn);
        mTvMedfeeSumamt = findViewById(R.id.tv_medfee_sumamt);
        mTvFundPaySumamt = findViewById(R.id.tv_fund_pay_sumamt);
        mTvAcctPay = findViewById(R.id.tv_acct_pay);
        mTvPsnCashPay = findViewById(R.id.tv_psn_cash_pay);
        mTvMoney = findViewById(R.id.tv_money);

        mTvOnlinePrice = findViewById(R.id.tv_online_price);
    }

    @Override
    public void initData() {
        orderNo=getIntent().getStringExtra("orderNo");
        onlineAmount=getIntent().getStringExtra("onlineAmount");
        settlementStatus=getIntent().getStringExtra("settlementStatus");
        super.initData();

        showWaitDialog();
        getDetailData();

        mTvOnlinePrice.setText(EmptyUtils.strEmpty(onlineAmount));
    }

    @Override
    public void initEvent() {
        super.initEvent();
    }

    //获取页面数据
    private void getDetailData() {
        // 2022-10-11 模拟获取页面数据
        RefundProgressModel.sendStoreOrderRefundProgressRequest(TAG, orderNo, new CustomerJsonCallBack<RefundProgressModel>() {
            @Override
            public void onRequestError(RefundProgressModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(RefundProgressModel returnData) {
                hideWaitDialog();
                refundProgressDataModels.clear();
                //设置页面头部退款数据
                refundProgressTopItemModels.clear();
                RefundProgressModel.DataBean detailData = returnData.getData();
                if (detailData!=null){
                    mTvReceiptTime.setText(EmptyUtils.strEmpty(detailData.getEstimateContent()));

                    refundProgressDataModels.add(new RefundProgressDataModel("发起取消订单申请","",detailData.getCancelTime()));
                    if ("1".equals(detailData.getIsReturnMedicare())){
                        refundProgressTopItemModels.add(new RefundProgressTopItemModel("退回至医保个人账户",detailData.getReturnMedicareAmount()));

                        refundProgressDataModels.add(new RefundProgressDataModel(detailData.getStepTitle1(),detailData.getStepContent1(),detailData.getStepTime1()));
                    }

                    if ("1".equals(detailData.getIsReturnOriginalAccount())){
                        refundProgressTopItemModels.add(new RefundProgressTopItemModel("退回至原支付账户",detailData.getReturnOriginalAmount()));

                        refundProgressDataModels.add(new RefundProgressDataModel(detailData.getStepTitle2(),detailData.getStepContent2(),detailData.getStepTime2()));
                        refundProgressDataModels.add(new RefundProgressDataModel(detailData.getStepTitle3(),detailData.getStepContent3(),detailData.getStepTime3()));
                    }

                    if ("1".equals(detailData.getIsReturnEnterpriseFund())){
                        refundProgressTopItemModels.add(new RefundProgressTopItemModel("退回至原授信额度",detailData.getReturnEnterpriseFundAmount()));

                        refundProgressDataModels.add(new RefundProgressDataModel(detailData.getStepTitle4(),detailData.getStepContent4(),detailData.getStepTime4()));
                    }

                    setTopContain();
                    setRefundProgressInfo();
                    //设置商品数据
                    setProductInfo(detailData.getOrderGoods());

                    mTvOrderId.setText(EmptyUtils.strEmpty(detailData.getOrderNo()));
                    mTvRefundId.setText(EmptyUtils.strEmpty(detailData.getOrderRefundNo()));
                    mTvRefundNo.setText(EmptyUtils.strEmpty(detailData.getOrderRefundSerialNo()));

                    //2022-10-10 需要区分 是否自费订单
                    if ("0".equals(settlementStatus)) {//是自费订单
                        mSllPreSettl.setVisibility(View.GONE);
                    } else {//医保报销订单
                        //获取证件类型
                        String psnCertTypeText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("PSN_CERT_TYPE", detailData.getMdtrt_cert_type());
                        //获取险种类型
                        String insutypeText = CommonlyUsedDataUtils.getInstance().getDataDicLabel("INSUTYPE", detailData.getInsutype());
                        mSllPreSettl.setVisibility(View.VISIBLE);
                        mTvPsnNo.setText(EmptyUtils.strEmpty(detailData.getPsn_no()));
                        mTvPsnName.setText(EmptyUtils.isEmpty(detailData.getPsn_name())?"": StringUtils.encryptionName(detailData.getPsn_name()));
                        mTvCertType.setText(psnCertTypeText);
                        mTvCertNo.setText(EmptyUtils.isEmpty(detailData.getMdtrt_cert_no())?"":StringUtils.encryptionIDCard(detailData.getMdtrt_cert_no()));
                        mTvInsutypeName.setText(insutypeText);
                        mTvFeedetlSn.setText(EmptyUtils.strEmpty(detailData.getSetl_id()));
                        mTvMedfeeSumamt.setText(EmptyUtils.strEmpty(detailData.getMedfee_sumamt()));
                        mTvFundPaySumamt.setText(EmptyUtils.strEmpty(detailData.getFund_pay_sumamt()));
                        mTvAcctPay.setText(EmptyUtils.strEmpty(detailData.getAcct_pay()));
                        mTvPsnCashPay.setText(EmptyUtils.strEmpty(detailData.getPsn_cash_pay()));
                        mTvMoney.setText("****元");
                    }
                }
            }
        });
    }

    //设置页面头部退款数据
    private void setTopContain() {
        mSllTopContain.removeAllViews();
        int size=refundProgressTopItemModels.size();
        for (int i = size - 1; i >= 0; i--) {
            RefundProgressTopItemModel refundProgressTopItemModel=refundProgressTopItemModels.get(i);
            View view=LayoutInflater.from(this).inflate(R.layout.layout_refund_progress_item,mSllTopContain,false);
            TextView mTvPay = view.findViewById(R.id.tv_pay);
            TextView mTvDescribe = view.findViewById(R.id.tv_describe);
            TextView mTvPrice = view.findViewById(R.id.tv_price);

            if (size==1){
                mTvPay.setText("退款金额");
            }else {
                switch (i){
                    case 0:
                        mTvPay.setText("一次退款金额");
                        break;
                    case 1:
                        mTvPay.setText("二次退款金额");
                        break;
                    case 2:
                        mTvPay.setText("三次退款金额");
                        break;
                }
            }

            mTvDescribe.setText(EmptyUtils.strEmpty(refundProgressTopItemModel.getDescribe()));
            mTvPrice.setText(EmptyUtils.strEmpty(refundProgressTopItemModel.getPrice()));

            mSllTopContain.addView(view);
        }
    }

    //设置页面退款流程数据
    private void setRefundProgressInfo() {
        mSllRefundContain.removeAllViews();
        int size=refundProgressDataModels.size();
        for (int i = size - 1; i >= 0; i--) {
            RefundProgressDataModel refundInfo = refundProgressDataModels.get(i);

            View view= LayoutInflater.from(this).inflate(R.layout.layout_refund_timeline_item, mSllRefundContain,false);
            ImageView iv=view.findViewById(R.id.iv);
            View line=view.findViewById(R.id.line);

            TextView tvTitle=view.findViewById(R.id.tv_title);
            TextView tvContent=view.findViewById(R.id.tv_content);
            TextView tvTime=view.findViewById(R.id.tv_time);

            if (i==size - 1){
                iv.setImageResource(R.drawable.ic_stmt_press);

                tvTitle.setTextColor(getResources().getColor(R.color.color_333333));
                tvContent.setTextColor(getResources().getColor(R.color.color_333333));
                tvTime.setTextColor(getResources().getColor(R.color.color_333333));
            }else {
                iv.setImageResource(R.drawable.ic_list_weixuan_bg);

                tvTitle.setTextColor(getResources().getColor(R.color.txt_color_999));
                tvContent.setTextColor(getResources().getColor(R.color.txt_color_999));
                tvTime.setTextColor(getResources().getColor(R.color.txt_color_999));
            }

            if (i==0){
                line.setVisibility(View.GONE);
            }else {
                line.setVisibility(View.VISIBLE);
            }

            tvTitle.setText(EmptyUtils.strEmpty(refundInfo.getStatus()));
            if (EmptyUtils.isEmpty(refundInfo.getContent())){
                tvContent.setVisibility(View.GONE);
            }else {
                tvContent.setVisibility(View.VISIBLE);
                tvContent.setText(refundInfo.getContent());
            }

            tvTime.setText(EmptyUtils.strEmpty(refundInfo.getDate()));

            mSllRefundContain.addView(view);
        }
    }
    //设置页面退款信息数据
    private void setProductInfo(List<RefundProgressModel.DataBean.OrderGoodsBean> orderGoods) {
        if (orderGoods==null||orderGoods.isEmpty()){
            return;
        }
        mLlOrderListContain.removeAllViews();

        String totalPrice = "0";
        int totalNum = 0;
        for (int i=0,size=orderGoods.size();i<size;i++){
            RefundProgressModel.DataBean.OrderGoodsBean itemData = orderGoods.get(i);
            totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(itemData.getPrice())
                    .multiply(new BigDecimal(String.valueOf(itemData.getGoodsNum())))).toPlainString();
            totalNum += itemData.getGoodsNum();

            View view= LayoutInflater.from(this).inflate(R.layout.layout_orderlist_product_item, mLlOrderListContain,false);
            ImageView ivPic = view.findViewById(R.id.iv_pic);
            ShapeTextView stvOtc = view.findViewById(R.id.stv_otc);
            TextView tvTitle = view.findViewById(R.id.tv_title);
            TextView tvEstimatePrice = view.findViewById(R.id.tv_estimate_price);
            TextView tvRealPrice = view.findViewById(R.id.tv_real_price);
            View lineItem = view.findViewById(R.id.line_item);

            GlideUtils.loadImg(itemData.getBannerAccessoryUrl1(), ivPic, R.drawable.ic_placeholder_bg, new RoundedCornersTransformation(getResources().getDimensionPixelSize(R.dimen.dp_16), 0));
            //判断是否是处方药
            if ("1".equals(itemData.getItemType())) {
                stvOtc.setText("OTC");
                stvOtc.setVisibility(View.VISIBLE);
            } else if ("2".equals(itemData.getItemType())) {
                stvOtc.setText("处方药");
                stvOtc.setVisibility(View.VISIBLE);
            } else {
                stvOtc.setVisibility(View.GONE);
            }

            tvTitle.setText(EmptyUtils.strEmpty(itemData.getGoodsName()));
            tvEstimatePrice.setText(EmptyUtils.strEmpty(itemData.getPrice()));
            tvRealPrice.setText("x" + itemData.getGoodsNum());

            //2022-10-10 最后一个item的分割线需要隐藏
            if (i==1){
                lineItem.setVisibility(View.GONE);
            }else {
                lineItem.setVisibility(View.VISIBLE);
            }
            mLlOrderListContain.addView(view);
        }

        mTvTotalNum.setText("共" + totalNum + "件商品");
        mTvTotalPrice.setText(totalPrice);
    }

    public static void newIntance(Context context,String orderNo,String onlineAmount,String settlementStatus) {
        Intent intent = new Intent(context, RefundProgressActivity.class);
        intent.putExtra("orderNo", orderNo);
        intent.putExtra("onlineAmount", onlineAmount);
        intent.putExtra("settlementStatus", settlementStatus);
        context.startActivity(intent);
    }
}
