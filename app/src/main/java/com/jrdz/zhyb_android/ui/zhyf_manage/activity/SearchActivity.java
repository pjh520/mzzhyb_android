package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSON;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.JustifyContent;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.ui.mine.activity.HwScanActivity;
import com.jrdz.zhyb_android.ui.mine.activity.ZbarScanActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.SearchHistoryAdapter;
import com.jrdz.zhyb_android.utils.MMKVUtils;
import com.jrdz.zhyb_android.widget.MyFlexboxLayoutManager;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-09
 * 描    述：搜索页面
 * ================================================
 */
public class SearchActivity extends BaseActivity {
    private LinearLayout mLlTitle;
    private FrameLayout mFlClose;
    protected AppCompatEditText mEtSearch;
    private FrameLayout mFlClean;
    private FrameLayout mFlScan;
    private TextView mTvSearch;
    private FrameLayout mFlCleanHistory;
    protected CustomeRecyclerView mCrvHistory;
    protected SearchHistoryAdapter searchHistoryAdapter;

    protected CustomerDialogUtils customerDialogUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_search;
    }

    @Override
    public void initView() {
        super.initView();
        mLlTitle = findViewById(R.id.ll_title);
        mFlClose = findViewById(R.id.fl_close);
        mEtSearch = findViewById(R.id.et_search);
        mFlClean = findViewById(R.id.fl_clean);
        mFlScan = findViewById(R.id.fl_scan);
        mTvSearch = findViewById(R.id.tv_search);
        mFlCleanHistory = findViewById(R.id.fl_clean_history);
        mCrvHistory = findViewById(R.id.crv_history);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mLlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        super.initData();

        //初始化历史记录列表
        mCrvHistory.setHasFixedSize(true);
        mCrvHistory.setLayoutManager(getLayoutManager());
        searchHistoryAdapter = new SearchHistoryAdapter();
        mCrvHistory.setAdapter(searchHistoryAdapter);

        getHistoryData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    search();
                    return true;
                }
                return false;
            }
        });
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    mFlClean.setVisibility(View.GONE);
                } else {
                    mFlClean.setVisibility(View.VISIBLE);
                }
            }
        });
        mFlClose.setOnClickListener(this);
        mFlClean.setOnClickListener(this);
        mFlScan.setOnClickListener(this);
        mFlCleanHistory.setOnClickListener(this);
        mTvSearch.setOnClickListener(this);

        searchHistoryClick();
    }

    //搜索历史item点击
    protected void searchHistoryClick() {
        //历史记录 item点击事件
        searchHistoryAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                SearchResultActivity.newIntance(SearchActivity.this,searchHistoryAdapter.getItem(i),"");
            }
        });
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        MyFlexboxLayoutManager flexboxLayoutManager = new MyFlexboxLayoutManager(this);
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        flexboxLayoutManager.setFlexWrap(FlexWrap.WRAP);
        flexboxLayoutManager.setJustifyContent(JustifyContent.FLEX_START);

        return flexboxLayoutManager;
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close://关闭
                goFinish();
                break;
            case R.id.fl_clean://清除搜索数据
                mEtSearch.setText("");
                break;
            case R.id.fl_scan://扫码
                if (Constants.Configure.IS_POS) {
                    goPosScan();
                } else {
                    goHwScan();
                }
                break;
            case R.id.fl_clean_history://清空历史记录
                cleanHistory();
                break;
            case R.id.tv_search://搜索
                search();
                break;
        }
    }
    //使用pos机自带的扫码
    private void goPosScan() {
        startActivityForResult(new Intent(SearchActivity.this, ZbarScanActivity.class), new BaseActivity.OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == -1) {
                    String scanResult = data.getStringExtra("scanResult");
                    scanOrgan(scanResult);
                }
            }
        });
    }

    //手机端使用华为扫码
    private void goHwScan() {
        startActivityForResult(new Intent(SearchActivity.this, HwScanActivity.class), new BaseActivity.OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == -1) {
                    String scanResult = data.getStringExtra("scanResult");
                    scanOrgan(scanResult);
                }
            }
        });
    }

    //清空历史记录
    public void cleanHistory() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(SearchActivity.this, "提示", "确定清除所有历史搜索记录？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                MMKVUtils.delete("zhyf_search_history");
                searchHistoryAdapter.setNewData(new ArrayList<>());
            }
        });
    }

    //搜索
    public void search() {
        if (EmptyUtils.isEmpty(mEtSearch.getText().toString())){
            showShortToast("请输入药品名称");
            return;
        }
        saveHistoryData(mEtSearch.getText().toString());

        SearchResultActivity.newIntance(SearchActivity.this,mEtSearch.getText().toString(),"");
    }

    //保存搜索数据
    private void saveHistoryData(String searchText){
        //获取历史搜索本地记录
        List<String> shopHistoryDatas=JSON.parseArray(MMKVUtils.getString("zhyf_search_history"),String.class);
        if (shopHistoryDatas==null){
            shopHistoryDatas=new ArrayList<>();
        }else {
            if (shopHistoryDatas.contains(searchText)){
                shopHistoryDatas.remove(searchText);
            }

            if (shopHistoryDatas.size()>=10){
                shopHistoryDatas.remove(9);
            }
        }
        shopHistoryDatas.add(0,searchText);
        MMKVUtils.putString("zhyf_search_history",JSON.toJSONString(shopHistoryDatas));
        getHistoryData();
    }

    //获取历史搜索记录
    protected void getHistoryData() {
        //获取历史搜索本地记录
        List<String> shopHistoryDatas=JSON.parseArray(MMKVUtils.getString("zhyf_search_history"),String.class);
        searchHistoryAdapter.setNewData(shopHistoryDatas);
    }

    //扫机构
    public void scanOrgan(String barCode) {
        SearchResultActivity.newIntance(SearchActivity.this,mEtSearch.getText().toString(),barCode);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, SearchActivity.class);
        context.startActivity(intent);
    }
}
