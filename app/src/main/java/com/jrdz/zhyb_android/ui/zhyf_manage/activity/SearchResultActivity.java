package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.RxTool;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.ProductPoolAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.SearchResultAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.SearchResultModel;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.GoodDetailActivity;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-09
 * 描    述：搜索结果页面
 * ================================================
 */
public class SearchResultActivity extends BaseRecyclerViewActivity {
    private LinearLayout mLlTitle;
    private FrameLayout mFlClose;
    protected AppCompatEditText mEtSearch;
    private FrameLayout mFlClean;
    private TextView mTvSearch;

    protected String searchText,barCode;

    @Override
    public int getLayoutId() {
        return R.layout.activity_search_result;
    }

    @Override
    public void initView() {
        super.initView();
        mLlTitle = findViewById(R.id.ll_title);
        mFlClose = findViewById(R.id.fl_close);
        mEtSearch = findViewById(R.id.et_search);
        mFlClean = findViewById(R.id.fl_clean);
        mTvSearch = findViewById(R.id.tv_search);
    }

    @Override
    public void initAdapter() {
        mAdapter = new SearchResultAdapter();
    }

    @Override
    protected void initImmersionBar() {
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mLlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        searchText = getIntent().getStringExtra("searchText");
        barCode = getIntent().getStringExtra("barCode");
        super.initData();
        mEtSearch.setText(searchText);
        if (EmptyUtils.isEmpty(searchText)){
            mFlClean.setVisibility(View.GONE);
        }else {
            mFlClean.setVisibility(View.VISIBLE);
        }
        RxTool.setEditTextCursorLocation(mEtSearch);

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                    return true;
                }
                return false;
            }
        });

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    mFlClean.setVisibility(View.GONE);
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                } else {
                    mFlClean.setVisibility(View.VISIBLE);
                }
            }
        });

        mFlClose.setOnClickListener(this);
        mFlClean.setOnClickListener(this);
        mTvSearch.setOnClickListener(this);

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close://关闭
                goFinish();
                break;
            case R.id.fl_clean://清除搜索数据
                mEtSearch.setText("");
                break;
            case R.id.tv_search://搜索
                KeyboardUtils.hideSoftInput(mEtSearch);
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
        }
    }

    @Override
    public void getData() {
        // 2022-10-09获取数据
        SearchResultModel.sendSearchResultRequest(TAG, String.valueOf(mPageNum), "20", mEtSearch.getText().toString(),barCode,new CustomerJsonCallBack<SearchResultModel>() {
            @Override
            public void onRequestError(SearchResultModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(SearchResultModel returnData) {
                hideRefreshView();
                List<SearchResultModel.DataBean> infos = returnData.getData();
                if (mAdapter!=null&&infos != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(infos);
                    } else {
                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        SearchResultModel.DataBean itemData = ((SearchResultAdapter) adapter).getItem(position);
        Intent intent=new Intent(this,LookGoodsActivity.class);
        intent.putExtra("GoodsNo", itemData.getGoodsNo());
        intent.putExtra("hasLowerShelfBtn", false);
        startActivity(intent);

//        GoodDetailActivity.newIntance(SearchResultActivity.this, itemData.getGoodsNo());
    }

    public static void newIntance(Context context, String searchText,String barCode) {
        Intent intent = new Intent(context, SearchResultActivity.class);
        intent.putExtra("searchText", searchText);
        intent.putExtra("barCode", barCode);
        context.startActivity(intent);
    }
}
