package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.switchButton.SwitchButton;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.login.activity.LoginActivity;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：智慧药房-商户-设置页面
 * ================================================
 */
public class SettingActivity_ZhyfManage extends BaseActivity {
    private SwitchButton mSbOrderVoiceReminder;
    private LinearLayout mLlAccountCancellation,mLlAboutUs;
    private ShapeTextView mTvLogout;

    private CustomerDialogUtils customerDialogUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_setting_zhyf_manage;
    }

    @Override
    public void initView() {
        super.initView();
        mSbOrderVoiceReminder = findViewById(R.id.sb_order_voice_reminder);
        mLlAccountCancellation= findViewById(R.id.ll_account_cancellation);
        mLlAboutUs = findViewById(R.id.ll_about_us);
        mTvLogout = findViewById(R.id.tv_logout);
    }

    @Override
    public void initData() {
        super.initData();

        //设置订单语音提醒是否开启
        mSbOrderVoiceReminder.setCheckedImmediatelyNoEvent("1".equals(MechanismInfoUtils.getVoiceNotification())?true:false);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mSbOrderVoiceReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                // 2022-10-13 请求接口 将设置接口上报
                setVoiceNotification(b);
            }
        });
        mLlAccountCancellation.setOnClickListener(this);
        mLlAboutUs.setOnClickListener(this);
        mTvLogout.setOnClickListener(this);
    }

    //设置订单语音提醒
    private void setVoiceNotification(boolean b) {//（1开启0关闭）
        showWaitDialog();
        String voiceNotification=b ? "1" : "0";
        BaseModel.sendVoiceNotificationRequest(TAG,voiceNotification, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);

                mSbOrderVoiceReminder.setCheckedImmediatelyNoEvent(!b);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                MechanismInfoUtils.setVoiceNotification(voiceNotification);
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.ll_account_cancellation://注销账户
                AccountCancellationActivity.newIntance(SettingActivity_ZhyfManage.this);
                break;
            case R.id.ll_about_us://关于我们
                AboutUsActivity_ZhyfManage.newIntance(SettingActivity_ZhyfManage.this,2);
                break;
            case R.id.tv_logout://退出登录
                exit();
                break;
        }
    }

    //退出登录
    private void exit() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(SettingActivity_ZhyfManage.this, "提示", "是否确认退出登录?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        showWaitDialog();
                        BaseModel.sendLogoutRequest(TAG, new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                LoginActivity.newIntance(SettingActivity_ZhyfManage.this);
                            }
                        });
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, SettingActivity_ZhyfManage.class);
        context.startActivity(intent);
    }
}
