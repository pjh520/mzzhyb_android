package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.core.content.ContextCompat;

import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.SpanUtils;
import com.frame.compiler.widget.countDownTime.CustomCountDownTimer;
import com.frame.compiler.widget.customPop.CustomerBottomListPop;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.fragment.ShopStyleFragment;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.StoreStatusModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;

import java.math.BigDecimal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-09-27
 * 描    述：入驻申请 页面
 * ================================================
 */
public class SettleInApplyActivity extends BaseActivity implements CompressUploadSinglePicUtils_zhyf.IChoosePic{
    public final String TAG_SELECT_PIC_01 = "1";//法人身份证正面照片
    public final String TAG_SELECT_PIC_02 = "2";//法人身份证反面照片
    public final String TAG_SELECT_PIC_03 = "3";//上传医疗机构执业许可证或营业执照
    public final String TAG_SELECT_PIC_04 = "4";//药品经营许可证
    public final String TAG_SELECT_PIC_05 = "5";//医疗器械经营许可证
    public final String TAG_SELECT_PIC_06 = "6";//GS认证证书
    public final String TAG_SELECT_PIC_07 = "7";//开户许可证
    public final String TAG_SELECT_PIC_08 = "8";//门头照片
    public final String TAG_SELECT_PIC_09 = "9";//内景照片1
    public final String TAG_SELECT_PIC_10 = "10";//内景照片2

    protected TextView mTvFixmedinsCode;
    protected TextView mTvFixmedinsName;
    protected AppCompatRadioButton mSrbOutpat;
    protected EditText mEtRealName;
    protected EditText mEtPhone;
    protected EditText mEtVerifCode;
    protected TextView mTvTime;
    protected EditText mEtSettlBankcard;
    protected EditText mEtSettlBank;
    protected ImageView mIvAddLicense01;
    protected ImageView mIvAddPhoto01;
    protected FrameLayout mFlPic01;
    protected ImageView mIvPic01;
    protected ImageView mIvDelete01;
    protected ImageView mIvAddLicense02;
    protected ImageView mIvAddPhoto02;
    protected FrameLayout mFlPic02;
    protected ImageView mIvPic02;
    protected ImageView mIvDelete02;
    protected ImageView mIvAddLicense03;
    protected ImageView mIvAddPhoto03;
    protected FrameLayout mFlPic03;
    protected ImageView mIvPic03;
    protected ImageView mIvDelete03;
    protected ImageView mIvAddLicense04;
    protected ImageView mIvAddPhoto04;
    protected FrameLayout mFlPic04;
    protected ImageView mIvPic04;
    protected ImageView mIvDelete04;
    protected ImageView mIvAddLicense05;
    protected ImageView mIvAddPhoto05;
    protected FrameLayout mFlPic05;
    protected ImageView mIvPic05;
    protected ImageView mIvDelete05;
    protected ImageView mIvAddLicense06;
    protected ImageView mIvAddPhoto06;
    protected FrameLayout mFlPic06;
    protected ImageView mIvPic06;
    protected ImageView mIvDelete06;
    protected ImageView mIvAddLicense07;
    protected ImageView mIvAddPhoto07;
    protected FrameLayout mFlPic07;
    protected ImageView mIvPic07;
    protected ImageView mIvDelete07;
    protected ImageView mIvAddLicense08;
    protected ImageView mIvAddPhoto08;
    protected FrameLayout mFlPic08;
    protected ImageView mIvPic08;
    protected ImageView mIvDelete08;
    protected ImageView mIvAddLicense09;
    protected ImageView mIvAddPhoto09;
    protected FrameLayout mFlPic09;
    protected ImageView mIvPic09;
    protected ImageView mIvDelete09;
    protected ImageView mIvAddLicense10;
    protected ImageView mIvAddPhoto10;
    protected FrameLayout mFlPic10;
    protected ImageView mIvPic10;
    protected ImageView mIvDelete10;
    protected FrameLayout mFlCb;
    protected CheckBox mCb;
    protected TextView mTvAgreeRule;
    protected ShapeTextView mTvSubmit;

    private CompressUploadSinglePicUtils_zhyf compressUploadSinglePicUtils;
    private CustomCountDownTimer mCustomCountDownTimer;
    private CustomerBottomListPop customerBottomListPop; //选择弹框

    @Override
    public int getLayoutId() {
        return R.layout.activity_settlein_apply;
    }

    @Override
    public void initView() {
        super.initView();
        mTvFixmedinsCode = findViewById(R.id.tv_fixmedins_code);
        mTvFixmedinsName = findViewById(R.id.tv_fixmedins_name);
        mSrbOutpat = findViewById(R.id.srb_outpat);
        mEtRealName = findViewById(R.id.et_real_name);
        mEtPhone = findViewById(R.id.et_phone);
        mEtVerifCode = findViewById(R.id.et_verif_code);
        mTvTime = findViewById(R.id.tv_time);
        mEtSettlBankcard = findViewById(R.id.et_settl_bankcard);
        mEtSettlBank = findViewById(R.id.et_settl_bank);

        mIvAddLicense01 = findViewById(R.id.iv_add_license_01);
        mIvAddPhoto01 = findViewById(R.id.iv_add_photo_01);
        mFlPic01 = findViewById(R.id.fl_pic_01);
        mIvPic01 = findViewById(R.id.iv_pic_01);
        mIvDelete01 = findViewById(R.id.iv_delete_01);

        mIvAddLicense02 = findViewById(R.id.iv_add_license_02);
        mIvAddPhoto02 = findViewById(R.id.iv_add_photo_02);
        mFlPic02 = findViewById(R.id.fl_pic_02);
        mIvPic02 = findViewById(R.id.iv_pic_02);
        mIvDelete02 = findViewById(R.id.iv_delete_02);

        mIvAddLicense03 = findViewById(R.id.iv_add_license_03);
        mIvAddPhoto03 = findViewById(R.id.iv_add_photo_03);
        mFlPic03 = findViewById(R.id.fl_pic_03);
        mIvPic03 = findViewById(R.id.iv_pic_03);
        mIvDelete03 = findViewById(R.id.iv_delete_03);

        mIvAddLicense04 = findViewById(R.id.iv_add_license_04);
        mIvAddPhoto04 = findViewById(R.id.iv_add_photo_04);
        mFlPic04 = findViewById(R.id.fl_pic_04);
        mIvPic04 = findViewById(R.id.iv_pic_04);
        mIvDelete04 = findViewById(R.id.iv_delete_04);

        mIvAddLicense05 = findViewById(R.id.iv_add_license_05);
        mIvAddPhoto05 = findViewById(R.id.iv_add_photo_05);
        mFlPic05 = findViewById(R.id.fl_pic_05);
        mIvPic05 = findViewById(R.id.iv_pic_05);
        mIvDelete05 = findViewById(R.id.iv_delete_05);

        mIvAddLicense06 = findViewById(R.id.iv_add_license_06);
        mIvAddPhoto06 = findViewById(R.id.iv_add_photo_06);
        mFlPic06 = findViewById(R.id.fl_pic_06);
        mIvPic06 = findViewById(R.id.iv_pic_06);
        mIvDelete06 = findViewById(R.id.iv_delete_06);

        mIvAddLicense07 = findViewById(R.id.iv_add_license_07);
        mIvAddPhoto07 = findViewById(R.id.iv_add_photo_07);
        mFlPic07 = findViewById(R.id.fl_pic_07);
        mIvPic07 = findViewById(R.id.iv_pic_07);
        mIvDelete07 = findViewById(R.id.iv_delete_07);

        mIvAddLicense08 = findViewById(R.id.iv_add_license_08);
        mIvAddPhoto08 = findViewById(R.id.iv_add_photo_08);
        mFlPic08 = findViewById(R.id.fl_pic_08);
        mIvPic08 = findViewById(R.id.iv_pic_08);
        mIvDelete08 = findViewById(R.id.iv_delete_08);

        mIvAddLicense09 = findViewById(R.id.iv_add_license_09);
        mIvAddPhoto09 = findViewById(R.id.iv_add_photo_09);
        mFlPic09 = findViewById(R.id.fl_pic_09);
        mIvPic09 = findViewById(R.id.iv_pic_09);
        mIvDelete09 = findViewById(R.id.iv_delete_09);

        mIvAddLicense10 = findViewById(R.id.iv_add_license_10);
        mIvAddPhoto10 = findViewById(R.id.iv_add_photo_10);
        mFlPic10 = findViewById(R.id.fl_pic_10);
        mIvPic10 = findViewById(R.id.iv_pic_10);
        mIvDelete10 = findViewById(R.id.iv_delete_10);

        mFlCb = findViewById(R.id.fl_cb);
        mCb = findViewById(R.id.cb);
        mTvAgreeRule = findViewById(R.id.tv_agree_rule);
        mTvSubmit = findViewById(R.id.tv_submit);
    }

    @Override
    public void initData() {
        super.initData();
        mTvFixmedinsCode.setText(MechanismInfoUtils.getFixmedinsCode());
        mTvFixmedinsName.setText(MechanismInfoUtils.getFixmedinsName());
        if ("1".equals(MechanismInfoUtils.getFixmedinsType())) {//定点医院
            mSrbOutpat.setText("定点医疗机构");
        } else {//定点药店
            mSrbOutpat.setText("定点零售药店");
        }
        onSetPicCert();
        setHighlightColor();
        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mIvAddLicense01.setOnClickListener(this);
        mIvPic01.setOnClickListener(this);
        mIvDelete01.setOnClickListener(this);

        mIvAddLicense02.setOnClickListener(this);
        mIvPic02.setOnClickListener(this);
        mIvDelete02.setOnClickListener(this);

        mIvAddLicense03.setOnClickListener(this);
        mIvPic03.setOnClickListener(this);
        mIvDelete03.setOnClickListener(this);

        mIvAddLicense04.setOnClickListener(this);
        mIvPic04.setOnClickListener(this);
        mIvDelete04.setOnClickListener(this);

        mIvAddLicense05.setOnClickListener(this);
        mIvPic05.setOnClickListener(this);
        mIvDelete05.setOnClickListener(this);

        mIvAddLicense06.setOnClickListener(this);
        mIvPic06.setOnClickListener(this);
        mIvDelete06.setOnClickListener(this);

        mIvAddLicense07.setOnClickListener(this);
        mIvPic07.setOnClickListener(this);
        mIvDelete07.setOnClickListener(this);

        mIvAddLicense08.setOnClickListener(this);
        mIvPic08.setOnClickListener(this);
        mIvDelete08.setOnClickListener(this);

        mIvAddLicense09.setOnClickListener(this);
        mIvPic09.setOnClickListener(this);
        mIvDelete09.setOnClickListener(this);

        mIvAddLicense10.setOnClickListener(this);
        mIvPic10.setOnClickListener(this);
        mIvDelete10.setOnClickListener(this);

        if (mTvTime!=null){
            mTvTime.setOnClickListener(this);
        }
        if (mFlCb!=null){
            mFlCb.setOnClickListener(this);
        }
        mTvSubmit.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.iv_add_license_01://添加身份证人像正面照
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_01, CompressUploadSinglePicUtils_zhyf.PIC_APPLY_FRONT_PHOTO_TAG);
                break;
            case R.id.iv_pic_01://查看身份证人像正面照
                showBigPic(mIvPic01,String.valueOf(mIvPic01.getTag(R.id.tag_2)));
                break;
            case R.id.iv_delete_01://删除身份证人像正面照
                mIvAddLicense02.setVisibility(View.VISIBLE);
                mIvAddPhoto02.setVisibility(View.VISIBLE);
                mIvAddLicense02.setEnabled(true);

                mIvPic03.setTag(R.id.tag_1, "");
                mIvPic03.setTag(R.id.tag_2, "");
                mFlPic03.setVisibility(View.GONE);
                break;
            case R.id.iv_add_license_02://添加身份证国徽反面照
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_02, CompressUploadSinglePicUtils_zhyf.PIC_APPLY_NEGATIVE_PHOTO_TAG);
                break;
            case R.id.iv_pic_02://查看身份证国徽反面照
                showBigPic(mIvPic02,String.valueOf(mIvPic02.getTag(R.id.tag_2)));
                break;
            case R.id.iv_delete_02://删除身份证国徽反面照
                mIvAddLicense03.setVisibility(View.VISIBLE);
                mIvAddPhoto03.setVisibility(View.VISIBLE);
                mIvAddLicense03.setEnabled(true);

                mIvPic02.setTag(R.id.tag_1, "");
                mIvPic02.setTag(R.id.tag_2, "");
                mFlPic02.setVisibility(View.GONE);
                break;
            case R.id.iv_add_license_03://添加营业执照信息
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_03, CompressUploadSinglePicUtils_zhyf.PIC_APPLY_CERT_TAG);
                break;
            case R.id.iv_pic_03://查看营业执照图片
                showBigPic(mIvPic03,String.valueOf(mIvPic03.getTag(R.id.tag_2)));
                break;
            case R.id.iv_delete_03://删除营业执照信息
                mIvAddLicense03.setVisibility(View.VISIBLE);
                mIvAddPhoto03.setVisibility(View.VISIBLE);
                mIvAddLicense03.setEnabled(true);

                mIvPic03.setTag(R.id.tag_1, "");
                mIvPic03.setTag(R.id.tag_2, "");
                mFlPic03.setVisibility(View.GONE);
                break;
            case R.id.iv_add_license_04://添加药品经营许可证
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_04, CompressUploadSinglePicUtils_zhyf.PIC_QUALI_02_TAG);
                break;
            case R.id.iv_pic_04://查看药品经营许可证
                showBigPic(mIvPic04,String.valueOf(mIvPic04.getTag(R.id.tag_2)));
                break;
            case R.id.iv_delete_04://删除药品经营许可证
                mIvAddLicense04.setVisibility(View.VISIBLE);
                mIvAddPhoto04.setVisibility(View.VISIBLE);
                mIvAddLicense04.setEnabled(true);

                mIvPic04.setTag(R.id.tag_1, "");
                mIvPic04.setTag(R.id.tag_2, "");
                mFlPic04.setVisibility(View.GONE);
                break;
            case R.id.iv_add_license_05://添加医疗器械经营许可证
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_05, CompressUploadSinglePicUtils_zhyf.PIC_QUALI_03_TAG);
                break;
            case R.id.iv_pic_05://查看医疗器械经营许可证
                showBigPic(mIvPic05,String.valueOf(mIvPic05.getTag(R.id.tag_2)));
                break;
            case R.id.iv_delete_05://删除医疗器械经营许可证
                mIvAddLicense05.setVisibility(View.VISIBLE);
                mIvAddPhoto05.setVisibility(View.VISIBLE);
                mIvAddLicense05.setEnabled(true);

                mIvPic05.setTag(R.id.tag_1, "");
                mIvPic05.setTag(R.id.tag_2, "");
                mFlPic05.setVisibility(View.GONE);
                break;
            case R.id.iv_add_license_06://添加GS认证证书
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_06, CompressUploadSinglePicUtils_zhyf.PIC_QUALI_04_TAG);
                break;
            case R.id.iv_pic_06://查看GS认证证书
                showBigPic(mIvPic06,String.valueOf(mIvPic06.getTag(R.id.tag_2)));
                break;
            case R.id.iv_delete_06://删除GS认证证书
                mIvAddLicense06.setVisibility(View.VISIBLE);
                mIvAddPhoto06.setVisibility(View.VISIBLE);
                mIvAddLicense06.setEnabled(true);

                mIvPic06.setTag(R.id.tag_1, "");
                mIvPic06.setTag(R.id.tag_2, "");
                mFlPic06.setVisibility(View.GONE);
                break;
            case R.id.iv_add_license_07://添加开户许可证
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_07, CompressUploadSinglePicUtils_zhyf.PIC_APPLY_01_TAG);
                break;
            case R.id.iv_pic_07://查看开户许可证
                showBigPic(mIvPic07,String.valueOf(mIvPic07.getTag(R.id.tag_2)));
                break;
            case R.id.iv_delete_07://删除开户许可证
                mIvAddLicense07.setVisibility(View.VISIBLE);
                mIvAddPhoto07.setVisibility(View.VISIBLE);
                mIvAddLicense07.setEnabled(true);

                mIvPic07.setTag(R.id.tag_1, "");
                mIvPic07.setTag(R.id.tag_2, "");
                mFlPic07.setVisibility(View.GONE);
                break;
            case R.id.iv_add_license_08://添加门头照片
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_08, CompressUploadSinglePicUtils_zhyf.PIC_APPLY_02_TAG);
                break;
            case R.id.iv_pic_08://查看门头照片
                showBigPic(mIvPic08,String.valueOf(mIvPic08.getTag(R.id.tag_2)));
                break;
            case R.id.iv_delete_08://删除门头照片
                mIvAddLicense08.setVisibility(View.VISIBLE);
                mIvAddPhoto08.setVisibility(View.VISIBLE);
                mIvAddLicense08.setEnabled(true);

                mIvPic08.setTag(R.id.tag_1, "");
                mIvPic08.setTag(R.id.tag_2, "");
                mFlPic08.setVisibility(View.GONE);
                break;
            case R.id.iv_add_license_09://添加内景照片1
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_09, CompressUploadSinglePicUtils_zhyf.PIC_APPLY_03_TAG);
                break;
            case R.id.iv_pic_09://查看内景照片1
                showBigPic(mIvPic09,String.valueOf(mIvPic09.getTag(R.id.tag_2)));
                break;
            case R.id.iv_delete_09://删除内景照片1
                mIvAddLicense09.setVisibility(View.VISIBLE);
                mIvAddPhoto09.setVisibility(View.VISIBLE);
                mIvAddLicense09.setEnabled(true);

                mIvPic09.setTag(R.id.tag_1, "");
                mIvPic09.setTag(R.id.tag_2, "");
                mFlPic09.setVisibility(View.GONE);
                break;
            case R.id.iv_add_license_10://添加内景照片2
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_10, CompressUploadSinglePicUtils_zhyf.PIC_APPLY_04_TAG);
                break;
            case R.id.iv_pic_10://查看内景照片2
                showBigPic(mIvPic10,String.valueOf(mIvPic10.getTag(R.id.tag_2)));
                break;
            case R.id.iv_delete_10://删除内景照片2
                mIvAddLicense10.setVisibility(View.VISIBLE);
                mIvAddPhoto10.setVisibility(View.VISIBLE);
                mIvAddLicense10.setEnabled(true);

                mIvPic10.setTag(R.id.tag_1, "");
                mIvPic10.setTag(R.id.tag_2, "");
                mFlPic10.setVisibility(View.GONE);
                break;
            case R.id.tv_time://获取短信验证码
                //2022-07-25 先请求接口 接口返回发送短信验证码成功 然后开始倒计时
                sendSms();
                break;
            case R.id.fl_cb://是否同意用户注册协议
                if (mCb.isChecked()) {
                    mCb.setChecked(false);
                } else {
                    mCb.setChecked(true);
                }
                break;
            case R.id.tv_submit://提交审核
                submit();
                break;
        }
    }

    //设置营业执照
    protected void onSetPicCert() {
        if (!EmptyUtils.isEmpty(MechanismInfoUtils.getAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, MechanismInfoUtils.getAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicSuccess(mFlPic03,mIvPic03,mIvAddLicense03,mIvAddPhoto03,MechanismInfoUtils.getAccessoryId(), MechanismInfoUtils.getAccessoryUrl(), width, height);
                }
            });
        }
    }
    //设置文字高亮
    public void setHighlightColor() {
        //入驻需已签署《智慧医保合作协议》和《快钱开户意愿确认函》
        // 响应点击事件的话必须设置以下属性
        String text=mTvAgreeRule.getText().toString().trim();
        mTvAgreeRule.setMovementMethod(LinkMovementMethod.getInstance());
        mTvAgreeRule.setHighlightColor(ContextCompat.getColor(RxTool.getContext(), android.R.color.transparent));
        String s1 = text.substring(0, 6);
        String s2 = text.substring(6, 16);
        String s3 = text.substring(16, 17);
        String s4 = text.substring(17, text.length());

        SpannableStringBuilder str2 = new SpanUtils()
                .append(s1)
                .append(s2).setForegroundColor(Color.parseColor("#4870E0")).setClickSpan(new ClickableSpan() {
                    @Override
                    public void onClick(@NonNull View widget) {
                        MyWebViewActivity.newIntance(SettleInApplyActivity.this, "快钱开户意愿确认函",
                                Constants.BASE_URL+Constants.WebUrl.USER_AGREEMENT_URL,true, false);
                    }

                    @Override
                    public void updateDrawState(@NonNull TextPaint ds) {
                        ds.setColor(Color.parseColor("#FFA201"));
                        ds.setUnderlineText(false);
                    }
                })
                .append(s3)
                .append(s4).setForegroundColor(Color.parseColor("#4870E0")).setClickSpan(new ClickableSpan() {
                    @Override
                    public void onClick(@NonNull View widget) {
                        MyWebViewActivity.newIntance(SettleInApplyActivity.this, "米脂医保合作协议",
                                Constants.BASE_URL+Constants.WebUrl.USER_AGREEMENT_URL,true, false);
                    }

                    @Override
                    public void updateDrawState(@NonNull TextPaint ds) {
                        ds.setColor(Color.parseColor("#FFA201"));
                        ds.setUnderlineText(false);
                    }
                })
                .create();
        mTvAgreeRule.setText(str2);
    }

    //发送短信
    private void sendSms() {
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString().trim()) || mEtPhone.getText().toString().length() < 11) {
            showShortToast("手机号码格式不正确!");
            return;
        }
        showWaitDialog();
        BaseModel.sendSendsmsRequest(TAG, mEtPhone.getText().toString().trim(), "4", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();

                //发送短信验证码成功
                mTvTime.setEnabled(false);
                showShortToast("验证码已发送");
                mTvTime.setTextColor(getResources().getColor(R.color.color_ff0202));
                setTimeStart(60000);
            }
        });
    }

    //开启定时器  millisecond:倒计时的时间
    private void setTimeStart(long millisecond) {
        //防止内存泄漏
        if (null != mCustomCountDownTimer) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        mCustomCountDownTimer = new CustomCountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long S = millisUntilFinished / 1000;
                mTvTime.setText(S < 10 ? "0" + S + "s" : S + "s");
            }

            @Override
            public void onFinish() {
                mTvTime.setEnabled(true);
                mTvTime.setTextColor(getResources().getColor(R.color.color_4870e0));
                mTvTime.setText("获取验证码");
            }
        };

        mCustomCountDownTimer.start();
    }

    //展示大图
    private void showBigPic(ImageView imageView,String imageUrl) {
        OpenImage.with(SettleInApplyActivity.this)
                //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                .setClickImageView(imageView)
                //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                //RecyclerView的数据
                .setImageUrl(imageUrl,MediaType.IMAGE)
                //点击的ImageView所在数据的位置
                .setClickPosition(0)
                //开始展示大图
                .show();
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String mTag, String accessoryId, String url, int width, int height) {
        switch (mTag) {
            case TAG_SELECT_PIC_01://法人身份证正面照片
                selectPicSuccess(mFlPic01,mIvPic01,mIvAddLicense01,mIvAddPhoto01,accessoryId, url, width, height);
                break;
            case TAG_SELECT_PIC_02://法人身份证反面照片
                selectPicSuccess(mFlPic02,mIvPic02,mIvAddLicense02,mIvAddPhoto02,accessoryId, url, width, height);
                break;
            case TAG_SELECT_PIC_03://上传医疗机构执业许可证或营业执照
                selectPicSuccess(mFlPic03,mIvPic03,mIvAddLicense03,mIvAddPhoto03,accessoryId, url, width, height);
                break;
            case TAG_SELECT_PIC_04://药品经营许可证
                selectPicSuccess(mFlPic04,mIvPic04,mIvAddLicense04,mIvAddPhoto04,accessoryId, url, width, height);
                break;
            case TAG_SELECT_PIC_05://医疗器械经营许可证
                selectPicSuccess(mFlPic05,mIvPic05,mIvAddLicense05,mIvAddPhoto05,accessoryId, url, width, height);
                break;
            case TAG_SELECT_PIC_06://GS认证证书
                selectPicSuccess(mFlPic06,mIvPic06,mIvAddLicense06,mIvAddPhoto06,accessoryId, url, width, height);
                break;
            case TAG_SELECT_PIC_07://开户许可证
                selectPicSuccess(mFlPic07,mIvPic07,mIvAddLicense07,mIvAddPhoto07,accessoryId, url, width, height);
                break;
            case TAG_SELECT_PIC_08://门头照片
                selectPicSuccess(mFlPic08,mIvPic08,mIvAddLicense08,mIvAddPhoto08,accessoryId, url, width, height);
                break;
            case TAG_SELECT_PIC_09://内景照片1
                selectPicSuccess(mFlPic09,mIvPic09,mIvAddLicense09,mIvAddPhoto09,accessoryId, url, width, height);
                break;
            case TAG_SELECT_PIC_10://内景照片2
                selectPicSuccess(mFlPic10,mIvPic10,mIvAddLicense10,mIvAddPhoto10,accessoryId, url, width, height);
                break;
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }

    //设置选择的照片
    protected void selectPicSuccess(FrameLayout mFlPic,ImageView mIvPic,ImageView mIvAddLicense,ImageView mIvAddPhoto,String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvPic.getLayoutParams();
            if (radio >= 2.3) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_300) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_300));
            }

            mIvPic.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvPic);

            mIvAddLicense.setVisibility(View.GONE);
            mIvAddPhoto.setVisibility(View.GONE);
            mIvAddLicense.setEnabled(false);

            mIvPic.setTag(R.id.tag_1, accessoryId);
            mIvPic.setTag(R.id.tag_2, url);
            mFlPic.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }
    //--------------------------选择图片----------------------------------------------------

    //提交审核
    protected void submit() {
        if (EmptyUtils.isEmpty(mEtRealName.getText().toString())) {
            showShortToast("请填写法人姓名");
            return;
        }
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString())) {
            showShortToast("请填写法人联系电话");
            return;
        }
        if (!"1".equals(String.valueOf(mEtPhone.getText().charAt(0))) || mEtPhone.getText().length() != 11) {
            showShortToast("请输入正确的法人联系电话");
            return;
        }
        if (EmptyUtils.isEmpty(mEtSettlBankcard.getText().toString())) {
            showShortToast("请填写银行卡号");
            return;
        }
        if (EmptyUtils.isEmpty(mEtSettlBank.getText().toString())) {
            showShortToast("请填写开户行");
            return;
        }

        if (null == mIvPic01.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvPic01.getTag(R.id.tag_2)))) {
            showShortToast("请上传法人身份证正面照片");
            return;
        }
        if (null == mIvPic02.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvPic02.getTag(R.id.tag_2)))) {
            showShortToast("请上传法人身份证反面照片");
            return;
        }
        if (null == mIvPic03.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvPic03.getTag(R.id.tag_2)))) {
            showShortToast("请上传医疗机构执业许可证或营业执照");
            return;
        }
        if (null == mIvPic04.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvPic04.getTag(R.id.tag_2)))) {
            showShortToast("请上传药品经营许可证");
            return;
        }
        if (null == mIvPic07.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvPic07.getTag(R.id.tag_2)))) {
            showShortToast("请上传开户许可证");
            return;
        }
        if (null == mIvPic08.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvPic08.getTag(R.id.tag_2)))) {
            showShortToast("请上传门头照片");
            return;
        }
        if (null == mIvPic09.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvPic09.getTag(R.id.tag_2)))) {
            showShortToast("请上传内景照片1");
            return;
        }
        if (null == mIvPic10.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvPic10.getTag(R.id.tag_2)))) {
            showShortToast("请上传内景照片2");
            return;
        }
//        if (EmptyUtils.isEmpty(mEtVerifCode.getText().toString())) {
//            showShortToast("请输入验证码");
//            return;
//        }
        if (!mCb.isChecked()) {
            showShortToast("请同意《用户注册协议》");
            return;
        }
        showWaitDialog();
        BaseModel.sendAuthManaRequest(TAG, MechanismInfoUtils.getFixmedinsCode(), MechanismInfoUtils.getFixmedinsName(),
                MechanismInfoUtils.getFixmedinsType(),mEtRealName.getText().toString(),mEtPhone.getText().toString(),mEtSettlBankcard.getText().toString(),
                mEtSettlBank.getText().toString(),mIvPic01.getTag(R.id.tag_1),mIvPic02.getTag(R.id.tag_1),
                mIvPic03.getTag(R.id.tag_1),mIvPic04.getTag(R.id.tag_1),mIvPic05.getTag(R.id.tag_1),
                mIvPic06.getTag(R.id.tag_1),mIvPic07.getTag(R.id.tag_1),mIvPic08.getTag(R.id.tag_1),
                mIvPic09.getTag(R.id.tag_1),mIvPic10.getTag(R.id.tag_1), new CustomerJsonCallBack<BaseModel>() {

                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        //设置机构的营业执照数据
                        MechanismInfoUtils.setAccessoryId(String.valueOf(mIvPic03.getTag(R.id.tag_1)));
                        MechanismInfoUtils.setAccessoryUrl(String.valueOf(mIvPic03.getTag(R.id.tag_2)));
                        //设置机构的电话号码
                        MechanismInfoUtils.setPhone(mEtPhone.getText().toString());
                        //设置机构审核状态
                        MechanismInfoUtils.setApproveStatus("1");
                        //设置管理员的姓名
                        LoginUtils.setUserName(mEtRealName.getText().toString());
                        //设置机构的电话号码
                        LoginUtils.setDr_phone(mEtPhone.getText().toString());

                        SettleInApplyResultActivity.newIntance(SettleInApplyActivity.this);
                        goFinish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCustomCountDownTimer != null) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        if (customerBottomListPop != null) {
            customerBottomListPop.dismiss();
            customerBottomListPop.onDestroy();
        }
        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, SettleInApplyActivity.class);
        context.startActivity(intent);
    }
}
