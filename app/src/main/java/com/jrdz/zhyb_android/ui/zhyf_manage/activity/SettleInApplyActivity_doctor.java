package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.EditDoctorActivity;
import com.jrdz.zhyb_android.ui.home.model.ApplyResultModel;
import com.jrdz.zhyb_android.ui.home.model.DoctorManageModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.SettleInApplyResultModel;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;

import java.math.BigDecimal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-09-27
 * 描    述：入驻申请页面--医师
 * ================================================
 */
public class SettleInApplyActivity_doctor extends BaseActivity implements CompressUploadSinglePicUtils_zhyf.IChoosePic {
    public final String TAG_SELECT_PIC_CERT = "1";
    public final String TAG_SELECT_PIC_FRONT_PHOTO = "2";
    public final String TAG_SELECT_PIC_NEGATIVE_PHOTO = "3";

    protected ImageView mIvAddLicense;
    protected ImageView mIvAddPhoto;
    protected FrameLayout mFlCert;
    protected ImageView mIvCert;
    protected ImageView mIvDelete;
    protected EditText mEtLicenseNo, mEtCardno;
    protected ImageView mIvAddLicense02;
    protected ImageView mIvAddPhoto02;
    protected FrameLayout mFlFrontPhoto;
    protected ImageView mIvFrontPhoto;
    protected ImageView mIvDelete02;
    protected ImageView mIvAddLicense03;
    protected ImageView mIvAddPhoto03;
    protected FrameLayout mFlNegativePhoto;
    protected ImageView mIvNegativePhoto;
    protected ImageView mIvDelete03;
    protected TextView mTvDescribe;
    protected LinearLayout mLlAgree;
    protected FrameLayout mFlCb;
    protected CheckBox mCb;
    protected TextView mTvAgreeRule;
    protected ShapeTextView mTvSubmit;

    private int pos = 0;
    private DoctorManageModel.DataBean resultObjBean;
    private CompressUploadSinglePicUtils_zhyf compressUploadSinglePicUtils;
    private CustomerDialogUtils customerDialogUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_settlein_apply_doctor;
    }

    @Override
    public void initView() {
        super.initView();
        mIvAddLicense = findViewById(R.id.iv_add_license);
        mIvAddPhoto = findViewById(R.id.iv_add_photo);
        mFlCert = findViewById(R.id.fl_cert);
        mIvCert = findViewById(R.id.iv_cert);
        mIvDelete = findViewById(R.id.iv_delete);
        mEtLicenseNo = findViewById(R.id.et_license_no);
        mIvAddLicense02 = findViewById(R.id.iv_add_license_02);
        mIvAddPhoto02 = findViewById(R.id.iv_add_photo_02);
        mFlFrontPhoto = findViewById(R.id.fl_front_photo);
        mIvFrontPhoto = findViewById(R.id.iv_front_photo);
        mIvDelete02 = findViewById(R.id.iv_delete_02);
        mIvAddLicense03 = findViewById(R.id.iv_add_license_03);
        mIvAddPhoto03 = findViewById(R.id.iv_add_photo_03);
        mFlNegativePhoto = findViewById(R.id.fl_negative_photo);
        mIvNegativePhoto = findViewById(R.id.iv_negative_photo);
        mIvDelete03 = findViewById(R.id.iv_delete_03);
        mEtCardno = findViewById(R.id.et_cardno);
        mTvDescribe= findViewById(R.id.tv_describe);
        mLlAgree= findViewById(R.id.ll_agree);
        mFlCb = findViewById(R.id.fl_cb);
        mCb = findViewById(R.id.cb);
        mTvAgreeRule = findViewById(R.id.tv_agree_rule);
        mTvSubmit = findViewById(R.id.tv_submit);
    }

    @Override
    public void initData() {
        pos = getIntent().getIntExtra("pos", 0);
        resultObjBean = getIntent().getParcelableExtra("resultObjBean");
        super.initData();
        //设置医师职业资格证
        if (!EmptyUtils.isEmpty(resultObjBean.getAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, resultObjBean.getAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicCertSuccess(resultObjBean.getAccessoryId(), resultObjBean.getAccessoryUrl(), width, height);
                }
            });
        }
        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);

        if ("5".equals(resultObjBean.getApproveStatus())) {
            mTvSubmit.setText("注销");
            mTvDescribe.setVisibility(View.GONE);
            mLlAgree.setVisibility(View.GONE);

            mIvAddLicense.setEnabled(false);
            mIvDelete.setEnabled(false);
            mEtLicenseNo.setEnabled(false);
            mIvAddLicense02.setEnabled(false);
            mIvDelete02.setEnabled(false);
            mIvAddLicense03.setEnabled(false);
            mIvDelete03.setEnabled(false);
            mEtCardno.setEnabled(false);
        } else {
            mTvSubmit.setText("提交");
            mTvDescribe.setVisibility(View.VISIBLE);
            mLlAgree.setVisibility(View.VISIBLE);

            mIvAddLicense.setEnabled(true);
            mIvDelete.setEnabled(true);
            mEtLicenseNo.setEnabled(true);
            mIvAddLicense02.setEnabled(true);
            mIvDelete02.setEnabled(true);
            mIvAddLicense03.setEnabled(true);
            mIvDelete03.setEnabled(true);
            mEtCardno.setEnabled(true);
        }

        showWaitDialog();
        getPagerData();
    }

    //获取页面数据
    private void getPagerData() {
        SettleInApplyResultModel.sendSettleInApplyResultDoctorRequest(TAG, resultObjBean.getDoctorId(), new CustomerJsonCallBack<SettleInApplyResultModel>() {
            @Override
            public void onRequestError(SettleInApplyResultModel returnData, String msg) {
                hideWaitDialog();
//                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(SettleInApplyResultModel returnData) {
                hideWaitDialog();
                if (returnData.getData() != null && returnData.getData().get(0) != null) {
                    //2022-09-28 请求后台数据 获取审核的结果
                    setData(returnData.getData().get(0));
                }
            }
        });
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mIvAddLicense.setOnClickListener(this);
        mIvCert.setOnClickListener(this);
        mIvDelete.setOnClickListener(this);
        mIvAddLicense02.setOnClickListener(this);
        mIvFrontPhoto.setOnClickListener(this);
        mIvDelete02.setOnClickListener(this);
        mIvAddLicense03.setOnClickListener(this);
        mIvNegativePhoto.setOnClickListener(this);
        mIvDelete03.setOnClickListener(this);
        mTvAgreeRule.setOnClickListener(this);
        mFlCb.setOnClickListener(this);
        mTvSubmit.setOnClickListener(this);
    }

    //设置数据(医师或其他人员)
    protected void setData(SettleInApplyResultModel.DataBean dataBean) {
        if (dataBean == null) {
            return;
        }
        mEtLicenseNo.setText(EmptyUtils.strEmpty(dataBean.getQualificationCertificateNo()));
        //选择身份证人像正面照
        if (!EmptyUtils.isEmpty(dataBean.getFrontAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, dataBean.getFrontAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicFrontPhotoSuccess(dataBean.getFrontAccessoryId(), dataBean.getFrontAccessoryUrl(), width, height);
                }
            });
        }
        //选择身份证国徽反面照
        if (!EmptyUtils.isEmpty(dataBean.getBackAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, dataBean.getBackAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectPicNegativePhotoSuccess(dataBean.getBackAccessoryId(), dataBean.getBackAccessoryUrl(), width, height);
                }
            });
        }
        mEtCardno.setText(EmptyUtils.strEmpty(dataBean.getIDNumber()));
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.iv_add_license://添加证件信息
                KeyboardUtils.hideSoftInput(SettleInApplyActivity_doctor.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_CERT, CompressUploadSinglePicUtils.PIC_ADDDOCTOR_TAG);
                break;
            case R.id.iv_cert://查看营业执照图片
                showBigPic(mIvCert,String.valueOf(mIvCert.getTag(R.id.tag_2)));
                break;
            case R.id.iv_delete://删除证件信息
                mIvAddLicense.setVisibility(View.VISIBLE);
                mIvAddPhoto.setVisibility(View.VISIBLE);
                mIvAddLicense.setEnabled(true);

                mIvCert.setTag(R.id.tag_1, "");
                mIvCert.setTag(R.id.tag_2, "");
                mFlCert.setVisibility(View.GONE);
                break;
            case R.id.iv_add_license_02://添加身份证人像正面照
                KeyboardUtils.hideSoftInput(SettleInApplyActivity_doctor.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_FRONT_PHOTO, CompressUploadSinglePicUtils_zhyf.PIC_APPLY_FRONT_PHOTO_TAG);
                break;
            case R.id.iv_front_photo://查看身份证人像正面照
                showBigPic(mIvFrontPhoto,String.valueOf(mIvFrontPhoto.getTag(R.id.tag_2)));
                break;
            case R.id.iv_delete_02://删除身份证人像正面照
                mIvAddLicense02.setVisibility(View.VISIBLE);
                mIvAddPhoto02.setVisibility(View.VISIBLE);
                mIvAddLicense02.setEnabled(true);

                mIvFrontPhoto.setTag(R.id.tag_1, "");
                mIvFrontPhoto.setTag(R.id.tag_2, "");
                mFlFrontPhoto.setVisibility(View.GONE);
                break;
            case R.id.iv_add_license_03://添加身份证国徽反面照
                KeyboardUtils.hideSoftInput(SettleInApplyActivity_doctor.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_NEGATIVE_PHOTO, CompressUploadSinglePicUtils_zhyf.PIC_APPLY_NEGATIVE_PHOTO_TAG);
                break;
            case R.id.iv_negative_photo://查看身份证国徽反面照
                showBigPic(mIvNegativePhoto,String.valueOf(mIvNegativePhoto.getTag(R.id.tag_2)));
                break;
            case R.id.iv_delete_03://删除身份证国徽反面照
                mIvAddLicense03.setVisibility(View.VISIBLE);
                mIvAddPhoto03.setVisibility(View.VISIBLE);
                mIvAddLicense03.setEnabled(true);

                mIvNegativePhoto.setTag(R.id.tag_1, "");
                mIvNegativePhoto.setTag(R.id.tag_2, "");
                mFlNegativePhoto.setVisibility(View.GONE);
                break;
            case R.id.fl_cb://是否同意用户注册协议
                if (mCb.isChecked()) {
                    mCb.setChecked(false);
                } else {
                    mCb.setChecked(true);
                }
                break;
            case R.id.tv_agree_rule://用户注册协议
                MyWebViewActivity.newIntance(SettleInApplyActivity_doctor.this, "用户注册协议", Constants.BASE_URL + Constants.WebUrl.USER_AGREEMENT_URL,
                        true, false);
                break;
            case R.id.tv_submit://提交审核
                if ("5".equals(resultObjBean.getApproveStatus())) {
                    cancellation();
                } else {
                    submit();
                }
                break;
        }
    }

    //展示大图
    private void showBigPic(ImageView imageView,String imageUrl) {
        OpenImage.with(SettleInApplyActivity_doctor.this)
                //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                .setClickImageView(imageView)
                //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                //RecyclerView的数据
                .setImageUrl(imageUrl,MediaType.IMAGE)
                //点击的ImageView所在数据的位置
                .setClickPosition(0)
                //开始展示大图
                .show();
    }

    //提交审核
    protected void submit() {
        //职业资格证 医师必填
        if ("2".equals(resultObjBean.getType())) {
            if (null == mIvCert.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvCert.getTag(R.id.tag_2)))) {
                showShortToast("请上传职业资格证");
                return;
            }
            if (EmptyUtils.isEmpty(mEtLicenseNo.getText().toString())) {
                showShortToast("请输入医师资格证编号");
                return;
            }
        }

        if (null == mIvFrontPhoto.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvFrontPhoto.getTag(R.id.tag_2)))) {
            showShortToast("请上传身份证人像正面照");
            return;
        }
        if (null == mIvNegativePhoto.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvNegativePhoto.getTag(R.id.tag_2)))) {
            showShortToast("请上传身份证国徽反面照");
            return;
        }
        if (EmptyUtils.isEmpty(mEtCardno.getText().toString())) {
            showShortToast("请填写您的身份证号码");
            return;
        }
        if (mEtCardno.getText().length() != 15 && mEtCardno.getText().length() != 18) {
            showShortToast("身份证号码为15位或者18位");
            return;
        }

        if (!mCb.isChecked()) {
            showShortToast("请同意《用户注册协议》");
            return;
        }

        showWaitDialog();
        BaseModel.sendAuthDoctorRequest(TAG, EmptyUtils.strEmpty(resultObjBean.getDoctorId()), MechanismInfoUtils.getFixmedinsCode(), MechanismInfoUtils.getFixmedinsName(),
                EmptyUtils.strEmpty(resultObjBean.getDr_name()), EmptyUtils.strEmpty(resultObjBean.getDr_phone()), mIvCert.getTag(R.id.tag_1), mEtLicenseNo.getText().toString(),
                mEtCardno.getText().toString(), mIvFrontPhoto.getTag(R.id.tag_1), mIvNegativePhoto.getTag(R.id.tag_1), "2",
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("提交成功");
                        MsgBus.sendApplyResult().post(new ApplyResultModel("1",pos,null ==mIvCert.getTag(R.id.tag_2)?"":String.valueOf(mIvCert.getTag(R.id.tag_2)),null ==mIvCert.getTag(R.id.tag_1)?"":String.valueOf(mIvCert.getTag(R.id.tag_1))));
                        goFinish();
                    }
                });
    }

    //注销
    private void cancellation() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(SettleInApplyActivity_doctor.this, "确认注销此身份吗？", "注意：当在线购药中有此账号需要处理的订单，账户有余额等将无法注销，且注销之后不可恢复，谨慎操作！！！",
                "取消", "确定", new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {}

                    @Override
                    public void onBtn02Click() {
                        showWaitDialog();
                        BaseModel.sendLogoffdoctorRequest(TAG, EmptyUtils.strEmpty(resultObjBean.getDoctorId()), new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                showShortToast("注销成功");
                                MsgBus.sendApplyResult().post(new ApplyResultModel("2",pos));
                                goFinish();
                            }
                        });
                    }
                });
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String mTag, String accessoryId, String url, int width, int height) {
        switch (mTag) {
            case TAG_SELECT_PIC_CERT://选择营业执照
                selectPicCertSuccess(accessoryId, url, width, height);
                break;
            case TAG_SELECT_PIC_FRONT_PHOTO://选择身份证人像正面照
                selectPicFrontPhotoSuccess(accessoryId, url, width, height);
                break;
            case TAG_SELECT_PIC_NEGATIVE_PHOTO://选择身份证国徽反面照
                selectPicNegativePhotoSuccess(accessoryId, url, width, height);
                break;
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }

    //选择营业执照
    private void selectPicCertSuccess(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCert.getLayoutParams();
            if (radio >= 2.3) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_300) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_300));
            }

            mIvCert.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCert);

            mIvAddLicense.setVisibility(View.GONE);
            mIvAddPhoto.setVisibility(View.GONE);
            mIvAddLicense.setEnabled(false);

            mIvCert.setTag(R.id.tag_1, accessoryId);
            mIvCert.setTag(R.id.tag_2, url);
            mFlCert.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //选择身份证人像正面照
    protected void selectPicFrontPhotoSuccess(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvFrontPhoto.getLayoutParams();
            if (radio >= 2.3) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_300) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_300));
            }
            mIvFrontPhoto.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvFrontPhoto);

            mIvAddLicense02.setVisibility(View.GONE);
            mIvAddPhoto02.setVisibility(View.GONE);
            mIvAddLicense02.setEnabled(false);

            mIvFrontPhoto.setTag(R.id.tag_1, accessoryId);
            mIvFrontPhoto.setTag(R.id.tag_2, url);
            mFlFrontPhoto.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //选择身份证国徽反面照
    protected void selectPicNegativePhotoSuccess(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvNegativePhoto.getLayoutParams();
            if (radio >= 2.3) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_300) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_300));
            }

            mIvNegativePhoto.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvNegativePhoto);

            mIvAddLicense03.setVisibility(View.GONE);
            mIvAddPhoto03.setVisibility(View.GONE);
            mIvAddLicense03.setEnabled(false);

            mIvNegativePhoto.setTag(R.id.tag_1, accessoryId);
            mIvNegativePhoto.setTag(R.id.tag_2, url);
            mFlNegativePhoto.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }
    //--------------------------选择图片----------------------------------------------------

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
        }
        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }
        resultObjBean = null;
    }

    public static void newIntance(Context context,int pos,  DoctorManageModel.DataBean resultObjBean) {
        Intent intent = new Intent(context, SettleInApplyActivity_doctor.class);
        intent.putExtra("pos", pos);
        intent.putExtra("resultObjBean", resultObjBean);
        context.startActivity(intent);
    }
}
