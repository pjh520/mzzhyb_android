package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.SettleInApplyResultModel;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-09-28
 * 描    述：入驻申请结果 页面
 * ================================================
 */
public class SettleInApplyResultActivity extends BaseActivity {
    protected TextView mTvResult;
    protected TextView mTvDescribe01;
    protected TextView mTvDescribe02;
    protected TextView mTvDescribe03;
    protected TextView mTvDescribe04, mTvDescribe05;
    protected ShapeTextView mTvLogin;

    protected SettleInApplyResultModel.DataBean pagerData;

    @Override
    public int getLayoutId() {
        return R.layout.activity_settlein_apply_result;
    }

    @Override
    public void initView() {
        super.initView();
        mTvResult = findViewById(R.id.tv_result);
        mTvDescribe01 = findViewById(R.id.tv_describe01);
        mTvDescribe02 = findViewById(R.id.tv_describe02);
        mTvDescribe03 = findViewById(R.id.tv_describe03);
        mTvDescribe04 = findViewById(R.id.tv_describe04);
        mTvDescribe05 = findViewById(R.id.tv_describe05);
        mTvLogin = findViewById(R.id.tv_login);
    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        getPagerData();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvLogin.setOnClickListener(this);
    }

    //获取管理员审核数据
    protected void getPagerData() {
        SettleInApplyResultModel.sendSettleInApplyResultRequest(TAG, new CustomerJsonCallBack<SettleInApplyResultModel>() {
            @Override
            public void onRequestError(SettleInApplyResultModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(SettleInApplyResultModel returnData) {
                hideWaitDialog();

                if (returnData.getData() != null && returnData.getData().get(0) != null) {
                    pagerData = returnData.getData().get(0);
                    //2022-09-28 请求后台数据 获取审核的结果
                    setData(pagerData);
                }
            }
        });
    }

    //设置数据(管理员)
    protected void setData(SettleInApplyResultModel.DataBean dataBean) {
        Log.e("666666", "dataBean.getApproveStatus(): "+dataBean.getApproveStatus());
        switch (dataBean.getApproveStatus()) {
            case "1"://审核中
                mTvDescribe02.setVisibility(View.VISIBLE);
                mTvDescribe03.setVisibility(View.GONE);
                mTvDescribe04.setVisibility(View.GONE);
                mTvDescribe05.setVisibility(View.GONE);
                mTvLogin.setVisibility(View.GONE);
                break;
            case "2"://审核未通过
                mTvDescribe02.setVisibility(View.GONE);
                mTvDescribe03.setVisibility(View.GONE);
                mTvDescribe04.setVisibility(View.VISIBLE);
                mTvDescribe04.setText("非常抱歉的通知您,"+pagerData.getRejectReason()+"您的资质审核未通过，请去");
                mTvDescribe05.setVisibility(View.GONE);
                mTvLogin.setVisibility(View.VISIBLE);
                mTvLogin.setText("修改/补充材料");
                break;
            case "3"://审核通过
                if ("1".equals(dataBean.getEntryMode())) {
                    //固定抽佣模式
                    mTvDescribe02.setVisibility(View.GONE);
                    mTvDescribe03.setVisibility(View.VISIBLE);
                    mTvDescribe03.setText("恭喜您审核通过，请通知机构其他人员进行入驻申请。您选择的是固定抽佣模式，可点击“立即登录”进入首页");
                    mTvDescribe04.setVisibility(View.GONE);
                    mTvDescribe05.setVisibility(View.GONE);
                    mTvLogin.setVisibility(View.VISIBLE);
                    mTvLogin.setText("立即登录");
                } else if ("2".equals(dataBean.getEntryMode())) {
                    //管理服务模式
                    mTvDescribe02.setVisibility(View.GONE);
                    mTvDescribe03.setVisibility(View.VISIBLE);
                    mTvDescribe03.setText("恭喜您审核通过，请通知机构其他人员进行入驻申请。您选择的是管理服务模式，需要先行充值管理服务费，才能进入首页");
                    mTvDescribe04.setVisibility(View.GONE);
                    mTvDescribe05.setVisibility(View.GONE);
                    mTvLogin.setVisibility(View.VISIBLE);
                    mTvLogin.setText("立即充值");
                }
                break;
            case "4"://提交五次审核 均未通过
                mTvDescribe02.setVisibility(View.GONE);
                mTvDescribe03.setVisibility(View.GONE);
                mTvDescribe04.setVisibility(View.GONE);
                mTvDescribe05.setVisibility(View.VISIBLE);
                mTvDescribe05.setText("非常抱歉的通知您，由于您已提交五次审核均未通过，暂不能再次提交审核，审核通道关闭");
                mTvLogin.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()) {
            case R.id.tv_login:
                onLoginBtn();
                break;
        }
    }

    //按钮点击事件
    protected void onLoginBtn() {
        switch (pagerData.getApproveStatus()) {
            case "3":
                if ("1".equals(pagerData.getEntryMode())) {//固定抽佣模式-审核通过  ---立即登录
                    //2022-11-14 请求接口 通知后台 已经点击立即登录按钮 需要改变审核状态
                    showWaitDialog();
                    onSureLogin("1");
                } else if ("2".equals(pagerData.getEntryMode())) {//管理服务模式--审核通过  ---立即充值
                    RechargeActivity_manage.newIntance(SettleInApplyResultActivity.this,pagerData.getCreateUser(),"2");
                }
                break;
            case "2"://审核未通过  ---修改/补充材料
                ModifyMaterialActivity.newIntance(SettleInApplyResultActivity.this,pagerData);
                goFinish();
                break;
        }
    }

    //立即登录 需要记录用户已经登录过首页的转态
    protected void onSureLogin(String ActivationType) {
        BaseModel.sendAuthLoginRequest(TAG, ActivationType, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                //1、管理员认证2、医师认证3、其他人员认证
                switch (ActivationType){
                    case "1":
                        MechanismInfoUtils.setApproveStatus("5");
                        break;
                    case "2":
                        LoginUtils.setApproveStatus("5");
                        break;
                    case "3":
                        break;
                }
                // 2022-09-28 跳转智慧药房首页
                SmartPhaMainActivity.newIntance(SettleInApplyResultActivity.this, 0);
                goFinish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pagerData = null;
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, SettleInApplyResultActivity.class);
        context.startActivity(intent);
    }
}
