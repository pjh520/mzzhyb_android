package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.frame.compiler.utils.viewPage.BaseViewPagerAndTabsAdapter_new;
import com.frame.compiler.widget.CustomViewPager;
import com.frame.compiler.widget.customPop.CustomerBottomListPop;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.fragment.ShopInfoFragment;
import com.jrdz.zhyb_android.ui.zhyf_manage.fragment.ShopStyleFragment;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GetStoreStatusModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.StoreStatusModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;

import java.util.Arrays;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-16
 * 描    述：店铺装修
 * ================================================
 */
public class ShopDecoActivity extends BaseActivity implements CustomerBottomListPop.IBottomChooseListener<StoreStatusModel> {
    private LinearLayout mLlStoreStatus;
    private TextView mTvStoreStatus;
    private ImageView mIvStoreStatus;
    private SlidingTabLayout mStbShop;
    private CustomViewPager mVpShop;

    String[] titles = new String[]{"店铺风格","店铺信息"};
    private CustomerBottomListPop customerBottomListPop; //店铺状态选择弹框
    private ShopStyleFragment shopStyleFragment;//店铺风格

    @Override
    public int getLayoutId() {
        return R.layout.activity_shop_deco;
    }

    @Override
    protected void initImmersionBar() {
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mTitleBar).keyboardEnable(true);
        mImmersionBar.init();
    }

    @Override
    public void initView() {
        super.initView();
        mLlStoreStatus = findViewById(R.id.ll_store_status);
        mTvStoreStatus = findViewById(R.id.tv_store_status);
        mIvStoreStatus = findViewById(R.id.iv_store_status);
        mStbShop = findViewById(R.id.stb_shop);
        mVpShop = findViewById(R.id.vp_shop);
    }

    @Override
    public void initData() {
        super.initData();
        //不同身份的人进入首页 展现不一样
        if (LoginUtils.isManage()){
            mLlStoreStatus.setVisibility(View.VISIBLE);

            getStoreStatusData();
        }else {
            mLlStoreStatus.setVisibility(View.GONE);
        }
        initTabLayout();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mLlStoreStatus.setOnClickListener(this);
    }

    private void initTabLayout() {
        shopStyleFragment=ShopStyleFragment.newIntance();
        BaseViewPagerAndTabsAdapter_new baseViewPagerAndTabsAdapter_new=new BaseViewPagerAndTabsAdapter_new(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position){
                    case 0://店铺风格
                        return shopStyleFragment;
                    case 1://店铺信息
                        return ShopInfoFragment.newIntance();
                }
                return ShopStyleFragment.newIntance();
            }
        };

        baseViewPagerAndTabsAdapter_new.setData(Arrays.asList(titles));
        mVpShop.setAdapter(baseViewPagerAndTabsAdapter_new);
        mStbShop.setViewPager(mVpShop);
    }

    //获取店铺状态数据
    private void getStoreStatusData() {
        //2022-09-30 店铺状态得做成全局来 获取店铺状态数据
        if ("1".equals(Constants.AppStorage.APP_STORE_STATUS)) {//上线
            //模拟店铺状态
            mTvStoreStatus.setText("上线");
            mTvStoreStatus.setTextColor(getResources().getColor(R.color.color_4870e0));
            mIvStoreStatus.setImageResource(R.drawable.ic_pull_down_blue);
        } else {//下线
            mTvStoreStatus.setText("下线");
            mTvStoreStatus.setTextColor(getResources().getColor(R.color.txt_color_999));
            mIvStoreStatus.setImageResource(R.drawable.ic_pull_down_gray);
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.ll_store_status://设置店铺状态
                if (customerBottomListPop==null){
                    customerBottomListPop = new CustomerBottomListPop(ShopDecoActivity.this, this);
                }
                customerBottomListPop.setDatas("1", "", mTvStoreStatus.getText().toString(), CommonlyUsedDataUtils.getInstance().getStoreStatusData());
                customerBottomListPop.showPopupWindow();
                break;
        }
    }

    @Override
    public void onItemClick(String tag, StoreStatusModel model) {
        if (Constants.AppStorage.APP_STORE_STATUS.equals(model.getId())){
            return;
        }
        showWaitDialog();
        GetStoreStatusModel.sendUpdateStoreStatusRequest(TAG, model.getId(), new CustomerJsonCallBack<GetStoreStatusModel>() {
            @Override
            public void onRequestError(GetStoreStatusModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GetStoreStatusModel returnData) {
                hideWaitDialog();
                mTvStoreStatus.setText(model.getText());
                if ("上线".equals(model.getText())){
                    mTvStoreStatus.setTextColor(getResources().getColor(R.color.color_4870e0));
                    mIvStoreStatus.setImageResource(R.drawable.ic_pull_down_blue);
                }else {
                    mTvStoreStatus.setTextColor(getResources().getColor(R.color.txt_color_999));
                    mIvStoreStatus.setImageResource(R.drawable.ic_pull_down_gray);
                }

                if (shopStyleFragment!=null){
                    shopStyleFragment.refreshStoreStatusChange(model.getId());
                }

                MsgBus.sendStoreStatus().post(model.getId());
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (customerBottomListPop != null) {
            customerBottomListPop.dismiss();
            customerBottomListPop.onDestroy();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, ShopDecoActivity.class);
        context.startActivity(intent);
    }
}
