package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSON;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.customPop.CustomerBottomListPop;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.JustifyContent;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.activity.SelectMultDiseCataActivity;
import com.jrdz.zhyb_android.ui.catalogue.model.DiseCataModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.RelationDiseAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.RelationDisetypeAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.DragStoreHouseModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.StoreStatusModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;
import com.jrdz.zhyb_android.widget.MyFlexboxLayoutManager;
import com.jrdz.zhyb_android.widget.pop.AddRelationDisePop;
import com.jrdz.zhyb_android.widget.pop.UpdateNumPop;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-24
 * 描    述：添加商品 --药品专区
 * ================================================
 */
public class SpecialAreaAddGoodsActivity extends BaseActivity implements CompressUploadSinglePicUtils_zhyf.IChoosePic, CustomerBottomListPop.IBottomChooseListener<StoreStatusModel> {
    public final String TAG_SELECT_BANNER_01 = "1";//banner 1号位
    public final String TAG_SELECT_BANNER_02 = "2";//banner 2号位
    public final String TAG_SELECT_BANNER_03 = "3";//banner 3号位
    public final String TAG_SELECT_DRUG_MANUAL = "4";//药品说明书
    public final String TAG_SELECT_COMMODITY_DETAILS_01 = "5";//商品详情1
    public final String TAG_SELECT_COMMODITY_DETAILS_02 = "6";//商品详情2
    public final String TAG_SELECT_COMMODITY_DETAILS_03 = "7";//商品详情3
    public final String TAG_SELECT_COMMODITY_DETAILS_04 = "8";//商品详情4
    public final String TAG_SELECT_COMMODITY_DETAILS_05 = "9";//商品详情5

    private NestedScrollView scrolleview;
    protected RelativeLayout mLlHorizontalPic;
    protected TextView mTv01;
    protected ShapeFrameLayout mSflHorizontalPic01;
    protected LinearLayout mLlAddHorizontalPic01;
    protected FrameLayout mFlHorizontalPic01;
    protected ImageView mIvHorizontalPic01;
    protected ImageView mIvDelete01;
    protected TextView mTvBanner01;
    protected ShapeFrameLayout mSflHorizontalPic02;
    protected LinearLayout mLlAddHorizontalPic02;
    protected FrameLayout mFlHorizontalPic02;
    protected ImageView mIvHorizontalPic02;
    protected ImageView mIvDelete02;
    protected TextView mTvBanner02;
    protected ShapeFrameLayout mSflHorizontalPic03;
    protected LinearLayout mLlAddHorizontalPic03;
    protected FrameLayout mFlHorizontalPic03;
    protected ImageView mIvHorizontalPic03;
    protected ImageView mIvDelete03;
    protected TextView mTvBanner03;
    protected ShapeLinearLayout mSllBannerTime;
    protected TextView mTvBannerTime;
    protected TextView mTvSetlectDrugs;
    protected LinearLayout mLlDrugsInfoContain;

    //西药中成药特有
    protected EditText mEtBrand, mEtSpecs, mEtPacking, mEtProdentpName, mEtApplyRange, mEtSort;
    protected TextView mTvType;

    protected LinearLayout mLlRelationDisetype;
    protected View mLineRelationDisetype01;
    protected View mLineRelationDisetype02;
    protected FrameLayout mFlAddRelationDisetype;
    protected CustomeRecyclerView mCrvRelationDisetype;
    protected FrameLayout mFlAddRelationDise;
    protected CustomeRecyclerView mCrvRelationDise;
    protected EditText mEtFunctions;
    protected EditText mEtCommonUsage;
    protected EditText mEtStorageConditions;
    //药品共用
    protected EditText mEtDrugName, mEtDrugProductName;
    protected EditText mEtDrugId;
    protected LinearLayout mLlPromotion;
    protected TextView mTvPromotionDescribe;
    protected ShapeFrameLayout mFlReducePrice;
    protected ShapeTextView mEtNumPrice;
    protected ShapeFrameLayout mFlAddPrice;
    protected TextView mEtPromotion;
    protected ShapeFrameLayout mFlReducePromotion;
    protected LinearLayout mLlEstimatePrice;
    protected View mLineEstimatePrice;
    protected ShapeTextView mEtNumPromotion;
    protected ShapeFrameLayout mFlAddPromotion;
    protected EditText mEtEstimatePrice;
    protected ShapeFrameLayout mFlReduceNum;
    protected ShapeTextView mEtNum;
    protected ShapeFrameLayout mFlAddNum;
    protected ShapeFrameLayout mFlReduceValidity;
    protected ShapeTextView mEtValidity;
    protected ShapeFrameLayout mFlAddValidity;
    protected LinearLayout mLlDrugManual;
    protected FrameLayout mFlDrugManual;
    protected ImageView mIvDrugManual;
    protected ImageView mIvDrugManualDelete;
    protected EditText mEtStoreSimilarity;
    protected LinearLayout mLlCommodityDetails01;
    protected FrameLayout mFlCommodityDetails01;
    protected ImageView mIvCommodityDetails01;
    protected ImageView mIvCommodityDetailsDelete01;
    protected LinearLayout mLlCommodityDetails02;
    protected FrameLayout mFlCommodityDetails02;
    protected ImageView mIvCommodityDetails02;
    protected ImageView mIvCommodityDetailsDelete02;
    protected LinearLayout mLlCommodityDetails03;
    protected FrameLayout mFlCommodityDetails03;
    protected ImageView mIvCommodityDetails03;
    protected ImageView mIvCommodityDetailsDelete03;
    protected LinearLayout mLlCommodityDetails04;
    protected FrameLayout mFlCommodityDetails04;
    protected ImageView mIvCommodityDetails04;
    protected ImageView mIvCommodityDetailsDelete04;
    protected LinearLayout mLlCommodityDetails05;
    protected FrameLayout mFlCommodityDetails05;
    protected ImageView mIvCommodityDetails05;
    protected ImageView mIvCommodityDetailsDelete05;
    //保存按钮
    private ShapeTextView mTvPull;

    protected CompressUploadSinglePicUtils_zhyf compressUploadSinglePicUtils;
    protected CustomerBottomListPop customerBottomListPop; //选择弹框
    protected RelationDisetypeAdapter relationDisetypeAdapter;//关联病种与诊断信息适配器
    protected RelationDiseAdapter relationDiseAdapter;//关联疾病适配器
    protected CustomerDialogUtils customerDialogUtils;
    protected AddRelationDisePop addRelationDisePop;//添加关联疾病弹框
    protected UpdateNumPop updateNumPop;
    protected DragStoreHouseModel.DataBean itemData;//选中 的药品数据

    protected String GoodsLocation, catalogueId, CatalogueName;

    @Override
    public int getLayoutId() {
        return R.layout.activity_special_area_add_goods;
    }

    @Override
    public void initView() {
        super.initView();
        scrolleview = findViewById(R.id.scrolleview);
        mLlHorizontalPic = findViewById(R.id.ll_horizontal_pic);
        mTv01 = findViewById(R.id.tv_01);
        mSflHorizontalPic01 = findViewById(R.id.sfl_horizontal_pic01);
        mLlAddHorizontalPic01 = findViewById(R.id.ll_add_horizontal_pic01);
        mFlHorizontalPic01 = findViewById(R.id.fl_horizontal_pic01);
        mIvHorizontalPic01 = findViewById(R.id.iv_horizontal_pic01);
        mIvDelete01 = findViewById(R.id.iv_delete01);
        mTvBanner01 = findViewById(R.id.tv_banner01);
        mSflHorizontalPic02 = findViewById(R.id.sfl_horizontal_pic02);
        mLlAddHorizontalPic02 = findViewById(R.id.ll_add_horizontal_pic02);
        mFlHorizontalPic02 = findViewById(R.id.fl_horizontal_pic02);
        mIvHorizontalPic02 = findViewById(R.id.iv_horizontal_pic02);
        mIvDelete02 = findViewById(R.id.iv_delete02);
        mTvBanner02 = findViewById(R.id.tv_banner02);
        mSflHorizontalPic03 = findViewById(R.id.sfl_horizontal_pic03);
        mLlAddHorizontalPic03 = findViewById(R.id.ll_add_horizontal_pic03);
        mFlHorizontalPic03 = findViewById(R.id.fl_horizontal_pic03);
        mIvHorizontalPic03 = findViewById(R.id.iv_horizontal_pic03);
        mIvDelete03 = findViewById(R.id.iv_delete03);
        mTvBanner03 = findViewById(R.id.tv_banner03);
        mSllBannerTime = findViewById(R.id.sll_banner_time);
        mTvBannerTime = findViewById(R.id.tv_banner_time);
        mTvSetlectDrugs = findViewById(R.id.tv_setlect_drugs);
        mLlDrugsInfoContain = findViewById(R.id.ll_drugs_info_contain);

        mTvPull = findViewById(R.id.tv_pull);
    }

    @Override
    public void initData() {
        GoodsLocation = getIntent().getStringExtra("GoodsLocation");
        catalogueId = getIntent().getStringExtra("CatalogueId");
        CatalogueName = getIntent().getStringExtra("CatalogueName");
        super.initData();
        mTvBannerTime.setTag("3");
        mTvBannerTime.setText("3S");
        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mLlAddHorizontalPic01.setOnClickListener(this);
        mIvDelete01.setOnClickListener(this);
        mLlAddHorizontalPic02.setOnClickListener(this);
        mIvDelete02.setOnClickListener(this);
        mLlAddHorizontalPic03.setOnClickListener(this);
        mIvDelete03.setOnClickListener(this);
        mIvHorizontalPic01.setOnClickListener(this);
        mIvHorizontalPic02.setOnClickListener(this);
        mIvHorizontalPic03.setOnClickListener(this);
        mSllBannerTime.setOnClickListener(this);
        mTvSetlectDrugs.setOnClickListener(this);

        if (mTvPull != null) {
            mTvPull.setOnClickListener(this);
        }
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        MyFlexboxLayoutManager flexboxLayoutManager = new MyFlexboxLayoutManager(this);
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        flexboxLayoutManager.setFlexWrap(FlexWrap.WRAP);
        flexboxLayoutManager.setJustifyContent(JustifyContent.FLEX_START);

        return flexboxLayoutManager;
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.ll_add_horizontal_pic01://banner 1号位
                KeyboardUtils.hideSoftInput(SpecialAreaAddGoodsActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_BANNER_01, CompressUploadSinglePicUtils_zhyf.PIC_BANNER_01_TAG);
                break;
            case R.id.iv_delete01://banner 1号位 删除
                mLlAddHorizontalPic01.setVisibility(View.VISIBLE);
                mLlAddHorizontalPic01.setEnabled(true);

                mIvHorizontalPic01.setTag(R.id.tag_1, "");
                mIvHorizontalPic01.setTag(R.id.tag_2, "");
                mFlHorizontalPic01.setVisibility(View.GONE);
                break;
            case R.id.ll_add_horizontal_pic02://banner 2号位
                KeyboardUtils.hideSoftInput(SpecialAreaAddGoodsActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_BANNER_02, CompressUploadSinglePicUtils_zhyf.PIC_BANNER_02_TAG);
                break;
            case R.id.iv_delete02://banner 2号位 删除
                mLlAddHorizontalPic02.setVisibility(View.VISIBLE);
                mLlAddHorizontalPic02.setEnabled(true);

                mIvHorizontalPic02.setTag(R.id.tag_1, "");
                mIvHorizontalPic02.setTag(R.id.tag_2, "");
                mFlHorizontalPic02.setVisibility(View.GONE);
                break;
            case R.id.ll_add_horizontal_pic03://banner 3号位
                KeyboardUtils.hideSoftInput(SpecialAreaAddGoodsActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_BANNER_03, CompressUploadSinglePicUtils_zhyf.PIC_BANNER_03_TAG);
                break;
            case R.id.iv_delete03://banner 3号位 删除
                mLlAddHorizontalPic03.setVisibility(View.VISIBLE);
                mLlAddHorizontalPic03.setEnabled(true);

                mIvHorizontalPic03.setTag(R.id.tag_1, "");
                mIvHorizontalPic03.setTag(R.id.tag_2, "");
                mFlHorizontalPic03.setVisibility(View.GONE);
                break;
            case R.id.tv_setlect_drugs://选择药品
                Intent intent = new Intent(SpecialAreaAddGoodsActivity.this, DrugStoreHouseActivity.class);
                intent.putExtra("from", 1);
                startActivityForResult(intent, new OnActivityCallback() {
                    @Override
                    public void onActivityResult(int resultCode, @Nullable Intent data) {
                        if (resultCode == RESULT_OK) {
                            itemData = data.getParcelableExtra("itemData");
                            if ("1".equals(itemData.getIsPlatformDrug())) {//平台药品库-西药中成药
                                addPlatformWestDragInfo(itemData);
                            } else {
                                switch (itemData.getDrugsClassification()) {
                                    case "1"://店铺药品库-西药中成药
                                        addShopWestDragInfo(itemData);
                                        break;
                                    case "2"://店铺药品库-医疗器械
                                        addShopApparatusDragInfo(itemData);
                                        break;
                                    case "3"://店铺药品库-成人用品
                                        addShopAdultproduceDragInfo(itemData);
                                        break;
                                    case "4"://店铺药品库-个人用品
                                        addShopPerproduceDragInfo(itemData);
                                        break;
                                }
                            }
                        }
                    }
                });
                break;
            case R.id.iv_horizontal_pic01://banner 1号位
                showBigPic(mIvHorizontalPic01,(String)mIvHorizontalPic01.getTag(R.id.tag_2));
                break;
            case R.id.iv_horizontal_pic02://banner 2号位
                showBigPic(mIvHorizontalPic02,(String)mIvHorizontalPic02.getTag(R.id.tag_2));
                break;
            case R.id.iv_horizontal_pic03://banner 3号位
                showBigPic(mIvHorizontalPic03,(String)mIvHorizontalPic03.getTag(R.id.tag_2));
                break;
            case R.id.iv_drug_manual:
                showBigPic(mIvDrugManual,(String)mIvDrugManual.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details01:
                showBigPic(mIvCommodityDetails01,(String)mIvCommodityDetails01.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details02:
                showBigPic(mIvCommodityDetails02,(String)mIvCommodityDetails02.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details03:
                showBigPic(mIvCommodityDetails03,(String)mIvCommodityDetails03.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details04:
                showBigPic(mIvCommodityDetails04,(String)mIvCommodityDetails04.getTag(R.id.tag_2));
                break;
            case R.id.iv_commodity_details05:
                showBigPic(mIvCommodityDetails05,(String)mIvCommodityDetails05.getTag(R.id.tag_2));
                break;
            case R.id.ll_drug_manual://药品说明书
                KeyboardUtils.hideSoftInput(SpecialAreaAddGoodsActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_DRUG_MANUAL, CompressUploadSinglePicUtils_zhyf.PIC_DRUG_MANUAL_TAG);
                break;
            case R.id.iv_drug_manual_delete://药品说明书 删除
                mLlDrugManual.setVisibility(View.VISIBLE);
                mLlDrugManual.setEnabled(true);

                mIvDrugManual.setTag(R.id.tag_1, "");
                mIvDrugManual.setTag(R.id.tag_2, "");
                mFlDrugManual.setVisibility(View.GONE);
                break;
            case R.id.ll_commodity_details01://商品详情1
                KeyboardUtils.hideSoftInput(SpecialAreaAddGoodsActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_COMMODITY_DETAILS_01, CompressUploadSinglePicUtils_zhyf.PIC_COMMODITY_DETAILS_TAG);
                break;
            case R.id.iv_commodity_details_delete01://商品详情1 删除
                mLlCommodityDetails01.setVisibility(View.VISIBLE);
                mLlCommodityDetails01.setEnabled(true);

                mIvCommodityDetails01.setTag(R.id.tag_1, "");
                mIvCommodityDetails01.setTag(R.id.tag_2, "");
                mFlCommodityDetails01.setVisibility(View.GONE);
                break;
            case R.id.ll_commodity_details02://商品详情2
                KeyboardUtils.hideSoftInput(SpecialAreaAddGoodsActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_COMMODITY_DETAILS_02, CompressUploadSinglePicUtils_zhyf.PIC_COMMODITY_DETAILS_TAG);
                break;
            case R.id.iv_commodity_details_delete02://商品详情2 删除
                mLlCommodityDetails02.setVisibility(View.VISIBLE);
                mLlCommodityDetails02.setEnabled(true);

                mIvCommodityDetails02.setTag(R.id.tag_1, "");
                mIvCommodityDetails02.setTag(R.id.tag_2, "");
                mFlCommodityDetails02.setVisibility(View.GONE);
                break;
            case R.id.ll_commodity_details03://商品详情3
                KeyboardUtils.hideSoftInput(SpecialAreaAddGoodsActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_COMMODITY_DETAILS_03, CompressUploadSinglePicUtils_zhyf.PIC_COMMODITY_DETAILS_TAG);
                break;
            case R.id.iv_commodity_details_delete03://商品详情3 删除
                mLlCommodityDetails03.setVisibility(View.VISIBLE);
                mLlCommodityDetails03.setEnabled(true);

                mIvCommodityDetails03.setTag(R.id.tag_1, "");
                mIvCommodityDetails03.setTag(R.id.tag_2, "");
                mFlCommodityDetails03.setVisibility(View.GONE);
                break;
            case R.id.ll_commodity_details04://商品详情4
                KeyboardUtils.hideSoftInput(SpecialAreaAddGoodsActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_COMMODITY_DETAILS_04, CompressUploadSinglePicUtils_zhyf.PIC_COMMODITY_DETAILS_TAG);
                break;
            case R.id.iv_commodity_details_delete04://商品详情4 删除
                mLlCommodityDetails04.setVisibility(View.VISIBLE);
                mLlCommodityDetails04.setEnabled(true);

                mIvCommodityDetails04.setTag(R.id.tag_1, "");
                mIvCommodityDetails04.setTag(R.id.tag_2, "");
                mFlCommodityDetails04.setVisibility(View.GONE);
                break;
            case R.id.ll_commodity_details05://商品详情5
                KeyboardUtils.hideSoftInput(SpecialAreaAddGoodsActivity.this);
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_COMMODITY_DETAILS_05, CompressUploadSinglePicUtils_zhyf.PIC_COMMODITY_DETAILS_TAG);
                break;
            case R.id.iv_commodity_details_delete05://商品详情5 删除
                mLlCommodityDetails05.setVisibility(View.VISIBLE);
                mLlCommodityDetails05.setEnabled(true);

                mIvCommodityDetails05.setTag(R.id.tag_1, "");
                mIvCommodityDetails05.setTag(R.id.tag_2, "");
                mFlCommodityDetails05.setVisibility(View.GONE);
                break;
            case R.id.sll_banner_time://选择轮播间隔时间
                KeyboardUtils.hideSoftInput(SpecialAreaAddGoodsActivity.this);
                if (customerBottomListPop == null) {
                    customerBottomListPop = new CustomerBottomListPop(SpecialAreaAddGoodsActivity.this, SpecialAreaAddGoodsActivity.this);
                }
                customerBottomListPop.setDatas("1", "", mTvBannerTime.getText().toString(), CommonlyUsedDataUtils.getInstance().getBannerTimeData());
                customerBottomListPop.showPopupWindow();
                break;
            case R.id.tv_type://选择药品类型
                KeyboardUtils.hideSoftInput(SpecialAreaAddGoodsActivity.this);
                if (customerBottomListPop == null) {
                    customerBottomListPop = new CustomerBottomListPop(SpecialAreaAddGoodsActivity.this, SpecialAreaAddGoodsActivity.this);
                }
                customerBottomListPop.setDatas("2", "", mTvType.getText().toString(), CommonlyUsedDataUtils.getInstance().getDrugTypeData());
                customerBottomListPop.showPopupWindow();
                break;
            case R.id.et_promotion://选择是否促销
                KeyboardUtils.hideSoftInput(SpecialAreaAddGoodsActivity.this);
                if (customerBottomListPop == null) {
                    customerBottomListPop = new CustomerBottomListPop(SpecialAreaAddGoodsActivity.this, SpecialAreaAddGoodsActivity.this);
                }
                customerBottomListPop.setDatas("3", "", mEtPromotion.getText().toString(), CommonlyUsedDataUtils.getInstance().getPromotionData());
                customerBottomListPop.showPopupWindow();
                break;
            case R.id.fl_add_relation_disetype://关联病种与诊断信息
                if (relationDisetypeAdapter != null && relationDisetypeAdapter.getData().size() >= 5) {
                    showShortToast("最多只能选择5个关联病种与诊断信息");
                    return;
                }

                Intent intent02 = new Intent(SpecialAreaAddGoodsActivity.this, SelectMultDiseCataActivity.class);
                intent02.putExtra("selectSize", relationDisetypeAdapter.getData().size());
                startActivityForResult(intent02, new OnActivityCallback() {
                    @Override
                    public void onActivityResult(int resultCode, @Nullable Intent data) {
                        if (resultCode == RESULT_OK) {
                            ArrayList<DiseCataModel.DataBean> selectDatas = data.getParcelableArrayListExtra("selectDatas");
                            if (selectDatas == null || selectDatas.isEmpty()) return;

                            List<DiseCataModel.DataBean> hasDatas = relationDisetypeAdapter.getData();
                            for (DiseCataModel.DataBean hasData : hasDatas) {
                                Iterator<DiseCataModel.DataBean> iterator = selectDatas.iterator();
                                while (iterator.hasNext()) {
                                    if (hasData.getCode().equals(iterator.next().getCode())) {
                                        iterator.remove();
                                    }
                                }
                            }
                            if (hasDatas == null) {
                                hasDatas = new ArrayList<>();
                            }
                            hasDatas.addAll(selectDatas);
                            relationDisetypeAdapter.notifyDataSetChanged();
                        }
                    }
                });
                break;
            case R.id.fl_add_relation_dise://添加关联疾病
                if (relationDiseAdapter == null) {
                    showShortToast("数据初始化错误,请重新进入页面");
                    return;
                }
                if (relationDiseAdapter.getData().size() >= 5) {
                    showShortToast("关联疾病最多可添加5个");
                    return;
                }

                if (addRelationDisePop == null) {
                    addRelationDisePop = new AddRelationDisePop(SpecialAreaAddGoodsActivity.this, "添加关联疾病");
                    addRelationDisePop.setOnListener(new AddRelationDisePop.IOptionListener() {
                        @Override
                        public void onAgree(String data) {
                            if (EmptyUtils.isEmpty(data)) {
                                showShortToast("请输入疾病名称");
                                return;
                            }
                            addRelationDisePop.dismiss();
                            relationDiseAdapter.getData().add(data);
                            relationDiseAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancle() {
                        }
                    });
                }
                addRelationDisePop.cleanData();
                addRelationDisePop.setOutSideDismiss(false).showPopupWindow();
                break;
            case R.id.fl_reduce_price://药品价格 减
                reducePrice();
                break;
            case R.id.et_num_price://设置药品价格
                setPrice();
                break;
            case R.id.fl_add_price://药品价格 加
                addPrice();
                break;
            case R.id.fl_reduce_promotion://促销折扣 减
                reducePromotion();
                break;
            case R.id.et_num_promotion://设置促销折扣
                setPromotion();
                break;
            case R.id.fl_add_promotion://促销折扣 加
                addPromotion();
                break;
            case R.id.fl_reduce_num://库存数量 减
                reduceNum();
                break;
            case R.id.et_num://设置库存数量
                setNum();
                break;
            case R.id.fl_add_num://库存数量 加
                addNum();
                break;

            case R.id.fl_reduce_validity://有效期 减
                reduceValidity();
                break;
            case R.id.et_validity://设置有效期
                setValidity();
                break;
            case R.id.fl_add_validity://有效期 加
                addValidity();
                break;
            case R.id.tv_pull://上架
                onPull();
                break;
        }
    }

    @Override
    public void onItemClick(String tag, StoreStatusModel str) {
        switch (tag) {
            case "1"://选择轮播间隔时间
                mTvBannerTime.setTag(str.getId());
                mTvBannerTime.setText(EmptyUtils.strEmpty(str.getText()));
                break;
            case "2"://选择药品类型
                mTvType.setTag(str.getId());
                mTvType.setText(EmptyUtils.strEmpty(str.getText()));
                break;
            case "3"://选择是否促销
                mEtPromotion.setTag(str.getId());
                mEtPromotion.setText(EmptyUtils.strEmpty(str.getText()));

                estimatePriceVisibility();
                calculEstimatePrice();
                break;
        }
    }

    //展示大图
    private void showBigPic(ImageView imageView,String imageUrl) {
        OpenImage.with(SpecialAreaAddGoodsActivity.this)
                //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                .setClickImageView(imageView)
                //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                //RecyclerView的数据
                .setImageUrl(imageUrl, MediaType.IMAGE)
                //点击的ImageView所在数据的位置
                .setClickPosition(0)
                //开始展示大图
                .show();
    }

    //------------------------选择药品之后展示的布局----------------------------------------
    //添加药品  平台药品库-西药中成药
    protected void addPlatformWestDragInfo(DragStoreHouseModel.DataBean itemData) {
        mLlDrugsInfoContain.removeAllViews();
        LayoutInflater.from(this).inflate(R.layout.layout_add_goods_platform_west, mLlDrugsInfoContain, true);

        mEtSpecs = findViewById(R.id.et_specs);
        mTvType = findViewById(R.id.tv_type);

        mLlRelationDisetype = findViewById(R.id.ll_relation_disetype);
        mLineRelationDisetype01 = findViewById(R.id.line_relation_disetype01);
        mLineRelationDisetype02 = findViewById(R.id.line_relation_disetype02);
        mFlAddRelationDisetype = findViewById(R.id.fl_add_relation_disetype);
        mCrvRelationDisetype = findViewById(R.id.crv_relation_disetype);

        mFlAddRelationDise = findViewById(R.id.fl_add_relation_dise);
        mCrvRelationDise = findViewById(R.id.crv_relation_dise);
        mEtFunctions = findViewById(R.id.et_functions);
        mEtCommonUsage = findViewById(R.id.et_common_usage);
        mEtStorageConditions = findViewById(R.id.et_storage_conditions);
        addDragPublicLayout(itemData);

        //设置数据
        mEtSpecs.setText(EmptyUtils.strEmpty(itemData.getSpec()));

        if (EmptyUtils.isEmpty(itemData.getItemType()) || "0".equals(itemData.getItemType())) {
            mTvType.setText("");
        } else {
            mTvType.setTag(itemData.getItemType());
            mTvType.setText("1".equals(itemData.getItemType()) ? "非处方药（OTC）" : "处方药");
        }

        mEtFunctions.setText(EmptyUtils.strEmpty(itemData.getEfccAtd()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsualWay()));
        mEtStorageConditions.setText(EmptyUtils.strEmpty(itemData.getStorageConditions()));

        if ("1".equals(MechanismInfoUtils.getFixmedinsType())) {//医院
            mLlRelationDisetype.setVisibility(View.VISIBLE);
            mLineRelationDisetype01.setVisibility(View.VISIBLE);
            mLineRelationDisetype02.setVisibility(View.VISIBLE);
            mCrvRelationDisetype.setVisibility(View.VISIBLE);
        } else if ("2".equals(MechanismInfoUtils.getFixmedinsType())) {//药店
            mLlRelationDisetype.setVisibility(View.GONE);
            mLineRelationDisetype01.setVisibility(View.GONE);
            mLineRelationDisetype02.setVisibility(View.GONE);
            mCrvRelationDisetype.setVisibility(View.GONE);
        }

        //关联病种与诊断信息
        mCrvRelationDisetype.setHasFixedSize(true);
        mCrvRelationDisetype.setLayoutManager(getLayoutManager());
        relationDisetypeAdapter = new RelationDisetypeAdapter();
        mCrvRelationDisetype.setAdapter(relationDisetypeAdapter);
        //关联病种与诊断信息的数据
        List<DiseCataModel.DataBean> dataBeans = JSON.parseArray(itemData.getAssociatedDiagnostics(), DiseCataModel.DataBean.class);
        relationDisetypeAdapter.setNewData(dataBeans);
        //关联疾病
        mCrvRelationDise.setHasFixedSize(true);
        mCrvRelationDise.setLayoutManager(getLayoutManager());
        relationDiseAdapter = new RelationDiseAdapter();
        mCrvRelationDise.setAdapter(relationDiseAdapter);
        //关联疾病的数据
        if (!EmptyUtils.isEmpty(itemData.getAssociatedDiseases())) {
            String[] associatedDiseases = itemData.getAssociatedDiseases().split("∞#");
            relationDiseAdapter.setNewData(new ArrayList<>(Arrays.asList(associatedDiseases)));
        }

        //关联病种与诊断信息 item点击
        mFlAddRelationDisetype.setOnClickListener(this);
        relationDisetypeAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
//                if (relationDisetypeAdapter.getData().size() <= 1) {
//                    showShortToast("至少保留一个关联病种与诊断信息");
//                    return;
//                }
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(SpecialAreaAddGoodsActivity.this, "提示", "确定要删除么？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        baseQuickAdapter.getData().remove(i);
                        baseQuickAdapter.notifyDataSetChanged();
                    }
                });
            }
        });

        mTvType.setOnClickListener(this);
        mFlAddRelationDise.setOnClickListener(this);
        //关联疾病 item点击
        relationDiseAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
//                if (relationDiseAdapter.getData().size() <= 1) {
//                    showShortToast("至少保留一个关联疾病");
//                    return;
//                }
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(SpecialAreaAddGoodsActivity.this, "提示", "确定要删除么？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        baseQuickAdapter.getData().remove(i);
                        baseQuickAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    //添加药品 店铺药品库-西药中成药
    protected void addShopWestDragInfo(DragStoreHouseModel.DataBean itemData) {
        mLlDrugsInfoContain.removeAllViews();
        LayoutInflater.from(this).inflate(R.layout.layout_add_goods_shop_west, mLlDrugsInfoContain, true);

        mEtSpecs = findViewById(R.id.et_specs);
        mTvType = findViewById(R.id.tv_type);

        mFlAddRelationDise = findViewById(R.id.fl_add_relation_dise);
        mCrvRelationDise = findViewById(R.id.crv_relation_dise);
        mEtFunctions = findViewById(R.id.et_functions);
        mEtCommonUsage = findViewById(R.id.et_common_usage);

        mEtStorageConditions = findViewById(R.id.et_storage_conditions);
        addDragPublicLayout(itemData);

        //设置数据
        mEtSpecs.setText(EmptyUtils.strEmpty(itemData.getSpec()));
        if (EmptyUtils.isEmpty(itemData.getItemType()) || "0".equals(itemData.getItemType())) {
            mTvType.setText("");
        } else {
            mTvType.setTag(itemData.getItemType());
            mTvType.setText("1".equals(itemData.getItemType()) ? "非处方药（OTC）" : "处方药");
        }
        mEtFunctions.setText(EmptyUtils.strEmpty(itemData.getEfccAtd()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsualWay()));
        mEtStorageConditions.setText(EmptyUtils.strEmpty(itemData.getStorageConditions()));

        //关联疾病
        mCrvRelationDise.setHasFixedSize(true);
        mCrvRelationDise.setLayoutManager(getLayoutManager());
        relationDiseAdapter = new RelationDiseAdapter();
        mCrvRelationDise.setAdapter(relationDiseAdapter);
        //关联疾病的数据
        if (!EmptyUtils.isEmpty(itemData.getAssociatedDiseases())) {
            String[] associatedDiseases = itemData.getAssociatedDiseases().split("∞#");
            relationDiseAdapter.setNewData(new ArrayList<>(Arrays.asList(associatedDiseases)));
        }

        mTvType.setOnClickListener(this);
        mFlAddRelationDise.setOnClickListener(this);
        //关联疾病 item点击
        relationDiseAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
//                if (relationDiseAdapter.getData().size() <= 1) {
//                    showShortToast("至少保留一个关联疾病");
//                    return;
//                }
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(SpecialAreaAddGoodsActivity.this, "提示", "确定要删除么？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        baseQuickAdapter.getData().remove(i);
                        baseQuickAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    //添加药品 店铺药品库-医疗器械
    protected void addShopApparatusDragInfo(DragStoreHouseModel.DataBean itemData) {
        mLlDrugsInfoContain.removeAllViews();
        LayoutInflater.from(this).inflate(R.layout.layout_add_goods_shop_apparatus, mLlDrugsInfoContain, true);

        mEtBrand = findViewById(R.id.et_brand);
        mEtSpecs = findViewById(R.id.et_specs);
        mEtPacking = findViewById(R.id.et_packing);
        mEtProdentpName = findViewById(R.id.et_prodentp_name);
        mEtApplyRange = findViewById(R.id.et_apply_range);
        mEtCommonUsage = findViewById(R.id.et_common_usage);

        addDragPublicLayout(itemData);

        //设置数据
        mEtBrand.setText(EmptyUtils.strEmpty(itemData.getBrand()));
        mEtSpecs.setText(EmptyUtils.strEmpty(itemData.getSpec()));
        mEtPacking.setText(EmptyUtils.strEmpty(itemData.getPackaging()));
        mEtProdentpName.setText(EmptyUtils.strEmpty(itemData.getEnterpriseName()));
        mEtApplyRange.setText(EmptyUtils.strEmpty(itemData.getApplicableScope()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsageDosage()));
    }

    //添加药品 店铺药品库-成人用品
    protected void addShopAdultproduceDragInfo(DragStoreHouseModel.DataBean itemData) {
        mLlDrugsInfoContain.removeAllViews();
        LayoutInflater.from(this).inflate(R.layout.layout_add_goods_shop_adultproduct, mLlDrugsInfoContain, true);

        mEtBrand = findViewById(R.id.et_brand);
        mEtSpecs = findViewById(R.id.et_specs);
        mEtPacking = findViewById(R.id.et_packing);
        mEtProdentpName = findViewById(R.id.et_prodentp_name);
        mEtCommonUsage = findViewById(R.id.et_common_usage);
        mEtStorageConditions = findViewById(R.id.et_storage_conditions);

        addDragPublicLayout(itemData);

        //设置数据
        mEtBrand.setText(EmptyUtils.strEmpty(itemData.getBrand()));
        mEtSpecs.setText(EmptyUtils.strEmpty(itemData.getSpec()));
        mEtPacking.setText(EmptyUtils.strEmpty(itemData.getPackaging()));
        mEtProdentpName.setText(EmptyUtils.strEmpty(itemData.getEnterpriseName()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsagemethod()));
        mEtStorageConditions.setText(EmptyUtils.strEmpty(itemData.getStorageConditions()));
    }

    //添加药品  店铺药品库-个人用品
    protected void addShopPerproduceDragInfo(DragStoreHouseModel.DataBean itemData) {
        mLlDrugsInfoContain.removeAllViews();
        LayoutInflater.from(this).inflate(R.layout.layout_add_goods_shop_perproduct, mLlDrugsInfoContain, true);

        mEtBrand = findViewById(R.id.et_brand);
        mEtSpecs = findViewById(R.id.et_specs);
        mEtPacking = findViewById(R.id.et_packing);
        mEtProdentpName = findViewById(R.id.et_prodentp_name);
        mEtCommonUsage = findViewById(R.id.et_common_usage);
        mEtStorageConditions = findViewById(R.id.et_storage_conditions);

        addDragPublicLayout(itemData);

        //设置数据
        mEtBrand.setText(EmptyUtils.strEmpty(itemData.getBrand()));
        mEtSpecs.setText(EmptyUtils.strEmpty(itemData.getSpec()));
        mEtPacking.setText(EmptyUtils.strEmpty(itemData.getPackaging()));
        mEtProdentpName.setText(EmptyUtils.strEmpty(itemData.getEnterpriseName()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsageDosage()));
        mEtStorageConditions.setText(EmptyUtils.strEmpty(itemData.getStorageConditions()));
    }

    //添加药品--公共的布局
    protected void addDragPublicLayout(DragStoreHouseModel.DataBean itemData) {
        mEtDrugName = findViewById(R.id.et_drug_name);
        mEtDrugId = findViewById(R.id.et_drug_id);
        mEtDrugProductName = findViewById(R.id.et_drug_product_name);
        mEtSort = findViewById(R.id.et_sort);
        //药品价格 价格
        mFlReducePrice = findViewById(R.id.fl_reduce_price);
        mEtNumPrice = findViewById(R.id.et_num_price);
        mFlAddPrice = findViewById(R.id.fl_add_price);
        //是否促销 促销折扣
        mEtPromotion = findViewById(R.id.et_promotion);
        mLlPromotion = findViewById(R.id.ll_promotion);
        mTvPromotionDescribe = findViewById(R.id.tv_promotion_describe);
        mFlReducePromotion = findViewById(R.id.fl_reduce_promotion);
        mEtNumPromotion = findViewById(R.id.et_num_promotion);
        mFlAddPromotion = findViewById(R.id.fl_add_promotion);
        //预估到手价
        mLlEstimatePrice = findViewById(R.id.ll_estimate_price);
        mLineEstimatePrice = findViewById(R.id.line_estimate_price);
        mEtEstimatePrice = findViewById(R.id.et_estimate_price);
        //库存数量
        mFlReduceNum = findViewById(R.id.fl_reduce_num);
        mEtNum = findViewById(R.id.et_num);
        mFlAddNum = findViewById(R.id.fl_add_num);
        //有效期
        mFlReduceValidity = findViewById(R.id.fl_reduce_validity);
        mEtValidity = findViewById(R.id.et_validity);
        mFlAddValidity = findViewById(R.id.fl_add_validity);
        //说明书 店内相似 商品详情
        mLlDrugManual = findViewById(R.id.ll_drug_manual);
        mFlDrugManual = findViewById(R.id.fl_drug_manual);
        mIvDrugManual = findViewById(R.id.iv_drug_manual);
        mIvDrugManualDelete = findViewById(R.id.iv_drug_manual_delete);
        mEtStoreSimilarity = findViewById(R.id.et_store_similarity);
        mLlCommodityDetails01 = findViewById(R.id.ll_commodity_details01);
        mFlCommodityDetails01 = findViewById(R.id.fl_commodity_details01);
        mIvCommodityDetails01 = findViewById(R.id.iv_commodity_details01);
        mIvCommodityDetailsDelete01 = findViewById(R.id.iv_commodity_details_delete01);
        mLlCommodityDetails02 = findViewById(R.id.ll_commodity_details02);
        mFlCommodityDetails02 = findViewById(R.id.fl_commodity_details02);
        mIvCommodityDetails02 = findViewById(R.id.iv_commodity_details02);
        mIvCommodityDetailsDelete02 = findViewById(R.id.iv_commodity_details_delete02);
        mLlCommodityDetails03 = findViewById(R.id.ll_commodity_details03);
        mFlCommodityDetails03 = findViewById(R.id.fl_commodity_details03);
        mIvCommodityDetails03 = findViewById(R.id.iv_commodity_details03);
        mIvCommodityDetailsDelete03 = findViewById(R.id.iv_commodity_details_delete03);
        mLlCommodityDetails04 = findViewById(R.id.ll_commodity_details04);
        mFlCommodityDetails04 = findViewById(R.id.fl_commodity_details04);
        mIvCommodityDetails04 = findViewById(R.id.iv_commodity_details04);
        mIvCommodityDetailsDelete04 = findViewById(R.id.iv_commodity_details_delete04);
        mLlCommodityDetails05 = findViewById(R.id.ll_commodity_details05);
        mFlCommodityDetails05 = findViewById(R.id.fl_commodity_details05);
        mIvCommodityDetails05 = findViewById(R.id.iv_commodity_details05);
        mIvCommodityDetailsDelete05 = findViewById(R.id.iv_commodity_details_delete05);

        //设置数据
        mEtDrugName.setText(EmptyUtils.strEmpty(itemData.getItemName()));
        mEtDrugId.setText(EmptyUtils.strEmpty(itemData.getItemCode()));
        mEtDrugProductName.setText(EmptyUtils.isEmpty(itemData.getGoodsName()) ? EmptyUtils.strEmpty(itemData.getItemName()) : itemData.getGoodsName());
        mEtSort.setText(EmptyUtils.strEmpty(itemData.getDrugsClassificationName()));
        mEtNumPrice.setText(EmptyUtils.strEmpty(itemData.getPrice()));
        //===============================注意=============================================
        mEtPromotion.setTag(EmptyUtils.strEmptyToText(itemData.getIsPromotion(),"1"));
        mEtPromotion.setText("1".equals(EmptyUtils.strEmptyToText(itemData.getIsPromotion(),"1"))?"是":"否");
        estimatePriceVisibility();
        calculEstimatePrice();
        //================================注意============================================
        //库存数量
        mEtNum.setText(EmptyUtils.strEmpty(itemData.getInventoryQuantity()));
        //有效期
        mEtValidity.setText(EmptyUtils.strEmpty(itemData.getExpiryDateCount()));

        //banner数据
        if (!EmptyUtils.isEmpty(itemData.getBannerAccessoryUrl1())) {
            GlideUtils.getImageWidHeig(this, itemData.getBannerAccessoryUrl1(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner01Success(itemData.getBannerAccessoryId1(), itemData.getBannerAccessoryUrl1(), width, height);
                }
            });
        }else {
            mLlAddHorizontalPic01.setVisibility(View.VISIBLE);
            mLlAddHorizontalPic01.setEnabled(true);

            mIvHorizontalPic01.setTag(R.id.tag_1, "");
            mIvHorizontalPic01.setTag(R.id.tag_2, "");
            mFlHorizontalPic01.setVisibility(View.GONE);
        }
        if (!EmptyUtils.isEmpty(itemData.getBannerAccessoryUrl2())) {
            GlideUtils.getImageWidHeig(this, itemData.getBannerAccessoryUrl2(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner02Success(itemData.getBannerAccessoryId2(), itemData.getBannerAccessoryUrl2(), width, height);
                }
            });
        }else {
            mLlAddHorizontalPic02.setVisibility(View.VISIBLE);
            mLlAddHorizontalPic02.setEnabled(true);

            mIvHorizontalPic02.setTag(R.id.tag_1, "");
            mIvHorizontalPic02.setTag(R.id.tag_2, "");
            mFlHorizontalPic02.setVisibility(View.GONE);
        }
        if (!EmptyUtils.isEmpty(itemData.getBannerAccessoryUrl3())) {
            GlideUtils.getImageWidHeig(this, itemData.getBannerAccessoryUrl3(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner03Success(itemData.getBannerAccessoryId3(), itemData.getBannerAccessoryUrl3(), width, height);
                }
            });
        }else {
            mLlAddHorizontalPic03.setVisibility(View.VISIBLE);
            mLlAddHorizontalPic03.setEnabled(true);

            mIvHorizontalPic03.setTag(R.id.tag_1, "");
            mIvHorizontalPic03.setTag(R.id.tag_2, "");
            mFlHorizontalPic03.setVisibility(View.GONE);
        }
        //说明书 商品详情
        if (!EmptyUtils.isEmpty(itemData.getInstructionsAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, itemData.getInstructionsAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectDrugManualSuccess(itemData.getInstructionsAccessoryId(), itemData.getInstructionsAccessoryUrl(), width, height);
                }
            });
        }else {
            mLlDrugManual.setVisibility(View.VISIBLE);
            mLlDrugManual.setEnabled(true);

            mIvDrugManual.setTag(R.id.tag_1, "");
            mIvDrugManual.setTag(R.id.tag_2, "");
            mFlDrugManual.setVisibility(View.GONE);
        }

        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl1())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl1(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectCommodityDetails01Success(itemData.getDetailAccessoryId1(), itemData.getDetailAccessoryUrl1(), width, height);
                }
            });
        }else {
            mLlCommodityDetails01.setVisibility(View.VISIBLE);
            mLlCommodityDetails01.setEnabled(true);

            mIvCommodityDetails01.setTag(R.id.tag_1, "");
            mIvCommodityDetails01.setTag(R.id.tag_2, "");
            mFlCommodityDetails01.setVisibility(View.GONE);
        }
        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl2())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl2(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectCommodityDetails02Success(itemData.getDetailAccessoryId2(), itemData.getDetailAccessoryUrl2(), width, height);
                }
            });
        }else {
            mLlCommodityDetails02.setVisibility(View.VISIBLE);
            mLlCommodityDetails02.setEnabled(true);

            mIvCommodityDetails02.setTag(R.id.tag_1, "");
            mIvCommodityDetails02.setTag(R.id.tag_2, "");
            mFlCommodityDetails02.setVisibility(View.GONE);
        }
        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl3())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl3(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectCommodityDetails03Success(itemData.getDetailAccessoryId3(), itemData.getDetailAccessoryUrl3(), width, height);
                }
            });
        }else {
            mLlCommodityDetails03.setVisibility(View.VISIBLE);
            mLlCommodityDetails03.setEnabled(true);

            mIvCommodityDetails03.setTag(R.id.tag_1, "");
            mIvCommodityDetails03.setTag(R.id.tag_2, "");
            mFlCommodityDetails03.setVisibility(View.GONE);
        }
        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl4())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl4(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectCommodityDetails04Success(itemData.getDetailAccessoryId4(), itemData.getDetailAccessoryUrl4(), width, height);
                }
            });
        }else {
            mLlCommodityDetails04.setVisibility(View.VISIBLE);
            mLlCommodityDetails04.setEnabled(true);

            mIvCommodityDetails04.setTag(R.id.tag_1, "");
            mIvCommodityDetails04.setTag(R.id.tag_2, "");
            mFlCommodityDetails04.setVisibility(View.GONE);
        }
        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl5())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl5(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectCommodityDetails05Success(itemData.getDetailAccessoryId5(), itemData.getDetailAccessoryUrl5(), width, height);
                }
            });
        }else {
            mLlCommodityDetails05.setVisibility(View.VISIBLE);
            mLlCommodityDetails05.setEnabled(true);

            mIvCommodityDetails05.setTag(R.id.tag_1, "");
            mIvCommodityDetails05.setTag(R.id.tag_2, "");
            mFlCommodityDetails05.setVisibility(View.GONE);
        }

        mEtPromotion.setOnClickListener(this);
        //药品价格 减 设置 加 点击事件
        mFlReducePrice.setOnClickListener(this);
        mEtNumPrice.setOnClickListener(this);
        mFlAddPrice.setOnClickListener(this);
        //促销折扣 减 设置 加 点击事件
        mFlReducePromotion.setOnClickListener(this);
        mEtNumPromotion.setOnClickListener(this);
        mFlAddPromotion.setOnClickListener(this);

        //库存数量 减 设置 加 点击事件
        mFlReduceNum.setOnClickListener(this);
        mEtNum.setOnClickListener(this);
        mFlAddNum.setOnClickListener(this);

        //有效期 减 设置 加 点击事件
        mFlReduceValidity.setOnClickListener(this);
        mEtValidity.setOnClickListener(this);
        mFlAddValidity.setOnClickListener(this);

        //选择图片上传
        mLlDrugManual.setOnClickListener(this);
        mIvDrugManualDelete.setOnClickListener(this);
        mLlCommodityDetails01.setOnClickListener(this);
        mIvCommodityDetailsDelete01.setOnClickListener(this);
        mLlCommodityDetails02.setOnClickListener(this);
        mIvCommodityDetailsDelete02.setOnClickListener(this);
        mLlCommodityDetails03.setOnClickListener(this);
        mIvCommodityDetailsDelete03.setOnClickListener(this);
        mLlCommodityDetails04.setOnClickListener(this);
        mIvCommodityDetailsDelete04.setOnClickListener(this);
        mLlCommodityDetails05.setOnClickListener(this);
        mIvCommodityDetailsDelete05.setOnClickListener(this);
        mIvDrugManual.setOnClickListener(this);
        mIvCommodityDetails01.setOnClickListener(this);
        mIvCommodityDetails02.setOnClickListener(this);
        mIvCommodityDetails03.setOnClickListener(this);
        mIvCommodityDetails04.setOnClickListener(this);
        mIvCommodityDetails05.setOnClickListener(this);
    }

    //------------------------药品价格----------------------------------------
    //药品价格 减 药品价格和促销折扣不能为0，当小于1的时候点不了“减号”
    private void reducePrice() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mEtNumPrice.getText().toString())) {
            mEtNumPrice.setText("0");
        } else if (new BigDecimal(mEtNumPrice.getText().toString()).doubleValue() > 1) {
            mEtNumPrice.setText(new BigDecimal(mEtNumPrice.getText().toString()).subtract(new BigDecimal("1")).toPlainString());
        } else {
            showShortToast("药品价格不能为0");
            hideWaitDialog();
            return;
        }

        calculEstimatePrice();
        hideWaitDialog();
    }

    //设置药品价格
    private void setPrice() {
        if (updateNumPop == null) {
            updateNumPop = new UpdateNumPop(SpecialAreaAddGoodsActivity.this);
        }
        updateNumPop.setOnListener(new UpdateNumPop.IOptionListener() {
            @Override
            public void onAgree(String data) {
                if (!RxTool.isPrice(data)) {
                    showShortToast("请输入正确的金额");
                    return;
                }

                mEtNumPrice.setText(data);
                calculEstimatePrice();
            }

            @Override
            public void onCancle() {
            }
        });
        updateNumPop.setFilters(2);
        updateNumPop.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
        updateNumPop.setContent(EmptyUtils.strEmptyToText(mEtNumPrice.getText().toString(), "0"));
        updateNumPop.setOutSideDismiss(false).showPopupWindow();
        KeyboardUtils.showSoftInput(SpecialAreaAddGoodsActivity.this);
    }

    //药品价格 加
    private void addPrice() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mEtNumPrice.getText().toString())) {
            mEtNumPrice.setText("0");
        } else {
            mEtNumPrice.setText(new BigDecimal(mEtNumPrice.getText().toString()).add(new BigDecimal("1")).toPlainString());
        }
        calculEstimatePrice();
        hideWaitDialog();
    }

    //------------------------促销折扣----------------------------------------
    //促销折扣 减 药品价格和促销折扣不能为0，当小于1的时候点不了“减号”
    private void reducePromotion() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mEtNumPromotion.getText().toString())) {
            mEtNumPromotion.setText("0");
        } else if (new BigDecimal(mEtNumPromotion.getText().toString()).doubleValue() > 1) {
            mEtNumPromotion.setText(new BigDecimal(mEtNumPromotion.getText().toString()).subtract(new BigDecimal("1")).toPlainString());
        } else {
            showShortToast("促销折扣不能为0");
            hideWaitDialog();
            return;
        }
        calculEstimatePrice();
        hideWaitDialog();
    }

    //设置促销折扣
    private void setPromotion() {
        if (updateNumPop == null) {
            updateNumPop = new UpdateNumPop(SpecialAreaAddGoodsActivity.this);
        }
        updateNumPop.setOnListener(new UpdateNumPop.IOptionListener() {
            @Override
            public void onAgree(String data) {
                if (new BigDecimal(data).doubleValue() >= 9.9) {
                    showShortToast("折扣最大值为9.9");
                    mEtNumPromotion.setText("9.9");
                } else {
                    mEtNumPromotion.setText(data);
                }
                calculEstimatePrice();
                hideWaitDialog();
            }

            @Override
            public void onCancle() {
            }
        });
        updateNumPop.setFilters(1);
        updateNumPop.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
        updateNumPop.setContent(EmptyUtils.strEmptyToText(mEtNumPromotion.getText().toString(), "0"));
        updateNumPop.setOutSideDismiss(false).showPopupWindow();
        KeyboardUtils.showSoftInput(SpecialAreaAddGoodsActivity.this);
    }

    //促销折扣 加
    private void addPromotion() {
        showWaitDialog();
        double addPromotionValue = new BigDecimal(EmptyUtils.strEmptyToText(mEtNumPromotion.getText().toString(), "0")).add(new BigDecimal("1")).doubleValue();
        if (addPromotionValue >= 9.9) {
            showShortToast("已达到折扣最大值");
            mEtNumPromotion.setText("9.9");
            hideWaitDialog();
            return;
        } else {
            mEtNumPromotion.setText(String.valueOf(addPromotionValue));
        }
        calculEstimatePrice();
        hideWaitDialog();
    }

    //------------------------库存数量----------------------------------------
    //库存数量 减 库存数量大于等于1，小于等于1时也点不了
    private void reduceNum() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mEtNum.getText().toString())) {
            mEtNum.setText("1");
        } else if (new BigDecimal(mEtNum.getText().toString()).doubleValue() > 1) {
            mEtNum.setText(new BigDecimal(mEtNum.getText().toString()).subtract(new BigDecimal("1")).toPlainString());
        } else {
            showShortToast("库存数量不能为0");
            hideWaitDialog();
            return;
        }

        hideWaitDialog();
    }

    //设置库存数量
    private void setNum() {
        if (updateNumPop == null) {
            updateNumPop = new UpdateNumPop(SpecialAreaAddGoodsActivity.this);
        }
        updateNumPop.setOnListener(new UpdateNumPop.IOptionListener() {
            @Override
            public void onAgree(String data) {
                int num = new BigDecimal(data).intValue();
                if (num < 1) {
                    showShortToast("请输入正确的数量");
                    return;
                }
                mEtNum.setText(data);
            }

            @Override
            public void onCancle() {
            }
        });
        updateNumPop.setInputType(InputType.TYPE_CLASS_NUMBER);
        updateNumPop.setContent(EmptyUtils.strEmptyToText(mEtNum.getText().toString(), "0"));
        updateNumPop.setOutSideDismiss(false).showPopupWindow();
        KeyboardUtils.showSoftInput(SpecialAreaAddGoodsActivity.this);
    }

    //库存数量 加
    private void addNum() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mEtNum.getText().toString())) {
            mEtNum.setText("1");
        } else {
            mEtNum.setText(new BigDecimal(mEtNum.getText().toString()).add(new BigDecimal("1")).toPlainString());
        }

        hideWaitDialog();
    }

    //------------------------有效期----------------------------------------
    //有效期 减 库存数量大于等于1，小于等于1时也点不了
    private void reduceValidity() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mEtValidity.getText().toString())) {
            mEtValidity.setText("1");
        } else if (new BigDecimal(mEtValidity.getText().toString()).doubleValue() > 1) {
            mEtValidity.setText(new BigDecimal(mEtValidity.getText().toString()).subtract(new BigDecimal("1")).toPlainString());
        } else {
            showShortToast("有效期不能为0");
            hideWaitDialog();
            return;
        }

        hideWaitDialog();
    }

    //设置有效期
    private void setValidity() {
        if (updateNumPop == null) {
            updateNumPop = new UpdateNumPop(SpecialAreaAddGoodsActivity.this);
        }
        updateNumPop.setOnListener(new UpdateNumPop.IOptionListener() {
            @Override
            public void onAgree(String data) {
                int num = new BigDecimal(data).intValue();
                if (num < 1) {
                    showShortToast("请输入正确的有效期");
                    return;
                }
                mEtValidity.setText(data);
            }

            @Override
            public void onCancle() {
            }
        });
        updateNumPop.setInputType(InputType.TYPE_CLASS_NUMBER);
        updateNumPop.setContent(EmptyUtils.strEmptyToText(mEtValidity.getText().toString(), "0"));
        updateNumPop.setOutSideDismiss(false).showPopupWindow();
        KeyboardUtils.showSoftInput(SpecialAreaAddGoodsActivity.this);
    }

    //有效期 加
    private void addValidity() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mEtValidity.getText().toString())) {
            mEtValidity.setText("1");
        } else {
            mEtValidity.setText(new BigDecimal(mEtValidity.getText().toString()).add(new BigDecimal("1")).toPlainString());
        }

        hideWaitDialog();
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String mTag, String accessoryId, String url, int width, int height) {
        switch (mTag) {
            case TAG_SELECT_BANNER_01://banner 1号位
                selectBanner01Success(accessoryId, url, width, height);
                break;
            case TAG_SELECT_BANNER_02://banner 2号位
                selectBanner02Success(accessoryId, url, width, height);
                break;
            case TAG_SELECT_BANNER_03://banner 3号位
                selectBanner03Success(accessoryId, url, width, height);
                break;
            case TAG_SELECT_DRUG_MANUAL://药品说明书
                selectDrugManualSuccess(accessoryId, url, width, height);
                break;
            case TAG_SELECT_COMMODITY_DETAILS_01://商品详情1
                selectCommodityDetails01Success(accessoryId, url, width, height);
                break;
            case TAG_SELECT_COMMODITY_DETAILS_02://商品详情2
                selectCommodityDetails02Success(accessoryId, url, width, height);
                break;
            case TAG_SELECT_COMMODITY_DETAILS_03://商品详情3
                selectCommodityDetails03Success(accessoryId, url, width, height);
                break;
            case TAG_SELECT_COMMODITY_DETAILS_04://商品详情4
                selectCommodityDetails04Success(accessoryId, url, width, height);
                break;
            case TAG_SELECT_COMMODITY_DETAILS_05://商品详情5
                selectCommodityDetails05Success(accessoryId, url, width, height);
                break;
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }

    //banner 1号位
    protected void selectBanner01Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic01.getLayoutParams();
            if (radio >= 2.025) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_486));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_486))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_240) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_240));
            }

            mIvHorizontalPic01.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic01);

            mLlAddHorizontalPic01.setVisibility(View.GONE);
            mLlAddHorizontalPic01.setEnabled(false);

            mIvHorizontalPic01.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic01.setTag(R.id.tag_2, url);
            mFlHorizontalPic01.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //banner 2号位
    protected void selectBanner02Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic02.getLayoutParams();
            if (radio >= 2.025) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_486));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_486))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_240) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_240));
            }

            mIvHorizontalPic02.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic02);

            mLlAddHorizontalPic02.setVisibility(View.GONE);
            mLlAddHorizontalPic02.setEnabled(false);

            mIvHorizontalPic02.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic02.setTag(R.id.tag_2, url);
            mFlHorizontalPic02.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //banner 3号位
    protected void selectBanner03Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic03.getLayoutParams();
            if (radio >= 2.025) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_486));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_486))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_240) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_240));
            }

            mIvHorizontalPic03.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic03);

            mLlAddHorizontalPic03.setVisibility(View.GONE);
            mLlAddHorizontalPic03.setEnabled(false);

            mIvHorizontalPic03.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic03.setTag(R.id.tag_2, url);
            mFlHorizontalPic03.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //药品说明书
    protected void selectDrugManualSuccess(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvDrugManual.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvDrugManual.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvDrugManual);

            mLlDrugManual.setVisibility(View.GONE);
            mLlDrugManual.setEnabled(false);

            mIvDrugManual.setTag(R.id.tag_1, accessoryId);
            mIvDrugManual.setTag(R.id.tag_2, url);
            mFlDrugManual.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //商品详情01
    protected void selectCommodityDetails01Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails01.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvCommodityDetails01.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCommodityDetails01);

            mLlCommodityDetails01.setVisibility(View.GONE);
            mLlCommodityDetails01.setEnabled(false);

            mIvCommodityDetails01.setTag(R.id.tag_1, accessoryId);
            mIvCommodityDetails01.setTag(R.id.tag_2, url);
            mFlCommodityDetails01.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //商品详情02
    protected void selectCommodityDetails02Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails02.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvCommodityDetails02.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCommodityDetails02);

            mLlCommodityDetails02.setVisibility(View.GONE);
            mLlCommodityDetails02.setEnabled(false);

            mIvCommodityDetails02.setTag(R.id.tag_1, accessoryId);
            mIvCommodityDetails02.setTag(R.id.tag_2, url);
            mFlCommodityDetails02.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //商品详情03
    protected void selectCommodityDetails03Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails03.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvCommodityDetails03.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCommodityDetails03);

            mLlCommodityDetails03.setVisibility(View.GONE);
            mLlCommodityDetails03.setEnabled(false);

            mIvCommodityDetails03.setTag(R.id.tag_1, accessoryId);
            mIvCommodityDetails03.setTag(R.id.tag_2, url);
            mFlCommodityDetails03.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //商品详情04
    protected void selectCommodityDetails04Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails04.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvCommodityDetails04.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCommodityDetails04);

            mLlCommodityDetails04.setVisibility(View.GONE);
            mLlCommodityDetails04.setEnabled(false);

            mIvCommodityDetails04.setTag(R.id.tag_1, accessoryId);
            mIvCommodityDetails04.setTag(R.id.tag_2, url);
            mFlCommodityDetails04.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //商品详情05
    protected void selectCommodityDetails05Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails05.getLayoutParams();
            if (radio >= 2.029) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
            }

            mIvCommodityDetails05.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvCommodityDetails05);

            mLlCommodityDetails05.setVisibility(View.GONE);
            mLlCommodityDetails05.setEnabled(false);

            mIvCommodityDetails05.setTag(R.id.tag_1, accessoryId);
            mIvCommodityDetails05.setTag(R.id.tag_2, url);
            mFlCommodityDetails05.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //--------------------------选择图片----------------------------------------------------
    //是否显示促销价
    protected void estimatePriceVisibility() {
        if ("1".equals(mEtPromotion.getTag())) {
            mLlPromotion.setVisibility(View.VISIBLE);
            mTvPromotionDescribe.setVisibility(View.VISIBLE);
            mLlEstimatePrice.setVisibility(View.VISIBLE);
            mLineEstimatePrice.setVisibility(View.VISIBLE);
        } else {
            mLlPromotion.setVisibility(View.GONE);
            mTvPromotionDescribe.setVisibility(View.GONE);
            mLlEstimatePrice.setVisibility(View.GONE);
            mLineEstimatePrice.setVisibility(View.GONE);
        }
    }
    //计算预估到手价
    protected void calculEstimatePrice() {
        double estimatePrice =0;
        if ("1".equals(mEtPromotion.getTag())){
            estimatePrice=new BigDecimal(mEtNumPrice.getText().toString()).multiply(new BigDecimal(mEtNumPromotion.getText().toString())).divide(new BigDecimal("10"), 2, BigDecimal.ROUND_HALF_UP).doubleValue();
        }else {
            estimatePrice=new BigDecimal(mEtNumPrice.getText().toString()).doubleValue();
        }
        mEtEstimatePrice.setText(String.valueOf(estimatePrice));
    }

    //上架
    protected void onPull() {
        if (null == mIvHorizontalPic01.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvHorizontalPic01.getTag(R.id.tag_2)))) {
            showShortToast("请上传banner 1号位");
            return;
        }

        if (EmptyUtils.isEmpty(mTvBannerTime.getText().toString())) {
            showShortToast("请选择轮播间隔时间");
            return;
        }

        if (itemData == null) {
            showShortToast("请选择药品");
            return;
        }

        if (EmptyUtils.isEmpty(mEtDrugName.getText().toString())) {
            showShortToast("请输入药品名称");
            return;
        }
        if (EmptyUtils.isEmpty(mEtDrugId.getText().toString())) {
            showShortToast("请输入医保药品编码");
            return;
        }
        if (EmptyUtils.isEmpty(mEtDrugProductName.getText().toString())) {
            showShortToast("请输入商品名称");
            return;
        }
        if (EmptyUtils.isEmpty(mEtSpecs.getText().toString())) {
            showShortToast("请输入药品规格");
            return;
        }

        if ("1".equals(itemData.getIsPlatformDrug())) {//平台药品库-西药中成药
            if (EmptyUtils.isEmpty(mEtSort.getText().toString())) {
                showShortToast("请输入药品分类");
                return;
            }
            if (EmptyUtils.isEmpty(mTvType.getText().toString())) {
                showShortToast("请选择药品类型");
                return;
            }

            if ("1".equals(MechanismInfoUtils.getFixmedinsType()) && relationDisetypeAdapter.getData().size() <= 0) {//医院
                showShortToast("请关联病种与诊断信息");
                return;
            }

            if (relationDiseAdapter.getData().size() <= 0) {
                showShortToast("请关联疾病");
                return;
            }

            if (EmptyUtils.isEmpty(mEtFunctions.getText().toString())) {
                showShortToast("请输入功能主治");
                return;
            }
            if (EmptyUtils.isEmpty(mEtCommonUsage.getText().toString())) {
                showShortToast("请输入药品用法");
                return;
            }
            if (EmptyUtils.isEmpty(mEtStorageConditions.getText().toString())) {
                showShortToast("请输入贮存条件");
                return;
            }
        } else {
            switch (itemData.getDrugsClassification()) {
                case "1"://店铺药品库-西药中成药
                    if (EmptyUtils.isEmpty(mEtSort.getText().toString())) {
                        showShortToast("请输入药品分类");
                        return;
                    }
                    if (EmptyUtils.isEmpty(mTvType.getText().toString())) {
                        showShortToast("请选择药品类型");
                        return;
                    }
                    if (relationDiseAdapter.getData().size() <= 0) {
                        showShortToast("请关联疾病");
                        return;
                    }
                    if (EmptyUtils.isEmpty(mEtFunctions.getText().toString())) {
                        showShortToast("请输入功能主治");
                        return;
                    }
                    if (EmptyUtils.isEmpty(mEtCommonUsage.getText().toString())) {
                        showShortToast("请输入药品用法");
                        return;
                    }
                    if (EmptyUtils.isEmpty(mEtStorageConditions.getText().toString())) {
                        showShortToast("请输入贮存条件");
                        return;
                    }
                    break;
                case "2"://店铺药品库-医疗器械
                    if (EmptyUtils.isEmpty(mEtBrand.getText().toString())) {
                        showShortToast("请输入品牌");
                        return;
                    }

                    if (EmptyUtils.isEmpty(mEtPacking.getText().toString())) {
                        showShortToast("请输入包装");
                        return;
                    }

                    if (EmptyUtils.isEmpty(mEtProdentpName.getText().toString())) {
                        showShortToast("请输入生产企业");
                        return;
                    }
                    if (EmptyUtils.isEmpty(mEtApplyRange.getText().toString())) {
                        showShortToast("请输入适用范围");
                        return;
                    }
                    if (EmptyUtils.isEmpty(mEtCommonUsage.getText().toString())) {
                        showShortToast("请输入用法用量");
                        return;
                    }
                    break;
                case "3"://店铺药品库-成人用品
                    if (EmptyUtils.isEmpty(mEtBrand.getText().toString())) {
                        showShortToast("请输入品牌");
                        return;
                    }

                    if (EmptyUtils.isEmpty(mEtPacking.getText().toString())) {
                        showShortToast("请输入包装");
                        return;
                    }

                    if (EmptyUtils.isEmpty(mEtProdentpName.getText().toString())) {
                        showShortToast("请输入生产企业");
                        return;
                    }
                    if (EmptyUtils.isEmpty(mEtCommonUsage.getText().toString())) {
                        showShortToast("请输入使用方法");
                        return;
                    }

                    if (EmptyUtils.isEmpty(mEtStorageConditions.getText().toString())) {
                        showShortToast("请输入贮存条件");
                        return;
                    }
                    break;
                case "4"://店铺药品库-个人用品
                    if (EmptyUtils.isEmpty(mEtBrand.getText().toString())) {
                        showShortToast("请输入品牌");
                        return;
                    }

                    if (EmptyUtils.isEmpty(mEtPacking.getText().toString())) {
                        showShortToast("请输入包装");
                        return;
                    }

                    if (EmptyUtils.isEmpty(mEtProdentpName.getText().toString())) {
                        showShortToast("请输入生产企业");
                        return;
                    }
                    if (EmptyUtils.isEmpty(mEtCommonUsage.getText().toString())) {
                        showShortToast("请输入用法用量");
                        return;
                    }

                    if (EmptyUtils.isEmpty(mEtStorageConditions.getText().toString())) {
                        showShortToast("请输入贮存条件");
                        return;
                    }
                    break;
            }
        }

        if (EmptyUtils.isEmpty(mEtNumPrice.getText().toString()) || "0".equals(mEtNumPrice.getText().toString())) {
            showShortToast("请输入正确的药品价格");
            return;
        }
        if (EmptyUtils.isEmpty(mEtPromotion.getText().toString())) {
            showShortToast("请选择是否促销");
            return;
        }
        if (EmptyUtils.isEmpty(mEtNumPromotion.getText().toString()) || "0".equals(mEtNumPromotion.getText().toString())) {
            showShortToast("请输入正确的促销折扣");
            return;
        }
        if (EmptyUtils.isEmpty(mEtNum.getText().toString()) || "0".equals(mEtNum.getText().toString())) {
            showShortToast("请输入正确的库存数量");
            return;
        }
        if (EmptyUtils.isEmpty(mEtValidity.getText().toString()) || "0".equals(mEtValidity.getText().toString())) {
            showShortToast("请输入正确的有效期");
            return;
        }

        if (null == mIvDrugManual.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvDrugManual.getTag(R.id.tag_2)))) {
            showShortToast("请上传药品说明书");
            return;
        }

        int selectCommodityDetailsPic = 0;
        if (null != mIvCommodityDetails01.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails01.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails02.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails02.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails03.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails03.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails04.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails04.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails05.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails05.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (selectCommodityDetailsPic < 3) {
            showShortToast("请上传最少三张，至多五张商品详情图片");
            return;
        }
        requestData();
    }

    //请求接口
    protected void requestData() {
        if (itemData == null) {
            showShortToast("页面数据有误，请重新进入页面");
            return;
        }
        //2022-10-08 调用接口 成功之后跳转成功页面
        String enterpriseName = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? itemData.getEnterpriseName() : mEtProdentpName.getText().toString();
        String storageConditions = ("2".equals(itemData.getDrugsClassification())) ? itemData.getStorageConditions() : mEtStorageConditions.getText().toString();
        Object ItemType = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? mTvType.getTag() : itemData.getItemType();
        String efccAtd = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? mEtFunctions.getText().toString() : itemData.getEfccAtd();
        String usualWay = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? mEtCommonUsage.getText().toString() : itemData.getUsualWay();
        String associatedDiagnostics = "";
        if ("1".equals(MechanismInfoUtils.getFixmedinsType()) && "1".equals(itemData.getIsPlatformDrug())) {//医院并且是平台药 才需要获取关联诊断
            associatedDiagnostics = JSON.toJSONString(relationDisetypeAdapter.getData());
//            Log.e("666666", "associatedDiagnostics:===" + associatedDiagnostics);
        }
        String associatedDiseases = "";
        if ("1".equals(itemData.getDrugsClassification())) {//只要是西药中成药 才需要获取关联疾病
            //关联疾病数据
            List<String> relationDiseDatas = relationDiseAdapter.getData();
            for (int i = 0, size = relationDiseDatas.size(); i < size; i++) {
                if (i == size - 1) {
                    associatedDiseases += relationDiseDatas.get(i);
                } else {
                    associatedDiseases += relationDiseDatas.get(i) + "∞#";
                }
            }
        }
        String brand = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? itemData.getBrand() : mEtBrand.getText().toString();
        String packaging = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? itemData.getPackaging() : mEtPacking.getText().toString();
        String applicableScope = ("2".equals(itemData.getDrugsClassification())) ? mEtApplyRange.getText().toString() : itemData.getApplicableScope();
        String usageDosage = ("2".equals(itemData.getDrugsClassification()) || "4".equals(itemData.getDrugsClassification())) ? mEtCommonUsage.getText().toString() : itemData.getUsageDosage();
        String usagemethod = ("3".equals(itemData.getDrugsClassification())) ? mEtCommonUsage.getText().toString() : itemData.getUsagemethod();

        showWaitDialog();
        BaseModel.sendAddGoodsRequest(TAG, GoodsLocation, catalogueId, CatalogueName, mIvHorizontalPic01.getTag(R.id.tag_1), mIvHorizontalPic02.getTag(R.id.tag_1),
                mIvHorizontalPic03.getTag(R.id.tag_1), mTvBannerTime.getTag(), mEtPromotion.getTag(), mEtNumPromotion.getText().toString(), mEtEstimatePrice.getText().toString(),
                mEtStoreSimilarity.getText().toString(), itemData.getIsPlatformDrug(), itemData.getDrugsClassification(), itemData.getDrugsClassificationName(),
                itemData.getMIC_Code(), mEtDrugName.getText().toString(), mEtDrugId.getText().toString(), mEtDrugProductName.getText().toString(), mEtSpecs.getText().toString(),
                enterpriseName, storageConditions, mEtValidity.getText().toString(), mEtNum.getText().toString(), mEtNumPrice.getText().toString(), mIvDrugManual.getTag(R.id.tag_1),
                mIvCommodityDetails01.getTag(R.id.tag_1), mIvCommodityDetails02.getTag(R.id.tag_1), mIvCommodityDetails03.getTag(R.id.tag_1), mIvCommodityDetails04.getTag(R.id.tag_1),
                mIvCommodityDetails05.getTag(R.id.tag_1), "1", ItemType, itemData.getApprovalNumber(), itemData.getRegDosform(), efccAtd, usualWay, associatedDiagnostics,
                associatedDiseases, brand, itemData.getRegistrationCertificateNo(), itemData.getProductionLicenseNo(),
                packaging, applicableScope, usageDosage, itemData.getPrecautions(), usagemethod,
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("添加商品成功");

                        MsgBus.sendAddGoodsRefresh().post("1");
                        AddGoodsSuccessActivity.newIntance(SpecialAreaAddGoodsActivity.this, "3", GoodsLocation, catalogueId, CatalogueName);
                        goFinish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }

        if (customerBottomListPop != null) {
            customerBottomListPop.dismiss();
            customerBottomListPop.onDestroy();
        }

        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }

        if (updateNumPop != null) {
            updateNumPop.onCleanListener();
            updateNumPop.onDestroy();
        }

        if (addRelationDisePop != null) {
            addRelationDisePop.onCleanListener();
            addRelationDisePop.onDestroy();
        }
    }

    public static void newIntance(Context context, String GoodsLocation, String catalogueId, String CatalogueName) {
        Intent intent = new Intent(context, SpecialAreaAddGoodsActivity.class);
        intent.putExtra("GoodsLocation", GoodsLocation);
        intent.putExtra("CatalogueId", catalogueId);
        intent.putExtra("CatalogueName", CatalogueName);
        context.startActivity(intent);
    }
}
