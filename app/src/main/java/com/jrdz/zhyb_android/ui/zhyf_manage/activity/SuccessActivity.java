package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-08
 * 描    述： 充值成功
 * ================================================
 */
public class SuccessActivity extends BaseActivity {
    private TextView mTvResult;
    private ShapeTextView mTvBackMyAccount;

    private String result,btnText,from;

    @Override
    public int getLayoutId() {
        return R.layout.activity_recharge_success;
    }

    @Override
    public void initView() {
        super.initView();
        mTvResult = findViewById(R.id.tv_result);
        mTvBackMyAccount = findViewById(R.id.tv_back_my_account);
    }

    @Override
    public void initData() {
        result=getIntent().getStringExtra("result");
        btnText=getIntent().getStringExtra("btnText");
        from=getIntent().getStringExtra("from");
        super.initData();

        mTvResult.setText(EmptyUtils.strEmpty(result));
        mTvBackMyAccount.setText(EmptyUtils.strEmpty(btnText));
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvBackMyAccount.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_back_my_account://返回我的账户
                if (!EmptyUtils.isEmpty(from)&&"2".equals(from)){
                    SmartPhaMainActivity.newIntance(SuccessActivity.this,0);
                }
                goFinish();
                break;
        }
    }

    public static void newIntance(Context context,String result,String btnText) {
        Intent intent = new Intent(context, SuccessActivity.class);
        intent.putExtra("result", result);
        intent.putExtra("btnText", btnText);
        context.startActivity(intent);
    }

    public static void newIntance(Context context,String result,String btnText,String from) {
        Intent intent = new Intent(context, SuccessActivity.class);
        intent.putExtra("result", result);
        intent.putExtra("btnText", btnText);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }
}
