package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.frame.compiler.utils.AssetUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.utils.wheel.CityWheelUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.CityListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-11
 * 描    述：编辑地址
 * ================================================
 */
public class UpdateAddressActivtiy_manage extends BaseActivity {
    private TextView mTvRegion;
    private EditText mEtDetailAddress;
    private ShapeTextView mTvUpdate;

    private List<CityListModel> data;
    private CityWheelUtils cityWheelUtils;
    private String province;//省
    private String city;//市
    private String area;//区

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_address_manage;
    }

    @Override
    public void initView() {
        super.initView();
        mTvRegion = findViewById(R.id.tv_region);
        mEtDetailAddress = findViewById(R.id.et_detail_address);
        mTvUpdate = findViewById(R.id.tv_update);
    }

    @Override
    public void initData() {
        super.initData();

        mTvRegion.setText("北京市昌平区");
        mEtDetailAddress.setText("北京市昌平区水秀花园2号楼一单元502");

        showWaitDialog();
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                String cityData = AssetUtils.loadStringAsset(UpdateAddressActivtiy_manage.this, "cityData.json");
                data = JSON.parseArray(cityData, CityListModel.class);
                hideWaitDialog();
            }
        });
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvRegion.setOnClickListener(this);
        mTvUpdate.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_region://选择省市区
                KeyboardUtils.hideSoftInput(UpdateAddressActivtiy_manage.this);
                selectRegion();
                break;
            case R.id.tv_update://保存
                onUpdate();
                break;
        }
    }

    //选择省市区
    private void selectRegion() {
        if (cityWheelUtils==null){
            cityWheelUtils = new CityWheelUtils();
        }
        cityWheelUtils.showCityWheel(UpdateAddressActivtiy_manage.this, new CityWheelUtils.CityWheelClickListener<CityListModel, CityListModel.CityBean, CityListModel.CityBean.AreaBean>() {
            @Override
            public void onCity(CityListModel provinceData) {
                getCityData(provinceData.getCode());
            }

            @Override
            public void onArea(CityListModel.CityBean cityData) {
                getAreaData(cityData.getCode());
            }

            @Override
            public void onchooseCity(int provincePos, CityListModel provinceItemData,
                                     int cityPos, CityListModel.CityBean cityItemData,
                                     int areaPos, CityListModel.CityBean.AreaBean areaItemData) {
                province=provinceItemData.getName();//省
                city=cityItemData.getName();//市
                area=areaItemData.getName();//区
                mTvRegion.setText(provinceItemData.getName()+cityItemData.getName()+ areaItemData.getName());
            }
        });
        cityWheelUtils.setProvinceData(data);
        getCityData(data.get(0).getCode());
    }

    //获取市级数据
    private void getCityData(String provinceId) {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                for (CityListModel datum : data) {
                    if (provinceId.equals(datum.getCode())) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                cityWheelUtils.setCityData(datum.getCity() == null ? new ArrayList() : datum.getCity());
                            }
                        });

                        if (datum.getCity() != null && datum.getCity().get(0) != null) {
                            getAreaData(datum.getCity().get(0).getCode());
                        }
                        break;
                    }
                }
            }
        });
    }

    //获取区级数据
    private void getAreaData(String cityId) {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                for (CityListModel datum : data) {
                    if (datum != null && datum.getCity() != null) {
                        for (CityListModel.CityBean cityBean : datum.getCity()) {
                            if (cityId.equals(cityBean.getCode())) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cityWheelUtils.setAreaData(cityBean.getArea());
                                    }
                                });
                                break;
                            }
                        }
                    }
                }
            }
        });
    }

    //保存
    private void onUpdate() {
        if (EmptyUtils.isEmpty(mTvRegion.getText().toString())) {
            showShortToast("请选择所在地区");
            return;
        }
        if (EmptyUtils.isEmpty(mEtDetailAddress.getText().toString())) {
            showShortToast("请输入详细地址");
            return;
        }

        //数据检测无误，则可以待会返回上一个页面
        setResult(RESULT_OK);
        goFinish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (data!=null){
            data.clear();
            data=null;
        }

        if (cityWheelUtils != null) {
            cityWheelUtils.onCleanData();
        }
    }
}
