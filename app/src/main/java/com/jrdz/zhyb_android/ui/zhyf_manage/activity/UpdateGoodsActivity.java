package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;

import com.alibaba.fastjson.JSON;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.model.DiseCataModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.RelationDiseAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.RelationDisetypeAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.DragStoreHouseModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodDetailModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-24
 * 描    述：编辑商品
 * ================================================
 */
public class UpdateGoodsActivity extends AddGoodsDraftsActivity {
    private GoodsModel.DataBean itemDataExtern;
    private String GoodsNo, from;
    private boolean isChoose;
    private GoodDetailModel.DataBean pagerData;

    @Override
    public void initView() {
        super.initView();
        mTvSetlectDrugs.setVisibility(View.GONE);
    }

    @Override
    public void initData() {
        GoodsNo = getIntent().getStringExtra("GoodsNo");
        isChoose = getIntent().getBooleanExtra("isChoose", false);
        from = getIntent().getStringExtra("from");
        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);

        showWaitDialog();
        getPagerData();
    }

    //获取页面数据
    private void getPagerData() {
        GoodDetailModel.sendGoodDetailRequest(TAG, GoodsNo, new CustomerJsonCallBack<GoodDetailModel>() {
            @Override
            public void onRequestError(GoodDetailModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GoodDetailModel returnData) {
                hideWaitDialog();
                if (isFinishing()) return;
                GoodDetailModel.DataBean data = returnData.getData();
                if (data != null) {
                    setPagerData(data);
                }
            }
        });
    }

    //设置详情数据
    private void setPagerData(GoodDetailModel.DataBean data) {
        pagerData = data;
        //设置位置数据
        mTvPostion.setTag(data.getGoodsLocation());
        if ("1".equals(data.getGoodsLocation())) {
            mTvPostion.setText("首页");
        } else if ("2".equals(data.getGoodsLocation())) {
            mTvPostion.setText("分类");
        }

        switch (data.getGoodsLocation()) {
            case "1":
                if (phaSortDatas == null) {
                    showWaitDialog();
                    getHomeSortData();
                }
                break;
            case "2":
                if (sortDatas == null) {
                    showWaitDialog();
                    getSortData();
                }
                break;
        }
        //设置名称数据
        mTvSortName.setTag(data.getCatalogueId());
        mTvSortName.setText(EmptyUtils.strEmpty(data.getCatalogueName()));
        //设置banner图片
        if (!EmptyUtils.isEmpty(data.getBannerAccessoryUrl1())) {
            GlideUtils.getImageWidHeig(this, data.getBannerAccessoryUrl1(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner01Success(data.getBannerAccessoryId1(), data.getBannerAccessoryUrl1(), width, height);
                }
            });
        }
        if (!EmptyUtils.isEmpty(data.getBannerAccessoryUrl2())) {
            GlideUtils.getImageWidHeig(this, data.getBannerAccessoryUrl2(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner02Success(data.getBannerAccessoryId2(), data.getBannerAccessoryUrl2(), width, height);
                }
            });
        }
        if (!EmptyUtils.isEmpty(data.getBannerAccessoryUrl3())) {
            GlideUtils.getImageWidHeig(this, data.getBannerAccessoryUrl3(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner03Success(data.getBannerAccessoryId3(), data.getBannerAccessoryUrl3(), width, height);
                }
            });
        }
        mTvBannerTime.setTag(data.getIntervaltime());
        mTvBannerTime.setText(data.getIntervaltime() + "S");
        //设置药品数据
        itemData = new DragStoreHouseModel.DataBean(data.getInstructionsAccessoryUrl(), data.getDetailAccessoryUrl1(), data.getDetailAccessoryUrl2(),
                data.getDetailAccessoryUrl3(), data.getDetailAccessoryUrl4(), data.getDetailAccessoryUrl5(), "", data.getMIC_Code(),
                data.getItemCode(), data.getItemName(), data.getGoodsName(), data.getItemType(), data.getApprovalNumber(), data.getRegDosform(), data.getSpec(),
                data.getEnterpriseName(), data.getEfccAtd(), data.getUsualWay(), data.getStorageConditions(), data.getExpiryDateCount(),
                data.getInventoryQuantity(), data.getPrice(), data.getInstructionsAccessoryId(), data.getDetailAccessoryId1(), data.getDetailAccessoryId2(),
                data.getDetailAccessoryId3(), data.getDetailAccessoryId4(), data.getDetailAccessoryId5(), data.getRegistrationCertificateNo(),
                data.getProductionLicenseNo(), data.getBrand(), data.getPackaging(), data.getApplicableScope(), data.getUsageDosage(), data.getPrecautions(),
                data.getUsagemethod(), data.getIsPlatformDrug(), data.getDrugsClassification(), data.getDrugsClassificationName(),
                data.getAssociatedDiseases(), data.getAssociatedDiagnostics(),data.getIsPromotion());

        if ("1".equals(data.getIsPlatformDrug())) {//平台药品库-西药中成药
            addPlatformWestDragInfo_update(itemData);
        } else {
            switch (data.getDrugsClassification()) {
                case "1"://店铺药品库-西药中成药
                    addShopWestDragInfo_update(itemData);
                    break;
                case "2"://店铺药品库-医疗器械
                    addShopApparatusDragInfo_update(itemData);
                    break;
                case "3"://店铺药品库-成人用品
                    addShopAdultproduceDragInfo_update(itemData);
                    break;
                case "4"://店铺药品库-个人用品
                    addShopPerproduceDragInfo_update(itemData);
                    break;
            }
        }
    }

//    @Override
//    protected void addPlatformWestDragInfo(DragStoreHouseModel.DataBean itemData) {
//        super.addPlatformWestDragInfo(itemData);
//        if (pagerData == null) {
//            showShortToast("页面数据有误,请重新进入页面");
//            return;
//        }
//        //设置关联诊断
//        List<DiseCataModel.DataBean> dataBeans = JSON.parseArray(pagerData.getAssociatedDiagnostics(), DiseCataModel.DataBean.class);
//        relationDisetypeAdapter.setNewData(dataBeans);
//    }

    //------------------------选择药品之后展示的布局----------------------------------------
    //添加药品  平台药品库-西药中成药
    protected void addPlatformWestDragInfo_update(DragStoreHouseModel.DataBean itemData) {
        mLlDrugsInfoContain.removeAllViews();
        LayoutInflater.from(this).inflate(R.layout.layout_add_goods_platform_west, mLlDrugsInfoContain, true);

        mEtSpecs = findViewById(R.id.et_specs);
        mTvType = findViewById(R.id.tv_type);

        mLlRelationDisetype = findViewById(R.id.ll_relation_disetype);
        mLineRelationDisetype01 = findViewById(R.id.line_relation_disetype01);
        mLineRelationDisetype02 = findViewById(R.id.line_relation_disetype02);
        mFlAddRelationDisetype = findViewById(R.id.fl_add_relation_disetype);
        mCrvRelationDisetype = findViewById(R.id.crv_relation_disetype);

        mFlAddRelationDise = findViewById(R.id.fl_add_relation_dise);
        mCrvRelationDise = findViewById(R.id.crv_relation_dise);
        mEtFunctions = findViewById(R.id.et_functions);
        mEtCommonUsage = findViewById(R.id.et_common_usage);
        mEtStorageConditions = findViewById(R.id.et_storage_conditions);
        addDragPublicLayout_update(itemData);

        //设置数据
        mEtSpecs.setText(EmptyUtils.strEmpty(itemData.getSpec()));

        if (EmptyUtils.isEmpty(itemData.getItemType()) || "0".equals(itemData.getItemType())) {
            mTvType.setText("");
        } else {
            mTvType.setTag(itemData.getItemType());
            mTvType.setText("1".equals(itemData.getItemType()) ? "非处方药（OTC）" : "处方药");
        }

        mEtFunctions.setText(EmptyUtils.strEmpty(itemData.getEfccAtd()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsualWay()));
        mEtStorageConditions.setText(EmptyUtils.strEmpty(itemData.getStorageConditions()));

        if ("1".equals(MechanismInfoUtils.getFixmedinsType())) {//医院
            mLlRelationDisetype.setVisibility(View.VISIBLE);
            mLineRelationDisetype01.setVisibility(View.VISIBLE);
            mLineRelationDisetype02.setVisibility(View.VISIBLE);
            mCrvRelationDisetype.setVisibility(View.VISIBLE);
        } else if ("2".equals(MechanismInfoUtils.getFixmedinsType())) {//药店
            mLlRelationDisetype.setVisibility(View.GONE);
            mLineRelationDisetype01.setVisibility(View.GONE);
            mLineRelationDisetype02.setVisibility(View.GONE);
            mCrvRelationDisetype.setVisibility(View.GONE);
        }

        //关联病种与诊断信息
        mCrvRelationDisetype.setHasFixedSize(true);
        mCrvRelationDisetype.setLayoutManager(getLayoutManager());
        relationDisetypeAdapter = new RelationDisetypeAdapter();
        mCrvRelationDisetype.setAdapter(relationDisetypeAdapter);
        //关联病种与诊断信息的数据
        List<DiseCataModel.DataBean> dataBeans = JSON.parseArray(itemData.getAssociatedDiagnostics(), DiseCataModel.DataBean.class);
        relationDisetypeAdapter.setNewData(dataBeans);
        //关联疾病
        mCrvRelationDise.setHasFixedSize(true);
        mCrvRelationDise.setLayoutManager(getLayoutManager());
        relationDiseAdapter = new RelationDiseAdapter();
        mCrvRelationDise.setAdapter(relationDiseAdapter);
        //关联疾病的数据
        if (!EmptyUtils.isEmpty(itemData.getAssociatedDiseases())) {
            String[] associatedDiseases = itemData.getAssociatedDiseases().split("∞#");
            relationDiseAdapter.setNewData(new ArrayList<>(Arrays.asList(associatedDiseases)));
        }

        //关联病种与诊断信息 item点击
        mFlAddRelationDisetype.setOnClickListener(this);
        relationDisetypeAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
//                if (relationDisetypeAdapter.getData().size() <= 1) {
//                    showShortToast("至少保留一个关联病种与诊断信息");
//                    return;
//                }
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(UpdateGoodsActivity.this, "提示", "确定要删除么？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        baseQuickAdapter.getData().remove(i);
                        baseQuickAdapter.notifyDataSetChanged();
                    }
                });
            }
        });

        mTvType.setOnClickListener(this);
        mFlAddRelationDise.setOnClickListener(this);
        //关联疾病 item点击
        relationDiseAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
//                if (relationDiseAdapter.getData().size() <= 1) {
//                    showShortToast("至少保留一个关联疾病");
//                    return;
//                }
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(UpdateGoodsActivity.this, "提示", "确定要删除么？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        baseQuickAdapter.getData().remove(i);
                        baseQuickAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    //添加药品 店铺药品库-西药中成药
    protected void addShopWestDragInfo_update(DragStoreHouseModel.DataBean itemData) {
        mLlDrugsInfoContain.removeAllViews();
        LayoutInflater.from(this).inflate(R.layout.layout_add_goods_shop_west, mLlDrugsInfoContain, true);

        mEtSpecs = findViewById(R.id.et_specs);
        mTvType = findViewById(R.id.tv_type);

        mFlAddRelationDise = findViewById(R.id.fl_add_relation_dise);
        mCrvRelationDise = findViewById(R.id.crv_relation_dise);
        mEtFunctions = findViewById(R.id.et_functions);
        mEtCommonUsage = findViewById(R.id.et_common_usage);

        mEtStorageConditions = findViewById(R.id.et_storage_conditions);
        addDragPublicLayout_update(itemData);

        //设置数据
        mEtSpecs.setText(EmptyUtils.strEmpty(itemData.getSpec()));
        if (EmptyUtils.isEmpty(itemData.getItemType()) || "0".equals(itemData.getItemType())) {
            mTvType.setText("");
        } else {
            mTvType.setTag(itemData.getItemType());
            mTvType.setText("1".equals(itemData.getItemType()) ? "非处方药（OTC）" : "处方药");
        }
        mEtFunctions.setText(EmptyUtils.strEmpty(itemData.getEfccAtd()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsualWay()));
        mEtStorageConditions.setText(EmptyUtils.strEmpty(itemData.getStorageConditions()));

        //关联疾病
        mCrvRelationDise.setHasFixedSize(true);
        mCrvRelationDise.setLayoutManager(getLayoutManager());
        relationDiseAdapter = new RelationDiseAdapter();
        mCrvRelationDise.setAdapter(relationDiseAdapter);
        //关联疾病的数据
        if (!EmptyUtils.isEmpty(itemData.getAssociatedDiseases())) {
            String[] associatedDiseases = itemData.getAssociatedDiseases().split("∞#");
            relationDiseAdapter.setNewData(new ArrayList<>(Arrays.asList(associatedDiseases)));
        }

        mTvType.setOnClickListener(this);
        mFlAddRelationDise.setOnClickListener(this);
        //关联疾病 item点击
        relationDiseAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
//                if (relationDiseAdapter.getData().size() <= 1) {
//                    showShortToast("至少保留一个关联疾病");
//                    return;
//                }
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(UpdateGoodsActivity.this, "提示", "确定要删除么？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        baseQuickAdapter.getData().remove(i);
                        baseQuickAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    //添加药品 店铺药品库-医疗器械
    protected void addShopApparatusDragInfo_update(DragStoreHouseModel.DataBean itemData) {
        mLlDrugsInfoContain.removeAllViews();
        LayoutInflater.from(this).inflate(R.layout.layout_add_goods_shop_apparatus, mLlDrugsInfoContain, true);

        mEtBrand = findViewById(R.id.et_brand);
        mEtSpecs = findViewById(R.id.et_specs);
        mEtPacking = findViewById(R.id.et_packing);
        mEtProdentpName = findViewById(R.id.et_prodentp_name);
        mEtApplyRange = findViewById(R.id.et_apply_range);
        mEtCommonUsage = findViewById(R.id.et_common_usage);

        addDragPublicLayout_update(itemData);

        //设置数据
        mEtBrand.setText(EmptyUtils.strEmpty(itemData.getBrand()));
        mEtSpecs.setText(EmptyUtils.strEmpty(itemData.getSpec()));
        mEtPacking.setText(EmptyUtils.strEmpty(itemData.getPackaging()));
        mEtProdentpName.setText(EmptyUtils.strEmpty(itemData.getEnterpriseName()));
        mEtApplyRange.setText(EmptyUtils.strEmpty(itemData.getApplicableScope()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsageDosage()));
    }

    //添加药品 店铺药品库-成人用品
    protected void addShopAdultproduceDragInfo_update(DragStoreHouseModel.DataBean itemData) {
        mLlDrugsInfoContain.removeAllViews();
        LayoutInflater.from(this).inflate(R.layout.layout_add_goods_shop_adultproduct, mLlDrugsInfoContain, true);

        mEtBrand = findViewById(R.id.et_brand);
        mEtSpecs = findViewById(R.id.et_specs);
        mEtPacking = findViewById(R.id.et_packing);
        mEtProdentpName = findViewById(R.id.et_prodentp_name);
        mEtCommonUsage = findViewById(R.id.et_common_usage);
        mEtStorageConditions = findViewById(R.id.et_storage_conditions);

        addDragPublicLayout_update(itemData);

        //设置数据
        mEtBrand.setText(EmptyUtils.strEmpty(itemData.getBrand()));
        mEtSpecs.setText(EmptyUtils.strEmpty(itemData.getSpec()));
        mEtPacking.setText(EmptyUtils.strEmpty(itemData.getPackaging()));
        mEtProdentpName.setText(EmptyUtils.strEmpty(itemData.getEnterpriseName()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsagemethod()));
        mEtStorageConditions.setText(EmptyUtils.strEmpty(itemData.getStorageConditions()));
    }

    //添加药品  店铺药品库-个人用品
    protected void addShopPerproduceDragInfo_update(DragStoreHouseModel.DataBean itemData) {
        mLlDrugsInfoContain.removeAllViews();
        LayoutInflater.from(this).inflate(R.layout.layout_add_goods_shop_perproduct, mLlDrugsInfoContain, true);

        mEtBrand = findViewById(R.id.et_brand);
        mEtSpecs = findViewById(R.id.et_specs);
        mEtPacking = findViewById(R.id.et_packing);
        mEtProdentpName = findViewById(R.id.et_prodentp_name);
        mEtCommonUsage = findViewById(R.id.et_common_usage);
        mEtStorageConditions = findViewById(R.id.et_storage_conditions);

        addDragPublicLayout_update(itemData);

        //设置数据
        mEtBrand.setText(EmptyUtils.strEmpty(itemData.getBrand()));
        mEtSpecs.setText(EmptyUtils.strEmpty(itemData.getSpec()));
        mEtPacking.setText(EmptyUtils.strEmpty(itemData.getPackaging()));
        mEtProdentpName.setText(EmptyUtils.strEmpty(itemData.getEnterpriseName()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsageDosage()));
        mEtStorageConditions.setText(EmptyUtils.strEmpty(itemData.getStorageConditions()));
    }

    //添加药品--公共的布局
    protected void addDragPublicLayout_update(DragStoreHouseModel.DataBean itemData) {
        mEtDrugName = findViewById(R.id.et_drug_name);
        mEtDrugId = findViewById(R.id.et_drug_id);
        mEtDrugProductName = findViewById(R.id.et_drug_product_name);
        mEtSort = findViewById(R.id.et_sort);
        //药品价格 价格
        mFlReducePrice = findViewById(R.id.fl_reduce_price);
        mEtNumPrice = findViewById(R.id.et_num_price);
        mFlAddPrice = findViewById(R.id.fl_add_price);
        //是否促销 促销折扣
        mEtPromotion = findViewById(R.id.et_promotion);
        mLlPromotion = findViewById(R.id.ll_promotion);
        mTvPromotionDescribe = findViewById(R.id.tv_promotion_describe);
        mFlReducePromotion = findViewById(R.id.fl_reduce_promotion);
        mEtNumPromotion = findViewById(R.id.et_num_promotion);
        mFlAddPromotion = findViewById(R.id.fl_add_promotion);
        //预估到手价
        mLlEstimatePrice = findViewById(R.id.ll_estimate_price);
        mLineEstimatePrice = findViewById(R.id.line_estimate_price);
        mEtEstimatePrice = findViewById(R.id.et_estimate_price);
        //库存数量
        mFlReduceNum = findViewById(R.id.fl_reduce_num);
        mEtNum = findViewById(R.id.et_num);
        mFlAddNum = findViewById(R.id.fl_add_num);
        //有效期
        mFlReduceValidity = findViewById(R.id.fl_reduce_validity);
        mEtValidity = findViewById(R.id.et_validity);
        mFlAddValidity = findViewById(R.id.fl_add_validity);
        //说明书 店内相似 商品详情
        mLlDrugManual = findViewById(R.id.ll_drug_manual);
        mFlDrugManual = findViewById(R.id.fl_drug_manual);
        mIvDrugManual = findViewById(R.id.iv_drug_manual);
        mIvDrugManualDelete = findViewById(R.id.iv_drug_manual_delete);
        mEtStoreSimilarity = findViewById(R.id.et_store_similarity);
        mLlCommodityDetails01 = findViewById(R.id.ll_commodity_details01);
        mFlCommodityDetails01 = findViewById(R.id.fl_commodity_details01);
        mIvCommodityDetails01 = findViewById(R.id.iv_commodity_details01);
        mIvCommodityDetailsDelete01 = findViewById(R.id.iv_commodity_details_delete01);
        mLlCommodityDetails02 = findViewById(R.id.ll_commodity_details02);
        mFlCommodityDetails02 = findViewById(R.id.fl_commodity_details02);
        mIvCommodityDetails02 = findViewById(R.id.iv_commodity_details02);
        mIvCommodityDetailsDelete02 = findViewById(R.id.iv_commodity_details_delete02);
        mLlCommodityDetails03 = findViewById(R.id.ll_commodity_details03);
        mFlCommodityDetails03 = findViewById(R.id.fl_commodity_details03);
        mIvCommodityDetails03 = findViewById(R.id.iv_commodity_details03);
        mIvCommodityDetailsDelete03 = findViewById(R.id.iv_commodity_details_delete03);
        mLlCommodityDetails04 = findViewById(R.id.ll_commodity_details04);
        mFlCommodityDetails04 = findViewById(R.id.fl_commodity_details04);
        mIvCommodityDetails04 = findViewById(R.id.iv_commodity_details04);
        mIvCommodityDetailsDelete04 = findViewById(R.id.iv_commodity_details_delete04);
        mLlCommodityDetails05 = findViewById(R.id.ll_commodity_details05);
        mFlCommodityDetails05 = findViewById(R.id.fl_commodity_details05);
        mIvCommodityDetails05 = findViewById(R.id.iv_commodity_details05);
        mIvCommodityDetailsDelete05 = findViewById(R.id.iv_commodity_details_delete05);

        //设置数据
        mEtDrugName.setText(EmptyUtils.strEmpty(itemData.getItemName()));
        mEtDrugId.setText(EmptyUtils.strEmpty(itemData.getItemCode()));
        mEtDrugProductName.setText(EmptyUtils.isEmpty(itemData.getGoodsName()) ? EmptyUtils.strEmpty(itemData.getItemName()) : itemData.getGoodsName());
        mEtSort.setText(EmptyUtils.strEmpty(itemData.getDrugsClassificationName()));
        mEtNumPrice.setText(EmptyUtils.strEmpty(itemData.getPrice()));
        //===============================注意=============================================
        mEtPromotion.setTag(EmptyUtils.strEmptyToText(itemData.getIsPromotion(),"1"));
        mEtPromotion.setText("1".equals(EmptyUtils.strEmptyToText(itemData.getIsPromotion(),"1"))?"是":"否");
        estimatePriceVisibility();
        calculEstimatePrice();
        //================================注意============================================
        //库存数量
        mEtNum.setText(EmptyUtils.strEmpty(itemData.getInventoryQuantity()));
        //有效期
        mEtValidity.setText(EmptyUtils.strEmpty(itemData.getExpiryDateCount()));

        //banner数据
        if (!EmptyUtils.isEmpty(itemData.getBannerAccessoryUrl1())) {
            GlideUtils.getImageWidHeig(this, itemData.getBannerAccessoryUrl1(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner01Success(itemData.getBannerAccessoryId1(), itemData.getBannerAccessoryUrl1(), width, height);
                }
            });
        }
        if (!EmptyUtils.isEmpty(itemData.getBannerAccessoryUrl2())) {
            GlideUtils.getImageWidHeig(this, itemData.getBannerAccessoryUrl2(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner02Success(itemData.getBannerAccessoryId2(), itemData.getBannerAccessoryUrl2(), width, height);
                }
            });
        }
        if (!EmptyUtils.isEmpty(itemData.getBannerAccessoryUrl3())) {
            GlideUtils.getImageWidHeig(this, itemData.getBannerAccessoryUrl3(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner03Success(itemData.getBannerAccessoryId3(), itemData.getBannerAccessoryUrl3(), width, height);
                }
            });
        }
        //说明书 商品详情
        if (!EmptyUtils.isEmpty(itemData.getInstructionsAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, itemData.getInstructionsAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectDrugManualSuccess(itemData.getInstructionsAccessoryId(), itemData.getInstructionsAccessoryUrl(), width, height);
                }
            });
        }

        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl1())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl1(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectCommodityDetails01Success(itemData.getDetailAccessoryId1(), itemData.getDetailAccessoryUrl1(), width, height);
                }
            });
        }
        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl2())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl2(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectCommodityDetails02Success(itemData.getDetailAccessoryId2(), itemData.getDetailAccessoryUrl2(), width, height);
                }
            });
        }
        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl3())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl3(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectCommodityDetails03Success(itemData.getDetailAccessoryId3(), itemData.getDetailAccessoryUrl3(), width, height);
                }
            });
        }
        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl4())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl4(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectCommodityDetails04Success(itemData.getDetailAccessoryId4(), itemData.getDetailAccessoryUrl4(), width, height);
                }
            });
        }
        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl5())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl5(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectCommodityDetails05Success(itemData.getDetailAccessoryId5(), itemData.getDetailAccessoryUrl5(), width, height);
                }
            });
        }

        mEtPromotion.setOnClickListener(this);
        //药品价格 减 设置 加 点击事件
        mFlReducePrice.setOnClickListener(this);
        mEtNumPrice.setOnClickListener(this);
        mFlAddPrice.setOnClickListener(this);
        //促销折扣 减 设置 加 点击事件
        mFlReducePromotion.setOnClickListener(this);
        mEtNumPromotion.setOnClickListener(this);
        mFlAddPromotion.setOnClickListener(this);

        //库存数量 减 设置 加 点击事件
        mFlReduceNum.setOnClickListener(this);
        mEtNum.setOnClickListener(this);
        mFlAddNum.setOnClickListener(this);

        //有效期 减 设置 加 点击事件
        mFlReduceValidity.setOnClickListener(this);
        mEtValidity.setOnClickListener(this);
        mFlAddValidity.setOnClickListener(this);

        //选择图片上传
        mLlDrugManual.setOnClickListener(this);
        mIvDrugManualDelete.setOnClickListener(this);
        mLlCommodityDetails01.setOnClickListener(this);
        mIvCommodityDetailsDelete01.setOnClickListener(this);
        mLlCommodityDetails02.setOnClickListener(this);
        mIvCommodityDetailsDelete02.setOnClickListener(this);
        mLlCommodityDetails03.setOnClickListener(this);
        mIvCommodityDetailsDelete03.setOnClickListener(this);
        mLlCommodityDetails04.setOnClickListener(this);
        mIvCommodityDetailsDelete04.setOnClickListener(this);
        mLlCommodityDetails05.setOnClickListener(this);
        mIvCommodityDetailsDelete05.setOnClickListener(this);
        mIvDrugManual.setOnClickListener(this);
        mIvCommodityDetails01.setOnClickListener(this);
        mIvCommodityDetails02.setOnClickListener(this);
        mIvCommodityDetails03.setOnClickListener(this);
        mIvCommodityDetails04.setOnClickListener(this);
        mIvCommodityDetails05.setOnClickListener(this);

        if (pagerData == null) {
            showShortToast("页面数据有误,请重新进入页面");
            return;
        }
        mEtPromotion.setTag(pagerData.getIsPromotion());
        mEtPromotion.setText("1".equals(pagerData.getIsPromotion()) ? "是" : "否");
    }

    @Override
    public void customClick(View v) {
        int viewId = v.getId();

        if (viewId != R.id.iv_horizontal_pic01 && viewId != R.id.iv_horizontal_pic02 && viewId != R.id.iv_horizontal_pic03
                && viewId != R.id.iv_drug_manual && viewId != R.id.iv_commodity_details01 && viewId != R.id.iv_commodity_details02
                && viewId != R.id.iv_commodity_details03 && viewId != R.id.iv_commodity_details04 && viewId != R.id.iv_commodity_details05
                && "1".equals(pagerData.getIsOnSale())) {//已上架的商品 只有查看的权限
            showShortToast("下架之后才能对产品进行编辑");
            return;
        }
        super.customClick(v);
    }

    @Override
    protected void onSave() {
        if (null == mIvHorizontalPic01.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvHorizontalPic01.getTag(R.id.tag_2)))) {
            showShortToast("请上传banner 1号位");
            return;
        }

        //2022-10-08 调用接口 成功之后跳转成功页面
        String enterpriseName = (null == itemData) ? "" : (("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? itemData.getEnterpriseName() : mEtProdentpName.getText().toString());
        String storageConditions = (null == itemData) ? "" : (("2".equals(itemData.getDrugsClassification())) ? itemData.getStorageConditions() : mEtStorageConditions.getText().toString());
        Object ItemType = (null == itemData) ? "" : (("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? mTvType.getTag() : itemData.getItemType());
        String efccAtd = (null == itemData) ? "" : (("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? mEtFunctions.getText().toString() : itemData.getEfccAtd());
        String usualWay = (null == itemData) ? "" : (("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? mEtCommonUsage.getText().toString() : itemData.getUsualWay());
        String associatedDiagnostics = "";
        if (null != itemData && "1".equals(MechanismInfoUtils.getFixmedinsType()) && "1".equals(itemData.getIsPlatformDrug())) {//医院并且是平台药 才需要获取关联诊断
            associatedDiagnostics = JSON.toJSONString(relationDisetypeAdapter.getData());
            Log.e("666666", "associatedDiagnostics:===" + associatedDiagnostics);
        }
        String associatedDiseases = "";
        if (null != itemData && "1".equals(itemData.getDrugsClassification())) {//只要是西药中成药 才需要获取关联疾病
            //关联疾病数据
            List<String> relationDiseDatas = relationDiseAdapter.getData();

            for (int i = 0, size = relationDiseDatas.size(); i < size; i++) {
                if (i == size - 1) {
                    associatedDiseases += relationDiseDatas.get(i);
                } else {
                    associatedDiseases += relationDiseDatas.get(i) + "∞#";
                }
            }
            Log.e("666666", "associatedDiagnostics:===" + associatedDiseases);
        }
        String brand = (null == itemData) ? "" : (("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? itemData.getBrand() : mEtBrand.getText().toString());
        String packaging = (null == itemData) ? "" : (("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? itemData.getPackaging() : mEtPacking.getText().toString());
        String applicableScope = (null == itemData) ? "" : (("2".equals(itemData.getDrugsClassification())) ? mEtApplyRange.getText().toString() : itemData.getApplicableScope());
        String usageDosage = (null == itemData) ? "" : (("2".equals(itemData.getDrugsClassification()) || "4".equals(itemData.getDrugsClassification())) ? mEtCommonUsage.getText().toString() : itemData.getUsageDosage());
        String usagemethod = (null == itemData) ? "" : (("3".equals(itemData.getDrugsClassification())) ? mEtCommonUsage.getText().toString() : itemData.getUsagemethod());

        showWaitDialog();
        BaseModel.sendUpdateGoodsRequest(TAG, GoodsNo, null == mTvPostion.getTag() ? "" : String.valueOf(mTvPostion.getTag()),
                null == mTvSortName.getTag() ? "" : String.valueOf(mTvSortName.getTag()), mTvSortName.getText().toString(),
                mIvHorizontalPic01.getTag(R.id.tag_1), mIvHorizontalPic02.getTag(R.id.tag_1), mIvHorizontalPic03.getTag(R.id.tag_1), mTvBannerTime.getTag(),
                null == itemData ? "" : mEtPromotion.getTag(), null == itemData ? "" : mEtNumPromotion.getText().toString(), null == itemData ? "" : mEtEstimatePrice.getText().toString(),
                null == itemData ? "" : mEtStoreSimilarity.getText().toString(), null == itemData ? "" : itemData.getIsPlatformDrug(), null == itemData ? "" : itemData.getDrugsClassification(),
                null == itemData ? "" : itemData.getDrugsClassificationName(), null == itemData ? "" : itemData.getMIC_Code(), null == itemData ? "" : mEtDrugName.getText().toString(),
                null == itemData ? "" : mEtDrugId.getText().toString(), null == itemData ? "" : mEtDrugProductName.getText().toString(), null == itemData ? "" : mEtSpecs.getText().toString(), enterpriseName, storageConditions,
                null == itemData ? "" : mEtValidity.getText().toString(), null == itemData ? "" : mEtNum.getText().toString(), null == itemData ? "" : mEtNumPrice.getText().toString(), null == itemData ? "" : mIvDrugManual.getTag(R.id.tag_1),
                null == itemData ? "" : mIvCommodityDetails01.getTag(R.id.tag_1), null == itemData ? "" : mIvCommodityDetails02.getTag(R.id.tag_1), null == itemData ? "" : mIvCommodityDetails03.getTag(R.id.tag_1),
                null == itemData ? "" : mIvCommodityDetails04.getTag(R.id.tag_1), null == itemData ? "" : mIvCommodityDetails05.getTag(R.id.tag_1), "1".equals(from) ? "2" : "0", ItemType,
                null == itemData ? "" : itemData.getApprovalNumber(), null == itemData ? "" : itemData.getRegDosform(), efccAtd, usualWay, associatedDiagnostics,
                associatedDiseases, brand, null == itemData ? "" : itemData.getRegistrationCertificateNo(), null == itemData ? "" : itemData.getProductionLicenseNo(),
                packaging, applicableScope, usageDosage, null == itemData ? "" : itemData.getPrecautions(), usagemethod, from,
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("更新商品成功");
                        //2022-10-24 编辑成功之后 需要将当前的model带回列表页面
                        itemDataExtern = new GoodsModel.DataBean(GoodsNo, String.valueOf(ItemType), efccAtd, Integer.valueOf(mEtNum.getText().toString()), mEtNumPrice.getText().toString(),
                                itemData.getIsPlatformDrug(), null == mTvPostion.getTag() ? "" : String.valueOf(mTvPostion.getTag()), null == mTvSortName.getTag() ? "" : String.valueOf(mTvSortName.getTag()),
                                null == mIvHorizontalPic01.getTag(R.id.tag_1) ? "" : String.valueOf(mIvHorizontalPic01.getTag(R.id.tag_1)),
                                itemData.getDrugsClassification(), itemData.getDrugsClassificationName(), null == mEtPromotion.getTag() ? "1" : String.valueOf(mEtPromotion.getTag()),
                                mEtDrugProductName.getText().toString(), mEtNumPromotion.getText().toString(), mEtEstimatePrice.getText().toString(),
                                "1".equals(from) ? "2" : "0", null == mIvHorizontalPic01.getTag(R.id.tag_2) ? "" : String.valueOf(mIvHorizontalPic01.getTag(R.id.tag_2)),
                                pagerData.getMonthlySales(), isChoose,pagerData.getIsShowGoodsSold(),pagerData.getIsShowMargin());
                        Intent intent = new Intent();
                        intent.putExtra("itemData", itemDataExtern);
                        setResult(RESULT_OK, intent);

                        goFinish();
                    }
                });
    }

    @Override
    protected void requestData() {
        if (itemData == null) {
            showShortToast("页面数据有误，请重新进入页面");
            return;
        }
        //2022-10-08 调用接口 成功之后跳转成功页面
        String enterpriseName = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? itemData.getEnterpriseName() : mEtProdentpName.getText().toString();
        String storageConditions = ("2".equals(itemData.getDrugsClassification())) ? itemData.getStorageConditions() : mEtStorageConditions.getText().toString();
        Object ItemType = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? mTvType.getTag() : itemData.getItemType();
        String efccAtd = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? mEtFunctions.getText().toString() : itemData.getEfccAtd();
        String usualWay = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? mEtCommonUsage.getText().toString() : itemData.getUsualWay();
        String associatedDiagnostics = "";
        if ("1".equals(MechanismInfoUtils.getFixmedinsType()) && "1".equals(itemData.getIsPlatformDrug())) {//医院并且是平台药 才需要获取关联诊断
            associatedDiagnostics = JSON.toJSONString(relationDisetypeAdapter.getData());
            Log.e("666666", "associatedDiagnostics:===" + associatedDiagnostics);
        }
        String associatedDiseases = "";
        if ("1".equals(itemData.getDrugsClassification())) {//只要是西药中成药 才需要获取关联疾病
            //关联疾病数据
            List<String> relationDiseDatas = relationDiseAdapter.getData();

            for (int i = 0, size = relationDiseDatas.size(); i < size; i++) {
                if (i == size - 1) {
                    associatedDiseases += relationDiseDatas.get(i);
                } else {
                    associatedDiseases += relationDiseDatas.get(i) + "∞#";
                }
            }
            Log.e("666666", "associatedDiagnostics:===" + associatedDiseases);
        }
        String brand = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? itemData.getBrand() : mEtBrand.getText().toString();
        String packaging = ("1".equals(itemData.getIsPlatformDrug()) || "1".equals(itemData.getDrugsClassification())) ? itemData.getPackaging() : mEtPacking.getText().toString();
        String applicableScope = ("2".equals(itemData.getDrugsClassification())) ? mEtApplyRange.getText().toString() : itemData.getApplicableScope();
        String usageDosage = ("2".equals(itemData.getDrugsClassification()) || "4".equals(itemData.getDrugsClassification())) ? mEtCommonUsage.getText().toString() : itemData.getUsageDosage();
        String usagemethod = ("3".equals(itemData.getDrugsClassification())) ? mEtCommonUsage.getText().toString() : itemData.getUsagemethod();

        showWaitDialog();
        BaseModel.sendUpdateGoodsRequest(TAG, GoodsNo, null == mTvPostion.getTag() ? "" : String.valueOf(mTvPostion.getTag()),
                null == mTvSortName.getTag() ? "" : String.valueOf(mTvSortName.getTag()), mTvSortName.getText().toString(),
                mIvHorizontalPic01.getTag(R.id.tag_1), mIvHorizontalPic02.getTag(R.id.tag_1), mIvHorizontalPic03.getTag(R.id.tag_1),
                mTvBannerTime.getTag(), mEtPromotion.getTag(), mEtNumPromotion.getText().toString(), mEtEstimatePrice.getText().toString(),
                mEtStoreSimilarity.getText().toString(), itemData.getIsPlatformDrug(), itemData.getDrugsClassification(), itemData.getDrugsClassificationName(),
                itemData.getMIC_Code(), mEtDrugName.getText().toString(), mEtDrugId.getText().toString(), mEtDrugProductName.getText().toString(), mEtSpecs.getText().toString(),
                enterpriseName, storageConditions, mEtValidity.getText().toString(), mEtNum.getText().toString(), mEtNumPrice.getText().toString(), mIvDrugManual.getTag(R.id.tag_1),
                mIvCommodityDetails01.getTag(R.id.tag_1), mIvCommodityDetails02.getTag(R.id.tag_1), mIvCommodityDetails03.getTag(R.id.tag_1), mIvCommodityDetails04.getTag(R.id.tag_1),
                mIvCommodityDetails05.getTag(R.id.tag_1), "1", ItemType, itemData.getApprovalNumber(), itemData.getRegDosform(), efccAtd, usualWay, associatedDiagnostics,
                associatedDiseases, brand, itemData.getRegistrationCertificateNo(), itemData.getProductionLicenseNo(),
                packaging, applicableScope, usageDosage, itemData.getPrecautions(), usagemethod, from,
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("更新商品成功");
                        //2022-10-24 编辑成功之后 需要将当前的model带回列表页面
                        itemDataExtern = new GoodsModel.DataBean(GoodsNo, String.valueOf(ItemType), efccAtd, Integer.valueOf(mEtNum.getText().toString()), mEtNumPrice.getText().toString(),
                                itemData.getIsPlatformDrug(), null == mTvPostion.getTag() ? "" : String.valueOf(mTvPostion.getTag()), null == mTvSortName.getTag() ? "" : String.valueOf(mTvSortName.getTag()),
                                null == mIvHorizontalPic01.getTag(R.id.tag_1) ? "" : String.valueOf(mIvHorizontalPic01.getTag(R.id.tag_1)),
                                itemData.getDrugsClassification(), itemData.getDrugsClassificationName(), null == mEtPromotion.getTag() ? "1" : String.valueOf(mEtPromotion.getTag()),
                                mEtDrugProductName.getText().toString(), mEtNumPromotion.getText().toString(), mEtEstimatePrice.getText().toString(),
                                "1", null == mIvHorizontalPic01.getTag(R.id.tag_2) ? "" : String.valueOf(mIvHorizontalPic01.getTag(R.id.tag_2)),
                                pagerData.getMonthlySales(), isChoose,pagerData.getIsShowGoodsSold(),pagerData.getIsShowMargin());
                        Intent intent = new Intent();
                        intent.putExtra("itemData", itemDataExtern);
                        setResult(RESULT_OK, intent);

                        goFinish();
                    }
                });
    }
}
