package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Intent;
import android.location.Address;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;

import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.geocode.GeoCodeOption;
import com.baidu.mapapi.search.geocode.GeoCodeResult;
import com.baidu.mapapi.search.geocode.GeoCoder;
import com.baidu.mapapi.search.geocode.OnGetGeoCoderResultListener;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeResult;
import com.baidu.mapapi.utils.DistanceUtil;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.lbs_library.utils.CustomLocationBean;
import com.frame.lbs_library.utils.IGeoCoderListener;
import com.frame.lbs_library.utils.LBSUtil;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.utils.location.CoordinateUtils;
import com.jrdz.zhyb_android.utils.location.LocationTool;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-10
 * 描    述：修改订单页面
 * ================================================
 */
public class UpdateOrderActivity extends BaseActivity {
    private RadioGroup mSrbGroup;
    private AppCompatRadioButton mSrbSelfLifting;
    private AppCompatRadioButton mSrbMerchantDistr;
    private View mLine01;
    private LinearLayout mLlAddressInfo;
    private ImageView mIvAddressSeize;
    private TextView mTvAddressInfo;
    private ShapeTextView mTvUpdate;

    private String orderNo, shippingMethod, fullName = "", phone = "", location = "", address = "", areaName = "", cityName = "", latitude = "", longitude = "";
    String isSelfLifting = "1", isSelfLiftingText = "自提"; //默认自提 //默认自助购药

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_order;
    }

    @Override
    public void initView() {
        super.initView();
        mSrbGroup = findViewById(R.id.srb_group);
        mSrbSelfLifting = findViewById(R.id.srb_self_lifting);
        mSrbMerchantDistr = findViewById(R.id.srb_merchant_distr);
        mLine01 = findViewById(R.id.line_01);
        mLlAddressInfo = findViewById(R.id.ll_address_info);
        mIvAddressSeize = findViewById(R.id.iv_address_seize);
        mTvAddressInfo = findViewById(R.id.tv_address_info);
        mTvUpdate = findViewById(R.id.tv_update);
    }

    @Override
    public void initData() {
        Intent intent = getIntent();
        orderNo = intent.getStringExtra("OrderNo");
        shippingMethod = intent.getStringExtra("ShippingMethod");
        fullName = intent.getStringExtra("FullName");
        phone = intent.getStringExtra("Phone");
        location = intent.getStringExtra("Location");
        address = intent.getStringExtra("Address");
        areaName = intent.getStringExtra("AreaName");
        cityName = intent.getStringExtra("CityName");
        latitude=intent.getStringExtra("latitude");
        longitude=intent.getStringExtra("longitude");

        super.initData();

        if ("1".equals(shippingMethod)) {
            mSrbSelfLifting.setChecked(true);

            mLine01.setVisibility(View.GONE);
            mLlAddressInfo.setVisibility(View.GONE);

            isSelfLifting = "1";
            isSelfLiftingText = "自提";
        } else {
            mSrbMerchantDistr.setChecked(true);

            mLine01.setVisibility(View.VISIBLE);
            mLlAddressInfo.setVisibility(View.VISIBLE);

            if (EmptyUtils.isEmpty(fullName)) {
                mIvAddressSeize.setVisibility(View.GONE);
                mTvAddressInfo.setText("请选择地址");
            } else {
                mIvAddressSeize.setVisibility(View.VISIBLE);
                mTvAddressInfo.setText(fullName + StringUtils.encryptionPhone(phone) + "\n" + location + address);
            }

            isSelfLifting = "2";
            isSelfLiftingText = "商家配送";
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mSrbGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.srb_self_lifting) {//自提
                    mLine01.setVisibility(View.GONE);
                    mLlAddressInfo.setVisibility(View.GONE);

                    isSelfLifting = "1";
                    isSelfLiftingText = "自提";
                } else {//商家配送
                    mLine01.setVisibility(View.VISIBLE);
                    mLlAddressInfo.setVisibility(View.VISIBLE);

                    if (EmptyUtils.isEmpty(fullName)) {
                        mIvAddressSeize.setVisibility(View.GONE);
                        mTvAddressInfo.setText("请选择地址");
                    } else {
                        mIvAddressSeize.setVisibility(View.VISIBLE);
                        mTvAddressInfo.setText(fullName + StringUtils.encryptionPhone(phone) + "\n" + location + address);
                    }

                    isSelfLifting = "2";
                    isSelfLiftingText = "商家配送";
                }
            }
        });
        mLlAddressInfo.setOnClickListener(this);
        mTvUpdate.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.ll_address_info://编辑用户收货地址
                Intent intent = new Intent(UpdateOrderActivity.this, UpdateReceiptAddressActivity.class);
                intent.putExtra("FullName", fullName);
                intent.putExtra("Phone", phone);
                intent.putExtra("Location", location);
                intent.putExtra("Address", address);
                intent.putExtra("AreaName", areaName);
                intent.putExtra("CityName", cityName);

                startActivityForResult(intent, new BaseActivity.OnActivityCallback() {
                    @Override
                    public void onActivityResult(int resultCode, @Nullable Intent data) {
                        if (resultCode == RESULT_OK) {
                            fullName = data.getStringExtra("FullName");
                            phone = data.getStringExtra("Phone");
                            location = data.getStringExtra("Location");
                            address = data.getStringExtra("Address");
                            areaName = data.getStringExtra("AreaName");
                            cityName = data.getStringExtra("CityName");
                            mTvAddressInfo.setText(fullName + StringUtils.encryptionPhone(phone) + "\n" + location + address);
                        }
                    }
                });
                break;
            case R.id.tv_update://确认修改
                onUpdate();
                break;
        }
    }

    private void onUpdate() {
        if ("2".equals(isSelfLifting)) {
            if (Constants.Configure.IS_POS){
                if (EmptyUtils.isEmpty(cityName)||EmptyUtils.isEmpty(areaName)) {
                    showShortToast("请选择地址");
                    return;
                }
            }else {
                if (EmptyUtils.isEmpty(location)||EmptyUtils.isEmpty(areaName)) {
                    showShortToast("请选择地址");
                    return;
                }
            }
            showWaitDialog();
            ThreadUtils.getSinglePool().execute(new Runnable() {
                @Override
                public void run() {
                    getDataByThridPart();
//                    if (Constants.Configure.IS_POS){
//                        getDataByThridPart();
//                    }else {
//                        getDataByLocal();
//                    }
                }
            });
        }else {
            fullName="";
            address="";
            location="";
            showWaitDialog();
            onUpdateRequest("","","");
        }
    }

    //有第三方api解析地址 获取经纬度
    private void getDataByThridPart() {
        showWaitDialog();
        LBSUtil.getInstance().GeoCoder(cityName, address, new IGeoCoderListener() {
            @Override
            public void onGeoCoderSuccess(double latitudeNew, double longitudeNew) {
                String distance = String.valueOf((int) DistanceUtil.getDistance(new LatLng(Double.valueOf(EmptyUtils.strEmptyToText(latitude, "0")), Double.valueOf(EmptyUtils.strEmptyToText(longitude, "0"))),
                        new LatLng(latitudeNew,longitudeNew)) / 1000);
                onUpdateRequest(distance,areaName,cityName);
            }

            @Override
            public void onGeoCoderError(String errorText) {
                hideWaitDialog();
                showShortToast(errorText);
            }
        });
    }

    //有原生api解析地址 获取经纬度
    private void getDataByLocal() {
        Address localAddress= LocationTool.getInstance().getLocalTion(UpdateOrderActivity.this, location+address);
        if (localAddress==null) {
            hideWaitDialog();
            //没有检索到结果
            showShortToast("该地址不合法，暂未检测到地址经纬度");
        } else {
            CustomLocationBean customLocationBean = CoordinateUtils.GPS84ToBD09(localAddress.getLongitude(), localAddress.getLatitude());
            String distance = String.valueOf((int) DistanceUtil.getDistance(new LatLng(Double.valueOf(EmptyUtils.strEmptyToText(latitude, "0")), Double.valueOf(EmptyUtils.strEmptyToText(longitude, "0"))),
                    new LatLng(customLocationBean.getLatitude(),customLocationBean.getLongitude())) / 1000);
            onUpdateRequest(distance,areaName,cityName);
        }
    }

    private void onUpdateRequest(String distance, String AreaName, String CityName) {
        // 2022-10-10 请求确定修改接口
        BaseModel.sendUpdateOrderRequest(TAG, orderNo, isSelfLifting, isSelfLiftingText, fullName, phone, address, location,
                distance, AreaName, CityName, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast(returnData.getMsg());
                        setResult(RESULT_OK);
                        goFinish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        LBSUtil.getInstance().stopGeoCoder();
    }
}
