package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.frame.compiler.utils.AssetUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.utils.wheel.CityWheelUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.CityListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-10
 * 描    述：编辑收货地址--商户-管理端-修改订单时
 * ================================================
 */
public class UpdateReceiptAddressActivity extends BaseActivity {
    private EditText mEtUsername;
    private EditText mEtPhone;
    private TextView mTvRegion;
    private EditText mEtDetailAddress;
    private ShapeTextView mTvUpdate;

    private List<CityListModel> data;

    private String fullName, phone, location,address;
    private CityWheelUtils cityWheelUtils;
    private String provinceName,cityName;

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_receipt_address;
    }

    @Override
    public void initView() {
        super.initView();
        mEtUsername = findViewById(R.id.et_username);
        mEtPhone = findViewById(R.id.et_phone);
        mTvRegion = findViewById(R.id.tv_region);
        mEtDetailAddress = findViewById(R.id.et_detail_address);
        mTvUpdate = findViewById(R.id.tv_update);
    }

    @Override
    public void initData() {
        Intent intent = getIntent();
        fullName = intent.getStringExtra("FullName");
        phone = intent.getStringExtra("Phone");
        location = intent.getStringExtra("Location");
        address = intent.getStringExtra("Address");
        provinceName = intent.getStringExtra("AreaName");
        cityName = intent.getStringExtra("CityName");
        super.initData();

        mEtUsername.setText(EmptyUtils.strEmpty(fullName));
        mEtPhone.setText(EmptyUtils.strEmpty(phone));
        mTvRegion.setText(EmptyUtils.strEmpty(location));
        mEtDetailAddress.setText(EmptyUtils.strEmpty(address));

        showWaitDialog();
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                String cityData = AssetUtils.loadStringAsset(UpdateReceiptAddressActivity.this, "cityData.json");
                data = JSON.parseArray(cityData, CityListModel.class);
                hideWaitDialog();
            }
        });
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvRegion.setOnClickListener(this);
        mTvUpdate.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_region://选择省市区
                KeyboardUtils.hideSoftInput(UpdateReceiptAddressActivity.this);
                selectRegion();
                break;
            case R.id.tv_update://保存
                onUpdate();
                break;
        }
    }

    //选择省市区
    private void selectRegion() {
        if (cityWheelUtils==null){
            cityWheelUtils = new CityWheelUtils();
        }
        cityWheelUtils.showCityWheel(UpdateReceiptAddressActivity.this, new CityWheelUtils.CityWheelClickListener<CityListModel, CityListModel.CityBean, CityListModel.CityBean.AreaBean>() {
            @Override
            public void onCity(CityListModel provinceData) {
                getCityData(provinceData.getCode());
            }

            @Override
            public void onArea(CityListModel.CityBean cityData) {
                getAreaData(cityData.getCode());
            }

            @Override
            public void onchooseCity(int provincePos, CityListModel provinceItemData,
                                     int cityPos, CityListModel.CityBean cityItemData,
                                     int areaPos, CityListModel.CityBean.AreaBean areaItemData) {
                provinceName=provinceItemData.getName();
                cityName=cityItemData.getName();
                mTvRegion.setText(provinceItemData.getName()+cityItemData.getName()+ areaItemData.getName());
            }
        });
        cityWheelUtils.setProvinceData(data);
        getCityData(data.get(0).getCode());
    }

    //获取市级数据
    private void getCityData(String provinceId) {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                for (CityListModel datum : data) {
                    if (provinceId.equals(datum.getCode())) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                cityWheelUtils.setCityData(datum.getCity() == null ? new ArrayList() : datum.getCity());
                            }
                        });

                        if (datum.getCity() != null && datum.getCity().get(0) != null) {
                            getAreaData(datum.getCity().get(0).getCode());
                        }
                        break;
                    }
                }
            }
        });
    }

    //获取区级数据
    private void getAreaData(String cityId) {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                for (CityListModel datum : data) {
                    if (datum != null && datum.getCity() != null) {
                        for (CityListModel.CityBean cityBean : datum.getCity()) {
                            if (cityId.equals(cityBean.getCode())) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cityWheelUtils.setAreaData(cityBean.getArea());
                                    }
                                });
                                break;
                            }
                        }
                    }
                }
            }
        });
    }

    //保存
    private void onUpdate() {
        if (EmptyUtils.isEmpty(mEtUsername.getText().toString())) {
            showShortToast("请输入收货人姓名");
            return;
        }
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString())) {
            showShortToast("请输入联系电话");
            return;
        }
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString().trim()) || mEtPhone.getText().toString().length() < 11) {
            showShortToast("手机号码格式不正确!");
            return;
        }
        if (EmptyUtils.isEmpty(mTvRegion.getText().toString())) {
            showShortToast("请选择所在地区");
            return;
        }
        if (EmptyUtils.isEmpty(mEtDetailAddress.getText().toString())) {
            showShortToast("请输入详细地址");
            return;
        }

        //数据检测无误，则可以待会返回上一个页面
        Intent intent=new Intent();
        intent.putExtra("FullName", mEtUsername.getText().toString());
        intent.putExtra("Phone", mEtPhone.getText().toString());
        intent.putExtra("Location", mTvRegion.getText().toString());
        intent.putExtra("Address", mEtDetailAddress.getText().toString());
        intent.putExtra("AreaName", provinceName);
        intent.putExtra("CityName", cityName);
        setResult(RESULT_OK,intent);
        goFinish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (data!=null){
            data.clear();
            data=null;
        }

        if (cityWheelUtils != null) {
            cityWheelUtils.onCleanData();
        }
    }

//    public static void newIntance(Activity activity, OrderListModel.ReceiptAddressBean receiptAddressBean, int requestCode) {
//        Intent intent = new Intent(activity, UpdateReceiptAddressActivity.class);
//        intent.putExtra("receiptAddressBean", receiptAddressBean);
//        activity.startActivityForResult(intent,requestCode);
//    }
}
