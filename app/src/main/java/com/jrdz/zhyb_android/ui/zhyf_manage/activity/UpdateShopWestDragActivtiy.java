package com.jrdz.zhyb_android.ui.zhyf_manage.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.RelationDiseAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.DragStoreHouseModel;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-19
 * 描    述：删除、更新--店铺药品库--西药中成药
 * ================================================
 */
public class UpdateShopWestDragActivtiy extends AddShopWestDragActivtiy {
    private TextView mTvDrugName;
    private TextView mTvDrugId;
    private ShapeTextView mTvUpdate;

    private int position;
    private DragStoreHouseModel.DataBean itemData;
    private String associatedDiseases="";

    @Override
    public int getLayoutId() {
        return R.layout.activtiy_update_shop_westdrag;
    }

    @Override
    public void initView() {
        super.initView();
        mTvDrugName = findViewById(R.id.tv_drug_name);
        mTvDrugId = findViewById(R.id.tv_drug_id);
        mTvUpdate = findViewById(R.id.tv_update);
    }

    @Override
    public void initData() {
        position = getIntent().getIntExtra("position", 0);
        itemData = getIntent().getParcelableExtra("itemData");

        setRightTitleView("删除");
        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);

        mTvDrugName.setText("药品名称：" + itemData.getItemName());
        mTvDrugId.setText("药品编码：" + itemData.getItemCode());

        if (EmptyUtils.isEmpty(itemData.getItemType())||"0".equals(itemData.getItemType())){
            mTvType.setText("");
        }else {
            mTvType.setTag(itemData.getItemType());
            mTvType.setText("1".equals(itemData.getItemType()) ? "非处方药（OTC）" : "处方药");
        }

        mEtAprvnoBegndate.setText(EmptyUtils.strEmpty(itemData.getApprovalNumber()));
        mEtRegDosform.setText(EmptyUtils.strEmpty(itemData.getRegDosform()));
        mEtSpec.setText(EmptyUtils.strEmpty(itemData.getSpec()));
        mEtProdentpName.setText(EmptyUtils.strEmpty(itemData.getEnterpriseName()));
        mEtFuncMain.setText(EmptyUtils.strEmpty(itemData.getEfccAtd()));
        mEtCommonUsage.setText(EmptyUtils.strEmpty(itemData.getUsualWay()));
        mEtStorageConditions.setText(EmptyUtils.strEmpty(itemData.getStorageConditions()));

        //关联病种与诊断信息
        mCrvRelationDise.setHasFixedSize(true);
        mCrvRelationDise.setLayoutManager(getLayoutManager());
        relationDiseAdapter = new RelationDiseAdapter();
        mCrvRelationDise.setAdapter(relationDiseAdapter);
        //模拟关联病种的数据
        String[] relationDises = itemData.getAssociatedDiseases().split("∞#");
        relationDiseAdapter.setNewData(new ArrayList<>(Arrays.asList(relationDises)));
        //有效期
        mEtValidity.setText(EmptyUtils.strEmpty(itemData.getExpiryDateCount()));
        mEtNum.setText(EmptyUtils.strEmpty(itemData.getInventoryQuantity()));
        mEtNumPrice.setText(EmptyUtils.strEmpty(itemData.getPrice()));
        //设置药品说明书图片
        if (!EmptyUtils.isEmpty(itemData.getInstructionsAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, itemData.getInstructionsAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvDrugManual.getLayoutParams();
                    if (radio >= 2.029) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                    }

                    mIvDrugManual.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(itemData.getInstructionsAccessoryUrl(), mIvDrugManual);

                    mLlDrugManual.setVisibility(View.GONE);
                    mLlDrugManual.setEnabled(false);

                    mIvDrugManual.setTag(R.id.tag_1, itemData.getInstructionsAccessoryId());
                    mIvDrugManual.setTag(R.id.tag_2, itemData.getInstructionsAccessoryUrl());
                    mFlDrugManual.setVisibility(View.VISIBLE);
                }
            });
        }
        //设置商品详情图片
        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl1())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl1(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails01.getLayoutParams();
                    if (radio >= 2.029) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                    }

                    mIvCommodityDetails01.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(itemData.getDetailAccessoryUrl1(), mIvCommodityDetails01);

                    mLlCommodityDetails01.setVisibility(View.GONE);
                    mLlCommodityDetails01.setEnabled(false);

                    mIvCommodityDetails01.setTag(R.id.tag_1, itemData.getDetailAccessoryId1());
                    mIvCommodityDetails01.setTag(R.id.tag_2, itemData.getDetailAccessoryUrl1());
                    mFlCommodityDetails01.setVisibility(View.VISIBLE);
                }
            });
        }

        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl2())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl2(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails02.getLayoutParams();
                    if (radio >= 2.029) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                    }

                    mIvCommodityDetails02.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(itemData.getDetailAccessoryUrl2(), mIvCommodityDetails02);

                    mLlCommodityDetails02.setVisibility(View.GONE);
                    mLlCommodityDetails02.setEnabled(false);

                    mIvCommodityDetails02.setTag(R.id.tag_1, itemData.getDetailAccessoryId2());
                    mIvCommodityDetails02.setTag(R.id.tag_2, itemData.getDetailAccessoryUrl2());
                    mFlCommodityDetails02.setVisibility(View.VISIBLE);
                }
            });
        }

        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl3())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl3(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails03.getLayoutParams();
                    if (radio >= 2.029) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                    }

                    mIvCommodityDetails03.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(itemData.getDetailAccessoryUrl3(), mIvCommodityDetails03);

                    mLlCommodityDetails03.setVisibility(View.GONE);
                    mLlCommodityDetails03.setEnabled(false);

                    mIvCommodityDetails03.setTag(R.id.tag_1, itemData.getDetailAccessoryId3());
                    mIvCommodityDetails03.setTag(R.id.tag_2, itemData.getDetailAccessoryUrl3());
                    mFlCommodityDetails03.setVisibility(View.VISIBLE);
                }
            });
        }

        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl4())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl4(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails04.getLayoutParams();
                    if (radio >= 2.029) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                    }

                    mIvCommodityDetails04.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(itemData.getDetailAccessoryUrl4(), mIvCommodityDetails04);

                    mLlCommodityDetails04.setVisibility(View.GONE);
                    mLlCommodityDetails04.setEnabled(false);

                    mIvCommodityDetails04.setTag(R.id.tag_1, itemData.getDetailAccessoryId4());
                    mIvCommodityDetails04.setTag(R.id.tag_2, itemData.getDetailAccessoryUrl4());
                    mFlCommodityDetails04.setVisibility(View.VISIBLE);
                }
            });
        }

        if (!EmptyUtils.isEmpty(itemData.getDetailAccessoryUrl5())) {
            GlideUtils.getImageWidHeig(this, itemData.getDetailAccessoryUrl5(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvCommodityDetails05.getLayoutParams();
                    if (radio >= 2.029) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                    }

                    mIvCommodityDetails05.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(itemData.getDetailAccessoryUrl5(), mIvCommodityDetails05);

                    mLlCommodityDetails05.setVisibility(View.GONE);
                    mLlCommodityDetails05.setEnabled(false);

                    mIvCommodityDetails05.setTag(R.id.tag_1, itemData.getDetailAccessoryId5());
                    mIvCommodityDetails05.setTag(R.id.tag_2, itemData.getDetailAccessoryUrl5());
                    mFlCommodityDetails05.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvUpdate.setOnClickListener(this);
    }

    @Override
    public void rightTitleViewClick() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(this, "提示", "确定删除吗?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        showWaitDialog();
                        onDelete();
                    }
                });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()) {
            case R.id.tv_update://确定修改
                onUpdate();
                break;
        }
    }

    //更新
    protected void onUpdate() {
        if (EmptyUtils.isEmpty(mTvType.getText().toString())) {
            showShortToast("请选择药品类型");
            return;
        }
        if (EmptyUtils.isEmpty(mEtSpec.getText().toString())) {
            showShortToast("请输入药品规格");
            return;
        }
        if (EmptyUtils.isEmpty(mEtFuncMain.getText().toString())) {
            showShortToast("请输入功能主治");
            return;
        }
        if (EmptyUtils.isEmpty(mEtCommonUsage.getText().toString())) {
            showShortToast("请输入常见用法");
            return;
        }
        if (EmptyUtils.isEmpty(mEtStorageConditions.getText().toString())) {
            showShortToast("请输入贮存条件");
            return;
        }
        if (relationDiseAdapter.getData().size() <= 0) {
            showShortToast("请关联疾病");
            return;
        }
        if (EmptyUtils.isEmpty(mEtValidity.getText().toString()) || "0".equals(mEtValidity.getText().toString())) {
            showShortToast("请输入正确的有效期");
            return;
        }
        if (EmptyUtils.isEmpty(mEtNum.getText().toString()) || "0".equals(mEtNum.getText().toString())) {
            showShortToast("请输入正确的库存数量");
            return;
        }
        if (EmptyUtils.isEmpty(mEtNumPrice.getText().toString()) || "0".equals(mEtNumPrice.getText().toString())) {
            showShortToast("请输入正确的药品价格");
            return;
        }

        if (null == mIvDrugManual.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvDrugManual.getTag(R.id.tag_2)))) {
            showShortToast("请上传药品说明书");
            return;
        }

        //关联疾病数据
        List<String> relationDiseDatas = relationDiseAdapter.getData();
        associatedDiseases = "";
        for (int i = 0, size = relationDiseDatas.size(); i < size; i++) {
            if (i == size - 1) {
                associatedDiseases += relationDiseDatas.get(i);
            } else {
                associatedDiseases += relationDiseDatas.get(i) + "∞#";
            }
        }

        int selectCommodityDetailsPic = 0;
        if (null != mIvCommodityDetails01.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails01.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails02.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails02.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails03.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails03.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails04.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails04.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (null != mIvCommodityDetails05.getTag(R.id.tag_2) && !EmptyUtils.isEmpty(String.valueOf(mIvCommodityDetails05.getTag(R.id.tag_2)))) {
            selectCommodityDetailsPic += 1;
        }
        if (selectCommodityDetailsPic < 3) {
            showShortToast("请上传最少三张，至多五张商品详情图片");
            return;
        }

        //  2022-10-08 调用接口 成功之后跳转成功页面
        showWaitDialog();
        BaseModel.sendUpdateShopDrugDataRequest(TAG,itemData.getDrugDataId(), "1", "西药中成药", itemData.getItemName(), mEtSpec.getText().toString(),
                mEtProdentpName.getText().toString(), mEtStorageConditions.getText().toString(), mEtValidity.getText().toString(), mEtNum.getText().toString(),
                mEtNumPrice.getText().toString(), mIvDrugManual.getTag(R.id.tag_1), mIvCommodityDetails01.getTag(R.id.tag_1), mIvCommodityDetails02.getTag(R.id.tag_1),
                mIvCommodityDetails03.getTag(R.id.tag_1), mIvCommodityDetails04.getTag(R.id.tag_1), mIvCommodityDetails05.getTag(R.id.tag_1),
                mTvType.getTag(), mEtAprvnoBegndate.getText().toString(), mEtRegDosform.getText().toString(), mEtFuncMain.getText().toString(),
                mEtCommonUsage.getText().toString(), associatedDiseases, "", "", "", "", "",
                "", "", "", new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        // 2022-10-19 将修改的数据封装进  itemData之后 返回上一页 进行更新
                        showShortToast("修改成功");
                        Intent intent = new Intent();
                        intent.putExtra("option_type", "update");
                        intent.putExtra("position", position);
                        intent.putExtra("itemData", updateItemData(associatedDiseases));
                        setResult(RESULT_OK, intent);
                        goFinish();
                    }
                });
    }

    //删除条目
    private void onDelete() {
        BaseModel.sendDelShopDrugDataRequest(TAG, itemData.getDrugDataId(), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                // TODO: 2022-10-19 模拟请求删除接口
                showShortToast("删除成功");
                Intent intent = new Intent();
                intent.putExtra("option_type", "delete");
                intent.putExtra("position", position);
                setResult(RESULT_OK, intent);
                goFinish();
            }
        });
    }

    //更新itemdata
    private DragStoreHouseModel.DataBean updateItemData(String associatedDiseases) {
        itemData.setSpec(mEtSpec.getText().toString());
        itemData.setEnterpriseName(mEtProdentpName.getText().toString());
        itemData.setStorageConditions(mEtStorageConditions.getText().toString());
        itemData.setExpiryDateCount(mEtValidity.getText().toString());
        itemData.setInventoryQuantity(mEtNum.getText().toString());
        itemData.setPrice(mEtNumPrice.getText().toString());

        itemData.setInstructionsAccessoryId(null == mIvDrugManual.getTag(R.id.tag_1) ? "" : String.valueOf(mIvDrugManual.getTag(R.id.tag_1)));
        itemData.setInstructionsAccessoryUrl(null == mIvDrugManual.getTag(R.id.tag_2) ? "" : String.valueOf(mIvDrugManual.getTag(R.id.tag_2)));

        itemData.setDetailAccessoryId1(null == mIvCommodityDetails01.getTag(R.id.tag_1) ? "" : String.valueOf(mIvCommodityDetails01.getTag(R.id.tag_1)));
        itemData.setDetailAccessoryUrl1(null == mIvCommodityDetails01.getTag(R.id.tag_2) ? "" : String.valueOf(mIvCommodityDetails01.getTag(R.id.tag_2)));

        itemData.setDetailAccessoryId2(null == mIvCommodityDetails02.getTag(R.id.tag_1) ? "" : String.valueOf(mIvCommodityDetails02.getTag(R.id.tag_1)));
        itemData.setDetailAccessoryUrl2(null == mIvCommodityDetails02.getTag(R.id.tag_2) ? "" : String.valueOf(mIvCommodityDetails02.getTag(R.id.tag_2)));

        itemData.setDetailAccessoryId3(null == mIvCommodityDetails03.getTag(R.id.tag_1) ? "" : String.valueOf(mIvCommodityDetails03.getTag(R.id.tag_1)));
        itemData.setDetailAccessoryUrl3(null == mIvCommodityDetails03.getTag(R.id.tag_2) ? "" : String.valueOf(mIvCommodityDetails03.getTag(R.id.tag_2)));

        itemData.setDetailAccessoryId4(null == mIvCommodityDetails04.getTag(R.id.tag_1) ? "" : String.valueOf(mIvCommodityDetails04.getTag(R.id.tag_1)));
        itemData.setDetailAccessoryUrl4(null == mIvCommodityDetails04.getTag(R.id.tag_2) ? "" : String.valueOf(mIvCommodityDetails04.getTag(R.id.tag_2)));

        itemData.setDetailAccessoryId5(null == mIvCommodityDetails05.getTag(R.id.tag_1) ? "" : String.valueOf(mIvCommodityDetails05.getTag(R.id.tag_1)));
        itemData.setDetailAccessoryUrl5(null == mIvCommodityDetails05.getTag(R.id.tag_2) ? "" : String.valueOf(mIvCommodityDetails05.getTag(R.id.tag_2)));

        //西药中成药特有
        itemData.setItemType(null == mTvType.getTag() ? "" : String.valueOf(mTvType.getTag()));
        itemData.setApprovalNumber(mEtAprvnoBegndate.getText().toString());
        itemData.setRegDosform(mEtRegDosform.getText().toString());
        itemData.setEfccAtd(mEtFuncMain.getText().toString());
        itemData.setUsualWay(mEtCommonUsage.getText().toString());
        itemData.setAssociatedDiseases(associatedDiseases);
        //医疗器械
        itemData.setBrand("");
        itemData.setRegistrationCertificateNo("");
        itemData.setProductionLicenseNo("");
        itemData.setPackaging("");
        itemData.setApplicableScope("");
        itemData.setUsageDosage("");
        itemData.setPrecautions("");
        //成人用品
        itemData.setUsagemethod("");

        return itemData;
    }
}
