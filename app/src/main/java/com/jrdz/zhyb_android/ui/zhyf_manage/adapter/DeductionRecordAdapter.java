package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.DeductionRecordModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-14
 * 描    述：
 * ================================================
 */
public class DeductionRecordAdapter extends BaseQuickAdapter<DeductionRecordModel.DataBean, BaseViewHolder> {
    public DeductionRecordAdapter() {
        super(R.layout.layout_deduction_record_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, DeductionRecordModel.DataBean item) {
        baseViewHolder.setText(R.id.tv_serial_num, EmptyUtils.strEmpty(item.getSerialNumber()));
        baseViewHolder.setText(R.id.tv_deduction_time, item.getDeductionTime()+" 扣款金额："+item.getDeductionAmount()+"元");
        baseViewHolder.setText(R.id.tv_deduction_reason, EmptyUtils.strEmpty(item.getDeductionReason()));
        baseViewHolder.setText(R.id.tv_account, "账户余额("+item.getUserName()+")：");
        baseViewHolder.setText(R.id.tv_balance, item.getDepositBalance()+"元");
    }
}
