package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;

/**
 * ================================================
 * 项目名称：AndroidFrame
 * 包    名：com.example.myapplication.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-08-01
 * 描    述：
 * ================================================
 */
public class LeftLinkAdapter extends BaseQuickAdapter<PhaSortModel.DataBean, BaseViewHolder> {

    public LeftLinkAdapter() {
        super(R.layout.layout_left_link_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, PhaSortModel.DataBean item) {
        ShapeFrameLayout sflItemview= baseViewHolder.getView(R.id.sfl_itemview);
        ImageView ivSelectTag= baseViewHolder.getView(R.id.iv_select_tag);
        TextView tvName = baseViewHolder.getView(R.id.tv_name);

        tvName.setText(EmptyUtils.strEmpty(item.getCatalogueName()));
        String choose=item.getChoose();
        switch (choose){
            case "0"://未选中的item
                sflItemview.getShapeDrawableBuilder().setSolidColor(mContext.getResources().getColor(R.color.color_f4f4f4))
                        .setTopRightRadius(0)
                        .setBottomRightRadius(0)
                        .intoBackground();
                ivSelectTag.setVisibility(View.GONE);
                break;
            case "1"://选中的item
                sflItemview.getShapeDrawableBuilder().setSolidColor(mContext.getResources().getColor(R.color.white))
                        .setTopRightRadius(0)
                        .setBottomRightRadius(0)
                        .intoBackground();
                ivSelectTag.setVisibility(View.VISIBLE);
                break;
            case "top01"://选中的上一个 item
                sflItemview.getShapeDrawableBuilder().setSolidColor(mContext.getResources().getColor(R.color.color_f4f4f4))
                        .setTopRightRadius(0)
                        .setBottomRightRadius(mContext.getResources().getDimensionPixelSize(R.dimen.dp_30))
                        .intoBackground();
                ivSelectTag.setVisibility(View.GONE);
                break;
            case "bottom01"://选中的下一个 item
                sflItemview.getShapeDrawableBuilder().setSolidColor(mContext.getResources().getColor(R.color.color_f4f4f4))
                        .setTopRightRadius(mContext.getResources().getDimensionPixelSize(R.dimen.dp_30))
                        .setBottomRightRadius(0)
                        .intoBackground();
                ivSelectTag.setVisibility(View.GONE);
                break;
        }
    }

    public void setSelectedPosttion(int size,int currentposition,int selectPosition) {
        if (selectPosition-1==currentposition){
            getData().get(currentposition).setChoose("top01");
            notifyItemChanged(currentposition);

            if (selectPosition+1<size){
                getData().get(selectPosition+1).setChoose("bottom01");
                notifyItemChanged(selectPosition+1);
            }

            if (currentposition-1>=0){
                getData().get(currentposition-1).setChoose("0");
                notifyItemChanged(currentposition-1);
            }
        }else if (selectPosition+1==currentposition){
            if (selectPosition-1>=0){
                getData().get(selectPosition-1).setChoose("top01");
                notifyItemChanged(selectPosition-1);
            }

            getData().get(currentposition).setChoose("bottom01");
            notifyItemChanged(currentposition);

            if (currentposition+1<size){
                getData().get(currentposition+1).setChoose("0");
                notifyItemChanged(currentposition+1);
            }
        }else if (selectPosition-2==currentposition){
            if (currentposition-1>=0){
                getData().get(currentposition-1).setChoose("0");
                notifyItemChanged(currentposition-1);
            }

            getData().get(currentposition).setChoose("0");
            notifyItemChanged(currentposition);

            if (currentposition+1<size){
                getData().get(currentposition+1).setChoose("top01");
                notifyItemChanged(currentposition+1);
            }

            if (selectPosition+1<size){
                getData().get(selectPosition+1).setChoose("bottom01");
                notifyItemChanged(selectPosition+1);
            }
        }else if (selectPosition+2==currentposition){
            if (currentposition-1>=0){
                getData().get(currentposition-1).setChoose("bottom01");
                notifyItemChanged(currentposition-1);
            }

            getData().get(currentposition).setChoose("0");
            notifyItemChanged(currentposition);

            if (currentposition+1<size){
                getData().get(currentposition+1).setChoose("0");
                notifyItemChanged(currentposition+1);
            }

            if (selectPosition-1>=0){
                getData().get(selectPosition-1).setChoose("top01");
                notifyItemChanged(selectPosition-1);
            }
        }else {
            if (currentposition-1>=0){
                getData().get(currentposition-1).setChoose("0");
                notifyItemChanged(currentposition-1);
            }

            getData().get(currentposition).setChoose("0");
            notifyItemChanged(currentposition);

            if (currentposition+1<size){
                getData().get(currentposition+1).setChoose("0");
                notifyItemChanged(currentposition+1);
            }

            if (selectPosition-1>=0){
                getData().get(selectPosition-1).setChoose("top01");
                notifyItemChanged(selectPosition-1);
            }

            if (selectPosition+1<size){
                getData().get(selectPosition+1).setChoose("bottom01");
                notifyItemChanged(selectPosition+1);
            }
        }

        getData().get(selectPosition).setChoose("1");
        notifyItemChanged(selectPosition);
    }
}
