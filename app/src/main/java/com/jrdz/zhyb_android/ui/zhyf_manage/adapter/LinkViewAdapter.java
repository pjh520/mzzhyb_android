package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.graphics.Paint;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.view.ShapeTextView;
import com.hjq.shape.view.ShapeView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.LinkViewModel;

/**
 * ================================================
 * 项目名称：AndroidFrame
 * 包    名：com.example.myapplication.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/7/30
 * 描    述：
 * ================================================
 */
public class LinkViewAdapter extends BaseMultiItemQuickAdapter<LinkViewModel, BaseViewHolder> {
    public LinkViewAdapter() {
        super(null);
        addItemType(LinkViewModel.TITLE_TAG, R.layout.layout_linkview_title_item);
        addItemType(LinkViewModel.BODY_TAG, R.layout.layout_linkview_body_item);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.tv_add,R.id.ll_all,R.id.ll_sales,R.id.ll_price);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, LinkViewModel linkViewModel) {
        switch (baseViewHolder.getItemViewType()) {
            case LinkViewModel.TITLE_TAG:
                TextView tvTitle = baseViewHolder.getView(R.id.tv_title);

                LinearLayout llAll = baseViewHolder.getView(R.id.ll_all);
                TextView tvAll = baseViewHolder.getView(R.id.tv_all);
                ShapeView svAll = baseViewHolder.getView(R.id.sv_all);

                LinearLayout llSales = baseViewHolder.getView(R.id.ll_sales);
                TextView tvSales = baseViewHolder.getView(R.id.tv_sales);
                ShapeView svSales = baseViewHolder.getView(R.id.sv_sales);

                LinearLayout llPrice = baseViewHolder.getView(R.id.ll_price);
                TextView tvPrice = baseViewHolder.getView(R.id.tv_price);
                ShapeView svPrice = baseViewHolder.getView(R.id.sv_price);

                tvTitle.setText(linkViewModel.getTitle());
                break;
            case LinkViewModel.BODY_TAG:
                ImageView iv_pic=baseViewHolder.getView(R.id.iv_pic);
                ShapeTextView stv_otc=baseViewHolder.getView(R.id.stv_otc);
                ImageView iv_promotion=baseViewHolder.getView(R.id.iv_promotion);
                TextView tv_title=baseViewHolder.getView(R.id.tv_title);
                ShapeTextView stv_discount=baseViewHolder.getView(R.id.stv_discount);
                TextView tv_num=baseViewHolder.getView(R.id.tv_num);
                TextView tv_estimate_price=baseViewHolder.getView(R.id.tv_estimate_price);
                TextView tv_real_price=baseViewHolder.getView(R.id.tv_real_price);

                GlideUtils.loadImg("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic1.win4000.com%2Fwallpaper%2F5%2F57809651ee306.jpg&refer=http%3A%2F%2Fpic1.win4000.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1667885377&t=e3db802af6d34fcbae802c69242b5fec",iv_pic,R.drawable.ic_placeholder_bg,
                        new RoundedCornersTransformation(mContext.getResources().getDimensionPixelSize(R.dimen.dp_16),0));

                tv_real_price.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                tv_real_price.getPaint().setAntiAlias(true);
                break;
            default:
                break;
        }


    }
}
