package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.view.View;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.MsgListModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-18
 * 描    述：
 * ================================================
 */
public class MsgListAdapter extends BaseQuickAdapter<MsgListModel.DataBean, BaseViewHolder> {
    public MsgListAdapter() {
        super(R.layout.layout_msglist_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, MsgListModel.DataBean item) {
        ShapeView svRedPoint=baseViewHolder.getView(R.id.sv_red_point);

        baseViewHolder.setText(R.id.tv_title, EmptyUtils.strEmpty(item.getMessageTitle()));
        baseViewHolder.setText(R.id.tv_content, EmptyUtils.strEmpty(item.getMessageContent()));
        baseViewHolder.setText(R.id.tv_date, EmptyUtils.strEmpty(item.getCreateDT()));

        if ("0".equals(item.getIsRead())){//未读
            svRedPoint.setVisibility(View.VISIBLE);
        }else {//已读
            svRedPoint.setVisibility(View.GONE);
        }
    }
}
