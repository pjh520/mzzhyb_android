package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.MyIssuedPrescrModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-26
 * 描    述：
 * ================================================
 */
public class MyIssuedPrescrAdapter extends BaseQuickAdapter<MyIssuedPrescrModel.DataBean, BaseViewHolder> {
    public MyIssuedPrescrAdapter() {
        super(R.layout.layout_my_issued_prescr_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, MyIssuedPrescrModel.DataBean item) {
        baseViewHolder.setText(R.id.tv_prescr_no, "处方编号:"+item.getElectronicPrescriptionNo());
        baseViewHolder.setText(R.id.tv_create_time, "开具日期:"+item.getCreateDT());
        baseViewHolder.setText(R.id.tv_person_name, "姓名:"+item.getPsn_name());
        baseViewHolder.setText(R.id.tv_person_sex, "性别:"+("1".equals(item.getSex())?"男":"女"));
        baseViewHolder.setText(R.id.tv_person_age, "年龄:"+item.getAge());
        baseViewHolder.setText(R.id.tv_person_phone, "电话:"+item.getContact());
    }
}
