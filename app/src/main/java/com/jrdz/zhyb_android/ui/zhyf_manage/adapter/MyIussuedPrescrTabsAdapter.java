package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.jrdz.zhyb_android.ui.zhyf_manage.model.MyIussedTabModel;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2018/5/23
 * 描    述：ViewPager适配器(防止内存泄漏)
 * ================================================
 */
public abstract class MyIussuedPrescrTabsAdapter extends FragmentStatePagerAdapter {
    private List<MyIussedTabModel> data;

    public MyIussuedPrescrTabsAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setData(List<MyIussedTabModel> data){
        if(data != null){
            this.data = data;
        }else{
            this.data = new ArrayList<>();
        }

        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position){
        return getCustomeItem(data.get(position).getId(),position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        MyIussedTabModel item = data.get(position);
        return item.getText()+"("+item.getNum()+")";
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public void finishUpdate(@NonNull ViewGroup container) {
        try{
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException){
            System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }

    public abstract Fragment getCustomeItem(String id,int position);
}
