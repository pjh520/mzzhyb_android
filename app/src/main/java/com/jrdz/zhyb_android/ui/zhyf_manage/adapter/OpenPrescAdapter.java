package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.glide.GlideUtils;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.JustifyContent;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.OpenPrescModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.OrderDetailModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TagTextBean;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.jrdz.zhyb_android.widget.MyFlexboxLayoutManager;
import com.jrdz.zhyb_android.widget.TagTextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-25
 * 描    述：
 * ================================================
 */
public class OpenPrescAdapter extends BaseQuickAdapter<OpenPrescModel.DataBean, BaseViewHolder> {
    List<TagTextBean> tags = new ArrayList<>();
    public OpenPrescAdapter() {
        super(R.layout.layout_openpresc_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, OpenPrescModel.DataBean item) {
        TagTextView tvTitle = baseViewHolder.getView(R.id.tv_title);
        CustomeRecyclerView crvRelationDise= baseViewHolder.getView(R.id.crv_relation_dise);
        LinearLayout llPrice = baseViewHolder.getView(R.id.ll_price);
        ImageView ivShopHead = baseViewHolder.getView(R.id.iv_shop_head);

        tags.clear();
        tags.add(new TagTextBean("处方药",R.color.color_4870e0));

        if (item.getOrderGoods()!=null&&!item.getOrderGoods().isEmpty()){
            OrderDetailModel.DataBean.OrderGoodsBean orderGood = item.getOrderGoods().get(0);
            tvTitle.setContentAndTag(orderGood.getGoodsName()+"    "+orderGood.getSpec(), tags);
        }
        baseViewHolder.setText(R.id.tv_patient_info, "患者信息："+ StringUtils.encryptionName(item.getBuyer())+"    "+
                ("1".equals(item.getSex())?"男":"女")+"    "+item.getAge()+"岁\n已确诊的相关疾病:");

        //判断
        if (item.getFixmedins_code().equals(MechanismInfoUtils.getFixmedinsCode())){
            llPrice.setVisibility(View.GONE);
        }else {
            llPrice.setVisibility(View.VISIBLE);
            baseViewHolder.setText(R.id.tv_price, item.getPrescriptionPrice());
        }

        GlideUtils.loadImg(item.getStoreAccessoryUrl(),ivShopHead,R.drawable.ic_placeholder_bg,new CircleCrop());
        baseViewHolder.setText(R.id.tv_shop_name, item.getStoreName());
        //关联病种与诊断信息
        crvRelationDise.setHasFixedSize(true);
        crvRelationDise.setLayoutManager(getLayoutManager());
        OpenprescRelationDiseAdapter relationDiseAdapter = new OpenprescRelationDiseAdapter(1);
        crvRelationDise.setAdapter(relationDiseAdapter);
        //关联病种的数据
        if (item.getAssociatedDiseases()!=null){
            relationDiseAdapter.setNewData(Arrays.asList(item.getAssociatedDiseases().split(",")));
        }
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        MyFlexboxLayoutManager flexboxLayoutManager = new MyFlexboxLayoutManager(mContext);
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        flexboxLayoutManager.setFlexWrap(FlexWrap.WRAP);
        flexboxLayoutManager.setJustifyContent(JustifyContent.FLEX_START);

        return flexboxLayoutManager;
    }
}
