package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-08
 * 描    述：
 * ================================================
 */
public class OpenprescRelationDiseAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    private int mTag;//1:代表使用灰底黑字 2:代表使用蓝底白字

    public OpenprescRelationDiseAdapter(int tag) {
        super(R.layout.layout_openpresc_relation_disetype_item, null);
        this.mTag=tag;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, String dataBean) {
        ShapeTextView tvText=baseViewHolder.getView(R.id.tv_text);
        tvText.setText(EmptyUtils.strEmpty(dataBean));

        if (1==mTag){
            tvText.setTextColor(mContext.getResources().getColor(R.color.color_333333));
            tvText.getShapeDrawableBuilder().setSolidColor(mContext.getResources().getColor(R.color.windowbackground)).intoBackground();
        }else {
            tvText.setTextColor(mContext.getResources().getColor(R.color.white));
            tvText.getShapeDrawableBuilder().setSolidColor(mContext.getResources().getColor(R.color.color_4870e0)).intoBackground();
        }
    }
}

