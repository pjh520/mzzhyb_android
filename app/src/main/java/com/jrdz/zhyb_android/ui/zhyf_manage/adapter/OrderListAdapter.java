package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.DecimalFormatUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.OrderListModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TagTextBean;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.widget.TagTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-09
 * 描    述：
 * ================================================
 */
public class OrderListAdapter extends BaseQuickAdapter<OrderListModel.DataBean, BaseViewHolder> {
    List<TagTextBean> tags = new ArrayList<>();

    public OrderListAdapter() {
        super(R.layout.layout_orderlist_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder = super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.tv_01,R.id.tv_02,R.id.tv_03,R.id.tv_04);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, OrderListModel.DataBean item) {
        ImageView ivHeadProduct=baseViewHolder.getView(R.id.iv_head_product);
        TextView tvStatus=baseViewHolder.getView(R.id.tv_status);
        ShapeTextView tv01=baseViewHolder.getView(R.id.tv_01);
        ShapeTextView tv02=baseViewHolder.getView(R.id.tv_02);
        ShapeTextView tv03=baseViewHolder.getView(R.id.tv_03);
        ShapeTextView tv04=baseViewHolder.getView(R.id.tv_04);

        LinearLayout mllOrderListContain=baseViewHolder.getView(R.id.ll_order_list_contain);

        List<OrderListModel.DataBean.OrderGoodsBean> orderGoods = item.getOrderGoods();
        mllOrderListContain.removeAllViews();
        int totalNum = 0;
        for (int i = 0, size = orderGoods.size(); i < size; i++) {
            OrderListModel.DataBean.OrderGoodsBean orderGood = orderGoods.get(i);

            totalNum += orderGood.getGoodsNum();

            //设置商品信息
            View view = LayoutInflater.from(mContext).inflate(R.layout.layout_orderlist_product_item, mllOrderListContain, false);
            ImageView ivPic = view.findViewById(R.id.iv_pic);
            ShapeTextView stvOtc = view.findViewById(R.id.stv_otc);
            ShapeTextView stvEnterpriseTag= view.findViewById(R.id.stv_enterprise_tag);
            TagTextView tvTitle = view.findViewById(R.id.tv_title);
            TextView tvEstimatePrice = view.findViewById(R.id.tv_estimate_price);
            TextView tvRealPrice = view.findViewById(R.id.tv_real_price);
            View lineItem = view.findViewById(R.id.line_item);

            GlideUtils.loadImg(orderGood.getBannerAccessoryUrl1(), ivPic, R.drawable.ic_placeholder_bg,
                    new RoundedCornersTransformation(mContext.getResources().getDimensionPixelSize(R.dimen.dp_16), 0));
            //判断是否是处方药
            if ("1".equals(orderGood.getItemType())) {
                stvOtc.setText("OTC");
                stvOtc.setVisibility(View.VISIBLE);
            } else if ("2".equals(orderGood.getItemType())) {
                stvOtc.setText("处方药");
                stvOtc.setVisibility(View.VISIBLE);
            } else {
                stvOtc.setVisibility(View.GONE);
            }
            //判断是否是平台药 若是 需要展示医保
            tags.clear();
            if ("1".equals(orderGood.getIsPlatformDrug())){//是平台药
                tags.add(new TagTextBean("医保",R.color.color_ee8734));
                tvTitle.setContentAndTag(EmptyUtils.strEmpty(orderGood.getGoodsName()), tags);
            }else {
                tvTitle.setContentAndTag(EmptyUtils.strEmpty(orderGood.getGoodsName()), tags);
            }

            //判断是否支持企业基金支付
            if ("1".equals(orderGood.getIsEnterpriseFundPay())){
                stvEnterpriseTag.setVisibility(View.VISIBLE);
            }else {
                stvEnterpriseTag.setVisibility(View.GONE);
            }
            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(orderGood.getPrice())));
            tvRealPrice.setText("x" + orderGood.getGoodsNum());
            //2022-10-10 最后一个item的分割线需要隐藏
            if (i == size - 1) {
                lineItem.setVisibility(View.GONE);
            } else {
                lineItem.setVisibility(View.VISIBLE);
            }
            mllOrderListContain.addView(view);
        }
        GlideUtils.loadImg(item.getStoreAccessoryUrl(), ivHeadProduct, R.drawable.ic_placeholder_bg, new CircleCrop());
        baseViewHolder.setText(R.id.tv_order_id,"订单编号："+item.getOrderNo());
        baseViewHolder.setText(R.id.tv_total_num,"共" + totalNum + "件商品");
        baseViewHolder.setText(R.id.tv_total_price,EmptyUtils.strEmpty(DecimalFormatUtils.noZero(item.getTotalAmount())));

        //(0 不开处方 1、待开处方 2、已开处方3、待医师确认处方4、第三方开处方 5.未提交问诊信息）
        if ("2".equals(item.getIsPrescription())){
            tv04.setVisibility(View.VISIBLE);
            tv04.setText("查看处方");
        }else {
            tv04.setVisibility(View.GONE);
        }
        //订单状态（1、待付款2、已付款3、已发货4、已收货5用户取消订单（未支付）6系统取消订单（未支付完成）7系统取消订单（支付完成）8商家取消订单（支付完成））
        switch (item.getOrderStatus()){
            case "1"://待付款
                if ("1".equals(item.getIsPrescription())){
                    tvStatus.setText("待开处方");
                }else {
                    tvStatus.setText("待付款");
                }

                tv01.setVisibility(View.GONE);
                if ("2".equals(item.getOrderType())){
                    tv02.setVisibility(View.GONE);
                    tv03.setVisibility(View.VISIBLE);
                    tv03.setText("立即支付");
                }else {
                    tv02.setVisibility(View.VISIBLE);
                    tv02.setText("联系买家");
                    tv03.setVisibility(View.VISIBLE);
                    tv03.setText("修改订单");
                }
                break;
            case "2"://待发货
                tvStatus.setText("待发货");

                tv01.setVisibility(View.VISIBLE);
                tv01.setText("取消订单");
                tv02.setVisibility(View.VISIBLE);
                tv02.setText("联系买家");
                tv03.setVisibility(View.VISIBLE);
                tv03.setText("立即发货");
                break;
            case "3"://待收货
                tvStatus.setText("待收货");

                tv01.setVisibility(View.VISIBLE);
                tv01.setText("取消订单");
                tv02.setVisibility(View.VISIBLE);
                tv02.setText("联系买家");
                if ("1".equals(item.getShippingMethod())){//自提
                    tv03.setVisibility(View.GONE);
                }else {//商家配送
                    tv03.setVisibility(View.VISIBLE);
                    tv03.setText("查看物流");
                }
                break;
            case "4"://已完成
                tvStatus.setText("已完成");

                if ("2".equals(item.getOrderType())){
                    tv01.setVisibility(View.GONE);
                    tv02.setVisibility(View.GONE);
                    tv03.setVisibility(View.VISIBLE);
                    tv03.setText("去退款");
                }else {
                    tv01.setVisibility(View.VISIBLE);
                    tv01.setText("取消订单");
                    tv02.setVisibility(View.VISIBLE);
                    tv02.setText("联系买家");
                    if ("1".equals(item.getShippingMethod())){//自提
                        tv03.setVisibility(View.GONE);
                    }else {//商家配送
                        tv03.setVisibility(View.VISIBLE);
                        tv03.setText("查看物流");
                    }
                }

                break;
            case "5"://用户取消订单（未支付）
                tvStatus.setText("已取消");

                if ("2".equals(item.getOrderType())){
                    tv01.setVisibility(View.GONE);
                    tv02.setVisibility(View.GONE);
                    tv03.setVisibility(View.VISIBLE);
                    tv03.setText("+购物车");
                }else {
                    tv01.setVisibility(View.GONE);
                    tv02.setVisibility(View.VISIBLE);
                    tv02.setText("联系买家");
                    tv03.setVisibility(View.VISIBLE);
                    tv03.setText("删除订单");
                }
                break;
            case "6"://系统取消订单（未支付完成）
                tvStatus.setText("已取消");

                if ("2".equals(item.getOrderType())){
                    tv01.setVisibility(View.GONE);
                    tv02.setVisibility(View.GONE);
                    tv03.setVisibility(View.VISIBLE);
                    tv03.setText("+购物车");
                }else {
                    tv01.setVisibility(View.GONE);
                    tv02.setVisibility(View.VISIBLE);
                    tv02.setText("联系买家");
                    tv03.setVisibility(View.VISIBLE);
                    tv03.setText("删除订单");
                }
                break;
            case "7"://系统取消订单（支付完成）
                tvStatus.setText("已取消");
                if ("2".equals(item.getOrderType())){
                    tv01.setVisibility(View.GONE);
                    tv02.setVisibility(View.GONE);
                    tv03.setVisibility(View.VISIBLE);
                    tv03.setText("+购物车");
                }else {
                    tv01.setVisibility(View.VISIBLE);
                    tv01.setText("退款进度");
                    tv02.setVisibility(View.VISIBLE);
                    tv02.setText("联系买家");
                    tv03.setVisibility(View.VISIBLE);
                    tv03.setText("删除订单");
                }
                break;
            case "8"://商家取消订单（支付完成）
                tvStatus.setText("已取消");

                if ("2".equals(item.getOrderType())){
                    tv01.setVisibility(View.GONE);
                    tv02.setVisibility(View.GONE);
                    tv03.setVisibility(View.VISIBLE);
                    tv03.setText("+购物车");
                }else {
                    tv01.setVisibility(View.VISIBLE);
                    tv01.setText("退款进度");
                    tv02.setVisibility(View.VISIBLE);
                    tv02.setText("联系买家");

                    if ("1".equals(item.getShippingMethod())){//自提
                        tv03.setVisibility(View.GONE);
                    }else {//商家配送
                        // 2023/6/10 此处还得加个判断 商户端那边取消的时候 订单是否是处在代发货的状态 如果是 那么查看物流就该隐藏
                        if ("0".equals(item.getIsViewLogistics())){
                            tv03.setVisibility(View.VISIBLE);
                            tv03.setText("查看物流");
                        }else {
                            tv03.setVisibility(View.GONE);
                        }
                    }
                }
                break;
        }
    }
}
