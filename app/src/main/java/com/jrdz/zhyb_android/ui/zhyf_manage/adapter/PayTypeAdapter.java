package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PayTypeModel;


/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：
 * ================================================
 */
public class PayTypeAdapter extends BaseQuickAdapter<PayTypeModel, BaseViewHolder> {
    public PayTypeAdapter() {
        super(R.layout.layout_select_pay_type_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, PayTypeModel payTypeModel) {
        ImageView ivPic=baseViewHolder.getView(R.id.iv_pic);

        ivPic.setImageResource(payTypeModel.getImg());
        baseViewHolder.setText(R.id.tv_name, payTypeModel.getText());
    }
}
