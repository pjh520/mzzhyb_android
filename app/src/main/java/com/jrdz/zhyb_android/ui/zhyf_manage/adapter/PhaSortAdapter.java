package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.widget.glide.GlideUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;

/**
 * ================================================
 * 版    本：1.0
 * 创建日期：2022-09-29
 * 描    述：
 * ================================================
 */
public class PhaSortAdapter extends BaseQuickAdapter<PhaSortModel.DataBean, BaseViewHolder> {
    public PhaSortAdapter() {
        super(R.layout.layout_phasort_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, PhaSortModel.DataBean item) {
        ImageView ivMall = baseViewHolder.getView(R.id.iv_mall);

        if ("3".equals(item.getIsSystem())){
            GlideUtils.loadImg(R.drawable.ic_add_dir_offline, ivMall);
        }else {
            GlideUtils.loadImg(item.getAccessoryUrl(), ivMall,R.drawable.ic_add_dir_bg,new CircleCrop());
        }

        baseViewHolder.setText(R.id.tv_mall,item.getCatalogueName());
    }
}
