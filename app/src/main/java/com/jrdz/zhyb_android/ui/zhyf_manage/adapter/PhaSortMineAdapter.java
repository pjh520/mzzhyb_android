package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortMineModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-09-29
 * 描    述：
 * ================================================
 */
public class PhaSortMineAdapter extends BaseQuickAdapter<PhaSortMineModel, BaseViewHolder> {
    public PhaSortMineAdapter() {
        super(R.layout.layout_phasort_mine_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, PhaSortMineModel phaSortModel) {
        ImageView ivMall = baseViewHolder.getView(R.id.iv_mall);

        ivMall.setImageResource(phaSortModel.getImg());
        baseViewHolder.setText(R.id.tv_mall,phaSortModel.getText());
    }
}
