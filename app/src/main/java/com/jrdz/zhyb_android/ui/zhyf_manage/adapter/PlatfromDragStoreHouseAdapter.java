package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.DragStoreHouseModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/13
 * 描    述：
 * ================================================
 */
public class PlatfromDragStoreHouseAdapter extends BaseQuickAdapter<DragStoreHouseModel.DataBean, BaseViewHolder> {
    public PlatfromDragStoreHouseAdapter() {
        super(R.layout.layout_drag_storehouse_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, DragStoreHouseModel.DataBean resultObjBean) {
        if ("1".equals(resultObjBean.getIsPlatformDrug())) {//平台药品库-西药中成药
            setDdata01(baseViewHolder, resultObjBean);
        } else {
            switch (resultObjBean.getDrugsClassification()) {
                case "1"://1:店铺药品库-西药中成药
                    setDdata02(baseViewHolder, resultObjBean);
                    break;
                case "2"://2:店铺药品库-医疗器械
                    setDdata03(baseViewHolder, resultObjBean);
                    break;
                case "3"://3:店铺药品库-成人用品
                    setDdata04(baseViewHolder, resultObjBean);
                    break;
                case "4"://4:店铺药品库-个人用品
                    setDdata05(baseViewHolder, resultObjBean);
                    break;
            }
        }
    }

    //平台药品库-西药中成药
    private void setDdata01(BaseViewHolder baseViewHolder, DragStoreHouseModel.DataBean resultObjBean) {
        TextView tv_reg_nam = baseViewHolder.getView(R.id.tv_reg_nam);

        TextView tv_med_list_codg_tag = baseViewHolder.getView(R.id.tv_med_list_codg_tag);
        TextView tv_med_list_codg = baseViewHolder.getView(R.id.tv_med_list_codg);

        TextView tv_drug_type_tag = baseViewHolder.getView(R.id.tv_drug_type_tag);
        TextView tv_drug_type = baseViewHolder.getView(R.id.tv_drug_type);

        LinearLayout ll_aprvno_begndate = baseViewHolder.getView(R.id.ll_aprvno_begndate);
        TextView tv_aprvno_begndate_tag = baseViewHolder.getView(R.id.tv_aprvno_begndate_tag);
        TextView tv_aprvno_begndate = baseViewHolder.getView(R.id.tv_aprvno_begndate);

        TextView tv_drug_spec_tag = baseViewHolder.getView(R.id.tv_drug_spec_tag);
        TextView tv_drug_spec = baseViewHolder.getView(R.id.tv_drug_spec);

        TextView tv_prodentp_name_tag = baseViewHolder.getView(R.id.tv_prodentp_name_tag);
        TextView tv_prodentp_name = baseViewHolder.getView(R.id.tv_prodentp_name);

        ll_aprvno_begndate.setVisibility(View.VISIBLE);

        tv_reg_nam.setText("药品名称：" + resultObjBean.getItemName());

        tv_med_list_codg_tag.setText("药品编码");
        tv_med_list_codg.setText(EmptyUtils.strEmpty(resultObjBean.getItemCode()));

        tv_drug_type_tag.setText("类型");
        if (EmptyUtils.isEmpty(resultObjBean.getItemType())||"0".equals(resultObjBean.getItemType())){
            tv_drug_type.setText("");
        }else {
            tv_drug_type.setText("1".equals(resultObjBean.getItemType())?"非处方药（OTC）":"处方药");
        }

        tv_aprvno_begndate_tag.setText("批准文号");
        tv_aprvno_begndate.setText(EmptyUtils.strEmpty(resultObjBean.getApprovalNumber()));

        tv_drug_spec_tag.setText("药品规格");
        tv_drug_spec.setText(EmptyUtils.strEmpty(resultObjBean.getSpec()));

        tv_prodentp_name_tag.setText("生产企业");
        tv_prodentp_name.setText(EmptyUtils.strEmpty(resultObjBean.getEnterpriseName()));
    }

    //1:店铺药品库-西药中成药
    private void setDdata02(BaseViewHolder baseViewHolder, DragStoreHouseModel.DataBean resultObjBean) {
        TextView tv_reg_nam = baseViewHolder.getView(R.id.tv_reg_nam);

        TextView tv_med_list_codg_tag = baseViewHolder.getView(R.id.tv_med_list_codg_tag);
        TextView tv_med_list_codg = baseViewHolder.getView(R.id.tv_med_list_codg);

        TextView tv_drug_type_tag = baseViewHolder.getView(R.id.tv_drug_type_tag);
        TextView tv_drug_type = baseViewHolder.getView(R.id.tv_drug_type);

        LinearLayout ll_aprvno_begndate = baseViewHolder.getView(R.id.ll_aprvno_begndate);
        TextView tv_aprvno_begndate_tag = baseViewHolder.getView(R.id.tv_aprvno_begndate_tag);
        TextView tv_aprvno_begndate = baseViewHolder.getView(R.id.tv_aprvno_begndate);

        TextView tv_drug_spec_tag = baseViewHolder.getView(R.id.tv_drug_spec_tag);
        TextView tv_drug_spec = baseViewHolder.getView(R.id.tv_drug_spec);

        TextView tv_prodentp_name_tag = baseViewHolder.getView(R.id.tv_prodentp_name_tag);
        TextView tv_prodentp_name = baseViewHolder.getView(R.id.tv_prodentp_name);

        ll_aprvno_begndate.setVisibility(View.GONE);

        tv_reg_nam.setText("药品名称：" + resultObjBean.getItemName());

        tv_med_list_codg_tag.setText("药品编码");
        tv_med_list_codg.setText(EmptyUtils.strEmpty(resultObjBean.getItemCode()));

        tv_drug_type_tag.setText("类型");
        if (EmptyUtils.isEmpty(resultObjBean.getItemType())||"0".equals(resultObjBean.getItemType())){
            tv_drug_type.setText("");
        }else {
            tv_drug_type.setText("1".equals(resultObjBean.getItemType())?"非处方药（OTC）":"处方药");
        }

        tv_drug_spec_tag.setText("药品规格");
        tv_drug_spec.setText(EmptyUtils.strEmpty(resultObjBean.getSpec()));

        tv_prodentp_name_tag.setText("生产企业");
        tv_prodentp_name.setText(EmptyUtils.strEmpty(resultObjBean.getEnterpriseName()));
    }

    //2:店铺药品库-医疗器械
    private void setDdata03(BaseViewHolder baseViewHolder, DragStoreHouseModel.DataBean resultObjBean) {
        TextView tv_reg_nam = baseViewHolder.getView(R.id.tv_reg_nam);

        TextView tv_med_list_codg_tag = baseViewHolder.getView(R.id.tv_med_list_codg_tag);
        TextView tv_med_list_codg = baseViewHolder.getView(R.id.tv_med_list_codg);

        TextView tv_drug_type_tag = baseViewHolder.getView(R.id.tv_drug_type_tag);
        TextView tv_drug_type = baseViewHolder.getView(R.id.tv_drug_type);

        LinearLayout ll_aprvno_begndate = baseViewHolder.getView(R.id.ll_aprvno_begndate);
        TextView tv_aprvno_begndate_tag = baseViewHolder.getView(R.id.tv_aprvno_begndate_tag);
        TextView tv_aprvno_begndate = baseViewHolder.getView(R.id.tv_aprvno_begndate);

        TextView tv_drug_spec_tag = baseViewHolder.getView(R.id.tv_drug_spec_tag);
        TextView tv_drug_spec = baseViewHolder.getView(R.id.tv_drug_spec);

        TextView tv_prodentp_name_tag = baseViewHolder.getView(R.id.tv_prodentp_name_tag);
        TextView tv_prodentp_name = baseViewHolder.getView(R.id.tv_prodentp_name);

        ll_aprvno_begndate.setVisibility(View.GONE);

        tv_reg_nam.setText("名称：" + resultObjBean.getItemName());

        tv_med_list_codg_tag.setText("编码");
        tv_med_list_codg.setText(EmptyUtils.strEmpty(resultObjBean.getItemCode()));

        tv_drug_type_tag.setText("品牌");
        tv_drug_type.setText(EmptyUtils.strEmpty(resultObjBean.getBrand()));

        tv_drug_spec_tag.setText("规格");
        tv_drug_spec.setText(EmptyUtils.strEmpty(resultObjBean.getSpec()));

        tv_prodentp_name_tag.setText("生产企业");
        tv_prodentp_name.setText(EmptyUtils.strEmpty(resultObjBean.getEnterpriseName()));
    }

    //3:店铺药品库-成人用品
    private void setDdata04(BaseViewHolder baseViewHolder, DragStoreHouseModel.DataBean resultObjBean) {
        TextView tv_reg_nam = baseViewHolder.getView(R.id.tv_reg_nam);

        TextView tv_med_list_codg_tag = baseViewHolder.getView(R.id.tv_med_list_codg_tag);
        TextView tv_med_list_codg = baseViewHolder.getView(R.id.tv_med_list_codg);

        TextView tv_drug_type_tag = baseViewHolder.getView(R.id.tv_drug_type_tag);
        TextView tv_drug_type = baseViewHolder.getView(R.id.tv_drug_type);

        LinearLayout ll_aprvno_begndate = baseViewHolder.getView(R.id.ll_aprvno_begndate);
        TextView tv_aprvno_begndate_tag = baseViewHolder.getView(R.id.tv_aprvno_begndate_tag);
        TextView tv_aprvno_begndate = baseViewHolder.getView(R.id.tv_aprvno_begndate);

        TextView tv_drug_spec_tag = baseViewHolder.getView(R.id.tv_drug_spec_tag);
        TextView tv_drug_spec = baseViewHolder.getView(R.id.tv_drug_spec);

        TextView tv_prodentp_name_tag = baseViewHolder.getView(R.id.tv_prodentp_name_tag);
        TextView tv_prodentp_name = baseViewHolder.getView(R.id.tv_prodentp_name);

        ll_aprvno_begndate.setVisibility(View.GONE);

        tv_reg_nam.setText("名称：" + resultObjBean.getItemName());

        tv_med_list_codg_tag.setText("编码");
        tv_med_list_codg.setText(EmptyUtils.strEmpty(resultObjBean.getItemCode()));

        tv_drug_type_tag.setText("品牌");
        tv_drug_type.setText(EmptyUtils.strEmpty(resultObjBean.getBrand()));

        tv_drug_spec_tag.setText("规格");
        tv_drug_spec.setText(EmptyUtils.strEmpty(resultObjBean.getSpec()));

        tv_prodentp_name_tag.setText("生产企业");
        tv_prodentp_name.setText(EmptyUtils.strEmpty(resultObjBean.getEnterpriseName()));
    }

    //4:店铺药品库-个人用品
    private void setDdata05(BaseViewHolder baseViewHolder, DragStoreHouseModel.DataBean resultObjBean) {
        TextView tv_reg_nam = baseViewHolder.getView(R.id.tv_reg_nam);

        TextView tv_med_list_codg_tag = baseViewHolder.getView(R.id.tv_med_list_codg_tag);
        TextView tv_med_list_codg = baseViewHolder.getView(R.id.tv_med_list_codg);

        TextView tv_drug_type_tag = baseViewHolder.getView(R.id.tv_drug_type_tag);
        TextView tv_drug_type = baseViewHolder.getView(R.id.tv_drug_type);

        LinearLayout ll_aprvno_begndate = baseViewHolder.getView(R.id.ll_aprvno_begndate);
        TextView tv_aprvno_begndate_tag = baseViewHolder.getView(R.id.tv_aprvno_begndate_tag);
        TextView tv_aprvno_begndate = baseViewHolder.getView(R.id.tv_aprvno_begndate);

        TextView tv_drug_spec_tag = baseViewHolder.getView(R.id.tv_drug_spec_tag);
        TextView tv_drug_spec = baseViewHolder.getView(R.id.tv_drug_spec);

        TextView tv_prodentp_name_tag = baseViewHolder.getView(R.id.tv_prodentp_name_tag);
        TextView tv_prodentp_name = baseViewHolder.getView(R.id.tv_prodentp_name);

        ll_aprvno_begndate.setVisibility(View.GONE);

        tv_reg_nam.setText("名称：" + resultObjBean.getItemName());

        tv_med_list_codg_tag.setText("编码");
        tv_med_list_codg.setText(EmptyUtils.strEmpty(resultObjBean.getItemCode()));

        tv_drug_type_tag.setText("品牌");
        tv_drug_type.setText(EmptyUtils.strEmpty(resultObjBean.getBrand()));

        tv_drug_spec_tag.setText("规格");
        tv_drug_spec.setText(EmptyUtils.strEmpty(resultObjBean.getSpec()));

        tv_prodentp_name_tag.setText("生产企业");
        tv_prodentp_name.setText(EmptyUtils.strEmpty(resultObjBean.getEnterpriseName()));
    }
}
