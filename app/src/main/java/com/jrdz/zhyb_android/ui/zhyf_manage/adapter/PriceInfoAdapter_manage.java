package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.graphics.Paint;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PriceInfoModel_manage;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：
 * ================================================
 */
public class PriceInfoAdapter_manage extends BaseQuickAdapter<PriceInfoModel_manage, BaseViewHolder> {
    public PriceInfoAdapter_manage() {
        super(R.layout.layout_price_info_manage_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, PriceInfoModel_manage priceInfoModel) {
        ShapeTextView tvOriginalPrice=baseViewHolder.getView(R.id.tv_original_price);
        baseViewHolder.setText(R.id.tv_title, EmptyUtils.strEmpty(priceInfoModel.getTitle()));
        baseViewHolder.setText(R.id.tv_present_price, priceInfoModel.getPrice()+"元/每月");

        tvOriginalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        tvOriginalPrice.getPaint().setAntiAlias(true);
        tvOriginalPrice.setText(priceInfoModel.getOriginal_price()+"元/每月");
    }
}
