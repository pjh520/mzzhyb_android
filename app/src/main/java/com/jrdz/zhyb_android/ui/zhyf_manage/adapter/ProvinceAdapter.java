package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.QueryLogisticsSetModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-12
 * 描    述：
 * ================================================
 */
public class ProvinceAdapter extends BaseQuickAdapter<QueryLogisticsSetModel.DataBean, BaseViewHolder> {
    public ProvinceAdapter() {
        super(R.layout.layout_logis_setting_province_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, QueryLogisticsSetModel.DataBean item) {
        ImageView ivRegion02=baseViewHolder.getView(R.id.iv_region02);

        ivRegion02.setImageResource("1".equals(item.getIsOn())?R.drawable.ic_dagou_pre:R.drawable.ic_dagou_nor);
        baseViewHolder.setText(R.id.tv_region02, EmptyUtils.strEmpty(item.getAreaName()));
    }
}
