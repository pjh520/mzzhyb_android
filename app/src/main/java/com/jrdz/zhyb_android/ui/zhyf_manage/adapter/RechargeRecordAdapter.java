package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.RechargeRecordModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：
 * ================================================
 */
public class RechargeRecordAdapter extends BaseQuickAdapter<RechargeRecordModel.DataBean, BaseViewHolder> {
    public RechargeRecordAdapter() {
        super(R.layout.layout_recharge_record_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, RechargeRecordModel.DataBean item) {
        TextView tvAccount=baseViewHolder.getView(R.id.tv_account);
        TextView tvStatus=baseViewHolder.getView(R.id.tv_status);
        switch (item.getManagementServiceMode()){
            case "0"://非管理服务模式
                baseViewHolder.setText(R.id.tv_type,"充值账户");
                tvAccount.setVisibility(View.VISIBLE);
                tvAccount.setText("("+item.getRechargeAccount()+")");
                break;
            case "1"://按月付费
                baseViewHolder.setText(R.id.tv_type,"管理服务模式：按月付费");
                tvAccount.setVisibility(View.GONE);
                break;
            case "2"://按季付费
                baseViewHolder.setText(R.id.tv_type,"管理服务模式：按季付费");
                tvAccount.setVisibility(View.GONE);
                break;
            case "3"://按半年付费
                baseViewHolder.setText(R.id.tv_type,"管理服务模式：按半年付费");
                tvAccount.setVisibility(View.GONE);
                break;
            case "4"://按年付费
                baseViewHolder.setText(R.id.tv_type,"管理服务模式：按年付费");
                tvAccount.setVisibility(View.GONE);
                break;
        }

        //（0待充值1充值成功2充值失败）
        switch (item.getRechargeStatus()){
            case "0":
                tvStatus.setText("待充值");
                break;
            case "1":
                tvStatus.setText("充值成功");
                break;
            case "2":
                tvStatus.setText("充值失败");
                break;
        }

        baseViewHolder.setText(R.id.tv_recharge_time, "充值时间："+item.getRechargeTime());
        baseViewHolder.setText(R.id.tv_receipt_time, "到账时间："+item.getReceiptTime());

        //充值方式（1微信2支付宝3云闪付）
        switch (item.getRechargeWay()){
            case "1":
                baseViewHolder.setText(R.id.tv_recharge_type, "充值方式：微信");
                break;
            case "2":
                baseViewHolder.setText(R.id.tv_recharge_type, "充值方式：支付宝");
                break;
            case "3":
                baseViewHolder.setText(R.id.tv_recharge_type, "充值方式：云闪付");
                break;
        }
        baseViewHolder.setText(R.id.tv_recharge_money, "+"+item.getRechargeAmount());
    }
}
