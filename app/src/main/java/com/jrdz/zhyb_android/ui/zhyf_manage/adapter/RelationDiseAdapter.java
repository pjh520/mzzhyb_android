package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-08
 * 描    述：
 * ================================================
 */
public class RelationDiseAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    public RelationDiseAdapter() {
        super(R.layout.layout_relation_disetype_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, String dataBean) {
        baseViewHolder.setText(R.id.tv_text, EmptyUtils.strEmpty(dataBean));
    }
}

