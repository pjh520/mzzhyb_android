package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.DecimalFormatUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;

import java.math.BigDecimal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-05
 * 描    述：
 * ================================================
 */
public class ShopCarPopAdapter extends BaseQuickAdapter<GoodsModel.DataBean, BaseViewHolder> {
    public ShopCarPopAdapter() {
        super(R.layout.layout_shopcar_pop_product_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder = super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.fl_num_add,R.id.fl_num_reduce);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, GoodsModel.DataBean item) {
        ImageView ivPic=baseViewHolder.getView(R.id.iv_pic);
        ShapeTextView stvOtc=baseViewHolder.getView(R.id.stv_otc);
        ShapeTextView stvEnterpriseTag= baseViewHolder.getView(R.id.stv_enterprise_tag);
        TextView tvTitle=baseViewHolder.getView(R.id.tv_title);
        ShapeTextView stvDiscount=baseViewHolder.getView(R.id.stv_discount);
        ShapeTextView tvOnlyhasNum=baseViewHolder.getView(R.id.tv_onlyhas_num);
        TextView tvSpec=baseViewHolder.getView(R.id.tv_spec);
        TextView tvEstimatePrice=baseViewHolder.getView(R.id.tv_estimate_price);
        ShapeTextView stv02=baseViewHolder.getView(R.id.stv_02);
        TextView tvRealPrice=baseViewHolder.getView(R.id.tv_real_price);
        TextView tvSelectNum=baseViewHolder.getView(R.id.tv_select_num);
        View line=baseViewHolder.getView(R.id.line);

        GlideUtils.loadImg(item.getBannerAccessoryUrl1(),ivPic,R.drawable.ic_good_placeholder,
                new RoundedCornersTransformation(mContext.getResources().getDimensionPixelSize(R.dimen.dp_16),0));
        //判断是否是处方药
        if ("1".equals(item.getItemType())){
            stvOtc.setText("OTC");
            stvOtc.setVisibility(View.VISIBLE);
        }else if ("2".equals(item.getItemType())){
            stvOtc.setText("处方药");
            stvOtc.setVisibility(View.VISIBLE);
        }else {
            stvOtc.setVisibility(View.GONE);
        }
        //判断是否有折扣
        double promotionDiscountVlaue = new BigDecimal(item.getPromotionDiscount()).doubleValue();
        if ("1".equals(item.getIsPromotion())&&promotionDiscountVlaue<10){
            stv02.setVisibility(View.VISIBLE);
            tvRealPrice.setVisibility(View.VISIBLE);
            stvDiscount.setVisibility(View.VISIBLE);

            stvDiscount.setText(DecimalFormatUtils.noZero(item.getPromotionDiscount())+"折优惠");
            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(item.getPreferentialPrice())));
            tvRealPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            tvRealPrice.getPaint().setAntiAlias(true);
            tvRealPrice.setText("¥"+DecimalFormatUtils.noZero(item.getPrice()));
        }else {
            stv02.setVisibility(View.GONE);
            tvRealPrice.setVisibility(View.GONE);
            stvDiscount.setVisibility(View.GONE);

            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(item.getPrice())));
        }
        tvTitle.setText(EmptyUtils.strEmpty(item.getGoodsName()));

        if ("1".equals(item.getIsShowMargin())&&item.getInventoryQuantity()<=5){
            tvOnlyhasNum.setVisibility(View.VISIBLE);
            tvOnlyhasNum.setText("仅剩"+item.getInventoryQuantity()+"件");
        }else {
            tvOnlyhasNum.setVisibility(View.GONE);
        }
        tvSpec.setText(EmptyUtils.strEmpty(item.getSpec()));

        tvSelectNum.setText(String.valueOf(item.getShoppingCartNum()));
        if (baseViewHolder.getAbsoluteAdapterPosition()== getData().size()-1){
            line.setVisibility(View.GONE);
        }else {
            line.setVisibility(View.VISIBLE);
        }

        //判断是否支持企业基金支付
        if ("1".equals(item.getIsEnterpriseFundPay())){
            stvEnterpriseTag.setVisibility(View.VISIBLE);
        }else {
            stvEnterpriseTag.setVisibility(View.GONE);
        }
    }
}
