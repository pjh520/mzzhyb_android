package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.graphics.Color;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.DraggableController;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.ShopDecoActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.fragment.ShopStyleFragment;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.ShopStyleModel;


/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-16
 * 描    述：
 * ================================================
 */
public class ShopDecoSortAdapter extends BaseMultiItemQuickAdapter<ShopStyleModel.DataBean.ClassificationBean, BaseViewHolder> {
    private ShopDecoActivity mShopDecoActivity;
    private DraggableController mDraggableController;

    public ShopDecoSortAdapter(ShopDecoActivity shopDecoActivity) {
        super(null);
        addItemType(ShopStyleFragment.SORT_NOR, R.layout.layout_shop_deco_sort_item01);
        addItemType(ShopStyleFragment.SORT_NEWADD, R.layout.layout_shop_deco_sort_item02);
        addItemType(ShopStyleFragment.SORT_ADDBTN, R.layout.layout_shop_deco_sort_item03);
        this.mShopDecoActivity=shopDecoActivity;
        mDraggableController = new DraggableController(this);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ShopStyleModel.DataBean.ClassificationBean item) {
        mDraggableController.initView(baseViewHolder);
        switch (baseViewHolder.getItemViewType()) {
            case ShopStyleFragment.SORT_NOR://分类不可修改的固定项
                ShapeTextView stv01=baseViewHolder.getView(R.id.stv_01);

                stv01.setText(item.getCatalogueName());
                break;
            case ShopStyleFragment.SORT_NEWADD://分类可修改的新增项
                ShapeTextView stv02=baseViewHolder.getView(R.id.stv_02);

                stv02.setText(item.getCatalogueName());

                if ("1".equals(Constants.AppStorage.APP_STORE_STATUS)) {//上线状态 不能修改店铺信息
                    stv02.getShapeDrawableBuilder().setStrokeColor(mContext.getResources().getColor(R.color.color_bababa))
                            .setSolidColor(mContext.getResources().getColor(R.color.color_f4f4f4)).intoBackground();
                    stv02.setTextColor(mContext.getResources().getColor(R.color.txt_color_666));
                }else {
                    stv02.getShapeDrawableBuilder().setStrokeColor(mContext.getResources().getColor(R.color.color_4870e0))
                            .setSolidColor(Color.parseColor("#264870E0")).intoBackground();
                    stv02.setTextColor(mContext.getResources().getColor(R.color.color_4870e0));
                }
                break;
            case ShopStyleFragment.SORT_ADDBTN://分类的添加按钮
                ShapeFrameLayout sfl03=baseViewHolder.getView(R.id.stv_03);
                ImageView iv03=baseViewHolder.getView(R.id.iv_03);

                if ("1".equals(Constants.AppStorage.APP_STORE_STATUS)) {//上线状态 不能修改店铺信息
                    sfl03.getShapeDrawableBuilder().setStrokeColor(mContext.getResources().getColor(R.color.color_bababa))
                            .setSolidColor(mContext.getResources().getColor(R.color.color_f4f4f4)).intoBackground();

                    iv03.setImageResource(R.drawable.ic_add_sort_online);
                }else {
                    sfl03.getShapeDrawableBuilder().setStrokeColor(mContext.getResources().getColor(R.color.color_4870e0))
                            .setSolidColor(Color.parseColor("#264870E0")).intoBackground();

                    iv03.setImageResource(R.drawable.ic_add_sort_offline);
                }
                break;
        }
    }

    public DraggableController getDraggableController() {
        return mDraggableController;
    }
}
