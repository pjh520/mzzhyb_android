package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TransactionDetailsModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-06-13
 * 描    述：
 * ================================================
 */
public class TransactionDetailsAdapter extends BaseQuickAdapter<TransactionDetailsModel.DataBean, BaseViewHolder> {
    public TransactionDetailsAdapter() {
        super(R.layout.layout_transaction_details_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, TransactionDetailsModel.DataBean item) {
        baseViewHolder.setText(R.id.tv_date, EmptyUtils.strEmpty(item.getTransactionTime()));
        baseViewHolder.setText(R.id.tv_money, EmptyUtils.strEmpty(item.getTotalAmount()));
        baseViewHolder.setText(R.id.tv_pay_type, EmptyUtils.strEmpty(item.getPayDesc()));
        baseViewHolder.setText(R.id.tv_product_name, EmptyUtils.strEmpty(item.getOrderDesc()));
        baseViewHolder.setText(R.id.tv_total_amount, "商家总收入:"+EmptyUtils.strEmpty(item.getAccountBalance()));
    }
}
