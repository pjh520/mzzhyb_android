package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import android.graphics.Color;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;


/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：
 * ================================================
 */
public class TransactionScreenPayTypeAdapter extends BaseQuickAdapter<PhaSortModel.DataBean, BaseViewHolder> {
    private ShapeTextView currentFlItem;
    private PhaSortModel.DataBean currentData;

    public TransactionScreenPayTypeAdapter() {
        super(R.layout.layout_transscreen_pay_type_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, PhaSortModel.DataBean item) {
        ShapeTextView stvText=baseViewHolder.getView(R.id.stv_text);
        stvText.setText(EmptyUtils.strEmpty(item.getCatalogueName()));

        if ("0".equals(item.getChoose())){
            stvText.getShapeDrawableBuilder().setSolidColor(Color.parseColor("#F7F6FB")).intoBackground();
            stvText.setTextColor(mContext.getResources().getColor(R.color.color_333333));
        }else {
            stvText.getShapeDrawableBuilder().setSolidColor(Color.parseColor("#ECF0FC")).intoBackground();
            stvText.setTextColor(mContext.getResources().getColor(R.color.color_4870e0));

            currentFlItem=stvText;
            currentData=item;
        }
    }

    public ShapeTextView getCurrentFlItem() {
        return currentFlItem;
    }

    public void setCurrentFlItem(ShapeTextView currentFlItem) {
        this.currentFlItem = currentFlItem;
    }

    public PhaSortModel.DataBean getCurrentData() {
        return currentData;
    }

    public void setCurrentData(PhaSortModel.DataBean currentData) {
        this.currentData = currentData;
    }
}
