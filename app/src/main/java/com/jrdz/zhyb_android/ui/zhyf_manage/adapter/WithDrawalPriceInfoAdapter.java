package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PriceInfoModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：
 * ================================================
 */
public class WithDrawalPriceInfoAdapter extends BaseQuickAdapter<PriceInfoModel, BaseViewHolder> {
    public WithDrawalPriceInfoAdapter() {
        super(R.layout.layout_withdrawal_price_info_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, PriceInfoModel priceInfoModel) {
        baseViewHolder.setText(R.id.tv_price, EmptyUtils.strEmpty(priceInfoModel.getPrice()));
    }
}
