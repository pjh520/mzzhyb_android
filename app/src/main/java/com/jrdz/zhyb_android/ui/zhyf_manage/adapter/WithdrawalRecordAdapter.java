package com.jrdz.zhyb_android.ui.zhyf_manage.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.WithdrawalRecordModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：
 * ================================================
 */
public class WithdrawalRecordAdapter extends BaseQuickAdapter<WithdrawalRecordModel.DataBean, BaseViewHolder> {
    public WithdrawalRecordAdapter() {
        super(R.layout.layout_withdrawal_record_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, WithdrawalRecordModel.DataBean item) {
        baseViewHolder.setText(R.id.tv_type, "1".equals(item.getReceiveMethod())?"微信提现":"支付宝提现");
        baseViewHolder.setText(R.id.tv_account, "("+item.getReceiveAccount()+")");
        //提现状态（1、待审核2、提现成功3、提现失败）
        switch (item.getTakeStatus()){
            case "1":
                baseViewHolder.setText(R.id.tv_status, "待审核");
                break;
            case "2":
                baseViewHolder.setText(R.id.tv_status, "提现成功");
                break;
            case "3":
                baseViewHolder.setText(R.id.tv_status, "提现失败");
                break;
        }
        baseViewHolder.setText(R.id.tv_recharge_time, EmptyUtils.strEmpty(item.getApplyTime()));
        baseViewHolder.setText(R.id.tv_receipt_time, EmptyUtils.strEmpty(item.getTransferTime()));
        baseViewHolder.setText(R.id.tv_recharge_money, "-"+item.getReceivedAmount());
    }
}
