package com.jrdz.zhyb_android.ui.zhyf_manage.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.ElectPrescActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.MyIssuedPrescrActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.MyIssuedPrescrAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.MyIssuedPrescrModel;
import com.scwang.smart.refresh.layout.constant.RefreshState;

import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-26
 * 描    述：
 * ================================================
 */
public class MyIssuedPrescrFragment extends BaseRecyclerViewFragment {
    private String id;
    private MyIssuedPrescrActivity activity;
    //列表刷新
    private ObserverWrapper<String> mRefreshObserver=new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String s) {
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mTitleBar.setVisibility(View.GONE);
    }

    @Override
    protected void initAdapter() {
        mAdapter=new MyIssuedPrescrAdapter();
    }

    @Override
    public void initData() {
        id = getArguments().getString("id");
        super.initData();
        MsgBus.sendMyIssuedPrescrRefresh().observe(this, mRefreshObserver);
        activity=(MyIssuedPrescrActivity)getContext();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    protected void getData() {
        super.getData();

        if (mRefreshLayout.getState() == RefreshState.Refreshing){
            activity.getTabData();
        }

        switch (id){
            case "1":
                getShopData();
                break;
            case "2":
                getOnlineData();
                break;
        }
    }

    //获取店铺处方
    private void getShopData() {
        MyIssuedPrescrModel.sendStoreElectronicPrescriptionListRequest(TAG, String.valueOf(mPageNum), "20", activity.getBeginDate(), activity.getEndDate(), new CustomerJsonCallBack<MyIssuedPrescrModel>() {
            @Override
            public void onRequestError(MyIssuedPrescrModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(MyIssuedPrescrModel returnData) {
                hideRefreshView();
                List<MyIssuedPrescrModel.DataBean> myIssuedPrescrModels = returnData.getData();
                if (mAdapter!=null&&myIssuedPrescrModels != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(myIssuedPrescrModels);
                    } else {
                        mAdapter.addData(myIssuedPrescrModels);
                        mAdapter.loadMoreComplete();
                    }

                    if (myIssuedPrescrModels.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    //获取在线处方
    private void getOnlineData() {
        MyIssuedPrescrModel.sendOnlineElectronicPrescriptionListRequest(TAG, String.valueOf(mPageNum), "20", activity.getBeginDate(), activity.getEndDate(), new CustomerJsonCallBack<MyIssuedPrescrModel>() {
            @Override
            public void onRequestError(MyIssuedPrescrModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(MyIssuedPrescrModel returnData) {
                hideRefreshView();

                List<MyIssuedPrescrModel.DataBean> myIssuedPrescrModels = returnData.getData();
                if (mAdapter!=null&&myIssuedPrescrModels != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(myIssuedPrescrModels);
                    } else {
                        mAdapter.addData(myIssuedPrescrModels);
                        mAdapter.loadMoreComplete();
                    }

                    if (myIssuedPrescrModels.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        MyIssuedPrescrModel.DataBean itemData = ((MyIssuedPrescrAdapter) adapter).getItem(position);
        ElectPrescActivity.newIntance(getContext(), itemData.getOrderNo());
    }

    public static MyIssuedPrescrFragment newIntance(String id) {
        MyIssuedPrescrFragment myIssuedPrescrFragment = new MyIssuedPrescrFragment();
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        myIssuedPrescrFragment.setArguments(bundle);
        return myIssuedPrescrFragment;
    }
}
