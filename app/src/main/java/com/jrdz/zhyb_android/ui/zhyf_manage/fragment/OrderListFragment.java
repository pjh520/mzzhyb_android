package com.jrdz.zhyb_android.ui.zhyf_manage.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.BroadCastReceiveUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.ScanPayQRCodeActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.ElectPrescActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.ElectPrescActivity2;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.ImmediateDeliveryActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.LogisticsActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.OrderDetailActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.RefundProgressActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.ScanPayOrderDetailActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.UpdateOrderActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.OrderListAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.OrderListModel;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.ElectPrescActivity2_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.ElectPrescActivity_user;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-09
 * 描    述：我的订单列表 fragment
 * ================================================
 */
public class OrderListFragment extends BaseRecyclerViewFragment {
    private String status;
    private CustomerDialogUtils customerDialogUtils;

    private BroadCastReceiveUtils mReceiver = new BroadCastReceiveUtils() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String OperationType = intent.getStringExtra("OperationType");
            String orderNo = intent.getStringExtra("orderNo");
            String status02 = intent.getStringExtra("status");
            if (status02.equals(status)) {
                switch (OperationType) {
                    case "immediate_Delivery"://立即发货
                        deleteItem(orderNo);
                        break;
                }
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public void enableDataInitialized() {
        isDataInitialized = false;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_order_list;
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    protected void initAdapter() {
        mAdapter = new OrderListAdapter();
    }

    @Override
    public void initData() {
        status = getArguments().getString("status", "");
        super.initData();
        BroadCastReceiveUtils.registerLocalReceiver(getContext(), Constants.Action.SEND_REFRESH_DAILY_LOTTERY, mReceiver);

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    protected void getData() {
        super.getData();
        OrderListModel.sendOrderListRequest(TAG + status, status, String.valueOf(mPageNum), "10", new CustomerJsonCallBack<OrderListModel>() {
            @Override
            public void onRequestError(OrderListModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(OrderListModel returnData) {
                hideRefreshView();
                List<OrderListModel.DataBean> datas = returnData.getData();
                if (mAdapter != null && datas != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(datas);

                        if (datas.isEmpty()) {
                            mAdapter.isUseEmpty(true);
                        }
                    } else {
                        mAdapter.addData(datas);
                        mAdapter.loadMoreComplete();
                    }

                    if (datas.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        OrderListModel.DataBean itemData = ((OrderListAdapter) adapter).getItem(position);
        //跳转订单详情页面
        if ("2".equals(itemData.getOrderType())) {//O2O订单
            switch (itemData.getOrderStatus()){
                case "1":
                    goScanPayQRCode(itemData.getOrderNo());
                    break;
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                    Intent intent02 = new Intent(getContext(), ScanPayOrderDetailActivity.class);
                    intent02.putExtra("orderNo", itemData.getOrderNo());
                    startActivityForResult(intent02, new OnActivityCallback() {
                        @Override
                        public void onActivityResult(int resultCode, @Nullable Intent data) {
                            if (resultCode == -1) {
                                String OperationType = data.getStringExtra("OperationType");
                                String orderNo = data.getStringExtra("orderNo");
                                switch (OperationType) {
                                    case "cancle"://取消订单
                                        deleteItem(orderNo);
                                        break;
                                }
                            }
                        }
                    });
                    break;
            }
        } else {
            Intent intent = new Intent(getContext(), OrderDetailActivity.class);
            intent.putExtra("orderNo", itemData.getOrderNo());
            startActivityForResult(intent, new OnActivityCallback() {
                @Override
                public void onActivityResult(int resultCode, @Nullable Intent data) {
                    if (resultCode == -1) {
                        String OperationType = data.getStringExtra("OperationType");
                        String orderNo = data.getStringExtra("orderNo");
                        switch (OperationType) {
                            case "cancle"://取消订单
                            case "delete"://删除订单
                                deleteItem(orderNo);
                                break;
                            case "updateOrder"://修改订单
                                showWaitDialog();
                                onRefresh(mRefreshLayout);
                                break;
                        }
                    }
                }
            });
        }
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        OrderListModel.DataBean itemData = ((OrderListAdapter) adapter).getItem(position);

        switch (((TextView) view).getText().toString()) {
            case "取消订单":
                onCancle(itemData.getOrderNo(), position);
                break;
            case "退款进度":
                RefundProgressActivity.newIntance(getContext(), itemData.getOrderNo(), itemData.getOnlineAmount(), itemData.getSettlementStatus());
                break;
            case "联系买家":
                onTakePhone(itemData.getPhone());
                break;
            case "修改订单":
                Intent intent = new Intent(getContext(), UpdateOrderActivity.class);
                intent.putExtra("OrderNo", itemData.getOrderNo());
                intent.putExtra("ShippingMethod", itemData.getShippingMethod());
                intent.putExtra("FullName", itemData.getFullName());
                intent.putExtra("Phone", itemData.getPhone());
                intent.putExtra("Location", itemData.getLocation());
                intent.putExtra("Address", itemData.getAddress());
                intent.putExtra("AreaName", itemData.getAreaName());
                intent.putExtra("CityName", itemData.getCityName());
                intent.putExtra("latitude", itemData.getLatitude());
                intent.putExtra("longitude", itemData.getLongitude());
                startActivityForResult(intent, new OnActivityCallback() {
                    @Override
                    public void onActivityResult(int resultCode, @Nullable Intent data) {
                        if (resultCode == -1) {
                            showWaitDialog();
                            onRefresh(mRefreshLayout);
                        }
                    }
                });
                break;
            case "立即发货":
                //开具处方状态
                if ("0".equals(itemData.getIsPrescription()) || "2".equals(itemData.getIsPrescription())) {//不需开具处方或者已开具处方
                    immediateDelivery(itemData, position);
                } else {//需开具处方但未开具处方
                    showShortToast("未开具处方");
                }
                break;
            case "查看物流":
                LogisticsActivity.newIntance(getContext(), itemData.getOrderNo());
                break;
            case "删除订单":
                onDelete(itemData.getOrderNo(), position);
                break;
            case "立即支付"://线下订单才会有-跳转商户收款页面
                goScanPayQRCode(itemData.getOrderNo());
                break;
            case "去退款"://线下订单才会有
                onRefund(itemData.getOrderNo(), position);
                break;
            case "+购物车"://线下订单才会有
                addShopCar(itemData.getOrderNo(),1);
                break;
            case "查看处方":
                if ("1".equals(itemData.getIsThirdPartyPres())){
                    ElectPrescActivity2.newIntance(getContext(), itemData.getOrderNo());
                }else {
                    ElectPrescActivity.newIntance(getContext(), itemData.getOrderNo());
                }

                break;
        }
    }

    //跳转扫码支付页面
    private void goScanPayQRCode(String orderNo) {
        Intent intent = new Intent(getContext(), ScanPayQRCodeActivity.class);
        intent.putExtra("OrderNo", orderNo);
        startActivityForResult(intent, new OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == -1) {
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                }
            }
        });
    }

    //取消订单
    private void onCancle(String orderNo, int position) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(getContext(), "确定取消订单吗？", "注意：取消订单后医保支付部分将撤销,同时在线支付部分也将原路返回到个人账户。", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                // 2022-10-10 请求接口取消该订单
                showWaitDialog();
                BaseModel.sendStoreCancelOrderRequest(TAG, orderNo, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast(returnData.getMsg());
                        //当订单取消成功之后 前端需要移除该订单的展示
                        mAdapter.remove(position);
                    }
                });
            }
        });
    }

    //联系买家
    private void onTakePhone(String phone) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(getContext(), "提示", "是否拨打买家电话" + phone + "?", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                RxTool.takePhone(getContext(), phone);
            }
        });
    }

    //立即发货
    private void immediateDelivery(OrderListModel.DataBean itemData, int position) {
        if ("0".equals(itemData.getIsLogistics())) {
            showWaitDialog();
            BaseModel.sendImmediateDeliveryRequest(TAG, itemData.getOrderNo(), "1", "自提", itemData.getFullName(), itemData.getPhone(),
                    itemData.getLocation() + itemData.getAddress(), "", "", "", "",
                    new CustomerJsonCallBack<BaseModel>() {
                        @Override
                        public void onRequestError(BaseModel returnData, String msg) {
                            hideWaitDialog();
                            showShortToast(msg);
                        }

                        @Override
                        public void onRequestSuccess(BaseModel returnData) {
                            hideWaitDialog();
                            //当订单立即发货成功之后 前端需要移除该订单的展示
                            mAdapter.remove(position);
                        }
                    });
        } else {
            Intent intent = new Intent(getContext(), ImmediateDeliveryActivity.class);
            intent.putExtra("orderNo", itemData.getOrderNo());
            intent.putExtra("isLogistics", itemData.getIsLogistics());
            intent.putExtra("fullName", itemData.getFullName());
            intent.putExtra("phone", itemData.getPhone());
            intent.putExtra("location", itemData.getLocation());
            intent.putExtra("address", itemData.getAddress());
            startActivityForResult(intent, new BaseFragment.OnActivityCallback() {
                @Override
                public void onActivityResult(int resultCode, @Nullable Intent data) {
                    if (resultCode == -1) {
                        //当订单立即发货成功之后 前端需要移除该订单的展示
                        mAdapter.remove(position);
                    }
                }
            });
        }
    }

    //删除订单
    private void onDelete(String orderNo, int position) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(getContext(), "确定删除此订单吗？", "注意:订单删除后不可恢复,谨慎操作!", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                // 2022-10-10 请求接口删除该订单
                showWaitDialog();
                BaseModel.sendStoreDelOrderRequest(TAG, orderNo, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast(returnData.getMsg());
                        //当订单删除成功之后 前端需要移除该订单的展示
                        mAdapter.remove(position);
                    }
                });
            }
        });
    }

    //删除指定的列表item
    private void deleteItem(String orderNo) {
        for (int i = 0, size = mAdapter.getData().size(); i < size; i++) {
            OrderListModel.DataBean data = ((OrderListAdapter) mAdapter).getData().get(i);
            if (orderNo.equals(data.getOrderNo())) {
                mAdapter.remove(i);
                return;
            }
        }
    }

    //线下订单--去退款
    private void onRefund(String orderNo, int position) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(getContext(), "确定退款吗？", "注意：退款后企业基金支付将原路返回到用户个人账户。", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {}

            @Override
            public void onBtn02Click() {
                // 2022-10-10 请求接口取消该订单
                showWaitDialog();
                BaseModel.sendStoreCancelOrderRequest(TAG, orderNo, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast(returnData.getMsg());
                        //当订单取消成功之后 前端需要移除该订单的展示
                        mAdapter.remove(position);
                    }
                });
            }
        });
    }

    //线下订单--+购物车
    private void addShopCar(String orderNo, int shoppingCartNum) {
        showWaitDialog();
        BaseModel.sendAddOrderShopCarRequest_mana(TAG, orderNo, shoppingCartNum, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("加入购物车成功");
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BroadCastReceiveUtils.unregisterLocalReceiver(getContext(), mReceiver);
    }

    public static OrderListFragment newIntance(String status) {
        OrderListFragment orderFragment = new OrderListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("status", status);
        orderFragment.setArguments(bundle);
        return orderFragment;
    }
}
