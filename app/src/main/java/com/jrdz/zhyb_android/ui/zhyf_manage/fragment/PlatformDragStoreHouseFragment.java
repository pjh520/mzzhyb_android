package com.jrdz.zhyb_android.ui.zhyf_manage.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.DrugStoreHouseActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.LookPlatformWestDragActivtiy;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.PlatfromDragStoreHouseAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.DragStoreHouseDetailModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.DragStoreHouseModel;

import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-18
 * 描    述：平台药品库
 * ================================================
 */
public class PlatformDragStoreHouseFragment extends BaseRecyclerViewFragment {
    private TextView mTvNum;
    private ShapeTextView mTvAdd;
    private int from;

    private DrugStoreHouseActivity drugStoreHouseActivity;

    //监听外部搜索
    private ObserverWrapper<String> mOnrefreshObserve = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.fragment_platform_drag_storehouse;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mTvNum = view.findViewById(R.id.tv_num);
        mTvAdd = view.findViewById(R.id.tv_add);

        mTvAdd.setVisibility(View.GONE);
    }

    @Override
    public void initAdapter() {
        mAdapter = new PlatfromDragStoreHouseAdapter();
    }

    @Override
    public void initData() {
        from = getArguments().getInt("from", 0);
        super.initData();
        MsgBus.sendDrugStoreHouseSearch().observeForever(mOnrefreshObserve);
        drugStoreHouseActivity = (DrugStoreHouseActivity) getActivity();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        // 2022-10-18 请求接口获取数据
        DragStoreHouseModel.sendPlatformDragListRequest(TAG, drugStoreHouseActivity == null ? "" : drugStoreHouseActivity.getSearchText(),
                drugStoreHouseActivity == null ? "" : drugStoreHouseActivity.getBarCode(),
                String.valueOf(mPageNum), "20", new CustomerJsonCallBack<DragStoreHouseModel>() {
                    @Override
                    public void onRequestError(DragStoreHouseModel returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(DragStoreHouseModel returnData) {
                        hideRefreshView();
                        List<DragStoreHouseModel.DataBean> infos = returnData.getData();
                        if (mAdapter!=null&&infos != null) {
                            if (mPageNum == 0) {
                                mAdapter.setNewData(infos);

                                if (mTvNum != null) {
                                    mTvNum.setText("西药中成药：" + returnData.getTotalItems() + "条");
                                }
                                if (infos.size() <= 0) {
                                    mAdapter.isUseEmpty(true);
                                }
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        DragStoreHouseModel.DataBean itemData = ((PlatfromDragStoreHouseAdapter) adapter).getItem(position);

        showWaitDialog();
        DragStoreHouseDetailModel.sendPlatformDragDetailRequest(TAG, itemData.getItemCode(), new CustomerJsonCallBack<DragStoreHouseDetailModel>() {
            @Override
            public void onRequestError(DragStoreHouseDetailModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(DragStoreHouseDetailModel returnData) {
                hideWaitDialog();

                DragStoreHouseModel.DataBean data = returnData.getData();
                if (data!=null){
                    if (0 == from) {//进入查看页面
                        LookPlatformWestDragActivtiy.newIntance(getContext(), data);
                    } else if (1 == from) {//点击返回
                        Intent intent = new Intent();
                        intent.putExtra("itemData", data);
                        getActivity().setResult(Activity.RESULT_OK, intent);
                        ((BaseActivity) getActivity()).goFinish();
                    }
                }else {
                    showShortToast("服务器返回数据有误，请重新进入页面");
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MsgBus.sendDrugStoreHouseSearch().removeObserver(mOnrefreshObserve);
    }

    public static PlatformDragStoreHouseFragment newIntance(int from) {
        PlatformDragStoreHouseFragment platformDragStoreHouseFragment = new PlatformDragStoreHouseFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", from);
        platformDragStoreHouseFragment.setArguments(bundle);
        return platformDragStoreHouseFragment;
    }
}
