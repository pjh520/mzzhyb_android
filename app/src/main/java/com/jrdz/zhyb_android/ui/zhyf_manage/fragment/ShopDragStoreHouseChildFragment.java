package com.jrdz.zhyb_android.ui.zhyf_manage.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.AddShopAdultProductActivtiy;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.AddShopApparatusActivtiy;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.AddShopPersProductActivtiy;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.AddShopWestDragActivtiy;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.DrugStoreHouseActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.UpdateShopAdultProductActivtiy;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.UpdateShopApparatusActivtiy;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.UpdateShopPersProductActivtiy;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.UpdateShopWestDragActivtiy;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.PlatfromDragStoreHouseAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.DragStoreHouseModel;

import java.math.BigDecimal;
import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-18
 * 描    述：
 * ================================================
 */
public class ShopDragStoreHouseChildFragment extends BaseRecyclerViewFragment {
    private TextView mTvNum;
    private ShapeTextView mTvAdd;

    private String status;
    private int from;
    private DrugStoreHouseActivity drugStoreHouseActivity;
    String totalItems = "0";


    //activity 回调
    private OnActivityCallback mActivityForResult = new OnActivityCallback() {
        @Override
        public void onActivityResult(int resultCode, @Nullable Intent data) {
            if (resultCode == -1) {
                String option_type = data.getStringExtra("option_type");
                switch (option_type) {
                    case "add":
                        showWaitDialog();
                        onRefresh(mRefreshLayout);
                        break;
                    case "delete":
                        int position_delete = data.getIntExtra("position", 0);
                        mAdapter.remove(position_delete);

                        totalItems = new BigDecimal(totalItems).subtract(new BigDecimal("1")).toPlainString();
                        mTvNum.setText("目录条数：" + totalItems + "条");
                        break;
                    case "update":
                        int position = data.getIntExtra("position", 0);
                        DragStoreHouseModel.DataBean itemData = data.getParcelableExtra("itemData");

                        mAdapter.setData(position, itemData);
                        break;
                }
            }
        }
    };

    //监听外部搜索
    private ObserverWrapper<String> mOnrefreshObserve = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.fragment_platform_drag_storehouse;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mTvNum = view.findViewById(R.id.tv_num);
        mTvAdd = view.findViewById(R.id.tv_add);
    }

    @Override
    public void initAdapter() {
        mAdapter = new PlatfromDragStoreHouseAdapter();
    }

    @Override
    public void initData() {
        status = getArguments().getString("status", "");
        from = getArguments().getInt("from", 0);
        super.initData();
        MsgBus.sendDrugStoreHouseSearch().observeForever(mOnrefreshObserve);
        drugStoreHouseActivity = (DrugStoreHouseActivity) getActivity();

        if (0 == from) {
            mTvAdd.setVisibility(View.VISIBLE);
        } else if (1 == from) {
            mTvAdd.setVisibility(View.GONE);
        }

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();

        mTvAdd.setOnClickListener(this);
    }

    @Override
    public void getData() {
        //  2022-10-18 请求接口获取数据
        DragStoreHouseModel.sendShopDragListRequest(TAG + status, drugStoreHouseActivity == null ? "" : drugStoreHouseActivity.getSearchText(),
                drugStoreHouseActivity == null ? "" : drugStoreHouseActivity.getBarCode(),
                status, String.valueOf(mPageNum), "20", new CustomerJsonCallBack<DragStoreHouseModel>() {
                    @Override
                    public void onRequestError(DragStoreHouseModel returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(DragStoreHouseModel returnData) {
                        hideRefreshView();

                        List<DragStoreHouseModel.DataBean> infos = returnData.getData();
                        if (mAdapter != null && infos != null) {
                            if (mPageNum == 0) {
                                mAdapter.setNewData(infos);
                                totalItems = returnData.getTotalItems();
                                if (mTvNum!=null){
                                    mTvNum.setText("目录条数：" + totalItems + "条");
                                }
                                if (infos.size() <= 0) {
                                    mAdapter.isUseEmpty(true);
                                }
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_add://新增
                switch (status) {
                    case "1"://西药中成药
                        Intent intent01 = new Intent(getContext(), AddShopWestDragActivtiy.class);
                        startActivityForResult(intent01, mActivityForResult);
                        break;
                    case "2"://医疗器械
                        Intent intent02 = new Intent(getContext(), AddShopApparatusActivtiy.class);
                        startActivityForResult(intent02, mActivityForResult);
                        break;
                    case "3"://成人用品
                        Intent intent03 = new Intent(getContext(), AddShopAdultProductActivtiy.class);
                        startActivityForResult(intent03, mActivityForResult);
                        break;
                    case "4"://个人用品
                        Intent intent04 = new Intent(getContext(), AddShopPersProductActivtiy.class);
                        startActivityForResult(intent04, mActivityForResult);
                        break;
                }
                break;
        }
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.onItemClick(adapter, view, position);
        DragStoreHouseModel.DataBean itemData = ((PlatfromDragStoreHouseAdapter) adapter).getItem(position);
        if (0 == from) {
            switch (status) {
                case "1"://西药中成药
                    Intent intent01 = new Intent(getContext(), UpdateShopWestDragActivtiy.class);
                    intent01.putExtra("position", position);
                    intent01.putExtra("itemData", itemData);
                    startActivityForResult(intent01, mActivityForResult);
                    break;
                case "2"://医疗器械
                    Intent intent02 = new Intent(getContext(), UpdateShopApparatusActivtiy.class);
                    intent02.putExtra("position", position);
                    intent02.putExtra("itemData", itemData);
                    startActivityForResult(intent02, mActivityForResult);
                    break;
                case "3"://成人用品
                    Intent intent03 = new Intent(getContext(), UpdateShopAdultProductActivtiy.class);
                    intent03.putExtra("position", position);
                    intent03.putExtra("itemData", itemData);
                    startActivityForResult(intent03, mActivityForResult);
                    break;
                case "4"://个人用品
                    Intent intent04 = new Intent(getContext(), UpdateShopPersProductActivtiy.class);
                    intent04.putExtra("position", position);
                    intent04.putExtra("itemData", itemData);
                    startActivityForResult(intent04, mActivityForResult);
                    break;
            }
        } else if (1 == from) {//点击返回
            Intent intent = new Intent();
            intent.putExtra("itemData", itemData);
            getActivity().setResult(Activity.RESULT_OK, intent);
            ((BaseActivity) getActivity()).goFinish();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MsgBus.sendDrugStoreHouseSearch().removeObserver(mOnrefreshObserve);
    }

    public static ShopDragStoreHouseChildFragment newIntance(String status, int from) {
        ShopDragStoreHouseChildFragment shopDragStoreHouseChildFragment = new ShopDragStoreHouseChildFragment();
        Bundle bundle = new Bundle();
        bundle.putString("status", status);
        bundle.putInt("from", from);
        shopDragStoreHouseChildFragment.setArguments(bundle);
        return shopDragStoreHouseChildFragment;
    }
}
