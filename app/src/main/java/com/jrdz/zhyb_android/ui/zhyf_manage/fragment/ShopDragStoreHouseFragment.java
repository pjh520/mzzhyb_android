package com.jrdz.zhyb_android.ui.zhyf_manage.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.frame.compiler.utils.viewPage.BaseViewPagerAndTabsAdapter_new;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;

import java.util.Arrays;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-18
 * 描    述：店铺药品库
 * ================================================
 */
public class ShopDragStoreHouseFragment extends BaseFragment {
    private SlidingTabLayout mStbInside;
    private ViewPager mVpInside;

    String[] titles = new String[]{"西药中成药","医疗器械", "成人用品", "个人用品"};
    private int from;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_shop_drag_storehouse;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mStbInside = view.findViewById(R.id.stb_inside);
        mVpInside = view.findViewById(R.id.vp_inside);
    }

    @Override
    public void initData() {
        from = getArguments().getInt("from", 0);
        super.initData();

        initTabLayout();
    }

    //初始化tab  "西药中成药","医疗器械", "成人用品", "个人用品"
    private void initTabLayout() {
        BaseViewPagerAndTabsAdapter_new baseViewPagerAndTabsAdapter_new=new BaseViewPagerAndTabsAdapter_new(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                //在售中
                ShopDragStoreHouseChildFragment shopDragStoreHouseChildFragment=null;
                switch (position){
                    case 0://西药中成药
                        shopDragStoreHouseChildFragment= ShopDragStoreHouseChildFragment.newIntance("1",from);
                        break;
                    case 1://医疗器械
                        shopDragStoreHouseChildFragment= ShopDragStoreHouseChildFragment.newIntance("2",from);
                        break;
                    case 2://成人用品
                        shopDragStoreHouseChildFragment= ShopDragStoreHouseChildFragment.newIntance("3",from);
                        break;
                    case 3://个人用品
                        shopDragStoreHouseChildFragment= ShopDragStoreHouseChildFragment.newIntance("4",from);
                        break;
                }
                return shopDragStoreHouseChildFragment;
            }
        };

        baseViewPagerAndTabsAdapter_new.setData(Arrays.asList(titles));
        mVpInside.setAdapter(baseViewPagerAndTabsAdapter_new);
        mStbInside.setViewPager(mVpInside);
    }

    public static ShopDragStoreHouseFragment newIntance(int from) {
        ShopDragStoreHouseFragment shopDragStoreHouseFragment = new ShopDragStoreHouseFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("from", from);
        shopDragStoreHouseFragment.setArguments(bundle);
        return shopDragStoreHouseFragment;
    }
}
