package com.jrdz.zhyb_android.ui.zhyf_manage.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.InputType;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocation;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.AssetUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.utils.wheel.CityWheelUtils;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.switchButton.SwitchButton;
import com.frame.lbs_library.utils.CustomLocationBean;
import com.frame.lbs_library.utils.ILocationListener;
import com.frame.lbs_library.utils.LBSUtil;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.hjq.shape.view.ShapeEditText;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.model.KVModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.LogisDeliveManageActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.CityListModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.DistributionScopeModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.StoreInfoModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.location.LocationTool;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;
import com.jrdz.zhyb_android.widget.pop.UpdateNumPop;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-16
 * 描    述：店铺风格
 * ================================================
 */
public class ShopInfoFragment extends BaseFragment implements CompressUploadSinglePicUtils_zhyf.IChoosePic {
    public final String TAG_SELECT_BANNER_01 = "1";//缩略图

    private ShapeFrameLayout mSflHorizontalPic;
    private LinearLayout mLlAddHorizontalPic;
    private FrameLayout mFlHorizontalPic;
    private ImageView mIvHorizontalPic;
    private ImageView mIvDelete;
    private EditText mEtShopName;
    private SwitchButton mSbAutoOffOnLine;
    private TextView mTvStartTime,mTvBusinessStartTime;
    private TextView mTvEndTime,mTvBusinessEndTime;
    private SwitchButton mSbIsShowSold;
    private SwitchButton mSbIsShowMonthlySales;
    private SwitchButton mSbIsShowRemainder;
    private ShapeFrameLayout mFlReducePrice;
    private ShapeTextView mEtNumPrice;
    private ShapeFrameLayout mFlAddPrice;
    private LinearLayout mLlShopAddress;
    private TextView mTvShopAddress;
    private View mLineDept;
    private EditText mEtDetailAddress;
    private FrameLayout mFlDitu;
    private EditText mEtPhone;
    private EditText mEtSellerWx;
    private LinearLayout mLlAgreement;
    private TextView mEtDistriScope;
    private EditText mEtManageScope;
    private EditText mEtFeaturedServices;
    private TextView mTv01;
    private ShapeEditText mEtMainDesc;
    private ShapeTextView mTvSave;

    private CompressUploadSinglePicUtils_zhyf compressUploadSinglePicUtils;
    private WheelUtils wheelUtils;
    private UpdateNumPop updateNumPop;
    private List<CityListModel> data;
    private CityWheelUtils cityWheelUtils;
    private int requestTag=0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_shop_info;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mSflHorizontalPic = view.findViewById(R.id.sfl_horizontal_pic);
        mLlAddHorizontalPic = view.findViewById(R.id.ll_add_horizontal_pic);
        mFlHorizontalPic = view.findViewById(R.id.fl_horizontal_pic);
        mIvHorizontalPic = view.findViewById(R.id.iv_horizontal_pic);
        mIvDelete = view.findViewById(R.id.iv_delete);
        mEtShopName = view.findViewById(R.id.et_shop_name);
        mSbAutoOffOnLine = view.findViewById(R.id.sb_auto_off_on_line);
        mTvStartTime = view.findViewById(R.id.tv_start_time);
        mTvEndTime = view.findViewById(R.id.tv_end_time);
        mSbIsShowSold = view.findViewById(R.id.sb_is_show_sold);
        mSbIsShowMonthlySales = view.findViewById(R.id.sb_is_show_monthly_sales);
        mSbIsShowRemainder = view.findViewById(R.id.sb_is_show_remainder);
        mFlReducePrice = view.findViewById(R.id.fl_reduce_price);
        mEtNumPrice = view.findViewById(R.id.et_num_price);
        mFlAddPrice = view.findViewById(R.id.fl_add_price);
        mLlShopAddress = view.findViewById(R.id.ll_shop_address);
        mTvShopAddress = view.findViewById(R.id.tv_shop_address);
        mLineDept = view.findViewById(R.id.line_dept);
        mEtDetailAddress = view.findViewById(R.id.et_detail_address);
        mFlDitu = view.findViewById(R.id.fl_ditu);
        mEtPhone = view.findViewById(R.id.et_phone);
        mEtSellerWx = view.findViewById(R.id.et_seller_wx);
        mTvBusinessStartTime= view.findViewById(R.id.tv_business_start_time);
        mTvBusinessEndTime= view.findViewById(R.id.tv_business_end_time);
        mLlAgreement = view.findViewById(R.id.ll_agreement);
        mEtDistriScope = view.findViewById(R.id.et_distri_scope);
        mEtManageScope = view.findViewById(R.id.et_manage_scope);
        mEtFeaturedServices = view.findViewById(R.id.et_featured_services);
        mTv01 = view.findViewById(R.id.tv_01);
        mEtMainDesc = view.findViewById(R.id.et_main_desc);
        mTvSave = view.findViewById(R.id.tv_save);
    }

    @Override
    public void initData() {
        super.initData();
        //初始化获取图片
        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(getActivity(), true, this);

        //设置配送范围水平滑动
        mEtDistriScope.setMovementMethod(ScrollingMovementMethod.getInstance());
        mEtDistriScope.setHorizontallyScrolling(true);

        showWaitDialog();
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                String cityData = AssetUtils.loadStringAsset(getContext(), "cityData.json");
                data = JSON.parseArray(cityData, CityListModel.class);
                dissWaitDailog();
            }
        });
        getPagerData();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        //缩略图
        mLlAddHorizontalPic.setOnClickListener(this);
        mIvDelete.setOnClickListener(this);
        //店铺自动上下线时间设置
        mTvStartTime.setOnClickListener(this);
        mTvEndTime.setOnClickListener(this);
        //药品价格 减 设置 加 点击事件
        mFlReducePrice.setOnClickListener(this);
        mEtNumPrice.setOnClickListener(this);
        mFlAddPrice.setOnClickListener(this);
        //选择地址
        mLlShopAddress.setOnClickListener(this);
        mFlDitu.setOnClickListener(this);
        //营业开始时间 跟 结束时间
        mTvBusinessStartTime.setOnClickListener(this);
        mTvBusinessEndTime.setOnClickListener(this);
        //物流配送管理
        mLlAgreement.setOnClickListener(this);
        mIvHorizontalPic.setOnClickListener(this);
        mTvSave.setOnClickListener(this);
    }

    //获取页面数据
    private void getPagerData() {
        StoreInfoModel.sendStoreInfoRequest(TAG, new CustomerJsonCallBack<StoreInfoModel>() {
            @Override
            public void onRequestError(StoreInfoModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(StoreInfoModel returnData) {
                dissWaitDailog();
                if (returnData.getData()!=null&&!returnData.getData().isEmpty()){
                    setPagerData(returnData.getData().get(0));
                }
            }
        });
    }

    //设置页面数据
    private void setPagerData(StoreInfoModel.DataBean data) {
        //设置缩略图
        if (!EmptyUtils.isEmpty(data.getStoreAccessoryUrl())) {
            GlideUtils.getImageWidHeig(getContext(), data.getStoreAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner01Success(data.getStoreAccessoryId(), data.getStoreAccessoryUrl(), width, height);
                }
            });
        }

        mEtShopName.setText(EmptyUtils.strEmpty(data.getStoreName()));
        mSbAutoOffOnLine.setCheckedImmediatelyNoEvent("1".equals(data.getIsAutomatic())?true:false);
        mTvStartTime.setText(EmptyUtils.strEmpty(data.getStartTime()));
        mTvEndTime.setText(EmptyUtils.strEmpty(data.getEndTime()));
        mSbIsShowSold.setCheckedImmediatelyNoEvent("1".equals(data.getIsShowSold())?true:false);
        mSbIsShowMonthlySales.setCheckedImmediatelyNoEvent("1".equals(data.getIsShowGoodsSold())?true:false);
        mSbIsShowRemainder.setCheckedImmediatelyNoEvent("1".equals(data.getIsShowMargin())?true:false);
        mEtNumPrice.setText(EmptyUtils.strEmpty(data.getStartingPrice()));
        mTvShopAddress.setText(EmptyUtils.strEmpty(data.getStoreAdress()));
        mEtDetailAddress.setText(EmptyUtils.strEmpty(data.getDetailedAddress()));
        mEtPhone.setText(EmptyUtils.strEmpty(data.getTelephone()));
        mEtSellerWx.setText(EmptyUtils.strEmpty(data.getWechat()));
        mTvBusinessStartTime.setText(EmptyUtils.strEmpty(data.getOpeningStartTime()));
        mTvBusinessEndTime.setText(EmptyUtils.strEmpty(data.getOpeningEndTime()));
        mEtDistriScope.setText(EmptyUtils.strEmpty(data.getDistributionScope()));
        mEtManageScope.setText(EmptyUtils.strEmpty(data.getBusinessScope()));
        mEtFeaturedServices.setText(EmptyUtils.strEmpty(data.getFeaturedServices()));
        mEtMainDesc.setText(EmptyUtils.strEmpty(data.getNotice()));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisibleToUser){
            getDistributionScope();
        }
    }

    //查询店铺配送范围
    private void getDistributionScope(){
        DistributionScopeModel.sendDistributionScopeRequest(TAG, new CustomerJsonCallBack<DistributionScopeModel>() {
            @Override
            public void onRequestError(DistributionScopeModel returnData, String msg) {
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(DistributionScopeModel returnData) {
                if (returnData.getData()!=null){
                    mEtDistriScope.setText(EmptyUtils.strEmpty(returnData.getData().getDistributionScope()));
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        if ("1".equals(Constants.AppStorage.APP_STORE_STATUS)) {//上线状态 不能修改店铺信息
            showShortToast("请修改店铺状态在操作!");
            return;
        }
        switch (v.getId()) {
            case R.id.ll_add_horizontal_pic://banner 1号位
                KeyboardUtils.hideSoftInput(getActivity());
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_BANNER_01, CompressUploadSinglePicUtils_zhyf.PIC_SHOP_INFO_THUMBNAIL_TAG);
                break;
            case R.id.iv_delete://banner 1号位 删除
                mLlAddHorizontalPic.setVisibility(View.VISIBLE);
                mLlAddHorizontalPic.setEnabled(true);

                mIvHorizontalPic.setTag(R.id.tag_1, "");
                mIvHorizontalPic.setTag(R.id.tag_2, "");
                mFlHorizontalPic.setVisibility(View.GONE);
                break;
            case R.id.iv_horizontal_pic://店铺缩略图
                showBigPic(mIvHorizontalPic,(String)mIvHorizontalPic.getTag(R.id.tag_2));
                break;
            case R.id.tv_start_time://开始时间
                KeyboardUtils.hideSoftInput(getActivity());
                if (wheelUtils==null){
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(getContext(), "店铺上线时间", 0,
                        CommonlyUsedDataUtils.getInstance().getAutoOpenOrCloseShopTime(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                mTvStartTime.setTag(dateInfo.getCode());
                                mTvStartTime.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                            }
                        });
                break;
            case R.id.tv_end_time://结束时间
                KeyboardUtils.hideSoftInput(getActivity());
                if (wheelUtils==null){
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(getContext(), "店铺下线时间", 0,
                        CommonlyUsedDataUtils.getInstance().getAutoOpenOrCloseShopTime(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                mTvEndTime.setTag(dateInfo.getCode());
                                mTvEndTime.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                            }
                        });
                break;
            case R.id.fl_reduce_price://药品价格 减
                reducePrice();
                break;
            case R.id.et_num_price://设置药品价格
                setPrice();
                break;
            case R.id.fl_add_price://药品价格 加
                addPrice();
                break;
            case R.id.ll_shop_address://选择地址
                KeyboardUtils.hideSoftInput(getActivity());
                selectRegion();
                break;
            case R.id.fl_ditu://获取定位详细地址
                onLbsLocation();
                break;
            case R.id.tv_business_start_time://营业开始时间
                KeyboardUtils.hideSoftInput(getActivity());
                if (wheelUtils==null){
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(getContext(), "营业开始时间", 0,
                        CommonlyUsedDataUtils.getInstance().getAutoOpenOrCloseShopTime(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                mTvBusinessStartTime.setTag(dateInfo.getCode());
                                mTvBusinessStartTime.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                            }
                        });
                break;
            case R.id.tv_business_end_time://营业结束时间
                KeyboardUtils.hideSoftInput(getActivity());
                if (wheelUtils==null){
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(getContext(), "营业结束时间", 0,
                        CommonlyUsedDataUtils.getInstance().getAutoOpenOrCloseShopTime(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                mTvBusinessEndTime.setTag(dateInfo.getCode());
                                mTvBusinessEndTime.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                            }
                        });
                break;
            case R.id.ll_agreement://物流配送管理
                LogisDeliveManageActivity.newIntance(getContext());
                break;
            case R.id.tv_save://保存
                onSave();
                break;
        }
    }

    //------------------------药品价格----------------------------------------
    //药品价格 减 药品价格和促销折扣不能为0，当小于1的时候点不了“减号”
    private void reducePrice() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mEtNumPrice.getText().toString())) {
            mEtNumPrice.setText("0");
        } else if (new BigDecimal(mEtNumPrice.getText().toString()).doubleValue() > 1) {
            mEtNumPrice.setText(new BigDecimal(mEtNumPrice.getText().toString()).subtract(new BigDecimal("1")).toPlainString());
        } else {
            showShortToast("药品价格不能为0");
            hideWaitDialog();
            return;
        }

        hideWaitDialog();
    }

    //设置药品价格
    private void setPrice() {
        if (updateNumPop == null) {
            updateNumPop = new UpdateNumPop(getContext());
        }
        updateNumPop.setOnListener(new UpdateNumPop.IOptionListener() {
            @Override
            public void onAgree(String data) {
                if (!RxTool.isPrice(data)) {
                    showShortToast("请输入正确的金额");
                    return;
                }

                mEtNumPrice.setText(data);
            }

            @Override
            public void onCancle() {
            }
        });
        updateNumPop.setFilters(2);
        updateNumPop.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
        updateNumPop.setContent(EmptyUtils.strEmptyToText(mEtNumPrice.getText().toString(), "0"));
        updateNumPop.setOutSideDismiss(false).showPopupWindow();
        KeyboardUtils.showSoftInput(mEtNumPrice);
    }

    //药品价格 加
    private void addPrice() {
        showWaitDialog();
        if (EmptyUtils.isEmpty(mEtNumPrice.getText().toString())) {
            mEtNumPrice.setText("0");
        } else {
            mEtNumPrice.setText(new BigDecimal(mEtNumPrice.getText().toString()).add(new BigDecimal("1")).toPlainString());
        }

        hideWaitDialog();
    }

    //------------------------药品价格----------------------------------------
    //------------------------省市区----------------------------------------
    //选择省市区
    private void selectRegion() {
        if (cityWheelUtils==null){
            cityWheelUtils = new CityWheelUtils();
        }
        cityWheelUtils.showCityWheel(getContext(), new CityWheelUtils.CityWheelClickListener<CityListModel, CityListModel.CityBean, CityListModel.CityBean.AreaBean>() {
            @Override
            public void onCity(CityListModel provinceData) {
                getCityData(provinceData.getCode());
            }

            @Override
            public void onArea(CityListModel.CityBean cityData) {
                getAreaData(cityData.getCode());
            }

            @Override
            public void onchooseCity(int provincePos, CityListModel provinceItemData,
                                     int cityPos, CityListModel.CityBean cityItemData,
                                     int areaPos, CityListModel.CityBean.AreaBean areaItemData) {
//                province=provinceItemData.getName();//省
//                city=cityItemData.getName();//市
//                area=areaItemData.getName();//区
                mTvShopAddress.setText(provinceItemData.getName()+cityItemData.getName()+ areaItemData.getName());
            }
        });
        cityWheelUtils.setProvinceData(data);
        getCityData(data.get(0).getCode());
    }

    //获取市级数据
    private void getCityData(String provinceId) {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                for (CityListModel datum : data) {
                    if (provinceId.equals(datum.getCode())) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                cityWheelUtils.setCityData(datum.getCity() == null ? new ArrayList() : datum.getCity());
                            }
                        });

                        if (datum.getCity() != null && datum.getCity().get(0) != null) {
                            getAreaData(datum.getCity().get(0).getCode());
                        }
                        break;
                    }
                }
            }
        });
    }

    //获取区级数据
    private void getAreaData(String cityId) {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                for (CityListModel datum : data) {
                    if (datum != null && datum.getCity() != null) {
                        for (CityListModel.CityBean cityBean : datum.getCity()) {
                            if (cityId.equals(cityBean.getCode())) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cityWheelUtils.setAreaData(cityBean.getArea());
                                    }
                                });
                                break;
                            }
                        }
                    }
                }
            }
        });
    }
    //------------------------省市区----------------------------------------
    //------------------------获取定位详细地址----------------------------------------
    //获取定位详细地址
    private void onLbsLocation() {
        LocationTool.getInstance().registerLocation(this, new ILocationListener() {
                    @Override
                    public void onLocationSuccess(CustomLocationBean customLocationBean) {
                        hideWaitDialog();
                        mEtDetailAddress.setText((customLocationBean.getDetail_address().contains(customLocationBean.getStreet())?"":customLocationBean.getStreet())+customLocationBean.getDetail_address());
                    }

                    @Override
                    public void onLocationError(String errorText) {
                        hideWaitDialog();
                        showTipDialog(errorText);
                    }
                }
        );
    }
    //------------------------获取定位详细地址----------------------------------------
    //保存
    private void onSave() {
        if (null == mIvHorizontalPic.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvHorizontalPic.getTag(R.id.tag_2)))) {
            showShortToast("请上传缩略图");
            return;
        }
        if (EmptyUtils.isEmpty(mEtShopName.getText().toString())) {
            showShortToast("请输入店铺名称，4~20个字");
            return;
        }
        //开启每天店铺自动上线与下线时 需要验证时间是否有选择 以及 下线时间必须晚于上线时间
        if (mSbAutoOffOnLine.isChecked()){
            if (EmptyUtils.isEmpty(mTvStartTime.getText().toString())) {
                showShortToast("请选择上线时间");
                return;
            }
            if (EmptyUtils.isEmpty(mTvEndTime.getText().toString())) {
                showShortToast("请选择下线时间");
                return;
            }
            //时间怎么判断早晚  按24小时制比对大小
            Integer startTime = Integer.valueOf(mTvStartTime.getText().toString().replace(":", ""));
            Integer endTime=Integer.valueOf(mTvEndTime.getText().toString().replace(":", ""));
            if (startTime>=endTime){
                showShortToast("下线时间必须晚于上线时间");
                return;
            }
        }

        if ("0".equals(mEtNumPrice.getText().toString())){
            showShortToast("药品起送价不能为0");
            return;
        }

        if (EmptyUtils.isEmpty(mTvShopAddress.getText().toString())) {
            showShortToast("请选择店铺地址");
            return;
        }
        if (EmptyUtils.isEmpty(mEtDetailAddress.getText().toString())) {
            showShortToast("请输入店铺详细地址");
            return;
        }
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString())) {
            showShortToast("请输入店铺联系电话");
            return;
        }
        if (!"1".equals(String.valueOf(mEtPhone.getText().charAt(0))) || mEtPhone.getText().length() != 11) {
            showShortToast("请输入正确的手机号码");
            return;
        }
        if (EmptyUtils.isEmpty(mEtSellerWx.getText().toString())) {
            showShortToast("请输入店铺微信号");
            return;
        }
        if (EmptyUtils.isEmpty(mTvBusinessStartTime.getText().toString())) {
            showShortToast("请选择营业开始时间");
            return;
        }
        if (EmptyUtils.isEmpty(mTvBusinessEndTime.getText().toString())) {
            showShortToast("请选择营业结束时间");
            return;
        }
        //时间怎么判断早晚  按24小时制比对大小
        Integer businessStartTime = Integer.valueOf(mTvBusinessStartTime.getText().toString().replace(":", ""));
        Integer businessEndTime=Integer.valueOf(mTvBusinessEndTime.getText().toString().replace(":", ""));
        if (businessStartTime>=businessEndTime){
            showShortToast("营业结束时间必须晚于营业开始时间");
            return;
        }

        showWaitDialog();
        BaseModel.sendSaveStoreInfoRequest(TAG, mIvHorizontalPic.getTag(R.id.tag_1), mEtShopName.getText().toString(), mSbAutoOffOnLine.isChecked() ? "1" : "2",
                mTvStartTime.getText().toString(), mTvEndTime.getText().toString(), mSbIsShowSold.isChecked() ? "1" : "2", mSbIsShowMonthlySales.isChecked() ? "1" : "2",
                mSbIsShowRemainder.isChecked() ? "1" : "2", mEtNumPrice.getText().toString(), mTvShopAddress.getText().toString(), mEtDetailAddress.getText().toString(),
                mEtPhone.getText().toString(), mEtSellerWx.getText().toString(), mTvBusinessStartTime.getText().toString(), mTvBusinessEndTime.getText().toString(),
                mEtDistriScope.getText().toString(), mEtManageScope.getText().toString(), mEtFeaturedServices.getText().toString(), mEtMainDesc.getText().toString(),
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("保存成功");
                        goFinish();
                    }
                });
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String mTag, String accessoryId, String url, int width, int height) {
        switch (mTag) {
            case TAG_SELECT_BANNER_01://banner 1号位
                selectBanner01Success(accessoryId, url, width, height);
                break;
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }

    //banner 1号位
    private void selectBanner01Success(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic.getLayoutParams();
            if (radio >= 1.25) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_300));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_300))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_240) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_240));
            }

            mIvHorizontalPic.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic);

            mLlAddHorizontalPic.setVisibility(View.GONE);
            mLlAddHorizontalPic.setEnabled(false);

            mIvHorizontalPic.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic.setTag(R.id.tag_2, url);
            mFlHorizontalPic.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //--------------------------选择图片----------------------------------------------------

    //展示大图
    private void showBigPic(ImageView imageView,String imageUrl) {
        OpenImage.with(getContext())
                //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                .setClickImageView(imageView)
                //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                //RecyclerView的数据
                .setImageUrl(imageUrl, MediaType.IMAGE)
                //点击的ImageView所在数据的位置
                .setClickPosition(0)
                //开始展示大图
                .show();
    }

    //隐藏加载框
    public void dissWaitDailog() {
        requestTag++;
        if (requestTag >= 2) {
            hideWaitDialog();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }

        if (wheelUtils != null) {
            wheelUtils.dissWheel();
        }

        if (updateNumPop != null) {
            updateNumPop.onCleanListener();
            updateNumPop.onDestroy();
        }
        if (data!=null){
            data.clear();
            data=null;
        }
        if (cityWheelUtils != null) {
            cityWheelUtils.onCleanData();
        }

        LocationTool.getInstance().unRegisterLocation();
    }

    public static ShopInfoFragment newIntance() {
        ShopInfoFragment shopInfoFragment = new ShopInfoFragment();
        return shopInfoFragment;
    }

}
