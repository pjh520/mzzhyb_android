package com.jrdz.zhyb_android.ui.zhyf_manage.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.DraggableController;
import com.chad.library.adapter.base.callback.ItemDragAndSwipeCallback;
import com.chad.library.adapter.base.listener.OnItemDragListener;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.VibratorUtils;
import com.frame.compiler.widget.customPop.CustomerBottomListPop;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.AddDirActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.LookGoodsActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.PhaSortSpecialAreaActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.ShopDecoActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.UpdateDirActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.ShopDecoSortAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.ShopStylePhaSortAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.AddDirModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.ShopStyleModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.StoreStatusModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;
import com.jrdz.zhyb_android.widget.ShapeCustomeRecyclerView;
import com.jrdz.zhyb_android.widget.pop.AddNavigationPop;

import java.math.BigDecimal;
import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-16
 * 描    述：店铺风格
 * ================================================
 */
public class ShopStyleFragment extends BaseFragment implements CompressUploadSinglePicUtils_zhyf.IChoosePic, CustomerBottomListPop.IBottomChooseListener<StoreStatusModel>, AddNavigationPop.IOptionListener {
    public static final int SORT_NOR = 1;//分类不可修改的固定项
    public static final int SORT_NEWADD = 2;//分类可修改的新增项
    public static final int SORT_ADDBTN = 3;//分类的添加按钮
    public final String TAG_SELECT_BANNER_01 = "1";//banner 1号位
    public final String TAG_SELECT_BANNER_02 = "2";//banner 2号位
    public final String TAG_SELECT_BANNER_03 = "3";//banner 3号位

    private RelativeLayout mLlHorizontalPic;
    private TextView mTv01;
    private ShapeFrameLayout mSflHorizontalPic01;
    private LinearLayout mLlAddHorizontalPic01;
    private FrameLayout mFlHorizontalPic01;
    private ImageView mIvHorizontalPic01;
    private ImageView mIvDelete01;
    private TextView mTvBanner01;
    private ShapeFrameLayout mSflHorizontalPic02;
    private LinearLayout mLlAddHorizontalPic02;
    private FrameLayout mFlHorizontalPic02;
    private ImageView mIvHorizontalPic02;
    private ImageView mIvDelete02;
    private TextView mTvBanner02;
    private ShapeFrameLayout mSflHorizontalPic03;
    private LinearLayout mLlAddHorizontalPic03;
    private FrameLayout mFlHorizontalPic03;
    private ImageView mIvHorizontalPic03;
    private ImageView mIvDelete03;
    private TextView mTvBanner03;
    private ShapeLinearLayout mSllBannerTime;
    private TextView mTvBannerTime;
    private ShapeCustomeRecyclerView mSrlPhaSort;
    private ShapeCustomeRecyclerView mSrlSort;
    private ShapeTextView mTvSave;

    private ShopStylePhaSortAdapter shopStylePhaSortAdapter;//首页药品分类适配器
    private ItemTouchHelper itemTouchHelper;
    private ShopDecoSortAdapter shopDecoSortAdapter;//首页分类适配器
    private DraggableController mDraggableController;
    private ItemTouchHelper mItemTouchHelper02;
    private CompressUploadSinglePicUtils_zhyf compressUploadSinglePicUtils;
    private CustomerBottomListPop customerBottomListPop; //选择弹框
    private AddNavigationPop addNavigationPop;//新增导航 或者更新导航弹框

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    //两列表拖拽监听
    OnItemDragListener listener = new OnItemDragListener() {
        @Override
        public void onItemDragStart(RecyclerView.ViewHolder viewHolder, int pos) {
            Log.d(TAG, "drag start");
            BaseViewHolder holder = ((BaseViewHolder) viewHolder);
            //holder.setTextColor(R.id.layout_city_item, Color.WHITE);
            VibratorUtils.getInstance().vibrator(getContext(), 10);
        }

        @Override
        public void onItemDragMoving(RecyclerView.ViewHolder source, int from, RecyclerView.ViewHolder target, int to) {
            Log.d(TAG, "move from: " + source.getAdapterPosition() + " to: " + target.getAdapterPosition());
        }

        @Override
        public void onItemDragEnd(RecyclerView.ViewHolder viewHolder, int pos) {
            Log.d(TAG, "drag end");
            BaseViewHolder holder = ((BaseViewHolder) viewHolder);
//                holder.setTextColor(R.id.layout_city_item, Color.BLACK);
        }
    };

    //药品目录修改监听
    private ObserverWrapper<PhaSortModel.DataBean> mOnHomePhaSortObserve = new ObserverWrapper<PhaSortModel.DataBean>() {
        @Override
        public void onChanged(@Nullable PhaSortModel.DataBean value) {
            if (null != value) {
                //1代表添加目录 2代表修改目录 3代表删除目录
                switch (value.getSelfTag()) {
                    case 1://添加目录
                        int size = shopStylePhaSortAdapter.getData().size();
                        if (size == 12) {
                            shopStylePhaSortAdapter.setData(size - 1, value);
                        } else {
                            shopStylePhaSortAdapter.addData(size - 1, value);
                            shopStylePhaSortAdapter.loadMoreComplete();
                        }
                        break;
                    case 2://修改目录
                        shopStylePhaSortAdapter.getData().set(value.getPos(), value);
                        shopStylePhaSortAdapter.notifyDataSetChanged();
                        break;
                    case 3://删除目录
                        List<PhaSortModel.DataBean> datas = shopStylePhaSortAdapter.getData();
                        datas.remove(value.getPos());
                        if (!"3".equals(datas.get(datas.size() - 1).getIsSystem())) {
                            datas.add(new PhaSortModel.DataBean("3", "添加"));
                        }

                        shopStylePhaSortAdapter.notifyDataSetChanged();
                        break;
                }
            }
        }
    };

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_shop_style;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mLlHorizontalPic = view.findViewById(R.id.ll_horizontal_pic);
        mTv01 = view.findViewById(R.id.tv_01);
        mSflHorizontalPic01 = view.findViewById(R.id.sfl_horizontal_pic01);
        mLlAddHorizontalPic01 = view.findViewById(R.id.ll_add_horizontal_pic01);
        mFlHorizontalPic01 = view.findViewById(R.id.fl_horizontal_pic01);
        mIvHorizontalPic01 = view.findViewById(R.id.iv_horizontal_pic01);
        mIvDelete01 = view.findViewById(R.id.iv_delete01);
        mTvBanner01 = view.findViewById(R.id.tv_banner01);
        mSflHorizontalPic02 = view.findViewById(R.id.sfl_horizontal_pic02);
        mLlAddHorizontalPic02 = view.findViewById(R.id.ll_add_horizontal_pic02);
        mFlHorizontalPic02 = view.findViewById(R.id.fl_horizontal_pic02);
        mIvHorizontalPic02 = view.findViewById(R.id.iv_horizontal_pic02);
        mIvDelete02 = view.findViewById(R.id.iv_delete02);
        mTvBanner02 = view.findViewById(R.id.tv_banner02);
        mSflHorizontalPic03 = view.findViewById(R.id.sfl_horizontal_pic03);
        mLlAddHorizontalPic03 = view.findViewById(R.id.ll_add_horizontal_pic03);
        mFlHorizontalPic03 = view.findViewById(R.id.fl_horizontal_pic03);
        mIvHorizontalPic03 = view.findViewById(R.id.iv_horizontal_pic03);
        mIvDelete03 = view.findViewById(R.id.iv_delete03);
        mTvBanner03 = view.findViewById(R.id.tv_banner03);
        mSllBannerTime = view.findViewById(R.id.sll_banner_time);
        mTvBannerTime = view.findViewById(R.id.tv_banner_time);
        mSrlPhaSort = view.findViewById(R.id.srl_pha_sort);
        mSrlSort = view.findViewById(R.id.srl_sort);
        mTvSave = view.findViewById(R.id.tv_save);
    }

    @Override
    public void initData() {
        super.initData();
        MsgBus.sendHomePhaSortRefresh().observeForever(mOnHomePhaSortObserve);
        //初始化获取图片
        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(getActivity(), true, this);
        //模拟首页药品分类
        mSrlPhaSort.setHasFixedSize(true);
        mSrlPhaSort.setLayoutManager(new GridLayoutManager(getContext(), 4, GridLayoutManager.VERTICAL, false));
        shopStylePhaSortAdapter = new ShopStylePhaSortAdapter();
        //设置拖拽属性
        ItemDragAndSwipeCallback mItemDragAndSwipeCallback = new ItemDragAndSwipeCallback(shopStylePhaSortAdapter) {
            @Override
            public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                String isSystem = shopStylePhaSortAdapter.getItem(viewHolder.getAbsoluteAdapterPosition()).getIsSystem();
                if (String.valueOf(SORT_NOR).equals(isSystem)||String.valueOf(SORT_ADDBTN).equals(isSystem)) {
                    // 不可拖动，不可滑动
                    return makeMovementFlags(0, 0);
                }
                return super.getMovementFlags(recyclerView, viewHolder);
            }

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder source, @NonNull RecyclerView.ViewHolder target) {
                // 不可拖动其他 Item 与 target Item 交换位置
                String isSystem = shopStylePhaSortAdapter.getItem(target.getAbsoluteAdapterPosition()).getIsSystem();
                if (String.valueOf(SORT_NOR).equals(isSystem)||String.valueOf(SORT_ADDBTN).equals(isSystem)) {
                    return false;
                }
                return super.onMove(recyclerView, source, target);
            }
        };
        itemTouchHelper = new ItemTouchHelper(mItemDragAndSwipeCallback);
        itemTouchHelper.attachToRecyclerView(mSrlPhaSort);
        shopStylePhaSortAdapter.setOnItemDragListener(listener);
        mSrlPhaSort.setAdapter(shopStylePhaSortAdapter);

        //分类
        mSrlSort.setHasFixedSize(true);
        mSrlSort.setLayoutManager(new GridLayoutManager(getContext(), 4, GridLayoutManager.VERTICAL, false));
        shopDecoSortAdapter = new ShopDecoSortAdapter((ShopDecoActivity) getActivity());
        //获取拖拽控制器
        mDraggableController = shopDecoSortAdapter.getDraggableController();
        ItemDragAndSwipeCallback mItemDragAndSwipeCallback02 = new ItemDragAndSwipeCallback(mDraggableController) {
            @Override
            public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                int itemType = shopDecoSortAdapter.getItem(viewHolder.getAbsoluteAdapterPosition()).getItemType();

                if (SORT_ADDBTN == itemType||SORT_NOR == itemType) {
                    // 不可拖动，不可滑动
                    return makeMovementFlags(0, 0);
                }
                return super.getMovementFlags(recyclerView, viewHolder);
            }

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder source, @NonNull RecyclerView.ViewHolder target) {
                // 不可拖动其他 Item 与 target Item 交换位置
                int itemTypeTarget = shopDecoSortAdapter.getItem(target.getAbsoluteAdapterPosition()).getItemType();
                if (SORT_ADDBTN == itemTypeTarget||SORT_NOR == itemTypeTarget) {
                    return false;
                } else {
                    return true;
                }
            }
        };
        mItemTouchHelper02 = new ItemTouchHelper(mItemDragAndSwipeCallback02);
        mItemTouchHelper02.attachToRecyclerView(mSrlSort);
        mDraggableController.setOnItemDragListener(listener);
        mSrlSort.setAdapter(shopDecoSortAdapter);

        if ("1".equals(Constants.AppStorage.APP_STORE_STATUS)) {//上线状态 不能修改店铺信息
            //关闭拖拽
            shopStylePhaSortAdapter.disableDragItem();
            mDraggableController.disableDragItem();
        } else {
            //开启拖拽
            shopStylePhaSortAdapter.enableDragItem(itemTouchHelper);
            mDraggableController.enableDragItem(mItemTouchHelper02);
        }

        showWaitDialog();
        getPagerData();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mLlAddHorizontalPic01.setOnClickListener(this);
        mIvDelete01.setOnClickListener(this);
        mLlAddHorizontalPic02.setOnClickListener(this);
        mIvDelete02.setOnClickListener(this);
        mLlAddHorizontalPic03.setOnClickListener(this);
        mIvDelete03.setOnClickListener(this);

        mSllBannerTime.setOnClickListener(this);
        mIvHorizontalPic01.setOnClickListener(this);
        mIvHorizontalPic02.setOnClickListener(this);
        mIvHorizontalPic03.setOnClickListener(this);
        //药品分类 item点击事件
        shopStylePhaSortAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if ("1".equals(Constants.AppStorage.APP_STORE_STATUS)) {//上线状态 不能修改店铺信息
                    showShortToast("请修改店铺状态在操作!");
                    return;
                }
                PhaSortModel.DataBean itemData = shopStylePhaSortAdapter.getItem(i);

                if ("3".equals(itemData.getIsSystem())) {//进入添加目录页面
                    AddDirActivity.newIntance(getContext(),2);
                } else if ("2".equals(itemData.getIsSystem())) {//进入编辑目录页面
                    UpdateDirActivity.newIntance(getContext(), i, itemData);
                } else {
                    // 2022-09-30 点击item 进入药品专区
                    PhaSortSpecialAreaActivity.newIntance(getContext(), itemData.getCatalogueId(), itemData.getCatalogueName());
                }
            }
        });
        //分类 item点击事件
        shopDecoSortAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if ("1".equals(Constants.AppStorage.APP_STORE_STATUS)) {//上线状态 不能修改店铺信息
                    showShortToast("请修改店铺状态在操作!");
                    return;
                }
                ShopStyleModel.DataBean.ClassificationBean itemData = shopDecoSortAdapter.getItem(i);
                if (SORT_ADDBTN == itemData.getItemType()) {//进入添加目录页面
                    addNavigation();
                } else if (SORT_NEWADD == itemData.getItemType()) {
                    // 2022-09-30 点击item 进入修改导航名
                    updateNavigation(itemData);
                }
            }
        });
        mTvSave.setOnClickListener(this);
    }

    //新增导航
    private void addNavigation() {
        if (addNavigationPop == null) {
            addNavigationPop = new AddNavigationPop(getContext(), null, "1");
            addNavigationPop.setOnListener(this);
        }
        addNavigationPop.setPopData("1");
        addNavigationPop.cleanData();
        addNavigationPop.setOutSideDismiss(true).showPopupWindow();
    }

    //更新导航
    private void updateNavigation(ShopStyleModel.DataBean.ClassificationBean itemData) {
        if (addNavigationPop == null) {
            addNavigationPop = new AddNavigationPop(getContext(), itemData, "2");
            addNavigationPop.setOnListener(this);
        }
        addNavigationPop.setData(itemData);
        addNavigationPop.setPopData("2");
        addNavigationPop.setOutSideDismiss(true).showPopupWindow();
    }

    //获取页面数据
    private void getPagerData() {
        ShopStyleModel.sendShopStyleRequest(TAG, new CustomerJsonCallBack<ShopStyleModel>() {
            @Override
            public void onRequestError(ShopStyleModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(ShopStyleModel returnData) {
                hideWaitDialog();
                ShopStyleModel.DataBean data = returnData.getData();
                if (data != null) {
                    setPagerData(data);
                }
            }
        });
    }

    //设置页面数据
    private void setPagerData(ShopStyleModel.DataBean data) {
        //设置首页轮播图数据
        //选择banner01
        if (!EmptyUtils.isEmpty(data.getBannerAccessoryUrl1())) {
            GlideUtils.getImageWidHeig(getContext(), data.getBannerAccessoryUrl1(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner01Success(data.getBannerAccessoryId1(), data.getBannerAccessoryUrl1(), width, height);
                }
            });
        }
        //选择banner02
        if (!EmptyUtils.isEmpty(data.getBannerAccessoryUrl2())) {
            GlideUtils.getImageWidHeig(getContext(), data.getBannerAccessoryUrl2(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner02Success(data.getBannerAccessoryId2(), data.getBannerAccessoryUrl2(), width, height);
                }
            });
        }
        //选择banner03
        if (!EmptyUtils.isEmpty(data.getBannerAccessoryUrl3())) {
            GlideUtils.getImageWidHeig(getContext(), data.getBannerAccessoryUrl3(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    selectBanner03Success(data.getBannerAccessoryId3(), data.getBannerAccessoryUrl3(), width, height);
                }
            });
        }
        //设置轮播间隔时间
        mTvBannerTime.setTag(data.getIntervaltime()==0?"3":data.getIntervaltime());
        mTvBannerTime.setText(data.getIntervaltime()==0?"3S":(data.getIntervaltime()+"S"));
        //设置首页分类数据
        //尾部添加按钮
        if (data.getHomePage().size() < 12) {
            data.getHomePage().add(new PhaSortModel.DataBean("3", "添加"));
        }
        if (shopStylePhaSortAdapter!=null){
            shopStylePhaSortAdapter.setNewData(data.getHomePage());
        }
        //设置分类数据
        //尾部添加按钮
        if (data.getClassification().size() < 32) {
            data.getClassification().add(new ShopStyleModel.DataBean.ClassificationBean("-1001", "添加", SORT_ADDBTN));
        }
        if (shopDecoSortAdapter!=null){
            shopDecoSortAdapter.setNewData(data.getClassification());
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        if ("1".equals(Constants.AppStorage.APP_STORE_STATUS)) {//上线状态 不能修改店铺信息
            showShortToast("请修改店铺状态在操作!");
            return;
        }
        switch (v.getId()) {
            case R.id.ll_add_horizontal_pic01://banner 1号位
                KeyboardUtils.hideSoftInput(getActivity());
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_BANNER_01, CompressUploadSinglePicUtils_zhyf.PIC_BANNER_01_TAG);
                break;
            case R.id.iv_delete01://banner 1号位 删除
                mLlAddHorizontalPic01.setVisibility(View.VISIBLE);
                mLlAddHorizontalPic01.setEnabled(true);

                mIvHorizontalPic01.setTag(R.id.tag_1, "");
                mIvHorizontalPic01.setTag(R.id.tag_2, "");
                mFlHorizontalPic01.setVisibility(View.GONE);
                break;
            case R.id.ll_add_horizontal_pic02://banner 2号位
                KeyboardUtils.hideSoftInput(getActivity());
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_BANNER_02, CompressUploadSinglePicUtils_zhyf.PIC_BANNER_02_TAG);
                break;
            case R.id.iv_delete02://banner 2号位 删除
                mLlAddHorizontalPic02.setVisibility(View.VISIBLE);
                mLlAddHorizontalPic02.setEnabled(true);

                mIvHorizontalPic02.setTag(R.id.tag_1, "");
                mIvHorizontalPic02.setTag(R.id.tag_2, "");
                mFlHorizontalPic02.setVisibility(View.GONE);
                break;
            case R.id.ll_add_horizontal_pic03://banner 3号位
                KeyboardUtils.hideSoftInput(getActivity());
                compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_BANNER_03, CompressUploadSinglePicUtils_zhyf.PIC_BANNER_03_TAG);
                break;
            case R.id.iv_delete03://banner 3号位 删除
                mLlAddHorizontalPic03.setVisibility(View.VISIBLE);
                mLlAddHorizontalPic03.setEnabled(true);

                mIvHorizontalPic03.setTag(R.id.tag_1, "");
                mIvHorizontalPic03.setTag(R.id.tag_2, "");
                mFlHorizontalPic03.setVisibility(View.GONE);
                break;
            case R.id.sll_banner_time://选择轮播间隔时间
                KeyboardUtils.hideSoftInput(getActivity());
                if (customerBottomListPop == null) {
                    customerBottomListPop = new CustomerBottomListPop(getContext(), ShopStyleFragment.this);
                }
                customerBottomListPop.setDatas("1", "", mTvBannerTime.getText().toString(), CommonlyUsedDataUtils.getInstance().getBannerTimeData());
                customerBottomListPop.showPopupWindow();
                break;
            case R.id.iv_horizontal_pic01://banner 1号位
                showBigPic(mIvHorizontalPic01,(String)mIvHorizontalPic01.getTag(R.id.tag_2));
                break;
            case R.id.iv_horizontal_pic02://banner 2号位
                showBigPic(mIvHorizontalPic02,(String)mIvHorizontalPic02.getTag(R.id.tag_2));
                break;
            case R.id.iv_horizontal_pic03://banner 3号位
                showBigPic(mIvHorizontalPic03,(String)mIvHorizontalPic03.getTag(R.id.tag_2));
                break;
            case R.id.tv_save://保存
                onSave();
                break;
        }
    }

    @Override
    public void onItemClick(String tag, StoreStatusModel str) {
        switch (tag) {
            case "1"://选择轮播间隔时间
                mTvBannerTime.setTag(str.getId());
                mTvBannerTime.setText(EmptyUtils.strEmpty(str.getText()));
                break;
        }
    }

    @Override
    public void onAdd(String etContent) {
        // 2022-11-15 请求接口 上传分类信息
        showWaitDialog();
        AddDirModel.sendAddCataRequest(TAG, etContent, null, null,
                "2", "1", new CustomerJsonCallBack<AddDirModel>() {
                    @Override
                    public void onRequestError(AddDirModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(AddDirModel returnData) {
                        hideWaitDialog();
                        PhaSortModel.DataBean data = returnData.getData();
                        if (data != null) {
                            //上传分类成功
                            ShopStyleModel.DataBean.ClassificationBean classificationBean = new ShopStyleModel.DataBean.ClassificationBean(
                                    data.getCatalogueId(), data.getCatalogueName(), SORT_NEWADD);
                            int size = shopDecoSortAdapter.getData().size();
                            if (size == 32) {
                                shopDecoSortAdapter.getData().set(size - 1, classificationBean);
                            } else {
                                shopDecoSortAdapter.getData().add(size - 1, classificationBean);
                            }

                            shopDecoSortAdapter.notifyDataSetChanged();
                            MsgBus.sendShopDecoSortRefresh().post("1");
                        }
                    }
                });
    }

    @Override
    public void onCancle() {}

    @Override
    public void onDelete(String catalogueId) {
        showWaitDialog();
        BaseModel.sendDelCataRequest(TAG, catalogueId, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("删除成功");
                //删除目录成功
                List<ShopStyleModel.DataBean.ClassificationBean> datas = shopDecoSortAdapter.getData();
                for (int i = 0, size = datas.size(); i < size; i++) {
                    ShopStyleModel.DataBean.ClassificationBean itemData = datas.get(i);
                    if (catalogueId.equals(itemData.getCatalogueId())) {
                        datas.remove(i);
                        break;
                    }
                }

                if (SORT_ADDBTN != datas.get(datas.size() - 1).getItemType()) {
                    datas.add(new ShopStyleModel.DataBean.ClassificationBean("-1001", "", SORT_ADDBTN));
                }

                shopDecoSortAdapter.notifyDataSetChanged();

                MsgBus.sendShopDecoSortRefresh().post("3");
            }
        });
    }

    @Override
    public void onUpdate(String catalogueId, String etContent) {
        // 2022-11-15 请求接口 修改目录信息
        showWaitDialog();
        AddDirModel.sendUpdateCataRequest(TAG, catalogueId, etContent, null, null, "2", "1",
                new CustomerJsonCallBack<AddDirModel>() {
                    @Override
                    public void onRequestError(AddDirModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(AddDirModel returnData) {
                        hideWaitDialog();
                        PhaSortModel.DataBean data = returnData.getData();
                        if (data != null) {
                            ShopStyleModel.DataBean.ClassificationBean classificationBean = new ShopStyleModel.DataBean.ClassificationBean(
                                    data.getCatalogueId(), data.getCatalogueName(), SORT_NEWADD);

                            List<ShopStyleModel.DataBean.ClassificationBean> datas = shopDecoSortAdapter.getData();
                            for (int i = 0, size = datas.size(); i < size; i++) {
                                ShopStyleModel.DataBean.ClassificationBean itemData = datas.get(i);
                                if (catalogueId.equals(itemData.getCatalogueId())) {
                                    datas.set(i, classificationBean);
                                    shopDecoSortAdapter.notifyItemChanged(i);
                                    break;
                                }
                            }

                            MsgBus.sendShopDecoSortRefresh().post("2");
                        }
                    }
                });
    }

    //展示大图
    private void showBigPic(ImageView imageView,String imageUrl) {
        OpenImage.with(getContext())
                //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                .setClickImageView(imageView)
                //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                //RecyclerView的数据
                .setImageUrl(imageUrl, MediaType.IMAGE)
                //点击的ImageView所在数据的位置
                .setClickPosition(0)
                //开始展示大图
                .show();
    }

    //保存
    private void onSave() {
        if (null == mIvHorizontalPic01.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvHorizontalPic01.getTag(R.id.tag_2)))) {
            showShortToast("请上传banner 1号位");
            return;
        }
        if (null == mIvHorizontalPic02.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvHorizontalPic02.getTag(R.id.tag_2)))) {
            showShortToast("请上传banner 2号位");
            return;
        }
        if (null == mIvHorizontalPic03.getTag(R.id.tag_2) || EmptyUtils.isEmpty(String.valueOf(mIvHorizontalPic03.getTag(R.id.tag_2)))) {
            showShortToast("请上传banner 3号位");
            return;
        }

        if (EmptyUtils.isEmpty(mTvBannerTime.getText().toString())) {
            showShortToast("请选择轮播间隔时间");
            return;
        }

        //2022-10-08 请求接口 保存成功 返回上一页
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("BannerAccessoryId1", null==mIvHorizontalPic01.getTag(R.id.tag_1)?"":String.valueOf(mIvHorizontalPic01.getTag(R.id.tag_1)));//banner附件标识1
        jsonObject.put("BannerAccessoryId2", null==mIvHorizontalPic02.getTag(R.id.tag_1)?"":String.valueOf(mIvHorizontalPic02.getTag(R.id.tag_1)));//banner附件标识2
        jsonObject.put("BannerAccessoryId3", null==mIvHorizontalPic03.getTag(R.id.tag_1)?"":String.valueOf(mIvHorizontalPic03.getTag(R.id.tag_1)));//banner附件标识3
        jsonObject.put("Intervaltime", null==mTvBannerTime.getTag()?"":String.valueOf(mTvBannerTime.getTag()));//轮播间隔时间
        //首页分类
        JSONArray jsonArray01 = new JSONArray();
        List<PhaSortModel.DataBean> phaSortDatas = shopStylePhaSortAdapter.getData();
        int phaSortSize=phaSortDatas.size();
        if ("3".equals(phaSortDatas.get(phaSortDatas.size() - 1).getIsSystem())) {
            phaSortSize-=1;
        }
        for (int i = 0; i < phaSortSize; i++) {
            PhaSortModel.DataBean phaSortData = phaSortDatas.get(i);
            JSONObject jsonObjectChild = new JSONObject();
            jsonObjectChild.put("CatalogueId", EmptyUtils.strEmpty(phaSortData.getCatalogueId()));
            jsonObjectChild.put("CatalogueName", EmptyUtils.strEmpty(phaSortData.getCatalogueName()));

            jsonArray01.add(jsonObjectChild);
        }
        //分类
        JSONArray jsonArray02 = new JSONArray();
        List<ShopStyleModel.DataBean.ClassificationBean> sortDatas = shopDecoSortAdapter.getData();
        int sortSize=sortDatas.size();
        if (SORT_ADDBTN==sortDatas.get(sortDatas.size() - 1).getIsSystem()) {
            sortSize-=1;
        }
        for (int i = 0; i < sortSize; i++) {
            ShopStyleModel.DataBean.ClassificationBean sortData = sortDatas.get(i);
            JSONObject jsonObjectChild = new JSONObject();
            jsonObjectChild.put("CatalogueId", EmptyUtils.strEmpty(sortData.getCatalogueId()));
            jsonObjectChild.put("CatalogueName", EmptyUtils.strEmpty(sortData.getCatalogueName()));

            jsonArray02.add(jsonObjectChild);
        }

        jsonObject.put("HomePage", jsonArray01);//首页分类列表
        jsonObject.put("Classification", jsonArray02);//分类列表

        showWaitDialog();
        BaseModel.sendSaveStoreStyleRequest(TAG, jsonObject.toJSONString(), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("保存成功");
                goFinish();
            }
        });
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String mTag, String accessoryId, String url, int width, int height) {
        switch (mTag) {
            case TAG_SELECT_BANNER_01://banner 1号位
                selectBanner01Success(accessoryId, url, width, height);
                break;
            case TAG_SELECT_BANNER_02://banner 2号位
                selectBanner02Success(accessoryId, url, width, height);
                break;
            case TAG_SELECT_BANNER_03://banner 3号位
                selectBanner03Success(accessoryId, url, width, height);
                break;
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }

    //banner 1号位
    private void selectBanner01Success(String accessoryId, String url, int width, int height) {
        if (mIvHorizontalPic01!=null&&!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic01.getLayoutParams();
            if (radio >= 2.025) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_486));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_486))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_240) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_240));
            }

            mIvHorizontalPic01.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic01);

            mLlAddHorizontalPic01.setVisibility(View.GONE);
            mLlAddHorizontalPic01.setEnabled(false);

            mIvHorizontalPic01.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic01.setTag(R.id.tag_2, url);
            mFlHorizontalPic01.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //banner 2号位
    private void selectBanner02Success(String accessoryId, String url, int width, int height) {
        if (mIvHorizontalPic02!=null&&!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic02.getLayoutParams();
            if (radio >= 2.025) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_486));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_486))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_240) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_240));
            }

            mIvHorizontalPic02.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic02);

            mLlAddHorizontalPic02.setVisibility(View.GONE);
            mLlAddHorizontalPic02.setEnabled(false);

            mIvHorizontalPic02.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic02.setTag(R.id.tag_2, url);
            mFlHorizontalPic02.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //banner 3号位
    private void selectBanner03Success(String accessoryId, String url, int width, int height) {
        if (mIvHorizontalPic03!=null&&!EmptyUtils.isEmpty(url)) {
            double radio = new BigDecimal(width).divide(new BigDecimal(height), 3, BigDecimal.ROUND_HALF_UP).doubleValue();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvHorizontalPic03.getLayoutParams();
            if (radio >= 2.025) {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_486));
                layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_486))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
            } else {
                layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_240) * radio);
                layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_240));
            }

            mIvHorizontalPic03.setLayoutParams(layoutParams);
            GlideUtils.loadImg(url, mIvHorizontalPic03);

            mLlAddHorizontalPic03.setVisibility(View.GONE);
            mLlAddHorizontalPic03.setEnabled(false);

            mIvHorizontalPic03.setTag(R.id.tag_1, accessoryId);
            mIvHorizontalPic03.setTag(R.id.tag_2, url);
            mFlHorizontalPic03.setVisibility(View.VISIBLE);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //--------------------------选择图片----------------------------------------------------

    //刷新店铺风格-药品分类
    public void refreshStoreStatusChange(String storeStatus) {
        shopStylePhaSortAdapter.notifyDataSetChanged();
        shopDecoSortAdapter.notifyDataSetChanged();
        if ("1".equals(storeStatus)) {//上线状态 不能修改店铺信息
            //关闭拖拽
            shopStylePhaSortAdapter.disableDragItem();
            mDraggableController.disableDragItem();
        } else {
            //开启拖拽
            shopStylePhaSortAdapter.enableDragItem(itemTouchHelper);
            mDraggableController.enableDragItem(mItemTouchHelper02);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MsgBus.sendHomePhaSortRefresh().removeObserver(mOnHomePhaSortObserve);
        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }
        if (customerBottomListPop != null) {
            customerBottomListPop.dismiss();
            customerBottomListPop.onDestroy();
        }

        if (addNavigationPop != null) {
            addNavigationPop.onCleanListener();
            addNavigationPop.onDestroy();
        }
    }

    public static ShopStyleFragment newIntance() {
        ShopStyleFragment shopStyleFragment = new ShopStyleFragment();
        return shopStyleFragment;
    }
}
