package com.jrdz.zhyb_android.ui.zhyf_manage.fragment;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.banner.listener.OnBannerClickListener;
import com.frame.compiler.widget.banner.manager.GlideAppSimpleImageManager_round;
import com.frame.compiler.widget.banner.widget.BannerLayout;
import com.frame.compiler.widget.customPop.CustomerBottomListPop;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.NewsDetailActivity;
import com.jrdz.zhyb_android.ui.home.model.BannerDataModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.AddDirActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.DraftsActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.DrugStoreHouseActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.MsgListActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.OpenPrescListActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.OrderListActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.PhaSortSpecialAreaActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.ProductPoolActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.SearchActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.ShopDecoActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.OtherSortAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.PhaSortAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GetStoreStatusModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.OtherSortModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.RedPointDataModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.StoreStatusModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-09-29
 * 描    述：智慧药房 商户端首页
 * ================================================
 */
public class SmartPhaHomeFragment extends BaseFragment implements OnBannerClickListener, CustomerBottomListPop.IBottomChooseListener<StoreStatusModel> {
    private LinearLayout mLlTitle, mLlStoreStatus;
    private FrameLayout mFlClose;
    private TextView mTvStoreStatus;
    private FrameLayout mFlMsg;
    private ShapeView mSvRedPoint;
    private NestedScrollView mScrollview;
    private LinearLayout mLlFixmedinsName;
    private TextView mTvFixmedinsName;
    private ShapeLinearLayout mSllSearch;
    private BannerLayout mBanner;
    private CustomeRecyclerView mSrlPhaSort;
    private CustomeRecyclerView mSrlOtherSort;

    private int requestTag = 0;
    private PhaSortAdapter phaSortAdapter;
    private OtherSortAdapter otherSortAdapter;
    private CustomerBottomListPop customerBottomListPop; //店铺状态选择弹框
    private String storeStatusChooseText = "";//店铺状态选择的text
    private PhaSortModel.DataBean phaSortModel;//药品目录 添加按钮

    private boolean isRequest = false;//用来区分是否从接口请求还是全局消息通知 true 接口请求 false 全局消息通知

    //药品目录修改监听
    private ObserverWrapper<PhaSortModel.DataBean> mOnHomePhaSortObserve = new ObserverWrapper<PhaSortModel.DataBean>() {
        @Override
        public void onChanged(@Nullable PhaSortModel.DataBean value) {
            if (null != value) {
                //1代表添加目录 2代表修改目录 3代表删除目录
                switch (value.getSelfTag()) {
                    case 1://添加目录
                        int size = phaSortAdapter.getData().size();
                        if (size == 8) {
                            phaSortAdapter.setData(size - 1, value);
                        } else {
//                            if ("编辑".equals(mTvStoreEdit.getText().toString())){
                            if ("1".equals(Constants.AppStorage.APP_STORE_STATUS)) {
                                phaSortAdapter.addData(value);
                            } else {
                                phaSortAdapter.addData(size - 1, value);
                            }
                        }
                        break;
                    case 2://修改目录
                        phaSortAdapter.getData().set(value.getPos(), value);
                        phaSortAdapter.notifyDataSetChanged();
                        break;
                    case 3://删除目录
                        List<PhaSortModel.DataBean> datas = phaSortAdapter.getData();
                        datas.remove(value.getPos());
//                        if ("完成".equals(mTvStoreEdit.getText().toString())&&!"3".equals(datas.get(datas.size()-1).getIsSystem())){
                        if ("2".equals(Constants.AppStorage.APP_STORE_STATUS) && !"3".equals(datas.get(datas.size() - 1).getIsSystem())) {
                            datas.add(new PhaSortModel.DataBean("3", "添加"));
                        }

                        phaSortAdapter.notifyDataSetChanged();
                        break;
                }
            }
        }
    };
    //店铺状态
    private ObserverWrapper<String> mStoreStatusObserve = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            Constants.AppStorage.APP_STORE_STATUS = value;
            if (isRequest) {
                isRequest = false;
            } else {
                setOnlineStatus(value);
            }
        }
    };

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_smartpha_home;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mLlTitle = view.findViewById(R.id.ll_title);
        mFlClose = view.findViewById(R.id.fl_close);

        mLlStoreStatus = view.findViewById(R.id.ll_store_status);
        mTvStoreStatus = view.findViewById(R.id.tv_store_status);

        mFlMsg = view.findViewById(R.id.fl_msg);
        mSvRedPoint = view.findViewById(R.id.sv_red_point);
        mScrollview = view.findViewById(R.id.scrollview);
        mLlFixmedinsName = view.findViewById(R.id.ll_fixmedins_name);
        mTvFixmedinsName = view.findViewById(R.id.tv_fixmedins_name);
        mSllSearch = view.findViewById(R.id.sll_search);
        mBanner = view.findViewById(R.id.banner);
        mSrlPhaSort = view.findViewById(R.id.srl_pha_sort);
        mSrlOtherSort = view.findViewById(R.id.srl_other_sort);

        ImmersionBar.setTitleBar(this, mLlTitle);
    }

    @Override
    public void initData() {
        super.initData();
        MsgBus.sendHomePhaSortRefresh().observeForever(mOnHomePhaSortObserve);
        MsgBus.sendStoreStatus().observeForever(mStoreStatusObserve);

        mTvFixmedinsName.setText(EmptyUtils.strEmpty(MechanismInfoUtils.getFixmedinsName()));
        //药品分类
        mSrlPhaSort.setHasFixedSize(true);
        mSrlPhaSort.setLayoutManager(new GridLayoutManager(getContext(), 4, GridLayoutManager.VERTICAL, false));
        phaSortAdapter = new PhaSortAdapter();
        mSrlPhaSort.setAdapter(phaSortAdapter);
        //其他分类
        mSrlOtherSort.setHasFixedSize(true);
        mSrlOtherSort.setLayoutManager(new GridLayoutManager(getContext(), 4, GridLayoutManager.VERTICAL, false));
        otherSortAdapter = new OtherSortAdapter();
        mSrlOtherSort.setAdapter(otherSortAdapter);
        otherSortAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getOtherSortData());

        showWaitDialog();
        //不同身份的人进入首页 展现不一样
        if (LoginUtils.isManage()) {
            mLlStoreStatus.setVisibility(View.VISIBLE);

            mTvStoreStatus.setEnabled(true);
            //只要管理员需要才能操作店铺状态
            getStoreStatusData();
        } else {
            mLlStoreStatus.setVisibility(View.INVISIBLE);

            mTvStoreStatus.setEnabled(false);
            dissWaitDailog();
            getPhaSortData("");
        }

        getBannerData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mFlClose.setOnClickListener(this);
        mTvStoreStatus.setOnClickListener(this);
        mSllSearch.setOnClickListener(this);
        mFlMsg.setOnClickListener(this);
        //药品分类 item点击事件
        phaSortAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                PhaSortModel.DataBean itemData = phaSortAdapter.getItem(i);
                if ("3".equals(itemData.getIsSystem())) {//进入添加目录页面
                    AddDirActivity.newIntance(getContext(), 1);
                } else {
                    // 2022-09-30 点击item 进入药品专区
                    PhaSortSpecialAreaActivity.newIntance(getContext(), itemData.getCatalogueId(), itemData.getCatalogueName());
                }
            }
        });

        //其他分类 item点击事件
        otherSortAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                OtherSortModel itemData = otherSortAdapter.getItem(i);
                switch (itemData.getId()) {
                    case "3001"://产品池
                        ProductPoolActivity.newIntance(getContext());
                        break;
                    case "3002"://草稿箱
                        DraftsActivity.newIntance(getContext());
                        break;
                    case "3003"://药品库
                        DrugStoreHouseActivity.newIntance(getContext());
                        break;
                    case "3004"://待发货订单
                        OrderListActivity.newIntance(getContext(), 2);
                        break;
                    case "3005"://店铺装修
                        ShopDecoActivity.newIntance(getContext());
                        break;
                    case "3006"://开处方
                        OpenPrescListActivity.newIntance(getContext());
                        break;
                }
            }
        });
    }

    //获取店铺状态数据
    private void getStoreStatusData() {
        // 2022-09-30 店铺状态得做成全局来 存储到缓存 每次打开智慧药房-商户首页请求状态接口 获取店铺状态数据
        GetStoreStatusModel.sendGetStoreStatusRequest(TAG, new CustomerJsonCallBack<GetStoreStatusModel>() {
            @Override
            public void onRequestError(GetStoreStatusModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GetStoreStatusModel returnData) {
                dissWaitDailog();
                GetStoreStatusModel.DataBean data = returnData.getData();

                if (data != null) {
                    isRequest = true;
                    MsgBus.sendStoreStatus().post(data.getIsOline());

                    getPhaSortData(data.getIsOline());
                }
            }
        });
    }

    //获取轮播图数据
    private void getBannerData() {
        BannerDataModel.sendInsuredBannerDataRequest(TAG, new CustomerJsonCallBack<BannerDataModel>() {
            @Override
            public void onRequestError(BannerDataModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BannerDataModel returnData) {
                dissWaitDailog();
                List<BannerDataModel.DataBean> bannerDatas = returnData.getData();
                if (bannerDatas != null&&!bannerDatas.isEmpty()) {
                    //测试替换图片标记 为了给米脂先看效果图 直接替换图片
                    for (BannerDataModel.DataBean bannerData : bannerDatas) {
                        bannerData.setImgurl("https://up.enterdesk.com/edpic/cc/4c/f9/cc4cf93461658f9caf395bd244170b54.jpg");
                    }
                    if (bannerDatas.size() == 1) {
                        mBanner.initTips(false, false, false)
                                .setImageLoaderManager(new GlideAppSimpleImageManager_round())
                                .setPageNumViewMargin(12, 12, 12, 12)
                                .setViewPagerTouchMode(true)
                                .initListResources(bannerDatas)
                                .switchBanner(false)
                                .setOnBannerClickListener(SmartPhaHomeFragment.this);
                    } else {
                        mBanner.initTips(false, true, false)
                                .setImageLoaderManager(new GlideAppSimpleImageManager_round())
                                .setPageNumViewMargin(12, 12, 12, 12)
                                .setViewPagerTouchMode(false)
                                .initListResources(bannerDatas)
                                .switchBanner(true)
                                .setOnBannerClickListener(SmartPhaHomeFragment.this);
                    }
                }
            }
        });
    }

    //获取药品分类数据
    private void getPhaSortData(String isOline) {
        PhaSortModel.sendHomeSortRequest(TAG, "1", new CustomerJsonCallBack<PhaSortModel>() {
            @Override
            public void onRequestError(PhaSortModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(PhaSortModel returnData) {
                dissWaitDailog();
                List<PhaSortModel.DataBean> datas = returnData.getData();
                if (phaSortAdapter != null && datas != null) {
                    phaSortAdapter.setNewData(datas);

                    setOnlineStatus(isOline);
                }
            }
        });
    }

    //设置店铺状态
    private void setOnlineStatus(String value) {
        if (mTvStoreStatus==null||EmptyUtils.isEmpty(value)) {
            return;
        }
        if ("1".equals(value)) {//上线
            //模拟店铺状态
            mTvStoreStatus.setText("店铺状态:上线");
            storeStatusChooseText = "上线";

            if (phaSortAdapter != null) {
                List<PhaSortModel.DataBean> phaSortDatas = phaSortAdapter.getData();
                if (!phaSortDatas.isEmpty() && "3".equals(phaSortDatas.get(phaSortDatas.size() - 1).getIsSystem())) {
                    phaSortAdapter.getData().remove(phaSortDatas.size() - 1);
                    phaSortAdapter.notifyDataSetChanged();
                }
            }
        } else {//下线
            mTvStoreStatus.setText("店铺状态:下线");
            storeStatusChooseText = "下线";

            if (phaSortAdapter.getData().size() < 8) {
                if (phaSortModel == null) {
                    phaSortModel = new PhaSortModel.DataBean("3", "添加");
                }
                phaSortAdapter.getData().add(phaSortModel);
                phaSortAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onBannerClick(View view, int position, Object model) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            return;
        }
//        if ("编辑".equals(mTvStoreEdit.getText().toString())){
        if (model instanceof BannerDataModel.DataBean) {
            BannerDataModel.DataBean dataBean = (BannerDataModel.DataBean) model;
            if (dataBean != null) {
                //轮播图type 1代表纯广告 2跳外链 3跳公告        跳公告的id我放在url里面
                switch (dataBean.getTypeX()) {
                    case "1":
                        break;
                    case "2":
                        MyWebViewActivity.newIntance(getContext(), dataBean.getTitle(), dataBean.getUrlX(), true, false);
                        break;
                    case "3":
                        NewsDetailActivity.newIntance(getContext(), dataBean.getUrlX());
                        break;
                }
            }
        }
//        }else {
//            //在完成的状态下 可以进入banner管理页面
//            BannerManaActivity.newIntance(getContext());
//        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close:
                goFinish();
                break;
            case R.id.tv_store_status://店铺状态
                if (customerBottomListPop == null) {
                    customerBottomListPop = new CustomerBottomListPop(getContext(), this);
                }
                customerBottomListPop.setDatas("1", "", storeStatusChooseText, CommonlyUsedDataUtils.getInstance().getStoreStatusData());
                customerBottomListPop.showPopupWindow();
                break;
            case R.id.fl_msg://消息
                MsgListActivity.newIntance(getContext());
                break;
            case R.id.sll_search://跳转搜索页面
                SearchActivity.newIntance(getContext());
                break;
        }
    }

    //店铺状态 弹框选择列表点击事件
    @Override
    public void onItemClick(String tag, StoreStatusModel model) {
        if (Constants.AppStorage.APP_STORE_STATUS.equals(model.getId())) {
            return;
        }
        showWaitDialog();
        GetStoreStatusModel.sendUpdateStoreStatusRequest(TAG, model.getId(), new CustomerJsonCallBack<GetStoreStatusModel>() {
            @Override
            public void onRequestError(GetStoreStatusModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GetStoreStatusModel returnData) {
                hideWaitDialog();
                MsgBus.sendStoreStatus().post(model.getId());
            }
        });
    }

    //隐藏加载框
    public void dissWaitDailog() {
        requestTag++;
        if (requestTag >= 3) {
            hideWaitDialog();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mBanner != null) {
            mBanner.start();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mBanner != null) {
            mBanner.stop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisibleToUser) {
            getRedPointData();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            getRedPointData();
        }
    }

    //获取未读信息数量数据
    private void getRedPointData() {
        //2022-09-29 请求后台数据 获取是否有未读的消息
        RedPointDataModel.sendRedPointDataRequest(TAG, new CustomerJsonCallBack<RedPointDataModel>() {
            @Override
            public void onRequestError(RedPointDataModel returnData, String msg) {
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(RedPointDataModel returnData) {
                RedPointDataModel.DataBean data = returnData.getData();
                if (data != null) {
                    if ("0".equals(data.getTotalItems3())) {//全部已读
                        mSvRedPoint.setVisibility(View.GONE);
                    } else {//还有未读
                        mSvRedPoint.setVisibility(View.VISIBLE);
                    }

                    otherSortAdapter.getItem(3).setRedPointNum(Integer.valueOf(EmptyUtils.strEmptyToText(data.getTotalItems1(), "0")));
                    otherSortAdapter.notifyItemChanged(3);

//                    if ("2".equals(LoginUtils.getType())) {//医师身份
//                        otherSortAdapter.getItem(4).setRedPointNum(Integer.valueOf(EmptyUtils.strEmptyToText(data.getTotalItems2(), "0")));
//                        otherSortAdapter.notifyItemChanged(4);
//                    }
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MsgBus.sendHomePhaSortRefresh().removeObserver(mOnHomePhaSortObserve);
        MsgBus.sendStoreStatus().removeObserver(mStoreStatusObserve);

        if (mBanner != null) {
            mBanner.destroy();
            mBanner = null;
        }
        if (customerBottomListPop != null) {
            customerBottomListPop.dismiss();
            customerBottomListPop.onDestroy();
        }

        phaSortModel = null;
    }

    public static SmartPhaHomeFragment newIntance() {
        SmartPhaHomeFragment smartPhaHomeFragment = new SmartPhaHomeFragment();
        return smartPhaHomeFragment;
    }
}
