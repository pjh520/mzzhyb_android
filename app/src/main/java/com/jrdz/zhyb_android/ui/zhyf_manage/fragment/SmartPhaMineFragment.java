package com.jrdz.zhyb_android.ui.zhyf_manage.fragment;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.layout.ShapeRelativeLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.LogisDeliveManageActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.MsgListActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.MyAccountActivity_ZhyfDoctor;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.MyAccountActivity_ZhyfMana;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.MyAddressActivity_Manage;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.MyIssuedPrescrActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.MyQualiActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.OrderListActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.PersonalActivity_Zhyf;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.SettingActivity_ZhyfManage;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.PhaSortMineAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortMineModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/20
 * 描    述：智慧药房--我的页面
 * ================================================
 */
public class SmartPhaMineFragment extends BaseFragment {
    private FrameLayout mFlMsg;
    private FrameLayout mFlSetting;
    private ShapeRelativeLayout mRlPersonal;
    private ImageView mIvAccount;
    private TextView mTvPhone;
    private TextView mTvNickname;
    private CustomeRecyclerView mSrlOrderSort;
    private ShapeLinearLayout mSllOtherSort;
    private CustomeRecyclerView mSrlOtherSort;

    private PhaSortMineAdapter orderSortAdapter;//我的订单 适配器
    private PhaSortMineAdapter otherSortAdapter;//其他服务 适配器

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_smartpha_mine;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mFlMsg = view.findViewById(R.id.fl_msg);
        mFlSetting = view.findViewById(R.id.fl_setting);
        mRlPersonal = view.findViewById(R.id.rl_personal);

        mIvAccount = view.findViewById(R.id.iv_account);
        mTvPhone = view.findViewById(R.id.tv_phone);
        mTvNickname = view.findViewById(R.id.tv_nickname);

        mSrlOrderSort = view.findViewById(R.id.srl_order_sort);
        mSllOtherSort= view.findViewById(R.id.sll_other_sort);
        mSrlOtherSort = view.findViewById(R.id.srl_other_sort);
    }

    @Override
    public void initData() {
        super.initData();
        //2022-10-09 新版本需要增加用户头像
        GlideUtils.loadImg(LoginUtils.getThrAccessoryUrl(), mIvAccount,R.drawable.ic_mine_head,new CircleCrop());
        //不同身份的人进入首页 展现不一样
        if (LoginUtils.isManage()){
            mFlSetting.setVisibility(View.VISIBLE);
        }else {
            mFlSetting.setVisibility(View.GONE);
        }

        mTvPhone.setText(StringUtils.encryptionPhone(LoginUtils.getDr_phone()));
        mTvNickname.setText("管理员".equals(LoginUtils.getUserName())?"管理员":StringUtils.encryptionName(LoginUtils.getUserName()));

        //我的订单
        mSrlOrderSort.setHasFixedSize(true);
        mSrlOrderSort.setLayoutManager(new GridLayoutManager(getContext(), 4, GridLayoutManager.VERTICAL, false));
        orderSortAdapter = new PhaSortMineAdapter();
        mSrlOrderSort.setAdapter(orderSortAdapter);
        orderSortAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getOrderSortData());
        //其他服务
        if ("1".equals(LoginUtils.getType())||"2".equals(LoginUtils.getType())){
            mSllOtherSort.setVisibility(View.VISIBLE);
            mSrlOtherSort.setHasFixedSize(true);
            mSrlOtherSort.setLayoutManager(new GridLayoutManager(getContext(), 4, GridLayoutManager.VERTICAL, false));
            otherSortAdapter = new PhaSortMineAdapter();
            mSrlOtherSort.setAdapter(otherSortAdapter);
            otherSortAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getOtherServiceData());
        }else {
            mSllOtherSort.setVisibility(View.GONE);
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mFlMsg.setOnClickListener(this);
        mFlSetting.setOnClickListener(this);
        mRlPersonal.setOnClickListener(this);

        //我的订单 item点击事件
        orderSortAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                PhaSortMineModel itemData = orderSortAdapter.getItem(i);
                switch (itemData.getId()){
                    case "4001"://全部
                        OrderListActivity.newIntance(getContext(),0);
                        break;
                    case "4002"://待付款
                        OrderListActivity.newIntance(getContext(),1);
                        break;
                    case "4003"://待发货
                        OrderListActivity.newIntance(getContext(),2);
                        break;
                    case "4004"://待收货
                        OrderListActivity.newIntance(getContext(),3);
                        break;
                    case "4005"://已完成
                        OrderListActivity.newIntance(getContext(),4);
                        break;
                    case "4006"://已取消
                        OrderListActivity.newIntance(getContext(),5);
                        break;
                }
            }
        });

        //其他服务 item点击事件
        if (otherSortAdapter!=null){
            otherSortAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                    PhaSortMineModel itemData = otherSortAdapter.getItem(i);
                    switch (itemData.getId()){
                        case "4101"://我的账户
                            if ("1".equals(LoginUtils.getType())){//管理员身份
                                MyAccountActivity_ZhyfMana.newIntance(getContext());
                            }else if ("2".equals(LoginUtils.getType())){//医师身份
                                MyAccountActivity_ZhyfDoctor.newIntance(getContext());
                            }
                            break;
                        case "4102"://我的资质
                            MyQualiActivity.newIntance(getContext());
                            break;
                        case "4103"://我的地址
                            MyAddressActivity_Manage.newIntance(getContext());
                            break;
                        case "4104"://我的配送
                            LogisDeliveManageActivity.newIntance(getContext());
                            break;
                        case "4105"://我开具的处方
                            MyIssuedPrescrActivity.newIntance(getContext());
                            break;
                    }
                }
            });
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_msg://消息
                MsgListActivity.newIntance(getContext());
                break;
            case R.id.fl_setting://设置
                SettingActivity_ZhyfManage.newIntance(getContext());
                break;
            case R.id.rl_personal://个人信息
                PersonalActivity_Zhyf.newIntance(getContext());
                break;

        }
    }

    public static SmartPhaMineFragment newIntance() {
        SmartPhaMineFragment mineFragment = new SmartPhaMineFragment();
        return mineFragment;
    }
}
