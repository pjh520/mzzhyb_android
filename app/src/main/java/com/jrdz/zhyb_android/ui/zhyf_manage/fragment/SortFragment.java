package com.jrdz.zhyb_android.ui.zhyf_manage.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.CostomLoadMoreView;
import com.frame.compiler.widget.CustRefreshLayout;
import com.frame.compiler.widget.customPop.CustomerBottomListPop;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.hjq.shape.view.ShapeView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.LookGoodsActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.SpecialAreaAddGoodsActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.LeftLinkAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.RightLinkAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GetStoreStatusModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.StoreStatusModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.LoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.constant.RefreshState;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-21
 * 描    述：智慧药房--管理端--分类页面
 * ================================================
 */
public class SortFragment extends BaseFragment implements OnRefreshListener, BaseQuickAdapter.RequestLoadMoreListener, CustomerBottomListPop.IBottomChooseListener<StoreStatusModel> {
    private FrameLayout mFlTitle;
    private LinearLayout mLlStoreStatus;
    private TextView mTvStoreStatus;
    private ShapeLinearLayout mSllSearch;
    private EditText mEtSearch;
    private ShapeTextView mStvSearch;
    private RecyclerView mRlLeftList;
    private CustRefreshLayout mRefreshLayout;
    private RecyclerView mRlRightList;
    private TextView mTvTitle;
    private TextView mTvAdd;
    private LinearLayout mLlAll;
    private TextView mTvAll;
    private ShapeView mSvAll;
    private LinearLayout mLlSales;
    private TextView mTvSales;
    private ShapeView mSvSales;
    private LinearLayout mLlPrice;
    private TextView mTvPrice;
    private ShapeView mSvPrice;

    private int textColorPre;
    private int textColorNor;

    private LeftLinkAdapter leftLinkAdapter;
    private RightLinkAdapter rightLinkAdapter;
    private int currentSelectPos = 0;
    private int mPageNum = 0;
    private String catalogueId = "",catalogueName = "";
    private String BySales = "0",ByPrice = "0";
    private String storeStatusChooseText = "";//店铺状态选择的text
    private CustomerBottomListPop customerBottomListPop;

    private ObserverWrapper<String> mAddGoodsObserver=new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            Log.e("onChanged", "onChanged");
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    };

    //店铺状态
    private ObserverWrapper<String> mStoreStatusObserve = new ObserverWrapper<String>(true) {
        @Override
        public void onChanged(@Nullable String value) {
            if ("1".equals(value)) {//上线
                //模拟店铺状态
                mTvStoreStatus.setText("上线");
                storeStatusChooseText = "上线";
            } else {//下线
                mTvStoreStatus.setText("下线");
                storeStatusChooseText = "下线";
            }
        }
    };
    //店铺装修里面修改数据 通知分类页面
    private ObserverWrapper<String> mShopDecoSortObserver=new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String s) {
            showWaitDialog();
            getLeftData();
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_sort;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mFlTitle = view.findViewById(R.id.fl_title);

        mLlStoreStatus = view.findViewById(R.id.ll_store_status);
        mTvStoreStatus = view.findViewById(R.id.tv_store_status);

        mSllSearch = view.findViewById(R.id.sll_search);
        mEtSearch = view.findViewById(R.id.et_search);
        mStvSearch = view.findViewById(R.id.stv_search);

        mRlLeftList = view.findViewById(R.id.rl_left_list);
        mRefreshLayout = view.findViewById(R.id.refreshLayout);
        mRlRightList = view.findViewById(R.id.rl_right_list);

        mTvTitle = view.findViewById(R.id.tv_title);
        mTvAdd = view.findViewById(R.id.tv_add);

        mLlAll = view.findViewById(R.id.ll_all);
        mTvAll = view.findViewById(R.id.tv_all);
        mSvAll = view.findViewById(R.id.sv_all);
        mLlSales = view.findViewById(R.id.ll_sales);
        mTvSales = view.findViewById(R.id.tv_sales);
        mSvSales = view.findViewById(R.id.sv_sales);
        mLlPrice = view.findViewById(R.id.ll_price);
        mTvPrice = view.findViewById(R.id.tv_price);
        mSvPrice = view.findViewById(R.id.sv_price);

        ImmersionBar.setTitleBar(this, mFlTitle);
    }

    @Override
    public void initData() {
        super.initData();
        MsgBus.sendStoreStatus().observe(this, mStoreStatusObserve);
        MsgBus.sendAddGoodsRefresh().observe(this, mAddGoodsObserver);
        MsgBus.sendShopDecoSortRefresh().observe(this, mShopDecoSortObserver);
        textColorPre = getResources().getColor(R.color.color_4870e0);
        textColorNor = getResources().getColor(R.color.color_333333);

        //不同身份的人进入首页 展现不一样
        if (LoginUtils.isManage()) {
            mLlStoreStatus.setVisibility(View.VISIBLE);
            mTvAdd.setVisibility(View.VISIBLE);
        } else {
            mLlStoreStatus.setVisibility(View.GONE);
            mTvAdd.setVisibility(View.GONE);
        }
        setRefreshInfo();

        initLeftView();
        initRightView();
        showWaitDialog();
        getLeftData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mLlStoreStatus.setOnClickListener(this);
        //=========================左边列表===================================================
        //分类列表item点击事件
        leftLinkAdapter.setOnItemClickListener(new OnLeftItemClickListener());
        //=========================左边列表===================================================
        //=========================右边列表===================================================
        //分类列表item点击事件
        rightLinkAdapter.setOnItemClickListener(new OnRightItemClickListener());
        //=========================右边列表===================================================
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())){
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                }
            }
        });
        mStvSearch.setOnClickListener(this);
        mTvAdd.setOnClickListener(this);
        mLlAll.setOnClickListener(this);
        mLlSales.setOnClickListener(this);
        mLlPrice.setOnClickListener(this);
    }

    //设置SmartRefreshLayout的刷新 加载样式
    protected void setRefreshInfo() {
        mRefreshLayout.setEnableRefresh(true);
        mRefreshLayout.setEnableLoadMore(false);
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setPrimaryColorsId(R.color.bar_transparent, R.color.txt_color_666);
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
        mRefreshLayout.setHeaderMaxDragRate(5);//最大显示下拉高度/Header标准高度  头部下拉高度的比例

        mRefreshLayout.setDisableContentWhenRefresh(true);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.ll_store_status://切换店铺状态
                if (customerBottomListPop == null) {
                    customerBottomListPop = new CustomerBottomListPop(getContext(), this);
                }
                customerBottomListPop.setDatas("1", "", storeStatusChooseText, CommonlyUsedDataUtils.getInstance().getStoreStatusData());
                customerBottomListPop.showPopupWindow();
                break;
            case R.id.stv_search://搜索
                if (EmptyUtils.isEmpty(mEtSearch.getText().toString())){
                    showShortToast("请输入药品名称或药房名称");
                    return;
                }

                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.tv_add://添加
                SpecialAreaAddGoodsActivity.newIntance(getContext(),"2",catalogueId,catalogueName);
                break;
            case R.id.ll_all://全部
                setViewScreen(0);
                BySales="0";
                ByPrice="0";
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.ll_sales://销量
                setViewScreen(1);
                BySales="1";
                ByPrice="0";
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
            case R.id.ll_price://价格
                setViewScreen(2);
                BySales="0";
                ByPrice="2";
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
        }
    }

    //设置全部 销量 价格 点击之后 的样式
    private void setViewScreen(int tag) {
        switch (tag) {
            case 0:
                mTvAll.setTextColor(textColorPre);
                mSvAll.setVisibility(View.VISIBLE);
                mTvSales.setTextColor(textColorNor);
                mSvSales.setVisibility(View.INVISIBLE);
                mTvPrice.setTextColor(textColorNor);
                mSvPrice.setVisibility(View.INVISIBLE);
                break;
            case 1:
                mTvAll.setTextColor(textColorNor);
                mSvAll.setVisibility(View.INVISIBLE);
                mTvSales.setTextColor(textColorPre);
                mSvSales.setVisibility(View.VISIBLE);
                mTvPrice.setTextColor(textColorNor);
                mSvPrice.setVisibility(View.INVISIBLE);
                break;
            case 2:
                mTvAll.setTextColor(textColorNor);
                mSvAll.setVisibility(View.INVISIBLE);
                mTvSales.setTextColor(textColorNor);
                mSvSales.setVisibility(View.INVISIBLE);
                mTvPrice.setTextColor(textColorPre);
                mSvPrice.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onItemClick(String tag, StoreStatusModel model) {
        if (Constants.AppStorage.APP_STORE_STATUS.equals(model.getId())) {
            return;
        }
        showWaitDialog();
        GetStoreStatusModel.sendUpdateStoreStatusRequest(TAG, model.getId(), new CustomerJsonCallBack<GetStoreStatusModel>() {
            @Override
            public void onRequestError(GetStoreStatusModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GetStoreStatusModel returnData) {
                hideWaitDialog();
                MsgBus.sendStoreStatus().post(model.getId());
            }
        });
    }

    //=========================左边列表===================================================
    //初始化左边分类view
    private void initLeftView() {
        mRlLeftList.setHasFixedSize(true);
        mRlLeftList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        leftLinkAdapter = new LeftLinkAdapter();
        mRlLeftList.setAdapter(leftLinkAdapter);
    }

    //初始化左边分类数据
    private void getLeftData() {
        PhaSortModel.sendSortRequest(TAG,"1",new CustomerJsonCallBack<PhaSortModel>() {
            @Override
            public void onRequestError(PhaSortModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(PhaSortModel returnData) {
                List<PhaSortModel.DataBean> datas = returnData.getData();
                hideWaitDialog();
                if (leftLinkAdapter!=null&&datas != null) {
                    leftLinkAdapter.setNewData(datas);
                    //设置数据已经请求的id
                    for (int i = 0; i < datas.size(); i++) {
                        PhaSortModel.DataBean data = datas.get(i);
                        if (i == 0) {
                            data.setChoose("1");
                            catalogueId = data.getCatalogueId();
                            catalogueName= data.getCatalogueName();
                            if (mTvTitle!=null){
                                mTvTitle.setText(data.getCatalogueName());
                            }
                        } else if (i == 1) {
                            data.setChoose("bottom01");
                        } else {
                            data.setChoose("0");
                        }
                    }

                    onRefresh(mRefreshLayout);
                } else {
                    showShortToast("数据有误，请重新进入页面");
                }
            }
        });
    }

    //左边列表item点击事件
    private class OnLeftItemClickListener implements BaseQuickAdapter.OnItemClickListener {

        @Override
        public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
            LeftLinkAdapter leftLinkAdapter = ((LeftLinkAdapter) baseQuickAdapter);
            PhaSortModel.DataBean leftLinkModel = leftLinkAdapter.getItem(i);

            if (!"1".equals(leftLinkModel.getChoose())) {//只响应未被选中的item
                moveToMiddle(mRlLeftList, i);

                leftLinkAdapter.setSelectedPosttion(baseQuickAdapter.getItemCount(), currentSelectPos, i);
                currentSelectPos = i;
                //设置数据已经请求的id
                catalogueId = leftLinkModel.getCatalogueId();
                catalogueName= leftLinkModel.getCatalogueName();
                mTvTitle.setText(leftLinkModel.getCatalogueName());

                rightLinkAdapter.getData().clear();
                rightLinkAdapter.notifyDataSetChanged();
                rightLinkAdapter.isUseEmpty(false);
                showWaitDialog();
                onRefresh(mRefreshLayout);
            }
        }
    }

    //将当前选中的item居中
    public void moveToMiddle(RecyclerView recyclerView, int position) {
        //先从RecyclerView的LayoutManager中获取当前第一项和最后一项的Position
        int firstItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
        int lastItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
        //中间位置
        int middle = (firstItem + lastItem) / 2;
        // 取绝对值，index下标是当前的位置和中间位置的差，下标为index的view的top就是需要滑动的距离
        int index = (position - middle) >= 0 ? position - middle : -(position - middle);
        //左侧列表一共有getChildCount个Item，如果>这个值会返回null，程序崩溃，如果>getChildCount直接滑到指定位置,或者,都一样啦
        if (index >= recyclerView.getChildCount()) {
            recyclerView.scrollToPosition(position);
        } else {
            //如果当前位置在中间位置上面，往下移动，这里为了防止越界
            if (position < middle) {
                recyclerView.scrollBy(0, -recyclerView.getChildAt(index).getTop());
                // 在中间位置的下面，往上移动
            } else {
                recyclerView.scrollBy(0, recyclerView.getChildAt(index).getTop());
            }
        }
    }
    //=========================左边列表===================================================

    //=========================右边列表===================================================

    //初始化右边分类详情列表view
    public void initRightView() {
        mRlRightList.setHasFixedSize(true);
        mRlRightList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rightLinkAdapter = new RightLinkAdapter();
        mRlRightList.setAdapter(rightLinkAdapter);
        rightLinkAdapter.isUseEmpty(false);
        //设置开启上拉加载更多
        rightLinkAdapter.setEnableLoadMore(true);
        //设置上拉加载更多监听
        rightLinkAdapter.setOnLoadMoreListener(this, mRlRightList);
        rightLinkAdapter.setLoadMoreView(new CostomLoadMoreView());
        rightLinkAdapter.setEmptyView(R.layout.layout_empty_view, mRlRightList);
    }

    //获取右边列表数据
    private void getRightData() {
        GoodsModel.sendSortGoodsListRequest_user(TAG, String.valueOf(mPageNum), "20", mEtSearch.getText().toString(),
                catalogueId, "", MechanismInfoUtils.getFixmedinsCode(),BySales,ByPrice, new CustomerJsonCallBack<GoodsModel>() {
                    @Override
                    public void onRequestError(GoodsModel returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(GoodsModel returnData) {
                        hideRefreshView();
                        List<GoodsModel.DataBean> datas = returnData.getData();
                        if (rightLinkAdapter != null && datas != null) {
                            //获取数据成功
                            if (mPageNum == 0) {
                                if (datas == null || datas.isEmpty()) {
                                    rightLinkAdapter.isUseEmpty(true);
                                }
                                rightLinkAdapter.setNewData(datas);
                                mRlRightList.scrollToPosition(0);
                            } else {
                                rightLinkAdapter.addData(datas);
                                rightLinkAdapter.loadMoreComplete();
                            }

                            if (datas.isEmpty()) {
                                if (rightLinkAdapter.getData().size() < 8) {
                                    rightLinkAdapter.loadMoreEnd(true);
                                } else {
                                    rightLinkAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    //右边列表item点击事件
    private class OnRightItemClickListener implements BaseQuickAdapter.OnItemClickListener {

        @Override
        public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
            if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_1000)) {
                return;
            }
            RightLinkAdapter rightLinkAdapter = ((RightLinkAdapter) baseQuickAdapter);
            GoodsModel.DataBean rightLinkModel = rightLinkAdapter.getItem(i);
            if (rightLinkModel!=null&&!EmptyUtils.isEmpty(rightLinkModel.getGoodsNo())){
                Intent intent=new Intent(getContext(), LookGoodsActivity.class);
                intent.putExtra("GoodsNo", rightLinkModel.getGoodsNo());
                startActivityForResult(intent, new BaseFragment.OnActivityCallback() {
                    @Override
                    public void onActivityResult(int resultCode, @Nullable Intent data) {
                        if (resultCode== Activity.RESULT_OK){
                            baseQuickAdapter.remove(i);
                        }
                    }
                });
            }else {
                showShortToast("数据有误");
            }
        }
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        mPageNum = 0;
        getRightData();
    }

    @Override
    public void onLoadMoreRequested() {
        mPageNum++;
        getRightData();
    }

    //关闭刷新的view
    public void hideRefreshView() {
        if (mRefreshLayout!=null&&mRefreshLayout.getState() == RefreshState.Refreshing) {
            mRefreshLayout.finishRefresh();
        } else {
            hideWaitDialog();
        }
    }

    protected void setLoadMoreFail() {
        if (rightLinkAdapter!=null){
            rightLinkAdapter.loadMoreFail();
        }

        if (mPageNum > 1) {
            mPageNum -= 1;
        }
    }
    //=========================右边列表===================================================

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (customerBottomListPop != null) {
            customerBottomListPop.dismiss();
            customerBottomListPop.onDestroy();
        }
    }

    public static SortFragment newIntance() {
        SortFragment sortFragment = new SortFragment();
        return sortFragment;
    }
}
