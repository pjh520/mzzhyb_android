//package com.jrdz.zhyb_android.ui.zhyf_manage.fragment;
//
//import android.content.Context;
//import android.util.Log;
//import android.view.View;
//import android.widget.FrameLayout;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.chad.library.adapter.base.BaseQuickAdapter;
//import com.frame.compiler.widget.CostomLoadMoreView;
//import com.gyf.immersionbar.ImmersionBar;
//import com.jrdz.zhyb_android.R;
//import com.jrdz.zhyb_android.base.BaseFragment;
//import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.LeftLinkAdapter;
//import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.LinkViewAdapter;
//import com.jrdz.zhyb_android.ui.zhyf_manage.model.LeftLinkModel;
//import com.jrdz.zhyb_android.ui.zhyf_manage.model.LinkViewModel;
//import com.oushangfeng.pinnedsectionitemdecoration.PinnedHeaderItemDecoration;
//import com.oushangfeng.pinnedsectionitemdecoration.callback.OnHeaderClickListener;
//
//import java.util.ArrayList;
//
///**
// * ================================================
// * 项目名称：zhyb_android
// * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.fragment
// * 作    者：彭俊鸿
// * 邮    箱：1031028399@qq.com
// * 版    本：1.0
// * 创建日期：2022-10-19
// * 描    述：智慧药房--管理端--分类页面(美团)
// * ================================================
// */
//public class SortFragment_test extends BaseFragment implements BaseQuickAdapter.RequestLoadMoreListener {
//    private FrameLayout mFlTitle;
//    private RecyclerView mRlLeftList;
//    protected RecyclerView mRecyclerView;
//
//    private LeftLinkAdapter leftLinkAdapter;
//    private int currentSelectPos = 0;
//    protected LinkViewAdapter mAdapter;
//
//    public int mPageNum = 0;//上拉加载刷新的页数(大类)
//    public int mSmallPageNum = 0;//上拉加载刷新的页数(小类)
//    public int mMaxNum = 0;//上拉加载刷新的最大个数
//    public int mNum = 0;//上拉加载刷新的个数
//
//    private int mRefreshPageNum = 0;//下拉刷新的页数(大类)
//    private int mRefreshSmallPageNum = 0;//下拉刷新的页数(小类)
//    public int mRefreshMaxNum = 0;//下拉刷新的最大个数
//    public int mRefreshNum = 0;//下拉刷新的个数
//
//    private boolean isLoading = false;//是否正在下拉刷新
//    private boolean isLeftClickRefresh = false;//是否是点击左边分类 引起刷新
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        enableLazyLoad();
//    }
//
//    @Override
//    protected int getLayoutId() {
//        return R.layout.fragment_sort_test03;
//    }
//
//    @Override
//    public void initView(View view) {
//        super.initView(view);
//        mFlTitle = view.findViewById(R.id.fl_title);
//        mRlLeftList = view.findViewById(R.id.rl_left_list);
//        mRecyclerView = view.findViewById(R.id.recyclerView);
//
//        ImmersionBar.setTitleBar(this, mFlTitle);
//    }
//
//    @Override
//    public void initData() {
//        super.initData();
//        initLeftView();
//        initLeftData();
//        initRightView();
//
//        showWaitDialog();
//        onInitRightData();
//    }
//
//    @Override
//    public void initEvent() {
//        super.initEvent();
//        //=========================左边列表===================================================
//        //分类列表item点击事件
//        leftLinkAdapter.setOnItemClickListener(new OnLeftItemClickListener());
//        //=========================左边列表===================================================
//
//        //=========================右边列表===================================================
//        mRecyclerView.addItemDecoration(new PinnedHeaderItemDecoration.Builder(LinkViewModel.TITLE_TAG)
//                .setClickIds(R.id.tv_add, R.id.ll_all, R.id.ll_sales, R.id.ll_price)
//                .setHeaderClickListener(new OnHeaderClickListener() {
//                    @Override
//                    public void onHeaderClick(View view, int id, int position) {
//                        switch (view.getId()) {
//                            case R.id.tv_add://添加
//                                showShortToast("添加");
//                                break;
//                            case R.id.ll_all://全部
//                                Log.e("666666", "全部" + position);
//                                break;
//                            case R.id.ll_sales://销量
//                                Log.e("666666", "销量" + position);
//                                break;
//                            case R.id.ll_price://价格
//                                Log.e("666666", "价格" + position);
//                                break;
//                        }
//                    }
//
//                    @Override
//                    public void onHeaderLongClick(View view, int id, int position) {
//
//                    }
//                })
//                .create());
//        //设置开启上拉加载更多
//        mAdapter.setEnableLoadMore(true);
//        //设置上拉加载更多监听
//        mAdapter.setOnLoadMoreListener(this, mRecyclerView);
//        //设置列表移动监听
//        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//            }
//
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                if (mAdapter.getData().isEmpty()){
//                    return;
//                }
//                //获取右侧列表的第一个可见Item的position
//                int topPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
//                // 如果此项对应的是左边的大类的postion
//                int postion = mAdapter.getData().get(topPosition).getPostion();
//
//                //下拉刷新
//                if (!isLeftClickRefresh && mRefreshPageNum >= 0 && !isLoading) {
//                    // 第二条可见就触发加载
//                    if (topPosition <= 2) {
//                        isLoading = true;
//
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                getRightRefreshData();
//                            }
//                        });
//                    }
//                }
//
//                //关联左边的分类列表
//                if (postion != -1 && postion != currentSelectPos) {
//                    moveToMiddle(mRlLeftList, postion);
//                    leftLinkAdapter.setSelectedPosttion(leftLinkAdapter.getItemCount(), currentSelectPos, postion);
//                    currentSelectPos = postion;
//                }
//            }
//        });
//        //=========================右边列表===================================================
//    }
//
//    //=========================左边列表===================================================
//    //初始化左边分类view
//    private void initLeftView() {
//        mRlLeftList.setHasFixedSize(true);
//        mRlLeftList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
//        leftLinkAdapter = new LeftLinkAdapter();
//        mRlLeftList.setAdapter(leftLinkAdapter);
//    }
//
//    //初始化左边分类数据
//    private void initLeftData() {
//        ArrayList<LeftLinkModel> leftLinkModels = new ArrayList<>();
//
//        LeftLinkModel leftLinkModel01 = new LeftLinkModel("1", "活动", "1");
//        LeftLinkModel leftLinkModel02 = new LeftLinkModel("2", "日常/备用", "bottom01");
//        LeftLinkModel leftLinkModel03 = new LeftLinkModel("3", "感冒/用药", "0");
//        LeftLinkModel leftLinkModel04 = new LeftLinkModel("4", "夏日/花茶", "0");
//        LeftLinkModel leftLinkModel05 = new LeftLinkModel("5", "清热/解毒", "0");
//        LeftLinkModel leftLinkModel06 = new LeftLinkModel("6", "皮肤/用药", "0");
//        LeftLinkModel leftLinkModel07 = new LeftLinkModel("7", "肠胃/用药", "0");
//        LeftLinkModel leftLinkModel08 = new LeftLinkModel("8", "男科干威", "0");
//        LeftLinkModel leftLinkModel09 = new LeftLinkModel("9", "冈本避孕套", "0");
//        LeftLinkModel leftLinkModel10 = new LeftLinkModel("10", "计生/用品", "0");
//        LeftLinkModel leftLinkModel11 = new LeftLinkModel("11", "儿童/用品", "0");
//        LeftLinkModel leftLinkModel12 = new LeftLinkModel("12", "女性/健康", "0");
//        LeftLinkModel leftLinkModel13 = new LeftLinkModel("13", "呼吸/道药", "0");
//        LeftLinkModel leftLinkModel14 = new LeftLinkModel("14", "五官科", "0");
//        LeftLinkModel leftLinkModel15 = new LeftLinkModel("15", "Chu Fang", "0");
//
//        leftLinkModels.add(leftLinkModel01);
//        leftLinkModels.add(leftLinkModel02);
//        leftLinkModels.add(leftLinkModel03);
//        leftLinkModels.add(leftLinkModel04);
//        leftLinkModels.add(leftLinkModel05);
//        leftLinkModels.add(leftLinkModel06);
//        leftLinkModels.add(leftLinkModel07);
//        leftLinkModels.add(leftLinkModel08);
//        leftLinkModels.add(leftLinkModel09);
//        leftLinkModels.add(leftLinkModel10);
//        leftLinkModels.add(leftLinkModel11);
//        leftLinkModels.add(leftLinkModel12);
//        leftLinkModels.add(leftLinkModel13);
//        leftLinkModels.add(leftLinkModel14);
//        leftLinkModels.add(leftLinkModel15);
//
//        leftLinkAdapter.setNewData(leftLinkModels);
//    }
//
//    //左边列表item点击事件
//    private class OnLeftItemClickListener implements BaseQuickAdapter.OnItemClickListener {
//
//        @Override
//        public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
//            LeftLinkAdapter leftLinkAdapter = ((LeftLinkAdapter) baseQuickAdapter);
//            LeftLinkModel leftLinkModel = leftLinkAdapter.getItem(i);
//
//            if (!"1".equals(leftLinkModel.getChoose())) {//只响应未被选中的item
//                moveToMiddle(mRlLeftList, i);
//
//                leftLinkAdapter.setSelectedPosttion(baseQuickAdapter.getItemCount(), currentSelectPos, i);
//                currentSelectPos = i;
//
//                //右边列表 重新请求数据
//                mPageNum = i;
//                mRefreshPageNum = i;
//                mRefreshSmallPageNum = 0;
//                mRefreshNum = 0;
//                mRefreshMaxNum = 0;
//
//                isLeftClickRefresh = true;
//
//                showWaitDialog();
//                leftClcikLinkData();
//            }
//        }
//    }
//
//    //将当前选中的item居中
//    public void moveToMiddle(RecyclerView recyclerView, int position) {
//        //先从RecyclerView的LayoutManager中获取当前第一项和最后一项的Position
//        int firstItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
//        int lastItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
//        //中间位置
//        int middle = (firstItem + lastItem) / 2;
//        // 取绝对值，index下标是当前的位置和中间位置的差，下标为index的view的top就是需要滑动的距离
//        int index = (position - middle) >= 0 ? position - middle : -(position - middle);
//        //左侧列表一共有getChildCount个Item，如果>这个值会返回null，程序崩溃，如果>getChildCount直接滑到指定位置,或者,都一样啦
//        if (index >= recyclerView.getChildCount()) {
//            recyclerView.scrollToPosition(position);
//        } else {
//            //如果当前位置在中间位置上面，往下移动，这里为了防止越界
//            if (position < middle) {
//                recyclerView.scrollBy(0, -recyclerView.getChildAt(index).getTop());
//                // 在中间位置的下面，往上移动
//            } else {
//                recyclerView.scrollBy(0, recyclerView.getChildAt(index).getTop());
//            }
//        }
//    }
//    //=========================左边列表===================================================
//
//    //=========================右边列表===================================================
//    //初始化右边分类详情列表view
//    public void initRightView() {
//        mRecyclerView.setHasFixedSize(true);
//        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
//        mAdapter = new LinkViewAdapter();
//        mRecyclerView.setAdapter(mAdapter);
//
//        mAdapter.setLoadMoreView(new CostomLoadMoreView());
//        mAdapter.setEmptyView(R.layout.layout_empty_view, mRecyclerView);
//
//        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
//            @Override
//            public void onItemChildClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
//                switch (view.getId()) {
//                    case R.id.tv_add://添加
//                        showShortToast("添加");
//                        break;
//                    case R.id.ll_all://全部
//                        Log.e("666666", "全部" + i);
//                        break;
//                    case R.id.ll_sales://销量
//                        Log.e("666666", "销量" + i);
//                        break;
//                    case R.id.ll_price://价格
//                        Log.e("666666", "价格" + i);
//                        break;
//                }
//            }
//        });
//    }
//
//    //左边分类点击 右边根据分类获取新的数据
//    public void leftClcikLinkData() {
//        mRecyclerView.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                ArrayList<LinkViewModel> datas = getDataByClassifyId(leftLinkAdapter.getData().get(mPageNum).getClassifyId(), mPageNum);
//                LinkViewModel linkViewModelTitle = new LinkViewModel();
//                linkViewModelTitle.setTitleData(LinkViewModel.TITLE_TAG, leftLinkAdapter.getData().get(mPageNum).getTitle(), mPageNum);
//                datas.add(0, linkViewModelTitle);
//
//                mAdapter.setNewData(datas);
//                mNum = datas.size();
//                mMaxNum = 8;
//
//                mRecyclerView.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (isLeftClickRefresh) {
//                            int pageNo = mRefreshPageNum - 1;
//
//                            if (pageNo >= 0) {
//                                ArrayList<LinkViewModel> datas = getDataByClassifyId(leftLinkAdapter.getData().get(pageNo).getClassifyId(), pageNo);
//                                LinkViewModel linkViewModelTitle = new LinkViewModel();
//                                linkViewModelTitle.setTitleData(LinkViewModel.TITLE_TAG, leftLinkAdapter.getData().get(pageNo).getTitle(), pageNo);
//                                datas.add(0, linkViewModelTitle);
//
//                                mAdapter.getData().addAll(0, datas);
//                                mAdapter.notifyDataSetChanged();
//
//                                mRefreshPageNum--;
//                                mRefreshNum = datas.size();
//                                mRefreshMaxNum = 12;
//
//                                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
//                                linearLayoutManager.scrollToPositionWithOffset(datas.size(), 0);
//                            }
//
//                            isLeftClickRefresh = false;
//                        }
//                        hideWaitDialog();
//                    }
//                }, 1000);
//
//            }
//        }, 1000);
//    }
//
//    //右边列表下拉刷新
//    private void onInitRightData() {
//        mPageNum = 0;
//        mSmallPageNum = 0;
//        getRightLoadMoreData();
//    }
//
//    @Override
//    public void onLoadMoreRequested() {
//        if (mNum >= mMaxNum) {
//            mPageNum++;
//            mSmallPageNum = 0;
//            mNum = 0;
//            mMaxNum = 0;
//        } else {
//            mSmallPageNum++;
//        }
//
//        Log.e("onLoadMoreRequested", "onLoadMoreRequested===mPageNum===" + mPageNum + "====mSmallPageNum====" + mSmallPageNum);
//        getRightLoadMoreData();
//    }
//
//    //当直接点击左边分类时，点击的位置不是第一个时，用户可以下拉刷新获取数据
//    public void getRightRefreshData() {
//        showWaitDialog();
//
//        if (mRefreshNum >= mRefreshMaxNum) {
//            mRefreshSmallPageNum = 0;
//            mRefreshNum = 0;
//            mRefreshMaxNum = 0;
//        } else {
//            mRefreshSmallPageNum++;
//        }
//
//        int pageNo = mRefreshPageNum;
//        if (mRefreshSmallPageNum == 0) {
//            pageNo = mRefreshPageNum - 1;
//        }
//
//        if (pageNo < 0) {
//            mRefreshSmallPageNum = 0;
//            mRefreshNum = 0;
//            mRefreshMaxNum = 0;
//
//            isLoading = false;
//            hideWaitDialog();
//            return;
//        }
//
//        int finalPageNo = pageNo;
//        mRecyclerView.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                ArrayList<LinkViewModel> datas = getDataByClassifyId(leftLinkAdapter.getData().get(finalPageNo).getClassifyId(), finalPageNo);
//
//                if (datas != null) {
//                    if (mRefreshSmallPageNum == 0) {
//                        LinkViewModel linkViewModelTitle = new LinkViewModel();
//                        linkViewModelTitle.setTitleData(LinkViewModel.TITLE_TAG, leftLinkAdapter.getData().get(finalPageNo).getTitle(), finalPageNo);
//                        datas.add(0, linkViewModelTitle);
//                        //赋值该Classify下 数据的最大数
//                        mRefreshNum = datas.size();
//                        mRefreshMaxNum = 12;
//                        mAdapter.addData(0, datas);
//
//                        mRefreshPageNum--;
//                    } else {
//                        mRefreshNum += datas.size();
//                        mAdapter.addData(1, datas);
//                        mAdapter.loadMoreComplete();
//                    }
//
//                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
//                    linearLayoutManager.scrollToPositionWithOffset(datas.size(), 0);
//                }
//                isLoading = false;
//                hideWaitDialog();
//            }
//        }, 1000);
//    }
//
//    //获取右边分类详情数据(加载更多的情况)
//    public void getRightLoadMoreData() {
//        if (mPageNum >= leftLinkAdapter.getData().size()) {
//            hideWaitDialog();
//            mAdapter.loadMoreComplete();
//            mAdapter.loadMoreEnd();
//            return;
//        }
//
//        // TODO: 2022-10-20 模拟获取数据
//        mRecyclerView.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                ArrayList<LinkViewModel> datas = getDataByClassifyId(leftLinkAdapter.getData().get(mPageNum).getClassifyId(), mPageNum);
//                hideWaitDialog();
//                if (datas != null) {
//                    if (mSmallPageNum == 0) {
//                        LinkViewModel linkViewModelTitle = new LinkViewModel();
//                        linkViewModelTitle.setTitleData(LinkViewModel.TITLE_TAG, leftLinkAdapter.getData().get(mPageNum).getTitle(), mPageNum);
//
//                        //赋值该Classify下 数据的最大数
//                        mMaxNum = 8;
//                        datas.add(0, linkViewModelTitle);
//                    }
//
//                    if (mPageNum == 0 && mSmallPageNum == 0) {
//                        mNum = datas.size();
//                        mAdapter.setNewData(datas);
//                    } else {
//                        mNum += datas.size();
//                        mAdapter.addData(datas);
//                        mAdapter.loadMoreComplete();
//                    }
//                }
//            }
//        }, 1000);
//    }
//    //=========================右边列表===================================================
//
//    //模拟生成数据
//    public ArrayList<LinkViewModel> getDataByClassifyId(String classifyId, int postion) {
//
//        ArrayList<LinkViewModel> linkViewModels = new ArrayList<>();
//        //====================10============================
//        LinkViewModel linkViewModel11 = new LinkViewModel();
//        linkViewModel11.setBodyData(LinkViewModel.BODY_TAG, "内容-" + classifyId + "1", postion);
//        LinkViewModel linkViewModel12 = new LinkViewModel();
//        linkViewModel12.setBodyData(LinkViewModel.BODY_TAG, "内容-" + classifyId + "2", postion);
//        LinkViewModel linkViewModel13 = new LinkViewModel();
//        linkViewModel13.setBodyData(LinkViewModel.BODY_TAG, "内容-" + classifyId + "3", postion);
//        LinkViewModel linkViewModel14 = new LinkViewModel();
//        linkViewModel14.setBodyData(LinkViewModel.BODY_TAG, "内容-" + classifyId + "4", postion);
//
//        linkViewModels.add(linkViewModel11);
//        linkViewModels.add(linkViewModel12);
//        linkViewModels.add(linkViewModel13);
//        linkViewModels.add(linkViewModel14);
//
//        return linkViewModels;
//    }
//
//    //加载更多时 遇到外围因素 失败时调用
//    protected void setLoadMoreFail() {
//        mAdapter.loadMoreFail();
//        if (mPageNum >= 1) {
//            mPageNum -= 1;
//        }
//    }
//
//    public static SortFragment_test newIntance() {
//        SortFragment_test sortFragment = new SortFragment_test();
//        return sortFragment;
//    }
//}
