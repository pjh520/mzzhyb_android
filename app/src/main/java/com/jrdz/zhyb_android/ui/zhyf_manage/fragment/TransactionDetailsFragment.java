package com.jrdz.zhyb_android.ui.zhyf_manage.fragment;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.wheel.TimeWheelUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.TransactionDetailsAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TransactionDetailsModel;
import com.jrdz.zhyb_android.widget.pop.TransactionScreenPop;
import com.scwang.smart.refresh.layout.constant.RefreshState;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-06-14
 * 描    述：交易明细
 * ================================================
 */
public class TransactionDetailsFragment extends BaseRecyclerViewFragment implements TransactionScreenPop.IOptionListener{
    private ShapeLinearLayout mSllStartTime;
    private TextView mTvStartTime;
    private ShapeLinearLayout mSllEndTime;
    private TextView mTvEndTime;
    private ShapeTextView mTvScreen;

    private TimeWheelUtils timeWheelUtils;
    private TransactionScreenPop transactionScreenPop;
    private List<PhaSortModel.DataBean> datas;
    private String payType="",purchasedType="";
    private int requestTag=0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_transaction_details;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mSllStartTime = view.findViewById(R.id.sll_start_time);
        mTvStartTime = view.findViewById(R.id.tv_start_time);
        mSllEndTime = view.findViewById(R.id.sll_end_time);
        mTvEndTime = view.findViewById(R.id.tv_end_time);
        mTvScreen = view.findViewById(R.id.tv_screen);
        ImmersionBar.setTitleBar(this, mTitleBar);
        mTitleBar.setTitle("明细");
        hideLeftView();
    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        getPurchasedTypeData();
        onRefresh(mRefreshLayout);
//        mAdapter.setNewData(null);
    }

    @Override
    public void initAdapter() {
        mAdapter=new TransactionDetailsAdapter();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mSllStartTime.setOnClickListener(this);
        mSllEndTime.setOnClickListener(this);
        mTvScreen.setOnClickListener(this);

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    //获取购药种类数据
    private void getPurchasedTypeData() {
        PhaSortModel.sendSortRequest(TAG,"1",new CustomerJsonCallBack<PhaSortModel>() {
            @Override
            public void onRequestError(PhaSortModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(PhaSortModel returnData) {
                datas = returnData.getData();
                for (PhaSortModel.DataBean data : datas) {
                    data.setChoose("0");
                }

                dissWaitDailog();
            }
        });
    }

    @Override
    protected void getData() {
        super.getData();
        String begindate="", enddate="";
        if (!EmptyUtils.isEmpty(mTvStartTime.getText().toString())&&!EmptyUtils.isEmpty(mTvEndTime.getText().toString())){
            begindate=mTvStartTime.getText().toString();
            enddate=mTvEndTime.getText().toString();
        }

        TransactionDetailsModel.sendTransactionDetailsRequest(TAG, String.valueOf(mPageNum), "20",
                begindate, enddate, payType, purchasedType, new CustomerJsonCallBack<TransactionDetailsModel>() {
                    @Override
                    public void onRequestError(TransactionDetailsModel returnData, String msg) {
                        dissWaitDailog();
                        setLoadMoreFail();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(TransactionDetailsModel returnData) {
                        dissWaitDailog();
                        if (returnData.getData() != null) {
                            List<TransactionDetailsModel.DataBean> infos = returnData.getData();
                            if (mAdapter!=null&&infos != null) {
                                if (mPageNum == 0) {
                                    mAdapter.setNewData(infos);
                                    if (infos.size() <= 0) {
                                        mAdapter.isUseEmpty(true);
                                    }
                                } else {
                                    mAdapter.addData(infos);
                                    mAdapter.loadMoreComplete();
                                }

                                if (infos.isEmpty()) {
                                    if (mAdapter.getData().size() < 6) {
                                        mAdapter.loadMoreEnd(true);
                                    } else {
                                        mAdapter.loadMoreEnd();
                                    }
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.sll_start_time:
                KeyboardUtils.hideSoftInput(mSllStartTime);
                if (timeWheelUtils == null) {
                    timeWheelUtils = new TimeWheelUtils();
                    timeWheelUtils.isShowDay(true, true, true, true, true, true);
                }
                timeWheelUtils.showTimeWheel(getContext(), "开始日期", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mTvStartTime.setText(EmptyUtils.strEmpty(dateInfo));

                        if (!EmptyUtils.isEmpty(mTvEndTime.getText().toString())){
                            showWaitDialog();
                            onRefresh(mRefreshLayout);
                        }
                    }
                });
                break;
            case R.id.sll_end_time:
                KeyboardUtils.hideSoftInput(mSllStartTime);
                if (timeWheelUtils == null) {
                    timeWheelUtils = new TimeWheelUtils();
                    timeWheelUtils.isShowDay(true, true, true, true, true, true);
                }
                timeWheelUtils.showTimeWheel(getContext(), "结束日期", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mTvEndTime.setText(EmptyUtils.strEmpty(dateInfo));
                        if (!EmptyUtils.isEmpty(mTvStartTime.getText().toString())){
                            showWaitDialog();
                            onRefresh(mRefreshLayout);
                        }
                    }
                });
                break;
            case R.id.tv_screen://筛选
                if (transactionScreenPop == null) {
                    if (datas!=null){
                        transactionScreenPop = new TransactionScreenPop(getContext(), datas, this);
                    }else {
                        showShortToast("数据有误，请关闭app重新进入");
                        return;
                    }
                }

                transactionScreenPop.showPopupWindow();
                break;
        }
    }

    @Override
    public void onSure(String payType,String purchasedType) {
        this.payType=payType;
        this.purchasedType=purchasedType;

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    //隐藏加载框
    public void dissWaitDailog() {
        requestTag++;
        if (requestTag >= 2) {
            if (mRefreshLayout.getState() == RefreshState.Refreshing) {
                mRefreshLayout.finishRefresh();
            } else {
                hideWaitDialog();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timeWheelUtils != null) {
            timeWheelUtils.dissTimeWheel();
            timeWheelUtils = null;
        }

        if (transactionScreenPop != null) {
            transactionScreenPop.onClean();
        }
    }

    public static TransactionDetailsFragment newIntance() {
        TransactionDetailsFragment transactionDetailsFragment = new TransactionDetailsFragment();
        return transactionDetailsFragment;
    }
}
