package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-15
 * 描    述：
 * ================================================
 */
public class AddDirModel {

    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-15 14:41:47
     * data : {"AccessoryUrl":"","SubAccessoryUrl":"~/App_Upload/Storage/Medicare_Ticket/48f3b8bd-5bd1-4dca-b185-e5d6c5ef6467/25dbc9aa-23b6-4266-a74a-aa7910db1594.jpg","CatalogueId":11,"CatalogueName":"测试五","AccessoryId":"ed5fdeff-cc03-49c2-918a-11711d4b943e","SubAccessoryId":"48f3b8bd-5bd1-4dca-b185-e5d6c5ef6467","fixmedins_code":"H61080200145","IsHomePage":1,"IsClassification":2,"Sort":50,"IsSystem":2}
     */

    private String code;
    private String msg;
    private String server_time;
    private PhaSortModel.DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public PhaSortModel.DataBean getData() {
        return data;
    }

    public void setData(PhaSortModel.DataBean data) {
        this.data = data;
    }

    //新增目录
    public static void sendAddCataRequest(final String TAG,String CatalogueName,Object AccessoryId,Object SubAccessoryId,String IsHomePage, String IsClassification,
                                          final CustomerJsonCallBack<AddDirModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CatalogueName", CatalogueName);//目录名称
        jsonObject.put("AccessoryId", null==AccessoryId?"":String.valueOf(AccessoryId));//图标唯一标识
        jsonObject.put("SubAccessoryId", null==SubAccessoryId?"":String.valueOf(SubAccessoryId));//横版图唯一标识
        jsonObject.put("IsHomePage", IsHomePage);//是否放首页（1是2不是）
        jsonObject.put("IsClassification", IsClassification);//是否放分类页（1是 2不是）
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_ADDCATALOGUE_URL, jsonObject.toJSONString(), callback);
    }

    //修改目录
    public static void sendUpdateCataRequest(final String TAG,String CatalogueId,String CatalogueName,Object AccessoryId,Object SubAccessoryId,String IsHomePage, String IsClassification,
                                          final CustomerJsonCallBack<AddDirModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CatalogueId", CatalogueId);//唯一标识
        jsonObject.put("CatalogueName", CatalogueName);//目录名称
        jsonObject.put("AccessoryId", null==AccessoryId?"":String.valueOf(AccessoryId));//图标唯一标识
        jsonObject.put("SubAccessoryId", null==SubAccessoryId?"":String.valueOf(SubAccessoryId));//横版图唯一标识
        jsonObject.put("IsHomePage", IsHomePage);//是否放首页（1是2不是）
        jsonObject.put("IsClassification", IsClassification);//是否放分类页（1是 2不是）
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_EDITCATALOGUE_URL, jsonObject.toJSONString(), callback);
    }
}
