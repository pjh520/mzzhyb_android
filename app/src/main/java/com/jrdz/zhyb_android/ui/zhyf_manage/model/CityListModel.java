package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.frame.compiler.widget.wheel.entity.IWheelEntity;

import java.util.List;

/**
 * ================================================
 * 项目名称：SuperWarehouse_Android
 * 包    名：com.tjl.super_warehouse.ui.mine.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2019/11/10 0010
 * 描    述：
 * ================================================
 */
public class CityListModel implements Parcelable ,IWheelEntity{
    /**
     * name : 北京市
     * code : 110000
     * city : [{"name":"市辖区","code":"110100","area":[{"name":"东城区","code":"110101"},{"name":"西城区","code":"110102"},{"name":"朝阳区","code":"110105"},{"name":"丰台区","code":"110106"},{"name":"石景山区","code":"110107"},{"name":"海淀区","code":"110108"},{"name":"门头沟区","code":"110109"},{"name":"房山区","code":"110111"},{"name":"通州区","code":"110112"},{"name":"顺义区","code":"110113"},{"name":"昌平区","code":"110114"},{"name":"大兴区","code":"110115"},{"name":"怀柔区","code":"110116"},{"name":"平谷区","code":"110117"},{"name":"密云区","code":"110118"},{"name":"延庆区","code":"110119"}]}]
     */

    private String name;
    private String code;
    private List<CityBean> city;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<CityBean> getCity() {
        return city;
    }

    public void setCity(List<CityBean> city) {
        this.city = city;
    }

    @Override
    public String getWheelText() {
        return name;
    }

    public static class CityBean implements Parcelable ,IWheelEntity{
        /**
         * name : 市辖区
         * code : 110100
         * area : [{"name":"东城区","code":"110101"},{"name":"西城区","code":"110102"},{"name":"朝阳区","code":"110105"},{"name":"丰台区","code":"110106"},{"name":"石景山区","code":"110107"},{"name":"海淀区","code":"110108"},{"name":"门头沟区","code":"110109"},{"name":"房山区","code":"110111"},{"name":"通州区","code":"110112"},{"name":"顺义区","code":"110113"},{"name":"昌平区","code":"110114"},{"name":"大兴区","code":"110115"},{"name":"怀柔区","code":"110116"},{"name":"平谷区","code":"110117"},{"name":"密云区","code":"110118"},{"name":"延庆区","code":"110119"}]
         */

        private String name;
        private String code;
        private List<AreaBean> area;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public List<AreaBean> getArea() {
            return area;
        }

        public void setArea(List<AreaBean> area) {
            this.area = area;
        }

        @Override
        public String getWheelText() {
            return name;
        }

        public static class AreaBean implements Parcelable,IWheelEntity {
            /**
             * name : 东城区
             * code : 110101
             */

            private String name;
            private String code;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.name);
                dest.writeString(this.code);
            }

            public AreaBean() {
            }

            protected AreaBean(Parcel in) {
                this.name = in.readString();
                this.code = in.readString();
            }

            public static final Creator<AreaBean> CREATOR = new Creator<AreaBean>() {
                @Override
                public AreaBean createFromParcel(Parcel source) {
                    return new AreaBean(source);
                }

                @Override
                public AreaBean[] newArray(int size) {
                    return new AreaBean[size];
                }
            };

            @Override
            public String getWheelText() {
                return name;
            }
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.name);
            dest.writeString(this.code);
            dest.writeTypedList(this.area);
        }

        public CityBean() {
        }

        protected CityBean(Parcel in) {
            this.name = in.readString();
            this.code = in.readString();
            this.area = in.createTypedArrayList(AreaBean.CREATOR);
        }

        public static final Creator<CityBean> CREATOR = new Creator<CityBean>() {
            @Override
            public CityBean createFromParcel(Parcel source) {
                return new CityBean(source);
            }

            @Override
            public CityBean[] newArray(int size) {
                return new CityBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.code);
        dest.writeTypedList(this.city);
    }

    public CityListModel() {
    }

    protected CityListModel(Parcel in) {
        this.name = in.readString();
        this.code = in.readString();
        this.city = in.createTypedArrayList(CityBean.CREATOR);
    }

    public static final Creator<CityListModel> CREATOR = new Creator<CityListModel>() {
        @Override
        public CityListModel createFromParcel(Parcel source) {
            return new CityListModel(source);
        }

        @Override
        public CityListModel[] newArray(int size) {
            return new CityListModel[size];
        }
    };


}
