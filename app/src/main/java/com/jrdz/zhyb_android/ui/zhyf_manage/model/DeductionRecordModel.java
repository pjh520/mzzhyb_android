package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-14
 * 描    述：
 * ================================================
 */
public class DeductionRecordModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-12-01 17:03:09
     * data : [{"DeductionRecordId":1,"SerialNumber":"20221201101024002","DeductionTime":"2022-12-01 10:10:24","DeductionAmount":10,"DeductionReason":"扣款","DeductionAccessoryId":"5fcbbd86-2187-40b7-800c-ff1914f9432b","DepositBalance":700,"CreateDT":"2022-12-01 10:10:24","UpdateDT":"2022-12-01 10:10:24","CreateUser":"99","UpdateUser":"99","UserId":"5fcbbd86-2187-40b7-800c-ff1914f9432b","UserName":"99","fixmedins_code":"H61080200145"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * DeductionRecordId : 1
         * SerialNumber : 20221201101024002
         * DeductionTime : 2022-12-01 10:10:24
         * DeductionAmount : 10
         * DeductionReason : 扣款
         * DeductionAccessoryId : 5fcbbd86-2187-40b7-800c-ff1914f9432b
         * DepositBalance : 700
         * CreateDT : 2022-12-01 10:10:24
         * UpdateDT : 2022-12-01 10:10:24
         * CreateUser : 99
         * UpdateUser : 99
         * UserId : 5fcbbd86-2187-40b7-800c-ff1914f9432b
         * UserName : 99
         * fixmedins_code : H61080200145
         */

        private String DeductionRecordId;
        private String SerialNumber;
        private String DeductionTime;
        private String DeductionAmount;
        private String DeductionReason;
        private String DeductionAccessoryId;
        private String DepositBalance;
        private String UserName;

        public String getDeductionRecordId() {
            return DeductionRecordId;
        }

        public void setDeductionRecordId(String DeductionRecordId) {
            this.DeductionRecordId = DeductionRecordId;
        }

        public String getSerialNumber() {
            return SerialNumber;
        }

        public void setSerialNumber(String SerialNumber) {
            this.SerialNumber = SerialNumber;
        }

        public String getDeductionTime() {
            return DeductionTime;
        }

        public void setDeductionTime(String DeductionTime) {
            this.DeductionTime = DeductionTime;
        }

        public String getDeductionAmount() {
            return DeductionAmount;
        }

        public void setDeductionAmount(String DeductionAmount) {
            this.DeductionAmount = DeductionAmount;
        }

        public String getDeductionReason() {
            return DeductionReason;
        }

        public void setDeductionReason(String DeductionReason) {
            this.DeductionReason = DeductionReason;
        }

        public String getDeductionAccessoryId() {
            return DeductionAccessoryId;
        }

        public void setDeductionAccessoryId(String DeductionAccessoryId) {
            this.DeductionAccessoryId = DeductionAccessoryId;
        }

        public String getDepositBalance() {
            return DepositBalance;
        }

        public void setDepositBalance(String DepositBalance) {
            this.DepositBalance = DepositBalance;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }
    }

    //扣款记录列表
    public static void sendDeductionRecordRequest(final String TAG, String pageindex, String pagesize,final CustomerJsonCallBack<DeductionRecordModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_DEDUCTIONRECORDLIST_URL, jsonObject.toJSONString(), callback);
    }
}
