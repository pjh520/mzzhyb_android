package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-15
 * 描    述：
 * ================================================
 */
public class DirDefaultIconModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-15 12:09:14
     * data : {"DefaultIconAccessoryId":"ED5FDEFF-CC03-49C2-918A-11711D4B943E","DefaultIconAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/CRM_Notice/defaulticon.png"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * DefaultIconAccessoryId : ED5FDEFF-CC03-49C2-918A-11711D4B943E
         * DefaultIconAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/CRM_Notice/defaulticon.png
         */

        private String DefaultIconAccessoryId;
        private String DefaultIconAccessoryUrl;

        public String getDefaultIconAccessoryId() {
            return DefaultIconAccessoryId;
        }

        public void setDefaultIconAccessoryId(String DefaultIconAccessoryId) {
            this.DefaultIconAccessoryId = DefaultIconAccessoryId;
        }

        public String getDefaultIconAccessoryUrl() {
            return DefaultIconAccessoryUrl;
        }

        public void setDefaultIconAccessoryUrl(String DefaultIconAccessoryUrl) {
            this.DefaultIconAccessoryUrl = DefaultIconAccessoryUrl;
        }
    }

    //获取默认图标
    public static void sendDefaultIconRequest(final String TAG,final CustomerJsonCallBack<DirDefaultIconModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_GETDEFAULTICON_URL,"", callback);
    }
}
