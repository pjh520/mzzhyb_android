package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-05
 * 描    述：
 * ================================================
 */
public class DistributionScopeModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-12-05 10:43:55
     * data : {"DistributionScope":"商家自配（20km内）"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    //查询店铺配送范围
    public static void sendDistributionScopeRequest(final String TAG,final CustomerJsonCallBack<DistributionScopeModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_QUERYDISTRIBUTIONSCOPE_URL, "", callback);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * DistributionScope : 商家自配（20km内）
         */

        private String DistributionScope;

        public String getDistributionScope() {
            return DistributionScope;
        }

        public void setDistributionScope(String DistributionScope) {
            this.DistributionScope = DistributionScope;
        }
    }
}
