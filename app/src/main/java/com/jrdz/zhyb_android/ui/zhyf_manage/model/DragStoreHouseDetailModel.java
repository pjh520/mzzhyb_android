package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-02-06
 * 描    述：
 * ================================================
 */
public class DragStoreHouseDetailModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-10 16:46:58
     * totalItems : 2
     * data : [{"InstructionsAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/82b3622d-9a1d-4bcd-ab88-a9329a0743d4/c0349afc-6e33-4cd1-b0b2-0e74ffa9974b.jpg","DetailAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/173f6d1f-b1c1-428f-b65e-e88953cec239/4095a596-31d7-48ef-8858-9e05e44e200f.jpg","DetailAccessoryUrl2":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/00a733ed-11e6-492f-84cc-d493b1de1d54/923ef641-6c6c-4a05-9aa4-394a858731cc.jpg","DetailAccessoryUrl3":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/830e2f9a-b765-43ad-8a8d-defdb744b9b3/1304062b-5c9f-4fe7-a261-7e32a6087fa0.jpg","DetailAccessoryUrl4":"","DetailAccessoryUrl5":"","DrugDataId":1,"MIC_Code":"","ItemCode":"H61080200145202211101545560001","ItemName":"苯丙酮尿症2","ItemType":1,"ApprovalNumber":"国药准字H20193280-1111","RegDosform":"胶囊剂","Spec":"100mg","EnterpriseName":"通药制药集团股份有限公司","EfccAtd":"功能主治1111","UsualWay":"常见用法1111","StorageConditions":"贮存条件1111","ExpiryDateCount":12,"InventoryQuantity":5,"Price":1,"InstructionsAccessoryId":"82b3622d-9a1d-4bcd-ab88-a9329a0743d4","DetailAccessoryId1":"173f6d1f-b1c1-428f-b65e-e88953cec239","DetailAccessoryId2":"00a733ed-11e6-492f-84cc-d493b1de1d54","DetailAccessoryId3":"830e2f9a-b765-43ad-8a8d-defdb744b9b3","DetailAccessoryId4":"00000000-0000-0000-0000-000000000000","DetailAccessoryId5":"00000000-0000-0000-0000-000000000000","RegistrationCertificateNo":"","ProductionLicenseNo":"","Brand":"","Packaging":"","ApplicableScope":"","UsageDosage":"","Precautions":"","Usagemethod":"","IsPlatformDrug":2,"CreateDT":"2022-11-10 15:45:56","UpdateDT":"2022-11-10 15:45:56","CreateUser":"99","UpdateUser":"99","fixmedins_code":"H61080200145","DrugsClassification":1,"DrugsClassificationName":"西药中成药","AssociatedDiseases":"盗汗咳血肺结核，经证实的,"},{"InstructionsAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/3eb4dfdc-f203-4b3b-8530-67aae24e9c03/a2c27b1c-26d0-4c71-83fa-45ee565fbd65.jpg","DetailAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/9cbd529e-33a6-4a77-8483-8e1499bbe09f/793539ef-ef52-4340-b80d-780f1a344823.jpg","DetailAccessoryUrl2":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/19a1250e-1005-4121-8bf7-4c9b7716531 2022-11-10 16:46:59.035 25864-26653/com.jrdz.zhyb_android E/LogUtils: 2/a797ad11-f912-4a51-aefd-4c071e289ccb.jpg","DetailAccessoryUrl3":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/80615713-e479-4e28-a7b3-a17d5e7ae9f6/007b99e7-5ac7-4a09-8bc8-e7dbd206daca.jpg","DetailAccessoryUrl4":"","DetailAccessoryUrl5":"","DrugDataId":2,"MIC_Code":"","ItemCode":"H61080200145202211101612140001","ItemName":"苯丙酮尿症3","ItemType":1,"ApprovalNumber":"国药准字H20193280-1111","RegDosform":"胶囊剂","Spec":"100mg","EnterpriseName":"通药制药集团股份有限公司","EfccAtd":"功能主治1111","UsualWay":"常见用法1111","StorageConditions":"贮存条件1111","ExpiryDateCount":12,"InventoryQuantity":5,"Price":1,"InstructionsAccessoryId":"3eb4dfdc-f203-4b3b-8530-67aae24e9c03","DetailAccessoryId1":"9cbd529e-33a6-4a77-8483-8e1499bbe09f","DetailAccessoryId2":"19a1250e-1005-4121-8bf7-4c9b77165312","DetailAccessoryId3":"80615713-e479-4e28-a7b3-a17d5e7ae9f6","DetailAccessoryId4":"00000000-0000-0000-0000-000000000000","DetailAccessoryId5":"00000000-0000-0000-0000-000000000000","RegistrationCertificateNo":"","ProductionLicenseNo":"","Brand":"","Packaging":"","ApplicableScope":"","UsageDosage":"","Precautions":"","Usagemethod":"","IsPlatformDrug":2,"CreateDT":"2022-11-10 16:12:14","UpdateDT":"2022-11-10 16:12:14","CreateUser":"99","UpdateUser":"99","fixmedins_code":"H61080200145","DrugsClassification":1,"DrugsClassificationName":"西药中成药","AssociatedDiseases":"[\"盗汗\",\"咳血\",\"肺结核，经证实的\"]"}]
     */
    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private DragStoreHouseModel.DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public DragStoreHouseModel.DataBean getData() {
        return data;
    }

    public void setData(DragStoreHouseModel.DataBean data) {
        this.data = data;
    }


    //获取药品库数据详情
    public static void sendPlatformDragDetailRequest(final String TAG, String ItemCode,
                                                   final CustomerJsonCallBack<DragStoreHouseDetailModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ItemCode", ItemCode);//药品编码

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_DETAILDRUGDATA_URL, jsonObject.toJSONString(), callback);
    }
}
