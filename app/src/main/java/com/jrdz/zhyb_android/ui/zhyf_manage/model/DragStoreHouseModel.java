package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-18
 * 描    述：
 * ================================================
 */
public class DragStoreHouseModel implements Parcelable {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-10 16:46:58
     * totalItems : 2
     * data : [{"InstructionsAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/82b3622d-9a1d-4bcd-ab88-a9329a0743d4/c0349afc-6e33-4cd1-b0b2-0e74ffa9974b.jpg","DetailAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/173f6d1f-b1c1-428f-b65e-e88953cec239/4095a596-31d7-48ef-8858-9e05e44e200f.jpg","DetailAccessoryUrl2":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/00a733ed-11e6-492f-84cc-d493b1de1d54/923ef641-6c6c-4a05-9aa4-394a858731cc.jpg","DetailAccessoryUrl3":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/830e2f9a-b765-43ad-8a8d-defdb744b9b3/1304062b-5c9f-4fe7-a261-7e32a6087fa0.jpg","DetailAccessoryUrl4":"","DetailAccessoryUrl5":"","DrugDataId":1,"MIC_Code":"","ItemCode":"H61080200145202211101545560001","ItemName":"苯丙酮尿症2","ItemType":1,"ApprovalNumber":"国药准字H20193280-1111","RegDosform":"胶囊剂","Spec":"100mg","EnterpriseName":"通药制药集团股份有限公司","EfccAtd":"功能主治1111","UsualWay":"常见用法1111","StorageConditions":"贮存条件1111","ExpiryDateCount":12,"InventoryQuantity":5,"Price":1,"InstructionsAccessoryId":"82b3622d-9a1d-4bcd-ab88-a9329a0743d4","DetailAccessoryId1":"173f6d1f-b1c1-428f-b65e-e88953cec239","DetailAccessoryId2":"00a733ed-11e6-492f-84cc-d493b1de1d54","DetailAccessoryId3":"830e2f9a-b765-43ad-8a8d-defdb744b9b3","DetailAccessoryId4":"00000000-0000-0000-0000-000000000000","DetailAccessoryId5":"00000000-0000-0000-0000-000000000000","RegistrationCertificateNo":"","ProductionLicenseNo":"","Brand":"","Packaging":"","ApplicableScope":"","UsageDosage":"","Precautions":"","Usagemethod":"","IsPlatformDrug":2,"CreateDT":"2022-11-10 15:45:56","UpdateDT":"2022-11-10 15:45:56","CreateUser":"99","UpdateUser":"99","fixmedins_code":"H61080200145","DrugsClassification":1,"DrugsClassificationName":"西药中成药","AssociatedDiseases":"盗汗咳血肺结核，经证实的,"},{"InstructionsAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/3eb4dfdc-f203-4b3b-8530-67aae24e9c03/a2c27b1c-26d0-4c71-83fa-45ee565fbd65.jpg","DetailAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/9cbd529e-33a6-4a77-8483-8e1499bbe09f/793539ef-ef52-4340-b80d-780f1a344823.jpg","DetailAccessoryUrl2":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/19a1250e-1005-4121-8bf7-4c9b7716531 2022-11-10 16:46:59.035 25864-26653/com.jrdz.zhyb_android E/LogUtils: 2/a797ad11-f912-4a51-aefd-4c071e289ccb.jpg","DetailAccessoryUrl3":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/80615713-e479-4e28-a7b3-a17d5e7ae9f6/007b99e7-5ac7-4a09-8bc8-e7dbd206daca.jpg","DetailAccessoryUrl4":"","DetailAccessoryUrl5":"","DrugDataId":2,"MIC_Code":"","ItemCode":"H61080200145202211101612140001","ItemName":"苯丙酮尿症3","ItemType":1,"ApprovalNumber":"国药准字H20193280-1111","RegDosform":"胶囊剂","Spec":"100mg","EnterpriseName":"通药制药集团股份有限公司","EfccAtd":"功能主治1111","UsualWay":"常见用法1111","StorageConditions":"贮存条件1111","ExpiryDateCount":12,"InventoryQuantity":5,"Price":1,"InstructionsAccessoryId":"3eb4dfdc-f203-4b3b-8530-67aae24e9c03","DetailAccessoryId1":"9cbd529e-33a6-4a77-8483-8e1499bbe09f","DetailAccessoryId2":"19a1250e-1005-4121-8bf7-4c9b77165312","DetailAccessoryId3":"80615713-e479-4e28-a7b3-a17d5e7ae9f6","DetailAccessoryId4":"00000000-0000-0000-0000-000000000000","DetailAccessoryId5":"00000000-0000-0000-0000-000000000000","RegistrationCertificateNo":"","ProductionLicenseNo":"","Brand":"","Packaging":"","ApplicableScope":"","UsageDosage":"","Precautions":"","Usagemethod":"","IsPlatformDrug":2,"CreateDT":"2022-11-10 16:12:14","UpdateDT":"2022-11-10 16:12:14","CreateUser":"99","UpdateUser":"99","fixmedins_code":"H61080200145","DrugsClassification":1,"DrugsClassificationName":"西药中成药","AssociatedDiseases":"[\"盗汗\",\"咳血\",\"肺结核，经证实的\"]"}]
     */
    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * InstructionsAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/82b3622d-9a1d-4bcd-ab88-a9329a0743d4/c0349afc-6e33-4cd1-b0b2-0e74ffa9974b.jpg
         * DetailAccessoryUrl1 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/173f6d1f-b1c1-428f-b65e-e88953cec239/4095a596-31d7-48ef-8858-9e05e44e200f.jpg
         * DetailAccessoryUrl2 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/00a733ed-11e6-492f-84cc-d493b1de1d54/923ef641-6c6c-4a05-9aa4-394a858731cc.jpg
         * DetailAccessoryUrl3 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/830e2f9a-b765-43ad-8a8d-defdb744b9b3/1304062b-5c9f-4fe7-a261-7e32a6087fa0.jpg
         * DetailAccessoryUrl4 :
         * DetailAccessoryUrl5 :
         * DrugDataId : 1
         * MIC_Code :
         * ItemCode : H61080200145202211101545560001
         * ItemName : 苯丙酮尿症2
         * ItemType : 1
         * ApprovalNumber : 国药准字H20193280-1111
         * RegDosform : 胶囊剂
         * Spec : 100mg
         * EnterpriseName : 通药制药集团股份有限公司
         * EfccAtd : 功能主治1111
         * UsualWay : 常见用法1111
         * StorageConditions : 贮存条件1111
         * ExpiryDateCount : 12
         * InventoryQuantity : 5
         * Price : 1
         * InstructionsAccessoryId : 82b3622d-9a1d-4bcd-ab88-a9329a0743d4
         * DetailAccessoryId1 : 173f6d1f-b1c1-428f-b65e-e88953cec239
         * DetailAccessoryId2 : 00a733ed-11e6-492f-84cc-d493b1de1d54
         * DetailAccessoryId3 : 830e2f9a-b765-43ad-8a8d-defdb744b9b3
         * DetailAccessoryId4 : 00000000-0000-0000-0000-000000000000
         * DetailAccessoryId5 : 00000000-0000-0000-0000-000000000000
         * RegistrationCertificateNo :
         * ProductionLicenseNo :
         * Brand :
         * Packaging :
         * ApplicableScope :
         * UsageDosage :
         * Precautions :
         * Usagemethod :
         * IsPlatformDrug : 2
         * CreateDT : 2022-11-10 15:45:56
         * UpdateDT : 2022-11-10 15:45:56
         * CreateUser : 99
         * UpdateUser : 99
         * fixmedins_code : H61080200145
         * DrugsClassification : 1
         * DrugsClassificationName : 西药中成药
         * AssociatedDiseases : 盗汗咳血肺结核，经证实的,
         */
        private String InstructionsAccessoryUrl;
        private String DetailAccessoryUrl1;
        private String DetailAccessoryUrl2;
        private String DetailAccessoryUrl3;
        private String DetailAccessoryUrl4;
        private String DetailAccessoryUrl5;
        private String DrugDataId;
        private String MIC_Code;
        private String ItemCode;
        private String ItemName;
        private String GoodsName;
        private String ItemType;
        private String ApprovalNumber;
        private String RegDosform;
        private String Spec;
        private String EnterpriseName;
        private String EfccAtd;
        private String UsualWay;
        private String StorageConditions;
        private String ExpiryDateCount;
        private String InventoryQuantity;
        private String Price;
        private String InstructionsAccessoryId;
        private String DetailAccessoryId1;
        private String DetailAccessoryId2;
        private String DetailAccessoryId3;
        private String DetailAccessoryId4;
        private String DetailAccessoryId5;
        private String RegistrationCertificateNo;
        private String ProductionLicenseNo;
        private String Brand;
        private String Packaging;
        private String ApplicableScope;
        private String UsageDosage;
        private String Precautions;
        private String Usagemethod;
        private String IsPlatformDrug;
        private String DrugsClassification;
        private String DrugsClassificationName;
        private String AssociatedDiseases;
        private String AssociatedDiagnostics;
        private String IsPromotion;
        //banner附件标识1
        public String BannerAccessoryId1;
        //banner附件标识2
        public String BannerAccessoryId2;
        //banner附件标识3
        public String BannerAccessoryId3;
        //banner附件1
        public String BannerAccessoryUrl1;
        //banner附件2
        public String BannerAccessoryUrl2;
        //banner附件3
        public String BannerAccessoryUrl3;

        public DataBean(String instructionsAccessoryUrl, String detailAccessoryUrl1, String detailAccessoryUrl2, String detailAccessoryUrl3,
                        String detailAccessoryUrl4, String detailAccessoryUrl5, String drugDataId, String MIC_Code, String itemCode,
                        String itemName,String GoodsName, String itemType, String approvalNumber, String regDosform, String spec, String enterpriseName,
                        String efccAtd, String usualWay, String storageConditions, String expiryDateCount, String inventoryQuantity,
                        String price, String instructionsAccessoryId, String detailAccessoryId1, String detailAccessoryId2, String detailAccessoryId3,
                        String detailAccessoryId4, String detailAccessoryId5, String registrationCertificateNo, String productionLicenseNo, String brand,
                        String packaging, String applicableScope, String usageDosage, String precautions, String usagemethod, String isPlatformDrug,
                        String drugsClassification, String drugsClassificationName, String associatedDiseases, String associatedDiagnostics,String isPromotion) {
            InstructionsAccessoryUrl = instructionsAccessoryUrl;
            DetailAccessoryUrl1 = detailAccessoryUrl1;
            DetailAccessoryUrl2 = detailAccessoryUrl2;
            DetailAccessoryUrl3 = detailAccessoryUrl3;
            DetailAccessoryUrl4 = detailAccessoryUrl4;
            DetailAccessoryUrl5 = detailAccessoryUrl5;
            DrugDataId = drugDataId;
            this.MIC_Code = MIC_Code;
            ItemCode = itemCode;
            ItemName = itemName;
            this.GoodsName= GoodsName;
            ItemType = itemType;
            ApprovalNumber = approvalNumber;
            RegDosform = regDosform;
            Spec = spec;
            EnterpriseName = enterpriseName;
            EfccAtd = efccAtd;
            UsualWay = usualWay;
            StorageConditions = storageConditions;
            ExpiryDateCount = expiryDateCount;
            InventoryQuantity = inventoryQuantity;
            Price = price;
            InstructionsAccessoryId = instructionsAccessoryId;
            DetailAccessoryId1 = detailAccessoryId1;
            DetailAccessoryId2 = detailAccessoryId2;
            DetailAccessoryId3 = detailAccessoryId3;
            DetailAccessoryId4 = detailAccessoryId4;
            DetailAccessoryId5 = detailAccessoryId5;
            RegistrationCertificateNo = registrationCertificateNo;
            ProductionLicenseNo = productionLicenseNo;
            Brand = brand;
            Packaging = packaging;
            ApplicableScope = applicableScope;
            UsageDosage = usageDosage;
            Precautions = precautions;
            Usagemethod = usagemethod;
            IsPlatformDrug = isPlatformDrug;
            DrugsClassification = drugsClassification;
            DrugsClassificationName = drugsClassificationName;
            AssociatedDiseases = associatedDiseases;
            AssociatedDiagnostics = associatedDiagnostics;
            IsPromotion= isPromotion;
        }

        public String getInstructionsAccessoryUrl() {
            return InstructionsAccessoryUrl;
        }

        public void setInstructionsAccessoryUrl(String InstructionsAccessoryUrl) {
            this.InstructionsAccessoryUrl = InstructionsAccessoryUrl;
        }

        public String getDetailAccessoryUrl1() {
            return DetailAccessoryUrl1;
        }

        public void setDetailAccessoryUrl1(String DetailAccessoryUrl1) {
            this.DetailAccessoryUrl1 = DetailAccessoryUrl1;
        }

        public String getDetailAccessoryUrl2() {
            return DetailAccessoryUrl2;
        }

        public void setDetailAccessoryUrl2(String DetailAccessoryUrl2) {
            this.DetailAccessoryUrl2 = DetailAccessoryUrl2;
        }

        public String getDetailAccessoryUrl3() {
            return DetailAccessoryUrl3;
        }

        public void setDetailAccessoryUrl3(String DetailAccessoryUrl3) {
            this.DetailAccessoryUrl3 = DetailAccessoryUrl3;
        }

        public String getDetailAccessoryUrl4() {
            return DetailAccessoryUrl4;
        }

        public void setDetailAccessoryUrl4(String DetailAccessoryUrl4) {
            this.DetailAccessoryUrl4 = DetailAccessoryUrl4;
        }

        public String getDetailAccessoryUrl5() {
            return DetailAccessoryUrl5;
        }

        public void setDetailAccessoryUrl5(String DetailAccessoryUrl5) {
            this.DetailAccessoryUrl5 = DetailAccessoryUrl5;
        }

        public String getDrugDataId() {
            return DrugDataId;
        }

        public void setDrugDataId(String DrugDataId) {
            this.DrugDataId = DrugDataId;
        }

        public String getMIC_Code() {
            return MIC_Code;
        }

        public void setMIC_Code(String MIC_Code) {
            this.MIC_Code = MIC_Code;
        }

        public String getItemCode() {
            return ItemCode;
        }

        public void setItemCode(String ItemCode) {
            this.ItemCode = ItemCode;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String ItemName) {
            this.ItemName = ItemName;
        }

        public String getGoodsName() {
            return GoodsName;
        }

        public void setGoodsName(String goodsName) {
            GoodsName = goodsName;
        }

        public String getItemType() {
            return ItemType;
        }

        public void setItemType(String ItemType) {
            this.ItemType = ItemType;
        }

        public String getApprovalNumber() {
            return ApprovalNumber;
        }

        public void setApprovalNumber(String ApprovalNumber) {
            this.ApprovalNumber = ApprovalNumber;
        }

        public String getRegDosform() {
            return RegDosform;
        }

        public void setRegDosform(String RegDosform) {
            this.RegDosform = RegDosform;
        }

        public String getSpec() {
            return Spec;
        }

        public void setSpec(String Spec) {
            this.Spec = Spec;
        }

        public String getEnterpriseName() {
            return EnterpriseName;
        }

        public void setEnterpriseName(String EnterpriseName) {
            this.EnterpriseName = EnterpriseName;
        }

        public String getEfccAtd() {
            return EfccAtd;
        }

        public void setEfccAtd(String EfccAtd) {
            this.EfccAtd = EfccAtd;
        }

        public String getUsualWay() {
            return UsualWay;
        }

        public void setUsualWay(String UsualWay) {
            this.UsualWay = UsualWay;
        }

        public String getStorageConditions() {
            return StorageConditions;
        }

        public void setStorageConditions(String StorageConditions) {
            this.StorageConditions = StorageConditions;
        }

        public String getExpiryDateCount() {
            return ExpiryDateCount;
        }

        public void setExpiryDateCount(String ExpiryDateCount) {
            this.ExpiryDateCount = ExpiryDateCount;
        }

        public String getInventoryQuantity() {
            return InventoryQuantity;
        }

        public void setInventoryQuantity(String InventoryQuantity) {
            this.InventoryQuantity = InventoryQuantity;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String Price) {
            this.Price = Price;
        }

        public String getInstructionsAccessoryId() {
            return InstructionsAccessoryId;
        }

        public void setInstructionsAccessoryId(String InstructionsAccessoryId) {
            this.InstructionsAccessoryId = InstructionsAccessoryId;
        }

        public String getDetailAccessoryId1() {
            return DetailAccessoryId1;
        }

        public void setDetailAccessoryId1(String DetailAccessoryId1) {
            this.DetailAccessoryId1 = DetailAccessoryId1;
        }

        public String getDetailAccessoryId2() {
            return DetailAccessoryId2;
        }

        public void setDetailAccessoryId2(String DetailAccessoryId2) {
            this.DetailAccessoryId2 = DetailAccessoryId2;
        }

        public String getDetailAccessoryId3() {
            return DetailAccessoryId3;
        }

        public void setDetailAccessoryId3(String DetailAccessoryId3) {
            this.DetailAccessoryId3 = DetailAccessoryId3;
        }

        public String getDetailAccessoryId4() {
            return DetailAccessoryId4;
        }

        public void setDetailAccessoryId4(String DetailAccessoryId4) {
            this.DetailAccessoryId4 = DetailAccessoryId4;
        }

        public String getDetailAccessoryId5() {
            return DetailAccessoryId5;
        }

        public void setDetailAccessoryId5(String DetailAccessoryId5) {
            this.DetailAccessoryId5 = DetailAccessoryId5;
        }

        public String getRegistrationCertificateNo() {
            return RegistrationCertificateNo;
        }

        public void setRegistrationCertificateNo(String RegistrationCertificateNo) {
            this.RegistrationCertificateNo = RegistrationCertificateNo;
        }

        public String getProductionLicenseNo() {
            return ProductionLicenseNo;
        }

        public void setProductionLicenseNo(String ProductionLicenseNo) {
            this.ProductionLicenseNo = ProductionLicenseNo;
        }

        public String getBrand() {
            return Brand;
        }

        public void setBrand(String Brand) {
            this.Brand = Brand;
        }

        public String getPackaging() {
            return Packaging;
        }

        public void setPackaging(String Packaging) {
            this.Packaging = Packaging;
        }

        public String getApplicableScope() {
            return ApplicableScope;
        }

        public void setApplicableScope(String ApplicableScope) {
            this.ApplicableScope = ApplicableScope;
        }

        public String getUsageDosage() {
            return UsageDosage;
        }

        public void setUsageDosage(String UsageDosage) {
            this.UsageDosage = UsageDosage;
        }

        public String getPrecautions() {
            return Precautions;
        }

        public void setPrecautions(String Precautions) {
            this.Precautions = Precautions;
        }

        public String getUsagemethod() {
            return Usagemethod;
        }

        public void setUsagemethod(String Usagemethod) {
            this.Usagemethod = Usagemethod;
        }

        public String getIsPlatformDrug() {
            return IsPlatformDrug;
        }

        public void setIsPlatformDrug(String IsPlatformDrug) {
            this.IsPlatformDrug = IsPlatformDrug;
        }

        public String getDrugsClassification() {
            return DrugsClassification;
        }

        public void setDrugsClassification(String DrugsClassification) {
            this.DrugsClassification = DrugsClassification;
        }

        public String getDrugsClassificationName() {
            return DrugsClassificationName;
        }

        public void setDrugsClassificationName(String DrugsClassificationName) {
            this.DrugsClassificationName = DrugsClassificationName;
        }

        public String getAssociatedDiseases() {
            return AssociatedDiseases;
        }

        public void setAssociatedDiseases(String AssociatedDiseases) {
            this.AssociatedDiseases = AssociatedDiseases;
        }

        public String getAssociatedDiagnostics() {
            return AssociatedDiagnostics;
        }

        public void setAssociatedDiagnostics(String associatedDiagnostics) {
            AssociatedDiagnostics = associatedDiagnostics;
        }

        public String getIsPromotion() {
            return IsPromotion;
        }

        public void setIsPromotion(String isPromotion) {
            IsPromotion = isPromotion;
        }

        public String getBannerAccessoryId1() {
            return BannerAccessoryId1;
        }

        public void setBannerAccessoryId1(String bannerAccessoryId1) {
            BannerAccessoryId1 = bannerAccessoryId1;
        }

        public String getBannerAccessoryId2() {
            return BannerAccessoryId2;
        }

        public void setBannerAccessoryId2(String bannerAccessoryId2) {
            BannerAccessoryId2 = bannerAccessoryId2;
        }

        public String getBannerAccessoryId3() {
            return BannerAccessoryId3;
        }

        public void setBannerAccessoryId3(String bannerAccessoryId3) {
            BannerAccessoryId3 = bannerAccessoryId3;
        }

        public String getBannerAccessoryUrl1() {
            return BannerAccessoryUrl1;
        }

        public void setBannerAccessoryUrl1(String bannerAccessoryUrl1) {
            BannerAccessoryUrl1 = bannerAccessoryUrl1;
        }

        public String getBannerAccessoryUrl2() {
            return BannerAccessoryUrl2;
        }

        public void setBannerAccessoryUrl2(String bannerAccessoryUrl2) {
            BannerAccessoryUrl2 = bannerAccessoryUrl2;
        }

        public String getBannerAccessoryUrl3() {
            return BannerAccessoryUrl3;
        }

        public void setBannerAccessoryUrl3(String bannerAccessoryUrl3) {
            BannerAccessoryUrl3 = bannerAccessoryUrl3;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.InstructionsAccessoryUrl);
            dest.writeString(this.DetailAccessoryUrl1);
            dest.writeString(this.DetailAccessoryUrl2);
            dest.writeString(this.DetailAccessoryUrl3);
            dest.writeString(this.DetailAccessoryUrl4);
            dest.writeString(this.DetailAccessoryUrl5);
            dest.writeString(this.DrugDataId);
            dest.writeString(this.MIC_Code);
            dest.writeString(this.ItemCode);
            dest.writeString(this.ItemName);
            dest.writeString(this.GoodsName);
            dest.writeString(this.ItemType);
            dest.writeString(this.ApprovalNumber);
            dest.writeString(this.RegDosform);
            dest.writeString(this.Spec);
            dest.writeString(this.EnterpriseName);
            dest.writeString(this.EfccAtd);
            dest.writeString(this.UsualWay);
            dest.writeString(this.StorageConditions);
            dest.writeString(this.ExpiryDateCount);
            dest.writeString(this.InventoryQuantity);
            dest.writeString(this.Price);
            dest.writeString(this.InstructionsAccessoryId);
            dest.writeString(this.DetailAccessoryId1);
            dest.writeString(this.DetailAccessoryId2);
            dest.writeString(this.DetailAccessoryId3);
            dest.writeString(this.DetailAccessoryId4);
            dest.writeString(this.DetailAccessoryId5);
            dest.writeString(this.RegistrationCertificateNo);
            dest.writeString(this.ProductionLicenseNo);
            dest.writeString(this.Brand);
            dest.writeString(this.Packaging);
            dest.writeString(this.ApplicableScope);
            dest.writeString(this.UsageDosage);
            dest.writeString(this.Precautions);
            dest.writeString(this.Usagemethod);
            dest.writeString(this.IsPlatformDrug);
            dest.writeString(this.DrugsClassification);
            dest.writeString(this.DrugsClassificationName);
            dest.writeString(this.AssociatedDiseases);
            dest.writeString(this.AssociatedDiagnostics);
            dest.writeString(this.IsPromotion);
            dest.writeString(this.BannerAccessoryId1);
            dest.writeString(this.BannerAccessoryId2);
            dest.writeString(this.BannerAccessoryId3);
            dest.writeString(this.BannerAccessoryUrl1);
            dest.writeString(this.BannerAccessoryUrl2);
            dest.writeString(this.BannerAccessoryUrl3);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.InstructionsAccessoryUrl = in.readString();
            this.DetailAccessoryUrl1 = in.readString();
            this.DetailAccessoryUrl2 = in.readString();
            this.DetailAccessoryUrl3 = in.readString();
            this.DetailAccessoryUrl4 = in.readString();
            this.DetailAccessoryUrl5 = in.readString();
            this.DrugDataId = in.readString();
            this.MIC_Code = in.readString();
            this.ItemCode = in.readString();
            this.ItemName = in.readString();
            this.GoodsName = in.readString();
            this.ItemType = in.readString();
            this.ApprovalNumber = in.readString();
            this.RegDosform = in.readString();
            this.Spec = in.readString();
            this.EnterpriseName = in.readString();
            this.EfccAtd = in.readString();
            this.UsualWay = in.readString();
            this.StorageConditions = in.readString();
            this.ExpiryDateCount = in.readString();
            this.InventoryQuantity = in.readString();
            this.Price = in.readString();
            this.InstructionsAccessoryId = in.readString();
            this.DetailAccessoryId1 = in.readString();
            this.DetailAccessoryId2 = in.readString();
            this.DetailAccessoryId3 = in.readString();
            this.DetailAccessoryId4 = in.readString();
            this.DetailAccessoryId5 = in.readString();
            this.RegistrationCertificateNo = in.readString();
            this.ProductionLicenseNo = in.readString();
            this.Brand = in.readString();
            this.Packaging = in.readString();
            this.ApplicableScope = in.readString();
            this.UsageDosage = in.readString();
            this.Precautions = in.readString();
            this.Usagemethod = in.readString();
            this.IsPlatformDrug = in.readString();
            this.DrugsClassification = in.readString();
            this.DrugsClassificationName = in.readString();
            this.AssociatedDiseases = in.readString();
            this.AssociatedDiagnostics = in.readString();
            this.IsPromotion = in.readString();
            this.BannerAccessoryId1 = in.readString();
            this.BannerAccessoryId2 = in.readString();
            this.BannerAccessoryId3 = in.readString();
            this.BannerAccessoryUrl1 = in.readString();
            this.BannerAccessoryUrl2 = in.readString();
            this.BannerAccessoryUrl3 = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.code);
        dest.writeString(this.msg);
        dest.writeString(this.server_time);
        dest.writeString(this.totalItems);
        dest.writeTypedList(this.data);
    }

    public DragStoreHouseModel() {
    }

    protected DragStoreHouseModel(Parcel in) {
        this.code = in.readString();
        this.msg = in.readString();
        this.server_time = in.readString();
        this.totalItems = in.readString();
        this.data = in.createTypedArrayList(DataBean.CREATOR);
    }

    public static final Parcelable.Creator<DragStoreHouseModel> CREATOR = new Parcelable.Creator<DragStoreHouseModel>() {
        @Override
        public DragStoreHouseModel createFromParcel(Parcel source) {
            return new DragStoreHouseModel(source);
        }

        @Override
        public DragStoreHouseModel[] newArray(int size) {
            return new DragStoreHouseModel[size];
        }
    };

    //获取平台药品库数据
    public static void sendPlatformDragListRequest(final String TAG, String name,String barCode,String pageindex, String pagesize,
                                                   final CustomerJsonCallBack<DragStoreHouseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", name);
        jsonObject.put("barCode", barCode);
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_PDRUGDATA_LIST_URL, jsonObject.toJSONString(), callback);
    }

    //获取店铺药品库数据
    public static void sendShopDragListRequest(final String TAG, String name,String barCode,String DrugsClassification, String pageindex, String pagesize,
                                               final CustomerJsonCallBack<DragStoreHouseModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", name);
        jsonObject.put("barCode", barCode);
        jsonObject.put("DrugsClassification", DrugsClassification);
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_PSTOREDRUGDATA_LIST_URL, jsonObject.toJSONString(), callback);
    }
}
