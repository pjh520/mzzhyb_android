package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-16
 * 描    述：
 * ================================================
 */
public class GetStoreStatusModel {
    /**
     * code : 1
     * msg : 获取店铺状态成功
     * server_time : 2022-11-16 14:56:38
     * data : {"fixmedins_code":"H61080200145","IsOline":"2"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * fixmedins_code : H61080200145
         * IsOline : 2
         */

        private String fixmedins_code;
        private String IsOline;//是否在线（1、在线 2、下线）

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getIsOline() {
            return IsOline;
        }

        public void setIsOline(String IsOline) {
            this.IsOline = IsOline;
        }
    }

    //获取店铺状态
    public static void sendGetStoreStatusRequest(final String TAG, final CustomerJsonCallBack<GetStoreStatusModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_GETSTORESTATUS_URL, "", callback);
    }

    //更新店铺状态
    public static void sendUpdateStoreStatusRequest(final String TAG,String IsOline, final CustomerJsonCallBack<GetStoreStatusModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("IsOline", IsOline);//是否在线（1、在线 2、下线）
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_UPDATESTORESTATUS_URL,  jsonObject.toJSONString(), callback);
    }
}
