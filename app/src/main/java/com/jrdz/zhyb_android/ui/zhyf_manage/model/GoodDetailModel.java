package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/11/19
 * 描    述：
 * ================================================
 */
public class GoodDetailModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-19 11:35:22
     * data : {"InstructionsAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/c0fdef89-b6ea-43bc-8cd5-ab5929688a8a/eaa0ad1f-c635-4c61-97d4-d626e01156a9.jpg","DetailAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/c479df5d-2be3-4e07-97f2-fcfcabf9bdd1/a35da7bd-67da-4fc6-a988-035a600bb4f9.jpg","DetailAccessoryUrl2":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/514a3000-47f9-4035-9141-602bbe0b1f24/5746a86e-0d98-40e5-9a3e-5c01a58e1c7f.jpg","DetailAccessoryUrl3":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/88aa843b-cabd-4f68-87b8-2e481195b25e/57dc84fe-d71e-40ad-a1ba-7792ba2324ce.jpg","DetailAccessoryUrl4":"","DetailAccessoryUrl5":"","BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/a7e199f9-045c-433b-84bd-7e969bb96eb4/f5d1c506-b214-4e24-addb-dfa0bc42c1e4.jpg","BannerAccessoryUrl2":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/86415536-aa5d-43a4-895b-a0d6db771f6e/ef7c5ca3-e2a6-4ef9-89ec-cab294c5b263.jpg","BannerAccessoryUrl3":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/c42b523f-6bb9-42d3-a6e1-01bb98c76fd1/a8d411fd-f3f4-48a6-a230-7fabf363923e.jpg","MonthlySales":100,"GoodsId":1,"MIC_Code":"","ItemCode":"H61080200145202211111217020001","ItemName":"苯丙酮尿症1001","ItemType":1,"ApprovalNumber":"国药准字H20193280-1111","RegDosform":"胶囊剂","Spec":"100mg","EnterpriseName":"通药制药集团股份有限公司","EfccAtd":"功能主治1111","UsualWay":"常见用法1111","StorageConditions":"贮存条件1111","ExpiryDateCount":12,"InventoryQuantity":5,"Price":1,"InstructionsAccessoryId":"c0fdef89-b6ea-43bc-8cd5-ab5929688a8a","DetailAccessoryId1":"c479df5d-2be3-4e07-97f2-fcfcabf9bdd1","DetailAccessoryId2":"514a3000-47f9-4035-9141-602bbe0b1f24","DetailAccessoryId3":"88aa843b-cabd-4f68-87b8-2e481195b25e","DetailAccessoryId4":"00000000-0000-0000-0000-000000000000","DetailAccessoryId5":"00000000-0000-0000-0000-000000000000","RegistrationCertificateNo":"","ProductionLicenseNo":"","Brand":"","Packaging":"","ApplicableScope":"","UsageDosage":"","Precautions":"","Usagemethod":"","IsPlatformDrug":2,"CreateDT":"2022-11-18 16:56:18","UpdateDT":"2022-11-18 16:56:18","CreateUser":"99","UpdateUser":"99","fixmedins_code":"H61080200145","GoodsLocation":1,"CatalogueId":1,"BannerAccessoryId1":"a7e199f9-045c-433b-84bd-7e969bb96eb4","BannerAccessoryId2":"86415536-aa5d-43a4-895b-a0d6db771f6e","BannerAccessoryId3":"c42b523f-6bb9-42d3-a6e1-01bb98c76fd1","Intervaltime":5,"DrugsClassification":1,"DrugsClassificationName":"西药中成药","IsPromotion":1,"GoodsName":"苯丙酮尿症1001","PromotionDiscount":9,"PreferentialPrice":0.9,"StoreSimilarity":"","IsOnSale":1,"AssociatedDiseases":"胃痛1∞#胃痛2∞#胃痛3","AssociatedDiagnostics":"","GoodsNo":"300000"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * InstructionsAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/c0fdef89-b6ea-43bc-8cd5-ab5929688a8a/eaa0ad1f-c635-4c61-97d4-d626e01156a9.jpg
         * DetailAccessoryUrl1 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/c479df5d-2be3-4e07-97f2-fcfcabf9bdd1/a35da7bd-67da-4fc6-a988-035a600bb4f9.jpg
         * DetailAccessoryUrl2 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/514a3000-47f9-4035-9141-602bbe0b1f24/5746a86e-0d98-40e5-9a3e-5c01a58e1c7f.jpg
         * DetailAccessoryUrl3 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/88aa843b-cabd-4f68-87b8-2e481195b25e/57dc84fe-d71e-40ad-a1ba-7792ba2324ce.jpg
         * DetailAccessoryUrl4 :
         * DetailAccessoryUrl5 :
         * BannerAccessoryUrl1 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/a7e199f9-045c-433b-84bd-7e969bb96eb4/f5d1c506-b214-4e24-addb-dfa0bc42c1e4.jpg
         * BannerAccessoryUrl2 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/86415536-aa5d-43a4-895b-a0d6db771f6e/ef7c5ca3-e2a6-4ef9-89ec-cab294c5b263.jpg
         * BannerAccessoryUrl3 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/c42b523f-6bb9-42d3-a6e1-01bb98c76fd1/a8d411fd-f3f4-48a6-a230-7fabf363923e.jpg
         * MonthlySales : 100
         * GoodsId : 1
         * MIC_Code :
         * ItemCode : H61080200145202211111217020001
         * ItemName : 苯丙酮尿症1001
         * ItemType : 1
         * ApprovalNumber : 国药准字H20193280-1111
         * RegDosform : 胶囊剂
         * Spec : 100mg
         * EnterpriseName : 通药制药集团股份有限公司
         * EfccAtd : 功能主治1111
         * UsualWay : 常见用法1111
         * StorageConditions : 贮存条件1111
         * ExpiryDateCount : 12
         * InventoryQuantity : 5
         * Price : 1
         * InstructionsAccessoryId : c0fdef89-b6ea-43bc-8cd5-ab5929688a8a
         * DetailAccessoryId1 : c479df5d-2be3-4e07-97f2-fcfcabf9bdd1
         * DetailAccessoryId2 : 514a3000-47f9-4035-9141-602bbe0b1f24
         * DetailAccessoryId3 : 88aa843b-cabd-4f68-87b8-2e481195b25e
         * DetailAccessoryId4 : 00000000-0000-0000-0000-000000000000
         * DetailAccessoryId5 : 00000000-0000-0000-0000-000000000000
         * RegistrationCertificateNo :
         * ProductionLicenseNo :
         * Brand :
         * Packaging :
         * ApplicableScope :
         * UsageDosage :
         * Precautions :
         * Usagemethod :
         * IsPlatformDrug : 2
         * CreateDT : 2022-11-18 16:56:18
         * UpdateDT : 2022-11-18 16:56:18
         * CreateUser : 99
         * UpdateUser : 99
         * fixmedins_code : H61080200145
         * GoodsLocation : 1
         * CatalogueId : 1
         * BannerAccessoryId1 : a7e199f9-045c-433b-84bd-7e969bb96eb4
         * BannerAccessoryId2 : 86415536-aa5d-43a4-895b-a0d6db771f6e
         * BannerAccessoryId3 : c42b523f-6bb9-42d3-a6e1-01bb98c76fd1
         * Intervaltime : 5
         * DrugsClassification : 1
         * DrugsClassificationName : 西药中成药
         * IsPromotion : 1
         * GoodsName : 苯丙酮尿症1001
         * PromotionDiscount : 9
         * PreferentialPrice : 0.9
         * StoreSimilarity :
         * IsOnSale : 1
         * AssociatedDiseases : 胃痛1∞#胃痛2∞#胃痛3
         * AssociatedDiagnostics :
         * GoodsNo : 300000
         */

        private String InstructionsAccessoryUrl;
        private String DetailAccessoryUrl1;
        private String DetailAccessoryUrl2;
        private String DetailAccessoryUrl3;
        private String DetailAccessoryUrl4;
        private String DetailAccessoryUrl5;
        private String BannerAccessoryUrl1;
        private String BannerAccessoryUrl2;
        private String BannerAccessoryUrl3;
        private String MonthlySales;
        private String GoodsId;
        private String MIC_Code;
        private String ItemCode;
        private String ItemName;
        private String ItemType;
        private String ApprovalNumber;
        private String RegDosform;
        private String Spec;
        private String EnterpriseName;
        private String EfccAtd;
        private String UsualWay;
        private String StorageConditions;
        private String ExpiryDateCount;
        private String InventoryQuantity;
        private String Price;
        private String InstructionsAccessoryId;
        private String DetailAccessoryId1;
        private String DetailAccessoryId2;
        private String DetailAccessoryId3;
        private String DetailAccessoryId4;
        private String DetailAccessoryId5;
        private String RegistrationCertificateNo;
        private String ProductionLicenseNo;
        private String Brand;
        private String Packaging;
        private String ApplicableScope;
        private String UsageDosage;
        private String Precautions;
        private String Usagemethod;
        private String IsPlatformDrug;
        private String CreateDT;
        private String UpdateDT;
        private String CreateUser;
        private String UpdateUser;
        private String fixmedins_code;
        private String GoodsLocation;
        private String CatalogueId;
        private String CatalogueName;
        private String BannerAccessoryId1;
        private String BannerAccessoryId2;
        private String BannerAccessoryId3;
        private String Intervaltime;
        private String DrugsClassification;
        private String DrugsClassificationName;
        private String IsPromotion;
        private String GoodsName;
        private String PromotionDiscount;
        private String PreferentialPrice;
        private String StoreSimilarity;
        private String IsOnSale;
        private String AssociatedDiseases;
        private String AssociatedDiagnostics;
        private String GoodsNo;
        private int ShoppingCartNum;
        private String IsShowGoodsSold;
        private String IsShowSold;
        private String IsShowMargin;

        public String getInstructionsAccessoryUrl() {
            return InstructionsAccessoryUrl;
        }

        public void setInstructionsAccessoryUrl(String InstructionsAccessoryUrl) {
            this.InstructionsAccessoryUrl = InstructionsAccessoryUrl;
        }

        public String getDetailAccessoryUrl1() {
            return DetailAccessoryUrl1;
        }

        public void setDetailAccessoryUrl1(String DetailAccessoryUrl1) {
            this.DetailAccessoryUrl1 = DetailAccessoryUrl1;
        }

        public String getDetailAccessoryUrl2() {
            return DetailAccessoryUrl2;
        }

        public void setDetailAccessoryUrl2(String DetailAccessoryUrl2) {
            this.DetailAccessoryUrl2 = DetailAccessoryUrl2;
        }

        public String getDetailAccessoryUrl3() {
            return DetailAccessoryUrl3;
        }

        public void setDetailAccessoryUrl3(String DetailAccessoryUrl3) {
            this.DetailAccessoryUrl3 = DetailAccessoryUrl3;
        }

        public String getDetailAccessoryUrl4() {
            return DetailAccessoryUrl4;
        }

        public void setDetailAccessoryUrl4(String DetailAccessoryUrl4) {
            this.DetailAccessoryUrl4 = DetailAccessoryUrl4;
        }

        public String getDetailAccessoryUrl5() {
            return DetailAccessoryUrl5;
        }

        public void setDetailAccessoryUrl5(String DetailAccessoryUrl5) {
            this.DetailAccessoryUrl5 = DetailAccessoryUrl5;
        }

        public String getBannerAccessoryUrl1() {
            return BannerAccessoryUrl1;
        }

        public void setBannerAccessoryUrl1(String BannerAccessoryUrl1) {
            this.BannerAccessoryUrl1 = BannerAccessoryUrl1;
        }

        public String getBannerAccessoryUrl2() {
            return BannerAccessoryUrl2;
        }

        public void setBannerAccessoryUrl2(String BannerAccessoryUrl2) {
            this.BannerAccessoryUrl2 = BannerAccessoryUrl2;
        }

        public String getBannerAccessoryUrl3() {
            return BannerAccessoryUrl3;
        }

        public void setBannerAccessoryUrl3(String BannerAccessoryUrl3) {
            this.BannerAccessoryUrl3 = BannerAccessoryUrl3;
        }

        public String getMonthlySales() {
            return MonthlySales;
        }

        public void setMonthlySales(String MonthlySales) {
            this.MonthlySales = MonthlySales;
        }

        public String getGoodsId() {
            return GoodsId;
        }

        public void setGoodsId(String GoodsId) {
            this.GoodsId = GoodsId;
        }

        public String getMIC_Code() {
            return MIC_Code;
        }

        public void setMIC_Code(String MIC_Code) {
            this.MIC_Code = MIC_Code;
        }

        public String getItemCode() {
            return ItemCode;
        }

        public void setItemCode(String ItemCode) {
            this.ItemCode = ItemCode;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String ItemName) {
            this.ItemName = ItemName;
        }

        public String getItemType() {
            return ItemType;
        }

        public void setItemType(String ItemType) {
            this.ItemType = ItemType;
        }

        public String getApprovalNumber() {
            return ApprovalNumber;
        }

        public void setApprovalNumber(String ApprovalNumber) {
            this.ApprovalNumber = ApprovalNumber;
        }

        public String getRegDosform() {
            return RegDosform;
        }

        public void setRegDosform(String RegDosform) {
            this.RegDosform = RegDosform;
        }

        public String getSpec() {
            return Spec;
        }

        public void setSpec(String Spec) {
            this.Spec = Spec;
        }

        public String getEnterpriseName() {
            return EnterpriseName;
        }

        public void setEnterpriseName(String EnterpriseName) {
            this.EnterpriseName = EnterpriseName;
        }

        public String getEfccAtd() {
            return EfccAtd;
        }

        public void setEfccAtd(String EfccAtd) {
            this.EfccAtd = EfccAtd;
        }

        public String getUsualWay() {
            return UsualWay;
        }

        public void setUsualWay(String UsualWay) {
            this.UsualWay = UsualWay;
        }

        public String getStorageConditions() {
            return StorageConditions;
        }

        public void setStorageConditions(String StorageConditions) {
            this.StorageConditions = StorageConditions;
        }

        public String getExpiryDateCount() {
            return ExpiryDateCount;
        }

        public void setExpiryDateCount(String ExpiryDateCount) {
            this.ExpiryDateCount = ExpiryDateCount;
        }

        public String getInventoryQuantity() {
            return InventoryQuantity;
        }

        public void setInventoryQuantity(String InventoryQuantity) {
            this.InventoryQuantity = InventoryQuantity;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String Price) {
            this.Price = Price;
        }

        public String getInstructionsAccessoryId() {
            return InstructionsAccessoryId;
        }

        public void setInstructionsAccessoryId(String InstructionsAccessoryId) {
            this.InstructionsAccessoryId = InstructionsAccessoryId;
        }

        public String getDetailAccessoryId1() {
            return DetailAccessoryId1;
        }

        public void setDetailAccessoryId1(String DetailAccessoryId1) {
            this.DetailAccessoryId1 = DetailAccessoryId1;
        }

        public String getDetailAccessoryId2() {
            return DetailAccessoryId2;
        }

        public void setDetailAccessoryId2(String DetailAccessoryId2) {
            this.DetailAccessoryId2 = DetailAccessoryId2;
        }

        public String getDetailAccessoryId3() {
            return DetailAccessoryId3;
        }

        public void setDetailAccessoryId3(String DetailAccessoryId3) {
            this.DetailAccessoryId3 = DetailAccessoryId3;
        }

        public String getDetailAccessoryId4() {
            return DetailAccessoryId4;
        }

        public void setDetailAccessoryId4(String DetailAccessoryId4) {
            this.DetailAccessoryId4 = DetailAccessoryId4;
        }

        public String getDetailAccessoryId5() {
            return DetailAccessoryId5;
        }

        public void setDetailAccessoryId5(String DetailAccessoryId5) {
            this.DetailAccessoryId5 = DetailAccessoryId5;
        }

        public String getRegistrationCertificateNo() {
            return RegistrationCertificateNo;
        }

        public void setRegistrationCertificateNo(String RegistrationCertificateNo) {
            this.RegistrationCertificateNo = RegistrationCertificateNo;
        }

        public String getProductionLicenseNo() {
            return ProductionLicenseNo;
        }

        public void setProductionLicenseNo(String ProductionLicenseNo) {
            this.ProductionLicenseNo = ProductionLicenseNo;
        }

        public String getBrand() {
            return Brand;
        }

        public void setBrand(String Brand) {
            this.Brand = Brand;
        }

        public String getPackaging() {
            return Packaging;
        }

        public void setPackaging(String Packaging) {
            this.Packaging = Packaging;
        }

        public String getApplicableScope() {
            return ApplicableScope;
        }

        public void setApplicableScope(String ApplicableScope) {
            this.ApplicableScope = ApplicableScope;
        }

        public String getUsageDosage() {
            return UsageDosage;
        }

        public void setUsageDosage(String UsageDosage) {
            this.UsageDosage = UsageDosage;
        }

        public String getPrecautions() {
            return Precautions;
        }

        public void setPrecautions(String Precautions) {
            this.Precautions = Precautions;
        }

        public String getUsagemethod() {
            return Usagemethod;
        }

        public void setUsagemethod(String Usagemethod) {
            this.Usagemethod = Usagemethod;
        }

        public String getIsPlatformDrug() {
            return IsPlatformDrug;
        }

        public void setIsPlatformDrug(String IsPlatformDrug) {
            this.IsPlatformDrug = IsPlatformDrug;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getUpdateDT() {
            return UpdateDT;
        }

        public void setUpdateDT(String UpdateDT) {
            this.UpdateDT = UpdateDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String CreateUser) {
            this.CreateUser = CreateUser;
        }

        public String getUpdateUser() {
            return UpdateUser;
        }

        public void setUpdateUser(String UpdateUser) {
            this.UpdateUser = UpdateUser;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getGoodsLocation() {
            return GoodsLocation;
        }

        public void setGoodsLocation(String GoodsLocation) {
            this.GoodsLocation = GoodsLocation;
        }

        public String getCatalogueId() {
            return CatalogueId;
        }

        public void setCatalogueId(String CatalogueId) {
            this.CatalogueId = CatalogueId;
        }

        public String getCatalogueName() {
            return CatalogueName;
        }

        public void setCatalogueName(String catalogueName) {
            CatalogueName = catalogueName;
        }

        public String getBannerAccessoryId1() {
            return BannerAccessoryId1;
        }

        public void setBannerAccessoryId1(String BannerAccessoryId1) {
            this.BannerAccessoryId1 = BannerAccessoryId1;
        }

        public String getBannerAccessoryId2() {
            return BannerAccessoryId2;
        }

        public void setBannerAccessoryId2(String BannerAccessoryId2) {
            this.BannerAccessoryId2 = BannerAccessoryId2;
        }

        public String getBannerAccessoryId3() {
            return BannerAccessoryId3;
        }

        public void setBannerAccessoryId3(String BannerAccessoryId3) {
            this.BannerAccessoryId3 = BannerAccessoryId3;
        }

        public String getIntervaltime() {
            return Intervaltime;
        }

        public void setIntervaltime(String Intervaltime) {
            this.Intervaltime = Intervaltime;
        }

        public String getDrugsClassification() {
            return DrugsClassification;
        }

        public void setDrugsClassification(String DrugsClassification) {
            this.DrugsClassification = DrugsClassification;
        }

        public String getDrugsClassificationName() {
            return DrugsClassificationName;
        }

        public void setDrugsClassificationName(String DrugsClassificationName) {
            this.DrugsClassificationName = DrugsClassificationName;
        }

        public String getIsPromotion() {
            return IsPromotion;
        }

        public void setIsPromotion(String IsPromotion) {
            this.IsPromotion = IsPromotion;
        }

        public String getGoodsName() {
            return GoodsName;
        }

        public void setGoodsName(String GoodsName) {
            this.GoodsName = GoodsName;
        }

        public String getPromotionDiscount() {
            return PromotionDiscount;
        }

        public void setPromotionDiscount(String PromotionDiscount) {
            this.PromotionDiscount = PromotionDiscount;
        }

        public String getPreferentialPrice() {
            return PreferentialPrice;
        }

        public void setPreferentialPrice(String PreferentialPrice) {
            this.PreferentialPrice = PreferentialPrice;
        }

        public String getStoreSimilarity() {
            return StoreSimilarity;
        }

        public void setStoreSimilarity(String StoreSimilarity) {
            this.StoreSimilarity = StoreSimilarity;
        }

        public String getIsOnSale() {
            return IsOnSale;
        }

        public void setIsOnSale(String IsOnSale) {
            this.IsOnSale = IsOnSale;
        }

        public String getAssociatedDiseases() {
            return AssociatedDiseases;
        }

        public void setAssociatedDiseases(String AssociatedDiseases) {
            this.AssociatedDiseases = AssociatedDiseases;
        }

        public String getAssociatedDiagnostics() {
            return AssociatedDiagnostics;
        }

        public void setAssociatedDiagnostics(String AssociatedDiagnostics) {
            this.AssociatedDiagnostics = AssociatedDiagnostics;
        }

        public String getGoodsNo() {
            return GoodsNo;
        }

        public void setGoodsNo(String GoodsNo) {
            this.GoodsNo = GoodsNo;
        }

        public int getShoppingCartNum() {
            return ShoppingCartNum;
        }

        public void setShoppingCartNum(int shoppingCartNum) {
            ShoppingCartNum = shoppingCartNum;
        }

        public String getIsShowGoodsSold() {
            return IsShowGoodsSold;
        }

        public void setIsShowGoodsSold(String isShowGoodsSold) {
            IsShowGoodsSold = isShowGoodsSold;
        }

        public String getIsShowSold() {
            return IsShowSold;
        }

        public void setIsShowSold(String isShowSold) {
            IsShowSold = isShowSold;
        }

        public String getIsShowMargin() {
            return IsShowMargin;
        }

        public void setIsShowMargin(String isShowMargin) {
            IsShowMargin = isShowMargin;
        }
    }

    //获取商品详情
    public static void sendGoodDetailRequest(final String TAG, String GoodsNo, final CustomerJsonCallBack<GoodDetailModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("GoodsNo", GoodsNo);//搜索名称

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_DETAILGOODS_URL, jsonObject.toJSONString(), callback);
    }
}
