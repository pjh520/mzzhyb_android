package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;
import java.util.Objects;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-18
 * 描    述：
 * ================================================
 */
public class GoodsModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-19 09:46:46
     * totalItems : 2
     * data : [{"GoodsId":1,"MIC_Code":"","ItemCode":"H61080200145202211111217020001","ItemType":1,"EfccAtd":"功能主治1111","InventoryQuantity":5,"Price":1,"IsPlatformDrug":2,"fixmedins_code":"H61080200145","GoodsLocation":1,"CatalogueId":1,"BannerAccessoryId1":"a7e199f9-045c-433b-84bd-7e969bb96eb4","DrugsClassification":1,"DrugsClassificationName":"西药中成药","IsPromotion":1,"GoodsName":"苯丙酮尿症1001","PromotionDiscount":9,"PreferentialPrice":0.9,"IsOnSale":1,"BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/a7e199f9-045c-433b-84bd-7e969bb96eb4/f5d1c506-b214-4e24-addb-dfa0bc42c1e4.jpg"},{"GoodsId":2,"MIC_Code":"XA01ABD075A002010100483","ItemCode":"XA01ABD075A002010100483","ItemType":1,"EfccAtd":"功能主治1111","InventoryQuantity":5,"Price":1,"IsPlatformDrug":1,"fixmedins_code":"H61080200145","GoodsLocation":1,"CatalogueId":1,"BannerAccessoryId1":"cc9dae19-400f-4790-a302-22e69b867388","DrugsClassification":1,"DrugsClassificationName":"西药中成药","IsPromotion":1,"GoodsName":"地喹氯铵含片","PromotionDiscount":9,"PreferentialPrice":0.9,"IsOnSale":1,"BannerAccessoryUrl1":"~/App_Upload/Storage/Medicare_Ticket/cc9dae19-400f-4790-a302-22e69b867388/c9061f61-88b6-4df3-aba9-21d5b8b64546.jpg"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * GoodsId : 1
         * MIC_Code :
         * ItemCode : H61080200145202211111217020001
         * ItemType : 1
         * EfccAtd : 功能主治1111
         * InventoryQuantity : 5
         * Price : 1
         * IsPlatformDrug : 2
         * fixmedins_code : H61080200145
         * GoodsLocation : 1
         * CatalogueId : 1
         * BannerAccessoryId1 : a7e199f9-045c-433b-84bd-7e969bb96eb4
         * DrugsClassification : 1
         * DrugsClassificationName : 西药中成药
         * IsPromotion : 1
         * GoodsName : 苯丙酮尿症1001
         * PromotionDiscount : 9
         * PreferentialPrice : 0.9
         * IsOnSale : 1
         * BannerAccessoryUrl1 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/a7e199f9-045c-433b-84bd-7e969bb96eb4/f5d1c506-b214-4e24-addb-dfa0bc42c1e4.jpg
         */

        private String GoodsNo;
        private String ItemType;
        private String EfccAtd;
        private int InventoryQuantity;
        private String Price;
        private String IsPlatformDrug;
        private String fixmedins_code;
        private String GoodsLocation;
        private String CatalogueId;
        private String BannerAccessoryId1;
        private String DrugsClassification;
        private String DrugsClassificationName;
        private String IsPromotion;
        private String GoodsName;
        private String PromotionDiscount;
        private String PreferentialPrice;
        private String IsOnSale;
        private String BannerAccessoryUrl1;
        private String MonthlySales;
        private int ShoppingCartNum;
        private String Spec;
        private String AssociatedDiagnostics;
        private String IsCollection;
        private String Latitude;
        private String Longitude;
        private String IsShowGoodsSold;
        private String IsShowMargin;
        //是否允许企业基金支付（0不允许 1允许）
        private String IsEnterpriseFundPay;
        //本地数据
        public boolean choose = false;

        public DataBean() {
        }

        //更新商品数据时
        public DataBean(String goodsNo, String itemType, String efccAtd, int inventoryQuantity, String price, String isPlatformDrug,
                        String goodsLocation, String catalogueId, String bannerAccessoryId1, String drugsClassification, String drugsClassificationName,
                        String isPromotion, String goodsName, String promotionDiscount, String preferentialPrice, String isOnSale,
                        String bannerAccessoryUrl1, String monthlySales, boolean choose,String isShowGoodsSold, String isShowMargin) {
            GoodsNo = goodsNo;
            ItemType = itemType;
            EfccAtd = efccAtd;
            InventoryQuantity = inventoryQuantity;
            Price = price;
            IsPlatformDrug = isPlatformDrug;
            GoodsLocation = goodsLocation;
            CatalogueId = catalogueId;
            BannerAccessoryId1 = bannerAccessoryId1;
            DrugsClassification = drugsClassification;
            DrugsClassificationName = drugsClassificationName;
            IsPromotion = isPromotion;
            GoodsName = goodsName;
            PromotionDiscount = promotionDiscount;
            PreferentialPrice = preferentialPrice;
            IsOnSale = isOnSale;
            BannerAccessoryUrl1 = bannerAccessoryUrl1;
            MonthlySales = monthlySales;
            this.choose = choose;
            IsShowGoodsSold = isShowGoodsSold;
            IsShowMargin = isShowMargin;
        }

        //用户端-商品详情-加入购物车
        public DataBean(String goodsNo, String itemType, String efccAtd, int inventoryQuantity, String price, String isPlatformDrug, String fixmedins_code,
                        String goodsLocation, String catalogueId, String bannerAccessoryId1, String drugsClassification, String drugsClassificationName,
                        String isPromotion, String goodsName, String promotionDiscount, String preferentialPrice, String isOnSale,
                        String bannerAccessoryUrl1, String monthlySales, int shoppingCartNum, String spec, String associatedDiagnostics,
                        String isShowGoodsSold, String isShowMargin) {
            GoodsNo = goodsNo;
            ItemType = itemType;
            EfccAtd = efccAtd;
            InventoryQuantity = inventoryQuantity;
            Price = price;
            this.fixmedins_code = fixmedins_code;
            IsPlatformDrug = isPlatformDrug;
            GoodsLocation = goodsLocation;
            CatalogueId = catalogueId;
            BannerAccessoryId1 = bannerAccessoryId1;
            DrugsClassification = drugsClassification;
            DrugsClassificationName = drugsClassificationName;
            IsPromotion = isPromotion;
            GoodsName = goodsName;
            PromotionDiscount = promotionDiscount;
            PreferentialPrice = preferentialPrice;
            IsOnSale = isOnSale;
            BannerAccessoryUrl1 = bannerAccessoryUrl1;
            MonthlySales = monthlySales;
            ShoppingCartNum = shoppingCartNum;
            Spec = spec;
            AssociatedDiagnostics = associatedDiagnostics;
            IsShowGoodsSold = isShowGoodsSold;
            IsShowMargin = isShowMargin;
        }

        public String getGoodsNo() {
            return GoodsNo;
        }

        public void setGoodsNo(String goodsNo) {
            GoodsNo = goodsNo;
        }

        public String getItemType() {
            return ItemType;
        }

        public void setItemType(String ItemType) {
            this.ItemType = ItemType;
        }

        public String getEfccAtd() {
            return EfccAtd;
        }

        public void setEfccAtd(String EfccAtd) {
            this.EfccAtd = EfccAtd;
        }

        public int getInventoryQuantity() {
            return InventoryQuantity;
        }

        public void setInventoryQuantity(int InventoryQuantity) {
            this.InventoryQuantity = InventoryQuantity;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String Price) {
            this.Price = Price;
        }

        public String getIsPlatformDrug() {
            return IsPlatformDrug;
        }

        public void setIsPlatformDrug(String IsPlatformDrug) {
            this.IsPlatformDrug = IsPlatformDrug;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getGoodsLocation() {
            return GoodsLocation;
        }

        public void setGoodsLocation(String GoodsLocation) {
            this.GoodsLocation = GoodsLocation;
        }

        public String getCatalogueId() {
            return CatalogueId;
        }

        public void setCatalogueId(String CatalogueId) {
            this.CatalogueId = CatalogueId;
        }

        public String getBannerAccessoryId1() {
            return BannerAccessoryId1;
        }

        public void setBannerAccessoryId1(String BannerAccessoryId1) {
            this.BannerAccessoryId1 = BannerAccessoryId1;
        }

        public String getDrugsClassification() {
            return DrugsClassification;
        }

        public void setDrugsClassification(String DrugsClassification) {
            this.DrugsClassification = DrugsClassification;
        }

        public String getDrugsClassificationName() {
            return DrugsClassificationName;
        }

        public void setDrugsClassificationName(String DrugsClassificationName) {
            this.DrugsClassificationName = DrugsClassificationName;
        }

        public String getIsPromotion() {
            return IsPromotion;
        }

        public void setIsPromotion(String IsPromotion) {
            this.IsPromotion = IsPromotion;
        }

        public String getGoodsName() {
            return GoodsName;
        }

        public void setGoodsName(String GoodsName) {
            this.GoodsName = GoodsName;
        }

        public String getPromotionDiscount() {
            return PromotionDiscount;
        }

        public void setPromotionDiscount(String PromotionDiscount) {
            this.PromotionDiscount = PromotionDiscount;
        }

        public String getPreferentialPrice() {
            return PreferentialPrice;
        }

        public void setPreferentialPrice(String PreferentialPrice) {
            this.PreferentialPrice = PreferentialPrice;
        }

        public String getIsOnSale() {
            return IsOnSale;
        }

        public void setIsOnSale(String IsOnSale) {
            this.IsOnSale = IsOnSale;
        }

        public String getBannerAccessoryUrl1() {
            return BannerAccessoryUrl1;
        }

        public void setBannerAccessoryUrl1(String BannerAccessoryUrl1) {
            this.BannerAccessoryUrl1 = BannerAccessoryUrl1;
        }

        public String getMonthlySales() {
            return MonthlySales;
        }

        public void setMonthlySales(String monthlySales) {
            MonthlySales = monthlySales;
        }

        public int getShoppingCartNum() {
            return ShoppingCartNum;
        }

        public void setShoppingCartNum(int shoppingCartNum) {
            ShoppingCartNum = shoppingCartNum;
        }

        public String getSpec() {
            return Spec;
        }

        public void setSpec(String spec) {
            Spec = spec;
        }

        public String getAssociatedDiagnostics() {
            return AssociatedDiagnostics;
        }

        public void setAssociatedDiagnostics(String associatedDiagnostics) {
            AssociatedDiagnostics = associatedDiagnostics;
        }

        public String getIsCollection() {
            return IsCollection;
        }

        public void setIsCollection(String isCollection) {
            IsCollection = isCollection;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String latitude) {
            Latitude = latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String longitude) {
            Longitude = longitude;
        }

        public String getIsShowGoodsSold() {
            return IsShowGoodsSold;
        }

        public void setIsShowGoodsSold(String isShowGoodsSold) {
            IsShowGoodsSold = isShowGoodsSold;
        }

        public String getIsShowMargin() {
            return IsShowMargin;
        }

        public void setIsShowMargin(String isShowMargin) {
            IsShowMargin = isShowMargin;
        }

        public String getIsEnterpriseFundPay() {
            return IsEnterpriseFundPay;
        }

        public void setIsEnterpriseFundPay(String isEnterpriseFundPay) {
            IsEnterpriseFundPay = isEnterpriseFundPay;
        }

        public boolean isChoose() {
            return choose;
        }

        public void setChoose(boolean choose) {
            this.choose = choose;
        }

        //重写equals方法 只要对比id一样 就是同一个model 可以删除掉
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DataBean that = (DataBean) o;
            return Objects.equals(GoodsNo, that.GoodsNo);
        }

        @Override
        public int hashCode() {
            return Objects.hash(GoodsNo, ItemType, EfccAtd, InventoryQuantity, Price, IsPlatformDrug, fixmedins_code, GoodsLocation, CatalogueId, BannerAccessoryId1, DrugsClassification, DrugsClassificationName, IsPromotion, GoodsName, PromotionDiscount, PreferentialPrice, IsOnSale, BannerAccessoryUrl1, MonthlySales, ShoppingCartNum, Spec, AssociatedDiagnostics, IsCollection, Latitude, Longitude, IsShowGoodsSold, IsShowMargin, IsEnterpriseFundPay, choose);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.GoodsNo);
            dest.writeString(this.ItemType);
            dest.writeString(this.EfccAtd);
            dest.writeInt(this.InventoryQuantity);
            dest.writeString(this.Price);
            dest.writeString(this.fixmedins_code);
            dest.writeString(this.IsPlatformDrug);
            dest.writeString(this.GoodsLocation);
            dest.writeString(this.CatalogueId);
            dest.writeString(this.BannerAccessoryId1);
            dest.writeString(this.DrugsClassification);
            dest.writeString(this.DrugsClassificationName);
            dest.writeString(this.IsPromotion);
            dest.writeString(this.GoodsName);
            dest.writeString(this.PromotionDiscount);
            dest.writeString(this.PreferentialPrice);
            dest.writeString(this.IsOnSale);
            dest.writeString(this.BannerAccessoryUrl1);
            dest.writeString(this.MonthlySales);
            dest.writeInt(this.ShoppingCartNum);
            dest.writeString(this.Spec);
            dest.writeString(this.AssociatedDiagnostics);
            dest.writeString(this.IsCollection);
            dest.writeString(this.Latitude);
            dest.writeString(this.Longitude);
            dest.writeString(this.IsShowGoodsSold);
            dest.writeString(this.IsShowMargin);
            dest.writeString(this.IsEnterpriseFundPay);
            dest.writeByte(this.choose ? (byte) 1 : (byte) 0);
        }

        protected DataBean(Parcel in) {
            this.GoodsNo = in.readString();
            this.ItemType = in.readString();
            this.EfccAtd = in.readString();
            this.InventoryQuantity = in.readInt();
            this.Price = in.readString();
            this.fixmedins_code = in.readString();
            this.IsPlatformDrug = in.readString();
            this.GoodsLocation = in.readString();
            this.CatalogueId = in.readString();
            this.BannerAccessoryId1 = in.readString();
            this.DrugsClassification = in.readString();
            this.DrugsClassificationName = in.readString();
            this.IsPromotion = in.readString();
            this.GoodsName = in.readString();
            this.PromotionDiscount = in.readString();
            this.PreferentialPrice = in.readString();
            this.IsOnSale = in.readString();
            this.BannerAccessoryUrl1 = in.readString();
            this.MonthlySales = in.readString();
            this.ShoppingCartNum = in.readInt();
            this.Spec = in.readString();
            this.AssociatedDiagnostics = in.readString();
            this.IsCollection = in.readString();
            this.Latitude = in.readString();
            this.Longitude = in.readString();
            this.IsShowGoodsSold = in.readString();
            this.IsShowMargin = in.readString();
            this.IsEnterpriseFundPay = in.readString();
            this.choose = in.readByte() != 0;
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //产品池商品列表
    public static void sendGoodsRequest(final String TAG, String pageindex, String pagesize, String name, String CatalogueId, String DrugsClassification,
                                        final CustomerJsonCallBack<GoodsModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);//
        jsonObject.put("pagesize", pagesize);//
        jsonObject.put("name", name);//搜索名称
        jsonObject.put("CatalogueId", CatalogueId);//目录唯一标识
        jsonObject.put("DrugsClassification", DrugsClassification);//药品分类
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_GOODS_LIST_URL, jsonObject.toJSONString(), callback);
    }

    //草稿箱商品列表
    public static void sendDraftsGoodsRequest(final String TAG, String name, String pageindex, String pagesize,
                                              final CustomerJsonCallBack<GoodsModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", name);//搜索名称
        jsonObject.put("pageindex", pageindex);//
        jsonObject.put("pagesize", pagesize);//
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_DRAFTS_GOODS_LIST_URL, jsonObject.toJSONString(), callback);
    }

    //店内相似商品列表--用户端
    public static void sendStoreSimilarityRequest_user(final String TAG, String fixmedins_code, String StoreSimilarity, String GoodsNo,
                                                       final CustomerJsonCallBack<GoodsModel> callback) {
        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("fixmedins_code", fixmedins_code);//店内相似关键字
        jsonObject.put("StoreSimilarity", StoreSimilarity);//店内相似关键字
        jsonObject.put("GoodsNo", GoodsNo);//商品编码

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_STORESIMILARITYLIST_URL, jsonObject.toJSONString(), callback);
    }

    //获取首页商品推荐--用户端
    public static void sendHomeRecommendListRequest_user(final String TAG, final CustomerJsonCallBack<GoodsModel> callback) {
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_STORERECOMMENDLIST_URL, "", callback);
    }

    //分类商品列表--用户端
    public static void sendSortGoodsListRequest_user(final String TAG, String pageindex, String pagesize, String name, String CatalogueId,
                                                     String DrugsClassification, String fixmedins_code, String BySales, String ByPrice,
                                                     final CustomerJsonCallBack<GoodsModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);//
        jsonObject.put("pagesize", pagesize);//
        jsonObject.put("name", name);//搜索名称
        jsonObject.put("CatalogueId", CatalogueId);//目录唯一标识
        jsonObject.put("DrugsClassification", DrugsClassification);//药品分类
        jsonObject.put("fixmedins_code", fixmedins_code);//药品分类
        jsonObject.put("BySales", BySales);//BySales 按销量 0不是 1按销量高到低
        jsonObject.put("ByPrice", ByPrice);//按价格 0 不是 1价格由高到低 2价格由低到高
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_GOODSLIST_URL, jsonObject.toJSONString(), callback);
    }

    //获取商品列表--用户端
    public static void sendShopAllGoodsRequest_user(final String TAG, String pageindex, String pagesize, String name,
                                                    String CatalogueId, String DrugsClassification, String fixmedins_code,
                                                    final CustomerJsonCallBack<GoodsModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);//
        jsonObject.put("pagesize", pagesize);//
        jsonObject.put("name", name);//搜索名称
        jsonObject.put("CatalogueId", CatalogueId);//目录唯一标识
        jsonObject.put("DrugsClassification", DrugsClassification);//药品分类
        jsonObject.put("fixmedins_code", fixmedins_code);//机构编码
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_GOODSLIST_URL, jsonObject.toJSONString(), callback);
    }

    //获取商品列表--用户端
    public static void sendShopZoneGoodsRequest_user(final String TAG, String pageindex, String pagesize, String name,
                                                    String CatalogueId, String DrugsClassification, String fixmedins_code,
                                                    final CustomerJsonCallBack<GoodsModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);//
        jsonObject.put("pagesize", pagesize);//
        jsonObject.put("name", name);//搜索名称
        jsonObject.put("CatalogueId", CatalogueId);//目录唯一标识
        jsonObject.put("DrugsClassification", DrugsClassification);//药品分类
        jsonObject.put("fixmedins_code", fixmedins_code);//机构编码
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ZONEGOODSLIST_URL, jsonObject.toJSONString(), callback);
    }

    //分类商品列表--商户端
    public static void sendSortGoodsListRequest_mana(final String TAG, String pageindex, String pagesize, String name, String CatalogueId,
                                                     String DrugsClassification, String fixmedins_code, String BySales, String ByPrice,String barCode,
                                                     final CustomerJsonCallBack<GoodsModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);//
        jsonObject.put("pagesize", pagesize);//
        jsonObject.put("name", name);//搜索名称
        jsonObject.put("CatalogueId", CatalogueId);//目录唯一标识
        jsonObject.put("DrugsClassification", DrugsClassification);//药品分类
        jsonObject.put("fixmedins_code", fixmedins_code);//药品分类
        jsonObject.put("BySales", BySales);//BySales 按销量 0不是 1按销量高到低
        jsonObject.put("ByPrice", ByPrice);//按价格 0 不是 1价格由高到低 2价格由低到高
        jsonObject.put("barCode", barCode);//条形码
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MERCHANT_MERCHANTGOODSLIST_URL, jsonObject.toJSONString(), callback);
    }
}
