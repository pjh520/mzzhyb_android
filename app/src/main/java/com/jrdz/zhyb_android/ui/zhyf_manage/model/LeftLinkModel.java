package com.jrdz.zhyb_android.ui.zhyf_manage.model;

/**
 * ================================================
 * 项目名称：AndroidFrame
 * 包    名：com.example.myapplication.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-08-01
 * 描    述：
 * ================================================
 */
public class LeftLinkModel {
    private String classifyId;
    private String title;

    //------全部版块列表用到（内部程序自用）
    private String choose;//记录是否选中  1代表选中 0代表未选中 top01代表是选中的上一个 bottom01代表是选中的下一个

    public LeftLinkModel(String classifyId, String title, String choose) {
        this.classifyId = classifyId;
        this.title = title;
        this.choose = choose;
    }

    public String getClassifyId() {
        return classifyId;
    }

    public void setClassifyId(String classifyId) {
        this.classifyId = classifyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChoose() {
        return choose;
    }

    public void setChoose(String choose) {
        this.choose = choose;
    }
}
