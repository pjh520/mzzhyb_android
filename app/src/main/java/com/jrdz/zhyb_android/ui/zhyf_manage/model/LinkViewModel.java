package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * ================================================
 * 项目名称：AndroidFrame
 * 包    名：com.example.myapplication.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/7/30
 * 描    述：
 * ================================================
 */
public class LinkViewModel implements MultiItemEntity {
    public static final int TITLE_TAG=1;
    public static final int BODY_TAG=2;

    private int type;
    private String title;

    private String body;
    //==============程序自用=============================
    private int postion=-1;//title 对应的 左边大分类的pos
    private int selectHeadPos=0;//0:全部 1:销量 2:价格
    //==============程序自用=============================

    public void setTitleData(int type, String title, int postion) {
        this.type = type;
        this.title = title;
        this.postion = postion;
    }

    public void setBodyData(int type, String body, int postion) {
        this.type = type;
        this.body = body;
        this.postion = postion;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getPostion() {
        return postion;
    }

    public void setPostion(int postion) {
        this.postion = postion;
    }

    public int getSelectHeadPos() {
        return selectHeadPos;
    }

    public void setSelectHeadPos(int selectHeadPos) {
        this.selectHeadPos = selectHeadPos;
    }

    @Override
    public int getItemType() {
        return type;
    }
}
