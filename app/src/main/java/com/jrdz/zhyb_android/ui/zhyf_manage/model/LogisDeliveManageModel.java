package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-17
 * 描    述：
 * ================================================
 */
public class LogisDeliveManageModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-17 15:57:02
     * data : {"LogisticsDistributionId":0,"IsSelfDelivery":1,"SelfDeliveryName":"","SelfDeliveryPhone":"","fixmedins_code":"H61080200145","SelfDeliveryArea":5,"IsExpress":0,"ExpressCompany":""}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * LogisticsDistributionId : 0
         * IsSelfDelivery : 1
         * SelfDeliveryName :
         * SelfDeliveryPhone :
         * fixmedins_code : H61080200145
         * SelfDeliveryArea : 5
         * IsExpress : 0
         * ExpressCompany :
         */

        private String LogisticsDistributionId;
        private String IsSelfDelivery;
        private String SelfDeliveryName;
        private String SelfDeliveryPhone;
        private String fixmedins_code;
        private String SelfDeliveryArea;
        private String IsExpress;
        private String ExpressCompany;

        public String getLogisticsDistributionId() {
            return LogisticsDistributionId;
        }

        public void setLogisticsDistributionId(String LogisticsDistributionId) {
            this.LogisticsDistributionId = LogisticsDistributionId;
        }

        public String getIsSelfDelivery() {
            return IsSelfDelivery;
        }

        public void setIsSelfDelivery(String IsSelfDelivery) {
            this.IsSelfDelivery = IsSelfDelivery;
        }

        public String getSelfDeliveryName() {
            return SelfDeliveryName;
        }

        public void setSelfDeliveryName(String SelfDeliveryName) {
            this.SelfDeliveryName = SelfDeliveryName;
        }

        public String getSelfDeliveryPhone() {
            return SelfDeliveryPhone;
        }

        public void setSelfDeliveryPhone(String SelfDeliveryPhone) {
            this.SelfDeliveryPhone = SelfDeliveryPhone;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getSelfDeliveryArea() {
            return SelfDeliveryArea;
        }

        public void setSelfDeliveryArea(String SelfDeliveryArea) {
            this.SelfDeliveryArea = SelfDeliveryArea;
        }

        public String getIsExpress() {
            return IsExpress;
        }

        public void setIsExpress(String IsExpress) {
            this.IsExpress = IsExpress;
        }

        public String getExpressCompany() {
            return ExpressCompany;
        }

        public void setExpressCompany(String ExpressCompany) {
            this.ExpressCompany = ExpressCompany;
        }
    }

    //查询物流配送
    public static void sendQueryLogisCofigRequest(final String TAG,final CustomerJsonCallBack<LogisDeliveManageModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_QUERYLOGISTICSDISTRIBUTION_URL, "", callback);
    }
}
