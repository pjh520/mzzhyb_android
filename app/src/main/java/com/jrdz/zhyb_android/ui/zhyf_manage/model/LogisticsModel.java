package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-26
 * 描    述：
 * ================================================
 */
public class LogisticsModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-12-26 15:32:53
     * data : {"LogisticsProgressId":1,"OrderNo":"20221219094244100026","ShippingMethod":1,"ShippingMethodName":"自提","FullName":"郑富贵","Phone":"15050668985","Address":"福建省莆田市涵江区涵西街道湖园路在世纪名苑-东北门附近","SelfDeliveryName":"","SelfDeliveryPhone":"","fixmedins_code":"H61080200145","ExpressCompany":"","ExpressNo":"","LogisticsProgressInfo":"自提,预计2022-12-27到达","CreateDT":"2022-12-24 19:34:12","CreateUser":"99","UpdateDT":"2022-12-24 19:34:12","UpdateUser":"99","UserId":"eb6e9c20-ba61-4e94-ad58-550a6dd91f93","UserName":"99","DeliveryTime":"2022-12-24 19:34:12","ConfirmationTime":"2022-12-24 19:34:12","ConfirmationInfo":"","IsConfirm":0}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * LogisticsProgressId : 1
         * OrderNo : 20221219094244100026
         * ShippingMethod : 1
         * ShippingMethodName : 自提
         * FullName : 郑富贵
         * Phone : 15050668985
         * Address : 福建省莆田市涵江区涵西街道湖园路在世纪名苑-东北门附近
         * SelfDeliveryName :
         * SelfDeliveryPhone :
         * fixmedins_code : H61080200145
         * ExpressCompany :
         * ExpressNo :
         * LogisticsProgressInfo : 自提,预计2022-12-27到达
         * CreateDT : 2022-12-24 19:34:12
         * CreateUser : 99
         * UpdateDT : 2022-12-24 19:34:12
         * UpdateUser : 99
         * UserId : eb6e9c20-ba61-4e94-ad58-550a6dd91f93
         * UserName : 99
         * DeliveryTime : 2022-12-24 19:34:12
         * ConfirmationTime : 2022-12-24 19:34:12
         * ConfirmationInfo :
         * IsConfirm : 0
         */

        private String LogisticsProgressId;
        private String OrderNo;
        private String ShippingMethod;
        private String ShippingMethodName;
        private String FullName;
        private String Phone;
        private String Address;
        private String SelfDeliveryName;
        private String SelfDeliveryPhone;
        private String fixmedins_code;
        private String ExpressCompany;
        private String ExpressNo;
        private String LogisticsProgressInfo;
        private String CreateDT;
        private String CreateUser;
        private String UpdateDT;
        private String UpdateUser;
        private String UserId;
        private String UserName;
        private String DeliveryTime;
        private String ConfirmationTime;
        private String ConfirmationInfo;
        private String IsConfirm;
        //是否物流（0、客户自提1、商家自配 2、商家快递）
        private String IsLogistics;

        public String getLogisticsProgressId() {
            return LogisticsProgressId;
        }

        public void setLogisticsProgressId(String LogisticsProgressId) {
            this.LogisticsProgressId = LogisticsProgressId;
        }

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getShippingMethod() {
            return ShippingMethod;
        }

        public void setShippingMethod(String ShippingMethod) {
            this.ShippingMethod = ShippingMethod;
        }

        public String getShippingMethodName() {
            return ShippingMethodName;
        }

        public void setShippingMethodName(String ShippingMethodName) {
            this.ShippingMethodName = ShippingMethodName;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String Phone) {
            this.Phone = Phone;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getSelfDeliveryName() {
            return SelfDeliveryName;
        }

        public void setSelfDeliveryName(String SelfDeliveryName) {
            this.SelfDeliveryName = SelfDeliveryName;
        }

        public String getSelfDeliveryPhone() {
            return SelfDeliveryPhone;
        }

        public void setSelfDeliveryPhone(String SelfDeliveryPhone) {
            this.SelfDeliveryPhone = SelfDeliveryPhone;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getExpressCompany() {
            return ExpressCompany;
        }

        public void setExpressCompany(String ExpressCompany) {
            this.ExpressCompany = ExpressCompany;
        }

        public String getExpressNo() {
            return ExpressNo;
        }

        public void setExpressNo(String ExpressNo) {
            this.ExpressNo = ExpressNo;
        }

        public String getLogisticsProgressInfo() {
            return LogisticsProgressInfo;
        }

        public void setLogisticsProgressInfo(String LogisticsProgressInfo) {
            this.LogisticsProgressInfo = LogisticsProgressInfo;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String CreateUser) {
            this.CreateUser = CreateUser;
        }

        public String getUpdateDT() {
            return UpdateDT;
        }

        public void setUpdateDT(String UpdateDT) {
            this.UpdateDT = UpdateDT;
        }

        public String getUpdateUser() {
            return UpdateUser;
        }

        public void setUpdateUser(String UpdateUser) {
            this.UpdateUser = UpdateUser;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getDeliveryTime() {
            return DeliveryTime;
        }

        public void setDeliveryTime(String DeliveryTime) {
            this.DeliveryTime = DeliveryTime;
        }

        public String getConfirmationTime() {
            return ConfirmationTime;
        }

        public void setConfirmationTime(String ConfirmationTime) {
            this.ConfirmationTime = ConfirmationTime;
        }

        public String getConfirmationInfo() {
            return ConfirmationInfo;
        }

        public void setConfirmationInfo(String ConfirmationInfo) {
            this.ConfirmationInfo = ConfirmationInfo;
        }

        public String getIsConfirm() {
            return IsConfirm;
        }

        public void setIsConfirm(String IsConfirm) {
            this.IsConfirm = IsConfirm;
        }

        public String getIsLogistics() {
            return IsLogistics;
        }

        public void setIsLogistics(String isLogistics) {
            IsLogistics = isLogistics;
        }
    }

    //查询物流
    public static void sendLogisticsRequest(final String TAG, String OrderNo,final CustomerJsonCallBack<LogisticsModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STORE_QUERYLOGISTICSPROGRESS_URL, jsonObject.toJSONString(), callback);
    }
}
