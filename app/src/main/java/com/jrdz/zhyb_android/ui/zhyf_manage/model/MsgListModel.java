package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-18
 * 描    述：
 * ================================================
 */
public class MsgListModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-12-01 19:01:54
     * data : [{"MessageId":1,"Type":3,"IsRead":0,"MessageContent":"订单测试","No":"20221125182221100001","fixmedins_code":"H61080200145","UserId":"4249680b-2f99-4fc3-9e1f-d7efcad7d7ef","UserName":"15060338985","CreateDT":"2022-11-25 18:22:21"},{"MessageId":2,"Type":2,"IsRead":0,"MessageContent":"商品测试","No":"300009","fixmedins_code":"H61080200145","UserId":"4249680b-2f99-4fc3-9e1f-d7efcad7d7ef","UserName":"15060338985","CreateDT":"2022-11-25 18:22:21"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * MessageId : 1
         * Type : 3
         * IsRead : 1
         * MessageTitle : 未支付订单
         * MessageContent : 订单测试
         * No : 20221125182221100001
         * fixmedins_code : H61080200145
         * UserId : 4249680b-2f99-4fc3-9e1f-d7efcad7d7ef
         * UserName : 15060338985
         * CreateDT : 2022-11-25 18:22:21
         */

        private String MessageId;
        private String Type;
        private String IsRead;
        private String MessageTitle;
        private String MessageContent;
        private String No;
        private String fixmedins_code;
        private String UserId;
        private String UserName;
        private String CreateDT;

        public String getMessageId() {
            return MessageId;
        }

        public void setMessageId(String MessageId) {
            this.MessageId = MessageId;
        }

        public String getType() {
            return Type;
        }

        public void setType(String Type) {
            this.Type = Type;
        }

        public String getIsRead() {
            return IsRead;
        }

        public void setIsRead(String IsRead) {
            this.IsRead = IsRead;
        }

        public String getMessageTitle() {
            return MessageTitle;
        }

        public void setMessageTitle(String MessageTitle) {
            this.MessageTitle = MessageTitle;
        }

        public String getMessageContent() {
            return MessageContent;
        }

        public void setMessageContent(String MessageContent) {
            this.MessageContent = MessageContent;
        }

        public String getNo() {
            return No;
        }

        public void setNo(String No) {
            this.No = No;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }
    }

    //充值记录列表
    public static void sendMsgListRequest(final String TAG, String pageindex, String pagesize,final CustomerJsonCallBack<MsgListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_MESSAGELIST_URL, jsonObject.toJSONString(), callback);
    }

    //用户端消息列表
    public static void sendUserMsgListRequest(final String TAG, String pageindex, String pagesize,final CustomerJsonCallBack<MsgListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_USERMESSAGELIST_URL, jsonObject.toJSONString(), callback);
    }
}
