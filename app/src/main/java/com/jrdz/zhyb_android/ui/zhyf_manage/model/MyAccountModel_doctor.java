package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-02-13
 * 描    述：
 * ================================================
 */
public class MyAccountModel_doctor {

    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-02-13 10:59:32
     * data : {"totalItems1":"0","totalItems2":"0"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private String dr_code;
        private String AccountBalance;//账号余额
        private String onlinePrescriptionCount;//在线开处方数
        private String onlinePrescriptionPrice;//单价
        private String onlinePrescriptionAmount;//开处方金额

        public String getDr_code() {
            return dr_code;
        }

        public void setDr_code(String dr_code) {
            this.dr_code = dr_code;
        }

        public String getAccountBalance() {
            return AccountBalance;
        }

        public void setAccountBalance(String accountBalance) {
            AccountBalance = accountBalance;
        }

        public String getOnlinePrescriptionCount() {
            return onlinePrescriptionCount;
        }

        public void setOnlinePrescriptionCount(String onlinePrescriptionCount) {
            this.onlinePrescriptionCount = onlinePrescriptionCount;
        }

        public String getOnlinePrescriptionPrice() {
            return onlinePrescriptionPrice;
        }

        public void setOnlinePrescriptionPrice(String onlinePrescriptionPrice) {
            this.onlinePrescriptionPrice = onlinePrescriptionPrice;
        }

        public String getOnlinePrescriptionAmount() {
            return onlinePrescriptionAmount;
        }

        public void setOnlinePrescriptionAmount(String onlinePrescriptionAmount) {
            this.onlinePrescriptionAmount = onlinePrescriptionAmount;
        }
    }

    //我的账户-医师
    public static void sendMyAccountRequest_doctor(final String TAG, String month, final CustomerJsonCallBack<MyAccountModel_doctor> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("month", month);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_DOCTORCASHACCOUNT_URL, jsonObject.toJSONString(), callback);
    }
}
