package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-01
 * 描    述：
 * ================================================
 */
public class MyAccountModel_mana {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-12-01 09:09:40
     * data : {"CashAccountId":1,"AccountBalance":100,"TotalAccumulatedIncome":200,"TotalMedicareIncome":50,"TotalOnlineIncome":150,"DepositBalance":1000,"UserId":"4249680b-2f99-4fc3-9e1f-d7efcad7d7ef","UserName":"99","fixmedins_code":"H61080200145"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * CashAccountId : 1
         * AccountBalance : 100
         * TotalAccumulatedIncome : 200
         * TotalMedicareIncome : 50
         * TotalOnlineIncome : 150
         * DepositBalance : 1000
         * UserId : 4249680b-2f99-4fc3-9e1f-d7efcad7d7ef
         * UserName : 99
         */

        private String CashAccountId;
        private String AccountBalance;
        private String TotalAccumulatedIncome;
        private String TotalMedicareIncome;
        private String TotalOnlineIncome;
        private String DepositBalance;
        private String UserName;
        private String IsClick;
        private String IsDepositClick;
        private String TotalEnterpriseFundAmount;//企业基金总收入

        public String getCashAccountId() {
            return CashAccountId;
        }

        public void setCashAccountId(String CashAccountId) {
            this.CashAccountId = CashAccountId;
        }

        public String getAccountBalance() {
            return AccountBalance;
        }

        public void setAccountBalance(String AccountBalance) {
            this.AccountBalance = AccountBalance;
        }

        public String getTotalAccumulatedIncome() {
            return TotalAccumulatedIncome;
        }

        public void setTotalAccumulatedIncome(String TotalAccumulatedIncome) {
            this.TotalAccumulatedIncome = TotalAccumulatedIncome;
        }

        public String getTotalMedicareIncome() {
            return TotalMedicareIncome;
        }

        public void setTotalMedicareIncome(String TotalMedicareIncome) {
            this.TotalMedicareIncome = TotalMedicareIncome;
        }

        public String getTotalOnlineIncome() {
            return TotalOnlineIncome;
        }

        public void setTotalOnlineIncome(String TotalOnlineIncome) {
            this.TotalOnlineIncome = TotalOnlineIncome;
        }

        public String getDepositBalance() {
            return DepositBalance;
        }

        public void setDepositBalance(String DepositBalance) {
            this.DepositBalance = DepositBalance;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String userName) {
            UserName = userName;
        }

        public String getIsClick() {
            return IsClick;
        }

        public void setIsClick(String isClick) {
            IsClick = isClick;
        }

        public String getIsDepositClick() {
            return IsDepositClick;
        }

        public void setIsDepositClick(String isDepositClick) {
            IsDepositClick = isDepositClick;
        }

        public String getTotalEnterpriseFundAmount() {
            return TotalEnterpriseFundAmount;
        }

        public void setTotalEnterpriseFundAmount(String totalEnterpriseFundAmount) {
            TotalEnterpriseFundAmount = totalEnterpriseFundAmount;
        }
    }

    //我的账户
    public static void sendMyAccountRequest_mana(final String TAG,final CustomerJsonCallBack<MyAccountModel_mana> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_CASHACCOUNT_URL, "", callback);
    }
}
