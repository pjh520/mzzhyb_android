package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-02-13
 * 描    述：
 * ================================================
 */
public class MyIssuedPrescrCountModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-02-13 10:59:32
     * data : {"totalItems1":"0","totalItems2":"0"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * totalItems1 : 0
         * totalItems2 : 0
         */

        private String totalItems1;
        private String totalItems2;

        public String getTotalItems1() {
            return totalItems1;
        }

        public void setTotalItems1(String totalItems1) {
            this.totalItems1 = totalItems1;
        }

        public String getTotalItems2() {
            return totalItems2;
        }

        public void setTotalItems2(String totalItems2) {
            this.totalItems2 = totalItems2;
        }
    }

    //我开具的处方 在线处方
    public static void sendDoctorElectronicPrescriptionCountRequest(final String TAG, String begntime,String enddate,
                                                                    final CustomerJsonCallBack<MyIssuedPrescrCountModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("begntime", begntime);
        jsonObject.put("enddate", enddate);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STORE_GETDOCTORELECTRONICPRESCRIPTIONCOUNT_URL, jsonObject.toJSONString(), callback);
    }
}
