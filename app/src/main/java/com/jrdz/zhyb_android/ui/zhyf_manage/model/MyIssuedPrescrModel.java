package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-26
 * 描    述：
 * ================================================
 */
public class MyIssuedPrescrModel {


    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-02-13 09:52:59
     * data : [{"AccessoryUrl":"","StatusName":"结算成功","ElectronicPrescriptionId":301,"ipt_otp_no":"","psn_no":"","fixmedins_code":"H61080200145","CreateDT":"2023-01-05 17:32:02","UpdateDT":"2023-01-05 17:32:02","CreateUser":"D234567891234","UpdateUser":"D234567891234","settlementClassification":1,"electronicPrescriptionNo":"202301051732029891","PrescriptionType":1,"wmcpmDiagResults":"","wmcpmAmount":2,"cmDiagResults":"","cmDrugNum":0,"cmused_mtd":"","cmAmount":0,"cmRemark":"","unt":0,"accessoryId":"00000000-0000-0000-0000-000000000000","IsSubmitted":0,"OrderNo":"20230105165329100047"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String ElectronicPrescriptionId;
        private String electronicPrescriptionNo;
        private String CreateDT;
        private String psn_name;
        private String sex;
        private String age;
        private String contact;
        private String OrderNo;

        public String getElectronicPrescriptionId() {
            return ElectronicPrescriptionId;
        }

        public void setElectronicPrescriptionId(String ElectronicPrescriptionId) {
            this.ElectronicPrescriptionId = ElectronicPrescriptionId;
        }

        public String getElectronicPrescriptionNo() {
            return electronicPrescriptionNo;
        }

        public void setElectronicPrescriptionNo(String electronicPrescriptionNo) {
            this.electronicPrescriptionNo = electronicPrescriptionNo;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getPsn_name() {
            return psn_name;
        }

        public void setPsn_name(String psn_name) {
            this.psn_name = psn_name;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getContact() {
            return contact;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String orderNo) {
            OrderNo = orderNo;
        }
    }

    //我开具的处方 店铺处方
    public static void sendStoreElectronicPrescriptionListRequest(final String TAG, String pageindex, String pagesize,String begntime,String enddate,
                                                                  final CustomerJsonCallBack<MyIssuedPrescrModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("begntime", begntime);
        jsonObject.put("enddate", enddate);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STORE_ELECTRONICPRESCRIPTIONLIST_URL, jsonObject.toJSONString(), callback);
    }

    //我开具的处方 在线处方
    public static void sendOnlineElectronicPrescriptionListRequest(final String TAG, String pageindex, String pagesize,String begntime,String enddate,
                                                                   final CustomerJsonCallBack<MyIssuedPrescrModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("begntime", begntime);
        jsonObject.put("enddate", enddate);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STORE_ONLINEELECTRONICPRESCRIPTIONLIST_URL, jsonObject.toJSONString(), callback);
    }
}
