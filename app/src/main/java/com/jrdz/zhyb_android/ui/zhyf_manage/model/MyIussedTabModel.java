package com.jrdz.zhyb_android.ui.zhyf_manage.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-26
 * 描    述：
 * ================================================
 */
public class MyIussedTabModel {
    private String id;
    private String text;
    private String num;

    public MyIussedTabModel(String id, String text, String num) {
        this.id = id;
        this.text = text;
        this.num = num;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }
}
