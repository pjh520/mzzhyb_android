package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-02
 * 描    述：
 * ================================================
 */
public class MyQualiModel {
    /**
     * code : 1
     * msg : 更新成功
     * server_time : 2022-12-02 15:53:15
     * data : {"fixmedins_code":"H61080200145","BusinessLicenseAccessoryUrl":"~/App_Upload/Storage/Medicare_Ticket/709c7f4b-c79c-4f6f-8353-fa6721be8e64/4315404c-c9a4-4919-9245-6edb8069902c.jpg","DrugBLAccessoryUrl":"~/App_Upload/Storage/Medicare_Ticket/5957486e-bcf6-4ad7-bc8f-73606e45937c/fe50d056-93c0-4a04-87ae-75197fc4671b.jpg","MedicalDeviceBLAccessoryUrl":"","GSCertificateAccessoryUrl":""}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * fixmedins_code : H61080200145
         * BusinessLicenseAccessoryUrl : ~/App_Upload/Storage/Medicare_Ticket/709c7f4b-c79c-4f6f-8353-fa6721be8e64/4315404c-c9a4-4919-9245-6edb8069902c.jpg
         * DrugBLAccessoryUrl : ~/App_Upload/Storage/Medicare_Ticket/5957486e-bcf6-4ad7-bc8f-73606e45937c/fe50d056-93c0-4a04-87ae-75197fc4671b.jpg
         * MedicalDeviceBLAccessoryUrl :
         * GSCertificateAccessoryUrl :
         */

        private String fixmedins_code;
        private String BusinessLicenseAccessoryUrl;
        private String DrugBLAccessoryUrl;
        private String MedicalDeviceBLAccessoryUrl;
        private String GSCertificateAccessoryUrl;

        private String BusinessLicenseAccessoryId;
        private String DrugBLAccessoryId;
        private String MedicalDeviceBLAccessoryId;
        private String GSCertificateAccessoryId;

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getBusinessLicenseAccessoryUrl() {
            return BusinessLicenseAccessoryUrl;
        }

        public void setBusinessLicenseAccessoryUrl(String BusinessLicenseAccessoryUrl) {
            this.BusinessLicenseAccessoryUrl = BusinessLicenseAccessoryUrl;
        }

        public String getDrugBLAccessoryUrl() {
            return DrugBLAccessoryUrl;
        }

        public void setDrugBLAccessoryUrl(String DrugBLAccessoryUrl) {
            this.DrugBLAccessoryUrl = DrugBLAccessoryUrl;
        }

        public String getMedicalDeviceBLAccessoryUrl() {
            return MedicalDeviceBLAccessoryUrl;
        }

        public void setMedicalDeviceBLAccessoryUrl(String MedicalDeviceBLAccessoryUrl) {
            this.MedicalDeviceBLAccessoryUrl = MedicalDeviceBLAccessoryUrl;
        }

        public String getGSCertificateAccessoryUrl() {
            return GSCertificateAccessoryUrl;
        }

        public void setGSCertificateAccessoryUrl(String GSCertificateAccessoryUrl) {
            this.GSCertificateAccessoryUrl = GSCertificateAccessoryUrl;
        }

        public String getBusinessLicenseAccessoryId() {
            return BusinessLicenseAccessoryId;
        }

        public void setBusinessLicenseAccessoryId(String businessLicenseAccessoryId) {
            BusinessLicenseAccessoryId = businessLicenseAccessoryId;
        }

        public String getDrugBLAccessoryId() {
            return DrugBLAccessoryId;
        }

        public void setDrugBLAccessoryId(String drugBLAccessoryId) {
            DrugBLAccessoryId = drugBLAccessoryId;
        }

        public String getMedicalDeviceBLAccessoryId() {
            return MedicalDeviceBLAccessoryId;
        }

        public void setMedicalDeviceBLAccessoryId(String medicalDeviceBLAccessoryId) {
            MedicalDeviceBLAccessoryId = medicalDeviceBLAccessoryId;
        }

        public String getGSCertificateAccessoryId() {
            return GSCertificateAccessoryId;
        }

        public void setGSCertificateAccessoryId(String GSCertificateAccessoryId) {
            this.GSCertificateAccessoryId = GSCertificateAccessoryId;
        }
    }

    //我的店铺资质
    public static void sendMyQualiRequest(final String TAG,final CustomerJsonCallBack<MyQualiModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_GETSTORECREDENTIALS_URL,"",callback);
    }
}
