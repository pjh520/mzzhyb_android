package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-23
 * 描    述：
 * ================================================
 */
public class OpenPrescModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-28 09:33:43
     * totalItems : 28
     * data : [{"OrderNo":"20221125182221100001","ShippingMethod":1,"ShippingMethodName":"自提","FullName":"","Phone":"","Address":"","PurchasingDrugsMethod":1,"PurchasingDrugsMethodName":"自助购药","GoodsNum":2,"TotalAmount":1.8,"OrderStatus":1,"fixmedins_code":"H61080200145","StoreName":"测试店铺","Telephone":"15060338986","Wechat":"15060338986","setl_id":"","LogisticsFee":0,"CreateDT":"2022-11-25 18:22:21","OrderGoods":[{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1}]}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * OrderNo : 20221125182221100001
         * ShippingMethod : 1
         * ShippingMethodName : 自提
         * FullName :
         * Phone :
         * Address :
         * PurchasingDrugsMethod : 1
         * PurchasingDrugsMethodName : 自助购药
         * GoodsNum : 2
         * TotalAmount : 1.8
         * OrderStatus : 1
         * fixmedins_code : H61080200145
         * StoreName : 测试店铺
         * Telephone : 15060338986
         * Wechat : 15060338986
         * setl_id :
         * LogisticsFee : 0
         * CreateDT : 2022-11-25 18:22:21
         * OrderGoods : [{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1}]
         */

        private String OrderNo;
        private String ShippingMethod;
        private String ShippingMethodName;
        private String FullName;
        private String Phone;
        private String Location;
        private String Address;
        private String PurchasingDrugsMethod;
        private String PurchasingDrugsMethodName;
        private String GoodsNum;
        private String TotalAmount;
        private String OrderStatus;
        private String fixmedins_code;
        private String StoreName;
        private String Telephone;
        private String Wechat;
        private String setl_id;
        private String LogisticsFee;
        private String CreateDT;
        private String StoreAccessoryUrl;
        private String OnlineAmount;
        private String Buyer;
        private String PrescriptionPrice;
        private String sex;
        private String age;
        private String AssociatedDiseases;
        private String Symptom;
        private ArrayList<OrderDetailModel.DataBean.OrderGoodsBean> OrderGoods;

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getShippingMethod() {
            return ShippingMethod;
        }

        public void setShippingMethod(String ShippingMethod) {
            this.ShippingMethod = ShippingMethod;
        }

        public String getShippingMethodName() {
            return ShippingMethodName;
        }

        public void setShippingMethodName(String ShippingMethodName) {
            this.ShippingMethodName = ShippingMethodName;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String Phone) {
            this.Phone = Phone;
        }

        public String getLocation() {
            return Location;
        }

        public void setLocation(String location) {
            Location = location;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getPurchasingDrugsMethod() {
            return PurchasingDrugsMethod;
        }

        public void setPurchasingDrugsMethod(String PurchasingDrugsMethod) {
            this.PurchasingDrugsMethod = PurchasingDrugsMethod;
        }

        public String getPurchasingDrugsMethodName() {
            return PurchasingDrugsMethodName;
        }

        public void setPurchasingDrugsMethodName(String PurchasingDrugsMethodName) {
            this.PurchasingDrugsMethodName = PurchasingDrugsMethodName;
        }

        public String getGoodsNum() {
            return GoodsNum;
        }

        public void setGoodsNum(String GoodsNum) {
            this.GoodsNum = GoodsNum;
        }

        public String getTotalAmount() {
            return TotalAmount;
        }

        public void setTotalAmount(String TotalAmount) {
            this.TotalAmount = TotalAmount;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public String getTelephone() {
            return Telephone;
        }

        public void setTelephone(String Telephone) {
            this.Telephone = Telephone;
        }

        public String getWechat() {
            return Wechat;
        }

        public void setWechat(String Wechat) {
            this.Wechat = Wechat;
        }

        public String getSetl_id() {
            return setl_id;
        }

        public void setSetl_id(String setl_id) {
            this.setl_id = setl_id;
        }

        public String getLogisticsFee() {
            return LogisticsFee;
        }

        public void setLogisticsFee(String LogisticsFee) {
            this.LogisticsFee = LogisticsFee;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getStoreAccessoryUrl() {
            return StoreAccessoryUrl;
        }

        public void setStoreAccessoryUrl(String storeAccessoryUrl) {
            StoreAccessoryUrl = storeAccessoryUrl;
        }

        public String getOnlineAmount() {
            return OnlineAmount;
        }

        public void setOnlineAmount(String onlineAmount) {
            OnlineAmount = onlineAmount;
        }

        public String getBuyer() {
            return Buyer;
        }

        public void setBuyer(String buyer) {
            Buyer = buyer;
        }

        public String getPrescriptionPrice() {
            return PrescriptionPrice;
        }

        public void setPrescriptionPrice(String prescriptionPrice) {
            PrescriptionPrice = prescriptionPrice;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getAssociatedDiseases() {
            return AssociatedDiseases;
        }

        public void setAssociatedDiseases(String associatedDiseases) {
            AssociatedDiseases = associatedDiseases;
        }

        public String getSymptom() {
            return Symptom;
        }

        public void setSymptom(String symptom) {
            Symptom = symptom;
        }

        public ArrayList<OrderDetailModel.DataBean.OrderGoodsBean> getOrderGoods() {
            return OrderGoods;
        }

        public void setOrderGoods(ArrayList<OrderDetailModel.DataBean.OrderGoodsBean> OrderGoods) {
            this.OrderGoods = OrderGoods;
        }
    }

    //带处方订单列表
    public static void sendOrderWithPrescriptionlistRequest(final String TAG,String pageindex,String pagesize,String searchname,
                                                            final CustomerJsonCallBack<OpenPrescModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("searchname", searchname);
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STORE_ORDERWITHPRESCRIPTIONLIST_URL, jsonObject.toJSONString(), callback);
    }
}
