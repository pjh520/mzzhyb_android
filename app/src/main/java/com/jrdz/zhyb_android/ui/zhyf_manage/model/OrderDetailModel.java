package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-09
 * 描    述：
 * ================================================
 */
public class OrderDetailModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-12-09 16:57:50
     * data : {"OrderNo":"20221209125637100015","ShippingMethod":2,"ShippingMethodName":"商家配送","FullName":"彭俊鸿","Phone":"15060338985","Address":"新度镇港利村","PurchasingDrugsMethod":1,"PurchasingDrugsMethodName":"自助购药","GoodsNum":2,"TotalAmount":1.8,"OrderStatus":1,"fixmedins_code":"H61080200145","StoreName":"测试店铺","Telephone":"15060338986","Wechat":"15060338986","setl_id":"","LogisticsFee":0,"CreateDT":"2022-12-09 12:56:37","medfee_sumamt":0.9,"fund_pay_sumamt":0,"psn_cash_pay":0,"acct_pay":0,"ipt_otp_no":"","BuyerWeChat":"买家微信123","BuyerMessage":"卖家留言1234","StoreAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/4cf4e4f1-4d0c-486f-920a-baf79a13d9f3/25fb5b5e-057f-4cf2-a97a-7c86744c2d3d.jpg","balc":0,"OnlineAmount":0.9,"IsPrescription":0,"Location":"内蒙古自治区呼和浩特市市辖区","LastPaytTime":"2022-12-09 12:21:18","IsTake":0,"IsAllergy":0,"IsUntowardReaction":0,"OrderGoods":[{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":0.9,"fixmedins_code":"H61080200145","ItemType":1,"BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/cc9dae19-400f-4790-a302-22e69b867388/c9061f61-88b6-4df3-aba9-21d5b8b64546.jpg"}]}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * OrderNo : 20221209125637100015
         * ShippingMethod : 2
         * ShippingMethodName : 商家配送
         * FullName : 彭俊鸿
         * Phone : 15060338985
         * Address : 新度镇港利村
         * PurchasingDrugsMethod : 1
         * PurchasingDrugsMethodName : 自助购药
         * GoodsNum : 2
         * TotalAmount : 1.8
         * OrderStatus : 1
         * fixmedins_code : H61080200145
         * StoreName : 测试店铺
         * Telephone : 15060338986
         * Wechat : 15060338986
         * setl_id :
         * LogisticsFee : 0
         * CreateDT : 2022-12-09 12:56:37
         * medfee_sumamt : 0.9
         * fund_pay_sumamt : 0
         * psn_cash_pay : 0
         * acct_pay : 0
         * ipt_otp_no :
         * BuyerWeChat : 买家微信123
         * BuyerMessage : 卖家留言1234
         * StoreAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/4cf4e4f1-4d0c-486f-920a-baf79a13d9f3/25fb5b5e-057f-4cf2-a97a-7c86744c2d3d.jpg
         * balc : 0
         * OnlineAmount : 0.9
         * IsPrescription : 0
         * Location : 内蒙古自治区呼和浩特市市辖区
         * LastPaytTime : 2022-12-09 12:21:18
         * IsTake : 0
         * IsAllergy : 0
         * IsUntowardReaction : 0
         * OrderGoods : [{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":0.9,"fixmedins_code":"H61080200145","ItemType":1,"BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/cc9dae19-400f-4790-a302-22e69b867388/c9061f61-88b6-4df3-aba9-21d5b8b64546.jpg"}]
         */

        private String OrderNo;
        private String ShippingMethod;
        private String ShippingMethodName;
        private String FullName;
        private String Phone;
        private String Address;
        private String PurchasingDrugsMethod;
        private String PurchasingDrugsMethodName;
        private String GoodsNum;
        private String TotalAmount;
        private String OrderStatus;
        private String fixmedins_code;
        private String StoreName;
        private String Telephone;
        private String Wechat;
        private String setl_id;
        private String LogisticsFee;
        private String CreateDT;
        private String medfee_sumamt;
        private String fund_pay_sumamt;
        private String psn_cash_pay;
        private String acct_pay;
        private String ipt_otp_no;
        private String BuyerWeChat;
        private String BuyerMessage;
        private String StoreAccessoryUrl;
        private String balc;
        private String OnlineAmount;
        private String IsPrescription;
        private String Location;
        private String LastPaytTime;
        private String IsTake;
        private String IsAllergy;
        private String IsUntowardReaction;
        private String psn_no;
        private String psn_name;
        private String mdtrt_cert_type;
        private String mdtrt_cert_no;
        private String insutype;
        //订单取消时间
        private String CancelTime;
        //订单退款编号
        private String OrderRefundNo;
        //订单退款流水号
        private String OrderRefundSerialNo;
        //医保支付金额
        private String MedicareAmount;
        //医保结算状态(0、 未结算 1、结算成功）
        private String SettlementStatus;
        //订单发货时间
        private String DeliveryTime;
        //订单确认时间
        private String ConfirmTime;
        //是否已退款(0、 未退款 1、已退款）
        private String IsRefunded;
        //是否物流（0、客户自提1、商家自配 2、商家快递）
        private String IsLogistics;
        //是否允许企业基金支付（0不允许 1允许）
        private String IsEnterpriseFundPay;
        //企业基金余额
        private String EnterpriseFundAmount;
        //企业基金支付金额
        private String EnterpriseFundPay;
        //企业基金支付状态
        private String EnterpriseFundPayStatus;//(0未支付 1已支付)
        //医保基金支付总额(预结算）
        private String pre_fund_pay_sumamt;
        //医保个人账户余额(预结算）
        private String pre_balc;
        //取消订单是否查看物流（0、可以1、不可以）
        private String IsViewLogistics;

        private String AreaName;//省份
        private String CityName;//市区
        private String Latitude;//商家-纬度
        private String Longitude;//商家-经度
        private String DiscountAmount;//优惠金额
        private String Discount;//优惠折扣
        private String OriginalTotalAmount;//原始订单总额
        private String IsThirdPartyPres;//是否第三方开处方（1是，0否）

        private ArrayList<OrderGoodsBean> OrderGoods;

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getShippingMethod() {
            return ShippingMethod;
        }

        public void setShippingMethod(String ShippingMethod) {
            this.ShippingMethod = ShippingMethod;
        }

        public String getShippingMethodName() {
            return ShippingMethodName;
        }

        public void setShippingMethodName(String ShippingMethodName) {
            this.ShippingMethodName = ShippingMethodName;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String Phone) {
            this.Phone = Phone;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getPurchasingDrugsMethod() {
            return PurchasingDrugsMethod;
        }

        public void setPurchasingDrugsMethod(String PurchasingDrugsMethod) {
            this.PurchasingDrugsMethod = PurchasingDrugsMethod;
        }

        public String getPurchasingDrugsMethodName() {
            return PurchasingDrugsMethodName;
        }

        public void setPurchasingDrugsMethodName(String PurchasingDrugsMethodName) {
            this.PurchasingDrugsMethodName = PurchasingDrugsMethodName;
        }

        public String getGoodsNum() {
            return GoodsNum;
        }

        public void setGoodsNum(String GoodsNum) {
            this.GoodsNum = GoodsNum;
        }

        public String getTotalAmount() {
            return TotalAmount;
        }

        public void setTotalAmount(String TotalAmount) {
            this.TotalAmount = TotalAmount;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public String getTelephone() {
            return Telephone;
        }

        public void setTelephone(String Telephone) {
            this.Telephone = Telephone;
        }

        public String getWechat() {
            return Wechat;
        }

        public void setWechat(String Wechat) {
            this.Wechat = Wechat;
        }

        public String getSetl_id() {
            return setl_id;
        }

        public void setSetl_id(String setl_id) {
            this.setl_id = setl_id;
        }

        public String getLogisticsFee() {
            return LogisticsFee;
        }

        public void setLogisticsFee(String LogisticsFee) {
            this.LogisticsFee = LogisticsFee;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getMedfee_sumamt() {
            return medfee_sumamt;
        }

        public void setMedfee_sumamt(String medfee_sumamt) {
            this.medfee_sumamt = medfee_sumamt;
        }

        public String getFund_pay_sumamt() {
            return fund_pay_sumamt;
        }

        public void setFund_pay_sumamt(String fund_pay_sumamt) {
            this.fund_pay_sumamt = fund_pay_sumamt;
        }

        public String getPsn_cash_pay() {
            return psn_cash_pay;
        }

        public void setPsn_cash_pay(String psn_cash_pay) {
            this.psn_cash_pay = psn_cash_pay;
        }

        public String getAcct_pay() {
            return acct_pay;
        }

        public void setAcct_pay(String acct_pay) {
            this.acct_pay = acct_pay;
        }

        public String getIpt_otp_no() {
            return ipt_otp_no;
        }

        public void setIpt_otp_no(String ipt_otp_no) {
            this.ipt_otp_no = ipt_otp_no;
        }

        public String getBuyerWeChat() {
            return BuyerWeChat;
        }

        public void setBuyerWeChat(String BuyerWeChat) {
            this.BuyerWeChat = BuyerWeChat;
        }

        public String getBuyerMessage() {
            return BuyerMessage;
        }

        public void setBuyerMessage(String BuyerMessage) {
            this.BuyerMessage = BuyerMessage;
        }

        public String getStoreAccessoryUrl() {
            return StoreAccessoryUrl;
        }

        public void setStoreAccessoryUrl(String StoreAccessoryUrl) {
            this.StoreAccessoryUrl = StoreAccessoryUrl;
        }

        public String getBalc() {
            return balc;
        }

        public void setBalc(String balc) {
            this.balc = balc;
        }

        public String getOnlineAmount() {
            return OnlineAmount;
        }

        public void setOnlineAmount(String OnlineAmount) {
            this.OnlineAmount = OnlineAmount;
        }

        public String getIsPrescription() {
            return IsPrescription;
        }

        public void setIsPrescription(String IsPrescription) {
            this.IsPrescription = IsPrescription;
        }

        public String getLocation() {
            return Location;
        }

        public void setLocation(String Location) {
            this.Location = Location;
        }

        public String getLastPaytTime() {
            return LastPaytTime;
        }

        public void setLastPaytTime(String LastPaytTime) {
            this.LastPaytTime = LastPaytTime;
        }

        public String getIsTake() {
            return IsTake;
        }

        public void setIsTake(String IsTake) {
            this.IsTake = IsTake;
        }

        public String getIsAllergy() {
            return IsAllergy;
        }

        public void setIsAllergy(String IsAllergy) {
            this.IsAllergy = IsAllergy;
        }

        public String getIsUntowardReaction() {
            return IsUntowardReaction;
        }

        public void setIsUntowardReaction(String IsUntowardReaction) {
            this.IsUntowardReaction = IsUntowardReaction;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getPsn_name() {
            return psn_name;
        }

        public void setPsn_name(String psn_name) {
            this.psn_name = psn_name;
        }

        public String getMdtrt_cert_type() {
            return mdtrt_cert_type;
        }

        public void setMdtrt_cert_type(String mdtrt_cert_type) {
            this.mdtrt_cert_type = mdtrt_cert_type;
        }

        public String getMdtrt_cert_no() {
            return mdtrt_cert_no;
        }

        public void setMdtrt_cert_no(String mdtrt_cert_no) {
            this.mdtrt_cert_no = mdtrt_cert_no;
        }

        public String getInsutype() {
            return insutype;
        }

        public void setInsutype(String insutype) {
            this.insutype = insutype;
        }

        public String getCancelTime() {
            return CancelTime;
        }

        public void setCancelTime(String cancelTime) {
            CancelTime = cancelTime;
        }

        public String getOrderRefundNo() {
            return OrderRefundNo;
        }

        public void setOrderRefundNo(String orderRefundNo) {
            OrderRefundNo = orderRefundNo;
        }

        public String getOrderRefundSerialNo() {
            return OrderRefundSerialNo;
        }

        public void setOrderRefundSerialNo(String orderRefundSerialNo) {
            OrderRefundSerialNo = orderRefundSerialNo;
        }

        public String getMedicareAmount() {
            return MedicareAmount;
        }

        public void setMedicareAmount(String medicareAmount) {
            MedicareAmount = medicareAmount;
        }

        public String getSettlementStatus() {
            return SettlementStatus;
        }

        public void setSettlementStatus(String settlementStatus) {
            SettlementStatus = settlementStatus;
        }

        public String getDeliveryTime() {
            return DeliveryTime;
        }

        public void setDeliveryTime(String deliveryTime) {
            DeliveryTime = deliveryTime;
        }

        public String getConfirmTime() {
            return ConfirmTime;
        }

        public void setConfirmTime(String confirmTime) {
            ConfirmTime = confirmTime;
        }

        public String getIsRefunded() {
            return IsRefunded;
        }

        public void setIsRefunded(String isRefunded) {
            IsRefunded = isRefunded;
        }

        public String getIsLogistics() {
            return IsLogistics;
        }

        public void setIsLogistics(String isLogistics) {
            IsLogistics = isLogistics;
        }

        public ArrayList<OrderGoodsBean> getOrderGoods() {
            return OrderGoods;
        }

        public void setOrderGoods(ArrayList<OrderGoodsBean> OrderGoods) {
            this.OrderGoods = OrderGoods;
        }

        public String getAreaName() {
            return AreaName;
        }

        public void setAreaName(String areaName) {
            AreaName = areaName;
        }

        public String getCityName() {
            return CityName;
        }

        public void setCityName(String cityName) {
            CityName = cityName;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String latitude) {
            Latitude = latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String longitude) {
            Longitude = longitude;
        }

        public String getIsEnterpriseFundPay() {
            return IsEnterpriseFundPay;
        }

        public void setIsEnterpriseFundPay(String isEnterpriseFundPay) {
            IsEnterpriseFundPay = isEnterpriseFundPay;
        }

        public String getEnterpriseFundAmount() {
            return EnterpriseFundAmount;
        }

        public void setEnterpriseFundAmount(String enterpriseFundAmount) {
            EnterpriseFundAmount = enterpriseFundAmount;
        }

        public String getEnterpriseFundPay() {
            return EnterpriseFundPay;
        }

        public void setEnterpriseFundPay(String enterpriseFundPay) {
            EnterpriseFundPay = enterpriseFundPay;
        }

        public String getEnterpriseFundPayStatus() {
            return EnterpriseFundPayStatus;
        }

        public void setEnterpriseFundPayStatus(String enterpriseFundPayStatus) {
            EnterpriseFundPayStatus = enterpriseFundPayStatus;
        }

        public String getPre_fund_pay_sumamt() {
            return pre_fund_pay_sumamt;
        }

        public void setPre_fund_pay_sumamt(String pre_fund_pay_sumamt) {
            this.pre_fund_pay_sumamt = pre_fund_pay_sumamt;
        }

        public String getPre_balc() {
            return pre_balc;
        }

        public void setPre_balc(String pre_balc) {
            this.pre_balc = pre_balc;
        }

        public String getIsViewLogistics() {
            return IsViewLogistics;
        }

        public void setIsViewLogistics(String isViewLogistics) {
            IsViewLogistics = isViewLogistics;
        }

        public String getDiscountAmount() {
            return DiscountAmount;
        }

        public void setDiscountAmount(String discountAmount) {
            DiscountAmount = discountAmount;
        }

        public String getDiscount() {
            return Discount;
        }

        public void setDiscount(String discount) {
            Discount = discount;
        }

        public String getOriginalTotalAmount() {
            return OriginalTotalAmount;
        }

        public void setOriginalTotalAmount(String originalTotalAmount) {
            OriginalTotalAmount = originalTotalAmount;
        }

        public String getIsThirdPartyPres() {
            return IsThirdPartyPres;
        }

        public void setIsThirdPartyPres(String isThirdPartyPres) {
            IsThirdPartyPres = isThirdPartyPres;
        }

        public static class OrderGoodsBean implements Parcelable {
            /**
             * GoodsName : 地喹氯铵含片
             * GoodsNum : 2
             * Price : 0.9
             * fixmedins_code : H61080200145
             * ItemType : 1
             * BannerAccessoryUrl1 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/cc9dae19-400f-4790-a302-22e69b867388/c9061f61-88b6-4df3-aba9-21d5b8b64546.jpg
             */

            private String GoodsName;
            private int GoodsNum;
            private String Price;
            private String fixmedins_code;
            private String ItemType;
            private String BannerAccessoryUrl1;
            private String Spec;
            private String UsageDosage;
            private String Remark;
            private String IsPlatformDrug;
            //是否允许企业基金支付（0不允许 1允许）
            private String IsEnterpriseFundPay;

            public String getGoodsName() {
                return GoodsName;
            }

            public void setGoodsName(String GoodsName) {
                this.GoodsName = GoodsName;
            }

            public int getGoodsNum() {
                return GoodsNum;
            }

            public void setGoodsNum(int GoodsNum) {
                this.GoodsNum = GoodsNum;
            }

            public String getPrice() {
                return Price;
            }

            public void setPrice(String Price) {
                this.Price = Price;
            }

            public String getFixmedins_code() {
                return fixmedins_code;
            }

            public void setFixmedins_code(String fixmedins_code) {
                this.fixmedins_code = fixmedins_code;
            }

            public String getItemType() {
                return ItemType;
            }

            public void setItemType(String ItemType) {
                this.ItemType = ItemType;
            }

            public String getBannerAccessoryUrl1() {
                return BannerAccessoryUrl1;
            }

            public void setBannerAccessoryUrl1(String BannerAccessoryUrl1) {
                this.BannerAccessoryUrl1 = BannerAccessoryUrl1;
            }

            public String getSpec() {
                return Spec;
            }

            public void setSpec(String spec) {
                Spec = spec;
            }

            public String getUsageDosage() {
                return UsageDosage;
            }

            public void setUsageDosage(String usageDosage) {
                UsageDosage = usageDosage;
            }

            public String getRemark() {
                return Remark;
            }

            public void setRemark(String remark) {
                Remark = remark;
            }

            public String getIsPlatformDrug() {
                return IsPlatformDrug;
            }

            public void setIsPlatformDrug(String isPlatformDrug) {
                IsPlatformDrug = isPlatformDrug;
            }

            public String getIsEnterpriseFundPay() {
                return IsEnterpriseFundPay;
            }

            public void setIsEnterpriseFundPay(String isEnterpriseFundPay) {
                IsEnterpriseFundPay = isEnterpriseFundPay;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.GoodsName);
                dest.writeInt(this.GoodsNum);
                dest.writeString(this.Price);
                dest.writeString(this.fixmedins_code);
                dest.writeString(this.ItemType);
                dest.writeString(this.BannerAccessoryUrl1);
                dest.writeString(this.Spec);
                dest.writeString(this.UsageDosage);
                dest.writeString(this.Remark);
                dest.writeString(this.IsPlatformDrug);
                dest.writeString(this.IsEnterpriseFundPay);
            }

            public OrderGoodsBean() {
            }

            protected OrderGoodsBean(Parcel in) {
                this.GoodsName = in.readString();
                this.GoodsNum = in.readInt();
                this.Price = in.readString();
                this.fixmedins_code = in.readString();
                this.ItemType = in.readString();
                this.BannerAccessoryUrl1 = in.readString();
                this.Spec = in.readString();
                this.UsageDosage = in.readString();
                this.Remark = in.readString();
                this.IsPlatformDrug = in.readString();
                this.IsEnterpriseFundPay = in.readString();
            }

            public static final Parcelable.Creator<OrderGoodsBean> CREATOR = new Parcelable.Creator<OrderGoodsBean>() {
                @Override
                public OrderGoodsBean createFromParcel(Parcel source) {
                    return new OrderGoodsBean(source);
                }

                @Override
                public OrderGoodsBean[] newArray(int size) {
                    return new OrderGoodsBean[size];
                }
            };
        }
    }

    //订单详情
    public static void sendOrderDetailRequest(final String TAG, String OrderNo,final CustomerJsonCallBack<OrderDetailModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);//订单号

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STOREORDERDETAIL_URL, jsonObject.toJSONString(), callback);
    }
}
