package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-09
 * 描    述：
 * ================================================
 */
public class OrderListModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-28 09:33:43
     * totalItems : 28
     * data : [{"OrderNo":"20221125182221100001","ShippingMethod":1,"ShippingMethodName":"自提","FullName":"","Phone":"","Address":"","PurchasingDrugsMethod":1,"PurchasingDrugsMethodName":"自助购药","GoodsNum":2,"TotalAmount":1.8,"OrderStatus":1,"fixmedins_code":"H61080200145","StoreName":"测试店铺","Telephone":"15060338986","Wechat":"15060338986","setl_id":"","LogisticsFee":0,"CreateDT":"2022-11-25 18:22:21","OrderGoods":[{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1}]}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * OrderNo : 20221125182221100001
         * ShippingMethod : 1
         * ShippingMethodName : 自提
         * FullName :
         * Phone :
         * Address :
         * PurchasingDrugsMethod : 1
         * PurchasingDrugsMethodName : 自助购药
         * GoodsNum : 2
         * TotalAmount : 1.8
         * OrderStatus : 1
         * fixmedins_code : H61080200145
         * StoreName : 测试店铺
         * Telephone : 15060338986
         * Wechat : 15060338986
         * setl_id :
         * LogisticsFee : 0
         * CreateDT : 2022-11-25 18:22:21
         * OrderGoods : [{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1},{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1}]
         */

        private String OrderNo;
        private String ShippingMethod;
        private String ShippingMethodName;
        private String FullName;
        private String Phone;
        private String Location;
        private String Address;
        private String PurchasingDrugsMethod;
        private String PurchasingDrugsMethodName;
        private String GoodsNum;
        private String TotalAmount;
        private String OrderStatus;
        private String fixmedins_code;
        private String StoreName;
        private String Telephone;
        private String Wechat;
        private String setl_id;
        private String LogisticsFee;
        private String CreateDT;
        private String StoreAccessoryUrl;
        private String SettlementStatus;
        private String OnlineAmount;
        private String IsPrescription;
        //是否物流（0、客户自提1、商家自配 2、商家快递）
        private String IsLogistics;

        private String AreaName;//省份
        private String CityName;//市区
        private String Latitude;//商家-纬度
        private String Longitude;//商家-经度
        //取消订单是否查看物流（0、可以1、不可以）
        private String IsViewLogistics;
        private String OrderType;//1、普通订单 2、O2O订单
        private String IsThirdPartyPres;//是否第三方开处方（1是，0否）

        private List<OrderGoodsBean> OrderGoods;

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getShippingMethod() {
            return ShippingMethod;
        }

        public void setShippingMethod(String ShippingMethod) {
            this.ShippingMethod = ShippingMethod;
        }

        public String getShippingMethodName() {
            return ShippingMethodName;
        }

        public void setShippingMethodName(String ShippingMethodName) {
            this.ShippingMethodName = ShippingMethodName;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String Phone) {
            this.Phone = Phone;
        }

        public String getLocation() {
            return Location;
        }

        public void setLocation(String location) {
            Location = location;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getPurchasingDrugsMethod() {
            return PurchasingDrugsMethod;
        }

        public void setPurchasingDrugsMethod(String PurchasingDrugsMethod) {
            this.PurchasingDrugsMethod = PurchasingDrugsMethod;
        }

        public String getPurchasingDrugsMethodName() {
            return PurchasingDrugsMethodName;
        }

        public void setPurchasingDrugsMethodName(String PurchasingDrugsMethodName) {
            this.PurchasingDrugsMethodName = PurchasingDrugsMethodName;
        }

        public String getGoodsNum() {
            return GoodsNum;
        }

        public void setGoodsNum(String GoodsNum) {
            this.GoodsNum = GoodsNum;
        }

        public String getTotalAmount() {
            return TotalAmount;
        }

        public void setTotalAmount(String TotalAmount) {
            this.TotalAmount = TotalAmount;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public String getTelephone() {
            return Telephone;
        }

        public void setTelephone(String Telephone) {
            this.Telephone = Telephone;
        }

        public String getWechat() {
            return Wechat;
        }

        public void setWechat(String Wechat) {
            this.Wechat = Wechat;
        }

        public String getSetl_id() {
            return setl_id;
        }

        public void setSetl_id(String setl_id) {
            this.setl_id = setl_id;
        }

        public String getLogisticsFee() {
            return LogisticsFee;
        }

        public void setLogisticsFee(String LogisticsFee) {
            this.LogisticsFee = LogisticsFee;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getStoreAccessoryUrl() {
            return StoreAccessoryUrl;
        }

        public void setStoreAccessoryUrl(String storeAccessoryUrl) {
            StoreAccessoryUrl = storeAccessoryUrl;
        }

        public String getSettlementStatus() {
            return SettlementStatus;
        }

        public void setSettlementStatus(String settlementStatus) {
            SettlementStatus = settlementStatus;
        }

        public String getOnlineAmount() {
            return OnlineAmount;
        }

        public void setOnlineAmount(String onlineAmount) {
            OnlineAmount = onlineAmount;
        }

        public String getIsPrescription() {
            return IsPrescription;
        }

        public void setIsPrescription(String isPrescription) {
            IsPrescription = isPrescription;
        }

        public String getIsLogistics() {
            return IsLogistics;
        }

        public void setIsLogistics(String isLogistics) {
            IsLogistics = isLogistics;
        }

        public String getAreaName() {
            return AreaName;
        }

        public void setAreaName(String areaName) {
            AreaName = areaName;
        }

        public String getCityName() {
            return CityName;
        }

        public void setCityName(String cityName) {
            CityName = cityName;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String latitude) {
            Latitude = latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String longitude) {
            Longitude = longitude;
        }

        public String getIsViewLogistics() {
            return IsViewLogistics;
        }

        public void setIsViewLogistics(String isViewLogistics) {
            IsViewLogistics = isViewLogistics;
        }

        public String getOrderType() {
            return OrderType;
        }

        public void setOrderType(String orderType) {
            OrderType = orderType;
        }

        public String getIsThirdPartyPres() {
            return IsThirdPartyPres;
        }

        public void setIsThirdPartyPres(String isThirdPartyPres) {
            IsThirdPartyPres = isThirdPartyPres;
        }

        public List<OrderGoodsBean> getOrderGoods() {
            return OrderGoods;
        }

        public void setOrderGoods(List<OrderGoodsBean> OrderGoods) {
            this.OrderGoods = OrderGoods;
        }

        public static class OrderGoodsBean implements Parcelable {
            /**
             * GoodsName : 地喹氯铵含片
             * GoodsNum : 2
             * Price : 1
             * fixmedins_code : H61080200145
             * ItemType : 1
             */

            private String GoodsName;
            private int GoodsNum;
            private String Price;
            private String fixmedins_code;
            private String ItemType;
            private String BannerAccessoryUrl1;
            private String Spec;
            private String UsageDosage;
            private String Remark;
            private String IsPlatformDrug;
            //是否允许企业基金支付（0不允许 1允许）
            private String IsEnterpriseFundPay;

            public String getGoodsName() {
                return GoodsName;
            }

            public void setGoodsName(String GoodsName) {
                this.GoodsName = GoodsName;
            }

            public int getGoodsNum() {
                return GoodsNum;
            }

            public void setGoodsNum(int GoodsNum) {
                this.GoodsNum = GoodsNum;
            }

            public String getPrice() {
                return Price;
            }

            public void setPrice(String Price) {
                this.Price = Price;
            }

            public String getFixmedins_code() {
                return fixmedins_code;
            }

            public void setFixmedins_code(String fixmedins_code) {
                this.fixmedins_code = fixmedins_code;
            }

            public String getItemType() {
                return ItemType;
            }

            public void setItemType(String ItemType) {
                this.ItemType = ItemType;
            }

            public String getBannerAccessoryUrl1() {
                return BannerAccessoryUrl1;
            }

            public void setBannerAccessoryUrl1(String bannerAccessoryUrl1) {
                BannerAccessoryUrl1 = bannerAccessoryUrl1;
            }

            public String getSpec() {
                return Spec;
            }

            public void setSpec(String spec) {
                Spec = spec;
            }

            public String getUsageDosage() {
                return UsageDosage;
            }

            public void setUsageDosage(String usageDosage) {
                UsageDosage = usageDosage;
            }

            public String getRemark() {
                return Remark;
            }

            public void setRemark(String remark) {
                Remark = remark;
            }

            public String getIsPlatformDrug() {
                return IsPlatformDrug;
            }

            public void setIsPlatformDrug(String isPlatformDrug) {
                IsPlatformDrug = isPlatformDrug;
            }

            public String getIsEnterpriseFundPay() {
                return IsEnterpriseFundPay;
            }

            public void setIsEnterpriseFundPay(String isEnterpriseFundPay) {
                IsEnterpriseFundPay = isEnterpriseFundPay;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.GoodsName);
                dest.writeInt(this.GoodsNum);
                dest.writeString(this.Price);
                dest.writeString(this.fixmedins_code);
                dest.writeString(this.ItemType);
                dest.writeString(this.BannerAccessoryUrl1);
                dest.writeString(this.Spec);
                dest.writeString(this.UsageDosage);
                dest.writeString(this.Remark);
                dest.writeString(this.IsPlatformDrug);
                dest.writeString(this.IsEnterpriseFundPay);
            }

            public OrderGoodsBean() {
            }

            protected OrderGoodsBean(Parcel in) {
                this.GoodsName = in.readString();
                this.GoodsNum = in.readInt();
                this.Price = in.readString();
                this.fixmedins_code = in.readString();
                this.ItemType = in.readString();
                this.BannerAccessoryUrl1 = in.readString();
                this.Spec = in.readString();
                this.UsageDosage = in.readString();
                this.Remark = in.readString();
                this.IsPlatformDrug= in.readString();
                this.IsEnterpriseFundPay= in.readString();
            }

            public static final Parcelable.Creator<OrderGoodsBean> CREATOR = new Parcelable.Creator<OrderGoodsBean>() {
                @Override
                public OrderGoodsBean createFromParcel(Parcel source) {
                    return new OrderGoodsBean(source);
                }

                @Override
                public OrderGoodsBean[] newArray(int size) {
                    return new OrderGoodsBean[size];
                }
            };
        }
    }

    //订单列表
    public static void sendOrderListRequest(final String TAG,String OrderStatus, String pageindex,String pagesize,
                                            final CustomerJsonCallBack<OrderListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderStatus", OrderStatus);//（0、全部 1、待付款2、已付款3、已发货4、已收货5已取消）
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STORE_ORDERLIST_URL, jsonObject.toJSONString(), callback);
    }

    //订单搜索列表
    public static void sendSearchOrderListRequest(final String TAG,String pageindex,String pagesize,String name,String barCode,
                                            final CustomerJsonCallBack<OrderListModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", name);
        jsonObject.put("barCode", barCode);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STORE_ORDERSEARCHLIST_URL, jsonObject.toJSONString(), callback);
    }
}
