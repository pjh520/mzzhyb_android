package com.jrdz.zhyb_android.ui.zhyf_manage.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/20
 * 描    述：
 * ================================================
 */
public class OtherSortModel {
    private String id;
    private int img;
    private String text;
    private int redPointNum;

    public OtherSortModel(String id, int img, String text,int redPointNum) {
        this.id = id;
        this.img = img;
        this.text = text;
        this.redPointNum=redPointNum;
    }

    public String getId() {
        return id;
    }

    public int getImg() {
        return img;
    }

    public String getText() {
        return text;
    }

    public int getRedPointNum() {
        return redPointNum;
    }

    public void setRedPointNum(int redPointNum) {
        this.redPointNum = redPointNum;
    }
}
