package com.jrdz.zhyb_android.ui.zhyf_manage.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：
 * ================================================
 */
public class PayTypeModel {
    private String id;
    private String text;
    private int img;

    public PayTypeModel(String id, String text, int img) {
        this.id = id;
        this.text = text;
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "PayTypeModel{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                ", img=" + img +
                '}';
    }
}
