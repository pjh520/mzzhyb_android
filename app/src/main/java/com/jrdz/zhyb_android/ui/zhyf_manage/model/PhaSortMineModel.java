package com.jrdz.zhyb_android.ui.zhyf_manage.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-09
 * 描    述：
 * ================================================
 */
public class PhaSortMineModel {
    private String id;
    private int img;
    private String text;

    public PhaSortMineModel(String id, int img, String text) {
        this.id = id;
        this.img = img;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
