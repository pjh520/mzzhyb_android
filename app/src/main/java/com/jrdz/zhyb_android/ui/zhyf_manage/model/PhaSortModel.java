package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.frame.compiler.widget.wheel.entity.IWheelEntity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-09-29
 * 描    述：
 * ================================================
 */
public class PhaSortModel{
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-15 09:58:23
     * data : [{"CatalogueId":3,"CatalogueName":"女性专区","AccessoryId":"00000000-0000-0000-0000-000000000000","SubAccessoryId":"00000000-0000-0000-0000-000000000000","fixmedins_code":"","IsHomePage":1,"IsClassification":0,"Sort":3,"IsSystem":1},{"CatalogueId":2,"CatalogueName":"男性专区","AccessoryId":"00000000-0000-0000-0000-000000000000","SubAccessoryId":"00000000-0000-0000-0000-000000000000","fixmedins_code":"","IsHomePage":1,"IsClassification":0,"Sort":2,"IsSystem":1},{"CatalogueId":1,"CatalogueName":"常备药","AccessoryId":"00000000-0000-0000-0000-000000000000","SubAccessoryId":"00000000-0000-0000-0000-000000000000","fixmedins_code":"","IsHomePage":1,"IsClassification":0,"Sort":1,"IsSystem":1}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements  IWheelEntity, Parcelable {
        /**
         * CatalogueId : 3
         * CatalogueName : 女性专区
         * AccessoryId : 00000000-0000-0000-0000-000000000000
         * SubAccessoryId : 00000000-0000-0000-0000-000000000000
         * fixmedins_code :
         * IsHomePage : 1
         * IsClassification : 0
         * Sort : 3
         * IsSystem : 1
         */

        private String CatalogueId;
        private String CatalogueName;
        private String AccessoryId;
        private String SubAccessoryId;
        private String fixmedins_code;
        private String IsHomePage;//是否放首页（1是2不是）
        private String IsClassification;//是否放分类页（1是 2不是）
        private String Sort;
        private String IsSystem;//是否是系统的目录 1是，2是自定义 3是添加按钮
        private String AccessoryUrl;//目录图标
        private String SubAccessoryUrl;//横版图

        //-----------------------程序自用-----------------------------
        private int selfTag=0;//0代表没有操作目录 1代表添加目录 2代表修改目录 3代表删除目录
        private int pos=-1;//删除 修改时用到

        //------全部版块列表用到（内部程序自用）
        private String choose="0";//记录是否选中  1代表选中 0代表未选中 top01代表是选中的上一个 bottom01代表是选中的下一个
        //-----------------------程序自用-----------------------------

        //添加按钮专用
        public DataBean(String isSystem, String catalogueName) {
            IsSystem = isSystem;
            CatalogueName = catalogueName;
        }

        //添加按钮专用
        public DataBean(String isSystem,String catalogueId, String catalogueName) {
            IsSystem = isSystem;
            CatalogueId = catalogueId;
            CatalogueName = catalogueName;
        }

        public String getCatalogueId() {
            return CatalogueId;
        }

        public void setCatalogueId(String CatalogueId) {
            this.CatalogueId = CatalogueId;
        }

        public String getCatalogueName() {
            return CatalogueName;
        }

        public void setCatalogueName(String CatalogueName) {
            this.CatalogueName = CatalogueName;
        }

        public String getAccessoryId() {
            return AccessoryId;
        }

        public void setAccessoryId(String AccessoryId) {
            this.AccessoryId = AccessoryId;
        }

        public String getSubAccessoryId() {
            return SubAccessoryId;
        }

        public void setSubAccessoryId(String SubAccessoryId) {
            this.SubAccessoryId = SubAccessoryId;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getIsHomePage() {
            return IsHomePage;
        }

        public void setIsHomePage(String IsHomePage) {
            this.IsHomePage = IsHomePage;
        }

        public String getIsClassification() {
            return IsClassification;
        }

        public void setIsClassification(String IsClassification) {
            this.IsClassification = IsClassification;
        }

        public String getSort() {
            return Sort;
        }

        public void setSort(String Sort) {
            this.Sort = Sort;
        }

        public String getIsSystem() {
            return IsSystem;
        }

        public void setIsSystem(String IsSystem) {
            this.IsSystem = IsSystem;
        }

        public String getAccessoryUrl() {
            return AccessoryUrl;
        }

        public void setAccessoryUrl(String accessoryUrl) {
            AccessoryUrl = accessoryUrl;
        }

        public String getSubAccessoryUrl() {
            return SubAccessoryUrl;
        }

        public void setSubAccessoryUrl(String subAccessoryUrl) {
            SubAccessoryUrl = subAccessoryUrl;
        }

        public int getSelfTag() {
            return selfTag;
        }

        public void setSelfTag(int selfTag) {
            this.selfTag = selfTag;
        }

        public int getPos() {
            return pos;
        }

        public void setPos(int pos) {
            this.pos = pos;
        }

        public String getChoose() {
            return choose;
        }

        public void setChoose(String choose) {
            this.choose = choose;
        }

        @Override
        public String getWheelText() {
            return CatalogueName;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.CatalogueId);
            dest.writeString(this.CatalogueName);
            dest.writeString(this.AccessoryId);
            dest.writeString(this.SubAccessoryId);
            dest.writeString(this.fixmedins_code);
            dest.writeString(this.IsHomePage);
            dest.writeString(this.IsClassification);
            dest.writeString(this.Sort);
            dest.writeString(this.IsSystem);
            dest.writeString(this.AccessoryUrl);
            dest.writeString(this.SubAccessoryUrl);
            dest.writeInt(this.selfTag);
            dest.writeInt(this.pos);
            dest.writeString(this.choose);
        }

        protected DataBean(Parcel in) {
            this.CatalogueId = in.readString();
            this.CatalogueName = in.readString();
            this.AccessoryId = in.readString();
            this.SubAccessoryId = in.readString();
            this.fixmedins_code = in.readString();
            this.IsHomePage = in.readString();
            this.IsClassification = in.readString();
            this.Sort = in.readString();
            this.IsSystem = in.readString();
            this.AccessoryUrl = in.readString();
            this.SubAccessoryUrl = in.readString();
            this.selfTag = in.readInt();
            this.pos = in.readInt();
            this.choose = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //获取首页目录
    public static void sendHomeSortRequest(final String TAG, String IsHomePage,final CustomerJsonCallBack<PhaSortModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("IsHomePage", IsHomePage);//是否放首页（1是2不是）

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_HOMEPAGELIST_URL, jsonObject.toJSONString(), callback);
    }

    //获取分类
    public static void sendSortRequest(final String TAG, String IsClassification,final CustomerJsonCallBack<PhaSortModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("IsClassification", IsClassification);//是否放首页（1是2不是）

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_CLASSIFICATIONLIST_URL, jsonObject.toJSONString(), callback);
    }

    //获取店铺分类-用户端
    public static void sendShopClassifyRequest_user(final String TAG, String fixmedins_code,final CustomerJsonCallBack<PhaSortModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fixmedins_code", fixmedins_code);//机构编码

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_CLASSIFICATIONLIST_URL, jsonObject.toJSONString(), callback);
    }

    //获取店铺目录--用户端
    public static void sendShopSortRequest_user(final String TAG,String fixmedins_code,final CustomerJsonCallBack<PhaSortModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fixmedins_code", fixmedins_code);//机构编码

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_HOMEPAGELIST_URL, jsonObject.toJSONString(), callback);
    }
}
