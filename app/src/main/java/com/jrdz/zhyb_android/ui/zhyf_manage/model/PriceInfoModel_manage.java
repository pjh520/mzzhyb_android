package com.jrdz.zhyb_android.ui.zhyf_manage.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：
 * ================================================
 */
public class PriceInfoModel_manage {
    private String id;
    private String title;
    private String price;
    private String original_price;
    //程序自用
    private boolean isChoose;

    public PriceInfoModel_manage(String id, String title, String price, String original_price, boolean isChoose) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.original_price = original_price;
        this.isChoose = isChoose;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isChoose() {
        return isChoose;
    }

    public void setChoose(boolean choose) {
        isChoose = choose;
    }

    public String getOriginal_price() {
        return original_price;
    }

    public void setOriginal_price(String original_price) {
        this.original_price = original_price;
    }
}
