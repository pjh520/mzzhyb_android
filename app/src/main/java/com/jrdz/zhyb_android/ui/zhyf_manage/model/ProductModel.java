package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-14
 * 描    述：
 * ================================================
 */
public class ProductModel implements Parcelable {
    private String id;
    private String picUrl="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic1.win4000.com%2Fwallpaper%2F5%2F57809651ee306.jpg&refer=http%3A%2F%2Fpic1.win4000.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1667885377&t=e3db802af6d34fcbae802c69242b5fec";
    private String price;
    private int selectNum;//已加入购物车的数量
    //本地数据
    public boolean choose=false;

    public ProductModel(String id) {
        this.id = id;
    }

    public ProductModel(String id, String price) {
        this.id = id;
        this.price = price;
    }

    public ProductModel(String id, String price, int selectNum) {
        this.id = id;
        this.price = price;
        this.selectNum = selectNum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getSelectNum() {
        return selectNum;
    }

    public void setSelectNum(int selectNum) {
        this.selectNum = selectNum;
    }

    public boolean isChoose() {
        return choose;
    }

    public void setChoose(boolean choose) {
        this.choose = choose;
    }

    //重写equals方法 只要对比id一样 就是同一个model 可以删除掉
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductModel that = (ProductModel) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, picUrl, price, selectNum, choose);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.picUrl);
        dest.writeString(this.price);
        dest.writeInt(this.selectNum);
        dest.writeByte(this.choose ? (byte) 1 : (byte) 0);
    }

    protected ProductModel(Parcel in) {
        this.id = in.readString();
        this.picUrl = in.readString();
        this.price = in.readString();
        this.selectNum = in.readInt();
        this.choose = in.readByte() != 0;
    }

    public static final Parcelable.Creator<ProductModel> CREATOR = new Parcelable.Creator<ProductModel>() {
        @Override
        public ProductModel createFromParcel(Parcel source) {
            return new ProductModel(source);
        }

        @Override
        public ProductModel[] newArray(int size) {
            return new ProductModel[size];
        }
    };
}
