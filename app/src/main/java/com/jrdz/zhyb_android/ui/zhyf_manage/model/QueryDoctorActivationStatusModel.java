package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-02-27
 * 描    述：
 * ================================================
 */
public class QueryDoctorActivationStatusModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-02-27 17:24:13
     * data : {"ApproveStatus":"0"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * ApproveStatus : 0
         */

        private String ApproveStatus;

        public String getApproveStatus() {
            return ApproveStatus;
        }

        public void setApproveStatus(String ApproveStatus) {
            this.ApproveStatus = ApproveStatus;
        }
    }

    //获查询人员认证状态
    public static void sendQueryDoctorActivationStatusRequest(final String TAG,final CustomerJsonCallBack<QueryDoctorActivationStatusModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STORE_QUERYDOCTORACTIVATIONSTATUS_URL, "", callback);
    }
}
