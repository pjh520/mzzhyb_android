package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-17
 * 描    述：
 * ================================================
 */
public class QueryLogisticsSetModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-17 18:02:06
     * data : [{"AreaId":2,"AreaName":"黑龙江省","Type":2,"LogisticsFee":0,"LogisticsTimeliness":0,"IsOn":0},{"AreaId":3,"AreaName":"辽宁省","Type":2,"LogisticsFee":0,"LogisticsTimeliness":0,"IsOn":0},{"AreaId":4,"AreaName":"吉林省","Type":2,"LogisticsFee":0,"LogisticsTimeliness":0,"IsOn":0}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * AreaId : 2
         * AreaName : 黑龙江省
         * Type : 2
         * LogisticsFee : 0
         * LogisticsTimeliness : 0
         * IsOn : 0
         */

        private String AreaId;
        private String AreaName;
        private String Type;
        private String LogisticsFee;
        private String LogisticsTimeliness;
        private String IsOn;//0关，1开

        public String getAreaId() {
            return AreaId;
        }

        public void setAreaId(String AreaId) {
            this.AreaId = AreaId;
        }

        public String getAreaName() {
            return AreaName;
        }

        public void setAreaName(String AreaName) {
            this.AreaName = AreaName;
        }

        public String getType() {
            return Type;
        }

        public void setType(String Type) {
            this.Type = Type;
        }

        public String getLogisticsFee() {
            return LogisticsFee;
        }

        public void setLogisticsFee(String LogisticsFee) {
            this.LogisticsFee = LogisticsFee;
        }

        public String getLogisticsTimeliness() {
            return LogisticsTimeliness;
        }

        public void setLogisticsTimeliness(String LogisticsTimeliness) {
            this.LogisticsTimeliness = LogisticsTimeliness;
        }

        public String getIsOn() {
            return IsOn;
        }

        public void setIsOn(String IsOn) {
            this.IsOn = IsOn;
        }
    }

    //查询物流设置
    public static void sendQueryLogisticsSetRequest(final String TAG, String Type,final CustomerJsonCallBack<QueryLogisticsSetModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Type", Type);//1、陕西省 2东北3华东4华中5华南6华北7西南8西北

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_QUERYLOGISTICSSET_URL, jsonObject.toJSONString(), callback);
    }
}
