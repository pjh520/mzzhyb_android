package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-19
 * 描    述：
 * ================================================
 */
public class QueryOnlineInquiryModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-12-19 17:53:02
     * data : {"MedicalRecordAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/ce34b1dd-0edc-47c4-a2f0-d22a55c7d626/221c06e5-0d90-4317-90f4-8e94ddefdd1c.jpg","MedicalRecordAccessoryUrl2":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/0fd02de0-6e88-4b8c-aac0-dec4f2966d98/85cc8070-df74-4e8c-ac25-35a869b08f12.jpg","MedicalRecordAccessoryUrl3":"","MedicalRecordAccessoryUrl4":"","MedicalRecordAccessoryUrl5":"","OnlineInquiryId":1,"psn_name":"陈子平","mdtrt_cert_no":"612726196609210011","sex":"1","age":"56","AssociatedDiseases":"感冒1,盗汗咳血肺结核，经证实的,","IsTake":1,"IsAllergy":0,"IsUntowardReaction":1,"Symptom":"感冒，头疼，发烧","CreateDT":"2022-12-19 17:06:42","UpdateDT":"2022-12-19 17:06:42","CreateUser":"15060338988","UpdateUser":"15060338988","UserId":"50c82298-b028-4dc0-b26c-8eac6bd10c6b","UserName":"15060338988","fixmedins_code":"","OrderNo":"20221219094244100026","MedicalRecordAccessory1":"ce34b1dd-0edc-47c4-a2f0-d22a55c7d626","MedicalRecordAccessory2":"0fd02de0-6e88-4b8c-aac0-dec4f2966d98","MedicalRecordAccessory3":"00000000-0000-0000-0000-000000000000","MedicalRecordAccessory4":"00000000-0000-0000-0000-000000000000","MedicalRecordAccessory5":"00000000-0000-0000-0000-000000000000","IsDoctor":1,"IsPrescription":1}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * MedicalRecordAccessoryUrl1 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/ce34b1dd-0edc-47c4-a2f0-d22a55c7d626/221c06e5-0d90-4317-90f4-8e94ddefdd1c.jpg
         * MedicalRecordAccessoryUrl2 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/0fd02de0-6e88-4b8c-aac0-dec4f2966d98/85cc8070-df74-4e8c-ac25-35a869b08f12.jpg
         * MedicalRecordAccessoryUrl3 :
         * MedicalRecordAccessoryUrl4 :
         * MedicalRecordAccessoryUrl5 :
         * OnlineInquiryId : 1
         * psn_name : 陈子平
         * mdtrt_cert_no : 612726196609210011
         * sex : 1
         * age : 56
         * AssociatedDiseases : 感冒1,盗汗咳血肺结核，经证实的,
         * IsTake : 1
         * IsAllergy : 0
         * IsUntowardReaction : 1
         * Symptom : 感冒，头疼，发烧
         * CreateDT : 2022-12-19 17:06:42
         * UpdateDT : 2022-12-19 17:06:42
         * CreateUser : 15060338988
         * UpdateUser : 15060338988
         * UserId : 50c82298-b028-4dc0-b26c-8eac6bd10c6b
         * UserName : 15060338988
         * fixmedins_code :
         * OrderNo : 20221219094244100026
         * MedicalRecordAccessory1 : ce34b1dd-0edc-47c4-a2f0-d22a55c7d626
         * MedicalRecordAccessory2 : 0fd02de0-6e88-4b8c-aac0-dec4f2966d98
         * MedicalRecordAccessory3 : 00000000-0000-0000-0000-000000000000
         * MedicalRecordAccessory4 : 00000000-0000-0000-0000-000000000000
         * MedicalRecordAccessory5 : 00000000-0000-0000-0000-000000000000
         * IsDoctor : 1
         * IsPrescription : 1
         */

        private String MedicalRecordAccessoryUrl1;
        private String MedicalRecordAccessoryUrl2;
        private String MedicalRecordAccessoryUrl3;
        private String MedicalRecordAccessoryUrl4;
        private String MedicalRecordAccessoryUrl5;
        private String OnlineInquiryId;
        private String psn_name;
        private String mdtrt_cert_no;
        private String sex;
        private String age;
        private String AssociatedDiseases;
        private String IsTake;
        private String IsAllergy;
        private String IsUntowardReaction;
        private String Symptom;
        private String UserName;
        private String AssociatedDiseases1;

        public String getMedicalRecordAccessoryUrl1() {
            return MedicalRecordAccessoryUrl1;
        }

        public void setMedicalRecordAccessoryUrl1(String MedicalRecordAccessoryUrl1) {
            this.MedicalRecordAccessoryUrl1 = MedicalRecordAccessoryUrl1;
        }

        public String getMedicalRecordAccessoryUrl2() {
            return MedicalRecordAccessoryUrl2;
        }

        public void setMedicalRecordAccessoryUrl2(String MedicalRecordAccessoryUrl2) {
            this.MedicalRecordAccessoryUrl2 = MedicalRecordAccessoryUrl2;
        }

        public String getMedicalRecordAccessoryUrl3() {
            return MedicalRecordAccessoryUrl3;
        }

        public void setMedicalRecordAccessoryUrl3(String MedicalRecordAccessoryUrl3) {
            this.MedicalRecordAccessoryUrl3 = MedicalRecordAccessoryUrl3;
        }

        public String getMedicalRecordAccessoryUrl4() {
            return MedicalRecordAccessoryUrl4;
        }

        public void setMedicalRecordAccessoryUrl4(String MedicalRecordAccessoryUrl4) {
            this.MedicalRecordAccessoryUrl4 = MedicalRecordAccessoryUrl4;
        }

        public String getMedicalRecordAccessoryUrl5() {
            return MedicalRecordAccessoryUrl5;
        }

        public void setMedicalRecordAccessoryUrl5(String MedicalRecordAccessoryUrl5) {
            this.MedicalRecordAccessoryUrl5 = MedicalRecordAccessoryUrl5;
        }

        public String getOnlineInquiryId() {
            return OnlineInquiryId;
        }

        public void setOnlineInquiryId(String OnlineInquiryId) {
            this.OnlineInquiryId = OnlineInquiryId;
        }

        public String getPsn_name() {
            return psn_name;
        }

        public void setPsn_name(String psn_name) {
            this.psn_name = psn_name;
        }

        public String getMdtrt_cert_no() {
            return mdtrt_cert_no;
        }

        public void setMdtrt_cert_no(String mdtrt_cert_no) {
            this.mdtrt_cert_no = mdtrt_cert_no;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getAssociatedDiseases() {
            return AssociatedDiseases;
        }

        public void setAssociatedDiseases(String AssociatedDiseases) {
            this.AssociatedDiseases = AssociatedDiseases;
        }

        public String getIsTake() {
            return IsTake;
        }

        public void setIsTake(String IsTake) {
            this.IsTake = IsTake;
        }

        public String getIsAllergy() {
            return IsAllergy;
        }

        public void setIsAllergy(String IsAllergy) {
            this.IsAllergy = IsAllergy;
        }

        public String getIsUntowardReaction() {
            return IsUntowardReaction;
        }

        public void setIsUntowardReaction(String IsUntowardReaction) {
            this.IsUntowardReaction = IsUntowardReaction;
        }

        public String getSymptom() {
            return Symptom;
        }

        public void setSymptom(String Symptom) {
            this.Symptom = Symptom;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getAssociatedDiseases1() {
            return AssociatedDiseases1;
        }

        public void setAssociatedDiseases1(String associatedDiseases1) {
            AssociatedDiseases1 = associatedDiseases1;
        }
    }

    //查询问诊信息
    public static void sendQueryOnlineInquiryRequest(final String TAG, String OrderNo,final CustomerJsonCallBack<QueryOnlineInquiryModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_QUERYONLINEINQUIRY_URL, jsonObject.toJSONString(), callback);
    }
}
