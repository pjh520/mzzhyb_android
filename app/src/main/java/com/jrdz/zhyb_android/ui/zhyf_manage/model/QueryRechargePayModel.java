package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;
import com.jrdz.zhyb_android.ui.zhyf_user.model.QueryOrderPayOnlineModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023/1/8
 * 描    述：
 * ================================================
 */
public class QueryRechargePayModel {
    /**
     * code : 1
     * msg : 交易成功
     * server_time : 2022-12-16 21:27:16
     * data : {"OrderNo":"20221216212559100024","IsPaid":"1"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * SerialNumber : 20221216212559100024
         * IsPaid : 1
         */

        private String SerialNumber;
        private String IsPaid;

        public String getSerialNumber() {
            return SerialNumber;
        }

        public void setSerialNumber(String serialNumber) {
            SerialNumber = serialNumber;
        }

        public String getIsPaid() {
            return IsPaid;
        }

        public void setIsPaid(String IsPaid) {
            this.IsPaid = IsPaid;
        }
    }

    //充值状态查询
    public static void sendQueryRechargePayRequest(final String TAG,String SerialNumber, final CustomerJsonCallBack<QueryRechargePayModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("SerialNumber", SerialNumber);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_QUERYRECHARGECASH_URL, jsonObject.toJSONString(), callback);
    }
}
