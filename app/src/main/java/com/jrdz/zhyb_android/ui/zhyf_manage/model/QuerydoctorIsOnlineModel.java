package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-03-29
 * 描    述：
 * ================================================
 */
public class QuerydoctorIsOnlineModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2024-04-01 10:36:17
     * data : {"dr_code":"D610803001453","IsOnline":"0"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * dr_code : D610803001453
         * IsOnline : 0
         */

        private String dr_code;
        private String IsOnline;

        public String getDr_code() {
            return dr_code;
        }

        public void setDr_code(String dr_code) {
            this.dr_code = dr_code;
        }

        public String getIsOnline() {
            return IsOnline;
        }

        public void setIsOnline(String IsOnline) {
            this.IsOnline = IsOnline;
        }
    }

    //查询医师在线状态接口
    public static void sendQuerydoctorIsOnlineRequest(final String TAG, String doctorId,final CustomerJsonCallBack<QuerydoctorIsOnlineModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("doctorId", doctorId);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MERCHANT_QUERYDOCTORISONLINE_URL, jsonObject.toJSONString(), callback);
    }
}
