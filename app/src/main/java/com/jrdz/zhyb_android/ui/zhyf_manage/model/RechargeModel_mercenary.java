package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OrderPayOnlineModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-01
 * 描    述：
 * ================================================
 */
public class RechargeModel_mercenary {

    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-12-16 13:12:15
     * data : {"amt":0.01,"dataMap":{"WL.Ipaddress":"127.0.0.1","appScheme":"99billkuaiqianmerchant","merchant_appid":"wxf58f63ce7fa9971a","limit_pay":"1","orderdetail":"商品"},"externalTraceNo":"MH00120221216131213","merchantId":"812331545110013","mpayInfo":{"appletInfo":"eyJhbGlwYXlBcHBsZXRJRCI6IjIwMjEwMDIxNDA2ODIxMDYiLCJvcmRlclJlcXVlc3RFbnYiOiIxIiwiYXBwU2NoZW1lIjoiOTliaWxsa3VhaXFpYW5tZXJjaGFudCIsImFsaXBheUFwcGxldFR5cGUiOiIwIiwiYWxpcGF5QXBwbGV0UGF0aCI6InBhZ2VzL3N0YXJ0UGF5L3N0YXJ0UGF5In0=","orderRequestInfo":{"orderRequestKey":"5yhQ7bsjCbOJtun_NFaf8W2BKSrAtLDV7XG"}},"payType":"A2","requestTime":"20221216131213","resultCode":"1","resultMessage":"处理成功","sign":"ePdmAnAj5os24AI5Y7JfwU/nWRgNXXKbGlflWv/2TeVdgCRwYk2QjKflJqFxKd8k7z3zGX+GB5VgDswcNuzw9GjdcTRQlagtxc/5SJg+KrjAAu3GWyAart2R6i/p56FyD500lbH2hdxHWag3trQMqevN1SxMHcw67bvn/qgbfOVPnopi2U60GNJQnBvBYSXZJHpNA3ZlkweS5Vm/IddFq+h/2OfMqfSLRtbHxPB/wGhpQ4bWKwfm3yJG1BFH/l3eaZ31cy5j2mGVGW16fl8klPOhvT+2n8BH5ShqCF7hSteTJwLCsAa5Zk+zdtgDk0HQq5IzWZOXl3x3Dko/RSAEKA==","stlDate":"20221216","terminalId":"33150025","txnType":"ODR"}
     */

    private String code;
    private String msg;
    private String server_time;
    private String SerialNumber;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        SerialNumber = serialNumber;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * amt : 0.01
         * dataMap : {"WL.Ipaddress":"127.0.0.1","appScheme":"99billkuaiqianmerchant","merchant_appid":"wxf58f63ce7fa9971a","limit_pay":"1","orderdetail":"商品"}
         * externalTraceNo : MH00120221216131213
         * merchantId : 812331545110013
         * mpayInfo : {"appletInfo":"eyJhbGlwYXlBcHBsZXRJRCI6IjIwMjEwMDIxNDA2ODIxMDYiLCJvcmRlclJlcXVlc3RFbnYiOiIxIiwiYXBwU2NoZW1lIjoiOTliaWxsa3VhaXFpYW5tZXJjaGFudCIsImFsaXBheUFwcGxldFR5cGUiOiIwIiwiYWxpcGF5QXBwbGV0UGF0aCI6InBhZ2VzL3N0YXJ0UGF5L3N0YXJ0UGF5In0=","orderRequestInfo":{"orderRequestKey":"5yhQ7bsjCbOJtun_NFaf8W2BKSrAtLDV7XG"}}
         * payType : A2
         * requestTime : 20221216131213
         * resultCode : 1
         * resultMessage : 处理成功
         * sign : ePdmAnAj5os24AI5Y7JfwU/nWRgNXXKbGlflWv/2TeVdgCRwYk2QjKflJqFxKd8k7z3zGX+GB5VgDswcNuzw9GjdcTRQlagtxc/5SJg+KrjAAu3GWyAart2R6i/p56FyD500lbH2hdxHWag3trQMqevN1SxMHcw67bvn/qgbfOVPnopi2U60GNJQnBvBYSXZJHpNA3ZlkweS5Vm/IddFq+h/2OfMqfSLRtbHxPB/wGhpQ4bWKwfm3yJG1BFH/l3eaZ31cy5j2mGVGW16fl8klPOhvT+2n8BH5ShqCF7hSteTJwLCsAa5Zk+zdtgDk0HQq5IzWZOXl3x3Dko/RSAEKA==
         * stlDate : 20221216
         * terminalId : 33150025
         * txnType : ODR
         */
        private OrderPayOnlineModel.DataBean.MpayInfoBean mpayInfo;

        public OrderPayOnlineModel.DataBean.MpayInfoBean getMpayInfo() {
            return mpayInfo;
        }

        public void setMpayInfo(OrderPayOnlineModel.DataBean.MpayInfoBean mpayInfo) {
            this.mpayInfo = mpayInfo;
        }

        public static class MpayInfoBean {
            /**
             * appletInfo : eyJhbGlwYXlBcHBsZXRJRCI6IjIwMjEwMDIxNDA2ODIxMDYiLCJvcmRlclJlcXVlc3RFbnYiOiIxIiwiYXBwU2NoZW1lIjoiOTliaWxsa3VhaXFpYW5tZXJjaGFudCIsImFsaXBheUFwcGxldFR5cGUiOiIwIiwiYWxpcGF5QXBwbGV0UGF0aCI6InBhZ2VzL3N0YXJ0UGF5L3N0YXJ0UGF5In0=
             * orderRequestInfo : {"orderRequestKey":"5yhQ7bsjCbOJtun_NFaf8W2BKSrAtLDV7XG"}
             */

            private String appletInfo;
            private OrderPayOnlineModel.DataBean.MpayInfoBean.OrderRequestInfoBean orderRequestInfo;

            public String getAppletInfo() {
                return appletInfo;
            }

            public void setAppletInfo(String appletInfo) {
                this.appletInfo = appletInfo;
            }

            public OrderPayOnlineModel.DataBean.MpayInfoBean.OrderRequestInfoBean getOrderRequestInfo() {
                return orderRequestInfo;
            }

            public void setOrderRequestInfo(OrderPayOnlineModel.DataBean.MpayInfoBean.OrderRequestInfoBean orderRequestInfo) {
                this.orderRequestInfo = orderRequestInfo;
            }

            public static class OrderRequestInfoBean {
                /**
                 * orderRequestKey : 5yhQ7bsjCbOJtun_NFaf8W2BKSrAtLDV7XG
                 */

                private String orderRequestKey;

                public String getOrderRequestKey() {
                    return orderRequestKey;
                }

                public void setOrderRequestKey(String orderRequestKey) {
                    this.orderRequestKey = orderRequestKey;
                }
            }
        }
    }

    //新增充值
    public static void sendRechargeRequest_mercenary(final String TAG, String RechargeWay, String RechargeAccount, String ManagementServiceMode,
                                                     String RechargeAmount, String RechargeType, final CustomerJsonCallBack<RechargeModel_mercenary> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("RechargeWay", RechargeWay);//充值方式（1微信2支付宝3云闪付）
        jsonObject.put("RechargeAccount", RechargeAccount);//充值账号
        jsonObject.put("ManagementServiceMode", ManagementServiceMode);//管理服务模式（0、非管理服务模式1、按月付费2、按季付费3按半年付费4按年付费）
        jsonObject.put("RechargeAmount", RechargeAmount);//充值金额
        jsonObject.put("RechargeType", RechargeType);//充值类型（1平台抽佣模式2管理服务模式3保证金）

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_RECHARGECASH_URL, jsonObject.toJSONString(), callback);
    }
}
