package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：
 * ================================================
 */
public class RechargeRecordModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-12-01 17:36:54
     * data : [{"RechargeRecordId":3,"SerialNumber":"20221201173408001","RechargeTime":"2022-12-01 17:34:08","ReceiptTime":"2022-12-01 17:34:08","RechargeWay":2,"RechargeAccount":"99","ManagementServiceMode":0,"ServiceStartTime":"2022-12-01 17:34:08","ServiceEndTime":"2022-12-01 17:34:08","RechargeAmount":1000,"RechargeStatus":1,"CreateDT":"2022-12-01 17:34:08","UpdateDT":"2022-12-01 17:34:08","CreateUser":"99","UpdateUser":"99","UserId":"9ca7a208-f51b-45b9-ad94-3d2d414e6a75","UserName":"99","fixmedins_code":"H61080200145","RechargeType":3},{"RechargeRecordId":2,"SerialNumber":"20221201110823001","RechargeTime":"2022-12-01 11:08:23","ReceiptTime":"2022-12-01 11:08:23","RechargeWay":1,"RechargeAccount":"99","ManagementServiceMode":0,"ServiceStartTime":"2022-12-01 11:08:23","ServiceEndTime":"2022-12-01 11:08:23","RechargeAmount":20000,"RechargeStatus":1,"CreateDT":"2022-12-01 11:08:23","UpdateDT":"2022-12-01 11:08:23","CreateUser":"99","UpdateUser":"99","UserId":"7c9a0435-1f17-402e-b7e6-0bc811e66f85","UserName":"99","fixmedins_code":"H61080200145","RechargeType":1},{"RechargeRecordId":1,"SerialNumber":"20221201101024001","RechargeTime":"2022-12-01 10:10:24","ReceiptTime":"2022-12-01 10:10:24","RechargeWay":2,"RechargeAccount":"99","ManagementServiceMode":0,"ServiceStartTime":"2022-12-01 10:10:24","ServiceEndTime":"2022-12-01 10:10:24","RechargeAmount":100,"RechargeStatus":1,"CreateDT":"2022-12-01 10:10:24","UpdateDT":"2022-12-01 10:10:24","CreateUser":"99","UpdateUser":"99","UserId":"5fcbbd86-2187-40b7-800c-ff1914f9432b","UserName":"99","fixmedins_code":"H61080200145","RechargeType":1}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * RechargeRecordId : 3
         * SerialNumber : 20221201173408001
         * RechargeTime : 2022-12-01 17:34:08
         * ReceiptTime : 2022-12-01 17:34:08
         * RechargeWay : 2
         * RechargeAccount : 99
         * ManagementServiceMode : 0
         * ServiceStartTime : 2022-12-01 17:34:08
         * ServiceEndTime : 2022-12-01 17:34:08
         * RechargeAmount : 1000
         * RechargeStatus : 1
         * CreateDT : 2022-12-01 17:34:08
         * UpdateDT : 2022-12-01 17:34:08
         * CreateUser : 99
         * UpdateUser : 99
         * UserId : 9ca7a208-f51b-45b9-ad94-3d2d414e6a75
         * UserName : 99
         * fixmedins_code : H61080200145
         * RechargeType : 3
         */

        private String RechargeRecordId;
        private String SerialNumber;
        private String RechargeTime;
        private String ReceiptTime;
        private String RechargeWay;
        private String RechargeAccount;
        private String ManagementServiceMode;
        private String ServiceStartTime;
        private String ServiceEndTime;
        private String RechargeAmount;
        private String RechargeStatus;//（0待充值1充值成功2充值失败）
        private String UserName;
        private String RechargeType;

        public String getRechargeRecordId() {
            return RechargeRecordId;
        }

        public void setRechargeRecordId(String RechargeRecordId) {
            this.RechargeRecordId = RechargeRecordId;
        }

        public String getSerialNumber() {
            return SerialNumber;
        }

        public void setSerialNumber(String SerialNumber) {
            this.SerialNumber = SerialNumber;
        }

        public String getRechargeTime() {
            return RechargeTime;
        }

        public void setRechargeTime(String RechargeTime) {
            this.RechargeTime = RechargeTime;
        }

        public String getReceiptTime() {
            return ReceiptTime;
        }

        public void setReceiptTime(String ReceiptTime) {
            this.ReceiptTime = ReceiptTime;
        }

        public String getRechargeWay() {
            return RechargeWay;
        }

        public void setRechargeWay(String RechargeWay) {
            this.RechargeWay = RechargeWay;
        }

        public String getRechargeAccount() {
            return RechargeAccount;
        }

        public void setRechargeAccount(String RechargeAccount) {
            this.RechargeAccount = RechargeAccount;
        }

        public String getManagementServiceMode() {
            return ManagementServiceMode;
        }

        public void setManagementServiceMode(String ManagementServiceMode) {
            this.ManagementServiceMode = ManagementServiceMode;
        }

        public String getServiceStartTime() {
            return ServiceStartTime;
        }

        public void setServiceStartTime(String ServiceStartTime) {
            this.ServiceStartTime = ServiceStartTime;
        }

        public String getServiceEndTime() {
            return ServiceEndTime;
        }

        public void setServiceEndTime(String ServiceEndTime) {
            this.ServiceEndTime = ServiceEndTime;
        }

        public String getRechargeAmount() {
            return RechargeAmount;
        }

        public void setRechargeAmount(String RechargeAmount) {
            this.RechargeAmount = RechargeAmount;
        }

        public String getRechargeStatus() {
            return RechargeStatus;
        }

        public void setRechargeStatus(String RechargeStatus) {
            this.RechargeStatus = RechargeStatus;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getRechargeType() {
            return RechargeType;
        }

        public void setRechargeType(String RechargeType) {
            this.RechargeType = RechargeType;
        }
    }

    //充值记录列表
    public static void sendRechargeRecordRequest(final String TAG, String pageindex, String pagesize,final CustomerJsonCallBack<RechargeRecordModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_RECHARGERECORDLIST_URL, jsonObject.toJSONString(), callback);
    }
}
