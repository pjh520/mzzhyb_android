package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-11
 * 描    述：
 * ================================================
 */
public class RefundProgressModel {
    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        public String OrderNo;
        //是否退回医保账户（1是0不是）
        public String IsReturnMedicare;
        //退回医保账户金额
        public String ReturnMedicareAmount;
        //是否退回原支付账户（1是0不是）
        public String IsReturnOriginalAccount;
        //退回原支付账户金额
        public String ReturnOriginalAmount;
        //是否退回企业基金（1是0不是）
        public String IsReturnEnterpriseFund;
        //退回原企业基金金额
        public String ReturnEnterpriseFundAmount;
        //步骤标题1
        public String StepTitle1;
        //步骤内容1
        public String StepContent1;
        //步骤时间1
        public String StepTime1;
        //步骤标题2
        public String StepTitle2;
        //步骤内容2
        public String StepContent2;
        //步骤时间2
        public String StepTime2;
        //步骤标题3
        public String StepTitle3;
        //步骤内容3
        public String StepContent3;
        //步骤时间3
        public String StepTime3;
        //企业基金步骤标题4
        public String StepTitle4;
        //企业基金步骤内容4
        public String StepContent4;
        //企业基金步骤时间4
        public String StepTime4;


        //预计到账内容
        public String EstimateContent;
        //订单取消时间
        public String CancelTime;
        //订单退款编号
        public String OrderRefundNo;
        //订单退款流水号
        public String OrderRefundSerialNo;

        //人员编号
        public String psn_no;
        //证件类型
        public String mdtrt_cert_type;
        //证件号码
        public String mdtrt_cert_no;
        //医疗费总额
        public String medfee_sumamt;
        //险种类型
        public String insutype;
        //基金支付总额
        public String fund_pay_sumamt;
        //姓名
        public String psn_name;
        //个人现金支出
        public String psn_cash_pay;
        //余额
        public String balc;
        //个人账户支出
        public String acct_pay;
        //结算ID
        public String setl_id;

        private List<OrderGoodsBean> OrderGoods;

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String orderNo) {
            OrderNo = orderNo;
        }

        public String getIsReturnMedicare() {
            return IsReturnMedicare;
        }

        public void setIsReturnMedicare(String isReturnMedicare) {
            IsReturnMedicare = isReturnMedicare;
        }

        public String getReturnMedicareAmount() {
            return ReturnMedicareAmount;
        }

        public void setReturnMedicareAmount(String returnMedicareAmount) {
            ReturnMedicareAmount = returnMedicareAmount;
        }

        public String getIsReturnOriginalAccount() {
            return IsReturnOriginalAccount;
        }

        public void setIsReturnOriginalAccount(String isReturnOriginalAccount) {
            IsReturnOriginalAccount = isReturnOriginalAccount;
        }

        public String getIsReturnEnterpriseFund() {
            return IsReturnEnterpriseFund;
        }

        public void setIsReturnEnterpriseFund(String isReturnEnterpriseFund) {
            IsReturnEnterpriseFund = isReturnEnterpriseFund;
        }

        public String getReturnEnterpriseFundAmount() {
            return ReturnEnterpriseFundAmount;
        }

        public void setReturnEnterpriseFundAmount(String returnEnterpriseFundAmount) {
            ReturnEnterpriseFundAmount = returnEnterpriseFundAmount;
        }

        public String getReturnOriginalAmount() {
            return ReturnOriginalAmount;
        }

        public void setReturnOriginalAmount(String returnOriginalAmount) {
            ReturnOriginalAmount = returnOriginalAmount;
        }

        public String getStepTitle1() {
            return StepTitle1;
        }

        public void setStepTitle1(String stepTitle1) {
            StepTitle1 = stepTitle1;
        }

        public String getStepContent1() {
            return StepContent1;
        }

        public void setStepContent1(String stepContent1) {
            StepContent1 = stepContent1;
        }

        public String getStepTime1() {
            return StepTime1;
        }

        public void setStepTime1(String stepTime1) {
            StepTime1 = stepTime1;
        }

        public String getStepTitle2() {
            return StepTitle2;
        }

        public void setStepTitle2(String stepTitle2) {
            StepTitle2 = stepTitle2;
        }

        public String getStepContent2() {
            return StepContent2;
        }

        public void setStepContent2(String stepContent2) {
            StepContent2 = stepContent2;
        }

        public String getStepTime2() {
            return StepTime2;
        }

        public void setStepTime2(String stepTime2) {
            StepTime2 = stepTime2;
        }

        public String getStepTitle3() {
            return StepTitle3;
        }

        public void setStepTitle3(String stepTitle3) {
            StepTitle3 = stepTitle3;
        }

        public String getStepContent3() {
            return StepContent3;
        }

        public void setStepContent3(String stepContent3) {
            StepContent3 = stepContent3;
        }

        public String getStepTime3() {
            return StepTime3;
        }

        public void setStepTime3(String stepTime3) {
            StepTime3 = stepTime3;
        }

        public String getStepTitle4() {
            return StepTitle4;
        }

        public void setStepTitle4(String stepTitle4) {
            StepTitle4 = stepTitle4;
        }

        public String getStepContent4() {
            return StepContent4;
        }

        public void setStepContent4(String stepContent4) {
            StepContent4 = stepContent4;
        }

        public String getStepTime4() {
            return StepTime4;
        }

        public void setStepTime4(String stepTime4) {
            StepTime4 = stepTime4;
        }

        public String getEstimateContent() {
            return EstimateContent;
        }

        public void setEstimateContent(String estimateContent) {
            EstimateContent = estimateContent;
        }

        public String getCancelTime() {
            return CancelTime;
        }

        public void setCancelTime(String cancelTime) {
            CancelTime = cancelTime;
        }

        public String getOrderRefundNo() {
            return OrderRefundNo;
        }

        public void setOrderRefundNo(String orderRefundNo) {
            OrderRefundNo = orderRefundNo;
        }

        public String getOrderRefundSerialNo() {
            return OrderRefundSerialNo;
        }

        public void setOrderRefundSerialNo(String orderRefundSerialNo) {
            OrderRefundSerialNo = orderRefundSerialNo;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getMdtrt_cert_type() {
            return mdtrt_cert_type;
        }

        public void setMdtrt_cert_type(String mdtrt_cert_type) {
            this.mdtrt_cert_type = mdtrt_cert_type;
        }

        public String getMdtrt_cert_no() {
            return mdtrt_cert_no;
        }

        public void setMdtrt_cert_no(String mdtrt_cert_no) {
            this.mdtrt_cert_no = mdtrt_cert_no;
        }

        public String getMedfee_sumamt() {
            return medfee_sumamt;
        }

        public void setMedfee_sumamt(String medfee_sumamt) {
            this.medfee_sumamt = medfee_sumamt;
        }

        public String getInsutype() {
            return insutype;
        }

        public void setInsutype(String insutype) {
            this.insutype = insutype;
        }

        public String getFund_pay_sumamt() {
            return fund_pay_sumamt;
        }

        public void setFund_pay_sumamt(String fund_pay_sumamt) {
            this.fund_pay_sumamt = fund_pay_sumamt;
        }

        public String getPsn_name() {
            return psn_name;
        }

        public void setPsn_name(String psn_name) {
            this.psn_name = psn_name;
        }

        public String getPsn_cash_pay() {
            return psn_cash_pay;
        }

        public void setPsn_cash_pay(String psn_cash_pay) {
            this.psn_cash_pay = psn_cash_pay;
        }

        public String getBalc() {
            return balc;
        }

        public void setBalc(String balc) {
            this.balc = balc;
        }

        public String getAcct_pay() {
            return acct_pay;
        }

        public void setAcct_pay(String acct_pay) {
            this.acct_pay = acct_pay;
        }

        public String getSetl_id() {
            return setl_id;
        }

        public void setSetl_id(String setl_id) {
            this.setl_id = setl_id;
        }

        public List<OrderGoodsBean> getOrderGoods() {
            return OrderGoods;
        }

        public void setOrderGoods(List<OrderGoodsBean> OrderGoods) {
            this.OrderGoods = OrderGoods;
        }

        public static class OrderGoodsBean {
            /**
             * GoodsName : 地喹氯铵含片
             * GoodsNum : 2
             * Price : 0.9
             * fixmedins_code : H61080200145
             * ItemType : 1
             * BannerAccessoryUrl1 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/cc9dae19-400f-4790-a302-22e69b867388/c9061f61-88b6-4df3-aba9-21d5b8b64546.jpg
             */

            private String GoodsName;
            private int GoodsNum;
            private String Price;
            private String fixmedins_code;
            private String ItemType;
            private String BannerAccessoryUrl1;

            public String getGoodsName() {
                return GoodsName;
            }

            public void setGoodsName(String GoodsName) {
                this.GoodsName = GoodsName;
            }

            public int getGoodsNum() {
                return GoodsNum;
            }

            public void setGoodsNum(int GoodsNum) {
                this.GoodsNum = GoodsNum;
            }

            public String getPrice() {
                return Price;
            }

            public void setPrice(String Price) {
                this.Price = Price;
            }

            public String getFixmedins_code() {
                return fixmedins_code;
            }

            public void setFixmedins_code(String fixmedins_code) {
                this.fixmedins_code = fixmedins_code;
            }

            public String getItemType() {
                return ItemType;
            }

            public void setItemType(String ItemType) {
                this.ItemType = ItemType;
            }

            public String getBannerAccessoryUrl1() {
                return BannerAccessoryUrl1;
            }

            public void setBannerAccessoryUrl1(String BannerAccessoryUrl1) {
                this.BannerAccessoryUrl1 = BannerAccessoryUrl1;
            }
        }
    }

    //查看退款进度详情
    public static void sendOrderRefundProgressRequest(final String TAG, String OrderNo,final CustomerJsonCallBack<RefundProgressModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ORDERREFUNDPROGRESS_URL, jsonObject.toJSONString(), callback);
    }

    //查看退款进度详情
    public static void sendStoreOrderRefundProgressRequest(final String TAG, String OrderNo,final CustomerJsonCallBack<RefundProgressModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STORE_ORDERREFUNDPROGRESS_URL, jsonObject.toJSONString(), callback);
    }
}
