package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-14
 * 描    述：
 * ================================================
 */
public class SettleInApplyResultModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-14 11:37:58
     * data : [{"BusinessLicenseAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/b05a8d56-9ba7-432d-8621-0bf13418b7c3/5667ce01-fb3d-496c-a67e-6d44775de695.jpg","FrontAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/f6b7f95f-4957-4a7c-96ad-1b176cdc63f1/dd6087bc-6299-44e5-a82e-7d1027db2bcb.jpg","BackAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/4ddbaab7-ea75-4a15-a45e-7113c5a59bd5/57dd0a09-a565-4523-9725-a40675323afa.jpg","MedinsinfoActivationId":1,"fixmedins_code":"H61080200145","fixmedins_name":"榆林市第二医院","fixmedins_phone":"13859005801","fixmedins_type":1,"BusinessLicenseAccessoryId":"b05a8d56-9ba7-432d-8621-0bf13418b7c3","BusinessLicenseNo":"1236685269996","Director":"郑莱芜","IDNumber":"350321199211272656","FrontAccessoryId":"f6b7f95f-4957-4a7c-96ad-1b176cdc63f1","BackAccessoryId":"4ddbaab7-ea75-4a15-a45e-7113c5a59bd5","EntryMode":1,"EntryModeName":"固定抽佣模式","ApproveStatus":1,"ApproveNum":1,"Approvor":"","ApproveDT":"2022-11-14 11:26:19","CreateDT":"2022-11-14 11:26:19","UpdateDT":"2022-11-14 11:26:19","CreateUser":"99","UpdateUser":"99"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * BusinessLicenseAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/b05a8d56-9ba7-432d-8621-0bf13418b7c3/5667ce01-fb3d-496c-a67e-6d44775de695.jpg
         * FrontAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/f6b7f95f-4957-4a7c-96ad-1b176cdc63f1/dd6087bc-6299-44e5-a82e-7d1027db2bcb.jpg
         * BackAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/4ddbaab7-ea75-4a15-a45e-7113c5a59bd5/57dd0a09-a565-4523-9725-a40675323afa.jpg
         * MedinsinfoActivationId : 1
         * fixmedins_code : H61080200145
         * fixmedins_name : 榆林市第二医院
         * fixmedins_phone : 13859005801
         * fixmedins_type : 1
         * BusinessLicenseAccessoryId : b05a8d56-9ba7-432d-8621-0bf13418b7c3
         * BusinessLicenseNo : 1236685269996
         * Director : 郑莱芜
         * IDNumber : 350321199211272656
         * FrontAccessoryId : f6b7f95f-4957-4a7c-96ad-1b176cdc63f1
         * BackAccessoryId : 4ddbaab7-ea75-4a15-a45e-7113c5a59bd5
         * EntryMode : 1
         * EntryModeName : 固定抽佣模式
         * ApproveStatus : 1
         * ApproveNum : 1
         * Approvor :
         * ApproveDT : 2022-11-14 11:26:19
         * CreateDT : 2022-11-14 11:26:19
         * UpdateDT : 2022-11-14 11:26:19
         * CreateUser : 99
         * UpdateUser : 99
         */

        private String BusinessLicenseAccessoryUrl;
        private String FrontAccessoryUrl;
        private String BackAccessoryUrl;
        private String DrugBLAccessoryUrl;
        private String MedicalDeviceBLAccessoryUrl;
        private String GSCertificateAccessoryUrl;
        private String AccountLicenceAccessoryUrl;
        private String DoorHeaderAccessoryUrl;
        private String InteriorOneAccessoryUrl;
        private String InteriorTwoAccessoryUrl;
        private String MedinsinfoActivationId;
        private String fixmedins_code;
        private String fixmedins_name;
        private String fixmedins_phone;
        private String fixmedins_type;
        private String BusinessLicenseAccessoryId;
        private String BusinessLicenseNo;
        private String Director;
        private String IDNumber;
        private String FrontAccessoryId;
        private String BackAccessoryId;
        private String EntryMode;
        private String EntryModeName;
        private String ApproveStatus;
        private String ApproveNum;
        private String Approvor;
        private String ApproveDT;
        private String CreateDT;
        private String UpdateDT;
        private String CreateUser;
        private String UpdateUser;
        private String SettlementBankCard;
        private String Bank;
        private String DrugBLAccessoryId;
        private String MedicalDeviceBLAccessoryId;
        private String GSCertificateAccessoryId;
        private String AccountLicenceAccessoryId;
        private String DoorHeaderAccessoryId;
        private String InteriorOneAccessoryId;
        private String InteriorTwoAccessoryId;
        private String RejectReason;

        //医师特有
        private String dr_name;
        private String dr_phone;
        private String QualificationCertificateNo;
        private String DoctorAccessoryId;
        private String DoctorAccessoryUrl;

        public String getBusinessLicenseAccessoryUrl() {
            return BusinessLicenseAccessoryUrl;
        }

        public void setBusinessLicenseAccessoryUrl(String BusinessLicenseAccessoryUrl) {
            this.BusinessLicenseAccessoryUrl = BusinessLicenseAccessoryUrl;
        }

        public String getFrontAccessoryUrl() {
            return FrontAccessoryUrl;
        }

        public void setFrontAccessoryUrl(String FrontAccessoryUrl) {
            this.FrontAccessoryUrl = FrontAccessoryUrl;
        }

        public String getBackAccessoryUrl() {
            return BackAccessoryUrl;
        }

        public void setBackAccessoryUrl(String BackAccessoryUrl) {
            this.BackAccessoryUrl = BackAccessoryUrl;
        }

        public String getDrugBLAccessoryUrl() {
            return DrugBLAccessoryUrl;
        }

        public void setDrugBLAccessoryUrl(String drugBLAccessoryUrl) {
            DrugBLAccessoryUrl = drugBLAccessoryUrl;
        }

        public String getMedicalDeviceBLAccessoryUrl() {
            return MedicalDeviceBLAccessoryUrl;
        }

        public void setMedicalDeviceBLAccessoryUrl(String medicalDeviceBLAccessoryUrl) {
            MedicalDeviceBLAccessoryUrl = medicalDeviceBLAccessoryUrl;
        }

        public String getGSCertificateAccessoryUrl() {
            return GSCertificateAccessoryUrl;
        }

        public void setGSCertificateAccessoryUrl(String GSCertificateAccessoryUrl) {
            this.GSCertificateAccessoryUrl = GSCertificateAccessoryUrl;
        }

        public String getAccountLicenceAccessoryUrl() {
            return AccountLicenceAccessoryUrl;
        }

        public void setAccountLicenceAccessoryUrl(String accountLicenceAccessoryUrl) {
            AccountLicenceAccessoryUrl = accountLicenceAccessoryUrl;
        }

        public String getDoorHeaderAccessoryUrl() {
            return DoorHeaderAccessoryUrl;
        }

        public void setDoorHeaderAccessoryUrl(String doorHeaderAccessoryUrl) {
            DoorHeaderAccessoryUrl = doorHeaderAccessoryUrl;
        }

        public String getInteriorOneAccessoryUrl() {
            return InteriorOneAccessoryUrl;
        }

        public void setInteriorOneAccessoryUrl(String interiorOneAccessoryUrl) {
            InteriorOneAccessoryUrl = interiorOneAccessoryUrl;
        }

        public String getInteriorTwoAccessoryUrl() {
            return InteriorTwoAccessoryUrl;
        }

        public void setInteriorTwoAccessoryUrl(String interiorTwoAccessoryUrl) {
            InteriorTwoAccessoryUrl = interiorTwoAccessoryUrl;
        }

        public String getMedinsinfoActivationId() {
            return MedinsinfoActivationId;
        }

        public void setMedinsinfoActivationId(String MedinsinfoActivationId) {
            this.MedinsinfoActivationId = MedinsinfoActivationId;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getFixmedins_name() {
            return fixmedins_name;
        }

        public void setFixmedins_name(String fixmedins_name) {
            this.fixmedins_name = fixmedins_name;
        }

        public String getFixmedins_phone() {
            return fixmedins_phone;
        }

        public void setFixmedins_phone(String fixmedins_phone) {
            this.fixmedins_phone = fixmedins_phone;
        }

        public String getFixmedins_type() {
            return fixmedins_type;
        }

        public void setFixmedins_type(String fixmedins_type) {
            this.fixmedins_type = fixmedins_type;
        }

        public String getBusinessLicenseAccessoryId() {
            return BusinessLicenseAccessoryId;
        }

        public void setBusinessLicenseAccessoryId(String BusinessLicenseAccessoryId) {
            this.BusinessLicenseAccessoryId = BusinessLicenseAccessoryId;
        }

        public String getBusinessLicenseNo() {
            return BusinessLicenseNo;
        }

        public void setBusinessLicenseNo(String BusinessLicenseNo) {
            this.BusinessLicenseNo = BusinessLicenseNo;
        }

        public String getDirector() {
            return Director;
        }

        public void setDirector(String Director) {
            this.Director = Director;
        }

        public String getIDNumber() {
            return IDNumber;
        }

        public void setIDNumber(String IDNumber) {
            this.IDNumber = IDNumber;
        }

        public String getFrontAccessoryId() {
            return FrontAccessoryId;
        }

        public void setFrontAccessoryId(String FrontAccessoryId) {
            this.FrontAccessoryId = FrontAccessoryId;
        }

        public String getBackAccessoryId() {
            return BackAccessoryId;
        }

        public void setBackAccessoryId(String BackAccessoryId) {
            this.BackAccessoryId = BackAccessoryId;
        }

        public String getEntryMode() {
            return EntryMode;
        }

        public void setEntryMode(String EntryMode) {
            this.EntryMode = EntryMode;
        }

        public String getEntryModeName() {
            return EntryModeName;
        }

        public void setEntryModeName(String EntryModeName) {
            this.EntryModeName = EntryModeName;
        }

        public String getApproveStatus() {
            return ApproveStatus;
        }

        public void setApproveStatus(String ApproveStatus) {
            this.ApproveStatus = ApproveStatus;
        }

        public String getApproveNum() {
            return ApproveNum;
        }

        public void setApproveNum(String ApproveNum) {
            this.ApproveNum = ApproveNum;
        }

        public String getApprovor() {
            return Approvor;
        }

        public void setApprovor(String Approvor) {
            this.Approvor = Approvor;
        }

        public String getApproveDT() {
            return ApproveDT;
        }

        public void setApproveDT(String ApproveDT) {
            this.ApproveDT = ApproveDT;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getUpdateDT() {
            return UpdateDT;
        }

        public void setUpdateDT(String UpdateDT) {
            this.UpdateDT = UpdateDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String CreateUser) {
            this.CreateUser = CreateUser;
        }

        public String getUpdateUser() {
            return UpdateUser;
        }

        public void setUpdateUser(String UpdateUser) {
            this.UpdateUser = UpdateUser;
        }

        public String getSettlementBankCard() {
            return SettlementBankCard;
        }

        public void setSettlementBankCard(String settlementBankCard) {
            SettlementBankCard = settlementBankCard;
        }

        public String getBank() {
            return Bank;
        }

        public void setBank(String bank) {
            Bank = bank;
        }

        public String getDrugBLAccessoryId() {
            return DrugBLAccessoryId;
        }

        public void setDrugBLAccessoryId(String drugBLAccessoryId) {
            DrugBLAccessoryId = drugBLAccessoryId;
        }

        public String getMedicalDeviceBLAccessoryId() {
            return MedicalDeviceBLAccessoryId;
        }

        public void setMedicalDeviceBLAccessoryId(String medicalDeviceBLAccessoryId) {
            MedicalDeviceBLAccessoryId = medicalDeviceBLAccessoryId;
        }

        public String getGSCertificateAccessoryId() {
            return GSCertificateAccessoryId;
        }

        public void setGSCertificateAccessoryId(String GSCertificateAccessoryId) {
            this.GSCertificateAccessoryId = GSCertificateAccessoryId;
        }

        public String getAccountLicenceAccessoryId() {
            return AccountLicenceAccessoryId;
        }

        public void setAccountLicenceAccessoryId(String accountLicenceAccessoryId) {
            AccountLicenceAccessoryId = accountLicenceAccessoryId;
        }

        public String getDoorHeaderAccessoryId() {
            return DoorHeaderAccessoryId;
        }

        public void setDoorHeaderAccessoryId(String doorHeaderAccessoryId) {
            DoorHeaderAccessoryId = doorHeaderAccessoryId;
        }

        public String getInteriorOneAccessoryId() {
            return InteriorOneAccessoryId;
        }

        public void setInteriorOneAccessoryId(String interiorOneAccessoryId) {
            InteriorOneAccessoryId = interiorOneAccessoryId;
        }

        public String getInteriorTwoAccessoryId() {
            return InteriorTwoAccessoryId;
        }

        public void setInteriorTwoAccessoryId(String interiorTwoAccessoryId) {
            InteriorTwoAccessoryId = interiorTwoAccessoryId;
        }

        public String getRejectReason() {
            return RejectReason;
        }

        public void setRejectReason(String rejectReason) {
            RejectReason = rejectReason;
        }

        public String getDr_name() {
            return dr_name;
        }

        public void setDr_name(String dr_name) {
            this.dr_name = dr_name;
        }

        public String getDr_phone() {
            return dr_phone;
        }

        public void setDr_phone(String dr_phone) {
            this.dr_phone = dr_phone;
        }

        public String getQualificationCertificateNo() {
            return QualificationCertificateNo;
        }

        public void setQualificationCertificateNo(String qualificationCertificateNo) {
            QualificationCertificateNo = qualificationCertificateNo;
        }

        public String getDoctorAccessoryId() {
            return DoctorAccessoryId;
        }

        public void setDoctorAccessoryId(String doctorAccessoryId) {
            DoctorAccessoryId = doctorAccessoryId;
        }

        public String getDoctorAccessoryUrl() {
            return DoctorAccessoryUrl;
        }

        public void setDoctorAccessoryUrl(String doctorAccessoryUrl) {
            DoctorAccessoryUrl = doctorAccessoryUrl;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.BusinessLicenseAccessoryUrl);
            dest.writeString(this.FrontAccessoryUrl);
            dest.writeString(this.BackAccessoryUrl);
            dest.writeString(this.DrugBLAccessoryUrl);
            dest.writeString(this.MedicalDeviceBLAccessoryUrl);
            dest.writeString(this.GSCertificateAccessoryUrl);
            dest.writeString(this.AccountLicenceAccessoryUrl);
            dest.writeString(this.DoorHeaderAccessoryUrl);
            dest.writeString(this.InteriorOneAccessoryUrl);
            dest.writeString(this.InteriorTwoAccessoryUrl);
            dest.writeString(this.MedinsinfoActivationId);
            dest.writeString(this.fixmedins_code);
            dest.writeString(this.fixmedins_name);
            dest.writeString(this.fixmedins_phone);
            dest.writeString(this.fixmedins_type);
            dest.writeString(this.BusinessLicenseAccessoryId);
            dest.writeString(this.BusinessLicenseNo);
            dest.writeString(this.Director);
            dest.writeString(this.IDNumber);
            dest.writeString(this.FrontAccessoryId);
            dest.writeString(this.BackAccessoryId);
            dest.writeString(this.EntryMode);
            dest.writeString(this.EntryModeName);
            dest.writeString(this.ApproveStatus);
            dest.writeString(this.ApproveNum);
            dest.writeString(this.Approvor);
            dest.writeString(this.ApproveDT);
            dest.writeString(this.CreateDT);
            dest.writeString(this.UpdateDT);
            dest.writeString(this.CreateUser);
            dest.writeString(this.UpdateUser);
            dest.writeString(this.SettlementBankCard);
            dest.writeString(this.Bank);
            dest.writeString(this.DrugBLAccessoryId);
            dest.writeString(this.MedicalDeviceBLAccessoryId);
            dest.writeString(this.GSCertificateAccessoryId);
            dest.writeString(this.AccountLicenceAccessoryId);
            dest.writeString(this.DoorHeaderAccessoryId);
            dest.writeString(this.InteriorOneAccessoryId);
            dest.writeString(this.InteriorTwoAccessoryId);
            dest.writeString(this.RejectReason);
            dest.writeString(this.dr_name);
            dest.writeString(this.dr_phone);
            dest.writeString(this.QualificationCertificateNo);
            dest.writeString(this.DoctorAccessoryId);
            dest.writeString(this.DoctorAccessoryUrl);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.BusinessLicenseAccessoryUrl = in.readString();
            this.FrontAccessoryUrl = in.readString();
            this.BackAccessoryUrl = in.readString();
            this.DrugBLAccessoryUrl = in.readString();
            this.MedicalDeviceBLAccessoryUrl = in.readString();
            this.GSCertificateAccessoryUrl = in.readString();
            this.AccountLicenceAccessoryUrl = in.readString();
            this.DoorHeaderAccessoryUrl = in.readString();
            this.InteriorOneAccessoryUrl = in.readString();
            this.InteriorTwoAccessoryUrl = in.readString();
            this.MedinsinfoActivationId = in.readString();
            this.fixmedins_code = in.readString();
            this.fixmedins_name = in.readString();
            this.fixmedins_phone = in.readString();
            this.fixmedins_type = in.readString();
            this.BusinessLicenseAccessoryId = in.readString();
            this.BusinessLicenseNo = in.readString();
            this.Director = in.readString();
            this.IDNumber = in.readString();
            this.FrontAccessoryId = in.readString();
            this.BackAccessoryId = in.readString();
            this.EntryMode = in.readString();
            this.EntryModeName = in.readString();
            this.ApproveStatus = in.readString();
            this.ApproveNum = in.readString();
            this.Approvor = in.readString();
            this.ApproveDT = in.readString();
            this.CreateDT = in.readString();
            this.UpdateDT = in.readString();
            this.CreateUser = in.readString();
            this.UpdateUser = in.readString();
            this.SettlementBankCard = in.readString();
            this.Bank = in.readString();
            this.DrugBLAccessoryId = in.readString();
            this.MedicalDeviceBLAccessoryId = in.readString();
            this.GSCertificateAccessoryId = in.readString();
            this.AccountLicenceAccessoryId = in.readString();
            this.DoorHeaderAccessoryId = in.readString();
            this.InteriorOneAccessoryId = in.readString();
            this.InteriorTwoAccessoryId = in.readString();
            this.RejectReason = in.readString();
            this.dr_name = in.readString();
            this.dr_phone = in.readString();
            this.QualificationCertificateNo = in.readString();
            this.DoctorAccessoryId = in.readString();
            this.DoctorAccessoryUrl = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //查询机构认证（管理员）
    public static void sendSettleInApplyResultRequest(final String TAG,final CustomerJsonCallBack<SettleInApplyResultModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_AUTH_QUERYMEDINSINFOACTIVATION_URL, "", callback);
    }

    //查询机构认证（医师或其他人员）
    public static void sendSettleInApplyResultDoctorRequest(final String TAG,String doctorId,final CustomerJsonCallBack<SettleInApplyResultModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("doctorId", doctorId);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_AUTH_QUERYDOCTORACTIVATION_URL, jsonObject.toJSONString(), callback);
    }
}
