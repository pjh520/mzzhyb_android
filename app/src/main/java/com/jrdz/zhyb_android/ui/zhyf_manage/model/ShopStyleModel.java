package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-15
 * 描    述：
 * ================================================
 */
public class ShopStyleModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-15 15:28:05
     * data : {"BannerAccessoryId1":"","BannerAccessoryId2":"","BannerAccessoryId3":"","Intervaltime":0,"IsOline":2,"fixmedins_code":"H61080200145","HomePage":[{"CatalogueId":7,"CatalogueName":"添加一"},{"CatalogueId":1,"CatalogueName":"常备药"},{"CatalogueId":2,"CatalogueName":"男性专区"},{"CatalogueId":3,"CatalogueName":"女性专区"},{"CatalogueId":8,"CatalogueName":"测试二"},{"CatalogueId":9,"CatalogueName":"测试三"},{"CatalogueId":10,"CatalogueName":"测试四"},{"CatalogueId":11,"CatalogueName":"测试五"}],"Classification":[{"CatalogueId":4,"CatalogueName":"感冒发热"},{"CatalogueId":5,"CatalogueName":"男性健康"},{"CatalogueId":6,"CatalogueName":"女性健康"}]}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * BannerAccessoryId1 :
         * BannerAccessoryId2 :
         * BannerAccessoryId3 :
         * Intervaltime : 0
         * IsOline : 2
         * fixmedins_code : H61080200145
         * HomePage : [{"CatalogueId":7,"CatalogueName":"添加一"},{"CatalogueId":1,"CatalogueName":"常备药"},{"CatalogueId":2,"CatalogueName":"男性专区"},{"CatalogueId":3,"CatalogueName":"女性专区"},{"CatalogueId":8,"CatalogueName":"测试二"},{"CatalogueId":9,"CatalogueName":"测试三"},{"CatalogueId":10,"CatalogueName":"测试四"},{"CatalogueId":11,"CatalogueName":"测试五"}]
         * Classification : [{"CatalogueId":4,"CatalogueName":"感冒发热"},{"CatalogueId":5,"CatalogueName":"男性健康"},{"CatalogueId":6,"CatalogueName":"女性健康"}]
         */

        private String BannerAccessoryId1;
        private String BannerAccessoryId2;
        private String BannerAccessoryId3;
        private String BannerAccessoryUrl1;
        private String BannerAccessoryUrl2;
        private String BannerAccessoryUrl3;
        private int Intervaltime;
        private String IsOline;
        private String fixmedins_code;
        private List<PhaSortModel.DataBean> HomePage;
        private List<ClassificationBean> Classification;

        public String getBannerAccessoryId1() {
            return BannerAccessoryId1;
        }

        public void setBannerAccessoryId1(String BannerAccessoryId1) {
            this.BannerAccessoryId1 = BannerAccessoryId1;
        }

        public String getBannerAccessoryId2() {
            return BannerAccessoryId2;
        }

        public void setBannerAccessoryId2(String BannerAccessoryId2) {
            this.BannerAccessoryId2 = BannerAccessoryId2;
        }

        public String getBannerAccessoryId3() {
            return BannerAccessoryId3;
        }

        public void setBannerAccessoryId3(String BannerAccessoryId3) {
            this.BannerAccessoryId3 = BannerAccessoryId3;
        }

        public String getBannerAccessoryUrl1() {
            return BannerAccessoryUrl1;
        }

        public void setBannerAccessoryUrl1(String bannerAccessoryUrl1) {
            BannerAccessoryUrl1 = bannerAccessoryUrl1;
        }

        public String getBannerAccessoryUrl2() {
            return BannerAccessoryUrl2;
        }

        public void setBannerAccessoryUrl2(String bannerAccessoryUrl2) {
            BannerAccessoryUrl2 = bannerAccessoryUrl2;
        }

        public String getBannerAccessoryUrl3() {
            return BannerAccessoryUrl3;
        }

        public void setBannerAccessoryUrl3(String bannerAccessoryUrl3) {
            BannerAccessoryUrl3 = bannerAccessoryUrl3;
        }

        public int getIntervaltime() {
            return Intervaltime;
        }

        public void setIntervaltime(int Intervaltime) {
            this.Intervaltime = Intervaltime;
        }

        public String getIsOline() {
            return IsOline;
        }

        public void setIsOline(String IsOline) {
            this.IsOline = IsOline;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public List<PhaSortModel.DataBean> getHomePage() {
            return HomePage;
        }

        public void setHomePage(List<PhaSortModel.DataBean> HomePage) {
            this.HomePage = HomePage;
        }

        public List<ClassificationBean> getClassification() {
            return Classification;
        }

        public void setClassification(List<ClassificationBean> Classification) {
            this.Classification = Classification;
        }

        public static class ClassificationBean implements MultiItemEntity {
            /**
             * CatalogueId : 4
             * CatalogueName : 感冒发热
             */

            private String CatalogueId;
            private String CatalogueName;
            private int IsSystem;//是否是系统的目录 1是，2是自定义 3是添加按钮

            public ClassificationBean(String catalogueId, String catalogueName, int isSystem) {
                CatalogueId = catalogueId;
                CatalogueName = catalogueName;
                IsSystem = isSystem;
            }

            public String getCatalogueId() {
                return CatalogueId;
            }

            public void setCatalogueId(String CatalogueId) {
                this.CatalogueId = CatalogueId;
            }

            public String getCatalogueName() {
                return CatalogueName;
            }

            public void setCatalogueName(String CatalogueName) {
                this.CatalogueName = CatalogueName;
            }

            public int getIsSystem() {
                return IsSystem;
            }

            public void setIsSystem(int isSystem) {
                IsSystem = isSystem;
            }

            @Override
            public int getItemType() {
                return IsSystem;
            }
        }
    }

    //查询店铺风格
    public static void sendShopStyleRequest(final String TAG,final CustomerJsonCallBack<ShopStyleModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_QUERYSTORESTYLE_URL, "", callback);
    }
}
