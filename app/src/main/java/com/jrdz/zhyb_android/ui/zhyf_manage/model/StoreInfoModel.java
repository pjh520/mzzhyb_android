package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-17
 * 描    述：
 * ================================================
 */
public class StoreInfoModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-17 14:25:31
     * data : [{"StoreAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/4cf4e4f1-4d0c-486f-920a-baf79a13d9f3/25fb5b5e-057f-4cf2-a97a-7c86744c2d3d.jpg","BannerAccessoryUrl1":"~/App_Upload/Storage/Medicare_Ticket/a70aa962-107a-4f98-8dc1-636b810fc19b/78234837-bc22-4d96-a655-2c7b031267f7.jpg","BannerAccessoryUrl2":"~/App_Upload/Storage/Medicare_Ticket/e906618b-c19f-40d3-b097-b86b5b0953c5/5d130579-bff6-4056-8629-9a733dbd61da.jpg","BannerAccessoryUrl3":"~/App_Upload/Storage/Medicare_Ticket/7c662da3-27e2-4f0a-8a2a-b8d94bc74f33/e6ae6a87-321d-42a1-9f87-3bb46879d3fe.jpg","BusinessLicenseAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/709c7f4b-c79c-4f6f-8353-fa6721be8e64/4315404c-c9a4-4919-9245-6edb8069902c.jpg","DrugBLAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/5957486e-bcf6-4ad7-bc8f-73606e45937c/fe50d056-93c0-4a04-87ae-75197fc4671b.jpg","MedicalDeviceBLAccessoryUrl":"","GSCertificateAccessoryUrl":"","StoreInformationId":1,"StoreName":"测试店铺","StoreAccessoryId":"4cf4e4f1-4d0c-486f-920a-baf79a13d9f3","IsAutomatic":1,"StartTime":"08:00","EndTime":"22:00","IsShowSold":1,"IsShowGoodsSold":1,"IsShowMargin":1,"StartingPrice":1,"StoreAdress":"黑龙江省哈尔滨市市辖区","DetailedAddress":"涵西街道湖园路在世纪名苑-东北门附近","Telephone":"15060338986","Wechat":"15060338986","DistributionScope":"商家自配5km","BusinessScope":"经营党委","FeaturedServices":"医保","Notice":"这是店铺公告","fixmedins_code":"H61080200145","BannerAccessoryId1":"a70aa962-107a-4f98-8dc1-636b810fc19b","BannerAccessoryId2":"e906618b-c19f-40d3-b097-b86b5b0953c5","BannerAccessoryId3":"7c662da3-27e2-4f0a-8a2a-b8d94bc74f33","Intervaltime":3,"IsOline":0,"OpeningStartTime":"08:00","OpeningEndTime":"21:00","BusinessLicenseAccessoryId":"709c7f4b-c79c-4f6f-8353-fa6721be8e64","DrugBLAccessoryId":"5957486e-bcf6-4ad7-bc8f-73606e45937c","MedicalDeviceBLAccessoryId":"00000000-0000-0000-0000-000000000000","GSCertificateAccessoryId":"00000000-0000-0000-0000-000000000000"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * StoreAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/4cf4e4f1-4d0c-486f-920a-baf79a13d9f3/25fb5b5e-057f-4cf2-a97a-7c86744c2d3d.jpg
         * BannerAccessoryUrl1 : ~/App_Upload/Storage/Medicare_Ticket/a70aa962-107a-4f98-8dc1-636b810fc19b/78234837-bc22-4d96-a655-2c7b031267f7.jpg
         * BannerAccessoryUrl2 : ~/App_Upload/Storage/Medicare_Ticket/e906618b-c19f-40d3-b097-b86b5b0953c5/5d130579-bff6-4056-8629-9a733dbd61da.jpg
         * BannerAccessoryUrl3 : ~/App_Upload/Storage/Medicare_Ticket/7c662da3-27e2-4f0a-8a2a-b8d94bc74f33/e6ae6a87-321d-42a1-9f87-3bb46879d3fe.jpg
         * BusinessLicenseAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/709c7f4b-c79c-4f6f-8353-fa6721be8e64/4315404c-c9a4-4919-9245-6edb8069902c.jpg
         * DrugBLAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/5957486e-bcf6-4ad7-bc8f-73606e45937c/fe50d056-93c0-4a04-87ae-75197fc4671b.jpg
         * MedicalDeviceBLAccessoryUrl :
         * GSCertificateAccessoryUrl :
         * StoreInformationId : 1
         * StoreName : 测试店铺
         * StoreAccessoryId : 4cf4e4f1-4d0c-486f-920a-baf79a13d9f3
         * IsAutomatic : 1
         * StartTime : 08:00
         * EndTime : 22:00
         * IsShowSold : 1
         * IsShowGoodsSold : 1
         * IsShowMargin : 1
         * StartingPrice : 1
         * StoreAdress : 黑龙江省哈尔滨市市辖区
         * DetailedAddress : 涵西街道湖园路在世纪名苑-东北门附近
         * Telephone : 15060338986
         * Wechat : 15060338986
         * DistributionScope : 商家自配5km
         * BusinessScope : 经营党委
         * FeaturedServices : 医保
         * Notice : 这是店铺公告
         * fixmedins_code : H61080200145
         * BannerAccessoryId1 : a70aa962-107a-4f98-8dc1-636b810fc19b
         * BannerAccessoryId2 : e906618b-c19f-40d3-b097-b86b5b0953c5
         * BannerAccessoryId3 : 7c662da3-27e2-4f0a-8a2a-b8d94bc74f33
         * Intervaltime : 3
         * IsOline : 0
         * OpeningStartTime : 08:00
         * OpeningEndTime : 21:00
         * BusinessLicenseAccessoryId : 709c7f4b-c79c-4f6f-8353-fa6721be8e64
         * DrugBLAccessoryId : 5957486e-bcf6-4ad7-bc8f-73606e45937c
         * MedicalDeviceBLAccessoryId : 00000000-0000-0000-0000-000000000000
         * GSCertificateAccessoryId : 00000000-0000-0000-0000-000000000000
         */

        private String StoreAccessoryUrl;
        private String BusinessLicenseAccessoryUrl;
        private String DrugBLAccessoryUrl;
        private String MedicalDeviceBLAccessoryUrl;
        private String GSCertificateAccessoryUrl;
        private String StoreInformationId;
        private String StoreName;
        private String StoreAccessoryId;
        private String IsAutomatic;
        private String StartTime;
        private String EndTime;
        private String IsShowSold;
        private String IsShowGoodsSold;
        private String IsShowMargin;
        private String StartingPrice;
        private String StoreAdress;
        private String DetailedAddress;
        private String Telephone;
        private String Wechat;
        private String DistributionScope;
        private String BusinessScope;
        private String FeaturedServices;
        private String Notice;
        private String fixmedins_code;
        private String OpeningStartTime;
        private String OpeningEndTime;
        private String BusinessLicenseAccessoryId;
        private String DrugBLAccessoryId;
        private String MedicalDeviceBLAccessoryId;
        private String GSCertificateAccessoryId;

        public String getStoreAccessoryUrl() {
            return StoreAccessoryUrl;
        }

        public void setStoreAccessoryUrl(String StoreAccessoryUrl) {
            this.StoreAccessoryUrl = StoreAccessoryUrl;
        }

        public String getBusinessLicenseAccessoryUrl() {
            return BusinessLicenseAccessoryUrl;
        }

        public void setBusinessLicenseAccessoryUrl(String BusinessLicenseAccessoryUrl) {
            this.BusinessLicenseAccessoryUrl = BusinessLicenseAccessoryUrl;
        }

        public String getDrugBLAccessoryUrl() {
            return DrugBLAccessoryUrl;
        }

        public void setDrugBLAccessoryUrl(String DrugBLAccessoryUrl) {
            this.DrugBLAccessoryUrl = DrugBLAccessoryUrl;
        }

        public String getMedicalDeviceBLAccessoryUrl() {
            return MedicalDeviceBLAccessoryUrl;
        }

        public void setMedicalDeviceBLAccessoryUrl(String MedicalDeviceBLAccessoryUrl) {
            this.MedicalDeviceBLAccessoryUrl = MedicalDeviceBLAccessoryUrl;
        }

        public String getGSCertificateAccessoryUrl() {
            return GSCertificateAccessoryUrl;
        }

        public void setGSCertificateAccessoryUrl(String GSCertificateAccessoryUrl) {
            this.GSCertificateAccessoryUrl = GSCertificateAccessoryUrl;
        }

        public String getStoreInformationId() {
            return StoreInformationId;
        }

        public void setStoreInformationId(String StoreInformationId) {
            this.StoreInformationId = StoreInformationId;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public String getStoreAccessoryId() {
            return StoreAccessoryId;
        }

        public void setStoreAccessoryId(String StoreAccessoryId) {
            this.StoreAccessoryId = StoreAccessoryId;
        }

        public String getIsAutomatic() {
            return IsAutomatic;
        }

        public void setIsAutomatic(String IsAutomatic) {
            this.IsAutomatic = IsAutomatic;
        }

        public String getStartTime() {
            return StartTime;
        }

        public void setStartTime(String StartTime) {
            this.StartTime = StartTime;
        }

        public String getEndTime() {
            return EndTime;
        }

        public void setEndTime(String EndTime) {
            this.EndTime = EndTime;
        }

        public String getIsShowSold() {
            return IsShowSold;
        }

        public void setIsShowSold(String IsShowSold) {
            this.IsShowSold = IsShowSold;
        }

        public String getIsShowGoodsSold() {
            return IsShowGoodsSold;
        }

        public void setIsShowGoodsSold(String IsShowGoodsSold) {
            this.IsShowGoodsSold = IsShowGoodsSold;
        }

        public String getIsShowMargin() {
            return IsShowMargin;
        }

        public void setIsShowMargin(String IsShowMargin) {
            this.IsShowMargin = IsShowMargin;
        }

        public String getStartingPrice() {
            return StartingPrice;
        }

        public void setStartingPrice(String StartingPrice) {
            this.StartingPrice = StartingPrice;
        }

        public String getStoreAdress() {
            return StoreAdress;
        }

        public void setStoreAdress(String StoreAdress) {
            this.StoreAdress = StoreAdress;
        }

        public String getDetailedAddress() {
            return DetailedAddress;
        }

        public void setDetailedAddress(String DetailedAddress) {
            this.DetailedAddress = DetailedAddress;
        }

        public String getTelephone() {
            return Telephone;
        }

        public void setTelephone(String Telephone) {
            this.Telephone = Telephone;
        }

        public String getWechat() {
            return Wechat;
        }

        public void setWechat(String Wechat) {
            this.Wechat = Wechat;
        }

        public String getDistributionScope() {
            return DistributionScope;
        }

        public void setDistributionScope(String DistributionScope) {
            this.DistributionScope = DistributionScope;
        }

        public String getBusinessScope() {
            return BusinessScope;
        }

        public void setBusinessScope(String BusinessScope) {
            this.BusinessScope = BusinessScope;
        }

        public String getFeaturedServices() {
            return FeaturedServices;
        }

        public void setFeaturedServices(String FeaturedServices) {
            this.FeaturedServices = FeaturedServices;
        }

        public String getNotice() {
            return Notice;
        }

        public void setNotice(String Notice) {
            this.Notice = Notice;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getOpeningStartTime() {
            return OpeningStartTime;
        }

        public void setOpeningStartTime(String OpeningStartTime) {
            this.OpeningStartTime = OpeningStartTime;
        }

        public String getOpeningEndTime() {
            return OpeningEndTime;
        }

        public void setOpeningEndTime(String OpeningEndTime) {
            this.OpeningEndTime = OpeningEndTime;
        }

        public String getBusinessLicenseAccessoryId() {
            return BusinessLicenseAccessoryId;
        }

        public void setBusinessLicenseAccessoryId(String BusinessLicenseAccessoryId) {
            this.BusinessLicenseAccessoryId = BusinessLicenseAccessoryId;
        }

        public String getDrugBLAccessoryId() {
            return DrugBLAccessoryId;
        }

        public void setDrugBLAccessoryId(String DrugBLAccessoryId) {
            this.DrugBLAccessoryId = DrugBLAccessoryId;
        }

        public String getMedicalDeviceBLAccessoryId() {
            return MedicalDeviceBLAccessoryId;
        }

        public void setMedicalDeviceBLAccessoryId(String MedicalDeviceBLAccessoryId) {
            this.MedicalDeviceBLAccessoryId = MedicalDeviceBLAccessoryId;
        }

        public String getGSCertificateAccessoryId() {
            return GSCertificateAccessoryId;
        }

        public void setGSCertificateAccessoryId(String GSCertificateAccessoryId) {
            this.GSCertificateAccessoryId = GSCertificateAccessoryId;
        }
    }


    //查询店铺信息
    public static void sendStoreInfoRequest(final String TAG,final CustomerJsonCallBack<StoreInfoModel> callback) {
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_QUERYSTOREINFORMATION_URL, "", callback);
    }
}
