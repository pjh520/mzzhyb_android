package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.frame.compiler.widget.customPop.model.BottomChooseBean;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-09-30
 * 描    述：
 * ================================================
 */
public class StoreStatusModel implements BottomChooseBean{
    private String id;
    private String text;

    public StoreStatusModel(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    @Override
    public String getText() {
        return text;
    }
}
