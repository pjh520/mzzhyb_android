package com.jrdz.zhyb_android.ui.zhyf_manage.model;

/**
 * ================================================
 * 项目名称：Pumeng_master
 * 包    名：com.languo.pumeng_master.ui.home.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/6/28 0028
 * 描    述：
 * ================================================
 */
public class TagTextBean {
    private String tagText;
    private int bgResource;

    public TagTextBean(String tagText, int bgResource) {
        this.tagText = tagText;
        this.bgResource = bgResource;
    }

    public String getTagText() {
        return tagText;
    }

    public void setTagText(String tagText) {
        this.tagText = tagText;
    }

    public int getBgResource() {
        return bgResource;
    }

    public void setBgResource(int bgResource) {
        this.bgResource = bgResource;
    }
}
