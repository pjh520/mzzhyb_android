package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-06-13
 * 描    述：
 * ================================================
 */
public class TransactionDetailsModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-06-14 18:27:24
     * totalItems : 35
     * data : [{"SharingRecordId":1,"externalTraceNo":"MH00120230307102847","OrderNo":"202303071028341","TransactionTime":"2023-03-07 10:28:48","StartDT":"2023-03-07 00:00:00","EndDT":"2023-03-22 00:00:00","OnlineAmount":1.8,"Commission":0.09,"MerchantIncome":1.7,"AccountBalance":1.7,"SharingStatus":1,"Fee":0.01,"fixmedins_code":"H61080300942","CreateDT":"2023-03-07 10:28:48","UpdateDT":"2023-03-07 10:28:48","CreateUser":"15280283921","UpdateUser":"15280283921","MedicareAmount":1.8,"PayDesc":"支付宝","OrderDesc":"连花清瘟胶囊","CatalogueIdStr":"1/2/3/4/5/"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * SharingRecordId : 1
         * externalTraceNo : MH00120230307102847
         * OrderNo : 202303071028341
         * TransactionTime : 2023-03-07 10:28:48
         * StartDT : 2023-03-07 00:00:00
         * EndDT : 2023-03-22 00:00:00
         * OnlineAmount : 1.8
         * Commission : 0.09
         * MerchantIncome : 1.7
         * AccountBalance : 1.7
         * SharingStatus : 1
         * Fee : 0.01
         * fixmedins_code : H61080300942
         * CreateDT : 2023-03-07 10:28:48
         * UpdateDT : 2023-03-07 10:28:48
         * CreateUser : 15280283921
         * UpdateUser : 15280283921
         * MedicareAmount : 1.8
         * PayDesc : 支付宝
         * OrderDesc : 连花清瘟胶囊
         * CatalogueIdStr : 1/2/3/4/5/
         */

        private String SharingRecordId;
        private String externalTraceNo;
        private String OrderNo;
        private String TransactionTime;
        private String StartDT;
        private String EndDT;
        private String OnlineAmount;
        private String Commission;
        private String MerchantIncome;
        private String AccountBalance;
        private String SharingStatus;
        private String Fee;
        private String fixmedins_code;
        private String CreateDT;
        private String UpdateDT;
        private String CreateUser;
        private String UpdateUser;
        private String MedicareAmount;
        private String PayDesc;
        private String OrderDesc;
        private String CatalogueIdStr;
        private String TotalAmount;

        public String getSharingRecordId() {
            return SharingRecordId;
        }

        public void setSharingRecordId(String SharingRecordId) {
            this.SharingRecordId = SharingRecordId;
        }

        public String getExternalTraceNo() {
            return externalTraceNo;
        }

        public void setExternalTraceNo(String externalTraceNo) {
            this.externalTraceNo = externalTraceNo;
        }

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getTransactionTime() {
            return TransactionTime;
        }

        public void setTransactionTime(String TransactionTime) {
            this.TransactionTime = TransactionTime;
        }

        public String getStartDT() {
            return StartDT;
        }

        public void setStartDT(String StartDT) {
            this.StartDT = StartDT;
        }

        public String getEndDT() {
            return EndDT;
        }

        public void setEndDT(String EndDT) {
            this.EndDT = EndDT;
        }

        public String getOnlineAmount() {
            return OnlineAmount;
        }

        public void setOnlineAmount(String OnlineAmount) {
            this.OnlineAmount = OnlineAmount;
        }

        public String getCommission() {
            return Commission;
        }

        public void setCommission(String Commission) {
            this.Commission = Commission;
        }

        public String getMerchantIncome() {
            return MerchantIncome;
        }

        public void setMerchantIncome(String MerchantIncome) {
            this.MerchantIncome = MerchantIncome;
        }

        public String getAccountBalance() {
            return AccountBalance;
        }

        public void setAccountBalance(String AccountBalance) {
            this.AccountBalance = AccountBalance;
        }

        public String getSharingStatus() {
            return SharingStatus;
        }

        public void setSharingStatus(String SharingStatus) {
            this.SharingStatus = SharingStatus;
        }

        public String getFee() {
            return Fee;
        }

        public void setFee(String Fee) {
            this.Fee = Fee;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getUpdateDT() {
            return UpdateDT;
        }

        public void setUpdateDT(String UpdateDT) {
            this.UpdateDT = UpdateDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String CreateUser) {
            this.CreateUser = CreateUser;
        }

        public String getUpdateUser() {
            return UpdateUser;
        }

        public void setUpdateUser(String UpdateUser) {
            this.UpdateUser = UpdateUser;
        }

        public String getMedicareAmount() {
            return MedicareAmount;
        }

        public void setMedicareAmount(String MedicareAmount) {
            this.MedicareAmount = MedicareAmount;
        }

        public String getPayDesc() {
            return PayDesc;
        }

        public void setPayDesc(String PayDesc) {
            this.PayDesc = PayDesc;
        }

        public String getOrderDesc() {
            return OrderDesc;
        }

        public void setOrderDesc(String OrderDesc) {
            this.OrderDesc = OrderDesc;
        }

        public String getCatalogueIdStr() {
            return CatalogueIdStr;
        }

        public void setCatalogueIdStr(String CatalogueIdStr) {
            this.CatalogueIdStr = CatalogueIdStr;
        }

        public String getTotalAmount() {
            return TotalAmount;
        }

        public void setTotalAmount(String totalAmount) {
            TotalAmount = totalAmount;
        }
    }

    //提现记录列表
    public static void sendTransactionDetailsRequest(final String TAG, String pageindex, String pagesize, String begindate, String enddate,
                                                     String PayType, String CatalogueId, CustomerJsonCallBack<TransactionDetailsModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("begindate", begindate);//开始时间
        jsonObject.put("enddate", enddate);//结束时间
        jsonObject.put("PayType", PayType);//支付方式（1、医保 2、支付宝 3、微信 4、企业基金 5、其他）
        jsonObject.put("CatalogueId", CatalogueId);//药品种类目录标识
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_STORE_GETSHARINGRECORD_URL, jsonObject.toJSONString(), callback);
    }

    //提现记录列表--用户端
    public static void sendInsuredTransactionDetailsRequest(final String TAG, String pageindex, String pagesize, String begindate, String enddate,
                                                     String PayType, String CatalogueId, CustomerJsonCallBack<TransactionDetailsModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("begindate", begindate);//开始时间
        jsonObject.put("enddate", enddate);//结束时间
        jsonObject.put("PayType", PayType);//支付方式（1、医保 2、支付宝 3、微信 4、企业基金 5、其他）
        jsonObject.put("CatalogueId", CatalogueId);//药品种类目录标识
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_GETUSERSHARINGRECORD_URL, jsonObject.toJSONString(), callback);
    }
}
