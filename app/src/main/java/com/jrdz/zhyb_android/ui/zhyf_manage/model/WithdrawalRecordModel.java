package com.jrdz.zhyb_android.ui.zhyf_manage.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：
 * ================================================
 */
public class WithdrawalRecordModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-12-01 11:26:48
     * data : [{"TakeRecordId":1,"SerialNumber":"20221201112222002","ApplyTime":"2022-12-01 11:22:22","TransferTime":"2022-12-01 11:22:22","ReceiveMethod":2,"ReceiveAccount":"15060338985","FullName":"彭俊鸿","Phone":"15060338985","TakeAmount":1000,"ServiceCharge":0,"ReceivedAmount":1000,"AccountBalance":2000,"TakeStatus":1,"TakeAccessoryId":"f999f679-f767-435d-8fe8-034e648b4040","TakeRemark":"","CreateDT":"2022-12-01 11:22:22","UpdateDT":"2022-12-01 11:22:22","CreateUser":"99","UpdateUser":"99","UserId":"772b9816-fa5f-4029-bb58-914021453a21","UserName":"99","fixmedins_code":"H61080200145","TakeType":1}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * TakeRecordId : 1
         * SerialNumber : 20221201112222002
         * ApplyTime : 2022-12-01 11:22:22
         * TransferTime : 2022-12-01 11:22:22
         * ReceiveMethod : 2
         * ReceiveAccount : 15060338985
         * FullName : 彭俊鸿
         * Phone : 15060338985
         * TakeAmount : 1000
         * ServiceCharge : 0
         * ReceivedAmount : 1000
         * AccountBalance : 2000
         * TakeStatus : 1
         * TakeAccessoryId : f999f679-f767-435d-8fe8-034e648b4040
         * TakeRemark :
         * CreateDT : 2022-12-01 11:22:22
         * UpdateDT : 2022-12-01 11:22:22
         * CreateUser : 99
         * UpdateUser : 99
         * UserId : 772b9816-fa5f-4029-bb58-914021453a21
         * UserName : 99
         * fixmedins_code : H61080200145
         * TakeType : 1
         */

        private String TakeRecordId;
        private String ApplyTime;
        private String TransferTime;
        private String ReceiveMethod;
        private String ReceiveAccount;
        private String FullName;
        private String Phone;
        private String TakeAmount;
        private int ReceivedAmount;
        private String TakeStatus;
        private String UserName;
        private String TakeType;

        public String getTakeRecordId() {
            return TakeRecordId;
        }

        public void setTakeRecordId(String TakeRecordId) {
            this.TakeRecordId = TakeRecordId;
        }

        public String getApplyTime() {
            return ApplyTime;
        }

        public void setApplyTime(String ApplyTime) {
            this.ApplyTime = ApplyTime;
        }

        public String getTransferTime() {
            return TransferTime;
        }

        public void setTransferTime(String TransferTime) {
            this.TransferTime = TransferTime;
        }

        public String getReceiveMethod() {
            return ReceiveMethod;
        }

        public void setReceiveMethod(String ReceiveMethod) {
            this.ReceiveMethod = ReceiveMethod;
        }

        public String getReceiveAccount() {
            return ReceiveAccount;
        }

        public void setReceiveAccount(String ReceiveAccount) {
            this.ReceiveAccount = ReceiveAccount;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String Phone) {
            this.Phone = Phone;
        }

        public String getTakeAmount() {
            return TakeAmount;
        }

        public void setTakeAmount(String TakeAmount) {
            this.TakeAmount = TakeAmount;
        }

        public int getReceivedAmount() {
            return ReceivedAmount;
        }

        public void setReceivedAmount(int ReceivedAmount) {
            this.ReceivedAmount = ReceivedAmount;
        }

        public String getTakeStatus() {
            return TakeStatus;
        }

        public void setTakeStatus(String TakeStatus) {
            this.TakeStatus = TakeStatus;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getTakeType() {
            return TakeType;
        }

        public void setTakeType(String TakeType) {
            this.TakeType = TakeType;
        }
    }

    //提现记录列表
    public static void sendWithdrawalRecordRequest(final String TAG, String pageindex, String pagesize,final CustomerJsonCallBack<WithdrawalRecordModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_TAKERECORDLIST_URL, jsonObject.toJSONString(), callback);
    }
}
