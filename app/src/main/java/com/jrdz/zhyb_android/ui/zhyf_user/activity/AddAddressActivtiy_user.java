package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocation;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.geocode.GeoCodeOption;
import com.baidu.mapapi.search.geocode.GeoCodeResult;
import com.baidu.mapapi.search.geocode.GeoCoder;
import com.baidu.mapapi.search.geocode.OnGetGeoCoderResultListener;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeResult;
import com.frame.compiler.utils.AssetUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.utils.wheel.CityWheelUtils;
import com.frame.compiler.widget.switchButton.SwitchButton;
import com.frame.lbs_library.utils.CustomLocationBean;
import com.frame.lbs_library.utils.IGeoCoderListener;
import com.frame.lbs_library.utils.ILocationListener;
import com.frame.lbs_library.utils.LBSUtil;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.CityListModel;
import com.jrdz.zhyb_android.utils.location.CoordinateUtils;
import com.jrdz.zhyb_android.utils.location.LocationTool;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-01
 * 描    述：
 * ================================================
 */
public class AddAddressActivtiy_user extends BaseActivity {
    protected EditText mEtUsername;
    protected EditText mEtPhone;
    protected TextView mTvRegion;
    protected LinearLayout mLlLocation;
    protected EditText mEtDetailAddress;
    protected SwitchButton mSbDefault;
    protected ShapeTextView mTvSave;

    protected List<CityListModel> data;
    protected CityWheelUtils cityWheelUtils;
    protected String province;//省
    protected String city;//市
    protected String area;//区

    @Override
    public int getLayoutId() {
        return R.layout.activity_add_address_user;
    }

    @Override
    public void initView() {
        super.initView();
        mEtUsername = findViewById(R.id.et_username);
        mEtPhone = findViewById(R.id.et_phone);
        mTvRegion = findViewById(R.id.tv_region);
        mLlLocation = findViewById(R.id.ll_location);
        mEtDetailAddress = findViewById(R.id.et_detail_address);
        mSbDefault = findViewById(R.id.sb_default);
        mTvSave = findViewById(R.id.tv_save);
    }

    @Override
    public void initData() {
        super.initData();
        showWaitDialog();
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                String cityData = AssetUtils.loadStringAsset(AddAddressActivtiy_user.this, "cityData.json");
                data = JSON.parseArray(cityData, CityListModel.class);
                hideWaitDialog();
            }
        });
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvRegion.setOnClickListener(this);
        mLlLocation.setOnClickListener(this);
        mTvSave.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_region://选择省市区
                KeyboardUtils.hideSoftInput(AddAddressActivtiy_user.this);
                selectRegion();
                break;
            case R.id.ll_location://定位获取省市区
                showWaitDialog();
                LocationTool.getInstance().registerLocation(this,new ILocationListener() {
                            @Override
                            public void onLocationSuccess(CustomLocationBean customLocationBean) {
                                hideWaitDialog();
                                province = customLocationBean.getProvince();//省
                                city = customLocationBean.getCity();//市
                                area = customLocationBean.getDistrict();//区
                                mTvRegion.setText(customLocationBean.getProvince() + customLocationBean.getCity() + customLocationBean.getDistrict());
                                mEtDetailAddress.setText((customLocationBean.getDetail_address().contains(customLocationBean.getStreet())?"":customLocationBean.getStreet())+customLocationBean.getDetail_address());
                            }

                            @Override
                            public void onLocationError(String errorText) {
                                hideWaitDialog();
                                showTipDialog(errorText);
                            }
                        }
                );
                break;
            case R.id.tv_save://保存
                onSave();
                break;
        }
    }

    //选择省市区
    private void selectRegion() {
        if (cityWheelUtils == null) {
            cityWheelUtils = new CityWheelUtils();
        }
        cityWheelUtils.showCityWheel(AddAddressActivtiy_user.this, new CityWheelUtils.CityWheelClickListener<CityListModel, CityListModel.CityBean, CityListModel.CityBean.AreaBean>() {
            @Override
            public void onCity(CityListModel provinceData) {
                getCityData(provinceData.getCode());
            }

            @Override
            public void onArea(CityListModel.CityBean cityData) {
                getAreaData(cityData.getCode());
            }

            @Override
            public void onchooseCity(int provincePos, CityListModel provinceItemData,
                                     int cityPos, CityListModel.CityBean cityItemData,
                                     int areaPos, CityListModel.CityBean.AreaBean areaItemData) {
                province = provinceItemData.getName();//省
                city = cityItemData.getName();//市
                area = areaItemData.getName();//区
                mTvRegion.setText(provinceItemData.getName() + cityItemData.getName() + areaItemData.getName());
            }
        });
        cityWheelUtils.setProvinceData(data);
        getCityData(data.get(0).getCode());
    }

    //获取市级数据
    private void getCityData(String provinceId) {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                for (CityListModel datum : data) {
                    if (provinceId.equals(datum.getCode())) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                cityWheelUtils.setCityData(datum.getCity() == null ? new ArrayList() : datum.getCity());
                            }
                        });

                        if (datum.getCity() != null && datum.getCity().get(0) != null) {
                            getAreaData(datum.getCity().get(0).getCode());
                        }
                        break;
                    }
                }
            }
        });
    }

    //获取区级数据
    private void getAreaData(String cityId) {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                for (CityListModel datum : data) {
                    if (datum != null && datum.getCity() != null) {
                        for (CityListModel.CityBean cityBean : datum.getCity()) {
                            if (cityId.equals(cityBean.getCode())) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cityWheelUtils.setAreaData(cityBean.getArea());
                                    }
                                });
                                break;
                            }
                        }
                    }
                }
            }
        });
    }

    //保存
    protected void onSave() {
        if (EmptyUtils.isEmpty(mEtUsername.getText().toString())) {
            showShortToast("请输入收货人姓名");
            return;
        }
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString())) {
            showShortToast("请输入联系电话");
            return;
        }
        if (!"1".equals(String.valueOf(mEtPhone.getText().charAt(0))) || mEtPhone.getText().length() != 11) {
            showShortToast("请输入正确的电话号码");
            return;
        }
        if (EmptyUtils.isEmpty(mTvRegion.getText().toString())) {
            showShortToast("请选择所在地区");
            return;
        }
        if (EmptyUtils.isEmpty(mEtDetailAddress.getText().toString())) {
            showShortToast("请输入详细地址");
            return;
        }

        showWaitDialog();
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                getDataByThridPart();
//                if (Constants.Configure.IS_POS){
//                    getDataByThridPart();
//                }else {
//                    getDataByLocal();
//                }
            }
        });
    }

    //有第三方api解析地址 获取经纬度
    private void getDataByThridPart() {
        showWaitDialog();
        LBSUtil.getInstance().GeoCoder(city, mEtDetailAddress.getText().toString(), new IGeoCoderListener() {
            @Override
            public void onGeoCoderSuccess(double latitude, double longitude) {
                requestSave(latitude, longitude);
            }

            @Override
            public void onGeoCoderError(String errorText) {
                hideWaitDialog();
                showShortToast(errorText);
            }
        });
    }

    //有原生api解析地址 获取经纬度
    private void getDataByLocal() {
        Address address=LocationTool.getInstance().getLocalTion(AddAddressActivtiy_user.this, mTvRegion.getText().toString()+mEtDetailAddress.getText().toString());
        if (address==null) {
            hideWaitDialog();
            //没有检索到结果
            showShortToast("该地址不合法，暂未检测到地址经纬度");
        } else {
            CustomLocationBean customLocationBean = CoordinateUtils.GPS84ToBD09(address.getLongitude(), address.getLatitude());
            requestSave(customLocationBean.getLatitude(), customLocationBean.getLongitude());
        }
    }

    //获取到地址的经纬度之后 请求数据
    protected void requestSave(double latitude, double longitude) {
        String defalt = "0";
        if (mSbDefault.isChecked()) {
            defalt = "1";
        }
        BaseModel.sendAddAddressRequest_user(TAG, mEtUsername.getText().toString(), mEtPhone.getText().toString(),
                mTvRegion.getText().toString(), mEtDetailAddress.getText().toString(), String.valueOf(latitude), String.valueOf(longitude),province,city,
                defalt, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();

                        showShortToast("保存成功");
                        //数据检测无误，则可以待会返回上一个页面
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        goFinish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (data != null) {
            data.clear();
            data = null;
        }

        if (cityWheelUtils != null) {
            cityWheelUtils.onCleanData();
        }

        LocationTool.getInstance().unRegisterLocation();
        LBSUtil.getInstance().stopGeoCoder();
    }

    public static void newIntance(Activity activity, int requestCode) {
        Intent intent = new Intent(activity, AddAddressActivtiy_user.class);
        activity.startActivityForResult(intent, requestCode);
    }
}
