package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.home.model.KVModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.widget.pop.MedicalHistoryPop;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-08
 * 描    述：新增就诊人
 * ================================================
 */
public class AddVisitorActivity extends BaseActivity {
    protected TextView mTvRelationship;
    protected EditText mEtName;
    protected EditText mEtPhone;
    protected EditText mEtCertNo;
    protected TextView mTvSex;
    protected TextView mTvAge;
    protected ShapeTextView mStvPastMedical;
    protected ShapeTextView mStvAllergy;
    protected ShapeTextView mStvFamily;
    protected ShapeTextView mStvLiver;
    protected ShapeTextView mStvKidney;
    protected ShapeTextView mStvPregnancyLactation;
    protected ShapeTextView mTvSubmit;

    private WheelUtils wheelUtils;
    private int choosePos1 = 0;
    private MedicalHistoryPop medicalHistoryPop;
    protected String selectPastMedical="",etPastMedical = "", selectAllergy="",etAllergy = "", selectFamily="",etFamily = "";

    @Override
    public int getLayoutId() {
        return R.layout.activity_add_visitor;
    }

    @Override
    public void initView() {
        super.initView();
        mTvRelationship = findViewById(R.id.tv_relationship);
        mEtName = findViewById(R.id.et_name);
        mEtPhone = findViewById(R.id.et_phone);
        mEtCertNo = findViewById(R.id.et_cert_no);
        mTvSex = findViewById(R.id.tv_sex);
        mTvAge = findViewById(R.id.tv_age);
        mStvPastMedical = findViewById(R.id.stv_past_medical);
        mStvAllergy = findViewById(R.id.stv_allergy);
        mStvFamily = findViewById(R.id.stv_family);
        mStvLiver = findViewById(R.id.stv_liver);
        mStvKidney = findViewById(R.id.stv_kidney);
        mStvPregnancyLactation = findViewById(R.id.stv_pregnancy_lactation);
        mTvSubmit = findViewById(R.id.tv_submit);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        if (mTvRelationship!=null){
            mTvRelationship.setOnClickListener(this);
        }

        mStvPastMedical.setOnClickListener(this);
        mStvAllergy.setOnClickListener(this);
        mStvFamily.setOnClickListener(this);
        mStvLiver.setOnClickListener(this);
        mStvKidney.setOnClickListener(this);
        mStvPregnancyLactation.setOnClickListener(this);
        mTvSubmit.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.stv_past_medical://过往病史
                onPastMedical();
                break;
            case R.id.stv_allergy://过敏史
                onAllergy();
                break;
            case R.id.stv_family://家族病史
                onFamily();
                break;
            case R.id.stv_liver://肝功能异常
                onLiver();
                break;
            case R.id.stv_kidney://肾功能异常
                onKidney();
                break;
            case R.id.stv_pregnancy_lactation://妊娠哺乳期
                onPregnancyLactation();
                break;
            case R.id.tv_relationship://选择和本人的关系
                KeyboardUtils.hideSoftInput(AddVisitorActivity.this);
                if (wheelUtils == null) {
                    wheelUtils = new WheelUtils();
                }

                wheelUtils.showWheel(AddVisitorActivity.this, "", choosePos1,
                        CommonlyUsedDataUtils.getInstance().getRelationship(), new WheelUtils.WheelClickListener<KVModel>() {
                            @Override
                            public void onchooseDate(int choosePos, KVModel dateInfo) {
                                choosePos1 = choosePos;
                                mTvRelationship.setTag(dateInfo.getCode());
                                mTvRelationship.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                            }
                        });
                break;
            case R.id.tv_submit://提交
                onSubmit();
                break;
        }
    }

    //过往病史弹框
    private void onPastMedical() {
        if (mStvPastMedical.isSelected()) {
            mStvPastMedical.setSelected(false);
            mStvPastMedical.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.bar_transparent))
                    .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1))
                    .setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();

            mStvPastMedical.setTextColor(getResources().getColor(R.color.color_333333));
        } else {
            if (!EmptyUtils.isEmpty(getShowText(selectPastMedical,etPastMedical))){
                mStvPastMedical.setSelected(true);
                mStvPastMedical.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0))
                        .setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent)).intoBackground();
                mStvPastMedical.setTextColor(getResources().getColor(R.color.white));
            }

            KeyboardUtils.hideSoftInput(AddVisitorActivity.this);
            if (medicalHistoryPop == null) {
                medicalHistoryPop = new MedicalHistoryPop(AddVisitorActivity.this, "过往病史", CommonlyUsedDataUtils.getInstance().getPastMedicalDatas(), selectPastMedical, etPastMedical);
            } else {
                medicalHistoryPop.setData("过往病史", CommonlyUsedDataUtils.getInstance().getPastMedicalDatas(), selectPastMedical, etPastMedical);
            }
            medicalHistoryPop.setOnListener(new MedicalHistoryPop.IOptionListener() {
                @Override
                public void sure(String selectDatas, String etContent) {
                    selectPastMedical = selectDatas;
                    etPastMedical = etContent;
                    //设置需要展示的数据
                    String showText=getShowText(selectDatas,etContent);

                    if (EmptyUtils.isEmpty(showText)) {
                        mStvPastMedical.setText("过往病史");

                        mStvPastMedical.setSelected(false);
                        mStvPastMedical.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.bar_transparent))
                                .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1))
                                .setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();

                        mStvPastMedical.setTextColor(getResources().getColor(R.color.color_333333));
                    } else {
                        mStvPastMedical.setText("过往病史:" + showText);

                        mStvPastMedical.setSelected(true);
                        mStvPastMedical.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0))
                                .setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent)).intoBackground();

                        mStvPastMedical.setTextColor(getResources().getColor(R.color.white));
                    }
                }
            });
            medicalHistoryPop.showPopupWindow();
        }
    }

    //过敏史弹框
    private void onAllergy() {
        if (mStvAllergy.isSelected()) {
            mStvAllergy.setSelected(false);
            mStvAllergy.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.bar_transparent))
                    .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1))
                    .setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();

            mStvAllergy.setTextColor(getResources().getColor(R.color.color_333333));
        } else {
            if (!EmptyUtils.isEmpty(getShowText(selectAllergy,etAllergy))){
                mStvAllergy.setSelected(true);
                mStvAllergy.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0))
                        .setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent)).intoBackground();
                mStvAllergy.setTextColor(getResources().getColor(R.color.white));
            }

            KeyboardUtils.hideSoftInput(AddVisitorActivity.this);
            if (medicalHistoryPop == null) {
                medicalHistoryPop = new MedicalHistoryPop(AddVisitorActivity.this, "过敏史", CommonlyUsedDataUtils.getInstance().getAllergyDatas(), selectAllergy, etAllergy);
            } else {
                medicalHistoryPop.setData("过敏史", CommonlyUsedDataUtils.getInstance().getAllergyDatas(), selectAllergy, etAllergy);
            }
            medicalHistoryPop.setOnListener(new MedicalHistoryPop.IOptionListener() {
                @Override
                public void sure(String selectDatas, String etContent) {
                    selectAllergy = selectDatas;
                    etAllergy = etContent;

                    //设置需要展示的数据
                    String showText=getShowText(selectDatas,etContent);

                    if (EmptyUtils.isEmpty(showText)) {
                        mStvAllergy.setText("过敏史");

                        mStvAllergy.setSelected(false);
                        mStvAllergy.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.bar_transparent))
                                .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1))
                                .setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();

                        mStvAllergy.setTextColor(getResources().getColor(R.color.color_333333));
                    } else {
                        mStvAllergy.setText("过敏史:" + showText);

                        mStvAllergy.setSelected(true);
                        mStvAllergy.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0))
                                .setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent)).intoBackground();

                        mStvAllergy.setTextColor(getResources().getColor(R.color.white));
                    }
                }
            });
            medicalHistoryPop.showPopupWindow();
        }
    }

    //家族病史弹框
    private void onFamily() {
        if (mStvFamily.isSelected()) {
            mStvFamily.setSelected(false);
            mStvFamily.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.bar_transparent))
                    .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1))
                    .setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();

            mStvFamily.setTextColor(getResources().getColor(R.color.color_333333));
        } else {
            if (!EmptyUtils.isEmpty(getShowText(selectFamily,etFamily))){
                mStvFamily.setSelected(true);
                mStvFamily.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0))
                        .setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent)).intoBackground();
                mStvFamily.setTextColor(getResources().getColor(R.color.white));
            }

            KeyboardUtils.hideSoftInput(AddVisitorActivity.this);
            if (medicalHistoryPop == null) {
                medicalHistoryPop = new MedicalHistoryPop(AddVisitorActivity.this, "家族病史", CommonlyUsedDataUtils.getInstance().getFamilyDatas(), selectFamily, etFamily);
            } else {
                medicalHistoryPop.setData("家族病史", CommonlyUsedDataUtils.getInstance().getFamilyDatas(), selectFamily, etFamily);
            }
            medicalHistoryPop.setOnListener(new MedicalHistoryPop.IOptionListener() {
                @Override
                public void sure(String selectDatas, String etContent) {
                    selectFamily = selectDatas;
                    etFamily = etContent;

                    //设置需要展示的数据
                    String showText=getShowText(selectDatas,etContent);
                    if (EmptyUtils.isEmpty(showText)) {
                        mStvFamily.setText("家族病史");

                        mStvFamily.setSelected(false);
                        mStvFamily.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.bar_transparent))
                                .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1))
                                .setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();

                        mStvFamily.setTextColor(getResources().getColor(R.color.color_333333));
                    } else {
                        mStvFamily.setText("家族病史:" + showText);

                        mStvFamily.setSelected(true);
                        mStvFamily.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0))
                                .setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent)).intoBackground();

                        mStvFamily.setTextColor(getResources().getColor(R.color.white));
                    }
                }
            });
            medicalHistoryPop.showPopupWindow();
        }
    }

    //肝功能异常
    private void onLiver() {
        if (mStvLiver.isSelected()) {
            mStvLiver.setSelected(false);
            mStvLiver.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.bar_transparent))
                    .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1))
                    .setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();

            mStvLiver.setTextColor(getResources().getColor(R.color.color_333333));
        } else {
            mStvLiver.setSelected(true);
            mStvLiver.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0))
                    .setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent)).intoBackground();

            mStvLiver.setTextColor(getResources().getColor(R.color.white));
        }
    }

    //肾功能异常
    private void onKidney() {
        if (mStvKidney.isSelected()) {
            mStvKidney.setSelected(false);
            mStvKidney.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.bar_transparent))
                    .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1))
                    .setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();

            mStvKidney.setTextColor(getResources().getColor(R.color.color_333333));
        } else {
            mStvKidney.setSelected(true);
            mStvKidney.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0))
                    .setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent)).intoBackground();

            mStvKidney.setTextColor(getResources().getColor(R.color.white));
        }
    }

    //妊娠哺乳期
    private void onPregnancyLactation() {
        if (mStvPregnancyLactation.isSelected()) {
            mStvPregnancyLactation.setSelected(false);
            mStvPregnancyLactation.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.bar_transparent))
                    .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1))
                    .setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();

            mStvPregnancyLactation.setTextColor(getResources().getColor(R.color.color_333333));
        } else {
            mStvPregnancyLactation.setSelected(true);
            mStvPregnancyLactation.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0))
                    .setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent)).intoBackground();

            mStvPregnancyLactation.setTextColor(getResources().getColor(R.color.white));
        }
    }

    protected String getShowText(String selectDatas, String etContent) {
        String showText="";
        if (EmptyUtils.isEmpty(selectDatas)) {
            showText = etContent;
        } else {
            if (EmptyUtils.isEmpty(etContent)) {
                showText = selectDatas;
            } else {
                showText = selectDatas+","+ etContent;
            }
        }

        return showText;
    }

    //提交
    protected void onSubmit() {
        if (EmptyUtils.isEmpty(mTvRelationship.getText().toString())){
            showShortToast("请选择和本人的关系");
            return;
        }
        if (EmptyUtils.isEmpty(mEtName.getText().toString())){
            showShortToast("请输入用药人姓名");
            return;
        }
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString())){
            showShortToast("请输入手机号码");
            return;
        }
        if (!"1".equals(String.valueOf(mEtPhone.getText().charAt(0))) || mEtPhone.getText().length() != 11) {
            showShortToast("请输入正确的手机号码");
            return;
        }
        if (EmptyUtils.isEmpty(mEtCertNo.getText().toString())){
            showShortToast("请输入身份证号码");
            return;
        }

        showShortToast("新增就诊人成功");
        Intent intent=new Intent();
//        intent.putExtra("");
        setResult(RESULT_OK,intent);
        goFinish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (medicalHistoryPop != null) {
            medicalHistoryPop.onCleanListener();
            medicalHistoryPop.onDestroy();
        }
        if (wheelUtils != null) {
            wheelUtils.dissWheel();
        }
    }
}
