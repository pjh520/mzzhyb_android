package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.AddressManageAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.model.AddressManageModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-31
 * 描    述：地址管理--用户端
 * ================================================
 */
public class AddressManageActivtiy extends BaseRecyclerViewActivity {

    private CustomerDialogUtils customerDialogUtils;
    private int from;

    @Override
    public void initAdapter() {
        mAdapter = new AddressManageAdapter();
    }

    @Override
    public void initData() {
        from = getIntent().getIntExtra("from", 0);
        super.initData();

        setRightTitleView("新增");

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
//        setLoadMore();
    }

    @Override
    public void getData() {
        super.getData();

        AddressManageModel.sendAddAddressRequest_user(TAG, new CustomerJsonCallBack<AddressManageModel>() {
            @Override
            public void onRequestError(AddressManageModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(AddressManageModel returnData) {
                hideRefreshView();
                List<AddressManageModel.DataBean> infos = returnData.getData();
                if (mAdapter!=null&&infos != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(infos);
                    } else {
                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void rightTitleViewClick() {
        AddAddressActivtiy_user.newIntance(this, Constants.RequestCode.ADD_ADDRESS_USER_CODE);
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        if (1 == from) {
            Intent intent = new Intent();
            intent.putExtra("itemData", ((AddressManageAdapter) adapter).getItem(position));
            setResult(RESULT_OK, intent);
            goFinish();
        }
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        AddressManageAdapter addressManageAdapter = (AddressManageAdapter) adapter;
        AddressManageModel.DataBean itemData = addressManageAdapter.getItem(position);
        switch (view.getId()) {
            //设置默认
            case R.id.fl_select:
            case R.id.tv_default:
                ImageView ivSelect = (ImageView) adapter.getViewByPosition(mRecyclerView, position, R.id.iv_select);
                // 2022-11-01 请求接口重置默认
                showWaitDialog();
                BaseModel.sendUpdateAddressRequest_user(TAG, itemData.getShipToAddressId(), itemData.getFullName(),
                        itemData.getPhone(),itemData.getLocation(),itemData.getAddress(), itemData.getLatitude(),itemData.getLongitude(),itemData.getAreaName(),
                        EmptyUtils.strEmpty(itemData.getCity()),"1".equals(itemData.getIsDefault()) ? "0" : "1", new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();

                                if ("1".equals(itemData.getIsDefault())) {
                                    itemData.setIsDefault("0");
                                    ivSelect.setImageResource(R.drawable.ic_product_select_nor);

                                    addressManageAdapter.setCurrentItemData(null);
                                    addressManageAdapter.setCurrentIvSelect(null);

                                    showShortToast("取消默认成功");
                                } else {
                                    if (addressManageAdapter.getCurrentItemData() != null) {
                                        addressManageAdapter.getCurrentItemData().setIsDefault("0");
                                    }
                                    if (addressManageAdapter.getCurrentIvSelect() != null) {
                                        addressManageAdapter.getCurrentIvSelect().setImageResource(R.drawable.ic_product_select_nor);
                                    }

                                    itemData.setIsDefault("1");
                                    ivSelect.setImageResource(R.drawable.ic_product_select_pre);

                                    addressManageAdapter.setCurrentItemData(itemData);
                                    addressManageAdapter.setCurrentIvSelect(ivSelect);

                                    showShortToast("设置默认成功");
                                }
                            }
                        });
                break;
            //编辑
            case R.id.fl_edit:
            case R.id.tv_edit:
                UpdateAddressActivtiy_user.newIntance(AddressManageActivtiy.this, itemData, Constants.RequestCode.EDIT_ADDRESS_USER_CODE);
                break;
            //删除
            case R.id.fl_del:
            case R.id.tv_del:
                onDel(itemData, position);
                break;
        }
    }

    //删除
    private void onDel(AddressManageModel.DataBean itemData, int pos) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }

        customerDialogUtils.showDialog(AddressManageActivtiy.this, "提示", "是否确认删除该收货地址?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {}

                    @Override
                    public void onBtn02Click() {
                        // 2022-11-01 请求接口 删除地址
                        showWaitDialog();
                        BaseModel.sendDelAddressRequest_user(TAG, itemData.getShipToAddressId(), new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                //删除成功
                                showShortToast("删除成功");
                                if ("1".equals(itemData.getIsDefault())) {
                                    ((AddressManageAdapter) mAdapter).setCurrentItemData(null);
                                    ((AddressManageAdapter) mAdapter).setCurrentIvSelect(null);
                                }
                                mAdapter.remove(pos);
                            }
                        });
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {//新增收货地址
            String deleteData = data.getStringExtra("deleteData");
            if (!EmptyUtils.isEmpty(deleteData)) {//删除
                deleteItem(deleteData);
            } else {
                showWaitDialog();
                onRefresh(mRefreshLayout);
            }
        }
    }

    //删除指定的列表item
    private void deleteItem(String id) {
        for (int i = 0, size = mAdapter.getData().size(); i < size; i++) {
            AddressManageAdapter addressManageAdapter = (AddressManageAdapter) mAdapter;
            AddressManageModel.DataBean data = addressManageAdapter.getItem(i);
            if (id.equals(data.getShipToAddressId())) {
                mAdapter.remove(i);
                if ("1".equals(data.getIsDefault())) {
                    addressManageAdapter.setCurrentItemData(null);
                    addressManageAdapter.setCurrentIvSelect(null);
                }
                return;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, AddressManageActivtiy.class);
        context.startActivity(intent);
    }
}
