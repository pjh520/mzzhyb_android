package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.FreeBackPicAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.model.FreeBackPicModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.TwoLinkWheelModel;
import com.jrdz.zhyb_android.utils.TwoLinkWheelUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-04
 * 描    述：投诉商家
 * ================================================
 */
public class ComplaintStoreActivity extends BaseActivity implements CompressUploadSinglePicUtils_zhyf.IChoosePic {
    public final static String TAG_SELECT_PIC_01 = "1";//意见反馈图片

    public static final int PIC_NOR = 1;//新增图片
    public static final int PIC_ADDBTN = 2;//添加按钮

    private LinearLayout mLlComplaintType;
    private TextView mTvComplaintType;
    private LinearLayout mLlOrderStatus;
    private RadioGroup mSrbGroup;
    private AppCompatRadioButton mSrbHasOrder;
    private AppCompatRadioButton mSrbNoOrder;
    private EditText mEtPhone;
    private EditText mEtContent;
    private CustomeRecyclerView mCrlImglist;
    private ShapeTextView mTvSubmit;

    private String fixmedins_code;
    private CompressUploadSinglePicUtils_zhyf compressUploadSinglePicUtils;
    private FreeBackPicAdapter freeBackPicAdapter;
    private TwoLinkWheelUtils twoLinkWheelUtils;
    private ArrayList<TwoLinkWheelModel> twoLinkWheelModels = new ArrayList<>();
    ;
    private String text01, text02;

    @Override
    public int getLayoutId() {
        return R.layout.activity_complaint_store;
    }

    @Override
    public void initView() {
        super.initView();
        mLlComplaintType = findViewById(R.id.ll_complaint_type);
        mTvComplaintType = findViewById(R.id.tv_complaint_type);
        mLlOrderStatus = findViewById(R.id.ll_order_status);
        mSrbGroup = findViewById(R.id.srb_group);
        mSrbHasOrder = findViewById(R.id.srb_has_order);
        mSrbNoOrder = findViewById(R.id.srb_no_order);
        mEtPhone = findViewById(R.id.et_phone);
        mEtContent = findViewById(R.id.et_content);
        mCrlImglist = findViewById(R.id.crl_imglist);
        mTvSubmit = findViewById(R.id.tv_submit);
    }

    @Override
    public void initData() {
        fixmedins_code = getIntent().getStringExtra("fixmedins_code");
        super.initData();
        //初始化获取图片
        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);

        //设置图片列表
        mCrlImglist.setHasFixedSize(true);
        mCrlImglist.setLayoutManager(new GridLayoutManager(this, 4, RecyclerView.VERTICAL, false));
        freeBackPicAdapter = new FreeBackPicAdapter();
        mCrlImglist.setAdapter(freeBackPicAdapter);
        //添加图片增加按钮数据
        ArrayList<FreeBackPicModel> freeBackPicModels = new ArrayList<>();
        FreeBackPicModel freeBackPic_addbtn = new FreeBackPicModel("-1", "", PIC_ADDBTN);
        freeBackPicModels.add(freeBackPic_addbtn);

        freeBackPicAdapter.setNewData(freeBackPicModels);

        showWaitDialog();
        getTwoLinkWheelData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mLlComplaintType.setOnClickListener(this);

        //分类 item点击事件
        freeBackPicAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                FreeBackPicModel itemData = freeBackPicAdapter.getItem(i);
                if (PIC_NOR == itemData.getItemType()) {//查看图片
                    ImageView ivPhoto = view.findViewById(R.id.iv_photo);
                    OpenImage.with(ComplaintStoreActivity.this)
                            //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                            .setClickImageView(ivPhoto)
                            //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                            .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                            //RecyclerView的数据
                            .setImageUrl(itemData.getImg(), MediaType.IMAGE)
                            //点击的ImageView所在数据的位置
                            .setClickPosition(0)
                            //开始展示大图
                            .show();
                } else if (PIC_ADDBTN == itemData.getItemType()) {//新增按钮
                    KeyboardUtils.hideSoftInput(ComplaintStoreActivity.this);
                    if (freeBackPicAdapter.getData().size() >= 2) {
                        showShortToast("最多上传1张图片");
                        return;
                    }
                    compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_01, CompressUploadSinglePicUtils_zhyf.PIC_COMPLAINT_TAG);
                }
            }
        });

        freeBackPicAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }

                switch (view.getId()) {
                    case R.id.iv_delete://删除图片
                        freeBackPicAdapter.getData().remove(i);
                        freeBackPicAdapter.notifyDataSetChanged();
                        break;
                }
            }
        });
        mTvSubmit.setOnClickListener(this);
    }

    //获取联动数据
    private void getTwoLinkWheelData() {
        TwoLinkWheelModel twoLinkWheelModel1 = new TwoLinkWheelModel("资质问题");
        ArrayList<TwoLinkWheelModel.ChildBean> childBeanArrayList1 = new ArrayList<>();
        TwoLinkWheelModel.ChildBean childBean11 = new TwoLinkWheelModel.ChildBean("资质不全");
        TwoLinkWheelModel.ChildBean childBean12 = new TwoLinkWheelModel.ChildBean("资质不符");
        TwoLinkWheelModel.ChildBean childBean13 = new TwoLinkWheelModel.ChildBean("盗用资质");
        TwoLinkWheelModel.ChildBean childBean14 = new TwoLinkWheelModel.ChildBean("资质失效");
        TwoLinkWheelModel.ChildBean childBean15 = new TwoLinkWheelModel.ChildBean("资质造假");
        childBeanArrayList1.add(childBean11);
        childBeanArrayList1.add(childBean12);
        childBeanArrayList1.add(childBean13);
        childBeanArrayList1.add(childBean14);
        childBeanArrayList1.add(childBean15);
        twoLinkWheelModel1.setChildBean(childBeanArrayList1);

        TwoLinkWheelModel twoLinkWheelModel2 = new TwoLinkWheelModel("超出经营范围");
        ArrayList<TwoLinkWheelModel.ChildBean> childBeanArrayList2 = new ArrayList<>();
        TwoLinkWheelModel.ChildBean childBean21 = new TwoLinkWheelModel.ChildBean("日用品");
        TwoLinkWheelModel.ChildBean childBean22 = new TwoLinkWheelModel.ChildBean("医疗器械");
        TwoLinkWheelModel.ChildBean childBean23 = new TwoLinkWheelModel.ChildBean("保健品");
        childBeanArrayList2.add(childBean21);
        childBeanArrayList2.add(childBean22);
        childBeanArrayList2.add(childBean23);
        twoLinkWheelModel2.setChildBean(childBeanArrayList2);

        TwoLinkWheelModel twoLinkWheelModel3 = new TwoLinkWheelModel("违规商品");
        ArrayList<TwoLinkWheelModel.ChildBean> childBeanArrayList3 = new ArrayList<>();
        TwoLinkWheelModel.ChildBean childBean31 = new TwoLinkWheelModel.ChildBean("涉毒");
        TwoLinkWheelModel.ChildBean childBean32 = new TwoLinkWheelModel.ChildBean("假货");
        TwoLinkWheelModel.ChildBean childBean33 = new TwoLinkWheelModel.ChildBean("涉黄");
        TwoLinkWheelModel.ChildBean childBean34 = new TwoLinkWheelModel.ChildBean("涉暴");
        TwoLinkWheelModel.ChildBean childBean35 = new TwoLinkWheelModel.ChildBean("涉政");
        TwoLinkWheelModel.ChildBean childBean36 = new TwoLinkWheelModel.ChildBean("野生动物");
        TwoLinkWheelModel.ChildBean childBean37 = new TwoLinkWheelModel.ChildBean("香烟/电子烟");
        TwoLinkWheelModel.ChildBean childBean38 = new TwoLinkWheelModel.ChildBean("易燃易爆品");
        childBeanArrayList3.add(childBean31);
        childBeanArrayList3.add(childBean32);
        childBeanArrayList3.add(childBean33);
        childBeanArrayList3.add(childBean34);
        childBeanArrayList3.add(childBean35);
        childBeanArrayList3.add(childBean36);
        childBeanArrayList3.add(childBean37);
        childBeanArrayList3.add(childBean38);
        twoLinkWheelModel3.setChildBean(childBeanArrayList3);

        TwoLinkWheelModel twoLinkWheelModel4 = new TwoLinkWheelModel("品牌侵权");
        ArrayList<TwoLinkWheelModel.ChildBean> childBeanArrayList4 = new ArrayList<>();
        TwoLinkWheelModel.ChildBean childBean41 = new TwoLinkWheelModel.ChildBean("LOGO图侵权");
        TwoLinkWheelModel.ChildBean childBean42 = new TwoLinkWheelModel.ChildBean("店铺名称侵权");
        childBeanArrayList4.add(childBean41);
        childBeanArrayList4.add(childBean42);
        twoLinkWheelModel4.setChildBean(childBeanArrayList4);

        TwoLinkWheelModel twoLinkWheelModel5 = new TwoLinkWheelModel("虚假门店");
        ArrayList<TwoLinkWheelModel.ChildBean> childBeanArrayList5 = new ArrayList<>();
        TwoLinkWheelModel.ChildBean childBean51 = new TwoLinkWheelModel.ChildBean("无实体店");
        TwoLinkWheelModel.ChildBean childBean52 = new TwoLinkWheelModel.ChildBean("地址不一致，商家倒闭转让");
        childBeanArrayList5.add(childBean51);
        childBeanArrayList5.add(childBean52);
        twoLinkWheelModel5.setChildBean(childBeanArrayList5);

        twoLinkWheelModels.add(twoLinkWheelModel1);
        twoLinkWheelModels.add(twoLinkWheelModel2);
        twoLinkWheelModels.add(twoLinkWheelModel3);
        twoLinkWheelModels.add(twoLinkWheelModel4);
        twoLinkWheelModels.add(twoLinkWheelModel5);

        hideWaitDialog();
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.ll_complaint_type:
                onComplaintType();
                break;
            case R.id.tv_submit://提交
                onSubmit();
                break;
        }
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String mTag, String accessoryId, String url, int width, int height) {
        switch (mTag) {
            case TAG_SELECT_PIC_01://banner 1号位
                selectPicSuccess(accessoryId, url, width, height);
                break;
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }

    private void selectPicSuccess(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            FreeBackPicModel freeBackPic = new FreeBackPicModel(accessoryId, url, PIC_NOR);

            freeBackPicAdapter.addData(freeBackPicAdapter.getData().size() - 1, freeBackPic);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //--------------------------选择图片----------------------------------------------------
    //投诉类型
    private void onComplaintType() {
        if (twoLinkWheelUtils == null) {
            twoLinkWheelUtils = new TwoLinkWheelUtils();
        }

        twoLinkWheelUtils.showCityWheel(ComplaintStoreActivity.this, new TwoLinkWheelUtils.ITwoLinkWheelUtils<TwoLinkWheelModel, TwoLinkWheelModel.ChildBean>() {
            @Override
            public void onCity(TwoLinkWheelModel provinceData) {
                getWv2Data(provinceData.getChildBean());
            }

            @Override
            public void onchooseCity(int provincePos, TwoLinkWheelModel provinceItemData,
                                     int cityPos, TwoLinkWheelModel.ChildBean cityItemData) {
                text01 = provinceItemData.getText();
                text02 = cityItemData.getText();
                mTvComplaintType.setText(provinceItemData.getText() + "--" + cityItemData.getText());
            }
        });
        twoLinkWheelUtils.setWv01Data(twoLinkWheelModels);
        getWv2Data(twoLinkWheelModels.get(0).getChildBean());
    }

    private void getWv2Data(List<TwoLinkWheelModel.ChildBean> childBean) {
        twoLinkWheelUtils.setWv02Data(childBean);
    }

    //提交
    private void onSubmit() {
        if (EmptyUtils.isEmpty(mTvComplaintType.getText().toString())) {
            showShortToast("请选择投诉类型");
            return;
        }
        if (EmptyUtils.isEmpty(mEtPhone.getText().toString())) {
            showShortToast("请输入联系电话");
            return;
        }
        if (!"1".equals(String.valueOf(mEtPhone.getText().charAt(0))) || mEtPhone.getText().length() != 11) {
            showShortToast("请输入正确的电话号码");
            return;
        }
        if (EmptyUtils.isEmpty(mEtContent.getText().toString())) {
            showShortToast("请输入举报内容");
            return;
        }
        //下单状态(0 未下单 1已下单）
        String entry_mode = "1";
        if (mSrbGroup.getCheckedRadioButtonId() == R.id.srb_no_order) {
            entry_mode = "0";
        }

        String ComplaintAccessoryId = "";
        if (freeBackPicAdapter.getData().size() == 2) {
            ComplaintAccessoryId = freeBackPicAdapter.getData().get(0).getId();
        }

        showWaitDialog();
        BaseModel.sendAddComplaintRequest(TAG, mTvComplaintType.getText().toString(), entry_mode, mEtPhone.getText().toString(),
                mEtContent.getText().toString(), ComplaintAccessoryId, fixmedins_code, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast(returnData.getMsg());
                        goFinish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }
        if (twoLinkWheelUtils != null) {
            twoLinkWheelUtils.onCleanData();
        }
    }

    public static void newIntance(Context context, String fixmedins_code) {
        Intent intent = new Intent(context, ComplaintStoreActivity.class);
        intent.putExtra("fixmedins_code", fixmedins_code);
        context.startActivity(intent);
    }
}
