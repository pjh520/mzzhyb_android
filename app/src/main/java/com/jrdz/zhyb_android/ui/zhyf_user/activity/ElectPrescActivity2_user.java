package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.viewPage.BaseViewPagerAndTabsAdapter_new;
import com.frame.compiler.widget.CustomViewPager;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.ElectPrescFragment_image;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.ElectPrescFragment_video;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ElectPrescModel2;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：电子处方
 * ================================================
 */
public class ElectPrescActivity2_user extends BaseActivity {
    private LinearLayout mRlTitle;
    private FrameLayout mFlClose;
    private SlidingTabLayout mStbTab;
    private CustomViewPager mVp;

    protected String orderNo;
    ArrayList<String> titles = new ArrayList<>();
    protected ElectPrescModel2.DataBean pagerData;

    @Override
    public int getLayoutId() {
        return R.layout.activity_elect_presc2;
    }

    @Override
    public void initView() {
        super.initView();

        mRlTitle = findViewById(R.id.rl_title);
        mFlClose = findViewById(R.id.fl_close);
        mStbTab = findViewById(R.id.stb_tab);
        mVp = findViewById(R.id.vp);
    }

    @Override
    protected void initImmersionBar() {
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mRlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        orderNo = getIntent().getStringExtra("orderNo");
        super.initData();

        showWaitDialog();
        getPageData();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mFlClose.setOnClickListener(this);
    }

    //获取页面数据  "10002"
    public void getPageData() {
        ElectPrescModel2.sendElectPrescModelRequest_user(TAG, orderNo, new CustomerJsonCallBack<ElectPrescModel2>() {
            @Override
            public void onRequestError(ElectPrescModel2 returnData, String msg) {
                hideWaitDialog();
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(ElectPrescModel2 returnData) {
                hideWaitDialog();
                pagerData = returnData.getData();
                if (pagerData != null) {
                    initTabLayout(pagerData.getElectronicPrescriptions());
                }else {
                    showShortToast("数据有误,请重新进入页面");
                }
            }
        });
    }


    //初始化tablayout 跟 viewpager
    protected void initTabLayout(ElectPrescModel2.DataBean.ElectronicPrescriptionsBean electronicPrescriptions) {
        if (electronicPrescriptions==null){
            showShortToast("数据有误,请重新进入页面");
            return;
        }
        BaseViewPagerAndTabsAdapter_new baseViewPagerAndTabsAdapter_new = new BaseViewPagerAndTabsAdapter_new(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0://图文
                        return ElectPrescFragment_image.newIntance(EmptyUtils.strEmpty(electronicPrescriptions.getAccessoryUrl()));
                    case 1://视频
                        return ElectPrescFragment_video.newIntance(EmptyUtils.strEmptyToText(electronicPrescriptions.getVideo(),EmptyUtils.strEmpty(electronicPrescriptions.getRecord())));
                    default:
                        return ElectPrescFragment_image.newIntance(EmptyUtils.strEmpty(electronicPrescriptions.getAccessoryUrl()));
                }
            }
        };

        titles.clear();
        if (!EmptyUtils.isEmpty(electronicPrescriptions.getAccessoryUrl())) {
            titles.add("图文");
        }
        if (!EmptyUtils.isEmpty(electronicPrescriptions.getVideo())||!EmptyUtils.isEmpty(electronicPrescriptions.getRecord())) {
            titles.add("视频");
        }
        baseViewPagerAndTabsAdapter_new.setData(titles);
        mVp.setAdapter(baseViewPagerAndTabsAdapter_new);
        mStbTab.setViewPager(mVp);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()) {
            case R.id.fl_close:
                goFinish();
                break;
        }
    }

    public static void newIntance(Context context, String orderNo) {
        Intent intent = new Intent(context, ElectPrescActivity2_user.class);
        intent.putExtra("orderNo", orderNo);
        context.startActivity(intent);
    }
}
