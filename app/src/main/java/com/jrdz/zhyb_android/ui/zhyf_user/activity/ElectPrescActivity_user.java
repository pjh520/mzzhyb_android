package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.view.ShapeView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.ElectPrescActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.ElectPrescModel;

import java.math.BigDecimal;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：电子处方
 * ================================================
 */
public class ElectPrescActivity_user extends ElectPrescActivity {

    //获取页面数据
    @Override
    public void getPageData() {
        // 2022-10-13 模拟获取电子处方的数据
        ElectPrescModel.sendElectPrescModelRequest_user(TAG, orderNo, new CustomerJsonCallBack<ElectPrescModel>() {
            @Override
            public void onRequestError(ElectPrescModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(ElectPrescModel returnData) {
                hideWaitDialog();
                setPageData(returnData.getData());
            }
        });
    }

    public static void newIntance(Context context, String orderNo) {
        Intent intent = new Intent(context, ElectPrescActivity_user.class);
        intent.putExtra("orderNo", orderNo);
        context.startActivity(intent);
    }
}
