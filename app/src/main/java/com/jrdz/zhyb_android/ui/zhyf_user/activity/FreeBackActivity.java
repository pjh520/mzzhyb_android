package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeRadioGroup;
import com.hjq.shape.view.ShapeEditText;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.FreeBackPicAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.model.FreeBackPicModel;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-02
 * 描    述：意见反馈页面
 * ================================================
 */
public class FreeBackActivity extends BaseActivity implements CompressUploadSinglePicUtils_zhyf.IChoosePic {
    public final static String TAG_SELECT_PIC_01 = "1";//意见反馈图片

    public static final int PIC_NOR = 1;//新增图片
    public static final int PIC_ADDBTN = 2;//添加按钮

    private ShapeRadioGroup mSrbGroup;
    private AppCompatRadioButton mSrbFunctExcep;
    private AppCompatRadioButton mSrbOtherExcep;
    private ShapeEditText mEtContent;
    private CustomeRecyclerView mCrlImglist;
    private ShapeEditText mEtPhone;
    private ShapeTextView mTvSubmit;

    String funct_excep_text = "<font color=\"#333333\">功能异常</font><font color=\"#808080\">（不能使用现有功能）</font>";
    String other_excep_text = "<font color=\"#333333\">其他问题</font><font color=\"#808080\">（用的不爽，意见建议提过来吧）</font>";
    private FreeBackPicAdapter freeBackPicAdapter;
    private CompressUploadSinglePicUtils_zhyf compressUploadSinglePicUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_freeback;
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mTitleBar).keyboardEnable(true);
        mImmersionBar.init();
    }

    @Override
    public void initView() {
        super.initView();
        mCrlImglist = findViewById(R.id.crl_imglist);
        mTvSubmit = findViewById(R.id.tv_submit);
    }

    @Override
    public void initData() {
        super.initData();
        //初始化获取图片
        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);

        //设置图片列表
        mCrlImglist.setHasFixedSize(true);
        mCrlImglist.setLayoutManager(new GridLayoutManager(this, 4, RecyclerView.VERTICAL, false));
        freeBackPicAdapter = new FreeBackPicAdapter();
        freeBackPicAdapter.setHeaderFooterEmpty(true, true);
        initHeadView();
        initFootView();
        mCrlImglist.setAdapter(freeBackPicAdapter);

        //添加图片增加按钮数据
        ArrayList<FreeBackPicModel> freeBackPicModels = new ArrayList<>();
        FreeBackPicModel freeBackPic_addbtn = new FreeBackPicModel("-1", "", PIC_ADDBTN);
        freeBackPicModels.add(freeBackPic_addbtn);

        freeBackPicAdapter.setNewData(freeBackPicModels);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        //分类 item点击事件
        freeBackPicAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                FreeBackPicModel itemData = freeBackPicAdapter.getItem(i);
                if (PIC_NOR == itemData.getItemType()) {//查看图片
                    ImageView ivPhoto = view.findViewById(R.id.iv_photo);
                    OpenImage.with(FreeBackActivity.this)
                            //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                            .setClickImageView(ivPhoto)
                            //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                            .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                            //RecyclerView的数据
                            .setImageUrl(itemData.getImg(), MediaType.IMAGE)
                            //点击的ImageView所在数据的位置
                            .setClickPosition(0)
                            //开始展示大图
                            .show();
                } else if (PIC_ADDBTN == itemData.getItemType()) {//新增按钮
                    KeyboardUtils.hideSoftInput(FreeBackActivity.this);
                    compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_01, CompressUploadSinglePicUtils_zhyf.PIC_FREEBACK_TAG);
                }
            }
        });

        freeBackPicAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }

                switch (view.getId()) {
                    case R.id.iv_delete://删除图片
                        freeBackPicAdapter.getData().remove(i);
                        freeBackPicAdapter.notifyDataSetChanged();
                        break;
                }
            }
        });
        mTvSubmit.setOnClickListener(this);
    }

    //添加头部view
    private void initHeadView() {
        View headView = LayoutInflater.from(this).inflate(R.layout.layout_freeback_head, mCrlImglist, false);
        mSrbGroup = headView.findViewById(R.id.srb_group);
        mSrbFunctExcep = headView.findViewById(R.id.srb_funct_excep);
        mSrbOtherExcep = headView.findViewById(R.id.srb_other_excep);
        mEtContent = headView.findViewById(R.id.et_content);

        mSrbFunctExcep.setText(Html.fromHtml(funct_excep_text));
        mSrbOtherExcep.setText(Html.fromHtml(other_excep_text));

        freeBackPicAdapter.addHeaderView(headView);
    }

    //添加底部view
    private void initFootView() {
        View footView = LayoutInflater.from(this).inflate(R.layout.layout_freeback_foot, mCrlImglist, false);
        mEtPhone = footView.findViewById(R.id.et_phone);

        freeBackPicAdapter.addFooterView(footView);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_submit://提交
                onSubmit();
                break;
        }
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String mTag, String accessoryId, String url, int width, int height) {
        switch (mTag) {
            case TAG_SELECT_PIC_01://banner 1号位
                selectPicSuccess(accessoryId, url, width, height);
                break;
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }

    private void selectPicSuccess(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            FreeBackPicModel freeBackPic = new FreeBackPicModel(accessoryId, url, PIC_NOR);
            freeBackPicAdapter.addData(0, freeBackPic);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //--------------------------选择图片----------------------------------------------------

    //提交
    private void onSubmit() {
        if (EmptyUtils.isEmpty(mEtContent.getText().toString())){
            showShortToast("请输入问题和意见描述");
            return;
        }

        if (EmptyUtils.isEmpty(mEtPhone.getText().toString())||mEtPhone.getText().toString().length()<11){
            showShortToast("请输入正确的手机号码");
            return;
        }

        String picUrl = "";
        for (FreeBackPicModel datum : freeBackPicAdapter.getData()) {
            if (PIC_NOR == datum.getItemType()) {
                picUrl += datum.getId() + "∞#";
            }
        }

        if (!EmptyUtils.isEmpty(picUrl)) {
            picUrl = picUrl.substring(0, picUrl.length() - 2);
        }

        //问题类型
        String entry_mode = "1";
        if (mSrbGroup.getCheckedRadioButtonId() == R.id.srb_other_excep) {
            entry_mode = "2";
        }

        showWaitDialog();
        BaseModel.sendAddFeedbackRequest(TAG, entry_mode, mEtContent.getText().toString(),
                mEtPhone.getText().toString(), picUrl, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("提交成功");
                        goFinish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (compressUploadSinglePicUtils!=null){
            compressUploadSinglePicUtils.onDeatoryUtils();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, FreeBackActivity.class);
        context.startActivity(intent);
    }
}
