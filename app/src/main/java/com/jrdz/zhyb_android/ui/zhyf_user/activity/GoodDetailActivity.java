package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.beans.OpenImageUrl;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.flyjingfish.openimagelib.listener.SourceImageViewGet;
import com.flyjingfish.openimagelib.listener.SourceImageViewIdGet;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.banner.listener.OnBannerClickListener;
import com.frame.compiler.widget.banner.manager.GlideAppSimpleImageManager;
import com.frame.compiler.widget.banner.widget.BannerAdapter;
import com.frame.compiler.widget.banner.widget.BannerLayout;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.tabLayout.CommonTabLayout;
import com.frame.compiler.widget.tabLayout.listener.CustomTabEntity;
import com.frame.compiler.widget.tabLayout.listener.OnTabSelectListener;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeRelativeLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.NewsDetailActivity;
import com.jrdz.zhyb_android.ui.home.model.BannerDataModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.ProductDescribeAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.SimilarProductAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.model.GoodDetailModel_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.GoodsBannerModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ProductDescribeModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarRefreshModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopDetailMoreModel;
import com.jrdz.zhyb_android.utils.AppManager_GoodsDetailsAcivity;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginSmrzUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.ParcelUtils;
import com.jrdz.zhyb_android.widget.ShapeCustomeRecyclerView;
import com.jrdz.zhyb_android.widget.ShoppingCartAnimationView;
import com.jrdz.zhyb_android.widget.pop.DrugManualPop;
import com.jrdz.zhyb_android.widget.pop.ServicePromisePop;
import com.jrdz.zhyb_android.widget.pop.ShopCarPop_user;
import com.jrdz.zhyb_android.widget.pop.ShopDetailMorePop;
import com.zhy.http.okhttp.OkHttpUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import cody.bus.ObserverWrapper;
import razerdp.basepopup.BasePopupWindow;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/11/6
 * 描    述：商品详情页
 * ================================================
 */
public class GoodDetailActivity extends BaseActivity implements OnBannerClickListener {
    private RelativeLayout mRlTitle;
    private FrameLayout mFlClose;
    private CommonTabLayout mStbTab;
    private FrameLayout mFlCollect;
    private FrameLayout mFlMore;
    private ImageView mIvCollect,mIvMore;
    private NestedScrollView mScrollView;
    private BannerLayout mBanner;
    private TextView mTvPrice;
    private TextView mTvHasNum;
    private TextView mTvSaleNum;
    private TextView mTvTitle,mTvDescribe;
    private ShapeTextView mStvProductDescribe;
    private ShapeCustomeRecyclerView mSrlProductDescribe;
    private ShapeRelativeLayout srlServicePromise;
    private TextView mTvDrugType;
    private ImageView mIvEnter;
    private ImageView mIvPic;
    private TextView mTvShopName;
    private ImageView mIvLocation;
    private TextView mTvDistance;
    private TextView mTvShopSaleNum;
    private ShapeTextView mStvGoShop;
    private ShapeCustomeRecyclerView mScrvSimilar;
    private LinearLayout mLlContain;
    private ShapeTextView mStvShopDetailTag;
    private View mLine;
    private LinearLayout mLlShop;
    private LinearLayout mLlContactMerchants;
    private ShapeTextView mSvRedPoint;
    private FrameLayout mFlShopcarIcon;
    private LinearLayout mLlShopcar;
    private TextView mTvAddShopcar;
    private ShapeTextView mTvApplyBuy;
    private FrameLayout flItem;

    private ArrayList<CustomTabEntity> goodDetailTabDatas;//tab 数据
    private SimilarProductAdapter similarProductAdapter;//相似列表适配器
    private ProductDescribeAdapter productDescribeAdapter;//产品说明适配器
    private int requestTag = 0;

    private ShopDetailMorePop goodDetailMorePop;//更多弹框
    private DrugManualPop drugManualPop;//药品说明书弹框
    private ServicePromisePop servicePromisePop;//服务承诺弹框
    //判读是否是scrollview主动引起的滑动，true-是，false-否，由tablayout引起的
    private boolean isScroll;
    //记录上一次位置，防止在同一内容块里滑动 重复定位到tablayout
    private int lastPos = 0;

    private ViewGroup mDecorView;//添加小红点的父view
    ArrayList<GoodsModel.DataBean> hasJoinedShopCarDatas = new ArrayList<>();//记录加入购物车的数据
    private ShopCarPop_user shopCarPop;//购物车弹框
    private String totalRedNum = "0";//选择的商品总数量
    private String totalPrice = "0";//选择的商品总价
    private CustomerDialogUtils customerDialogUtils;

    private String goodsNo,fixmedins_code,goodsName;
    List<GoodsBannerModel> bannerDatas = new ArrayList<>();//存储轮播图数据
    ArrayList<ProductDescribeModel> productDescribeModels = new ArrayList<>();//产品说明数据
    ArrayList<String> goodPics = new ArrayList<>();//商品详情图片
    private GoodDetailModel_user.DataBean pagerData;//记录当前商品的数据
    private GoodsModel.DataBean addGoodsData;//记录需要添加进购物车的数据
    private InsuredLoginSmrzUtils insuredLoginSmrzUtils;//判断用户是否登录以及实名注册
    private int bgRadius;

    //登录成功信息
    private ObserverWrapper<Integer> mLoginObserve = new ObserverWrapper<Integer>() {
        @Override
        public void onChanged(@Nullable Integer value) {
            switch (value) {
                case 0://退出登录
                    totalRedNum = "0";
                    totalPrice = "0";
                    mSvRedPoint.setVisibility(View.GONE);
                    hasJoinedShopCarDatas.clear();
                    break;
                case 1://登录成功
                    if (pagerData==null){
                        showShortToast("数据有误，请重新进入页面");
                        return;
                    }
                    getShopCarData(pagerData.getFixmedins_code());
                    break;
            }
        }
    };
    //用户下单 需要通知商品详情的购物车刷新 传值为2 1代表在商品详情里面操作的时候 通知外部放购物车刷新
    private ObserverWrapper<ShopCarRefreshModel> mShopCarObserver=new ObserverWrapper<ShopCarRefreshModel>() {
        @Override
        public void onChanged(@Nullable ShopCarRefreshModel value) {
            switch (value.getTag_value()){
                case "2":
                    if (!EmptyUtils.isEmpty(TAG)&&!TAG.equals(value.getTag())){
                        if (pagerData==null){
                            return;
                        }

                        showWaitDialog();
                        getPagerData(Constants.AppStorage.APP_LATITUDE, Constants.AppStorage.APP_LONGITUDE);
                    }
                    break;
            }
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.activity_good_detail;
    }

    @Override
    public void initView() {
        super.initView();
        mRlTitle = findViewById(R.id.rl_title);
        mFlClose = findViewById(R.id.fl_close);
        mStbTab = findViewById(R.id.stb_tab);
        mFlCollect = findViewById(R.id.fl_collect);
        mIvCollect = findViewById(R.id.iv_collect);
        mFlMore = findViewById(R.id.fl_more);
        mIvMore = findViewById(R.id.iv_more);

        mScrollView = findViewById(R.id.scroll_view);
        mBanner = findViewById(R.id.banner);
        mTvPrice = findViewById(R.id.tv_price);
        mTvHasNum = findViewById(R.id.tv_has_num);
        mTvSaleNum = findViewById(R.id.tv_sale_num);
        mTvTitle = findViewById(R.id.tv_title);
        mTvDescribe= findViewById(R.id.tv_describe);
        mStvProductDescribe = findViewById(R.id.stv_product_describe);
        mSrlProductDescribe = findViewById(R.id.srl_product_describe);
        srlServicePromise = findViewById(R.id.srl_service_promise);
        mTvDrugType = findViewById(R.id.tv_drug_type);
        mIvEnter = findViewById(R.id.iv_enter);
        mIvPic = findViewById(R.id.iv_pic);
        mTvShopName = findViewById(R.id.tv_shop_name);
        mIvLocation = findViewById(R.id.iv_location);
        mTvDistance = findViewById(R.id.tv_distance);
        mTvShopSaleNum = findViewById(R.id.tv_shop_sale_num);
        mStvGoShop = findViewById(R.id.stv_go_shop);
        mScrvSimilar = findViewById(R.id.scrv_similar);
        mLlContain = findViewById(R.id.ll_contain);
        mStvShopDetailTag = findViewById(R.id.stv_shop_detail_tag);

        mLine = findViewById(R.id.line);
        mLlShop = findViewById(R.id.ll_shop);
        mLlContactMerchants = findViewById(R.id.ll_contact_merchants);
        mSvRedPoint = findViewById(R.id.sv_red_point);
        mFlShopcarIcon = findViewById(R.id.fl_shopcar_icon);
        mLlShopcar = findViewById(R.id.ll_shopcar);
        mTvAddShopcar = findViewById(R.id.tv_add_shopcar);
        mTvApplyBuy = findViewById(R.id.tv_apply_buy);

        mDecorView = (ViewGroup) getWindow().getDecorView();
    }

    @Override
    protected void initImmersionBar() {
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mRlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        goodsNo = getIntent().getStringExtra("goodsNo");
        fixmedins_code = getIntent().getStringExtra("fixmedins_code");
        goodsName = getIntent().getStringExtra("goodsName");
        super.initData();
        MsgBus.sendInsuredLoginStatus().observe(this, mLoginObserve);
        MsgBus.sendShopCarRefresh().observe(this, mShopCarObserver);
        bgRadius = getResources().getDimensionPixelSize(R.dimen.dp_39);
        //初始化tablayout
        goodDetailTabDatas = CommonlyUsedDataUtils.getInstance().getGoodDetailTabData();
        mStbTab.setTabData(goodDetailTabDatas);

        //初始化产品说明
        mSrlProductDescribe.setHasFixedSize(true);
        mSrlProductDescribe.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        productDescribeAdapter = new ProductDescribeAdapter();
        mSrlProductDescribe.setAdapter(productDescribeAdapter);
        //添加底部按钮 弹出药品说明书
        View footView = LayoutInflater.from(this).inflate(R.layout.layout_productdescribe_footview, mSrlProductDescribe, false);
        flItem=footView.findViewById(R.id.fl_item);
        productDescribeAdapter.addFooterView(footView, -1, LinearLayout.HORIZONTAL);
        //item点击事件
        productDescribeAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                onDrugManualPop();
            }
        });

        //相似列表
        mScrvSimilar.setHasFixedSize(true);
        mScrvSimilar.setLayoutManager(new GridLayoutManager(this, 2, RecyclerView.VERTICAL,false));
        similarProductAdapter = new SimilarProductAdapter();
        mScrvSimilar.setAdapter(similarProductAdapter);
        //添加底部站位view
//        View footView02 = LayoutInflater.from(this).inflate(R.layout.layout_similar_footview, mScrvSimilar, false);
//        similarProductAdapter.addFooterView(footView02, -1, LinearLayout.HORIZONTAL);

//        showWaitDialog();
        addTrack();
        getPagerData(Constants.AppStorage.APP_LATITUDE, Constants.AppStorage.APP_LONGITUDE);
        getShopCarData(fixmedins_code);
        getSimilarProductData(fixmedins_code, goodsName);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mStbTab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                //点击标签，使scrollview滑动，isScroll置false
                isScroll = false;
                int pos = mStbTab.getCurrentTab();
                int top = 0;
                switch (pos) {
                    case 0:
                        top = mBanner.getTop();
                        break;
                    case 1:
                        top = mStvShopDetailTag.getTop();
                        break;
                }

                mScrollView.fling(0);
                mScrollView.smoothScrollTo(0, top);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });

        mScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //当滑动由scrollview触发时，isScroll 置true
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    isScroll = true;
                }
                return false;
            }
        });

        mScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (isScroll) {
                    if (scrollY > mStvShopDetailTag.getTop() - 10) {
                        setScrollPos(1);
                        return;
                    }

                    if (scrollY > mBanner.getTop() - 10) {
                        setScrollPos(0);
                        return;
                    }
                }
            }
        });

        similarProductAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }

                GoodsModel.DataBean itemData = similarProductAdapter.getItem(i);
                GoodDetailActivity.newIntance(GoodDetailActivity.this,itemData.getGoodsNo(),itemData.getFixmedins_code(),itemData.getGoodsName());
            }
        });
        similarProductAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }

                if (insuredLoginSmrzUtils == null) {
                    insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
                }
                if (!insuredLoginSmrzUtils.isLoginSmrz(GoodDetailActivity.this)) {
                    return;
                }

                GoodsModel.DataBean itemData = similarProductAdapter.getItem(i);
                TextView tvSelectNum = (TextView) baseQuickAdapter.getViewByPosition(mScrvSimilar, i, R.id.tv_select_num);
                switch (view.getId()) {
                    case R.id.fl_num_add://数量加
                        onAddShopcar(itemData, baseQuickAdapter, view, i, tvSelectNum, "add");
                        break;
                    case R.id.fl_num_reduce://数量减
                        onAddShopcar(itemData, baseQuickAdapter, view, i, tvSelectNum, "reduce");
                        break;
                }
            }
        });

        mFlClose.setOnClickListener(this);
        mFlCollect.setOnClickListener(this);
        mFlMore.setOnClickListener(this);
        mStvProductDescribe.setOnClickListener(this);
        flItem.setOnClickListener(this);
        srlServicePromise.setOnClickListener(this);
        mStvGoShop.setOnClickListener(this);
        mLlShop.setOnClickListener(this);
        mLlContactMerchants.setOnClickListener(this);
        mLlShopcar.setOnClickListener(this);
        mTvAddShopcar.setOnClickListener(this);
        mTvApplyBuy.setOnClickListener(this);
    }

    //添加足迹
    private void addTrack() {
        BaseModel.sendAddTrackRequest(TAG, goodsNo, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {}
        });
    }

    //获取页面数据
    private void getPagerData(double latitude, double longitude) {
        GoodDetailModel_user.sendGoodDetailRequest_user(TAG+goodsNo, goodsNo, String.valueOf(longitude), String.valueOf(latitude), new CustomerJsonCallBack<GoodDetailModel_user>() {
            @Override
            public void onRequestError(GoodDetailModel_user returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GoodDetailModel_user returnData) {
                dissWaitDailog();
                if (isFinishing())return;
                GoodDetailModel_user.DataBean data = returnData.getData();
                if (data != null) {
                    setPagerData(data);
                }
            }
        });
    }

    //设置页面数据
    private void setPagerData(GoodDetailModel_user.DataBean data) {
        pagerData = data;
        //设置收藏状态
        if ("1".equals(pagerData.getIsCollection())){//已收藏
            mIvCollect.setImageResource(R.drawable.ic_gd_collevt_pre);
        }else {//未收藏
            mIvCollect.setImageResource(R.drawable.ic_gd_collevt_nor);
        }
        //获取轮播图数据
        setBannerData(data);
        //设置页面数据
        mTvPrice.setText("1".equals(data.getIsPromotion())?EmptyUtils.strEmpty(data.getPreferentialPrice()):EmptyUtils.strEmpty(data.getPrice()));
        if ("1".equals(data.getIsShowMargin())&&data.getInventoryQuantity() <= 5) {
            mTvHasNum.setVisibility(View.VISIBLE);
            mTvHasNum.setText("仅剩"+data.getInventoryQuantity()+"件");
        } else {
            mTvHasNum.setVisibility(View.GONE);
        }

        //是否展示商品的月售数据
        if ("1".equals(data.getIsShowGoodsSold())){
            mTvSaleNum.setVisibility(View.VISIBLE);
            mTvSaleNum.setText("月售"+data.getMonthlySales());
        }else {
            mTvSaleNum.setVisibility(View.GONE);
        }

        mTvTitle.setText(EmptyUtils.strEmpty(data.getGoodsName()));
        if (EmptyUtils.isEmpty(data.getItemType())||"0".equals(data.getItemType())){
            mTvDrugType.setText("");
            mTvDescribe.setVisibility(View.GONE);
        }else {
            //处方药有此提醒（1、OTC 2、处方药）
            if ("2".equals(data.getItemType())){
                mTvDescribe.setVisibility(View.VISIBLE);
            }else {
                mTvDescribe.setVisibility(View.GONE);
            }
            mTvDrugType.setTag(data.getItemType());
            mTvDrugType.setText("1".equals(data.getItemType()) ? "非处方药（OTC）" : "处方药");
        }

        //产品说明
        setGoodsDescribe(data);
        //设置店铺信息
        GlideUtils.loadImg(data.getStoreAccessoryUrl(), mIvPic, com.frame.compiler.R.drawable.ic_placeholder_bg,
                new RoundedCornersTransformation((int) getResources().getDimension(com.frame.compiler.R.dimen.dp_10), 0));
        mTvShopName.setText(EmptyUtils.strEmpty(data.getStoreName()));
        mTvDistance.setText(data.getDistance() + "km");
        //是否展示店铺的月售数据
        if ("1".equals(data.getIsShowSold())){
            mTvShopSaleNum.setVisibility(View.VISIBLE);
            mTvShopSaleNum.setText("月售" + data.getMonthlyStoreSales());
        }else {
            mTvShopSaleNum.setVisibility(View.GONE);
        }

        //模拟商品详情图片
        goodPics.clear();
        mLlContain.removeAllViews();
        if (!EmptyUtils.isEmpty(data.getDetailAccessoryUrl1())) {
            goodPics.add(data.getDetailAccessoryUrl1());
        }
        if (!EmptyUtils.isEmpty(data.getDetailAccessoryUrl2())) {
            goodPics.add(data.getDetailAccessoryUrl2());
        }
        if (!EmptyUtils.isEmpty(data.getDetailAccessoryUrl3())) {
            goodPics.add(data.getDetailAccessoryUrl3());
        }
        if (!EmptyUtils.isEmpty(data.getDetailAccessoryUrl4())) {
            goodPics.add(data.getDetailAccessoryUrl4());
        }
        if (!EmptyUtils.isEmpty(data.getDetailAccessoryUrl5())) {
            goodPics.add(data.getDetailAccessoryUrl5());
        }
        //新增商品详情view
        for (String goodPic : goodPics) {
            View view = LayoutInflater.from(this).inflate(R.layout.layout_good_detail_img_item, mLlContain, false);
            ImageView ivImg = view.findViewById(R.id.iv_img);
            SubsamplingScaleImageView ivLongImg = view.findViewById(R.id.iv_long_img);
            GlideUtils.loadLongImg(this, goodPic, ivImg, ivLongImg);

            ivImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenImage.with(GoodDetailActivity.this)
                            //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                            .setClickImageView(ivImg)
                            //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                            .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                            //RecyclerView的数据
                            .setImageUrl(goodPic, MediaType.IMAGE)
                            //点击的ImageView所在数据的位置
                            .setClickPosition(0)
                            //开始展示大图
                            .show();
                }
            });
            ivLongImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenImage.with(GoodDetailActivity.this)
                            //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                            .setNoneClickView()
                            //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                            .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                            //RecyclerView的数据
                            .setImageUrl(goodPic, MediaType.IMAGE)
                            //点击的ImageView所在数据的位置
                            .setClickPosition(0)
                            //开始展示大图
                            .show();
                }
            });
            mLlContain.addView(view);
        }
    }

    //设置产品说明 数据
    private void setGoodsDescribe(GoodDetailModel_user.DataBean data) {
        productDescribeModels.clear();
        //产品说明的数据
        switch (data.getDrugsClassification()) {
            case "1"://西药中成药
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_main_func, "功能主治", EmptyUtils.strEmpty(data.getEfccAtd())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_usage, "常见用法", EmptyUtils.strEmpty(data.getUsualWay())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_validity, "有效期", data.getExpiryDateCount() + "个月"));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_storage, "贮存条件", EmptyUtils.strEmpty(data.getStorageConditions())));
                break;
            case "2"://医疗器械
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_applicablescope, "适用范围", EmptyUtils.strEmpty(data.getApplicableScope())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_usagedosage, "用法用量", EmptyUtils.strEmpty(data.getUsageDosage())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_validity, "有效期", data.getExpiryDateCount() + "个月"));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_packaging, "包装", EmptyUtils.strEmpty(data.getPackaging())));
                break;
            case "3"://成人用品
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_usagemethod, "使用方法", EmptyUtils.strEmpty(data.getUsagemethod())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_storage, "贮存条件", EmptyUtils.strEmpty(data.getStorageConditions())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_validity, "有效期", data.getExpiryDateCount() + "个月"));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_packaging, "包装", EmptyUtils.strEmpty(data.getPackaging())));
                break;
            case "4"://个人用品
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_usagedosage, "用法用量", EmptyUtils.strEmpty(data.getUsageDosage())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_storage, "贮存条件", EmptyUtils.strEmpty(data.getStorageConditions())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_validity, "有效期", data.getExpiryDateCount() + "个月"));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_packaging, "包装", EmptyUtils.strEmpty(data.getPackaging())));
                break;
        }
        productDescribeAdapter.setNewData(productDescribeModels);
    }

    //获取轮播图数据
    private void setBannerData(GoodDetailModel_user.DataBean data) {
        bannerDatas.clear();
        if (!EmptyUtils.isEmpty(data.getBannerAccessoryUrl1())) {
            bannerDatas.add(new GoodsBannerModel(data.getBannerAccessoryUrl1()));
        }
        if (!EmptyUtils.isEmpty(data.getBannerAccessoryUrl2())) {
            bannerDatas.add(new GoodsBannerModel(data.getBannerAccessoryUrl2()));
        }
        if (!EmptyUtils.isEmpty(data.getBannerAccessoryUrl3())) {
            bannerDatas.add(new GoodsBannerModel(data.getBannerAccessoryUrl3()));
        }
        int banner_delay_time = new BigDecimal(data.getIntervaltime()).multiply(new BigDecimal("1000")).intValue();

        if (bannerDatas != null&&!bannerDatas.isEmpty()) {
            if (bannerDatas.size() == 1) {
                mBanner.initTips(false, false, false)
                        .setImageLoaderManager(new GlideAppSimpleImageManager())
                        .setPageNumViewMargin(12, 12, 12, 12)
                        .setViewPagerTouchMode(true)
                        .setDelayTime(banner_delay_time)
                        .initListResources(bannerDatas)
                        .switchBanner(false)
                        .setOnBannerClickListener(GoodDetailActivity.this);
            } else {
                mBanner.initTips(false, true, false)
                        .setImageLoaderManager(new GlideAppSimpleImageManager())
                        .setPageNumViewMargin(12, 12, 12, 12)
                        .setViewPagerTouchMode(false)
                        .setDelayTime(banner_delay_time)
                        .initListResources(bannerDatas)
                        .switchBanner(true)
                        .setOnBannerClickListener(GoodDetailActivity.this);
            }
        }
    }

    //获取相似商品数据
    private void getSimilarProductData(String fixmedins_code, String storeSimilarity) {
        GoodsModel.sendStoreSimilarityRequest_user(TAG+goodsNo, fixmedins_code, storeSimilarity,goodsNo, new CustomerJsonCallBack<GoodsModel>() {
            @Override
            public void onRequestError(GoodsModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GoodsModel returnData) {
                dissWaitDailog();
                if (similarProductAdapter!=null){
                    similarProductAdapter.setNewData(returnData.getData());
                }
            }
        });
    }

    //获取已加入购物车的数据
    private void getShopCarData(String fixmedins_code) {
        if (!InsuredLoginUtils.isLogin()) {
            dissWaitDailog();
            return;
        }
        //2022-11-05 请求接口 获取已加入购物车数据
        ShopCarModel.sendShopCarRequest(TAG+goodsNo, fixmedins_code,"0","1", new CustomerJsonCallBack<ShopCarModel>() {
            @Override
            public void onRequestError(ShopCarModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(ShopCarModel returnData) {
                dissWaitDailog();
                List<ShopCarModel.DataBean> datas = returnData.getData();
                hasJoinedShopCarDatas.clear();
                if (datas != null&&!datas.isEmpty()&&datas.get(0).getShoppingCartGoods()!=null) {
                    //已加入购物车的数据
                    hasJoinedShopCarDatas.addAll(datas.get(0).getShoppingCartGoods());
                }
                calculatePrice(null, null);
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close:
                goFinish();
                break;
            case R.id.fl_collect://收藏
                if (pagerData==null){
                    showShortToast("数据还在请求中，请稍后再试");
                    return;
                }
                if ("1".equals(pagerData.getIsCollection())){//已收藏
                    onDelGoodCollect();
                }else {//未收藏
                    onAddGoodCollect();
                }
                break;
            case R.id.fl_more://更多
                onMorePop();
                break;
            case R.id.fl_item:
            case R.id.stv_product_describe://弹出药品说明书
                onDrugManualPop();
                break;
            case R.id.srl_service_promise://弹出服务承诺
                onServicePromisePop();
                break;
            case R.id.stv_go_shop://进入店铺
            case R.id.ll_shop://进入店铺
                if (pagerData==null){
                    showShortToast("数据还在请求中，请稍后再试");
                    return;
                }
                ShopDetailActivity.newIntance(GoodDetailActivity.this,pagerData.getFixmedins_code());
                break;
            case R.id.ll_contact_merchants:
                if (pagerData==null){
                    showShortToast("数据还在请求中，请稍后再试");
                    return;
                }
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(GoodDetailActivity.this, "提示", "是否拨打电话" + pagerData.getTelephone() + "?", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        RxTool.takePhone(GoodDetailActivity.this, pagerData.getTelephone());
                    }
                });
                break;
            case R.id.ll_shopcar://购物车弹框
                if (hasJoinedShopCarDatas.isEmpty()) {
                    showShortToast("请添加商品~");
                } else {
                    onShopcarPop();
                }
                break;
            case R.id.tv_add_shopcar://加入购物车
                if (insuredLoginSmrzUtils == null) {
                    insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
                }
                if (insuredLoginSmrzUtils.isLoginSmrz(GoodDetailActivity.this)) {
                    onAddShopcar();
                }
                break;
            case R.id.tv_apply_buy://申请购药
                if (insuredLoginSmrzUtils == null) {
                    insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
                }
                if (insuredLoginSmrzUtils.isLoginSmrz(GoodDetailActivity.this)) {
                    if (pagerData==null){
                        showShortToast("数据还在请求中，请稍后再试");
                        return;
                    }
                    if (shopCarPop!=null&&shopCarPop.isShowing()){//结算购物车的数据
                        shopCarPop.dismiss();

                        OnlineBuyDrugActivity.newIntance(GoodDetailActivity.this,hasJoinedShopCarDatas,pagerData.getStoreName(),
                                pagerData.getStoreAccessoryUrl(),pagerData.getStartingPrice(),pagerData.getLatitude(),pagerData.getLongitude(),
                                pagerData.getFixmedins_type());
                    }else {//结算当前的商品
                        if (pagerData.getInventoryQuantity()<=0){
                            showShortToast("库存不足，请联系商家");
                            return;
                        }

                        ArrayList<GoodsModel.DataBean> dataBeans=new ArrayList<>();
                        GoodsModel.DataBean addGoodsData = new GoodsModel.DataBean(pagerData.getGoodsNo(), pagerData.getItemType(), pagerData.getEfccAtd(),
                                pagerData.getInventoryQuantity(), pagerData.getPrice(), pagerData.getIsPlatformDrug(), pagerData.getFixmedins_code(),
                                pagerData.getGoodsLocation(), pagerData.getCatalogueId(), pagerData.getBannerAccessoryId1(), pagerData.getDrugsClassification(),
                                pagerData.getDrugsClassificationName(), pagerData.getIsPromotion(), pagerData.getGoodsName(),
                                pagerData.getPromotionDiscount(), pagerData.getPreferentialPrice(), pagerData.getIsOnSale(),
                                pagerData.getBannerAccessoryUrl1(), pagerData.getMonthlySales(), 1,
                                pagerData.getSpec(),pagerData.getAssociatedDiagnostics(),pagerData.getIsShowGoodsSold(),pagerData.getIsShowMargin());
                        dataBeans.add(addGoodsData);
                        OnlineBuyDrugActivity.newIntance(GoodDetailActivity.this,dataBeans,pagerData.getStoreName(),
                                pagerData.getStoreAccessoryUrl(),pagerData.getStartingPrice(),pagerData.getLatitude(),pagerData.getLongitude(),
                                pagerData.getFixmedins_type());
                    }
                }
                break;
        }
    }

    //添加商品收藏
    private void onAddGoodCollect() {
        showWaitDialog();
        BaseModel.sendAddGoodCollectionRequest(TAG,goodsNo, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("收藏成功");
                mIvCollect.setImageResource(R.drawable.ic_gd_collevt_pre);
                pagerData.setIsCollection("1");
                MsgBus.sendCollectStatus_user().post("1");
            }
        });
    }

    //删除商品收藏
    private void onDelGoodCollect() {
        showWaitDialog();
        BaseModel.sendDelGoodCollectionRequest(TAG, goodsNo, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("删除收藏成功");
                mIvCollect.setImageResource(R.drawable.ic_gd_collevt_nor);
                pagerData.setIsCollection("0");
                MsgBus.sendCollectStatus_user().post("0");
            }
        });
    }

    @Override
    public void onBannerClick(View view, int position, Object model) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            return;
        }

        OpenImage.with(GoodDetailActivity.this)
                //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                .setClickViewPager(mBanner.getViewPager(),(BannerAdapter)mBanner.getViewPager().getAdapter())
                //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                //RecyclerView的数据
                .setImageUrlList(bannerDatas)
                //点击的ImageView所在数据的位置
                .setClickPosition(position)
                //开始展示大图
                .show();
    }

    //弹出药品说明书
    private void onDrugManualPop() {
        if (pagerData==null){
            showShortToast("数据还在请求中，请稍后再试");
            return;
        }
        if (drugManualPop == null) {
            drugManualPop = new DrugManualPop(GoodDetailActivity.this,productDescribeAdapter.getData(), pagerData.getInstructionsAccessoryUrl());
        }
        drugManualPop.showPopupWindow();
    }

    //更多弹框
    private void onMorePop() {
        if (goodDetailMorePop == null) {
            goodDetailMorePop = new ShopDetailMorePop(GoodDetailActivity.this, CommonlyUsedDataUtils.getInstance().getGoodDetailMoreData());
        }

        goodDetailMorePop.setOnListener(new ShopDetailMorePop.IOptionListener() {
            @Override
            public void onItemClick(ShopDetailMoreModel item) {
                switch (item.getText()) {
                    case "首页":
                        SmartPhaMainActivity_user.newIntance(GoodDetailActivity.this, 0);
                        break;
                }
            }
        });

        goodDetailMorePop.showPopupWindow(mIvMore);
    }

    //服务承诺弹框
    private void onServicePromisePop() {
        if (servicePromisePop == null) {
            servicePromisePop = new ServicePromisePop(GoodDetailActivity.this);
        }
        servicePromisePop.showPopupWindow();
    }

    //购物车弹框
    private void onShopcarPop() {
        if (shopCarPop == null) {
            shopCarPop = new ShopCarPop_user(GoodDetailActivity.this, hasJoinedShopCarDatas, totalRedNum, totalPrice);
            shopCarPop.setOnDismissListener(new BasePopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    mLlShop.setVisibility(View.VISIBLE);
                    mLlContactMerchants.setVisibility(View.VISIBLE);

                    mTvAddShopcar.setVisibility(View.VISIBLE);
                    mTvApplyBuy.getShapeDrawableBuilder().setRadius(0, bgRadius, 0, bgRadius).intoBackground();
                }
            });

            shopCarPop.setOnPopupWindowShowListener(new BasePopupWindow.OnPopupWindowShowListener() {
                @Override
                public void onShowing() {
                    mLlShop.setVisibility(View.GONE);
                    mLlContactMerchants.setVisibility(View.GONE);

                    mTvAddShopcar.setVisibility(View.GONE);
                    mTvApplyBuy.getShapeDrawableBuilder().setRadius(getResources().getDimensionPixelSize(R.dimen.dp_39)).intoBackground();
                }
            });
        } else {
            shopCarPop.setData(hasJoinedShopCarDatas, totalRedNum, totalPrice);
        }

        shopCarPop.setOnListener(new ShopCarPop_user.IOptionListener() {
            @Override
            public void onCleanShopCar() {
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(GoodDetailActivity.this, "提示", "确定清空购物车商品？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {}

                    @Override
                    public void onBtn02Click() {
                        onDelAllShopCar();
                    }
                });
            }

            @Override
            public void onItemClick(GoodsModel.DataBean itemData) {
                if (!pagerData.getGoodsNo().equals(itemData.getGoodsNo())) {
                    // 2022-11-05 跳转商品详情页面
                    GoodDetailActivity.newIntance(GoodDetailActivity.this, itemData.getGoodsNo(),itemData.getFixmedins_code(),itemData.getGoodsName());
                }
            }

            @Override
            public void onPopAdd(GoodsModel.DataBean itemData, TextView tvNum, TextView tvPrice) {
                onAddShopcar(itemData, tvNum, tvPrice, "add");
            }

            @Override
            public void onPopReduce(GoodsModel.DataBean itemData, TextView tvNum, TextView tvPrice) {
                onAddShopcar(itemData, tvNum, tvPrice, "reduce");
            }
        });
        shopCarPop.showPopupWindow(mLine);
    }

    //清空购物车
    private void onDelAllShopCar() {
        showWaitDialog();
        BaseModel.sendDelAllShopCarRequest(TAG, pagerData.getFixmedins_code(), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("清空购物车成功");
                MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "2"));
                hasJoinedShopCarDatas.clear();
                pagerData.setShoppingCartNum(0);
                onRefreshSimilar();
                calculatePrice(null, null);
            }
        });
    }

    //加入购物车
    private void onAddShopcar() {
        if (pagerData==null){
            showShortToast("数据还在请求中，请稍后再试");
            return;
        }
        int num = pagerData.getShoppingCartNum();
        if (num+1>pagerData.getInventoryQuantity()){
            showShortToast("该商品库存不足,请联系商家");
            return;
        }

        showWaitDialog();
        BaseModel.sendAddShopCarRequest(TAG, pagerData.getFixmedins_code(), pagerData.getGoodsNo(), pagerData.getShoppingCartNum() + 1, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("加入购物车成功");
                MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "2"));
                addGoodsData = new GoodsModel.DataBean(pagerData.getGoodsNo(), pagerData.getItemType(), pagerData.getEfccAtd(),
                        pagerData.getInventoryQuantity(), pagerData.getPrice(), pagerData.getIsPlatformDrug(), pagerData.getFixmedins_code(),
                        pagerData.getGoodsLocation(), pagerData.getCatalogueId(), pagerData.getBannerAccessoryId1(), pagerData.getDrugsClassification(),
                        pagerData.getDrugsClassificationName(), pagerData.getIsPromotion(), pagerData.getGoodsName(),
                        pagerData.getPromotionDiscount(), pagerData.getPreferentialPrice(), pagerData.getIsOnSale(),
                        pagerData.getBannerAccessoryUrl1(), pagerData.getMonthlySales(), pagerData.getShoppingCartNum() + 1,
                        pagerData.getSpec(),pagerData.getAssociatedDiagnostics(),pagerData.getIsShowGoodsSold(),pagerData.getIsShowMargin());
                pagerData.setShoppingCartNum(addGoodsData.getShoppingCartNum());
                addCart(mTvAddShopcar, addGoodsData);//ParcelUtils.copy(productModel)
            }
        });
    }

    //加入购物车--相似列表
    private void onAddShopcar(GoodsModel.DataBean itemData, BaseQuickAdapter baseQuickAdapter, View view, int pos, TextView tvSelectNum, String tag) {
        showWaitDialog();
        BaseModel.sendAddShopCarRequest(TAG, itemData.getFixmedins_code(), itemData.getGoodsNo(), "add".equals(tag) ? itemData.getShoppingCartNum() + 1 : itemData.getShoppingCartNum() - 1,
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "2"));
                        switch (tag) {
                            case "add":
                                if (itemData.getShoppingCartNum() == 0) {
                                    FrameLayout flNumReduce01 = (FrameLayout) baseQuickAdapter.getViewByPosition(mScrvSimilar, pos, R.id.fl_num_reduce);
                                    flNumReduce01.setVisibility(View.VISIBLE);
                                    tvSelectNum.setVisibility(View.VISIBLE);

                                    itemData.setShoppingCartNum(1);
                                    tvSelectNum.setText("1");
                                } else {
                                    itemData.setShoppingCartNum(itemData.getShoppingCartNum() + 1);
                                    tvSelectNum.setText(String.valueOf(itemData.getShoppingCartNum()));
                                }

                                addCart(view, ParcelUtils.copy(itemData));
                                break;
                            case "reduce":
                                if (itemData.getShoppingCartNum() == 1) {
                                    FrameLayout flNumReduce02 = (FrameLayout) baseQuickAdapter.getViewByPosition(mScrvSimilar, pos, R.id.fl_num_reduce);

                                    flNumReduce02.setVisibility(View.GONE);
                                    tvSelectNum.setVisibility(View.GONE);

                                    itemData.setShoppingCartNum(0);
                                    tvSelectNum.setText("0");
                                } else {
                                    itemData.setShoppingCartNum(itemData.getShoppingCartNum() - 1);
                                    tvSelectNum.setText(String.valueOf(itemData.getShoppingCartNum()));
                                }

                                onReduceCarList(ParcelUtils.copy(itemData), "1", null, null);
                                break;
                        }
                    }
                });
    }

    //加入购物车--购物车列表
    private void onAddShopcar(GoodsModel.DataBean itemData, TextView tvNum, TextView tvPrice, String tag) {
        showWaitDialog();
        BaseModel.sendAddShopCarRequest(TAG, itemData.getFixmedins_code(), itemData.getGoodsNo(), itemData.getShoppingCartNum(),
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "2"));
                        switch (tag) {
                            case "add":
                                //判断是否是属于该商品详情的
                                onAddCarList(itemData, tvNum, tvPrice);
                                if (pagerData.getGoodsNo().equals(itemData.getGoodsNo())) {
                                    //若属于该商品详情 那么只需将该商品数据中 被加入购物车的数量进行增减
                                    pagerData.setShoppingCartNum(itemData.getShoppingCartNum());
                                } else {//不属于 那说明顶多是在相似列表里面
                                    onRefreshSimilarItem(itemData);
                                }
                                break;
                            case "reduce":
                                onReduceCarList(itemData, "2", tvNum, tvPrice);
                                if (pagerData.getGoodsNo().equals(itemData.getGoodsNo())) {
                                    //若属于该商品详情 那么只需将该商品数据中 被加入购物车的数量进行增减
                                    pagerData.setShoppingCartNum(itemData.getShoppingCartNum());
                                } else {
                                    onRefreshSimilarItem(itemData);
                                }
                                break;
                        }
                    }
                });
    }

    /**
     * ★★★★★把商品添加到购物车的动画效果★★★★★
     */
    private void addCart(View view, GoodsModel.DataBean item) {
        ShoppingCartAnimationView shoppingCartAnimationView = new ShoppingCartAnimationView(this);
        int position[] = new int[2];
        view.getLocationInWindow(position);
        shoppingCartAnimationView.setStartPosition(new Point(position[0], position[1]));
        mDecorView.addView(shoppingCartAnimationView);
        int endPosition[] = new int[2];
        mFlShopcarIcon.getLocationInWindow(endPosition);
        shoppingCartAnimationView.setEndPosition(new Point(endPosition[0], endPosition[1]));
        shoppingCartAnimationView.startBeizerAnimation();

        //2022-11-04  加入购物车的记录列表
        onAddCarList(item, null, null);
    }

    //加入购物车的记录列表
    private void onAddCarList(GoodsModel.DataBean item, TextView tvNum, TextView tvPrice) {
        //判断当前添加的商品 是否商品数为1 为1说明是新添加的商品 若不是 则说明数量大于1 那么遍历列表 查询是同一个id的情况下 把数量设置进去
        if (item.getShoppingCartNum() == 1) {
            hasJoinedShopCarDatas.add(item);
        } else {
            for (GoodsModel.DataBean hasJoinedShopCarData : hasJoinedShopCarDatas) {
                if (hasJoinedShopCarData.getGoodsNo().equals(item.getGoodsNo())) {
                    hasJoinedShopCarData.setShoppingCartNum(item.getShoppingCartNum());
                    break;
                }
            }
        }

        Log.e("CarList", "Add_CarListsize=====" + hasJoinedShopCarDatas.size());
        // 2022-11-04 计算总价
        calculatePrice(tvNum, tvPrice);
    }

    //去除购物车的记录列表  from1:来自店铺详情页面列表 2:来自店铺详情 购物车弹框
    public void onReduceCarList(GoodsModel.DataBean item, String from, TextView tvNum, TextView tvPrice) {
        //判断当前添加的商品 是否商品数为0 为0说明是要删除的商品
        if ("1".equals(from)) {
            if (item.getShoppingCartNum() <= 0) {
                hasJoinedShopCarDatas.remove(item);
            } else {
                for (GoodsModel.DataBean hasJoinedShopCarData : hasJoinedShopCarDatas) {
                    if (hasJoinedShopCarData.getGoodsNo().equals(item.getGoodsNo())) {
                        hasJoinedShopCarData.setShoppingCartNum(item.getShoppingCartNum());
                        break;
                    }
                }
            }
        }

        Log.e("CarList", "Reduce_CarListsize=====" + hasJoinedShopCarDatas.size());
        // 2022-11-04 计算总价
        calculatePrice(tvNum, tvPrice);
    }

    //当购物车弹框 改变选择的数量时 需要更新列表中对应的数量
    public void onRefreshSimilarItem(GoodsModel.DataBean item) {
        List<GoodsModel.DataBean> allDatas = similarProductAdapter.getData();
        GoodsModel.DataBean itemData;
        for (int i = 0, size = allDatas.size(); i < size; i++) {
            itemData = allDatas.get(i);
            if (itemData.getGoodsNo().equals(item.getGoodsNo())) {
                itemData.setShoppingCartNum(item.getShoppingCartNum());
                similarProductAdapter.notifyItemChanged(i);
                break;
            }
        }
    }

    //当购物车弹框 清空购物车时 需要更新相似列表
    public void onRefreshSimilar() {
        List<GoodsModel.DataBean> allDatas = similarProductAdapter.getData();
        for (int i = 0, size = allDatas.size(); i < size; i++) {
            allDatas.get(i).setShoppingCartNum(0);
        }
        similarProductAdapter.notifyDataSetChanged();
    }

    //计算价格
    private void calculatePrice(TextView tvNum, TextView tvPrice) {
        totalRedNum = "0";
        totalPrice = "0";
        for (GoodsModel.DataBean rightLinkModel : hasJoinedShopCarDatas) {
            if ("1".equals(rightLinkModel.getIsPromotion())) {//有折扣
                totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(rightLinkModel.getPreferentialPrice())
                        .multiply(new BigDecimal(String.valueOf(rightLinkModel.getShoppingCartNum())))).toPlainString();

            }else {//没有折扣
                totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(rightLinkModel.getPrice())
                        .multiply(new BigDecimal(String.valueOf(rightLinkModel.getShoppingCartNum())))).toPlainString();
            }
            totalRedNum = new BigDecimal(totalRedNum).add(new BigDecimal(String.valueOf(rightLinkModel.getShoppingCartNum()))).toPlainString();
        }

        if ("0".equals(totalRedNum)) {
            mSvRedPoint.setVisibility(View.GONE);
        } else {
            mSvRedPoint.setVisibility(View.VISIBLE);
            mSvRedPoint.setText(totalRedNum);
        }

        if (tvNum != null) {
            tvNum.setText("(共" + totalRedNum + "件商品)");
        }
        if (tvPrice != null) {
            tvPrice.setText(totalPrice);
        }
    }

    //tablayout对应标签的切换
    private void setScrollPos(int newPos) {
        if (lastPos != newPos) {
            //该方法不会触发tablayout 的onTabSelected 监听
            mStbTab.setCurrentTab(newPos);
        }
        lastPos = newPos;
    }

    //隐藏加载框
    public void dissWaitDailog() {
        requestTag++;
        if (requestTag >= 3) {
            hideWaitDialog();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mBanner != null) {
            mBanner.start();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mBanner != null) {
            mBanner.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBanner != null) {
            mBanner.destroy();
            mBanner = null;
        }
        if (insuredLoginSmrzUtils != null) {
            insuredLoginSmrzUtils.cleanData();
            insuredLoginSmrzUtils = null;
        }
        if (goodDetailMorePop != null) {
            goodDetailMorePop.onCleanListener();
            goodDetailMorePop.onDestroy();
        }

        if (drugManualPop != null) {
            drugManualPop.onDestroy();
        }
        if (servicePromisePop != null) {
            servicePromisePop.onDestroy();
        }

        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }

        if (shopCarPop != null) {
            shopCarPop.onCleanListener();
            shopCarPop.onDestroy();
        }

        if (bannerDatas != null) {
            bannerDatas.clear();
            bannerDatas = null;
        }

        if (productDescribeModels != null) {
            productDescribeModels.clear();
            productDescribeModels = null;
        }

        if (goodPics != null) {
            goodPics.clear();
            goodPics = null;
        }
    }

    @Override
    protected void requestCancle() {
        //判断需要取消的请求 activity是否在前台
        if (AppManager_GoodsDetailsAcivity.getInstance().currentActivity().toString().equals(this.toString())){
            OkHttpUtils.getInstance().cancelTag(TAG+goodsNo);//取消以Activity.this作为tag的请求
        }
    }

    //goodsNo:300000
    public static void newIntance(Context context, String goodsNo,String fixmedins_code,String goodsName) {
        Intent intent = new Intent(context, GoodDetailActivity.class);
        intent.putExtra("goodsNo", goodsNo);
        intent.putExtra("fixmedins_code", fixmedins_code);
        intent.putExtra("goodsName", goodsName);
        context.startActivity(intent);
    }
}
