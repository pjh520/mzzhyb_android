package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.SpanUtils;
import com.frame.compiler.widget.banner.listener.OnBannerClickListener;
import com.frame.compiler.widget.banner.manager.GlideAppSimpleImageManager;
import com.frame.compiler.widget.banner.widget.BannerAdapter;
import com.frame.compiler.widget.banner.widget.BannerLayout;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.tabLayout.CommonTabLayout;
import com.frame.compiler.widget.tabLayout.listener.CustomTabEntity;
import com.frame.compiler.widget.tabLayout.listener.OnTabSelectListener;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeRelativeLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.GoodDetailModel_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.GoodDetailModel_user2;
import com.jrdz.zhyb_android.ui.zhyf_user.model.GoodsBannerModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ProductDescribeModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.QueryShoppingCartNumModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarRefreshModel;
import com.jrdz.zhyb_android.utils.InsuredLoginSmrzUtils;
import com.jrdz.zhyb_android.widget.pop.DrugManualPop;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import cody.bus.ObserverWrapper;
import razerdp.basepopup.BasePopupWindow;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/11/6
 * 描    述：商品详情页
 * ================================================
 */
public class GoodDetailActivity2 extends BaseActivity implements OnBannerClickListener {
    private NestedScrollView mScrollView;
    private EditText mEtSearch;
    private ShapeTextView mStvSearch;
    private BannerLayout mBanner;
    private TextView mTvTitle;
    private ShapeTextView mStvProductDescribe;
    private LinearLayout mLlContain;
    private View mLine;
    private LinearLayout mLlShop;
    private ShapeTextView mTvApplyBuy;

    private String goodsNo;
    List<GoodsBannerModel> bannerDatas = new ArrayList<>();//存储轮播图数据
    private GoodDetailModel_user.DataBean pagerData;//记录当前商品的数据
    private InsuredLoginSmrzUtils insuredLoginSmrzUtils;//判断用户是否登录以及实名注册
    private GoodDetailModel_user2.DataBean1 currentData;//当前选中的在售商家data
    private ShapeRelativeLayout currentView;//当前选中的在售商家view
    private DrugManualPop drugManualPop;//药品说明书弹框
    ArrayList<ProductDescribeModel> productDescribeModels = new ArrayList<>();//产品说明数据

    @Override
    public int getLayoutId() {
        return R.layout.activity_good_detail2;
    }

    @Override
    public void initView() {
        super.initView();
        mScrollView = findViewById(R.id.scroll_view);
        mEtSearch= findViewById(R.id.et_search);
        mStvSearch= findViewById(R.id.stv_search);
        mBanner = findViewById(R.id.banner);
        mTvTitle = findViewById(R.id.tv_title);
        mStvProductDescribe = findViewById(R.id.stv_product_describe);
        mLlContain = findViewById(R.id.ll_contain);
        mLine = findViewById(R.id.line);
        mLlShop = findViewById(R.id.ll_shop);
        mTvApplyBuy = findViewById(R.id.tv_apply_buy);
    }

    @Override
    public void initData() {
        goodsNo = getIntent().getStringExtra("goodsNo");
        super.initData();
        showWaitDialog();
        getPagerData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    showWaitDialog();
                    getPagerData();
                    return true;
                }
                return false;
            }
        });
        mStvSearch.setOnClickListener(this);
        mStvProductDescribe.setOnClickListener(this);
        mLlShop.setOnClickListener(this);
        mTvApplyBuy.setOnClickListener(this);
    }

    //获取页面数据
    private void getPagerData() {
        GoodDetailModel_user2.sendGoodDetailRequest_user2(TAG, goodsNo, String.valueOf(Constants.AppStorage.APP_LONGITUDE),
                String.valueOf(Constants.AppStorage.APP_LATITUDE), EmptyUtils.strEmpty(mEtSearch.getText().toString()),new CustomerJsonCallBack<GoodDetailModel_user2>() {
            @Override
            public void onRequestError(GoodDetailModel_user2 returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GoodDetailModel_user2 returnData) {
                hideWaitDialog();
                if (isFinishing()) return;
                setPagerData(returnData);
            }
        });
    }

    //设置页面数据
    private void setPagerData(GoodDetailModel_user2 returnData) {
        pagerData = returnData.getData();
        currentData = null;
        currentView = null;
        if (pagerData != null) {
            //获取轮播图数据
            setBannerData(pagerData);
            mTvTitle.setText(EmptyUtils.strEmpty(pagerData.getGoodsName()));
        }
        ArrayList<GoodDetailModel_user2.DataBean1> data1s = returnData.getData1();
        if (data1s != null) {
            mLlContain.removeAllViews();
            //店铺列表
            for (GoodDetailModel_user2.DataBean1 dataBean1 : data1s) {
                View view = LayoutInflater.from(this).inflate(R.layout.layout_good_detail_shop_item, mLlContain, false);
                ShapeRelativeLayout srlItem = view.findViewById(R.id.srl_item);
                TextView mTvPrice = view.findViewById(R.id.tv_price);
                TextView mTvSaleNum = view.findViewById(R.id.tv_sale_num);
                TextView mTvHasminPrice = view.findViewById(R.id.tv_hasmin_price);
                TextView mTvShopName = view.findViewById(R.id.tv_shop_name);
                TextView mTvFeeTime = view.findViewById(R.id.tv_fee_time);

                if (dataBean1.isChoose()) {
                    srlItem.getShapeDrawableBuilder().setStrokeColor(getResources().getColor(R.color.color_3663e1))
                            .setSolidColor(getResources().getColor(R.color.color_edf1fc)).intoBackground();
                    currentData = dataBean1;
                    currentView = srlItem;
                } else {
                    srlItem.getShapeDrawableBuilder().setStrokeColor(getResources().getColor(R.color.bar_transparent))
                            .setSolidColor(getResources().getColor(R.color.color_f7f7f7)).intoBackground();
                }
                String showPrice = "1".equals(dataBean1.getIsPromotion()) ? EmptyUtils.strEmpty(dataBean1.getPreferentialPrice()) : EmptyUtils.strEmpty(dataBean1.getPrice());
                mTvPrice.setText("¥" + showPrice);
                //是否展示商品的月售数据
                if ("1".equals(dataBean1.getIsShowGoodsSold())) {
                    mTvSaleNum.setVisibility(View.VISIBLE);
                    mTvSaleNum.setText("月售" + dataBean1.getMonthlySales());
                } else {
                    mTvSaleNum.setVisibility(View.GONE);
                }

                //判断是否够起送费
                double diffPrice = new BigDecimal(dataBean1.getStartingPrice()).subtract(new BigDecimal(EmptyUtils.strEmptyToText(showPrice, "0"))).doubleValue();
                if (diffPrice <= 0) {//够起送费 可以直接购买
                    mTvHasminPrice.setVisibility(View.GONE);
                } else {//不够起送费 还需要添加商品
                    mTvHasminPrice.setVisibility(View.VISIBLE);
                    mTvHasminPrice.setText("差¥" + diffPrice + "起送");
                }

                //设置店铺名称
                mTvShopName.setText(EmptyUtils.strEmpty(dataBean1.getStoreName()));
                mTvFeeTime.setText(EmptyUtils.strEmpty(dataBean1.getDistributionMethod()));

                srlItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!dataBean1.isChoose()) {
                            if (currentData != null) {
                                currentData.setChoose(false);
                            }
                            if (currentView != null) {
                                currentView.getShapeDrawableBuilder().setStrokeColor(getResources().getColor(R.color.bar_transparent))
                                        .setSolidColor(getResources().getColor(R.color.color_f7f7f7)).intoBackground();
                            }

                            dataBean1.setChoose(true);
                            srlItem.getShapeDrawableBuilder().setStrokeColor(getResources().getColor(R.color.color_3663e1))
                                    .setSolidColor(getResources().getColor(R.color.color_edf1fc)).intoBackground();

                            currentData = dataBean1;
                            currentView = srlItem;

//                            if (mTvHasminPrice.getVisibility() == View.VISIBLE) {
//                                SpannableStringBuilder str = new SpanUtils().append("凑单购买").setFontSize(getResources().getDimensionPixelSize(R.dimen.dp_30))
//                                        .append("\n")
//                                        .append(mTvHasminPrice.getText().toString()).setFontSize(getResources().getDimensionPixelSize(R.dimen.dp_16))
//                                        .create();
//                                mTvApplyBuy.setText(str);
//                            } else {
//                                mTvApplyBuy.setText("购买");
//                            }

//                            if (currentData != null && currentView != null) {
//                                mTvApplyBuy.setEnabled(true);
//                            }
                        }
                    }
                });

                mLlContain.addView(view);
            }
        }

        //产品说明
        setGoodsDescribe(pagerData);
    }

    //设置产品说明 数据
    private void setGoodsDescribe(GoodDetailModel_user.DataBean data) {
        productDescribeModels.clear();
        //产品说明的数据
        switch (data.getDrugsClassification()) {
            case "1"://西药中成药
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_main_func, "功能主治", EmptyUtils.strEmpty(data.getEfccAtd())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_usage, "常见用法", EmptyUtils.strEmpty(data.getUsualWay())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_validity, "有效期", data.getExpiryDateCount() + "个月"));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_storage, "贮存条件", EmptyUtils.strEmpty(data.getStorageConditions())));
                break;
            case "2"://医疗器械
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_applicablescope, "适用范围", EmptyUtils.strEmpty(data.getApplicableScope())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_usagedosage, "用法用量", EmptyUtils.strEmpty(data.getUsageDosage())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_validity, "有效期", data.getExpiryDateCount() + "个月"));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_packaging, "包装", EmptyUtils.strEmpty(data.getPackaging())));
                break;
            case "3"://成人用品
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_usagemethod, "使用方法", EmptyUtils.strEmpty(data.getUsagemethod())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_storage, "贮存条件", EmptyUtils.strEmpty(data.getStorageConditions())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_validity, "有效期", data.getExpiryDateCount() + "个月"));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_packaging, "包装", EmptyUtils.strEmpty(data.getPackaging())));
                break;
            case "4"://个人用品
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_usagedosage, "用法用量", EmptyUtils.strEmpty(data.getUsageDosage())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_storage, "贮存条件", EmptyUtils.strEmpty(data.getStorageConditions())));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_validity, "有效期", data.getExpiryDateCount() + "个月"));
                productDescribeModels.add(new ProductDescribeModel(R.drawable.ic_gd_packaging, "包装", EmptyUtils.strEmpty(data.getPackaging())));
                break;
        }
    }

    //获取轮播图数据
    private void setBannerData(GoodDetailModel_user.DataBean data) {
        bannerDatas.clear();
        if (!EmptyUtils.isEmpty(data.getBannerAccessoryUrl1())) {
            bannerDatas.add(new GoodsBannerModel(data.getBannerAccessoryUrl1()));
        }
        if (!EmptyUtils.isEmpty(data.getBannerAccessoryUrl2())) {
            bannerDatas.add(new GoodsBannerModel(data.getBannerAccessoryUrl2()));
        }
        if (!EmptyUtils.isEmpty(data.getBannerAccessoryUrl3())) {
            bannerDatas.add(new GoodsBannerModel(data.getBannerAccessoryUrl3()));
        }
        int banner_delay_time = new BigDecimal(data.getIntervaltime()).multiply(new BigDecimal("1000")).intValue();

        if (bannerDatas != null && !bannerDatas.isEmpty()) {
            if (bannerDatas.size() == 1) {
                mBanner.initTips(false, false, false)
                        .setImageLoaderManager(new GlideAppSimpleImageManager())
                        .setPageNumViewMargin(12, 12, 12, 12)
                        .setViewPagerTouchMode(true)
                        .setDelayTime(banner_delay_time)
                        .initListResources(bannerDatas)
                        .switchBanner(false)
                        .setOnBannerClickListener(GoodDetailActivity2.this);
            } else {
                mBanner.initTips(false, true, false)
                        .setImageLoaderManager(new GlideAppSimpleImageManager())
                        .setPageNumViewMargin(12, 12, 12, 12)
                        .setViewPagerTouchMode(false)
                        .setDelayTime(banner_delay_time)
                        .initListResources(bannerDatas)
                        .switchBanner(true)
                        .setOnBannerClickListener(GoodDetailActivity2.this);
            }
        }
    }

    @Override
    public void onBannerClick(View view, int position, Object model) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            return;
        }

        OpenImage.with(GoodDetailActivity2.this)
                //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                .setClickViewPager(mBanner.getViewPager(), (BannerAdapter) mBanner.getViewPager().getAdapter())
                //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP, true)
                //RecyclerView的数据
                .setImageUrlList(bannerDatas)
                //点击的ImageView所在数据的位置
                .setClickPosition(position)
                //开始展示大图
                .show();
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.stv_search:
                showWaitDialog();
                getPagerData();
                break;
            case R.id.stv_product_describe://弹出药品说明书
                onDrugManualPop();
                break;
            case R.id.ll_shop://进入店铺
                if (currentData == null) {
                    showShortToast("请选择在售商家!");
                    return;
                }
                ShopDetailActivity.newIntance(GoodDetailActivity2.this, currentData.getFixmedins_code());
                break;
            case R.id.tv_apply_buy://申请购药
                applyBuy();
                break;
        }
    }

    private void onDrugManualPop() {
        if (pagerData == null) return;
        if (drugManualPop == null) {
            drugManualPop = new DrugManualPop(GoodDetailActivity2.this, productDescribeModels, pagerData.getInstructionsAccessoryUrl());
        }
        drugManualPop.showPopupWindow();
    }

    //购买
    private void applyBuy() {
        if (insuredLoginSmrzUtils == null) {
            insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
        }
        if (insuredLoginSmrzUtils.isLoginSmrz(GoodDetailActivity2.this)) {
            if (currentData == null || currentView == null) {
                showShortToast("请先选择在售商家");
                return;
            }
            if (currentData.getInventoryQuantity() <= 0) {
                showShortToast("库存不足，请联系商家");
                return;
            }

            TextView mTvHasminPrice = currentView.findViewById(R.id.tv_hasmin_price);
//            if (mTvHasminPrice.getVisibility() == View.VISIBLE) {//还需要凑单 跳转店铺详情页面 将当前商品加入购物车
//                getGoodsShopCarNum(currentData.getGoodsNo(),currentData.getFixmedins_code());
//            } else {//不需要凑单 直接使用当前店铺 当前商品 跳转下单界面
            ArrayList<GoodsModel.DataBean> dataBeans = new ArrayList<>();
            GoodsModel.DataBean addGoodsData = new GoodsModel.DataBean(currentData.getGoodsNo(), currentData.getItemType(), currentData.getEfccAtd(),
                    currentData.getInventoryQuantity(), currentData.getPrice(), currentData.getIsPlatformDrug(), currentData.getFixmedins_code(),
                    currentData.getGoodsLocation(), currentData.getCatalogueId(), currentData.getBannerAccessoryId1(), currentData.getDrugsClassification(),
                    currentData.getDrugsClassificationName(), currentData.getIsPromotion(), currentData.getGoodsName(),
                    currentData.getPromotionDiscount(), currentData.getPreferentialPrice(), currentData.getIsOnSale(),
                    currentData.getBannerAccessoryUrl1(), currentData.getMonthlySales(), 1,
                    currentData.getSpec(), currentData.getAssociatedDiagnostics(), currentData.getIsShowGoodsSold(), currentData.getIsShowMargin());
            dataBeans.add(addGoodsData);
            OnlineBuyDrugActivity.newIntance(GoodDetailActivity2.this, dataBeans, currentData.getStoreName(),
                    currentData.getStoreAccessoryUrl(), currentData.getStartingPrice(), currentData.getLatitude(), currentData.getLongitude(),
                    currentData.getFixmedins_type());
//            }
        }
    }

    //获取商品 加入购物车的数量
    private void getGoodsShopCarNum(String goodsNo, String fixmedins_code) {
        showWaitDialog();
        QueryShoppingCartNumModel.sendQueryShoppingCartNumRequest(TAG, goodsNo, fixmedins_code, new CustomerJsonCallBack<QueryShoppingCartNumModel>() {
            @Override
            public void onRequestError(QueryShoppingCartNumModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(QueryShoppingCartNumModel returnData) {
                QueryShoppingCartNumModel.DataBean data = returnData.getData();
                if (data != null) {
                    int shoppingCartNum = new BigDecimal(EmptyUtils.strEmptyToText(data.getShoppingCartNum(), "0")).add(new BigDecimal("1")).intValue();
                    onAddShopcar(goodsNo, fixmedins_code, shoppingCartNum);
                }
            }
        });
    }

    //加入购物车--购物车列表
    private void onAddShopcar(String goodsNo, String fixmedins_code, int shoppingCartNum) {
        showWaitDialog();
        BaseModel.sendAddShopCarRequest(TAG, fixmedins_code, goodsNo, shoppingCartNum, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "1"));
                ShopDetailActivity2.newIntance(GoodDetailActivity2.this, fixmedins_code);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mBanner != null) {
            mBanner.start();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mBanner != null) {
            mBanner.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBanner != null) {
            mBanner.destroy();
            mBanner = null;
        }
        if (insuredLoginSmrzUtils != null) {
            insuredLoginSmrzUtils.cleanData();
            insuredLoginSmrzUtils = null;
        }
        if (drugManualPop != null) {
            drugManualPop.onDestroy();
        }
        if (bannerDatas != null) {
            bannerDatas.clear();
            bannerDatas = null;
        }
    }

    //goodsNo:300000
    public static void newIntance(Context context, String goodsNo) {
        Intent intent = new Intent(context, GoodDetailActivity2.class);
        intent.putExtra("goodsNo", goodsNo);
        context.startActivity(intent);
    }
}
