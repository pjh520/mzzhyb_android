package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.DateUtil;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.SpanUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.countDownTime.CustomCountDownTimer;
import com.frame.compiler.widget.glide.GlideUtils;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.JustifyContent;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeRelativeLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.ScanPayQRCodeActivity;
import com.jrdz.zhyb_android.ui.home.activity.ScanPayResultActivity;
import com.jrdz.zhyb_android.ui.home.model.ScanPayStatusModel;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.InquiryInfoPicAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.RelationDiseAdapter_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.FreeBackPicModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.HasDoctorModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.InquiryInfoModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OrderListModel_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.QueryPresStatusModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.RelationDiseModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.VisitorModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.WXUtils;
import com.jrdz.zhyb_android.utils.upload.CompressUploadSinglePicUtils_zhyf;
import com.jrdz.zhyb_android.widget.MyFlexboxLayoutManager;
import com.jrdz.zhyb_android.widget.pop.AddRelationDisePop;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-07
 * 描    述：填写问诊信息
 * ================================================
 */
public class InquiryInfoActivity extends BaseActivity implements CompressUploadSinglePicUtils_zhyf.IChoosePic {
    public final static String TAG_SELECT_PIC_01 = "1";//意见反馈图片
    public static final int PIC_NOR = 1;//新增图片
    public static final int PIC_ADDBTN = 2;//添加按钮

    private TextView mTv01Seize;
    private TextView mTv02Seize;
    private ImageView mIvEnter01;
    private View mLine01;
    private ImageView mIvPrescrHead;
    private TextView mTvPrescrInfo;
    private ShapeTextView mStvPrescrOneself;
    private TextView mTvPrescrCertNo;
    private ShapeRelativeLayout mSrlPrescrInfo;
    protected FrameLayout mFlAddRelationDise;
    private CustomeRecyclerView mCrvRelationDise;
    private AppCompatRadioButton mSrbContraind;
    private AppCompatRadioButton mSrbNoContraind;
    private RadioGroup mRgContraind;
    private AppCompatRadioButton mSrbAllergy;
    private AppCompatRadioButton mSrbNoAllergy;
    private RadioGroup mRgAllergy;
    private AppCompatRadioButton mSrbAdrs;
    private AppCompatRadioButton mSrbNoAdrs;
    private RadioGroup mRgAdrs;
    private EditText mEtContent;
    private TextView mTvContentNum;
    private CustomeRecyclerView mCrlImglist;
    private CheckBox mCb;
    private FrameLayout mFlCb;
    private TextView mTvAgreeRule;
    private ShapeTextView mTvSubmit;

    private String orderNo, fixmedins_code, associatedDiseases, drugListJson;
    private CompressUploadSinglePicUtils_zhyf compressUploadSinglePicUtils;
    private InquiryInfoPicAdapter freeBackPicAdapter;//选择复诊图片适配器
    private RelationDiseAdapter_user relationDiseAdapter;//相关疾病适配器
    private AddRelationDisePop addRelationDisePop;//关联疾病适配器

    private String rule = "确认已在线下就诊并使用过所选药品,且无不良反应,确认问诊信息真实用于复诊开方,已阅读并确认《服务使用须知》《敏感个人信息处理规则》";
    private ClickableSpan clickableSpan01, clickableSpan02;
    private SpannableStringBuilder str2;

    private String isDoctor;
    private VisitorModel visitorModel;
    private Handler handler;
    private CustomCountDownTimer mCustomCountDownTimer;
    private String isPrescription;

    @Override
    public int getLayoutId() {
        return R.layout.activity_inquiry_info;
    }

    @Override
    protected void initImmersionBar() {
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mTitleBar).keyboardEnable(true);
        mImmersionBar.init();
    }

    @Override
    public void initView() {
        super.initView();
        mTv01Seize = findViewById(R.id.tv_01_seize);
        mTv02Seize = findViewById(R.id.tv_02_seize);
        mIvEnter01 = findViewById(R.id.iv_enter_01);

        mLine01 = findViewById(R.id.line_01);
        mIvPrescrHead = findViewById(R.id.iv_prescr_head);
        mTvPrescrInfo = findViewById(R.id.tv_prescr_info);
        mStvPrescrOneself = findViewById(R.id.stv_prescr_oneself);
        mTvPrescrCertNo = findViewById(R.id.tv_prescr_cert_no);
        mSrlPrescrInfo = findViewById(R.id.srl_prescr_info);

        mFlAddRelationDise = findViewById(R.id.fl_add_relation_dise);
        mCrvRelationDise = findViewById(R.id.crv_relation_dise);
        mSrbContraind = findViewById(R.id.srb_contraind);
        mSrbNoContraind = findViewById(R.id.srb_no_contraind);
        mRgContraind = findViewById(R.id.rg_contraind);
        mSrbAllergy = findViewById(R.id.srb_allergy);
        mSrbNoAllergy = findViewById(R.id.srb_no_allergy);
        mRgAllergy = findViewById(R.id.rg_allergy);
        mSrbAdrs = findViewById(R.id.srb_adrs);
        mSrbNoAdrs = findViewById(R.id.srb_no_adrs);
        mRgAdrs = findViewById(R.id.rg_adrs);
        mEtContent = findViewById(R.id.et_content);
        mTvContentNum = findViewById(R.id.tv_content_num);
        mCrlImglist = findViewById(R.id.crl_imglist);
        mCb = findViewById(R.id.cb);
        mFlCb = findViewById(R.id.fl_cb);
        mTvAgreeRule = findViewById(R.id.tv_agree_rule);
        mTvSubmit = findViewById(R.id.tv_submit);
    }

    @Override
    public void initData() {
        Intent intent = getIntent();
        orderNo = intent.getStringExtra("orderNo");
        fixmedins_code = intent.getStringExtra("fixmedins_code");
        associatedDiseases = intent.getStringExtra("associatedDiseases");
        drugListJson = intent.getStringExtra("drugListJson");
        super.initData();
        handler = new Handler(getMainLooper());
        //初始化获取图片
        compressUploadSinglePicUtils = new CompressUploadSinglePicUtils_zhyf();
        compressUploadSinglePicUtils.initChoosePic(this, true, this);

        //设置图片列表
        mCrlImglist.setHasFixedSize(true);
        mCrlImglist.setLayoutManager(new GridLayoutManager(this, 4, RecyclerView.VERTICAL, false));
        freeBackPicAdapter = new InquiryInfoPicAdapter();
        mCrlImglist.setAdapter(freeBackPicAdapter);
        //添加图片增加按钮数据
        ArrayList<FreeBackPicModel> freeBackPicModels = new ArrayList<>();
        FreeBackPicModel freeBackPic_addbtn = new FreeBackPicModel("-1", "", PIC_ADDBTN);
        freeBackPicModels.add(freeBackPic_addbtn);
        freeBackPicAdapter.setNewData(freeBackPicModels);

        //关联疾病
        mCrvRelationDise.setHasFixedSize(true);
        mCrvRelationDise.setLayoutManager(getLayoutManager());
        relationDiseAdapter = new RelationDiseAdapter_user();
        mCrvRelationDise.setAdapter(relationDiseAdapter);
        //关联疾病的数据
        if (!EmptyUtils.isEmpty(associatedDiseases)) {
            String[] diseasesDatas = associatedDiseases.split("∞#");
            ArrayList<RelationDiseModel> relationDises = new ArrayList<>();
            for (String diseasesData : diseasesDatas) {
                relationDises.add(new RelationDiseModel(diseasesData, false));
            }
            relationDiseAdapter.setNewData(relationDises);
        }

        setHighlightColor();

        Map<String, String> certNoinfo = CommonlyUsedDataUtils.getInstance().getBirthdayAgeSex(InsuredLoginUtils.getIdCardNo());
        String sexText = "1".equals(String.valueOf(certNoinfo.get("sex"))) ? "男" : "女";
        visitorModel = new VisitorModel(InsuredLoginUtils.getName(), InsuredLoginUtils.getIdCardNo(), String.valueOf(certNoinfo.get("sex")),
                sexText, certNoinfo.get("age"), InsuredLoginUtils.getAccessoryId(), InsuredLoginUtils.getAccessoryUrl(), "1");
        setUserInfo(visitorModel);
        showWaitDialog();
        getPresStatus();
        getInquiryInfo();
//        getHasDoctor();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        //分类 item点击事件
        freeBackPicAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                FreeBackPicModel itemData = freeBackPicAdapter.getItem(i);
                if (PIC_NOR == itemData.getItemType()) {//查看图片
                    ImageView ivPhoto = view.findViewById(R.id.iv_photo);
                    OpenImage.with(InquiryInfoActivity.this)
                            //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                            .setClickImageView(ivPhoto)
                            //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                            .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP, true)
                            //RecyclerView的数据
                            .setImageUrl(itemData.getImg(), MediaType.IMAGE)
                            //点击的ImageView所在数据的位置
                            .setClickPosition(0)
                            //开始展示大图
                            .show();
                } else if (PIC_ADDBTN == itemData.getItemType()) {//新增按钮
                    if (!mTvSubmit.isEnabled()) {
                        showShortToast("问诊信息已提交，不能在修改数据");
                        return;
                    }
                    KeyboardUtils.hideSoftInput(InquiryInfoActivity.this);
                    if (freeBackPicAdapter.getData().size() >= 6) {
                        showShortToast("最多上传5张图片");
                        return;
                    }

                    String type = CompressUploadSinglePicUtils_zhyf.PIC_VOUCHER1_TAG;
                    if (freeBackPicAdapter.getData().size() == 2) {
                        type = CompressUploadSinglePicUtils_zhyf.PIC_VOUCHER2_TAG;
                    } else if (freeBackPicAdapter.getData().size() == 3) {
                        type = CompressUploadSinglePicUtils_zhyf.PIC_VOUCHER3_TAG;
                    } else if (freeBackPicAdapter.getData().size() == 4) {
                        type = CompressUploadSinglePicUtils_zhyf.PIC_VOUCHER4_TAG;
                    } else if (freeBackPicAdapter.getData().size() == 5) {
                        type = CompressUploadSinglePicUtils_zhyf.PIC_VOUCHER5_TAG;
                    }

                    compressUploadSinglePicUtils.showChoosePicTypeDialog(TAG_SELECT_PIC_01, type);
                }
            }
        });

        freeBackPicAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                if (!mTvSubmit.isEnabled()) {
                    showShortToast("问诊信息已提交，不能在修改数据");
                    return;
                }
                switch (view.getId()) {
                    case R.id.iv_delete://删除图片
                        freeBackPicAdapter.getData().remove(i);
                        freeBackPicAdapter.notifyDataSetChanged();
                        break;
                }
            }
        });

        mEtContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int length = 200 - charSequence.length();
                mTvContentNum.setText(length + "/200");
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mFlAddRelationDise.setOnClickListener(this);
        //关联疾病 item点击
        relationDiseAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                if (!mTvSubmit.isEnabled()) {
                    showShortToast("问诊信息已提交，不能在修改数据");
                    return;
                }
                RelationDiseModel itemData = relationDiseAdapter.getItem(i);
                TextView tvWx = view.findViewById(R.id.tv_text);

                tvWx.setSelected(!itemData.isChoose());
                itemData.setChoose(!itemData.isChoose());
            }
        });

        mSrlPrescrInfo.setOnClickListener(this);
        mFlCb.setOnClickListener(this);
        mTvSubmit.setOnClickListener(this);
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        MyFlexboxLayoutManager flexboxLayoutManager = new MyFlexboxLayoutManager(this);
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        flexboxLayoutManager.setFlexWrap(FlexWrap.WRAP);
        flexboxLayoutManager.setJustifyContent(JustifyContent.FLEX_START);

        return flexboxLayoutManager;
    }

    //获取问诊信息数据
    private void getInquiryInfo() {
        InquiryInfoModel.sendInquiryInfoRequest(TAG, orderNo, new CustomerJsonCallBack<InquiryInfoModel>() {
            @Override
            public void onRequestError(InquiryInfoModel returnData, String msg) {
                showTipDialog(msg);
            }

            @Override
            public void onRequestSuccess(InquiryInfoModel returnData) {
                setInquiryInfo(returnData.getData());
            }
        });
    }

    //设置问诊信息
    private void setInquiryInfo(InquiryInfoModel.DataBean data) {
        if (data == null) return;
        //关联疾病的数据
        String associatedDiseasesData = data.getAssociatedDiseases();
        if (!EmptyUtils.isEmpty(associatedDiseasesData)) {
            //感冒∞#发烧  发烧∞#感冒
            String[] diseasesDatas = associatedDiseasesData.split("∞#");
            if (relationDiseAdapter.getData() != null) {
                for (int i = 0, size = diseasesDatas.length; i < size; i++) {
                    String diseasesData = diseasesDatas[i];

                    int index = 0;
                    for (RelationDiseModel datum : relationDiseAdapter.getData()) {
                        if (diseasesData.equals(datum.getText())) {
                            datum.setChoose(true);
                            break;
                        }
                        index++;
                        if (index >= relationDiseAdapter.getData().size()) {
                            relationDiseAdapter.getData().add(new RelationDiseModel(diseasesData, true));
                        }
                    }
                }
                relationDiseAdapter.notifyDataSetChanged();
            } else {
                ArrayList<RelationDiseModel> relationDises = new ArrayList<>();
                for (String diseasesData : diseasesDatas) {
                    relationDises.add(new RelationDiseModel(diseasesData, true));
                }
                relationDiseAdapter.setNewData(relationDises);
            }
        }
        //设置确认信息
        if ("1".equals(data.getIsTake())) {
            mSrbContraind.setChecked(true);
            mSrbNoContraind.setChecked(false);
        } else {
            mSrbContraind.setChecked(false);
            mSrbNoContraind.setChecked(true);
        }
        if ("1".equals(data.getIsAllergy())) {
            mSrbAllergy.setChecked(true);
            mSrbNoAllergy.setChecked(false);
        } else {
            mSrbAllergy.setChecked(false);
            mSrbNoAllergy.setChecked(true);
        }
        if ("1".equals(data.getIsUntowardReaction())) {
            mSrbAdrs.setChecked(true);
            mSrbNoAdrs.setChecked(false);
        } else {
            mSrbAdrs.setChecked(false);
            mSrbNoAdrs.setChecked(true);
        }

        //设置确诊疾病症状
        mEtContent.setText(EmptyUtils.strEmpty(data.getSymptom()));
        mEtContent.setEnabled(false);
        //设置复诊图片
        ArrayList<FreeBackPicModel> freeBackPicModels = new ArrayList<>();
        if (!EmptyUtils.isEmpty(data.getMedicalRecordAccessoryUrl1())) {
            freeBackPicModels.add(new FreeBackPicModel(data.getMedicalRecordAccessory1(), data.getMedicalRecordAccessoryUrl1(), PIC_NOR));
        }
        if (!EmptyUtils.isEmpty(data.getMedicalRecordAccessoryUrl2())) {
            freeBackPicModels.add(new FreeBackPicModel(data.getMedicalRecordAccessory2(), data.getMedicalRecordAccessoryUrl2(), PIC_NOR));
        }
        if (!EmptyUtils.isEmpty(data.getMedicalRecordAccessoryUrl3())) {
            freeBackPicModels.add(new FreeBackPicModel(data.getMedicalRecordAccessory3(), data.getMedicalRecordAccessoryUrl3(), PIC_NOR));
        }
        if (!EmptyUtils.isEmpty(data.getMedicalRecordAccessoryUrl4())) {
            freeBackPicModels.add(new FreeBackPicModel(data.getMedicalRecordAccessory4(), data.getMedicalRecordAccessoryUrl4(), PIC_NOR));
        }
        if (!EmptyUtils.isEmpty(data.getMedicalRecordAccessoryUrl5())) {
            freeBackPicModels.add(new FreeBackPicModel(data.getMedicalRecordAccessory5(), data.getMedicalRecordAccessoryUrl5(), PIC_NOR));
        }
        freeBackPicAdapter.addData(freeBackPicAdapter.getData().size() - 1, freeBackPicModels);
        //设置是否选择协议
        mCb.setChecked(true);
    }

    //获取机构是否有医师执业执照
    private void getHasDoctor() {
        HasDoctorModel.sendHasDoctorRequest(TAG, fixmedins_code, new CustomerJsonCallBack<HasDoctorModel>() {
            @Override
            public void onRequestError(HasDoctorModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(HasDoctorModel returnData) {
                hideWaitDialog();
                HasDoctorModel.DataBean data = returnData.getData();
                // 2022-11-08 判断该商家是否有执业医师
                if (data != null) {
                    isDoctor = data.getIsDoctor();
                    if ("1".equals(data.getIsDoctor())) {
                        mTvSubmit.setText("提交问诊信息");
                    } else {
                        mTvSubmit.setText("在线处方");
                    }
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        if (!mTvSubmit.isEnabled()) {
            showShortToast("问诊信息已提交，不能在修改数据");
            return;
        }
        switch (v.getId()) {
            case R.id.srl_prescr_info://选择就诊人
                Intent intent = new Intent(InquiryInfoActivity.this, SelectVisitorActivity.class);
                startActivityForResult(intent, new OnActivityCallback() {
                    @Override
                    public void onActivityResult(int resultCode, @Nullable Intent data) {
                        if (resultCode == RESULT_OK) {
                            visitorModel = data.getParcelableExtra("visitorModel");
                            setUserInfo(visitorModel);
                        }
                    }
                });
                break;
            case R.id.fl_add_relation_dise://添加关联疾病
                if (relationDiseAdapter == null) {
                    showShortToast("数据初始化错误,请重新进入页面");
                    return;
                }

                if (addRelationDisePop == null) {
                    addRelationDisePop = new AddRelationDisePop(InquiryInfoActivity.this, "添加已确诊疾病");
                    addRelationDisePop.setOnListener(new AddRelationDisePop.IOptionListener() {
                        @Override
                        public void onAgree(String data) {
                            if (EmptyUtils.isEmpty(data)) {
                                showShortToast("请输入疾病名称");
                                return;
                            }
                            addRelationDisePop.dismiss();

                            if (relationDiseAdapter.getData().size() >= 10) {
                                List<RelationDiseModel> datas = relationDiseAdapter.getData();
                                datas.add(0, new RelationDiseModel(data, true));
                                datas.remove(datas.size() - 1);

                                relationDiseAdapter.setNewData(datas);
                            } else {
                                relationDiseAdapter.getData().add(0, new RelationDiseModel(data, true));
                                relationDiseAdapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onCancle() {
                        }
                    });
                }
                addRelationDisePop.cleanData();
                addRelationDisePop.setOutSideDismiss(false).showPopupWindow();
                break;
            case R.id.fl_cb:
                if (mCb.isChecked()) {
                    mCb.setChecked(false);
                } else {
                    mCb.setChecked(true);
                }
                break;
            case R.id.tv_submit://提交问诊信息  或者 在线处方
                switch (mTvSubmit.getText().toString()) {
                    case "提交问诊信息":
                        onSubmit();
                        break;
                    case "跳转第三方开处方":
                        goThirdOpenPres();
                        break;
                }
                break;
        }
    }

    //设置用户信息
    private void setUserInfo(VisitorModel visitorModel) {
        GlideUtils.loadImg(visitorModel.getAccessoryUrl(), mIvPrescrHead, R.drawable.ic_mine_head, new CircleCrop());
        mTvPrescrInfo.setText(visitorModel.getName() + "   " + visitorModel.getSexText() + "    " + visitorModel.getAge() + "岁");
        mTvPrescrCertNo.setText(StringUtils.encryptionIDCard(visitorModel.getIdCardNo()));
        if ("1".equals(visitorModel.getIsOneself())) {
            mStvPrescrOneself.setText("本人");
        } else {
            mStvPrescrOneself.setText("亲戚");
        }


        mLine01.setVisibility(View.VISIBLE);
        mIvPrescrHead.setVisibility(View.VISIBLE);
        mTvPrescrInfo.setVisibility(View.VISIBLE);
        mStvPrescrOneself.setVisibility(View.VISIBLE);
        mTvPrescrCertNo.setVisibility(View.VISIBLE);
    }

    //提交问诊信息  或者 在线处方
    private void onSubmit() {
        if (visitorModel == null) {
            showShortToast("请选择就诊人");
            return;
        }

        List<RelationDiseModel> datas = relationDiseAdapter.getData();
        String associatedDiseases = "";
        RelationDiseModel itemData;
        for (int i = 0, size = datas.size(); i < size; i++) {
            itemData = datas.get(i);
            if (itemData.isChoose()) {
                associatedDiseases += itemData.getText() + "∞#";
            }
        }

        if (EmptyUtils.isEmpty(associatedDiseases)) {
            showShortToast("请选择已确诊的相关疾病");
            return;
        }
        associatedDiseases = associatedDiseases.substring(0, associatedDiseases.length() - 2);

        if (!mCb.isChecked()) {
            showShortToast("请阅读并勾选底部协议");
            return;
        }

        //是否服用过该药品且无相关禁忌症
        String contraind = "1";
        if (mRgContraind.getCheckedRadioButtonId() == R.id.srb_no_contraind) {
            contraind = "0";
        }

        //是否对该药物过敏
        String allergy = "0";
        if (mRgAllergy.getCheckedRadioButtonId() == R.id.srb_allergy) {
            allergy = "1";
        }

        //有无不良反应
        String adrs = "0";
        if (mRgAdrs.getCheckedRadioButtonId() == R.id.srb_adrs) {
            adrs = "1";
        }

        List<FreeBackPicModel> picDatas = freeBackPicAdapter.getData();

        showWaitDialog();
        BaseModel.sendAddOnlineInquiryRequest_user(TAG, orderNo, visitorModel.getName(), visitorModel.getIdCardNo(), visitorModel.getSex(),
                visitorModel.getAge(), associatedDiseases, contraind, allergy, adrs, mEtContent.getText().toString(),
                picDatas == null ? "" : (picDatas.size() > 1 ? picDatas.get(0).getId() : ""), picDatas == null ? "" : (picDatas.size() > 2 ? picDatas.get(1).getId() : ""),
                picDatas == null ? "" : (picDatas.size() > 3 ? picDatas.get(2).getId() : ""), picDatas == null ? "" : (picDatas.size() > 4 ? picDatas.get(3).getId() : ""),
                picDatas == null ? "" : (picDatas.size() > 5 ? picDatas.get(4).getId() : ""), isDoctor, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast(returnData.getMsg());
                        if (mCustomCountDownTimer == null) {
                            //计算  定时器时间==120s-已过去多少时间
                            setTimeStart(120 * 1000);
                            mTvSubmit.setEnabled(false);
                        }
                        pollingMethod();
                    }
                });
    }

    //轮询
    private void pollingMethod() {
        if (handler == null) return;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getPresStatus();
            }
        }, 5000);
    }

    //轮询-获取处方问诊状态
    private void getPresStatus() {
        QueryPresStatusModel.sendQueryPresStatusRequest(TAG, orderNo, new CustomerJsonCallBack<QueryPresStatusModel>() {
            @Override
            public void onRequestError(QueryPresStatusModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(QueryPresStatusModel returnData) {
                hideWaitDialog();
                QueryPresStatusModel.DataBean data = returnData.getData();
                if (data != null) {
                    // (0 不开处方 1、待开处方 2、已开处方3、待医师确认处方4、第三方开处方 5.未提交问诊信息）
                    isPrescription=data.getIsPrescription();
                    if ("1".equals(isPrescription)) {//待开处方
                        pollingMethod();

                        if (mCustomCountDownTimer == null) {
                            //计算  定时器时间==30分钟-已过去多少时间
                            long pastTimes = new BigDecimal(DateUtil.dateToStamp2(returnData.getServer_time())).subtract(new BigDecimal(DateUtil.dateToStamp2(data.getCreateDT()))).longValue();
                            setTimeStart(120 * 1000 - pastTimes);
                            mTvSubmit.setEnabled(false);
                        }
                    } else if ("2".equals(isPrescription)) {//已开处方
                        setResult(RESULT_OK);
                        goFinish();
                    } else if ("3".equals(isPrescription)) {//待医师确认处方
                        pollingMethod();
                        //防止内存泄漏
                        if (null != mCustomCountDownTimer) {
                            mCustomCountDownTimer.stop();
                            mCustomCountDownTimer = null;
                        }
                        mTvSubmit.setText("处方开立中");
                        mTvSubmit.setEnabled(false);
                    } else if ("4".equals(isPrescription)) {//第三方开处方
                        //防止内存泄漏
                        if (null != mCustomCountDownTimer) {
                            mCustomCountDownTimer.stop();
                            mCustomCountDownTimer = null;
                        }
                        // 2024-03-29 调用跳转第三方开处方方法
                        goThirdOpenPres();
                    } else if ("5".equals(isPrescription)) {//未提交问诊信息
                        // 2024-03-29 调用跳转第三方开处方方法
                        mTvSubmit.setText("提交问诊信息");
                        mTvSubmit.setEnabled(true);
                    }
                }
            }
        });
    }

    //开启定时器  millisecond:倒计时的时间 1:待付款倒计时
    protected void setTimeStart(long millisecond) {
        //防止内存泄漏
        if (null != mCustomCountDownTimer) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        mCustomCountDownTimer = new CustomCountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long S = millisUntilFinished / 1000;
                mTvSubmit.setText("正在分配医师 " + S + "s");
            }

            @Override
            public void onFinish() {
                goThirdOpenPres();
            }
        };

        mCustomCountDownTimer.start();
    }

    //跳转第三方开处方
    private void goThirdOpenPres() {
        mTvSubmit.setText("跳转第三方开处方");
        mTvSubmit.setEnabled(true);
        if (Constants.Configure.IS_POS) {
            showShortToast("pos机不支持跳转微信小程序开处方");
            return;
        }
        //[{"erpCode":"drp_00016","count":1}]
        if (!EmptyUtils.isEmpty(drugListJson)) {
            WXUtils.goSmallRoutine(InquiryInfoActivity.this, Constants.Configure.WEZ_APPID,
                    Constants.Configure.WEZ_PATH + "&orderId=" + orderNo + "&drugList=" + drugListJson);
        }
    }

    //--------------------------选择图片----------------------------------------------------
    @Override
    public void onOssUpResult(String mTag, String accessoryId, String url, int width, int height) {
        switch (mTag) {
            case TAG_SELECT_PIC_01://banner 1号位
                selectPicSuccess(accessoryId, url, width, height);
                break;
        }
    }

    @Override
    public void onShowWait() {
        showWaitDialog();
    }

    @Override
    public void onHideWait() {
        hideWaitDialog();
    }

    private void selectPicSuccess(String accessoryId, String url, int width, int height) {
        if (!EmptyUtils.isEmpty(url)) {
            FreeBackPicModel freeBackPic = new FreeBackPicModel(accessoryId, url, PIC_NOR);

            freeBackPicAdapter.addData(freeBackPicAdapter.getData().size() - 1, freeBackPic);
        } else {
            showShortToast("图片上传失败，请重新上传");
        }
    }

    //--------------------------选择图片----------------------------------------------------


    @Override
    public void onBackPressed() {
        if ("1".equals(isPrescription)){
            showTipDialog("正在分配医师，请稍等");
        }else if ("3".equals(isPrescription)){
            showTipDialog("处方开立中，请稍等");
        }else {
            super.onBackPressed();
        }
    }

    //设置文字高亮
    private void setHighlightColor() {
        //确认已在线下就诊并使用过所选药品,且无不良反应,确认问诊信息真实用于复诊开方,已阅读并确认《服务使用须知》《敏感个人信息处理规则》
        // 响应点击事件的话必须设置以下属性
        mTvAgreeRule.setMovementMethod(LinkMovementMethod.getInstance());
        mTvAgreeRule.setHighlightColor(ContextCompat.getColor(RxTool.getContext(), android.R.color.transparent));
        String s3 = rule.substring(0, 45);
        String s4 = rule.substring(45, 53);
        String s5 = rule.substring(53, 65);

        clickableSpan01 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                MyWebViewActivity.newIntance(InquiryInfoActivity.this, "服务使用须知", Constants.BASE_URL + Constants.WebUrl.STORESERVICEINSTRUCTIONS_URL, true, false);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setColor(Color.parseColor("#4870E0"));
                ds.setUnderlineText(false);
            }
        };

        clickableSpan02 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                MyWebViewActivity.newIntance(InquiryInfoActivity.this, "敏感个人信息处理规则", Constants.BASE_URL + Constants.WebUrl.STORESENSITIVERULES_URL, true, false);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setColor(Color.parseColor("#4870E0"));
                ds.setUnderlineText(false);
            }
        };
        int textSize = getResources().getDimensionPixelSize(R.dimen.dp_24);

        str2 = new SpanUtils().append(s3).setFontSize(textSize, false)
                .append(s4).setFontSize(textSize, false).setForegroundColor(Color.parseColor("#4870E0"))
                .setClickSpan(clickableSpan01)
                .append(s5).setFontSize(textSize, false).setForegroundColor(Color.parseColor("#4870E0"))
                .setClickSpan(clickableSpan02)
                .create();
        mTvAgreeRule.setText(str2);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compressUploadSinglePicUtils != null) {
            compressUploadSinglePicUtils.onDeatoryUtils();
        }
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
        if (mCustomCountDownTimer != null) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        if (addRelationDisePop != null) {
            addRelationDisePop.onCleanListener();
            addRelationDisePop.onDestroy();
        }

        if (clickableSpan01 != null) {
            clickableSpan01 = null;
        }

        if (clickableSpan02 != null) {
            clickableSpan02 = null;
        }

        if (str2 != null) {
            str2.clear();
            str2.clearSpans();
        }
    }

    public static void newIntance(Context context, String orderNo, String fixmedins_code, String associatedDiseases, String drugListJson) {
        Intent intent = new Intent(context, InquiryInfoActivity.class);
        intent.putExtra("orderNo", orderNo);
        intent.putExtra("fixmedins_code", fixmedins_code);
        intent.putExtra("associatedDiseases", associatedDiseases);
        intent.putExtra("drugListJson", drugListJson);
        context.startActivity(intent);
    }
}
