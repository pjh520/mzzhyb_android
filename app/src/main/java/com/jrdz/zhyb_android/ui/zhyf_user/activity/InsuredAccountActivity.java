package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.model.InsuredAccountModel;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-06-14
 * 描    述：我的账户
 * ================================================
 */
public class InsuredAccountActivity extends BaseActivity {
    private LinearLayout mLlEnterpriseBalance;
    private TextView mTvTag;
    private TextView mTvEnterpriseBalance;
    private ShapeTextView mTvTransactionDetail;


    @Override
    public int getLayoutId() {
        return R.layout.activity_insured_account;
    }

    @Override
    public void initView() {
        super.initView();
        mLlEnterpriseBalance = findViewById(R.id.ll_enterprise_balance);
        mTvTag = findViewById(R.id.tv_tag);
        mTvEnterpriseBalance = findViewById(R.id.tv_enterprise_balance);
        mTvTransactionDetail = findViewById(R.id.tv_transaction_detail);
    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        getPagerData();

        if ("1".equals(InsuredLoginUtils.getIsEnterpriseFundPay())){
            mLlEnterpriseBalance.setVisibility(View.VISIBLE);
            mTvTag.setVisibility(View.VISIBLE);
        }else {
            mLlEnterpriseBalance.setVisibility(View.GONE);
            mTvTag.setVisibility(View.GONE);
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTvTransactionDetail.setOnClickListener(this);
    }

    //获取页面数据
    private void getPagerData() {
        InsuredAccountModel.sendInsuredAccountRequest(TAG, InsuredLoginUtils.getIdCardNo(), new CustomerJsonCallBack<InsuredAccountModel>() {
            @Override
            public void onRequestError(InsuredAccountModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(InsuredAccountModel returnData) {
                hideWaitDialog();
                if (returnData.getData()!=null){
                    mTvEnterpriseBalance.setText(EmptyUtils.strEmpty(returnData.getData().getEnterpriseFundAmount()));
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.tv_transaction_detail://交易明细
                TransactionDetailActivity.newIntance(InsuredAccountActivity.this);
                break;
        }

    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, InsuredAccountActivity.class);
        context.startActivity(intent);
    }
}
