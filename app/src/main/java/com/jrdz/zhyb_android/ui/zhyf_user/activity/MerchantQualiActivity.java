package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxClipboardTool;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;

import java.math.BigDecimal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-13
 * 描    述：商家资质 页面
 * ================================================
 */
public class MerchantQualiActivity extends BaseActivity{
    private ImageView mIvHorizontalPic01;
    private ImageView mIvHorizontalPic02;
    private ImageView mIvHorizontalPic03;

    private String businessLicenseAccessoryUrl,drugBLAccessoryUrl,medicalDeviceBLAccessoryUrl;

    @Override
    public int getLayoutId() {
        return R.layout.activity_merchant_quali;
    }

    @Override
    public void initView() {
        super.initView();

        mIvHorizontalPic01 = findViewById(R.id.iv_horizontal_pic01);
        mIvHorizontalPic02 = findViewById(R.id.iv_horizontal_pic02);
        mIvHorizontalPic03 = findViewById(R.id.iv_horizontal_pic03);
    }

    @Override
    public void initData() {
        Intent intent = getIntent();
        businessLicenseAccessoryUrl=intent.getStringExtra("businessLicenseAccessoryUrl");
        drugBLAccessoryUrl=intent.getStringExtra("drugBLAccessoryUrl");
        medicalDeviceBLAccessoryUrl=intent.getStringExtra("medicalDeviceBLAccessoryUrl");
        super.initData();

        //营业执照
        if (!EmptyUtils.isEmpty(businessLicenseAccessoryUrl)) {
            GlideUtils.getImageWidHeig(this,businessLicenseAccessoryUrl, new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    ShapeFrameLayout.LayoutParams layoutParams = (ShapeFrameLayout.LayoutParams) mIvHorizontalPic01.getLayoutParams();
                    if (radio >= 2.029) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                    }
                    mIvHorizontalPic01.setTag(R.id.tag_2,businessLicenseAccessoryUrl);
                    mIvHorizontalPic01.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(businessLicenseAccessoryUrl, mIvHorizontalPic01);
                }
            });
        }

        //药品经营许可证
        if (!EmptyUtils.isEmpty(drugBLAccessoryUrl)) {
            GlideUtils.getImageWidHeig(this,drugBLAccessoryUrl, new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    ShapeFrameLayout.LayoutParams layoutParams = (ShapeFrameLayout.LayoutParams) mIvHorizontalPic02.getLayoutParams();
                    if (radio >= 2.029) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                    }
                    mIvHorizontalPic02.setTag(R.id.tag_2,drugBLAccessoryUrl);
                    mIvHorizontalPic02.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(drugBLAccessoryUrl, mIvHorizontalPic02);
                }
            });
        }

        //医疗器械经营许可证
        if (!EmptyUtils.isEmpty(medicalDeviceBLAccessoryUrl)) {
            GlideUtils.getImageWidHeig(this,medicalDeviceBLAccessoryUrl, new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    ShapeFrameLayout.LayoutParams layoutParams = (ShapeFrameLayout.LayoutParams) mIvHorizontalPic03.getLayoutParams();
                    if (radio >= 2.029) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_690));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_690))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_340) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_340));
                    }

                    mIvHorizontalPic03.setTag(R.id.tag_2,medicalDeviceBLAccessoryUrl);
                    mIvHorizontalPic03.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(medicalDeviceBLAccessoryUrl, mIvHorizontalPic03);
                }
            });
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mIvHorizontalPic01.setOnClickListener(this);
        mIvHorizontalPic02.setOnClickListener(this);
        mIvHorizontalPic03.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.iv_horizontal_pic01://banner 1号位
                showBigPic(mIvHorizontalPic01,(String)mIvHorizontalPic01.getTag(R.id.tag_2));
                break;
            case R.id.iv_horizontal_pic02://banner 2号位
                showBigPic(mIvHorizontalPic02,(String)mIvHorizontalPic02.getTag(R.id.tag_2));
                break;
            case R.id.iv_horizontal_pic03://banner 3号位
                showBigPic(mIvHorizontalPic03,(String)mIvHorizontalPic03.getTag(R.id.tag_2));
                break;
        }
    }

    //展示大图
    private void showBigPic(ImageView imageView,String imageUrl) {
        if (EmptyUtils.isEmpty(imageUrl)){
            return;
        }
        OpenImage.with(MerchantQualiActivity.this)
                //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                .setClickImageView(imageView)
                //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                //RecyclerView的数据
                .setImageUrl(imageUrl, MediaType.IMAGE)
                //点击的ImageView所在数据的位置
                .setClickPosition(0)
                //开始展示大图
                .show();
    }


    public static void newIntance(Context context, String businessLicenseAccessoryUrl, String drugBLAccessoryUrl, String medicalDeviceBLAccessoryUrl) {
        Intent intent = new Intent(context, MerchantQualiActivity.class);
        intent.putExtra("businessLicenseAccessoryUrl", businessLicenseAccessoryUrl);
        intent.putExtra("drugBLAccessoryUrl", drugBLAccessoryUrl);
        intent.putExtra("medicalDeviceBLAccessoryUrl", medicalDeviceBLAccessoryUrl);
        context.startActivity(intent);
    }
}
