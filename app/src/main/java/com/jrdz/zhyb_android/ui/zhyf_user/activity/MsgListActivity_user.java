package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.MsgListActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.MsgListAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.MsgListModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-18
 * 描    述：消息列表页面
 * ================================================
 */
public class MsgListActivity_user extends MsgListActivity {

    @Override
    public void getData() {
        MsgListModel.sendUserMsgListRequest(TAG, String.valueOf(mPageNum), "20", new CustomerJsonCallBack<MsgListModel>() {
            @Override
            public void onRequestError(MsgListModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(MsgListModel returnData) {
                hideRefreshView();
                List<MsgListModel.DataBean> datas = returnData.getData();
                if (mAdapter!=null&&datas != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(datas);
                    } else {
                        mAdapter.addData(datas);
                        mAdapter.loadMoreComplete();
                    }

                    if (datas.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        MsgListModel.DataBean itemData = ((MsgListAdapter) adapter).getItem(position);
        if ("0".equals(itemData.getIsRead())) {
            showWaitDialog();
            BaseModel.sendUserIsReadMessageRequest(TAG, itemData.getMessageId(), "0", new CustomerJsonCallBack<BaseModel>() {
                @Override
                public void onRequestError(BaseModel returnData, String msg) {
                    hideWaitDialog();
                    showShortToast(msg);
                }

                @Override
                public void onRequestSuccess(BaseModel returnData) {
                    hideWaitDialog();
                    //接口请求成功
                    itemData.setIsRead("1");
                    adapter.notifyItemChanged(position);
                }
            });
        }
    }

    @Override
    public void rightTitleViewClick() {
        // 2022-10-18 请求接口 设置全部已读
        showWaitDialog();
        BaseModel.sendUserIsReadMessageRequest(TAG, "", "1", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                //接口请求成功

                List<MsgListModel.DataBean> datas = ((MsgListAdapter) mAdapter).getData();
                for (MsgListModel.DataBean data : datas) {
                    data.setIsRead("1");
                }
                mAdapter.notifyDataSetChanged();
                showShortToast("全部已读设置成功");
            }
        });
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, MsgListActivity_user.class);
        context.startActivity(intent);
    }
}
