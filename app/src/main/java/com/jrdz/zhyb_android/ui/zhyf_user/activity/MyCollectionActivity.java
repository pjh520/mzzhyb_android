package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.frame.compiler.utils.viewPage.BaseViewPagerAndTabsAdapter_new;
import com.frame.compiler.widget.CustomViewPager;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.ProductCollectionFragment;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.ShopCollectionFragment;

import java.util.Arrays;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-01
 * 描    述：我的收藏
 * ================================================
 */
public class MyCollectionActivity extends BaseActivity {
    private SlidingTabLayout mStbOrder;
    private CustomViewPager mVpOrder;

    String[] titles = new String[]{"店铺收藏","商品收藏"};

    @Override
    public int getLayoutId() {
        return R.layout.activity_my_collection;
    }

    @Override
    public void initView() {
        super.initView();
        mStbOrder = findViewById(R.id.stb_order);
        mVpOrder = findViewById(R.id.vp_order);
    }

    @Override
    public void initData() {
        super.initData();

        initTabLayout();
    }

    //初始化tablayout 跟 viewpager
    private void initTabLayout() {
        BaseViewPagerAndTabsAdapter_new baseViewPagerAndTabsAdapter_new=new BaseViewPagerAndTabsAdapter_new(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position){
                    case 0://店铺收藏
                        return ShopCollectionFragment.newIntance();
                    case 1://商品收藏
                        return ProductCollectionFragment.newIntance();
                    default:
                        return ShopCollectionFragment.newIntance();
                }
            }
        };

        baseViewPagerAndTabsAdapter_new.setData(Arrays.asList(titles));
        mVpOrder.setAdapter(baseViewPagerAndTabsAdapter_new);
        mStbOrder.setViewPager(mVpOrder);
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, MyCollectionActivity.class);
        context.startActivity(intent);
    }
}
