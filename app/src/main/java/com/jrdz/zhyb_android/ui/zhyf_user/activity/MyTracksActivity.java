package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.DateUtil;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.MyTracksAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.model.MyTracksModel;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-02
 * 描    述：我的足迹
 * ================================================
 */
public class MyTracksActivity extends BaseRecyclerViewActivity {
    private View mLine;
    private LinearLayout mLlBottom;
    private FrameLayout mFlCbAll;
    private CheckBox mCbAll;
    private TextView mTvAll;
    private ShapeTextView mStvDelete;

    private boolean isEdit = false;//是否在管理状态
    private CustomerDialogUtils customerDialogUtils;
    private String deleteIds;

    @Override
    public int getLayoutId() {
        return R.layout.activity_mytracks;
    }

    @Override
    public void initView() {
        super.initView();
        mLine = findViewById(R.id.line);
        mLlBottom = findViewById(R.id.ll_bottom);
        mFlCbAll = findViewById(R.id.fl_cb_all);
        mCbAll = findViewById(R.id.cb_all);
        mTvAll = findViewById(R.id.tv_all);
        mStvDelete = findViewById(R.id.stv_delete);
    }

    @Override
    public void initAdapter() {
        mAdapter = new MyTracksAdapter();

        mAdapter.setSpanSizeLookup(new BaseQuickAdapter.SpanSizeLookup() {
            @Override
            public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
                return ((MyTracksAdapter) mAdapter).getItem(position).getSpanSize();
            }
        });
    }

    @Override
    public void initData() {
        super.initData();

        setRightTitleView("管理");

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();

        mFlCbAll.setOnClickListener(this);
        mStvDelete.setOnClickListener(this);
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        return new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void getData() {
        super.getData();

        MyTracksModel.sendMyTracksRequest_user(TAG, String.valueOf(mPageNum), "20", new CustomerJsonCallBack<MyTracksModel>() {
            @Override
            public void onRequestError(MyTracksModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(MyTracksModel returnData) {
                hideRefreshView();
                if (isFinishing())return;
                List<MyTracksModel.DataBean> datas = returnData.getData();
                ArrayList<MyTracksModel.DataBean> myTracksModels_muilt = new ArrayList<>();
                if (datas != null&&!datas.isEmpty()) {
                    //判断第一页第一条是否是今日
                    int parentPos = 0;
                    if (mPageNum == 0) {
                        //设置第一条title 以及第一条content数据
                        String today = DateUtil.getStringDate("yyyyMMdd");
                        MyTracksModel.DataBean firstData = datas.get(0);
                        if (today.equals(firstData.getTrackTime().replaceAll("-", ""))) {
                            myTracksModels_muilt.add(new MyTracksModel.DataBean("今日",firstData.getTrackTime(),mCbAll.isChecked(),
                                    2, MyTracksModel.TITLE_TAG));
                        } else {
                            myTracksModels_muilt.add(new MyTracksModel.DataBean(firstData.getTrackTime(), firstData.getTrackTime(), mCbAll.isChecked(),
                                    2, MyTracksModel.TITLE_TAG));
                        }

                        if (mCbAll.isChecked()) {
                            firstData.setChoose(true);
                        }

                        firstData.setParentPos(parentPos);
                        firstData.setSpanSize(1);
                        firstData.setItemType(MyTracksModel.CONTENT_TAG);
                        myTracksModels_muilt.add(firstData);

                        //从第二条开始 判断日期是否跟上一条一样 若不一样 则新建头部 若一样 则不需要
                        MyTracksModel.DataBean preItemData;
                        MyTracksModel.DataBean itemData;
                        for (int i = 1, size = datas.size(); i < size; i++) {
                            preItemData = datas.get(i - 1);
                            itemData = datas.get(i);

                            if (!preItemData.getTrackTime().replaceAll("-", "").equals(itemData.getTrackTime().replaceAll("-", ""))) {
                                myTracksModels_muilt.add(new MyTracksModel.DataBean(itemData.getTrackTime(), itemData.getTrackTime(), mCbAll.isChecked(),
                                        2, MyTracksModel.TITLE_TAG));
                                parentPos = myTracksModels_muilt.size() - 1;
                            }
                            if (mCbAll.isChecked()) {
                                itemData.setChoose(true);
                            }
                            itemData.setParentPos(parentPos);
                            itemData.setSpanSize(1);
                            itemData.setItemType(MyTracksModel.CONTENT_TAG);
                            myTracksModels_muilt.add(itemData);
                        }

                        mAdapter.setNewData(myTracksModels_muilt);

                        if (myTracksModels_muilt.size() <= 0) {
                            mAdapter.isUseEmpty(true);
                        }
                    } else {
                        //从第二页开始 那么先要判断第一条 是否跟上一页的最后一条是否一样
                        //当前列表最后一条数据
                        MyTracksModel.DataBean currentLastData = ((MyTracksAdapter) mAdapter).getItem(mAdapter.getData().size() - 1);
                        parentPos = currentLastData.getParentPos();
                        //获取最后一条的头部数据
                        MyTracksModel.DataBean parentLastData = ((MyTracksAdapter) mAdapter).getItem(parentPos);

                        MyTracksModel.DataBean firstData = datas.get(0);
                        //判断时间是否一样 若不一样 则需要新增一个头部   若一样 则需要判断该项头部是否全选
                        if (!currentLastData.getTrackTime().replaceAll("-", "").equals(firstData.getTrackTime().replaceAll("-", ""))) {
                            myTracksModels_muilt.add(new MyTracksModel.DataBean(firstData.getTrackTime(), firstData.getTrackTime(), mCbAll.isChecked(),
                                    2, MyTracksModel.TITLE_TAG));
                            parentPos = myTracksModels_muilt.size() - 1;
                        }

                        if (mCbAll.isChecked()) {
                            firstData.setChoose(true);
                        } else {
                            if (parentPos == currentLastData.getParentPos() && parentLastData.isChoose()) {
                                firstData.setChoose(true);
                            }
                        }

                        firstData.setParentPos(parentPos);
                        firstData.setSpanSize(1);
                        firstData.setItemType(MyTracksModel.CONTENT_TAG);
                        myTracksModels_muilt.add(firstData);

                        //从第二条开始 判断日期是否跟上一条一样 若不一样 则新建头部 若一样 则不需要
                        MyTracksModel.DataBean preItemData;
                        MyTracksModel.DataBean itemData;
                        for (int i = 1, size = datas.size(); i < size; i++) {
                            preItemData = datas.get(i - 1);
                            itemData = datas.get(i);

                            if (!preItemData.getTrackTime().replaceAll("-", "").equals(itemData.getTrackTime().replaceAll("-", ""))) {
                                myTracksModels_muilt.add(new MyTracksModel.DataBean(itemData.getTrackTime(), itemData.getTrackTime(), mCbAll.isChecked(),
                                        2, MyTracksModel.TITLE_TAG));
                                parentPos = myTracksModels_muilt.size() - 1;
                            }

                            if (mCbAll.isChecked()) {
                                itemData.setChoose(true);
                            } else {
                                if (parentPos == currentLastData.getParentPos() && parentLastData.isChoose()) {
                                    itemData.setChoose(true);
                                }
                            }

                            itemData.setParentPos(parentPos);
                            itemData.setSpanSize(1);
                            itemData.setItemType(MyTracksModel.CONTENT_TAG);
                            myTracksModels_muilt.add(itemData);
                        }

                        mAdapter.addData(myTracksModels_muilt);
                        mAdapter.loadMoreComplete();
                    }
                }else {
                    if (mPageNum==0){
                        mAdapter.setNewData(myTracksModels_muilt);
                    }
                    mAdapter.loadMoreComplete();
                    mAdapter.loadMoreEnd();
                }
            }
        });
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        MyTracksModel.DataBean itemData = ((MyTracksAdapter) adapter).getItem(position);

        switch (view.getId()) {
            case R.id.fl_date_all_select://头部时间选中或者取消
                ImageView ivDateAllSelect = view.findViewById(R.id.iv_date_all_select);
                if (itemData.isChoose()) {//取消选中
                    ivDateAllSelect.setImageResource(R.drawable.ic_product_select_nor);
                    itemData.setChoose(false);
                    //取消当前日期的全选
                    onCancleCurrenDateAllChoose(itemData.getTrackTime());
                    //取消全选
                    mCbAll.setChecked(false);
                } else {//选中
                    ivDateAllSelect.setImageResource(R.drawable.ic_product_select_pre);
                    itemData.setChoose(true);
                    //当前日期全选
                    onCurrenDateAllChoose(itemData.getTrackTime());
                    //判断是否全选
                    onAllChoose();
                }
                break;
            case R.id.fl_product_select://商品选中或者取消
                ImageView ivProductSelect = view.findViewById(R.id.iv_product_select);
                if (itemData.isChoose()) {//取消选中
                    ivProductSelect.setImageResource(R.drawable.ic_product_select_nor);
                    itemData.setChoose(false);
                    //取消当前日期的全选
                    onCancleCurrenDateChoose(itemData.getTrackTime());
                    //取消全选
                    mCbAll.setChecked(false);
                } else {//选中
                    ivProductSelect.setImageResource(R.drawable.ic_product_select_pre);
                    itemData.setChoose(true);
                    //判断当前日期是否全选
                    onCurrenDateIsAllChoose(itemData.getTrackTime());
                    //判断是否全选
                    onAllChoose();
                }
                break;
        }
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);

        MyTracksModel.DataBean itemData = ((MyTracksAdapter) adapter).getItem(position);
        GoodDetailActivity.newIntance(MyTracksActivity.this,itemData.getGoodsNo(),itemData.getFixmedins_code(),itemData.getGoodsName());
    }

    @Override
    public void rightTitleViewClick() {
        if (isEdit) {
            setRightTitleView("管理");
            isEdit = false;
            mLine.setVisibility(View.GONE);
            mLlBottom.setVisibility(View.GONE);

            cleanChoose();
            ((MyTracksAdapter) mAdapter).setIsEditStatus(false);
            mCbAll.setChecked(false);
        } else {
            setRightTitleView("完成");
            isEdit = true;

            mLine.setVisibility(View.VISIBLE);
            mLlBottom.setVisibility(View.VISIBLE);

            ((MyTracksAdapter) mAdapter).setIsEditStatus(true);
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_cb_all:
                if (mCbAll.isChecked()) {//取消全选
                    mCbAll.setChecked(false);
                    cleanChoose();

                    mAdapter.notifyDataSetChanged();
                } else {//全选
                    mCbAll.setChecked(true);
                    allChoose();

                    mAdapter.notifyDataSetChanged();
                }
                break;
            case R.id.stv_delete://删除
                deleteIds = "";
                List<MyTracksModel.DataBean> allData = ((MyTracksAdapter) mAdapter).getData();
                for (MyTracksModel.DataBean allDatum : allData) {
                    if (MyTracksModel.CONTENT_TAG == allDatum.getItemType() && allDatum.isChoose()) {
                        deleteIds += allDatum.getTrackId() + ",";
                    }
                }
                if (EmptyUtils.isEmpty(deleteIds)) {
                    showShortToast("请选择要删除的商品");
                    return;
                }


                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(MyTracksActivity.this, "提示", "确定删除所选产品吗？", "取消", "确定",
                        new CustomerDialogUtils.IDialogListener() {

                            @Override
                            public void onBtn01Click() {

                            }

                            @Override
                            public void onBtn02Click() {
                                onDelete(deleteIds.substring(0, deleteIds.length() - 1));
                            }
                        });
                break;
        }
    }

    //删除
    private void onDelete(String deleteText) {
        // 2022-11-02 请求接口删除
        showWaitDialog();
        BaseModel.sendDelTrackRequest(TAG, deleteText, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                //删除成功
                showShortToast("删除成功");
                onRefresh(mRefreshLayout);
            }
        });
    }

    //取消标题中当前日期的选中
    private void onCancleCurrenDateChoose(String chooseDate) {
        chooseDate = chooseDate.replaceAll("-", "");
        List<MyTracksModel.DataBean> datas = ((MyTracksAdapter) mAdapter).getData();

        MyTracksModel.DataBean data;
        String date = "";
        for (int i = 0, size = datas.size(); i < size; i++) {
            data = datas.get(i);
            date = data.getTrackTime().replaceAll("-", "");
            //判断是否是头部 日期是否一样
            if (MyTracksModel.TITLE_TAG == data.getItemType() && chooseDate.equals(date)) {
                data.setChoose(false);
                mAdapter.notifyItemChanged(i);
                break;
            }
        }
    }

    //判断当前日期是否全选
    private void onCurrenDateIsAllChoose(String chooseDate) {
        chooseDate = chooseDate.replaceAll("-", "");
        List<MyTracksModel.DataBean> datas = ((MyTracksAdapter) mAdapter).getData();

        //记录当前日期的标题项
        MyTracksModel.DataBean titleData = null;
        //记录当前日期的标题项位置
        int pos = 0;

        MyTracksModel.DataBean data;
        String date = "";
        boolean isAllChoose = true;
        for (int i = 0, size = datas.size(); i < size; i++) {
            data = datas.get(i);
            date = data.getTrackTime().replaceAll("-", "");
            //判断是否是头部 日期是否一样
            if (chooseDate.equals(date) && MyTracksModel.CONTENT_TAG == data.getItemType() && !data.isChoose()) {
                isAllChoose = false;
            }

            //判断是否是头部 日期是否一样
            if (MyTracksModel.TITLE_TAG == data.getItemType() && chooseDate.equals(date)) {
                titleData = data;
                pos = i;
            }
        }

        if (isAllChoose && titleData != null) {
            titleData.setChoose(true);
            mAdapter.notifyItemChanged(pos);
        }
    }

    //取消当前日期的全选
    private void onCancleCurrenDateAllChoose(String chooseDate) {
        chooseDate = chooseDate.replaceAll("-", "");
        List<MyTracksModel.DataBean> datas = ((MyTracksAdapter) mAdapter).getData();

        MyTracksModel.DataBean data;
        String date = "";
        for (int i = 0, size = datas.size(); i < size; i++) {
            data = datas.get(i);
            date = data.getTrackTime().replaceAll("-", "");
            //判断是否是头部 日期是否一样
            if (chooseDate.equals(date) && MyTracksModel.CONTENT_TAG == data.getItemType() && data.isChoose()) {
                data.setChoose(false);
                mAdapter.notifyItemChanged(i);
            }
        }
    }

    //当前日期的全选
    private void onCurrenDateAllChoose(String chooseDate) {
        chooseDate = chooseDate.replaceAll("-", "");
        List<MyTracksModel.DataBean> datas = ((MyTracksAdapter) mAdapter).getData();

        MyTracksModel.DataBean data;
        String date = "";
        for (int i = 0, size = datas.size(); i < size; i++) {
            data = datas.get(i);
            date = data.getTrackTime().replaceAll("-", "");
            //判断是否是头部 日期是否一样
            if (chooseDate.equals(date) && MyTracksModel.CONTENT_TAG == data.getItemType() && !data.isChoose()) {
                data.setChoose(true);
                mAdapter.notifyItemChanged(i);
            }
        }
    }

    //清除选中
    private void cleanChoose() {
        for (MyTracksModel.DataBean myTracksModel : ((MyTracksAdapter) mAdapter).getData()) {
            myTracksModel.setChoose(false);
        }
    }

    //全选
    private void allChoose() {
        for (MyTracksModel.DataBean myTracksModel : ((MyTracksAdapter) mAdapter).getData()) {
            myTracksModel.setChoose(true);
        }
    }

    //判断是否全选
    private void onAllChoose() {
        boolean isAllChoose = true;
        List<MyTracksModel.DataBean> allData = ((MyTracksAdapter) mAdapter).getData();
        for (MyTracksModel.DataBean allDatum : allData) {
            if (!allDatum.isChoose()) {
                isAllChoose = false;
                break;
            }
        }

        if (isAllChoose) {
            //全选
            mCbAll.setChecked(true);
        } else {
            //未全选
            mCbAll.setChecked(false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        deleteIds = null;
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, MyTracksActivity.class);
        context.startActivity(intent);
    }
}
