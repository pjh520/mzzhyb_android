package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;

import com.alibaba.fastjson.JSON;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.SpanUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.utils.wheel.WheelUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.catalogue.model.DiseCataModel;
import com.jrdz.zhyb_android.ui.home.model.KVModel;
import com.jrdz.zhyb_android.ui.settlement.model.QueryPersonalInfoModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.AddressManageModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.DefaultAddressModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OnlineBuyDrugModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.QueryShoppingCartNumModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarRefreshModel;
import com.jrdz.zhyb_android.utils.AppManager_Acivity;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-07
 * 描    述：在线购药
 * ================================================
 */
public class OnlineBuyDrugActivity extends BaseActivity {
    private TextView mTv01Seize;
    private TextView mTv02Seize;
    private RadioGroup mSrbGroup;
    private AppCompatRadioButton mSrbSelfLifting;
    private AppCompatRadioButton mSrbMerchantDistr;
    private android.view.View mLine01;
    private LinearLayout mLlAddressInfo;
    private ImageView mIvAddressSeize;
    private TextView mTvAddressInfo, mTvMedical, mTvMante, mTvLiangbing,mTvTongchou;
    private RadioGroup mSrbGroupBuyType;
    private AppCompatRadioButton mSrbSelfBuy, mSrbMedicalBuy, mSrbManteBuy, mSrbLiangbingBuy,mSrbTongchouBuy;
    private LinearLayout mLlName;
    private EditText mEtName;
    private View mLineName;
    private LinearLayout mLlCertNo;
    private EditText mEtCertNo;
    private View mLineCertNo;
    private LinearLayout mLlDiseInfo;
    private TextView mTvDiseInfo;
    private View mLineDiseInfo;
    private LinearLayout mLlShopHeadInfo;
    private ImageView mIvHeadProduct;
    private TextView mTvShopNam;
    private LinearLayout mLlOrderListContain;
    private TextView mTvTotalNum;
    private TextView mTvTotalPrice01;
    private EditText mEtBuyerWx;
    private EditText mEtBuyerLeaveMsg;
    private TextView mTvDescribe02;
    private TextView mTvHeji;
    private TextView mTvTotalPriceTag;
    private TextView mTvTotalPrice02;
    private TextView mTvFreight;
    private ShapeTextView mTvApplyBuy;

    private ArrayList<GoodsModel.DataBean> goodBeans;//需要结算的商品列表
    private String storeName, storeAccessoryUrl, startingPrice="0", latitude, longitude,fixmedins_type;
    private AddressManageModel.DataBean selectAddressData;//选中的地址信息
    private ArrayList<KVModel> associatedDiagnostics = new ArrayList<>(), manteDiagnostics = new ArrayList<>(), liangbingDiagnostics = new ArrayList<>();//病种名称与诊断信息 //慢特病信息  //两病信息
    private WheelUtils wheelUtils;
    String isSelfLifting = "1", isSelfLiftingText = "自提", isSelfBuy = "1", isSelfBuyText = "自助购药"; //默认自提 //默认自助购药
    private String totalPrice;//总价
    private int totalNum = 0;//商品总数
    private String insutype;//险种类型

    //实名认证结果
    private ObserverWrapper<String> mSmrzResultObserve=new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String s) {
            if ("SUCCESS".equals(s)){
                mEtName.setText(EmptyUtils.strEmpty(InsuredLoginUtils.getName()));
                mEtCertNo.setText(EmptyUtils.strEmpty(InsuredLoginUtils.getIdCardNo()));
                queryPersonalInfo();
            }
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.activity_online_buy_drug;
    }

    @Override
    public void initView() {
        super.initView();
        mTv01Seize = findViewById(R.id.tv_01_seize);
        mTv02Seize = findViewById(R.id.tv_02_seize);
        mSrbGroup = findViewById(R.id.srb_group);
        mSrbSelfLifting = findViewById(R.id.srb_self_lifting);
        mSrbMerchantDistr = findViewById(R.id.srb_merchant_distr);
        mLine01 = findViewById(R.id.line_01);
        mLlAddressInfo = findViewById(R.id.ll_address_info);
        mIvAddressSeize = findViewById(R.id.iv_address_seize);
        mTvAddressInfo = findViewById(R.id.tv_address_info);

        mSrbGroupBuyType = findViewById(R.id.srb_group_buy_type);
        mSrbSelfBuy = findViewById(R.id.srb_self_buy);
        mSrbMedicalBuy = findViewById(R.id.srb_medical_buy);
        mTvMedical = findViewById(R.id.tv_medical);
        mSrbManteBuy = findViewById(R.id.srb_mante_buy);
        mTvMante = findViewById(R.id.tv_mante);
        mSrbLiangbingBuy = findViewById(R.id.srb_liangbing_buy);
        mTvLiangbing = findViewById(R.id.tv_liangbing);
        mSrbTongchouBuy= findViewById(R.id.srb_tongchou_buy);
        mTvTongchou= findViewById(R.id.tv_tongchou);

        mLlName = findViewById(R.id.ll_name);
        mEtName = findViewById(R.id.et_name);
        mLineName = findViewById(R.id.line_name);
        mLlCertNo = findViewById(R.id.ll_cert_no);
        mEtCertNo = findViewById(R.id.et_cert_no);
        mLineCertNo = findViewById(R.id.line_cert_no);
        mLlDiseInfo = findViewById(R.id.ll_dise_info);
        mTvDiseInfo = findViewById(R.id.tv_dise_info);
        mLineDiseInfo = findViewById(R.id.line_dise_info);

        mLlShopHeadInfo = findViewById(R.id.ll_shop_head_info);
        mIvHeadProduct = findViewById(R.id.iv_head_product);
        mTvShopNam = findViewById(R.id.tv_shop_nam);
        mLlOrderListContain = findViewById(R.id.ll_order_list_contain);
        mTvTotalNum = findViewById(R.id.tv_total_num);
        mTvTotalPrice01 = findViewById(R.id.tv_total_price01);
        mEtBuyerWx = findViewById(R.id.et_buyer_wx);
        mEtBuyerLeaveMsg = findViewById(R.id.et_buyer_leave_msg);
        mTvDescribe02 = findViewById(R.id.tv_describe02);
        mTvHeji = findViewById(R.id.tv_heji);
        mTvTotalPriceTag = findViewById(R.id.tv_total_price_tag);
        mTvTotalPrice02 = findViewById(R.id.tv_total_price02);
        mTvFreight = findViewById(R.id.tv_freight);
        mTvApplyBuy = findViewById(R.id.tv_apply_buy);
    }

    @Override
    public void initData() {
        MsgBus.sendSmrzResult_user().observe(this, mSmrzResultObserve);
        Intent intent = getIntent();
        goodBeans = intent.getParcelableArrayListExtra("goodBeans");
        storeName = intent.getStringExtra("storeName");
        storeAccessoryUrl = intent.getStringExtra("storeAccessoryUrl");
        startingPrice = intent.getStringExtra("startingPrice");
        latitude = intent.getStringExtra("latitude");
        longitude = intent.getStringExtra("longitude");
        fixmedins_type= intent.getStringExtra("fixmedins_type");
        super.initData();
        if (goodBeans == null) {
            showShortToast("数据有误，请重新点击直接购药");
            finish();
            return;
        }
        mTvApplyBuy.setTag(true);
        mTvDiseInfo.setTag("");
        mTvDiseInfo.setText("");
        // 2022-11-07 遍历药品列表 是否包含平台药品 若包含 那么购药方式展示自助购药跟医保购药 若不包含 那么只展示自助购药
        // 2022-11-07 遍历药品列表 是否包含有处方药，则底部显示注意事项，没有处方药则无
        boolean hasPlatfromDrug = false;
        boolean hasPrescrDrug = false;
        //设置药品列表
        mLlOrderListContain.removeAllViews();
        associatedDiagnostics.clear();
        GoodsModel.DataBean goodBean;
        totalPrice = "0";
        totalNum = 0;
        for (int i = 0, size = goodBeans.size(); i < size; i++) {
            goodBean = goodBeans.get(i);

            //获取平台西药中成药中的病种名称与诊断信息集合
            if ("1".equals(fixmedins_type) && "1".equals(goodBean.getIsPlatformDrug())) {
                List<DiseCataModel.DataBean> dataBeans = JSON.parseArray(goodBean.getAssociatedDiagnostics(), DiseCataModel.DataBean.class);
                if (dataBeans!=null){
                    for (DiseCataModel.DataBean dataBean : dataBeans) {
                        associatedDiagnostics.add(new KVModel(dataBean.getName(), dataBean.getCode()));
                    }
                }
            }

            if ("1".equals(goodBean.getIsPlatformDrug())) {//有平台药
                hasPlatfromDrug = true;
            }
            if ("2".equals(goodBean.getItemType())) {//有处方药  1、OTC 2、处方药
                hasPrescrDrug = true;
            }

            if ("1".equals(goodBean.getIsPromotion())) {//有折扣
                totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(goodBean.getPreferentialPrice())
                        .multiply(new BigDecimal(String.valueOf(goodBean.getShoppingCartNum())))).toPlainString();
            } else {//没有折扣
                totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(goodBean.getPrice())
                        .multiply(new BigDecimal(String.valueOf(goodBean.getShoppingCartNum())))).toPlainString();
            }

            totalNum += goodBean.getShoppingCartNum();

            View view = LayoutInflater.from(this).inflate(R.layout.layout_orderlist_product_item, mLlOrderListContain, false);
            ImageView ivPic = view.findViewById(R.id.iv_pic);
            ShapeTextView stvOtc = view.findViewById(R.id.stv_otc);
            TextView tvTitle = view.findViewById(R.id.tv_title);
            TextView tvEstimatePrice = view.findViewById(R.id.tv_estimate_price);
            TextView tvRealPrice = view.findViewById(R.id.tv_real_price);
            View lineItem = view.findViewById(R.id.line_item);

            GlideUtils.loadImg(goodBean.getBannerAccessoryUrl1(), ivPic, R.drawable.ic_placeholder_bg,
                    new RoundedCornersTransformation(getResources().getDimensionPixelSize(R.dimen.dp_16), 0));
            //判断是否是处方药
            if ("1".equals(goodBean.getItemType())) {
                stvOtc.setText("OTC");
                stvOtc.setVisibility(View.VISIBLE);
            } else if ("2".equals(goodBean.getItemType())) {
                stvOtc.setText("处方药");
                stvOtc.setVisibility(View.VISIBLE);
            } else {
                stvOtc.setVisibility(View.GONE);
            }

            tvTitle.setText(EmptyUtils.strEmpty(goodBean.getGoodsName()));
            tvEstimatePrice.setText("1".equals(goodBean.getIsPromotion()) ? goodBean.getPreferentialPrice() : goodBean.getPrice());
            tvRealPrice.setText("x" + goodBean.getShoppingCartNum());
            //2022-10-10 最后一个item的分割线需要隐藏
            if (i == size - 1) {
                lineItem.setVisibility(View.GONE);
            } else {
                lineItem.setVisibility(View.VISIBLE);
            }
            mLlOrderListContain.addView(view);
        }

        if (hasPlatfromDrug) {
            mSrbMedicalBuy.setVisibility(View.VISIBLE);
            mSrbManteBuy.setVisibility(View.VISIBLE);
            mSrbLiangbingBuy.setVisibility(View.VISIBLE);
            mSrbTongchouBuy.setVisibility(View.VISIBLE);
        } else {
            mSrbMedicalBuy.setVisibility(View.GONE);
            mTvMedical.setVisibility(View.GONE);
            mSrbManteBuy.setVisibility(View.GONE);
            mTvMante.setVisibility(View.GONE);
            mSrbLiangbingBuy.setVisibility(View.GONE);
            mTvLiangbing.setVisibility(View.GONE);
            mSrbTongchouBuy.setVisibility(View.GONE);
            mTvTongchou.setVisibility(View.GONE);
        }

        if (hasPrescrDrug) {
            mTvDescribe02.setVisibility(View.VISIBLE);
        } else {
            mTvDescribe02.setVisibility(View.GONE);
        }

        if ("1".equals(fixmedins_type)){//医院
            mSrbLiangbingBuy.setVisibility(View.VISIBLE);
        }else {//药店
            mSrbLiangbingBuy.setVisibility(View.GONE);
        }

        mEtName.setText(EmptyUtils.strEmpty(InsuredLoginUtils.getName()));
        mEtCertNo.setText(EmptyUtils.strEmpty(InsuredLoginUtils.getIdCardNo()));
        GlideUtils.loadImg(storeAccessoryUrl, mIvHeadProduct, R.drawable.ic_placeholder_bg, new CircleCrop());
        mTvShopNam.setText(EmptyUtils.strEmpty(storeName));
        mTvTotalNum.setText("共" + totalNum + "件商品");
        mTvTotalPrice01.setText(EmptyUtils.strEmpty(totalPrice));

        mTvTotalPrice02.setText(EmptyUtils.strEmpty(totalPrice));
        //获取慢病信息
        showWaitDialog();
        QueryPersonalInfoModel.sendQueryPersonalInfoRequest_user(TAG, goodBeans.get(0).getFixmedins_code(), InsuredLoginUtils.getIdCardNo(), "02", "14",
                new CustomerJsonCallBack<QueryPersonalInfoModel>() {
                    @Override
                    public void onRequestError(QueryPersonalInfoModel returnData, String msg) {
                        hideWaitDialog();
//                        showTipDialog(msg);
                    }

                    @Override
                    public void onRequestSuccess(QueryPersonalInfoModel returnData) {
                        hideWaitDialog();
                        if (returnData.getData() != null && returnData.getData().getInsuinfo() != null) {
                            List<QueryPersonalInfoModel.DataBean.InsuinfoBean> insuinfo = returnData.getData().getInsuinfo();
                            insutype = insuinfo.get(0).getInsutype();
                        }

                        ArrayList<QueryPersonalInfoModel.PsnOpspRegBean> datas = returnData.getPsnOpspReg();
                        manteDiagnostics.clear();
                        if (datas != null) {
                            for (QueryPersonalInfoModel.PsnOpspRegBean data : datas) {
                                manteDiagnostics.add(new KVModel(data.getOpsp_dise_name(), data.getOpsp_dise_code()));
                            }
                        }
                    }
                });
        //两病信息
        liangbingDiagnostics.clear();
        liangbingDiagnostics.add(new KVModel("高血压", "I10.x00x002"));
        liangbingDiagnostics.add(new KVModel("糖尿病", "E14.900x001"));
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mSrbGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.srb_self_lifting) {//自提
                    mLine01.setVisibility(View.GONE);
                    mLlAddressInfo.setVisibility(View.GONE);
                    selectAddressData = null;

                    isSelfLifting = "1";
                    isSelfLiftingText = "自提";

                    mTvApplyBuy.setTag(true);
                    mTvApplyBuy.setText("提交订单");
                } else {//商家配送
                    if (selectAddressData == null) {
                        showWaitDialog();
                        DefaultAddressModel.sendDefaultAddressRequest(TAG, new CustomerJsonCallBack<DefaultAddressModel>() {
                            @Override
                            public void onRequestError(DefaultAddressModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(DefaultAddressModel returnData) {
                                hideWaitDialog();
                                selectAddressData = returnData.getData();
                                setAddressInfo();
                            }
                        });
                    } else {
                        setAddressInfo();
                    }

                    isSelfLifting = "2";
                    isSelfLiftingText = "商家配送";

                    //判断是否够起送费
                    double diffPrice = new BigDecimal(startingPrice).subtract(new BigDecimal(EmptyUtils.strEmptyToText(totalPrice, "0"))).doubleValue();
                    if (diffPrice <= 0) {//够起送费 可以直接购买
                        mTvApplyBuy.setTag(true);
                        mTvApplyBuy.setText("提交订单");
                    } else {//不够起送费 还需要添加商品
                        SpannableStringBuilder str = new SpanUtils().append("凑单购买").setFontSize(getResources().getDimensionPixelSize(R.dimen.dp_30))
                                .append("\n")
                                .append("差¥" + diffPrice + "起送").setFontSize(getResources().getDimensionPixelSize(R.dimen.dp_16))
                                .create();
                        mTvApplyBuy.setTag(false);
                        mTvApplyBuy.setText(str);
                    }
                }
            }
        });
        mSrbGroupBuyType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mTvDiseInfo.setTag("");
                mTvDiseInfo.setText("");
                if (checkedId == R.id.srb_self_buy) {//自助购药
                    mTvMedical.setVisibility(View.GONE);
                    mTvMante.setVisibility(View.GONE);
                    mTvLiangbing.setVisibility(View.GONE);
                    mTvTongchou.setVisibility(View.GONE);

                    mLlName.setVisibility(View.GONE);
                    mLineName.setVisibility(View.GONE);
                    mLlCertNo.setVisibility(View.GONE);
                    mLineCertNo.setVisibility(View.GONE);
                    mLlDiseInfo.setVisibility(View.GONE);
                    mLineDiseInfo.setVisibility(View.GONE);

                    isSelfBuy = "1";
                    isSelfBuyText = "自费结算";
                } else if (checkedId == R.id.srb_medical_buy) {//医保购药
                    mTvMedical.setVisibility(View.VISIBLE);
                    mTvMante.setVisibility(View.GONE);
                    mTvLiangbing.setVisibility(View.GONE);
                    mTvTongchou.setVisibility(View.GONE);

                    mLlName.setVisibility(View.VISIBLE);
                    mLineName.setVisibility(View.VISIBLE);
                    mLlCertNo.setVisibility(View.VISIBLE);
                    mLineCertNo.setVisibility(View.VISIBLE);

                    if ("1".equals(fixmedins_type)) {//定点医院
                        mLlDiseInfo.setVisibility(View.VISIBLE);
                        mLineDiseInfo.setVisibility(View.VISIBLE);
                    } else {//定点药店
                        mLlDiseInfo.setVisibility(View.GONE);
                        mLineDiseInfo.setVisibility(View.GONE);
                    }

                    if (!associatedDiagnostics.isEmpty()) {
                        mTvDiseInfo.setTag(associatedDiagnostics.get(0).getCode());
                        mTvDiseInfo.setText(EmptyUtils.strEmpty(associatedDiagnostics.get(0).getText()));
                    }

                    isSelfBuy = "2";
                    isSelfBuyText = "城乡居民医保/职工医保购药（医保结算）";
                } else if (checkedId == R.id.srb_mante_buy) {//慢特病购药购药
                    if (manteDiagnostics.isEmpty()) {
                        showShortToast("您不是慢特病患者，请先进行慢特病备案");
                    }
                    mTvMedical.setVisibility(View.GONE);
                    mTvMante.setVisibility(View.VISIBLE);
                    mTvLiangbing.setVisibility(View.GONE);
                    mTvTongchou.setVisibility(View.GONE);

                    mLlName.setVisibility(View.VISIBLE);
                    mLineName.setVisibility(View.VISIBLE);
                    mLlCertNo.setVisibility(View.VISIBLE);
                    mLineCertNo.setVisibility(View.VISIBLE);

                    mLlDiseInfo.setVisibility(View.VISIBLE);
                    mLineDiseInfo.setVisibility(View.VISIBLE);

                    if (!manteDiagnostics.isEmpty()) {
                        mTvDiseInfo.setTag(manteDiagnostics.get(0).getCode());
                        mTvDiseInfo.setText(EmptyUtils.strEmpty(manteDiagnostics.get(0).getText()));
                    }

                    isSelfBuy = "3";
                    isSelfBuyText = "慢特病购药（医保结算）";
                } else if (checkedId == R.id.srb_liangbing_buy) {//两病购药购药
                    mTvMedical.setVisibility(View.GONE);
                    mTvMante.setVisibility(View.GONE);
                    mTvLiangbing.setVisibility(View.VISIBLE);
                    mTvTongchou.setVisibility(View.GONE);

                    mLlName.setVisibility(View.VISIBLE);
                    mLineName.setVisibility(View.VISIBLE);
                    mLlCertNo.setVisibility(View.VISIBLE);
                    mLineCertNo.setVisibility(View.VISIBLE);

                    mLlDiseInfo.setVisibility(View.VISIBLE);
                    mLineDiseInfo.setVisibility(View.VISIBLE);

                    if (!liangbingDiagnostics.isEmpty()) {
                        mTvDiseInfo.setTag(liangbingDiagnostics.get(0).getCode());
                        mTvDiseInfo.setText(EmptyUtils.strEmpty(liangbingDiagnostics.get(0).getText()));
                    }

                    isSelfBuy = "4";
                    isSelfBuyText = "两病购药（医保结算）";
                }else if (checkedId == R.id.srb_tongchou_buy) {//门诊统筹
                    mTvMedical.setVisibility(View.GONE);
                    mTvMante.setVisibility(View.GONE);
                    mTvLiangbing.setVisibility(View.GONE);
                    mTvTongchou.setVisibility(View.VISIBLE);

                    mLlName.setVisibility(View.VISIBLE);
                    mLineName.setVisibility(View.VISIBLE);
                    mLlCertNo.setVisibility(View.VISIBLE);
                    mLineCertNo.setVisibility(View.VISIBLE);

                    if ("1".equals(fixmedins_type)) {//定点医院
                        mLlDiseInfo.setVisibility(View.VISIBLE);
                        mLineDiseInfo.setVisibility(View.VISIBLE);
                    } else {//定点药店
                        mLlDiseInfo.setVisibility(View.GONE);
                        mLineDiseInfo.setVisibility(View.GONE);
                    }

                    if (!associatedDiagnostics.isEmpty()) {
                        mTvDiseInfo.setTag(associatedDiagnostics.get(0).getCode());
                        mTvDiseInfo.setText(EmptyUtils.strEmpty(associatedDiagnostics.get(0).getText()));
                    }

                    isSelfBuy = "5";
                    isSelfBuyText = "门诊统筹（医保结算）";
                }
            }
        });
        mLlAddressInfo.setOnClickListener(this);
        mLlDiseInfo.setOnClickListener(this);
        mTvApplyBuy.setOnClickListener(this);
    }

    //查询用户医保信息
    private void queryPersonalInfo() {
        showWaitDialog();
        QueryPersonalInfoModel.sendQueryPersonalInfoRequest_user(TAG, goodBeans.get(0).getFixmedins_code(), InsuredLoginUtils.getIdCardNo(), "02", "14",
                new CustomerJsonCallBack<QueryPersonalInfoModel>() {
                    @Override
                    public void onRequestError(QueryPersonalInfoModel returnData, String msg) {
                        hideWaitDialog();
//                        showTipDialog(msg);
                    }

                    @Override
                    public void onRequestSuccess(QueryPersonalInfoModel returnData) {
                        hideWaitDialog();
                        if (returnData.getData() != null && returnData.getData().getInsuinfo() != null) {
                            List<QueryPersonalInfoModel.DataBean.InsuinfoBean> insuinfo = returnData.getData().getInsuinfo();
                            insutype = insuinfo.get(0).getInsutype();
                        }

                        ArrayList<QueryPersonalInfoModel.PsnOpspRegBean> datas = returnData.getPsnOpspReg();
                        manteDiagnostics.clear();
                        if (datas != null) {
                            for (QueryPersonalInfoModel.PsnOpspRegBean data : datas) {
                                manteDiagnostics.add(new KVModel(data.getOpsp_dise_name(), data.getOpsp_dise_code()));
                            }
                        }
                    }
                });
    }

    //设置商家配送地址信息
    private void setAddressInfo() {
        mLine01.setVisibility(View.VISIBLE);
        mLlAddressInfo.setVisibility(View.VISIBLE);
        // 2022-11-07 1：判断是否有默认地址  若没有 则显示 “请选择地址”  若有 则直接显示默认地址
        if (selectAddressData == null || EmptyUtils.isEmpty(selectAddressData.getShipToAddressId())) {
            mIvAddressSeize.setVisibility(View.GONE);
            mTvAddressInfo.setText("请选择地址");
        } else {
            mIvAddressSeize.setVisibility(View.VISIBLE);
            mTvAddressInfo.setText(selectAddressData.getFullName() + "    " + StringUtils.encryptionPhone(selectAddressData.getPhone()) +
                    "\n" + selectAddressData.getLocation() + selectAddressData.getAddress());
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.ll_address_info://选择地址
                Intent intent = new Intent(OnlineBuyDrugActivity.this, AddressManageActivtiy.class);
                intent.putExtra("from", 1);
                startActivityForResult(intent, new OnActivityCallback() {
                    @Override
                    public void onActivityResult(int resultCode, @Nullable Intent data) {
                        if (resultCode == RESULT_OK) {
                            selectAddressData = data.getParcelableExtra("itemData");
                            setAddressInfo();
                        }
                    }
                });
                break;
            case R.id.ll_dise_info://诊断信息
                KeyboardUtils.hideSoftInput(OnlineBuyDrugActivity.this);
                switch (isSelfBuy) {
                    case "2":
                        onDiseInfo(associatedDiagnostics);
                        break;
                    case "3":
                        onDiseInfo(manteDiagnostics);
                        break;
                    case "4":
                        onDiseInfo(liangbingDiagnostics);
                        break;
                    case "5":
                        onDiseInfo(associatedDiagnostics);
                        break;
                }
                break;
            case R.id.tv_apply_buy://提交订单
                if ((Boolean) mTvApplyBuy.getTag()){//够起送费 可以直接购买
                    onApplyOrder();
                }else {//不够起送费 还需要添加商品
                    //判断商品数量是否大于1
                    // 1.如果是的话 那么说明肯定是有在购物车了 这时候 不需要做+1操作 直接进入店铺详情
                    // 2.如果只有一个商品的情况 那么查询该商品加入购物车数量 如果是0的话 那么购物车数量变成1 如果不是0 那么直接进入店铺详情页
                    if (goodBeans.size()>1){
                        ShopDetailActivity2.newIntance(OnlineBuyDrugActivity.this, goodBeans.get(0).getFixmedins_code());
                    }else {
                        getGoodsShopCarNum(goodBeans.get(0).getGoodsNo(),goodBeans.get(0).getFixmedins_code());
                    }
                }
                break;
        }
    }

    //弹出病种名称与诊断信息
    private void onDiseInfo(ArrayList<KVModel> associatedDiagnostics) {
        if (associatedDiagnostics.isEmpty()) {
            showShortToast("您不是慢特病患者，请先进行慢特病备案");
            return;
        }
        if (wheelUtils == null) {
            wheelUtils = new WheelUtils();
        }
        wheelUtils.showWheel(OnlineBuyDrugActivity.this, "选择诊断信息", 0,
                associatedDiagnostics, new WheelUtils.WheelClickListener<KVModel>() {
                    @Override
                    public void onchooseDate(int choosePos, KVModel dateInfo) {
                        mTvDiseInfo.setTag(dateInfo.getCode());
                        mTvDiseInfo.setText(EmptyUtils.strEmpty(dateInfo.getText()));
                    }
                });
    }

    //提交订单
    private void onApplyOrder() {
        String distance = "";
        if ("2".equals(isSelfLifting)) {//商家配送
            double disff = new BigDecimal(totalPrice).subtract(new BigDecimal(startingPrice)).doubleValue();
            if (disff < 0) {
                showShortToast("您的订单低于起送价" + startingPrice + "元,无法提交订单");
                return;
            }

            // 2022-11-07 需要验证 是否选择地址
            if (selectAddressData == null) {
                showShortToast("请选择配送地址");
                return;
            }
            distance = String.valueOf((int) DistanceUtil.getDistance(new LatLng(Double.valueOf(EmptyUtils.strEmptyToText(selectAddressData.getLatitude(), "0")), Double.valueOf(EmptyUtils.strEmptyToText(selectAddressData.getLongitude(), "0"))),
                    new LatLng(Double.valueOf(EmptyUtils.strEmptyToText(latitude, "0")), Double.valueOf(EmptyUtils.strEmptyToText(longitude, "0")))) / 1000);
        }
        if (!"1".equals(isSelfBuy)) {//医保购药
            //  2022-11-07 需要验证 1:定点医院时  姓名 身份证号 病种名称 诊断信息   2：定点药店时 姓名 身份证号
            if (EmptyUtils.isEmpty(mEtName.getText().toString())) {
                showShortToast("请填写您的姓名");
                return;
            }

            if (EmptyUtils.isEmpty(mEtCertNo.getText().toString())) {
                showShortToast("请填写您的身份证号");
                return;
            }

            if ("3".equals(isSelfBuy) && manteDiagnostics.isEmpty()) {
                showShortToast("您不是慢特病患者，请先进行慢特病备案");
                return;
            }

            if ("1".equals(fixmedins_type)) {//定点医院
                if (EmptyUtils.isEmpty(mTvDiseInfo.getText().toString())) {
                    showShortToast("请选择病种名称与诊断信息");
                    return;
                }
            }else {//定点药店
                if (!"2".equals(isSelfBuy)&&!"5".equals(isSelfBuy)&&EmptyUtils.isEmpty(mTvDiseInfo.getText().toString())) {
                    showShortToast("请选择病种名称与诊断信息");
                    return;
                }
            }
        }

//        if (EmptyUtils.isEmpty(mEtBuyerWx.getText().toString())) {
//            showShortToast("请填写您的微信");
//            return;
//        }

        showWaitDialog();
        OnlineBuyDrugModel.sendOnlineBuyDrugRequest(TAG, isSelfLifting, isSelfLiftingText, null == selectAddressData ? "" : selectAddressData.getFullName(),
                null == selectAddressData ? InsuredLoginUtils.getPhone() : selectAddressData.getPhone(), null == selectAddressData ? "" : selectAddressData.getLocation(),
                null == selectAddressData ? "" : selectAddressData.getAddress(), isSelfBuy, isSelfBuyText, String.valueOf(mTvDiseInfo.getTag()),
                mTvDiseInfo.getText().toString(), "1".equals(isSelfBuy) ? InsuredLoginUtils.getName() : mEtName.getText().toString(),
                mEtCertNo.getText().toString(),
                String.valueOf(totalNum), totalPrice, EmptyUtils.strEmpty(mEtBuyerWx.getText().toString()), mEtBuyerLeaveMsg.getText().toString(),
                goodBeans.get(0).getFixmedins_code(), goodBeans, null == selectAddressData ? "" : selectAddressData.getAreaName(),null == selectAddressData ? "" : selectAddressData.getCity()
                , distance, EmptyUtils.strEmpty(insutype), new CustomerJsonCallBack<OnlineBuyDrugModel>() {
                    @Override
                    public void onRequestError(OnlineBuyDrugModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(OnlineBuyDrugModel returnData) {
                        hideWaitDialog();
                        MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "2"));
                        showShortToast("提交订单成功");
                        if (returnData.getData() != null) {
                            OrderDetailActivity_user.newIntance(OnlineBuyDrugActivity.this, EmptyUtils.strEmpty(returnData.getData().getOrderNo()), 1);
                        }

                        goFinish();
                    }
                });
    }

    //获取商品 加入购物车的数量
    private void getGoodsShopCarNum(String goodsNo, String fixmedins_code) {
        showWaitDialog();
        QueryShoppingCartNumModel.sendQueryShoppingCartNumRequest(TAG, goodsNo, fixmedins_code, new CustomerJsonCallBack<QueryShoppingCartNumModel>() {
            @Override
            public void onRequestError(QueryShoppingCartNumModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(QueryShoppingCartNumModel returnData) {
                QueryShoppingCartNumModel.DataBean data = returnData.getData();
                if (data != null) {
                    int shoppingCartNum = new BigDecimal(EmptyUtils.strEmptyToText(data.getShoppingCartNum(), "0")).intValue();
                    if (shoppingCartNum==0){
                        shoppingCartNum = shoppingCartNum+1;
                        onAddShopcar(goodsNo, fixmedins_code, shoppingCartNum);
                    }else {
                        hideWaitDialog();
                        ShopDetailActivity2.newIntance(OnlineBuyDrugActivity.this, fixmedins_code);
                    }
                }
            }
        });
    }

    //加入购物车--购物车列表
    private void onAddShopcar(String goodsNo, String fixmedins_code, int shoppingCartNum) {
        BaseModel.sendAddShopCarRequest(TAG, fixmedins_code, goodsNo, shoppingCartNum, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "1"));
                ShopDetailActivity2.newIntance(OnlineBuyDrugActivity.this, fixmedins_code);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (wheelUtils != null) {
            wheelUtils.dissWheel();
        }
        if (goodBeans != null) {
            goodBeans.clear();
            goodBeans = null;
        }
        selectAddressData = null;
        if (associatedDiagnostics != null) {
            associatedDiagnostics.clear();
            associatedDiagnostics = null;
        }
        if (manteDiagnostics != null) {
            manteDiagnostics.clear();
            manteDiagnostics = null;
        }
        if (liangbingDiagnostics != null) {
            liangbingDiagnostics.clear();
            liangbingDiagnostics = null;
        }
    }

    public static void newIntance(Context context, ArrayList<GoodsModel.DataBean> datas, String storeName,
                                  String storeAccessoryUrl, String startingPrice, String latitude, String longitude,String fixmedins_type) {
        if (AppManager_Acivity.getInstance().hasActivity(OnlineBuyDrugActivity.class)){
            AppManager_Acivity.getInstance().finishActivity(OnlineBuyDrugActivity.class);
        }

        Intent intent = new Intent(context, OnlineBuyDrugActivity.class);
        intent.putExtra("goodBeans", datas);
        intent.putExtra("storeName", storeName);
        intent.putExtra("storeAccessoryUrl", storeAccessoryUrl);
        intent.putExtra("startingPrice", startingPrice);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        intent.putExtra("fixmedins_type", fixmedins_type);
        context.startActivity(intent);
    }
}
