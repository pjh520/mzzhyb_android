package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.alibaba.fastjson.JSON;
import com.frame.compiler.utils.DateUtil;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.LogUtils;
import com.frame.compiler.widget.countDownTime.CustomCountDownTimer;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeFrameLayout;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PayTypeModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OrderDetailModel_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OrderPayOnlineModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.QueryOrderPayOnlineModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.widget.pop.SelectPayTypePop;
import com.kuaiqian.fusedpay.entity.FusedPayRequest;
import com.kuaiqian.fusedpay.entity.FusedPayResult;
import com.kuaiqian.fusedpay.sdk.FusedPayApiFactory;
import com.kuaiqian.fusedpay.sdk.IFusedPayApi;
import com.kuaiqian.fusedpay.sdk.IFusedPayEventHandler;

import java.math.BigDecimal;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-07
 * 描    述：线上支付
 * ================================================
 */
public class OnlinePayActivity extends BaseActivity implements SelectPayTypePop.IOptionListener,IFusedPayEventHandler {
    private TextView mTvStatus;
    private TextView mTvTime;
    private TextView mTvDescribe;
    private ShapeLinearLayout mSllPreSettl;
    private TextView mTvOrderTotalPrice;
    private LinearLayout mLlMedicalPay;
    private TextView mTvMedicalPay;
    private LinearLayout mLlEnterprisePay;
    private TextView mTvEnterprisePay;
    private LinearLayout mLlContain;
    private ShapeFrameLayout mSllSignature;
    private ImageView mIvSignature;
    private TextView mTvNeedPayPriceTag,mTvNeedPayPrice;
    private TextView mTvDescribe02;
    private ShapeLinearLayout mLlPayType;
    private TextView mTvPayType;
    private TextView mTvHeji;
    private TextView mTvTotalPriceTag;
    private TextView mTvTotalPrice;
    private TextView mTvFreightTag;
    private TextView mTvTotalPriceTag02;
    private TextView mTvFreight;
    private ShapeTextView mTvApplyBuy;

    private SelectPayTypePop selectPayTypePop;
    private String orderNo;
    private OrderDetailModel_user.DataBean detailData;//详情数据
    private CustomCountDownTimer mCustomCountDownTimer;
    private boolean hasPrescr;//是否有处方药
    private boolean isPaying=false;//当前操作是否是正在支付

    @Override
    public int getLayoutId() {
        return R.layout.activity_online_pay;
    }

    @Override
    public void initView() {
        super.initView();
        mTvStatus = findViewById(R.id.tv_status);
        mTvTime = findViewById(R.id.tv_time);
        mTvDescribe = findViewById(R.id.tv_describe);

        mSllPreSettl = findViewById(R.id.sll_pre_settl);
        mTvOrderTotalPrice = findViewById(R.id.tv_order_total_price);
        mLlMedicalPay= findViewById(R.id.ll_medical_pay);
        mTvMedicalPay = findViewById(R.id.tv_medical_pay);
        mLlEnterprisePay= findViewById(R.id.ll_enterprise_pay);
        mTvEnterprisePay = findViewById(R.id.tv_enterprise_pay);

        mLlContain = findViewById(R.id.ll_contain);
        mSllSignature = findViewById(R.id.sll_signature);
        mIvSignature = findViewById(R.id.iv_signature);
        mTvNeedPayPriceTag= findViewById(R.id.tv_need_pay_price_tag);
        mTvNeedPayPrice = findViewById(R.id.tv_need_pay_price);
        mTvDescribe02 = findViewById(R.id.tv_describe02);
        mLlPayType = findViewById(R.id.ll_pay_type);
        mTvPayType = findViewById(R.id.tv_pay_type);
        mTvHeji = findViewById(R.id.tv_heji);
        mTvTotalPriceTag = findViewById(R.id.tv_total_price_tag);
        mTvTotalPrice = findViewById(R.id.tv_total_price);
        mTvFreightTag = findViewById(R.id.tv_freight_tag);
        mTvTotalPriceTag02 = findViewById(R.id.tv_total_price_tag02);
        mTvFreight = findViewById(R.id.tv_freight);
        mTvApplyBuy = findViewById(R.id.tv_apply_buy);
    }

    @Override
    public void initData() {
        orderNo=getIntent().getStringExtra("orderNo");
        super.initData();

        mTvPayType.setTag("A2");

        showWaitDialog();
        getPagerData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mLlPayType.setOnClickListener(this);
        mTvApplyBuy.setOnClickListener(this);
    }

    //获取页面数据
    private void getPagerData() {
        OrderDetailModel_user.sendOrderDetailRequest_user(TAG, orderNo, new CustomerJsonCallBack<OrderDetailModel_user>() {
            @Override
            public void onRequestError(OrderDetailModel_user returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(OrderDetailModel_user returnData) {
                hideWaitDialog();
                detailData = returnData.getData();
                if (detailData != null) {
                    setPagerData(returnData.getServer_time());
                }
            }
        });
    }

    //设置详情数据
    private void setPagerData(String server_time) {
        hasPrescr = false;
        //计算  定时器时间==30分钟-已过去多少时间
        mTvTime.setVisibility(View.VISIBLE);
        long pastTimes = new BigDecimal(DateUtil.dateToStamp2(server_time)).subtract(new BigDecimal(DateUtil.dateToStamp2(detailData.getCreateDT()))).longValue();
        setTimeStart(30 * 60 * 1000 - pastTimes);

        // 2022-10-31 判断是否有带处方药
        mLlContain.removeAllViews();
        List<OrderDetailModel_user.DataBean.OrderGoodsBean> orderGoods = detailData.getOrderGoods();
        for (int i = 0, size = orderGoods.size(); i < size; i++) {
            OrderDetailModel_user.DataBean.OrderGoodsBean orderGood = orderGoods.get(i);
            if ("2".equals(orderGood.getItemType())) {
                hasPrescr = true;
            }
            //设置商品信息
            View view = LayoutInflater.from(this).inflate(R.layout.layout_expense_details_item, mLlContain, false);
            TextView tvDrugName = view.findViewById(R.id.tv_drug_name);
            TextView tvDrugUnitprice = view.findViewById(R.id.tv_drug_unitprice);
            TextView tvDrugNum = view.findViewById(R.id.tv_drug_num);
            TextView tvDrugPrice = view.findViewById(R.id.tv_drug_price);

            tvDrugName.setText(EmptyUtils.strEmpty(orderGood.getGoodsName()));
            tvDrugUnitprice.setText(EmptyUtils.strEmpty(orderGood.getPrice()));
            tvDrugNum.setText(String.valueOf(orderGood.getGoodsNum()));
            String price=new BigDecimal(EmptyUtils.strEmptyToText(orderGood.getPrice(),"0")).multiply(new BigDecimal(String.valueOf(orderGood.getGoodsNum()))).toPlainString();
            tvDrugPrice.setText(price);

            mLlContain.addView(view);
        }
        //设置药品说明书图片
        if (!EmptyUtils.isEmpty(detailData.getAccessoryUrl())) {
            GlideUtils.getImageWidHeig(this, detailData.getAccessoryUrl(), new GlideUtils.IGetImageData() {
                @Override
                public void sendData(@NonNull Bitmap resource, int width, int height, double radio) {
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIvSignature.getLayoutParams();
                    if (radio >= 2.1) {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_626));
                        layoutParams.height = new BigDecimal((int) (getResources().getDimension(R.dimen.dp_626))).divide(new BigDecimal(radio), 1, BigDecimal.ROUND_HALF_UP).intValue();
                    } else {
                        layoutParams.width = (int) (getResources().getDimension(R.dimen.dp_300) * radio);
                        layoutParams.height = (int) (getResources().getDimension(R.dimen.dp_300));
                    }

                    mIvSignature.setLayoutParams(layoutParams);
                    GlideUtils.loadImg(detailData.getAccessoryUrl(), mIvSignature);
                }
            });
        }
        //医保结算 费用明细
        mTvOrderTotalPrice.setText(EmptyUtils.strEmpty(detailData.getTotalAmount()));
        mTvEnterprisePay.setText(EmptyUtils.strEmpty(detailData.getEnterpriseFundPay()));
        if ("1".equals(detailData.getPurchasingDrugsMethod())) {//1自助购药（自费结算）
            mLlMedicalPay.setVisibility(View.GONE);
        } else {//2、城乡居民医保/职工医保购药（医保结算）3、慢特病购药（医保结算）4、两病购药（医保结算）
            mLlMedicalPay.setVisibility(View.VISIBLE);
            double medicalPay=new BigDecimal(detailData.getFund_pay_sumamt()).add(new BigDecimal(detailData.getAcct_pay())).doubleValue();
            mTvMedicalPay.setText(String.valueOf(medicalPay));
        }

        double diffPrice=new BigDecimal(EmptyUtils.strEmptyToText(detailData.getOnlineAmount(),"0")).subtract(new BigDecimal(EmptyUtils.strEmptyToText(detailData.getLogisticsFee(),"0"))).doubleValue();
        if (diffPrice==0){
            mTvNeedPayPriceTag.setText("还需线上支付运费");
            mTvNeedPayPrice.setText(EmptyUtils.strEmpty(detailData.getLogisticsFee()));
            mTvDescribe02.setText("快递物流费用");
        }else {
            mTvNeedPayPriceTag.setText("还需线上支付");
            mTvNeedPayPrice.setText(EmptyUtils.strEmpty(detailData.getOnlineAmount()));
            mTvDescribe02.setText("由于部分药品医保无法报销，需进行线上支付");
        }

        mTvTotalPrice.setText(EmptyUtils.strEmpty(detailData.getOnlineAmount()));
        mTvFreight.setText(EmptyUtils.strEmpty(detailData.getLogisticsFee()));
    }

    //开启定时器  millisecond:倒计时的时间 1:待付款倒计时
    protected void setTimeStart(long millisecond) {
        //防止内存泄漏
        if (null != mCustomCountDownTimer) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        mCustomCountDownTimer = new CustomCountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long M = (millisUntilFinished % (60 * 60 * 1000)) / (60 * 1000);
                long S = (millisUntilFinished % (60 * 1000)) / 1000;

                mTvTime.setText((M < 10 ? "0" + M : String.valueOf(M)) + ":" + (S < 10 ? "0" + S : String.valueOf(S)));
            }

            @Override
            public void onFinish() {

            }
        };

        mCustomCountDownTimer.start();
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.ll_pay_type:
                if (selectPayTypePop == null) {
                    selectPayTypePop = new SelectPayTypePop(OnlinePayActivity.this,
                            CommonlyUsedDataUtils.getInstance().getSelectOrderPayTypeData(),this);
                }

                selectPayTypePop.showPopupWindow();
                break;
            case R.id.tv_apply_buy://立即支付
                onApplyBuy();
                break;
        }
    }

    //立即支付
    private void onApplyBuy() {
        // 2022-11-07 这位置根据用户选择的支付方式 去后台请求支付的参数 然后调起各个平台的支付
        if (mTvPayType.getTag()==null||EmptyUtils.isEmpty(String.valueOf(mTvPayType.getTag()))){
            showShortToast("请选择支付方式");
            return;
        }
        String payType=String.valueOf(mTvPayType.getTag());
        showWaitDialog();
        OrderPayOnlineModel.sendOrderPayOnlineRequest(TAG, orderNo, payType, new CustomerJsonCallBack<OrderPayOnlineModel>() {
            @Override
            public void onRequestError(OrderPayOnlineModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(OrderPayOnlineModel returnData) {
                hideWaitDialog();
                OrderPayOnlineModel.DataBean payData = returnData.getData();
                if (payData != null && payData.getMpayInfo() != null) {
                    //获取支付信息成功
                    switch (payType) {
                        case "W2"://微信
                            invokeFusedPaySDK("4", JSON.toJSONString(payData.getMpayInfo()));
                            break;
                        case "A2"://支付宝
                            invokeFusedPaySDK("7", JSON.toJSONString(payData.getMpayInfo()));
                            break;
                        case "Y"://云闪付
                            invokeFusedPaySDK("5", JSON.toJSONString(payData.getMpayInfo()));
                            break;
                    }
                }
            }
        });
        //支付成功
//        PaySuccessActivity.newIntance(OnlinePayActivity.this, "订单支付成功！", "暂不", "开具处方", "1");
    }

    @Override
    public void onItemClick(PayTypeModel payTypeModel) {
        mTvPayType.setTag(payTypeModel.getId());
        mTvPayType.setText(EmptyUtils.strEmpty(payTypeModel.getText()));
    }

    /**
     * 调起聚合支付sdk
     *
     * @param platform 支付平台 :1 --飞凡通支付   2 --支付宝支付     3 --微信支付  4.微信支付定制版 5.云闪付 7.支付宝支付定制版
     * @param mpayInfo 移动支付的信息
     */
    private void invokeFusedPaySDK(String platform, String mpayInfo) {
        FusedPayRequest payRequest = new FusedPayRequest();
        payRequest.setPlatform(platform);
        payRequest.setMpayInfo(mpayInfo);
        if ("5".equals(platform)) {
            payRequest.setUnionPayTestEnv(false);
        }
        // CallBackSchemeId可以自定义，自定义的结果页面需实现IKuaiqianEventHandler接口
        payRequest.setCallbackSchemeId("com.jrdz.zhyb_android.ui.zhyf_user.activity.OnlinePayActivity");
        IFusedPayApi payApi = FusedPayApiFactory.createPayApi(OnlinePayActivity.this);
        payApi.pay(payRequest);

        isPaying=true;
    }

    //==============================================支付方法回调================================================
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        isPaying=false;
        IFusedPayApi api = FusedPayApiFactory.createPayApi(this);
        api.handleIntent(getIntent(), this);
    }

    @Override
    public void onResponse(FusedPayResult fusedPayResult) {
        LogUtils.e("onResponse", "支付结果：" + fusedPayResult);
        String payResultCode = fusedPayResult.getResultStatus();
        String payResultMessage = fusedPayResult.getResultMessage();

        if ("00".equals(payResultCode)) {//00支付返回成功
            if (hasPrescr) {//带处方
                PaySuccessActivity.newIntance(OnlinePayActivity.this, orderNo, "订单支付成功！", "返回首页", "",detailData.getFixmedins_code() ,getAssociatedDiseases(), "1");
            } else {//不带处方
                PaySuccessActivity.newIntance(OnlinePayActivity.this, orderNo, "订单支付成功！", "返回首页", "", "","", "1");
            }
            goFinish();
        } else {
            showTipDialog(payResultMessage);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isPaying){
            showWaitDialog();
            getOrderPayOnlineStatus();
        }
    }

    //获取订单支付状态
    private void getOrderPayOnlineStatus() {
        QueryOrderPayOnlineModel.sendQueryOrderPayOnlineRequest(TAG, orderNo, new CustomerJsonCallBack<QueryOrderPayOnlineModel>() {
            @Override
            public void onRequestError(QueryOrderPayOnlineModel returnData, String msg) {
                hideWaitDialog();
                isPaying=false;
            }

            @Override
            public void onRequestSuccess(QueryOrderPayOnlineModel returnData) {
                hideWaitDialog();
//                IsPaid=1或者IsPaid=2
                QueryOrderPayOnlineModel.DataBean data = returnData.getData();
                if (data != null) {
                    if ("1".equals(data.getIsPaid()) || "2".equals(data.getIsPaid())) {//支付成功
                        if (hasPrescr) {//带处方
                            PaySuccessActivity.newIntance(OnlinePayActivity.this, orderNo, "订单支付成功！", "返回首页", "", detailData.getFixmedins_code(), getAssociatedDiseases(), "2");
                        } else {//不带处方
                            PaySuccessActivity.newIntance(OnlinePayActivity.this, orderNo, "订单支付成功！", "返回首页", "", "", "", "2");
                        }
                        goFinish();
                    }
                }

                isPaying=false;
            }
        });
    }
    //==============================================支付方法回调================================================

    private String getAssociatedDiseases() {
        List<OrderDetailModel_user.DataBean.OrderGoodsBean> goods = detailData.getOrderGoods();
        String associatedDiseases = "";
        for (OrderDetailModel_user.DataBean.OrderGoodsBean orderGood : goods) {
            String diseasesData = orderGood.getAssociatedDiseases();
            if (!EmptyUtils.isEmpty(diseasesData)) {
                associatedDiseases += diseasesData + "∞#";
            }
        }
        if (!EmptyUtils.isEmpty(associatedDiseases)) {
            associatedDiseases = associatedDiseases.substring(0, associatedDiseases.length() - 2);
        }
        return associatedDiseases;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (selectPayTypePop != null) {
            selectPayTypePop.onClean();
        }

        if (mCustomCountDownTimer != null) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
    }

    public static void newIntance(Context context,String orderNo) {
        Intent intent = new Intent(context, OnlinePayActivity.class);
        intent.putExtra("orderNo", orderNo);
        context.startActivity(intent);
    }
}
