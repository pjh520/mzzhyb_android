package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.BroadCastReceiveUtils;
import com.frame.compiler.utils.DateUtil;
import com.frame.compiler.utils.DecimalFormatUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ImageUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.LogUtils;
import com.frame.compiler.utils.RxClipboardTool;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.frame.compiler.widget.countDownTime.CustomCountDownTimer;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.layout.ShapeRelativeLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.insured.activity.SetEnterprisePwdActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.LogisticsActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PayTypeModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TagTextBean;
import com.jrdz.zhyb_android.ui.zhyf_user.model.EnterprisePayModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OrderDetailModel_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OrderPayModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OrderPayOnlineModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.QueryOrderPayOnlineModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarRefreshModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.InputPwdUtils;
import com.jrdz.zhyb_android.utils.InputPwdUtils_Enterprise;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.WXUtils;
import com.jrdz.zhyb_android.widget.TagTextView;
import com.jrdz.zhyb_android.widget.pop.HandWritePop;
import com.jrdz.zhyb_android.widget.pop.SelectPayTypePop;
import com.jrdz.zhyb_android.widget.pop.VerificationCodePop;
import com.kuaiqian.fusedpay.entity.FusedPayRequest;
import com.kuaiqian.fusedpay.entity.FusedPayResult;
import com.kuaiqian.fusedpay.sdk.FusedPayApiFactory;
import com.kuaiqian.fusedpay.sdk.IFusedPayApi;
import com.kuaiqian.fusedpay.sdk.IFusedPayEventHandler;
import com.library.jpush_library.PushMessageReceiver;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import cody.bus.ObserverWrapper;


/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-10
 * 描    述：智慧药房--用户端--订单详情页面
 * ================================================
 */
public class OrderDetailActivity_user extends BaseActivity implements InputPwdUtils.InputPwdListener,
        HandWritePop.IOptionListener, SelectPayTypePop.IOptionListener, IFusedPayEventHandler, InputPwdUtils_Enterprise.InputPwdListener_Enterprise {
    private TextView mTvStatus;
    private TextView mTvTime;
    private TextView mTvDescribe;
    private TextView mTvDeliveryType;
    private View mLine01;
    private ImageView mIvAddressSeize;
    private TextView mTvAddressInfo;
    private ShapeRelativeLayout mSrlPrescrInfo;
    private TextView mTvLookPrescr;
    private TextView mTvOpenPrescrDescribe;
    private ImageView mIvLookPrescr, mIvPrescrHead;
    private TextView mTvPrescrInfo;
    private ShapeTextView mStvPrescrOneself;
    private TextView mTvPrescrCertNo;
    private LinearLayout mLlShopHeadInfo;
    private ImageView mIvHeadProduct;
    private TextView mTvShopNam;
    private TextView mTvOrderRefundStatus;
    private LinearLayout mLlOrderListContain;
    private TextView mTvTotalNum;
    private TextView mTvTotalPrice;

    private TextView mTvOrderId02;
    private TextView mTvOrderIdCopy;
    private LinearLayout mLlRefundNo;
    private TextView mTvRefundNo;
    private TextView mTvRefundNoCopy;
    private TextView mTvBusinessPhone;
    private TextView mTvBusinessWx;
    private TextView mTvBusinessWxCopy;
    private TextView mTvBuyerWx;
    private TextView mTvBuyerWxCopy;
    private TextView mTvBuyerMsg;
    private TextView mTvOrderCreateTime;
    private LinearLayout mLlPayTime;
    private TextView mTvOrderPayTime;
    private LinearLayout mLlDeliverGoodsTime;
    private TextView mTvDeliverGoodsTime;
    private LinearLayout mLlFinishTime;
    private TextView mTvFinishTime;
    private LinearLayout mLlCancleTime;
    private TextView mTvOrderCancleTime;

    private ShapeLinearLayout mSllSerialno;
    private LinearLayout mLlIptOtpNo;
    private TextView mTvIptOtpNo;
    private LinearLayout mLlRefundSerialno;
    private TextView mTvRefundSerialno;
    private LinearLayout mLlOrderTotalPrice;
    private TextView mTvOrderTotalPrice;
    private LinearLayout mLlFreight;
    private TextView mTvFreight;
    private LinearLayout mLlMedicalPay;
    private TextView mTvMedicalPay;
    private LinearLayout mLlMedicalPersonPay;
    private TextView mTvMedicalPersonPay;
    private LinearLayout mLlEnterprisePay;
    private TextView mTvEnterprisePay;
    private LinearLayout mLlPsnCashPay02;
    private TextView mTvPsnCashPay02;

    private ShapeLinearLayout mSllWaitpayDetail;
    private LinearLayout mLlOnlinePrice;
    private TextView mTvOnlinePrice;
    private LinearLayout mLlFundPrice;
    private TextView mTvFundPrice;
    private LinearLayout mLlMedicalPersonPayMoney;
    private TextView mTvMedicalPersonPayMoney;
    private LinearLayout mLlEnterprisePayMoney;
    private TextView mTvEnterprisePayMoney;
    private LinearLayout mLlHasPrice;
    private TextView mTvHasPrice;
    private LinearLayout mLlMedicalBalance;
    private TextView mTvMedicalBalance;
    private LinearLayout mLlEnterpriseBalance;
    private TextView mTvEnterpriseBalance;

    private TextView mTvDescribe02;
    //选择支付方式（企业基金 或者 医保个人余额）
    private LinearLayout sllPayType;
    private RadioGroup srbGroupBuyType;
    private AppCompatRadioButton srbMedicalSelfBuy;
    private AppCompatRadioButton srbEnterpriseBuy;
    private LinearLayout mLlBtn;
    private ShapeTextView mTv01;
    private ShapeTextView mTv02;
    private ShapeTextView mTv03, mTv04;

    List<TagTextBean> tags = new ArrayList<>();
    private String orderNo, type;
    private int from;
    private OrderDetailModel_user.DataBean detailData;//详情数据
    private boolean hasPrescr;//是否有处方药
    private String totalPrice;//总价
    private int totalNum = 0;//商品总数

    private CustomerDialogUtils customerDialogUtils;
    private CustomCountDownTimer mCustomCountDownTimer;
    private CustomCountDownTimer mSmsCountDownTimer;
    private InputPwdUtils inputPwdUtils;
    private InputPwdUtils_Enterprise inputPwdUtils_enterprise;
    private VerificationCodePop verificationCodePop;//获取验证码弹框
    private HandWritePop handWritePop;//手写签名弹框
    String imagPath = BaseGlobal.getPicShotDir() + "user_signature.png";//存储签名的路径
    private SelectPayTypePop selectPayTypePop;//选择支付方式弹框
    private String verifCode = "";
    private String hasOnlinePay = "0";//医保结算完 是否还有在线支付
    private String setl_id;//医保结算完id

    private boolean isPaying = false;//当前操作是否是正在支付
    private boolean isOpenPreing = false;//当前操作是否是正在开处方

    private String describe_01 = "您的订单中包含处方药，支付完成后可填写问诊新开具处方，支付完成后超过10分钟仍未填写问诊信息，订单将自动取消。";
    private String describe_02 = "如有企业基金余额，优先使用企业基金的资金，不足支付商品才选择个人现金支付。";
    private String describe_03 = "首先进入后要选择是否使用个人账户余额支付，如选择是，先使用的个人余额后续才会使用其他支付方式，个人账户余额不足，如有企业基金的情况下优先使用，再使用其他支付。";

    //支付成功监听
    private ObserverWrapper<String> mPayStatusObserve = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            showWaitDialog();
            getDetailData();
        }
    };
    //互联网处方 小程序返回数据监听
    private ObserverWrapper<String> mAppletObserver = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String extraData) {
            Log.e("onResponse", "mAppletObserver extMsg = " + extraData);
            isOpenPreing = false;
            showWaitDialog();
            getDetailData();
        }
    };
    //接受推送消息
    private BroadCastReceiveUtils mPushReceiver = new BroadCastReceiveUtils() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra("type");
            String notificationExtras = intent.getStringExtra("extras");

            switch (type) {
                case "onNotifyMessageArrived"://通知到达
                    try {
                        JSONObject jsonObject = JSON.parseObject(notificationExtras);
                        String open_type = jsonObject.getString("type");
                        String open_OrderNo = jsonObject.getString("OrderNo");
                        switch (EmptyUtils.strEmpty(open_type)) {
                            case "5"://订单修改通知
                                if (!EmptyUtils.isEmpty(open_OrderNo) && open_OrderNo.equals(orderNo)) {
                                    showWaitDialog();
                                    getDetailData();
                                }
                                break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };


    @Override
    public int getLayoutId() {
        return R.layout.activity_order_detail_user;
    }

    @Override
    public void initView() {
        super.initView();
        mTvStatus = findViewById(R.id.tv_status);
        mTvTime = findViewById(R.id.tv_time);
        mTvDescribe = findViewById(R.id.tv_describe);
        mTvDeliveryType = findViewById(R.id.tv_delivery_type);
        mLine01 = findViewById(R.id.line_01);
        mIvAddressSeize = findViewById(R.id.iv_address_seize);
        mTvAddressInfo = findViewById(R.id.tv_address_info);

        mSrlPrescrInfo = findViewById(R.id.srl_prescr_info);
        mTvLookPrescr = findViewById(R.id.tv_look_prescr);
        mIvLookPrescr = findViewById(R.id.iv_look_prescr);
        mTvOpenPrescrDescribe = findViewById(R.id.tv_open_prescr_describe);
        mIvPrescrHead = findViewById(R.id.iv_prescr_head);
        mTvPrescrInfo = findViewById(R.id.tv_prescr_info);
        mStvPrescrOneself = findViewById(R.id.stv_prescr_oneself);
        mTvPrescrCertNo = findViewById(R.id.tv_prescr_cert_no);

        mLlShopHeadInfo = findViewById(R.id.ll_shop_head_info);
        mIvHeadProduct = findViewById(R.id.iv_head_product);
        mTvShopNam = findViewById(R.id.tv_shop_nam);
        mTvOrderRefundStatus = findViewById(R.id.tv_order_refund_status);
        mLlOrderListContain = findViewById(R.id.ll_order_list_contain);
        mTvTotalNum = findViewById(R.id.tv_total_num);
        mTvTotalPrice = findViewById(R.id.tv_total_price);

        mTvOrderId02 = findViewById(R.id.tv_order_id_02);
        mTvOrderIdCopy = findViewById(R.id.tv_order_id_copy);
        mLlRefundNo = findViewById(R.id.ll_refund_no);
        mTvRefundNo = findViewById(R.id.tv_refund_no);
        mTvRefundNoCopy = findViewById(R.id.tv_refund_no_copy);
        mTvBusinessPhone = findViewById(R.id.tv_business_phone);
        mTvBusinessWx = findViewById(R.id.tv_business_wx);
        mTvBusinessWxCopy = findViewById(R.id.tv_business_wx_copy);
        mTvBuyerWx = findViewById(R.id.tv_buyer_wx);
        mTvBuyerWxCopy = findViewById(R.id.tv_buyer_wx_copy);
        mTvBuyerMsg = findViewById(R.id.tv_buyer_msg);
        mTvOrderCreateTime = findViewById(R.id.tv_order_create_time);
        mLlPayTime = findViewById(R.id.ll_pay_time);
        mTvOrderPayTime = findViewById(R.id.tv_order_pay_time);
        mLlDeliverGoodsTime = findViewById(R.id.ll_deliver_goods_time);
        mTvDeliverGoodsTime = findViewById(R.id.tv_deliver_goods_time);
        mLlFinishTime = findViewById(R.id.ll_finish_time);
        mTvFinishTime = findViewById(R.id.tv_finish_time);
        mLlCancleTime = findViewById(R.id.ll_cancle_time);
        mTvOrderCancleTime = findViewById(R.id.tv_order_cancle_time);

        mSllSerialno = findViewById(R.id.sll_serialno);
        mLlIptOtpNo = findViewById(R.id.ll_ipt_otp_no);
        mTvIptOtpNo = findViewById(R.id.tv_ipt_otp_no);
        mLlRefundSerialno = findViewById(R.id.ll_refund_serialno);
        mTvRefundSerialno = findViewById(R.id.tv_refund_serialno);
        mLlOrderTotalPrice = findViewById(R.id.ll_order_total_price);
        mTvOrderTotalPrice = findViewById(R.id.tv_order_total_price);
        mLlFreight = findViewById(R.id.ll_freight);
        mTvFreight = findViewById(R.id.tv_freight);
        mLlMedicalPay = findViewById(R.id.ll_medical_pay);
        mTvMedicalPay = findViewById(R.id.tv_medical_pay);
        mLlMedicalPersonPay = findViewById(R.id.ll_medical_person_pay);
        mTvMedicalPersonPay = findViewById(R.id.tv_medical_person_pay);
        mLlEnterprisePay = findViewById(R.id.ll_enterprise_pay);
        mTvEnterprisePay = findViewById(R.id.tv_enterprise_pay);
        mLlPsnCashPay02 = findViewById(R.id.ll_psn_cash_pay02);
        mTvPsnCashPay02 = findViewById(R.id.tv_psn_cash_pay02);

        mSllWaitpayDetail = findViewById(R.id.sll_waitpay_detail);
        mLlOnlinePrice = findViewById(R.id.ll_online_price);
        mTvOnlinePrice = findViewById(R.id.tv_online_price);
        mLlFundPrice = findViewById(R.id.ll_fund_price);
        mTvFundPrice = findViewById(R.id.tv_fund_price);
        mLlMedicalPersonPayMoney = findViewById(R.id.ll_medical_person_pay_money);
        mTvMedicalPersonPayMoney = findViewById(R.id.tv_medical_person_pay_money);
        mLlEnterprisePayMoney = findViewById(R.id.ll_enterprise_pay_money);
        mTvEnterprisePayMoney = findViewById(R.id.tv_enterprise_pay_money);
        mLlHasPrice = findViewById(R.id.ll_has_price);
        mTvHasPrice = findViewById(R.id.tv_has_price);
        mLlMedicalBalance = findViewById(R.id.ll_medical_balance);
        mTvMedicalBalance = findViewById(R.id.tv_medical_balance);
        mLlEnterpriseBalance = findViewById(R.id.ll_enterprise_balance);
        mTvEnterpriseBalance = findViewById(R.id.tv_enterprise_balance);

        mTvDescribe02 = findViewById(R.id.tv_describe02);
        sllPayType = findViewById(R.id.sll_pay_type);
        srbGroupBuyType = findViewById(R.id.srb_group_buy_type);
        srbMedicalSelfBuy = findViewById(R.id.srb_medical_self_buy);
        srbEnterpriseBuy = findViewById(R.id.srb_enterprise_buy);

        mLlBtn = findViewById(R.id.ll_btn);
        mTv01 = findViewById(R.id.tv_01);
        mTv02 = findViewById(R.id.tv_02);
        mTv03 = findViewById(R.id.tv_03);
        mTv04 = findViewById(R.id.tv_04);
    }

    @Override
    public void initData() {
        Intent intent = getIntent();
        orderNo = intent.getStringExtra("orderNo");
        type = intent.getStringExtra("type");
        from = intent.getIntExtra("from", 0);
        super.initData();
        MsgBus.sendPayStatusRefresh_user().observe(this, mPayStatusObserve);
        MsgBus.sendAppletData_user().observe(this, mAppletObserver);
        BroadCastReceiveUtils.registerLocalReceiver(this, PushMessageReceiver.SEND_PUSH_SPEECH_SOUNDS, mPushReceiver);

        showWaitDialog();
        getDetailData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mSrlPrescrInfo.setOnClickListener(this);
        mTvLookPrescr.setOnClickListener(this);

        mTvOrderIdCopy.setOnClickListener(this);
        mTvRefundNoCopy.setOnClickListener(this);
        mTvBusinessWxCopy.setOnClickListener(this);
        mTvBuyerWxCopy.setOnClickListener(this);

        mTv01.setOnClickListener(this);
        mTv02.setOnClickListener(this);
        mTv03.setOnClickListener(this);
        mTv04.setOnClickListener(this);
    }

    //获取详情数据
    protected void getDetailData() {
        OrderDetailModel_user.sendOrderDetailRequest_user(TAG, orderNo, new CustomerJsonCallBack<OrderDetailModel_user>() {
            @Override
            public void onRequestError(OrderDetailModel_user returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(OrderDetailModel_user returnData) {
                hideWaitDialog();
                detailData = returnData.getData();
                if (isFinishing()) return;
                if (detailData != null) {
                    setDetailData(returnData.getServer_time());
                    hasOnlinePay = detailData.getOnlineAmount();
//                    if (!EmptyUtils.isEmpty(type) && "pay".equals(type)) {
//                        mTitleBar.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                onPay();
//                            }
//                        }, 400);
//                    }
                }
            }
        });
    }

    //设置详情数据
    private void setDetailData(String server_time) {
        // 2022-10-31 判断是否有带处方药
        hasPrescr = false;
        mLlOrderListContain.removeAllViews();
        totalPrice = "0";
        totalNum = 0;
        List<OrderDetailModel_user.DataBean.OrderGoodsBean> orderGoods = detailData.getOrderGoods();
        for (int i = 0, size = orderGoods.size(); i < size; i++) {
            OrderDetailModel_user.DataBean.OrderGoodsBean orderGood = orderGoods.get(i);
            if ("2".equals(orderGood.getItemType())) {
                hasPrescr = true;
            }

            totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(orderGood.getPrice())
                    .multiply(new BigDecimal(String.valueOf(orderGood.getGoodsNum())))).toPlainString();
            totalNum += orderGood.getGoodsNum();

            //设置商品信息
            View view = LayoutInflater.from(this).inflate(R.layout.layout_orderlist_product_item, mLlOrderListContain, false);
            ImageView ivPic = view.findViewById(R.id.iv_pic);
            ShapeTextView stvOtc = view.findViewById(R.id.stv_otc);
            ShapeTextView stvEnterpriseTag = view.findViewById(R.id.stv_enterprise_tag);
            TagTextView tvTitle = view.findViewById(R.id.tv_title);
            TextView tvEstimatePrice = view.findViewById(R.id.tv_estimate_price);
            TextView tvRealPrice = view.findViewById(R.id.tv_real_price);
            View lineItem = view.findViewById(R.id.line_item);

            GlideUtils.loadImg(orderGood.getBannerAccessoryUrl1(), ivPic, R.drawable.ic_placeholder_bg,
                    new RoundedCornersTransformation(getResources().getDimensionPixelSize(R.dimen.dp_16), 0));
            //判断是否是处方药
            if ("1".equals(orderGood.getItemType())) {
                stvOtc.setText("OTC");
                stvOtc.setVisibility(View.VISIBLE);
            } else if ("2".equals(orderGood.getItemType())) {
                stvOtc.setText("处方药");
                stvOtc.setVisibility(View.VISIBLE);
            } else {
                stvOtc.setVisibility(View.GONE);
            }
            //判断是否是平台药 若是 需要展示医保
            tags.clear();
            if ("1".equals(orderGood.getIsPlatformDrug())) {//是平台药
                tags.add(new TagTextBean("医保", R.color.color_ee8734));
                tvTitle.setContentAndTag(EmptyUtils.strEmpty(orderGood.getGoodsName()), tags);
            } else {
                tvTitle.setContentAndTag(EmptyUtils.strEmpty(orderGood.getGoodsName()), tags);
            }

            //判断1.用户是否是企业基金用户 2.是否支持企业基金支付
            if ("1".equals(InsuredLoginUtils.getIsEnterpriseFundPay())&&"1".equals(orderGood.getIsEnterpriseFundPay())) {
                stvEnterpriseTag.setVisibility(View.VISIBLE);
            } else {
                stvEnterpriseTag.setVisibility(View.GONE);
            }

            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(orderGood.getPrice())));
            tvRealPrice.setText("x" + orderGood.getGoodsNum());
            //2022-10-10 最后一个item的分割线需要隐藏
            if (i == size - 1) {
                lineItem.setVisibility(View.GONE);
            } else {
                lineItem.setVisibility(View.VISIBLE);
            }
            mLlOrderListContain.addView(view);
        }
        //需要区分是否是自提订单
        if ("1".equals(detailData.getShippingMethod())) {//自提
            mTvDeliveryType.setText("自提");
            mLine01.setVisibility(View.GONE);
            mIvAddressSeize.setVisibility(View.GONE);
            mTvAddressInfo.setVisibility(View.GONE);
        } else {//商家配送
            mTvDeliveryType.setText("商家配送");
            mLine01.setVisibility(View.VISIBLE);
            mIvAddressSeize.setVisibility(View.VISIBLE);
            mTvAddressInfo.setVisibility(View.VISIBLE);

            mTvAddressInfo.setText(detailData.getFullName() + "    " + StringUtils.encryptionPhone(detailData.getPhone()) + "\n" + detailData.getLocation() + detailData.getAddress());
        }
        GlideUtils.loadImg(detailData.getStoreAccessoryUrl(), mIvHeadProduct, R.drawable.ic_placeholder_bg, new CircleCrop());
        mTvShopNam.setText(EmptyUtils.strEmpty(detailData.getStoreName()));
        //退款状态 是否已退款(0、 未退款 1、已退款）
        if ("1".equals(detailData.getIsRefunded())) {
            mTvOrderRefundStatus.setVisibility(View.VISIBLE);
        } else {
            mTvOrderRefundStatus.setVisibility(View.GONE);
        }
        mTvTotalNum.setText("共" + totalNum + "件商品");
        mTvTotalPrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(totalPrice)));

        //退款编号
        mLlRefundNo.setVisibility(EmptyUtils.isEmpty(detailData.getOrderRefundNo()) ? View.GONE : View.VISIBLE);

        mTvOrderId02.setText(EmptyUtils.strEmpty(detailData.getOrderNo()));
        mTvRefundNo.setText(EmptyUtils.strEmpty(detailData.getOrderRefundNo()));
        mTvBusinessPhone.setText(EmptyUtils.strEmpty(detailData.getTelephone()));
        mTvBusinessWx.setText(EmptyUtils.strEmpty(detailData.getWechat()));
        mTvBuyerWx.setText(EmptyUtils.strEmpty(detailData.getBuyerWeChat()));
        mTvBuyerMsg.setText(EmptyUtils.strEmpty(detailData.getBuyerMessage()));
        mTvOrderCreateTime.setText(EmptyUtils.strEmpty(detailData.getCreateDT()));

        //医保结算 费用明细
        mTvIptOtpNo.setText(EmptyUtils.strEmptyToText(detailData.getIpt_otp_no(), "--"));
        mTvRefundSerialno.setText(EmptyUtils.strEmpty(detailData.getOrderRefundSerialNo()));
        mTvOrderTotalPrice.setText(EmptyUtils.strEmpty(detailData.getTotalAmount()));//订单总额
        mTvFreight.setText(EmptyUtils.strEmpty(detailData.getLogisticsFee()));//运费
        mTvMedicalPay.setText(EmptyUtils.strEmptyToText(detailData.getFund_pay_sumamt(), "0.0"));//医保报销
        mTvMedicalPersonPay.setText(EmptyUtils.strEmptyToText(detailData.getAcct_pay(), "0.0"));//医保个人账户支付
        mTvEnterprisePay.setText(EmptyUtils.strEmptyToText(detailData.getEnterpriseFundPay(), "0.0"));//企业基金支付
        mTvPsnCashPay02.setText(EmptyUtils.strEmptyToText(detailData.getOnlineAmount(), "0.0"));//个人现金支付

        //判断企业基金余额是否大于0 大于的0 的情况 优先使用
        double enterpriseBalance = new BigDecimal(detailData.getEnterpriseFundAmount()).doubleValue();
        if (enterpriseBalance > 0) {//优先使用企业基金
            srbMedicalSelfBuy.setChecked(false);
            srbEnterpriseBuy.setChecked(true);
        } else {
            srbMedicalSelfBuy.setChecked(true);
            srbEnterpriseBuy.setChecked(false);
        }

        //请求数据 成功
        switch (detailData.getOrderStatus()) {
            case "1"://待付款
                onWaitPay(server_time);
                break;
            case "2"://待发货
                waitDelivery();
                break;
            case "3"://待收货
                waitReceive();
                break;
            case "4"://已完成
                complete();
                break;
            case "5"://用户取消订单（未支付）
                cancle1();
                break;
            case "6"://系统取消订单（未支付完成）
                //医保结算状态(0、 未结算 1、结算成功）
                if ("1".equals(detailData.getSettlementStatus())) {//系统取消订单(医保已支付，线上支付未完成）
                    cancle2();
                } else {//系统取消订单(医保支付未完成，线上支付未完成）
                    cancle1();
                }
                break;
            case "7"://系统取消订单（支付完成）
                cancle3();
                break;
            case "8"://商家取消订单（支付完成）
                cancle4();
                break;
        }
    }

    //待付款状态
    private void onWaitPay(String server_time) {
        if ("0".equals(detailData.getIsPrescription()) || "2".equals(detailData.getIsPrescription())) {
            setTitle("待付款");
            mTvDescribe.setVisibility(View.VISIBLE);
        } else {
            setTitle("待开方");
            mTvDescribe.setVisibility(View.GONE);
        }
        mTvStatus.setText("待买家付款，剩余");
        mTvTime.setVisibility(View.VISIBLE);
        mTvTime.setText("30:00");
        //最后付款时间
        mLlPayTime.setVisibility(View.GONE);
        //成交时间
        mLlFinishTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.GONE);
        //费用明细
        mLlIptOtpNo.setVisibility(View.GONE);
        mLlRefundSerialno.setVisibility(View.GONE);
        mLlMedicalPay.setVisibility(View.GONE);
        mLlMedicalPersonPay.setVisibility(View.GONE);
        mLlEnterprisePay.setVisibility(View.GONE);
        mLlPsnCashPay02.setVisibility(View.GONE);

        //1.先判断是自费还是医保支付
        mSllWaitpayDetail.setVisibility(View.VISIBLE);
        mLlOnlinePrice.setVisibility(View.VISIBLE);
        mTvOnlinePrice.setText(EmptyUtils.strEmpty(detailData.getTotalAmount()));
        if ("1".equals(detailData.getPurchasingDrugsMethod())) {//1自助购药（自费结算）
            mLlFundPrice.setVisibility(View.GONE);
            mLlMedicalPersonPayMoney.setVisibility(View.GONE);
            if ("1".equals(detailData.getEnterpriseFundPayStatus())) {//企业基金已支付
                mLlEnterprisePayMoney.setVisibility(View.VISIBLE);
                mTvEnterprisePayMoney.setText(EmptyUtils.strEmpty(detailData.getEnterpriseFundPay()));
            } else {//企业基金未支付
                mLlEnterprisePayMoney.setVisibility(View.GONE);
            }
            mLlHasPrice.setVisibility(View.GONE);
            mLlMedicalBalance.setVisibility(View.GONE);

            double enterpriseBalance = new BigDecimal(detailData.getEnterpriseFundAmount()).doubleValue();
            if ("1".equals(detailData.getIsEnterpriseFundPay()) && enterpriseBalance > 0) {//企业基金支付
                mLlEnterpriseBalance.setVisibility(View.VISIBLE);
                mTvEnterpriseBalance.setText(EmptyUtils.strEmpty(detailData.getEnterpriseFundAmount()));
            } else {//不能企业基金支付
                mLlEnterpriseBalance.setVisibility(View.GONE);
            }
        } else {//2、城乡居民医保/职工医保购药（医保结算）3、慢特病购药（医保结算）4、两病购药（医保结算）
            mLlFundPrice.setVisibility(View.VISIBLE);
            if ("1".equals(detailData.getSettlementStatus())) {//医保个人账户已支付
                mLlMedicalPersonPayMoney.setVisibility(View.VISIBLE);
                mTvMedicalPersonPayMoney.setText(EmptyUtils.strEmpty(detailData.getAcct_pay()));
            } else {//医保个人账户未支付
                mLlMedicalPersonPayMoney.setVisibility(View.GONE);
            }
            if ("1".equals(detailData.getEnterpriseFundPayStatus())) {//企业基金已支付
                mLlEnterprisePayMoney.setVisibility(View.VISIBLE);
                mTvEnterprisePayMoney.setText(EmptyUtils.strEmpty(detailData.getEnterpriseFundPay()));
            } else {//企业基金未支付
                mLlEnterprisePayMoney.setVisibility(View.GONE);
            }
            mLlHasPrice.setVisibility(View.VISIBLE);
            mLlMedicalBalance.setVisibility(View.VISIBLE);

            mTvFundPrice.setText(EmptyUtils.strEmpty(detailData.getPre_fund_pay_sumamt()));
            mTvHasPrice.setText(EmptyUtils.strEmpty(detailData.getOnlineAmount()));
            mTvMedicalBalance.setText(EmptyUtils.strEmpty(detailData.getPre_balc()));

            double enterpriseBalance = new BigDecimal(detailData.getEnterpriseFundAmount()).doubleValue();
            if ("1".equals(detailData.getIsEnterpriseFundPay()) && enterpriseBalance > 0) {//企业基金支付
                mLlEnterpriseBalance.setVisibility(View.VISIBLE);
                mTvEnterpriseBalance.setText(EmptyUtils.strEmpty(detailData.getEnterpriseFundAmount()));
            } else {//不能企业基金支付
                mLlEnterpriseBalance.setVisibility(View.GONE);
            }
        }

        //选择支付方式（企业基金 或者 医保个人余额）---只有待付款、医保支付的时候 才会展示
        // 2022-10-31 判断是否有带处方药
        mTvDescribe02.setVisibility(View.VISIBLE);
        if ("1".equals(detailData.getPurchasingDrugsMethod())) {//1自助购药（自费结算）
            sllPayType.setVisibility(View.GONE);

            if (hasPrescr) {
                mTvDescribe02.setText("注意:1." + describe_01 + "\n2." + describe_02);
            } else {
                mTvDescribe02.setText("注意:" + describe_02);
            }
        } else {//2、城乡居民医保/职工医保购药（医保结算）3、慢特病购药（医保结算）4、两病购药（医保结算）
            if ("1".equals(detailData.getSettlementStatus()) || "1".equals(detailData.getEnterpriseFundPayStatus())) {//医保个人账户已支付 或者 企业基金已支付
                sllPayType.setVisibility(View.GONE);
            } else {
                sllPayType.setVisibility(View.VISIBLE);
            }

            if (hasPrescr) {
                mTvDescribe02.setText("注意:1." + describe_01 + "\n2." + describe_03);
            } else {
                mTvDescribe02.setText("注意:" + describe_03);
            }
        }

        mTv01.setVisibility(View.GONE);
        mTv02.setVisibility(View.VISIBLE);
        mTv02.setText("取消订单");
        //2022-10-31 判断是否有带处方药
        //是否开处方(0 不开处方 1、待开处方 2、已开处方3、待医师确认处方4、第三方开处方5.未提交问诊信息)
        switch (detailData.getIsPrescription()) {
            case "0":
                mTv04.setVisibility(View.GONE);
                mTv03.setVisibility(View.VISIBLE);
                mTv03.setText("立即支付");
                break;
            case "1":
            case "3":
            case "4":
            case "5":
                mTv04.setVisibility(View.GONE);
                mTv03.setVisibility(View.VISIBLE);
                mTv03.setText("开处方");
                break;
            case "2":
                mTv04.setVisibility(View.VISIBLE);
                mTv04.setText("查看处方");
                mTv03.setVisibility(View.VISIBLE);
                mTv03.setText("立即支付");
                break;
        }

        //计算  定时器时间==30分钟-已过去多少时间
        long pastTimes = new BigDecimal(DateUtil.dateToStamp2(server_time)).subtract(new BigDecimal(DateUtil.dateToStamp2(detailData.getCreateDT()))).longValue();
        setTimeStart(30 * 60 * 1000 - pastTimes);
    }

    //待发货
    private void waitDelivery() {
        setTitle("待发货");
        mTvStatus.setText("待卖家发货");
        mTvTime.setVisibility(View.GONE);
        mTvDescribe.setVisibility(View.GONE);
        if (mCustomCountDownTimer != null) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }

        //最后付款时间
        mLlPayTime.setVisibility(View.VISIBLE);
        mTvOrderPayTime.setText(EmptyUtils.strEmpty(detailData.getLastPaytTime()));
        //成交时间
        mLlFinishTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.GONE);
        //费用明细
        if ("1".equals(detailData.getPurchasingDrugsMethod())) {//自助购药
            mLlIptOtpNo.setVisibility(View.GONE);
            mLlMedicalPay.setVisibility(View.GONE);
            mLlMedicalPersonPay.setVisibility(View.GONE);
        } else {
            mLlIptOtpNo.setVisibility(View.VISIBLE);
            mLlMedicalPay.setVisibility(View.VISIBLE);
            mLlMedicalPersonPay.setVisibility(View.VISIBLE);
        }

        mLlRefundSerialno.setVisibility(View.GONE);
        mLlEnterprisePay.setVisibility(View.VISIBLE);
        mLlPsnCashPay02.setVisibility(View.VISIBLE);
        //待付款支付明细
        mSllWaitpayDetail.setVisibility(View.GONE);
        //选择支付方式（企业基金 或者 医保个人余额）
        sllPayType.setVisibility(View.GONE);
        //处方提示语
        mTvDescribe02.setVisibility(View.GONE);

        mTv01.setVisibility(View.VISIBLE);
        mTv01.setText("联系商家");
        mTv02.setVisibility(View.GONE);
        mTv03.setVisibility(View.VISIBLE);
        mTv03.setText("提醒发货");
        if (hasPrescr) {//有处方药
            mTv04.setVisibility(View.VISIBLE);
            mTv04.setText("查看处方");
        } else {//没有处方药
            mTv04.setVisibility(View.GONE);
        }
    }

    //待收货
    private void waitReceive() {
        setTitle("待收货");
        mTvStatus.setText("待买家确认收货");
        mTvTime.setVisibility(View.GONE);
        mTvDescribe.setVisibility(View.GONE);

        //最后付款时间
        mLlPayTime.setVisibility(View.VISIBLE);
        mTvOrderPayTime.setText(EmptyUtils.strEmpty(detailData.getLastPaytTime()));
        //发货时间
        mLlDeliverGoodsTime.setVisibility(View.VISIBLE);
        mTvDeliverGoodsTime.setText(EmptyUtils.strEmpty(detailData.getDeliveryTime()));
        //成交时间
        mLlFinishTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.GONE);
        //费用明细
        //费用明细
        if ("1".equals(detailData.getPurchasingDrugsMethod())) {//自助购药
            mLlIptOtpNo.setVisibility(View.GONE);
            mLlMedicalPay.setVisibility(View.GONE);
            mLlMedicalPersonPay.setVisibility(View.GONE);
        } else {
            mLlIptOtpNo.setVisibility(View.VISIBLE);
            mLlMedicalPay.setVisibility(View.VISIBLE);
            mLlMedicalPersonPay.setVisibility(View.VISIBLE);
        }
        mLlRefundSerialno.setVisibility(View.GONE);
        mLlEnterprisePay.setVisibility(View.VISIBLE);
        mLlPsnCashPay02.setVisibility(View.VISIBLE);
        //待付款支付明细
        mSllWaitpayDetail.setVisibility(View.GONE);
        //选择支付方式（企业基金 或者 医保个人余额）
        sllPayType.setVisibility(View.GONE);
        //处方提示语
        mTvDescribe02.setVisibility(View.GONE);

        mTv01.setVisibility(View.VISIBLE);
        mTv01.setText("联系商家");
        mTv02.setVisibility(View.VISIBLE);
        mTv02.setText("查看物流");
        mTv03.setVisibility(View.VISIBLE);
        mTv03.setText("确认收货");
        if (hasPrescr) {//有处方药
            mTv04.setVisibility(View.VISIBLE);
            mTv04.setText("查看处方");
        } else {//没有处方药
            mTv04.setVisibility(View.GONE);
        }
    }

    //已完成
    private void complete() {
        setTitle("已完成");
        mTvStatus.setText("已完成");
        mTvTime.setVisibility(View.GONE);
        mTvDescribe.setVisibility(View.GONE);

        //最后付款时间
        mLlPayTime.setVisibility(View.VISIBLE);
        mTvOrderPayTime.setText(EmptyUtils.strEmpty(detailData.getLastPaytTime()));
        //发货时间
        mLlDeliverGoodsTime.setVisibility(View.VISIBLE);
        mTvDeliverGoodsTime.setText(EmptyUtils.strEmpty(detailData.getDeliveryTime()));
        //成交时间
        mLlFinishTime.setVisibility(View.VISIBLE);
        mTvFinishTime.setText(EmptyUtils.strEmpty(detailData.getConfirmTime()));
        //订单取消时间
        mLlCancleTime.setVisibility(View.GONE);
        //费用明细
        //费用明细
        if ("1".equals(detailData.getPurchasingDrugsMethod())) {//自助购药
            mLlIptOtpNo.setVisibility(View.GONE);
            mLlMedicalPay.setVisibility(View.GONE);
            mLlMedicalPersonPay.setVisibility(View.GONE);
        } else {
            mLlIptOtpNo.setVisibility(View.VISIBLE);
            mLlMedicalPay.setVisibility(View.VISIBLE);
            mLlMedicalPersonPay.setVisibility(View.VISIBLE);
        }
        mLlRefundSerialno.setVisibility(View.GONE);
        mLlEnterprisePay.setVisibility(View.VISIBLE);
        mLlPsnCashPay02.setVisibility(View.VISIBLE);
        //待付款支付明细
        mSllWaitpayDetail.setVisibility(View.GONE);
        //选择支付方式（企业基金 或者 医保个人余额）
        sllPayType.setVisibility(View.GONE);
        //处方提示语
        mTvDescribe02.setVisibility(View.GONE);

        mTv01.setVisibility(View.VISIBLE);
        mTv01.setText("加入购物车");
        mTv02.setVisibility(View.VISIBLE);
        //2022-10-31 是否自提
        if ("1".equals(detailData.getShippingMethod())) {//自提
            mTv02.setText("联系商家");
        } else {//物流
            mTv02.setText("查看物流");
        }
        mTv03.setVisibility(View.GONE);
        if (hasPrescr) {//有处方药
            mTv04.setVisibility(View.VISIBLE);
            mTv04.setText("查看处方");
        } else {//没有处方药
            mTv04.setVisibility(View.GONE);
        }
    }

    //用户取消订单（未支付）
    private void cancle1() {
        setTitle("已取消");
        mTvStatus.setText("已取消");
        mTvTime.setVisibility(View.GONE);
        mTvDescribe.setVisibility(View.GONE);

        //最后付款时间
        mLlPayTime.setVisibility(View.GONE);
        //发货时间
        mLlDeliverGoodsTime.setVisibility(View.GONE);
        //成交时间
        mLlFinishTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.VISIBLE);
        mTvOrderCancleTime.setText(EmptyUtils.strEmpty(detailData.getCancelTime()));
        //费用明细
        mLlIptOtpNo.setVisibility(View.GONE);
        mLlRefundSerialno.setVisibility(View.GONE);
        mLlMedicalPay.setVisibility(View.GONE);
        mLlMedicalPersonPay.setVisibility(View.GONE);
        mLlEnterprisePay.setVisibility(View.GONE);
        mLlPsnCashPay02.setVisibility(View.GONE);
        //待付款支付明细
        mSllWaitpayDetail.setVisibility(View.GONE);
        //选择支付方式（企业基金 或者 医保个人余额）
        sllPayType.setVisibility(View.GONE);
        //处方提示语
        mTvDescribe02.setVisibility(View.GONE);

        mTv01.setVisibility(View.VISIBLE);
        mTv01.setText("加入购物车");
        mTv02.setVisibility(View.GONE);
        mTv03.setVisibility(View.VISIBLE);
        mTv03.setText("删除订单");
        mTv04.setVisibility(View.GONE);
    }

    //系统取消订单（未支付完成）
    private void cancle2() {
        setTitle("已取消");
        mTvStatus.setText("已取消");
        mTvTime.setVisibility(View.GONE);
        mTvDescribe.setVisibility(View.GONE);

        //最后付款时间
        mLlPayTime.setVisibility(View.GONE);
        //发货时间
        mLlDeliverGoodsTime.setVisibility(View.GONE);
        //成交时间
        mLlFinishTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.VISIBLE);
        mTvOrderCancleTime.setText(EmptyUtils.strEmpty(detailData.getCancelTime()));
        //费用明细
        if ("1".equals(detailData.getPurchasingDrugsMethod())) {//自助购药
            mLlIptOtpNo.setVisibility(View.GONE);
            mLlMedicalPay.setVisibility(View.GONE);
            mLlMedicalPersonPay.setVisibility(View.GONE);
        } else {
            mLlIptOtpNo.setVisibility(View.VISIBLE);
            mLlMedicalPay.setVisibility(View.VISIBLE);
            mLlMedicalPersonPay.setVisibility(View.VISIBLE);
        }
        mLlRefundSerialno.setVisibility(View.VISIBLE);
        mLlEnterprisePay.setVisibility(View.GONE);
        mLlPsnCashPay02.setVisibility(View.GONE);
        //待付款支付明细
        mSllWaitpayDetail.setVisibility(View.GONE);
        //选择支付方式（企业基金 或者 医保个人余额）
        sllPayType.setVisibility(View.GONE);
        //处方提示语
        mTvDescribe02.setVisibility(View.GONE);

        mTv01.setVisibility(View.VISIBLE);
        mTv01.setText("加入购物车");
        mTv02.setVisibility(View.VISIBLE);
        mTv02.setText("联系商家");
        mTv03.setVisibility(View.VISIBLE);
        mTv03.setText("删除订单");
        mTv04.setVisibility(View.GONE);
    }

    //系统取消订单（支付完成）
    private void cancle3() {
        setTitle("已取消");
        mTvStatus.setText("已取消");
        mTvTime.setVisibility(View.GONE);
        mTvDescribe.setVisibility(View.GONE);

        //最后付款时间
        mLlPayTime.setVisibility(View.VISIBLE);
        mTvOrderPayTime.setText(EmptyUtils.strEmpty(detailData.getLastPaytTime()));
        //发货时间
        // 2023/6/10 此处还得加个判断 商户端那边取消的时候 订单是否是处在代发货的状态 如果是 那么查看物流就该隐藏
        if ("0".equals(detailData.getIsViewLogistics())) {
            mLlDeliverGoodsTime.setVisibility(View.VISIBLE);
            mTvDeliverGoodsTime.setText(EmptyUtils.strEmpty(detailData.getDeliveryTime()));
        } else {
            mLlDeliverGoodsTime.setVisibility(View.GONE);
        }
        //成交时间
        mLlFinishTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.VISIBLE);
        mTvOrderCancleTime.setText(EmptyUtils.strEmpty(detailData.getCancelTime()));
        //费用明细
        //费用明细
        if ("1".equals(detailData.getPurchasingDrugsMethod())) {//自助购药
            mLlIptOtpNo.setVisibility(View.GONE);
            mLlMedicalPay.setVisibility(View.GONE);
            mLlMedicalPersonPay.setVisibility(View.GONE);
        } else {
            mLlIptOtpNo.setVisibility(View.VISIBLE);
            mLlMedicalPay.setVisibility(View.VISIBLE);
            mLlMedicalPersonPay.setVisibility(View.VISIBLE);
        }
        mLlRefundSerialno.setVisibility(View.VISIBLE);
        mLlEnterprisePay.setVisibility(View.VISIBLE);
        mLlPsnCashPay02.setVisibility(View.VISIBLE);
        //待付款支付明细
        mSllWaitpayDetail.setVisibility(View.GONE);
        //选择支付方式（企业基金 或者 医保个人余额）
        sllPayType.setVisibility(View.GONE);
        //处方提示语
        mTvDescribe02.setVisibility(View.GONE);

        mTv01.setVisibility(View.VISIBLE);
        mTv01.setText("加入购物车");
        mTv02.setVisibility(View.VISIBLE);
        mTv02.setText("联系商家");
        mTv03.setVisibility(View.VISIBLE);
        mTv03.setText("退款进度");
        mTv04.setVisibility(View.GONE);
    }

    //商家取消订单（支付完成）
    private void cancle4() {
        setTitle("已取消");
        mTvStatus.setText("已取消");
        mTvTime.setVisibility(View.GONE);
        mTvDescribe.setVisibility(View.GONE);

        //最后付款时间
        mLlPayTime.setVisibility(View.VISIBLE);
        mTvOrderPayTime.setText(EmptyUtils.strEmpty(detailData.getLastPaytTime()));
        //发货时间
        if ("0".equals(detailData.getIsViewLogistics())) {
            mLlDeliverGoodsTime.setVisibility(View.VISIBLE);
            mTvDeliverGoodsTime.setText(EmptyUtils.strEmpty(detailData.getDeliveryTime()));
        } else {
            mLlDeliverGoodsTime.setVisibility(View.GONE);
        }
        //成交时间
        mLlFinishTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.VISIBLE);
        mTvOrderCancleTime.setText(EmptyUtils.strEmpty(detailData.getCancelTime()));
        //费用明细
        //费用明细
        if ("1".equals(detailData.getPurchasingDrugsMethod())) {//自助购药
            mLlIptOtpNo.setVisibility(View.GONE);
            mLlMedicalPay.setVisibility(View.GONE);
            mLlMedicalPersonPay.setVisibility(View.GONE);
        } else {
            mLlIptOtpNo.setVisibility(View.VISIBLE);
            mLlMedicalPay.setVisibility(View.VISIBLE);
            mLlMedicalPersonPay.setVisibility(View.VISIBLE);
        }
        mLlRefundSerialno.setVisibility(View.VISIBLE);
        mLlEnterprisePay.setVisibility(View.VISIBLE);
        mLlPsnCashPay02.setVisibility(View.VISIBLE);
        //待付款支付明细
        mSllWaitpayDetail.setVisibility(View.GONE);
        //选择支付方式（企业基金 或者 医保个人余额）
        sllPayType.setVisibility(View.GONE);
        //处方提示语
        mTvDescribe02.setVisibility(View.GONE);

        if ("1".equals(detailData.getShippingMethod())) {//自提
            mTv01.setVisibility(View.GONE);
        } else {//商家配送
            // 2023/6/10 此处还得加个判断 商户端那边取消的时候 订单是否是处在代发货的状态 如果是 那么查看物流就该隐藏
            if ("0".equals(detailData.getIsViewLogistics())) {
                mTv01.setVisibility(View.VISIBLE);
                mTv01.setText("查看物流");
            } else {
                mTv01.setVisibility(View.GONE);
            }
        }

        mTv02.setVisibility(View.VISIBLE);
        mTv02.setText("联系商家");
        mTv03.setVisibility(View.VISIBLE);
        mTv03.setText("退款进度");
        mTv04.setVisibility(View.GONE);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        if (R.id.tv_look_prescr == v.getId()) {//查看处方
            ElectPrescActivity_user.newIntance(OrderDetailActivity_user.this, orderNo);
        } else if (R.id.srl_prescr_info == v.getId()) {//填写问诊信息开具处方
            //是否开处方(0 不开处方 1、待开处方 2、已开处方 3、待医师确认处方
            if ("3".equals(detailData.getIsPrescription())) {
                showShortToast("问诊信息已提交，等待医师开处方");
            } else {
                Intent intent = new Intent(OrderDetailActivity_user.this, InquiryInfoActivity.class);
                intent.putExtra("orderNo", orderNo);
                intent.putExtra("fixmedins_code", detailData.getFixmedins_code());
                intent.putExtra("associatedDiseases", getAssociatedDiseases());
                startActivityForResult(intent, new OnActivityCallback() {
                    @Override
                    public void onActivityResult(int resultCode, @Nullable Intent data) {
                        showWaitDialog();
                        getDetailData();
                    }
                });
            }
        } else if (R.id.tv_order_id_copy == v.getId()) {
            RxClipboardTool.copyText(OrderDetailActivity_user.this, mTvOrderId02.getText().toString());
        } else if (R.id.tv_refund_no_copy == v.getId()) {
            RxClipboardTool.copyText(OrderDetailActivity_user.this, mTvRefundNo.getText().toString());
        } else if (R.id.tv_business_wx_copy == v.getId()) {
            RxClipboardTool.copyText(OrderDetailActivity_user.this, mTvBusinessWx.getText().toString());
        } else if (R.id.tv_buyer_wx_copy == v.getId()) {
            RxClipboardTool.copyText(OrderDetailActivity_user.this, mTvBuyerWx.getText().toString());
        } else {
            switch (((TextView) v).getText().toString()) {
                case "取消订单":
                    onCancle();
                    break;
                case "立即支付":
                    onPay();
                    break;
                case "提醒发货":
                    remindShipment();
                    break;
                case "联系商家":
                    onTakePhone(detailData.getTelephone());
                    break;
                case "查看物流":
                    LogisticsActivity.newIntance(OrderDetailActivity_user.this, detailData.getOrderNo());
                    break;
                case "确认收货":
                    confirmReceipt();
                    break;
                case "加入购物车":
                    onAddShopcar(detailData.getOrderNo(), 1);
                    break;
                case "删除订单":
                    onDelete();
                    break;
                case "退款进度":
                    RefundProgressActivity_user.newIntance(OrderDetailActivity_user.this, detailData.getOrderNo(), detailData.getOnlineAmount());
                    break;
                case "开处方":
                    openPrescription();
                    break;
                case "查看处方":
                    if ("1".equals(detailData.getIsThirdPartyPres())){
                        ElectPrescActivity2_user.newIntance(OrderDetailActivity_user.this, orderNo);
                    }else {
                        ElectPrescActivity_user.newIntance(OrderDetailActivity_user.this, orderNo);
                    }
                    break;
            }
        }
    }

    //开启定时器  millisecond:倒计时的时间 1:待付款倒计时
    protected void setTimeStart(long millisecond) {
        //防止内存泄漏
        if (null != mCustomCountDownTimer) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        mCustomCountDownTimer = new CustomCountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long M = (millisUntilFinished % (60 * 60 * 1000)) / (60 * 1000);
                long S = (millisUntilFinished % (60 * 1000)) / 1000;

                mTvTime.setText((M < 10 ? "0" + M : String.valueOf(M)) + ":" + (S < 10 ? "0" + S : String.valueOf(S)));
            }

            @Override
            public void onFinish() {
                mTvTime.setText("00:00");
            }
        };

        mCustomCountDownTimer.start();
    }

    //取消订单 按钮点击事件
    private void onCancle() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(OrderDetailActivity_user.this, "1".equals(detailData.getOrderStatus()) ? "提示" : "确定取消订单吗？",
                "1".equals(detailData.getOrderStatus()) ? "确定取消订单吗？" : "注意：取消订单后医保支付部分将撤销,同时在线支付部分也将原路返回到个人账户。",
                2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        // 2022-10-10 请求接口取消该订单
                        showWaitDialog();
                        BaseModel.sendCancleOrderRequest_user(TAG, orderNo, new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                showShortToast("取消成功");
                                if (0 == from) {
                                    //当订单取消成功之后 前端需要移除该订单的展示
                                    Intent intent = new Intent();
                                    intent.putExtra("OperationType", "cancle");
                                    intent.putExtra("orderNo", orderNo);
                                    setResult(RESULT_OK, intent);
                                }
                                goFinish();
                            }
                        });
                    }
                });
    }

    //联系买家
    private void onTakePhone(String phone) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(OrderDetailActivity_user.this, "提示", "是否拨打买家电话" + phone + "?", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                RxTool.takePhone(OrderDetailActivity_user.this, phone);
            }
        });
    }

    //开处方
    //1.app端提交问诊信息之后，开启120s定时器，提示用户正在分配医师，请稍等。然后app开启接口（这个接口，可以返回一个字段 里面有几个状态 1.正在分配医师 2.医师已分配完成，正在开处方 3.医师已开完处方 4.分配医师失败，跳转第三方开处方）轮询，根据返回的不同状态 展示不同的提示文字。
    //状态：1 就是用户提交问诊信息之后
    //     2 给筛选中的医师分配之后，医师进入开处方页面 （*进入这个状态时，后台应该把定时器关闭掉，防止医师开处方太久 导致超过120s，app端这时候，拿到这个状态，也把读秒定时器停止）
    //     3 医师把开好的处方提交
    //     4 当一直是状态1的时候，时间也超过120s
    //2.如果出现用户提交完问诊信息，然后又没在当前等待开处方页面，之后又通过重新进入订单详情页 《1》当时间还没超时，那么开处方按钮文字改为“查看开处方进度”，点击按钮，进入提交问诊信息页面 《2》如果医师已开完处方，那么按钮直接改为“去支付”《3》如果时间已超时（分配医师失败，跳转第三方开处方），那么按钮直接改为“开处方”，点击之后，跳转第三方开处方
    private void openPrescription() {
        //[{"erpCode":"drp_00016","count":1}]
        if (detailData == null) return;
        if (Constants.Configure.IS_POS) {
            showShortToast("pos机不支持跳转微信小程序开处方");
            return;
        }
        // 2024-03-28 需要等后台确认 具体是使用哪个字段来判断 开处方的状态
        if (!"4".equals(detailData.getIsPrescription())) {//首次开处方 分配医师方案 跳转问诊信息填写页面
            JSONArray jsonArray = new JSONArray();
            List<OrderDetailModel_user.DataBean.OrderGoodsBean> orderGoods = detailData.getOrderGoods();
            if (orderGoods != null) {
                for (int i = 0, size = orderGoods.size(); i < size; i++) {
                    OrderDetailModel_user.DataBean.OrderGoodsBean orderGood = orderGoods.get(i);
                    JSONObject jsonObjectChild = new JSONObject();
                    jsonObjectChild.put("erpCode", EmptyUtils.strEmpty(orderGood.getGoodsNo()));//商品no
                    jsonObjectChild.put("count", orderGood.getGoodsNum());//商品数量

                    jsonArray.add(jsonObjectChild);
                }
            }

            Intent intent = new Intent(OrderDetailActivity_user.this, InquiryInfoActivity.class);
            intent.putExtra("orderNo", orderNo);
            intent.putExtra("fixmedins_code", detailData.getFixmedins_code());
            intent.putExtra("associatedDiseases", getAssociatedDiseases());
            intent.putExtra("drugListJson", jsonArray.toJSONString());
            startActivityForResult(intent, new OnActivityCallback() {
                @Override
                public void onActivityResult(int resultCode, @Nullable Intent data) {
                    showWaitDialog();
                    getDetailData();
                }
            });
        } else {//分配医师方案开处方失败 跳转第三方开处方页面
            List<OrderDetailModel_user.DataBean.OrderGoodsBean> orderGoods = detailData.getOrderGoods();
            if (orderGoods != null) {
                JSONArray jsonArray = new JSONArray();
                for (int i = 0, size = orderGoods.size(); i < size; i++) {
                    OrderDetailModel_user.DataBean.OrderGoodsBean orderGood = orderGoods.get(i);
                    JSONObject jsonObjectChild = new JSONObject();
                    jsonObjectChild.put("erpCode", EmptyUtils.strEmpty(orderGood.getGoodsNo()));//商品no
                    jsonObjectChild.put("count", orderGood.getGoodsNum());//商品数量

                    jsonArray.add(jsonObjectChild);
                }

                isOpenPreing = true;
                WXUtils.goSmallRoutine(OrderDetailActivity_user.this, Constants.Configure.WEZ_APPID,
                        Constants.Configure.WEZ_PATH + "&orderId=" + orderNo + "&drugList=" + jsonArray.toJSONString());
                if (0 == from) {
                    //当订单删除成功之后 前端需要移除该订单的展示
                    Intent intent = new Intent();
                    intent.putExtra("OperationType", "openPre");
                    setResult(RESULT_OK, intent);
                }
            }
        }
    }

    //确认收货
    private void confirmReceipt() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(OrderDetailActivity_user.this, "确认收货吗？", "注意:请确保已取货或已收到货物,确认收货后订单完成。", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                // 2022-10-10 请求接口确认收货
                showWaitDialog();
                BaseModel.sendConfirmReceiptOrderRequest_user(TAG, orderNo, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        if (0 == from) {
                            //当订单确认收货成功之后 前端需要移除该订单的展示
                            Intent intent = new Intent();
                            intent.putExtra("OperationType", "confirm_receipt");
                            intent.putExtra("orderNo", orderNo);
                            setResult(RESULT_OK, intent);
                        }
                        showShortToast("确认收货成功");
                        //2022-10-12 刷新页面
                        getDetailData();
                    }
                });
            }
        });
    }

    //加入购物车
    private void onAddShopcar(String orderNo, int shoppingCartNum) {
        showWaitDialog();
        BaseModel.sendAddOrderShopCarRequest(TAG, orderNo, shoppingCartNum, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("加入购物车成功");
                MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "1"));
            }
        });
    }

    //删除订单
    private void onDelete() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(OrderDetailActivity_user.this, "确定删除此订单吗？", "注意:订单删除后不可恢复,谨慎操作!", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                //2022-10-10 请求接口删除该订单
                showWaitDialog();
                BaseModel.sendDelOrderRequest(TAG, orderNo, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        if (0 == from) {
                            //当订单删除成功之后 前端需要移除该订单的展示
                            Intent intent = new Intent();
                            intent.putExtra("OperationType", "delete");
                            intent.putExtra("orderNo", orderNo);
                            setResult(RESULT_OK, intent);
                        }

                        showShortToast("删除成功");
                        goFinish();
                    }
                });
            }
        });
    }

    //提醒发货
    private void remindShipment() {
        showWaitDialog();
        BaseModel.sendRemindShipmentRequest(TAG, orderNo, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();

                showShortToast("提醒发货成功");
            }
        });
    }

    //能用企业基金， 必须满足2个条件：所购药品的商户支持使用企业基金；客户的企业基金余额大于0.
    //1、客户选择自费---企业基金支付--不足现金支付
    //2、客户选择医保报销-----先做预结算（展示医保基金支付、个人支付金额，并列出企业基金余额）----前端咨询是否优先选择医保个人账户支付
    //2.1是（优先医保个人账户支付）---医保个人账户支付---不足企业基金支付---不足现金支付
    //2.2否---选择企业基金---不足现金支付
    //立即支付
    private void onPay() {
        //1.判断是否是商户配送 那么需要判断金额是否高于起送价
        if ("2".equals(detailData.getShippingMethod())) {//商家配送
            double disff = new BigDecimal(totalPrice).subtract(new BigDecimal(detailData.getStartingPrice())).doubleValue();
            if (disff < 0) {
                showShortToast("您的订单低于起送价" + detailData.getStartingPrice() + "元,无法提交订单");
                return;
            }
        }

        double enterpriseBalance = new BigDecimal(detailData.getEnterpriseFundAmount()).doubleValue();
        if ("1".equals(detailData.getIsEnterpriseFundPay()) && enterpriseBalance > 0) {//支持企业基金支付
            if ("0".equals(InsuredLoginUtils.getIsSetPaymentPwd())) {//未设置密码
                goSetEnterprisePwd();
                return;
            }
        }

        if ("1".equals(detailData.getPurchasingDrugsMethod())) {//1自助购药（自费结算）
            enterprisePay(detailData.getOnlineAmount());
        } else {//2、城乡居民医保/职工医保购药（医保结算）3、慢特病购药（医保结算）4、两病购药（医保结算）
            //4.判断有没有医保结算，再判断结算状态(0、 未结算 1、结算成功）有没有成功，然后判断现金支付金额是不是不为0
            //待调整---得先判断现在是处在企业基金支付还是 现金支付（考虑下 每种类型支付完成之后 由后台给标识是否还需要其他类型支付）
            if ("1".equals(detailData.getSettlementStatus())) {//医保已结算成功 还需企业基金支付或者现金支付的情况
                //医保结算成功 需要判断是否有上传签名 没有的 先上传签名
                if (EmptyUtils.isEmpty(detailData.getAccessoryUrl())) {
                    showHandWritePop();
                } else {
                    enterprisePay(detailData.getOnlineAmount());
                }
            } else {
                if (verificationCodePop == null) {
                    verificationCodePop = new VerificationCodePop(OrderDetailActivity_user.this, new VerificationCodePop.IOptionListener() {
                        @Override
                        public void sendSms(TextView tvTime) {
                            sendSmsData(tvTime);
                        }

                        @Override
                        public void next(String verifCode, TextView tvTime) {
                            if (EmptyUtils.isEmpty(verifCode)) {
                                showShortToast("请输入验证码");
                                return;
                            }
                            onNext(verifCode, tvTime);
                        }
                    });
                }

                verificationCodePop.showPopupWindow();
            }
        }
    }

    //提示用户去设置企业基金支付密码
    private void goSetEnterprisePwd() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(this, "提示", "您还未设置企业基金支付密码，是否去设置？", 2, "不设置", "去设置", R.color.txt_color_666, R.color.color_4970e0,
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        SetEnterprisePwdActivity.newIntance(OrderDetailActivity_user.this);
                    }
                });
    }

    //发送短信
    private void sendSmsData(TextView tvTime) {
        showWaitDialog();
        BaseModel.sendSendsmsRequest(TAG, InsuredLoginUtils.getPhone(), "8", new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();

                //发送短信验证码成功
                tvTime.setEnabled(false);
                showShortToast("验证码已发送");
                tvTime.setTextColor(getResources().getColor(R.color.color_ff0202));
                setSmsTimeStart(60000, tvTime);
            }
        });
    }

    //开启发送短信定时器  millisecond:倒计时的时间
    private void setSmsTimeStart(long millisecond, TextView tvTime) {
        //防止内存泄漏
        if (null != mSmsCountDownTimer) {
            mSmsCountDownTimer.stop();
            mSmsCountDownTimer = null;
        }
        mSmsCountDownTimer = new CustomCountDownTimer(millisecond, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long S = millisUntilFinished / 1000;
                tvTime.setText(S < 10 ? "0" + S + "s" : S + "s");
            }

            @Override
            public void onFinish() {
                tvTime.setEnabled(true);
                tvTime.setTextColor(getResources().getColor(R.color.color_4870e0));
                tvTime.setText("获取验证码");
            }
        };

        mSmsCountDownTimer.start();
    }

    //下一步 输入医保支付密码
    private void onNext(String verifCode, TextView tvTime) {
        this.verifCode = verifCode;
        //防止内存泄漏
        if (null != mSmsCountDownTimer) {
            mSmsCountDownTimer.stop();
            mSmsCountDownTimer = null;
        }
        KeyboardUtils.hideSoftInput(this);
        tvTime.setEnabled(true);
        tvTime.setTextColor(getResources().getColor(R.color.color_4870e0));
        tvTime.setText("获取验证码");
        verificationCodePop.dismiss();
        //初始化密码输入框
        mTitleBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (inputPwdUtils == null) {
                    inputPwdUtils = new InputPwdUtils();
                }

                inputPwdUtils.showPwdDialog(OrderDetailActivity_user.this, OrderDetailActivity_user.this);
            }
        }, 400);
    }

    @Override
    public void onComplete(String pwd) {
        //2022-11-07  验证支付密码,医保结算密码是否正确
        //2023-06-07 将用户选择是否优先选择医保个人账户支付的结果上报给后台 由后台去结算
        //2.是否优先选择医保个人账户支付  是：（优先医保个人账户支付）---医保个人账户支付---不足企业基金支付---不足现金支付
        //3.否---选择企业基金---不足现金支付
        showWaitDialog();
        OrderPayModel.sendOrderPayRequest(TAG, orderNo, verifCode, detailData.getBuyer(), pwd,
                (srbGroupBuyType.getCheckedRadioButtonId() == R.id.srb_medical_self_buy) ? "1" : "0",
                new CustomerJsonCallBack<OrderPayModel>() {
                    @Override
                    public void onRequestError(OrderPayModel returnData, String msg) {
                        hideWaitDialog();
                        showMedicalPayFailPop(msg);
                    }

                    @Override
                    public void onRequestSuccess(OrderPayModel returnData) {
                        hideWaitDialog();
                        OrderPayModel.DataBean orderPayData = returnData.getData();

                        if (orderPayData != null) {
                            hasOnlinePay = orderPayData.getOnlineAmount();
                            detailData.setSettlementStatus(orderPayData.getSettlementStatus());
                            setl_id = orderPayData.getSetl_id();

                            mLlMedicalPersonPayMoney.setVisibility(View.VISIBLE);
                            mTvMedicalPersonPayMoney.setText(EmptyUtils.strEmptyToText(orderPayData.getAcct_pay(), "0"));
                            mTvHasPrice.setText(EmptyUtils.strEmpty(hasOnlinePay));

                            //验证成功通过  弹出手写签名
                            mTitleBar.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    showHandWritePop();
                                }
                            }, 400);
                        }
                    }
                });
    }

    //医保结算失败 弹框提示用户是否选择自费支付
    private void showMedicalPayFailPop(String msg) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(this, "医保结算", "医保结算失败！\n" + "失败原因:" + msg + "\n您可以选择自费继续支付", 2, "取消订单", "继续支付", R.color.txt_color_666, R.color.color_4970e0,
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                        mTitleBar.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                onCancle();
                            }
                        }, 500);
                    }

                    @Override
                    public void onBtn02Click() {
                        //1自助购药（自费结算）
                        detailData.setPurchasingDrugsMethod("1");
                        enterprisePay(detailData.getTotalAmount());
                    }
                });
    }

    //显示签名弹框
    private void showHandWritePop() {
        if (handWritePop == null) {
            handWritePop = new HandWritePop(OrderDetailActivity_user.this, OrderDetailActivity_user.this);
        }

        handWritePop.showPopupWindow();
    }

    @Override
    public void commit(Bitmap mBitmap) {
        showWaitDialog();
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                boolean isSaveSuccess = ImageUtils.save(mBitmap, imagPath, Bitmap.CompressFormat.PNG, true);
                if (isSaveSuccess) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            upSignPic();
                        }
                    });
                } else {
                    hideWaitDialog();
                    showShortToast("图片保存失败，请重新保存");
                }
            }
        });
    }

    //上传签名
    private void upSignPic() {
        BaseModel.sendUploadFileRequest(TAG, setl_id, new File(imagPath), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("上传成功");
                double hasPayamount = new BigDecimal(hasOnlinePay).doubleValue();
                detailData.setAccessoryUrl("1");
                if (hasPayamount > 0) {
                    enterprisePay(hasOnlinePay);
                } else {
                    if (hasPrescr) {//带处方
                        PaySuccessActivity.newIntance(OrderDetailActivity_user.this, orderNo, "订单支付成功！", "返回首页", "", detailData.getFixmedins_code(), getAssociatedDiseases(), "2");
                    } else {//不带处方
                        PaySuccessActivity.newIntance(OrderDetailActivity_user.this, orderNo, "订单支付成功！", "返回首页", "", "", "", "2");
                    }
                    goFinish();
                }
            }
        });
    }

    //企业基金支付--1.能用企业基金， 必须满足2个条件：所购药品的商户支持使用企业基金；客户的企业基金余额大于0.
    private void enterprisePay(String hasPayMoney) {
        double enterpriseBalance = new BigDecimal(detailData.getEnterpriseFundAmount()).doubleValue();
        if ("1".equals(detailData.getIsEnterpriseFundPay()) && enterpriseBalance > 0) {//企业基金支付
            //初始化企业基金密码输入框
            mTitleBar.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (inputPwdUtils_enterprise == null) {
                        inputPwdUtils_enterprise = new InputPwdUtils_Enterprise();
                    }

                    inputPwdUtils_enterprise.showPwdDialog(OrderDetailActivity_user.this, detailData.getEnterpriseFundAmount(), hasPayMoney, OrderDetailActivity_user.this);
                }
            }, 400);
        } else {//现金支付（调用快钱第三方支付--微信、支付宝、云闪付支付）
            if ("1".equals(detailData.getPurchasingDrugsMethod()) && "0".equals(detailData.getEnterpriseFundPayStatus())) {//自费
                cashPay();
            } else {
                OnlinePayActivity.newIntance(OrderDetailActivity_user.this, orderNo);
            }
        }
    }

    //企业基金密码输入完成回调
    @Override
    public void onComplete_Enterprise(String pwd) {
        //上传企业基金的数据
        showWaitDialog();
        EnterprisePayModel.sendEnterprisePayRequest(TAG, orderNo, pwd, new CustomerJsonCallBack<EnterprisePayModel>() {
            @Override
            public void onRequestError(EnterprisePayModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(EnterprisePayModel returnData) {
                hideWaitDialog();
                EnterprisePayModel.DataBean orderPayData = returnData.getData();
                if (orderPayData != null) {
                    detailData.setEnterpriseFundPayStatus("1");
                    hasOnlinePay = orderPayData.getOnlineAmount();
                    double hasPayamount = new BigDecimal(hasOnlinePay).doubleValue();

                    mLlEnterprisePayMoney.setVisibility(View.VISIBLE);
                    mTvEnterprisePayMoney.setText(EmptyUtils.strEmpty(orderPayData.getEnterpriseFundPay()));
                    mTvHasPrice.setText(EmptyUtils.strEmpty(hasOnlinePay));
                    //判断企业基金支付完之后 是否还需要现金支付
                    if (hasPayamount > 0) {
                        OnlinePayActivity.newIntance(OrderDetailActivity_user.this, orderNo);
                    } else {
                        if (hasPrescr) {//带处方
                            PaySuccessActivity.newIntance(OrderDetailActivity_user.this, orderNo, "订单支付成功！", "返回首页", "", detailData.getFixmedins_code(), getAssociatedDiseases(), "2");
                        } else {//不带处方
                            PaySuccessActivity.newIntance(OrderDetailActivity_user.this, orderNo, "订单支付成功！", "返回首页", "", "", "", "2");
                        }
                        goFinish();
                    }
                }
            }
        });
    }

    //现金支付（调用快钱第三方支付--微信、支付宝、云闪付支付）
    private void cashPay() {
        if (selectPayTypePop == null) {
            selectPayTypePop = new SelectPayTypePop(OrderDetailActivity_user.this,
                    CommonlyUsedDataUtils.getInstance().getSelectOrderPayTypeData(), this);
        }
        selectPayTypePop.showPopupWindow();
    }

    //选择支付方式
    @Override
    public void onItemClick(PayTypeModel payTypeModel) {
        //  2022-12-13 选择完支付方式之后 需要请求后台接口 获取支付信息 然后调用快钱sdk 调起原生app进行支付
        showWaitDialog();
        OrderPayOnlineModel.sendOrderPayOnlineRequest(TAG, orderNo, payTypeModel.getId(), new CustomerJsonCallBack<OrderPayOnlineModel>() {
            @Override
            public void onRequestError(OrderPayOnlineModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(OrderPayOnlineModel returnData) {
                hideWaitDialog();
                OrderPayOnlineModel.DataBean payData = returnData.getData();
                if (payData != null && payData.getMpayInfo() != null) {
                    //获取支付信息成功
                    switch (payTypeModel.getId()) {
                        case "W2"://微信
                            invokeFusedPaySDK("4", JSON.toJSONString(payData.getMpayInfo()));
                            break;
                        case "A2"://支付宝
                            invokeFusedPaySDK("7", JSON.toJSONString(payData.getMpayInfo()));
                            break;
                        case "Y"://云闪付
                            invokeFusedPaySDK("5", JSON.toJSONString(payData.getMpayInfo()));
                            break;
                    }
                }
            }
        });
    }

    /**
     * 调起聚合支付sdk
     *
     * @param platform 支付平台 :1 --飞凡通支付   2 --支付宝支付     3 --微信支付  4.微信支付定制版 5.云闪付 7.支付宝支付定制版
     * @param mpayInfo 移动支付的信息
     */
    private void invokeFusedPaySDK(String platform, String mpayInfo) {
        FusedPayRequest payRequest = new FusedPayRequest();
        payRequest.setPlatform(platform);
        payRequest.setMpayInfo(mpayInfo);
        if ("5".equals(platform)) {
            payRequest.setUnionPayTestEnv(false);
        }
        // CallBackSchemeId可以自定义，自定义的结果页面需实现IKuaiqianEventHandler接口
        payRequest.setCallbackSchemeId("com.jrdz.zhyb_android.ui.zhyf_user.activity.OrderDetailActivity_user");
        IFusedPayApi payApi = FusedPayApiFactory.createPayApi(OrderDetailActivity_user.this);
        payApi.pay(payRequest);

        isPaying = true;
    }

    //==============================================支付方法回调================================================
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        isPaying = false;
        IFusedPayApi api = FusedPayApiFactory.createPayApi(this);
        api.handleIntent(getIntent(), this);
    }

    @Override
    public void onResponse(FusedPayResult fusedPayResult) {
        LogUtils.e("onResponse", "支付结果：" + fusedPayResult);
        String payResultCode = fusedPayResult.getResultStatus();
        String payResultMessage = fusedPayResult.getResultMessage();

        if ("00".equals(payResultCode)) {//00支付返回成功
            if (hasPrescr) {//带处方
                PaySuccessActivity.newIntance(OrderDetailActivity_user.this, orderNo, "订单支付成功！", "返回首页", "", detailData.getFixmedins_code(), getAssociatedDiseases(), "1");
            } else {//不带处方
                PaySuccessActivity.newIntance(OrderDetailActivity_user.this, orderNo, "订单支付成功！", "返回首页", "", "", "", "1");
            }
            goFinish();
        } else {
            showTipDialog(payResultMessage);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isPaying) {
            showWaitDialog();
            getOrderPayOnlineStatus();
        }

        if (isOpenPreing) {
            isOpenPreing = false;
            showWaitDialog();
            getDetailData();
        }
    }

    //获取订单支付状态
    private void getOrderPayOnlineStatus() {
        QueryOrderPayOnlineModel.sendQueryOrderPayOnlineRequest(TAG, orderNo, new CustomerJsonCallBack<QueryOrderPayOnlineModel>() {
            @Override
            public void onRequestError(QueryOrderPayOnlineModel returnData, String msg) {
                hideWaitDialog();
                isPaying = false;
            }

            @Override
            public void onRequestSuccess(QueryOrderPayOnlineModel returnData) {
                hideWaitDialog();
//                IsPaid=1或者IsPaid=2
                QueryOrderPayOnlineModel.DataBean data = returnData.getData();
                if (data != null) {
                    if ("1".equals(data.getIsPaid()) || "2".equals(data.getIsPaid())) {//支付成功
                        if (hasPrescr) {//带处方
                            PaySuccessActivity.newIntance(OrderDetailActivity_user.this, orderNo, "订单支付成功！", "返回首页", "", detailData.getFixmedins_code(), getAssociatedDiseases(), "2");
                        } else {//不带处方
                            PaySuccessActivity.newIntance(OrderDetailActivity_user.this, orderNo, "订单支付成功！", "返回首页", "", "", "", "2");
                        }
                        goFinish();
                    }
                }

                isPaying = false;
            }
        });
    }

    //==============================================支付方法回调================================================

    private String getAssociatedDiseases() {
        List<OrderDetailModel_user.DataBean.OrderGoodsBean> goods = detailData.getOrderGoods();
        String associatedDiseases = "";
        for (OrderDetailModel_user.DataBean.OrderGoodsBean orderGood : goods) {
            String diseasesData = orderGood.getAssociatedDiseases();
            if (!EmptyUtils.isEmpty(diseasesData)) {
                associatedDiseases += diseasesData + "∞#";
            }
        }
        if (!EmptyUtils.isEmpty(associatedDiseases)) {
            associatedDiseases = associatedDiseases.substring(0, associatedDiseases.length() - 2);
        }
        return associatedDiseases;
    }

    @Override
    protected void onDestroy() {
        if (handWritePop != null) {
            handWritePop.onClean();
        }
        super.onDestroy();
        BroadCastReceiveUtils.unregisterLocalReceiver(this, mPushReceiver);
        if (mCustomCountDownTimer != null) {
            mCustomCountDownTimer.stop();
            mCustomCountDownTimer = null;
        }
        if (mSmsCountDownTimer != null) {
            mSmsCountDownTimer.stop();
            mSmsCountDownTimer = null;
        }

        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }

        if (selectPayTypePop != null) {
            selectPayTypePop.onClean();
        }

        if (verificationCodePop != null) {
            verificationCodePop.onCleanListener();
            verificationCodePop.onDestroy();
        }

        if (inputPwdUtils != null) {
            inputPwdUtils.dismissDialog();
        }

        if (inputPwdUtils_enterprise != null) {
            inputPwdUtils_enterprise.dismissDialog();
        }
    }

    //from 0来自订单列表  1来自提交订单 2来自消息通知
    public static void newIntance(Context context, String orderNo, int from) {
        Intent intent = new Intent(context, OrderDetailActivity_user.class);
        intent.putExtra("orderNo", orderNo);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }

    //from 0来自订单列表  1来自提交订单 2来自消息通知
    public static void newIntance(Context context, String orderNo, String type, int from) {
        Intent intent = new Intent(context, OrderDetailActivity_user.class);
        intent.putExtra("orderNo", orderNo);
        intent.putExtra("type", type);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }
}
