
package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.frame.compiler.utils.viewPage.BaseViewPagerAndTabsAdapter_new;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.OrderListFragment_user;

import java.util.Arrays;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-09
 * 描    述：我的订单页面--用户端
 * ================================================
 */
public class OrderListActivity_user extends BaseActivity {
    private SlidingTabLayout mStbOrder;
    private ViewPager mVpOrder;

    String[] titles = new String[]{"全部","待付款", "待发货", "待收货", "已完成", "已取消"};
    private int currentTab;

    @Override
    public int getLayoutId() {
        return R.layout.activity_order_list;
    }

    @Override
    public void initView() {
        super.initView();
        mStbOrder = findViewById(R.id.stb_order);
        mVpOrder = findViewById(R.id.vp_order);
    }

    @Override
    public void initData() {
        currentTab=getIntent().getIntExtra("currentTab",0);
        super.initData();

        setRightIcon(getResources().getDrawable(R.drawable.ic_search_black));

        initTabLayout();
    }

    //订单状态 all-全部;pay-待付款;wait_delivery-待发货;wait_receive-待收货;finish-已完成;cancle-已取消;
    private void initTabLayout() {
        BaseViewPagerAndTabsAdapter_new baseViewPagerAndTabsAdapter_new=new BaseViewPagerAndTabsAdapter_new(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                OrderListFragment_user orderFragment=null;
                switch (position){
                    case 0://全部
                        orderFragment= OrderListFragment_user.newIntance("0");
                        break;
                    case 1://待付款
                        orderFragment= OrderListFragment_user.newIntance("1");
                        break;
                    case 2://待发货
                        orderFragment= OrderListFragment_user.newIntance("2");
                        break;
                    case 3://待收货
                        orderFragment= OrderListFragment_user.newIntance("3");
                        break;
                    case 4://已完成
                        orderFragment= OrderListFragment_user.newIntance("4");
                        break;
                    case 5://已取消
                        orderFragment = OrderListFragment_user.newIntance("5");
                        break;
                }
                return orderFragment;
            }
        };

        baseViewPagerAndTabsAdapter_new.setData(Arrays.asList(titles));
        mVpOrder.setAdapter(baseViewPagerAndTabsAdapter_new);
        mStbOrder.setViewPager(mVpOrder);

        mStbOrder.setCurrentTab(currentTab);
    }

    @Override
    public void rightTitleViewClick() {
        SearchOrderActivity_user.newIntance(this);
    }

    public static void newIntance(Context context, int currentTab) {
        Intent intent = new Intent(context, OrderListActivity_user.class);
        intent.putExtra("currentTab",currentTab);
        context.startActivity(intent);
    }
}
