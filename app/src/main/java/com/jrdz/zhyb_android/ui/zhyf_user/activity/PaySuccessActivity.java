package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_user.model.QueryOrderPayOnlineModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-07
 * 描    述：支付成功
 * ================================================
 */
public class PaySuccessActivity extends BaseActivity {
    private TextView mTvResult;
    private ShapeTextView mTv01;
    private ShapeTextView mTv02;

    private String orderNo, result, btnText01, btnText02, fixmedins_code, associatedDiseases, from;
    private Handler handler;
    //是否调用接口验证支付转态
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            getOrderPayOnlineStatus();
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.activity_pay_success;
    }

    @Override
    public void initView() {
        super.initView();
        mTvResult = findViewById(R.id.tv_result);
        mTv01 = findViewById(R.id.tv_01);
        mTv02 = findViewById(R.id.tv_02);
    }

    @Override
    public void initData() {
        Intent intent = getIntent();
        orderNo = intent.getStringExtra("orderNo");
        result = intent.getStringExtra("result");
        btnText01 = intent.getStringExtra("btnText01");
        btnText02 = intent.getStringExtra("btnText02");
        fixmedins_code = intent.getStringExtra("fixmedins_code");
        associatedDiseases = intent.getStringExtra("associatedDiseases");
        from = getIntent().getStringExtra("from");
        super.initData();

        switch (from) {
            case "1"://需要查询支付结果
                handler = new Handler(getMainLooper());
                showWaitDialog("支付结果查询中...");
                getOrderPayOnlineStatus();
                break;
            case "2"://直接展示支付结果
                setSuccessUi();
                break;
        }
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mTv01.setOnClickListener(this);
        mTv02.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (((TextView) v).getText().toString()) {
            case "暂不":
                OrderListActivity_user.newIntance(PaySuccessActivity.this, 2);
                goFinish();
                break;
            case "开具处方":
//                InquiryInfoActivity.newIntance(PaySuccessActivity.this, orderNo, fixmedins_code, associatedDiseases);
//                goFinish();
                break;
            case "返回首页":
                SmartPhaMainActivity_user.newIntance(PaySuccessActivity.this, 0);
                goFinish();
                break;
        }
    }

    //获取订单支付状态
    private void getOrderPayOnlineStatus() {
        QueryOrderPayOnlineModel.sendQueryOrderPayOnlineRequest(TAG, orderNo, new CustomerJsonCallBack<QueryOrderPayOnlineModel>() {
            @Override
            public void onRequestError(QueryOrderPayOnlineModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(QueryOrderPayOnlineModel returnData) {
//                IsPaid=1或者IsPaid=2
                QueryOrderPayOnlineModel.DataBean data = returnData.getData();
                if (data != null) {
                    if ("1".equals(data.getIsPaid()) || "2".equals(data.getIsPaid())) {//支付成功
                        hideWaitDialog();
                        setSuccessUi();
                    } else {
                        handler.postDelayed(mRunnable, 2000);
                    }
                }
            }
        });
    }

    //设置成功的UI
    private void setSuccessUi() {
        mTvResult.setText(EmptyUtils.strEmpty(result));
        if (EmptyUtils.isEmpty(btnText01)) {
            mTv01.setVisibility(View.GONE);
        } else {
            mTv01.setVisibility(View.VISIBLE);
            mTv01.setText(EmptyUtils.strEmpty(btnText01));
        }
        if (EmptyUtils.isEmpty(btnText02)) {
            mTv02.setVisibility(View.GONE);
        } else {
            mTv02.setVisibility(View.VISIBLE);
            mTv02.setText(EmptyUtils.strEmpty(btnText02));
        }
        MsgBus.sendPayStatusRefresh_user().post("1");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
    }

    public static void newIntance(Context context, String orderNo, String result, String btnText01, String btnText02,
                                  String fixmedins_code, String associatedDiseases, String from) {
        Intent intent = new Intent(context, PaySuccessActivity.class);
        intent.putExtra("orderNo", orderNo);
        intent.putExtra("result", result);
        intent.putExtra("btnText01", btnText01);
        intent.putExtra("btnText02", btnText02);
        intent.putExtra("fixmedins_code", fixmedins_code);
        intent.putExtra("associatedDiseases", associatedDiseases);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }
}
