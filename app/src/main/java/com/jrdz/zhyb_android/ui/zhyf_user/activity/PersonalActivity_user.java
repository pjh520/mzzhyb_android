package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.widget.customPop.CustomerBottomListPop;
import com.frame.compiler.widget.glide.GlideUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.StoreStatusModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-08-11
 * 描    述： 个人信息页面--用户端
 * ================================================
 */
public class PersonalActivity_user extends BaseActivity implements CustomerBottomListPop.IBottomChooseListener<StoreStatusModel> {
    private LinearLayout mLlHead;
    private ImageView mIvHead;
    private LinearLayout mLlName;
    private TextView mTvName;
    private LinearLayout mLlSex;
    private TextView mTvSex;
    private LinearLayout mLlPhone;
    private TextView mTvPhone;

    private CustomerBottomListPop customerBottomListPop;

    private ObserverWrapper<String> mUpdateInsuredInfoObserve=new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            switch (value){
                case "11"://更新昵称
                    mTvName.setText(InsuredLoginUtils.getNickname());
                    break;
            }
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.activity_personal_user;
    }

    @Override
    public void initView() {
        super.initView();
        mLlHead = findViewById(R.id.ll_head);
        mIvHead = findViewById(R.id.iv_head);
        mLlName = findViewById(R.id.ll_name);
        mTvName = findViewById(R.id.tv_name);
        mLlSex = findViewById(R.id.ll_sex);
        mTvSex = findViewById(R.id.tv_sex);
        mLlPhone = findViewById(R.id.ll_phone);
        mTvPhone = findViewById(R.id.tv_phone);
    }

    @Override
    public void initData() {
        super.initData();
        MsgBus.updateInsuredInfo().observe(this, mUpdateInsuredInfoObserve);
        GlideUtils.loadImg(InsuredLoginUtils.getAccessoryUrl(), mIvHead,R.drawable.ic_insured_head,new CircleCrop());

        mTvName.setText(InsuredLoginUtils.getNickname());
        mTvSex.setText("2".equals(InsuredLoginUtils.getSex())?"女":"男");
        mTvPhone.setText(StringUtils.encryptionPhone(InsuredLoginUtils.getPhone()));
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mLlName.setOnClickListener(this);
        mLlSex.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.ll_name://修改昵称
                UpdateNameActivity_user.newIntance(PersonalActivity_user.this);
                break;
            case R.id.ll_sex://修改性别
                if (customerBottomListPop == null) {
                    customerBottomListPop = new CustomerBottomListPop(PersonalActivity_user.this, PersonalActivity_user.this);
                }
                customerBottomListPop.setDatas("1", "", mTvSex.getText().toString(), CommonlyUsedDataUtils.getInstance().getUserSexData());
                customerBottomListPop.showPopupWindow();
                break;
        }
    }

    @Override
    public void onItemClick(String tag, StoreStatusModel str) {
        //2022-11-03  更新参保人存储的本地信息 性别
        showWaitDialog();
        BaseModel.sendChangeSexRequest(TAG, str.getId(), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                mTvSex.setText(str.getText());
                InsuredLoginUtils.setSex(str.getId());
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerBottomListPop != null) {
            customerBottomListPop.dismiss();
            customerBottomListPop.onDestroy();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, PersonalActivity_user.class);
        context.startActivity(intent);
    }
}
