package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.DrugRecommendAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.PhaSortSpecialAreaAdapter2_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.DetailCatalogueModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.PhaSortSpecialAreaModel;

import java.util.ArrayList;
import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-14
 * 描    述：药品专区 页面--用户端
 * ================================================
 */
public class PhaSortSpecialAreaActivity2_user extends BaseRecyclerViewActivity {
    private String catalogueId, catalogueName;

    //登录成功信息
    private ObserverWrapper<Integer> mLoginObserve = new ObserverWrapper<Integer>() {
        @Override
        public void onChanged(@Nullable Integer value) {
            //设置机构、用户信息
            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void initData() {
        MsgBus.sendInsuredLoginStatus().observe(this, mLoginObserve);
        catalogueId = getIntent().getStringExtra("catalogueId");
        catalogueName = getIntent().getStringExtra("catalogueName");
        super.initData();
        setTitle(catalogueName);

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initAdapter() {
        mAdapter = new PhaSortSpecialAreaAdapter2_user();
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    public void getData() {
        PhaSortSpecialAreaModel.sendPhaSortSpecialAreaRequest_user(TAG, String.valueOf(mPageNum), "10", catalogueId, "",
                String.valueOf(Constants.AppStorage.APP_LONGITUDE), String.valueOf(Constants.AppStorage.APP_LATITUDE), new CustomerJsonCallBack<PhaSortSpecialAreaModel>() {
                    @Override
                    public void onRequestError(PhaSortSpecialAreaModel returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(PhaSortSpecialAreaModel returnData) {
                        hideRefreshView();
                        List<PhaSortSpecialAreaModel.DataBean> infos = returnData.getData();
                        if (mAdapter != null && infos != null) {
                            if (mPageNum == 0) {
                                mAdapter.setNewData(infos);
                                if (infos.size() <= 0) {
                                    mAdapter.isUseEmpty(true);
                                }
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 6) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        switch (view.getId()) {
            case R.id.ll_title:
                PhaSortSpecialAreaModel.DataBean itemData = ((PhaSortSpecialAreaAdapter2_user) adapter).getItem(position);
                PhaSortSpecialAreaActivity_user.newIntance(PhaSortSpecialAreaActivity2_user.this, catalogueId,catalogueName, itemData.getFixmedins_code());
                break;
        }
    }

    public static void newIntance(Context context, String catalogueId, String catalogueName) {
        Intent intent = new Intent(context, PhaSortSpecialAreaActivity2_user.class);
        intent.putExtra("catalogueId", catalogueId);
        intent.putExtra("catalogueName", catalogueName);
        context.startActivity(intent);
    }
}
