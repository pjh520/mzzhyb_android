package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.frame.compiler.utils.DecimalFormatUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxClipboardTool;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TagTextBean;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OrderDetailModel_user;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.widget.TagTextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-12-23
 * 描    述：扫码支付-用户端订单详情
 * ================================================
 */
public class ScanPayOrderDetailActivity_user extends BaseActivity {
    protected TextView mTvStatus;
    protected TextView mTvOrderId;
    protected TextView mTvOrderRefundStatus;
    protected LinearLayout mLlOrderListContain;
    protected TextView mTvTotalNum;
    protected TextView mTvTotalPrice;
    protected TextView mTvOrderId02;
    protected TextView mTvOrderIdCopy;
    protected LinearLayout mLlRefundNo;
    protected TextView mTvRefundNo;
    protected TextView mTvRefundNoCopy;
    protected TextView mTvOrderCreateTime;
    protected LinearLayout mLlPayTime;
    protected TextView mTvOrderPayTime;
    protected LinearLayout mLlCancleTime;
    protected TextView mTvOrderCancleTime;
    protected ShapeLinearLayout mSllSerialno;
    protected TextView mTvOrderTotalPrice,mTvDiscountAmount,mTvDiscount,mTvCancelPay;
    protected LinearLayout mLlCancelPay;
    protected TextView mTvPsnCashPay;

    protected String orderNo;
    protected OrderDetailModel_user.DataBean detailData;
    List<TagTextBean> tags = new ArrayList<>();
    private String totalPrice;//总价
    private int totalNum = 0;//商品总数
    private CustomerDialogUtils customerDialogUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_scanpay_order_detail_uesr;
    }

    @Override
    public void initView() {
        super.initView();
        mTvStatus = findViewById(R.id.tv_status);
        mTvOrderId = findViewById(R.id.tv_order_id);
        mTvOrderRefundStatus = findViewById(R.id.tv_order_refund_status);
        mLlOrderListContain = findViewById(R.id.ll_order_list_contain);
        mTvTotalNum = findViewById(R.id.tv_total_num);
        mTvTotalPrice = findViewById(R.id.tv_total_price);
        mTvOrderId02 = findViewById(R.id.tv_order_id_02);
        mTvOrderIdCopy = findViewById(R.id.tv_order_id_copy);
        mLlRefundNo = findViewById(R.id.ll_refund_no);
        mTvRefundNo = findViewById(R.id.tv_refund_no);
        mTvRefundNoCopy = findViewById(R.id.tv_refund_no_copy);
        mTvOrderCreateTime = findViewById(R.id.tv_order_create_time);
        mLlPayTime = findViewById(R.id.ll_pay_time);
        mTvOrderPayTime = findViewById(R.id.tv_order_pay_time);
        mLlCancleTime = findViewById(R.id.ll_cancle_time);
        mTvOrderCancleTime = findViewById(R.id.tv_order_cancle_time);
        mSllSerialno = findViewById(R.id.sll_serialno);

        mTvOrderTotalPrice = findViewById(R.id.tv_order_total_price);
        mTvDiscountAmount = findViewById(R.id.tv_discount_amount);
        mTvDiscount = findViewById(R.id.tv_discount);
        mTvPsnCashPay = findViewById(R.id.tv_psn_cash_pay);
        mLlCancelPay = findViewById(R.id.ll_cancel_pay);
        mTvCancelPay = findViewById(R.id.tv_cancel_pay);
    }

    @Override
    public void initData() {
        orderNo = getIntent().getStringExtra("orderNo");
        super.initData();

        showWaitDialog();
        getDetailData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mTvOrderIdCopy.setOnClickListener(this);
        mTvRefundNoCopy.setOnClickListener(this);
    }

    //获取详情数据
    protected void getDetailData() {
        //2022-10-12 模拟请求数据
        OrderDetailModel_user.sendOrderDetailRequest_user(TAG, orderNo, new CustomerJsonCallBack<OrderDetailModel_user>() {
            @Override
            public void onRequestError(OrderDetailModel_user returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(OrderDetailModel_user returnData) {
                hideWaitDialog();
                detailData = returnData.getData();
                if (isFinishing())return;
                if (detailData != null) {
                    setDetailData();
//                    if (!EmptyUtils.isEmpty(type) && "pay".equals(type)) {
//                        mTitleBar.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                onPay();
//                            }
//                        }, 400);
//                    }
                }
            }
        });
    }

    //设置页面数据
    protected void setDetailData() {
        mLlOrderListContain.removeAllViews();
        totalPrice = "0";
        totalNum = 0;
        List<OrderDetailModel_user.DataBean.OrderGoodsBean> orderGoods = detailData.getOrderGoods();
        for (int i = 0, size = orderGoods.size(); i < size; i++) {
            OrderDetailModel_user.DataBean.OrderGoodsBean orderGood = orderGoods.get(i);

            totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(orderGood.getPrice())
                    .multiply(new BigDecimal(String.valueOf(orderGood.getGoodsNum())))).toPlainString();
            totalNum += orderGood.getGoodsNum();

            //设置商品信息
            View view = LayoutInflater.from(this).inflate(R.layout.layout_orderlist_product_item, mLlOrderListContain, false);
            ImageView ivPic = view.findViewById(R.id.iv_pic);
            ShapeTextView stvOtc = view.findViewById(R.id.stv_otc);
            ShapeTextView stvEnterpriseTag= view.findViewById(R.id.stv_enterprise_tag);
            TagTextView tvTitle = view.findViewById(R.id.tv_title);
            TextView tvEstimatePrice = view.findViewById(R.id.tv_estimate_price);
            TextView tvRealPrice = view.findViewById(R.id.tv_real_price);
            View lineItem = view.findViewById(R.id.line_item);

            GlideUtils.loadImg(orderGood.getBannerAccessoryUrl1(), ivPic, R.drawable.ic_good_placeholder,
                    new RoundedCornersTransformation(getResources().getDimensionPixelSize(R.dimen.dp_16), 0));
            //判断是否是处方药
            if ("1".equals(orderGood.getItemType())) {
                stvOtc.setText("OTC");
                stvOtc.setVisibility(View.VISIBLE);
            } else if ("2".equals(orderGood.getItemType())) {
                stvOtc.setText("处方药");
                stvOtc.setVisibility(View.VISIBLE);
            } else {
                stvOtc.setVisibility(View.GONE);
            }
            //判断是否是平台药 若是 需要展示医保
            tags.clear();
            if ("1".equals(orderGood.getIsPlatformDrug())){//是平台药
                tags.add(new TagTextBean("医保",R.color.color_ee8734));
                tvTitle.setContentAndTag(EmptyUtils.strEmpty(orderGood.getGoodsName()), tags);
            }else {
                tvTitle.setContentAndTag(EmptyUtils.strEmpty(orderGood.getGoodsName()), tags);
            }

            //判断1.用户是否是企业基金用户 2.是否支持企业基金支付
            if ("1".equals(InsuredLoginUtils.getIsEnterpriseFundPay())&&"1".equals(orderGood.getIsEnterpriseFundPay())){
                stvEnterpriseTag.setVisibility(View.VISIBLE);
            }else {
                stvEnterpriseTag.setVisibility(View.GONE);
            }

            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(orderGood.getPrice())));
            tvRealPrice.setText("x" + orderGood.getGoodsNum());
            //2022-10-10 最后一个item的分割线需要隐藏
            if (i == size - 1) {
                lineItem.setVisibility(View.GONE);
            } else {
                lineItem.setVisibility(View.VISIBLE);
            }
            mLlOrderListContain.addView(view);
        }

        mTvOrderId.setText("订单编号：" + detailData.getOrderNo());
        //退款状态 是否已退款(0、 未退款 1、已退款）
        if ("1".equals(detailData.getIsRefunded())){
            mTvOrderRefundStatus.setVisibility(View.VISIBLE);
        }else {
            mTvOrderRefundStatus.setVisibility(View.GONE);
        }
        mTvTotalNum.setText("共" + totalNum + "件商品");
        mTvTotalPrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(totalPrice)));

        mTvOrderId02.setText(EmptyUtils.strEmpty(detailData.getOrderNo()));
        mTvRefundNo.setText(EmptyUtils.strEmpty(detailData.getOrderRefundNo()));
        mTvOrderCreateTime.setText(EmptyUtils.strEmpty(detailData.getCreateDT()));

        //医保结算 费用明细
        mTvOrderTotalPrice.setText("¥"+EmptyUtils.strEmpty(detailData.getOriginalTotalAmount()));//订单总额
        mTvDiscountAmount.setText(DecimalFormatUtils.noZero(EmptyUtils.strEmpty(detailData.getDiscountAmount()))+"元");//优惠金额
        Log.e("6666666", "getDiscount: "+detailData.getDiscount());
        mTvDiscount.setText(DecimalFormatUtils.noZero(EmptyUtils.strEmpty(detailData.getDiscount()))+"%");//优惠折扣
        mTvPsnCashPay.setText("¥"+EmptyUtils.strEmptyToText(detailData.getEnterpriseFundPay(),"0.0"));//个人现金支付
        mTvCancelPay.setText("¥"+EmptyUtils.strEmptyToText(detailData.getEnterpriseFundPay(),"0.0"));//取消金额
        //请求数据 成功
        switch (detailData.getOrderStatus()) {
            case "4"://已完成
                complete();
                break;
            case "5"://用户取消订单（未支付）
            case "6"://系统取消订单（未支付完成）
                cancle1();
                break;
            case "7"://系统取消订单（支付完成）
            case "8"://商家取消订单（支付完成）
                cancle2();
                break;
        }
    }

    //已完成
    private void complete() {
        setTitle("已完成");
        mTvStatus.setText("订单完成");

        //退款编号
        mLlRefundNo.setVisibility(View.GONE);
        //最后付款时间
        mLlPayTime.setVisibility(View.VISIBLE);
        mTvOrderPayTime.setText(EmptyUtils.strEmpty(detailData.getLastPaytTime()));
        //订单取消时间
        mLlCancleTime.setVisibility(View.GONE);
        //订单取消金额
        mLlCancelPay.setVisibility(View.GONE);
    }
    //用户取消订单（未支付）
    private void cancle1() {
        setTitle("已取消");
        mTvStatus.setText("已取消（交易关闭）");

        //退款编号
        mLlRefundNo.setVisibility(View.GONE);
        //最后付款时间
        mLlPayTime.setVisibility(View.GONE);
        //订单取消时间
        mLlCancleTime.setVisibility(View.VISIBLE);
        mTvOrderCancleTime.setText(EmptyUtils.strEmpty(detailData.getCancelTime()));
        //订单取消金额
        mLlCancelPay.setVisibility(View.VISIBLE);
    }
    //系统取消订单（支付完成）
    private void cancle2() {
        setTitle("已取消");
        mTvStatus.setText("已取消（交易关闭）");

        //退款编号
        mLlRefundNo.setVisibility(View.VISIBLE);
        //最后付款时间
        mLlPayTime.setVisibility(View.VISIBLE);
        mTvOrderPayTime.setText(EmptyUtils.strEmpty(detailData.getLastPaytTime()));
        //订单取消时间
        mLlCancleTime.setVisibility(View.VISIBLE);
        mTvOrderCancleTime.setText(EmptyUtils.strEmpty(detailData.getCancelTime()));
        //订单取消金额
        mLlCancelPay.setVisibility(View.VISIBLE);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        if (R.id.tv_order_id_copy == v.getId()) {
            RxClipboardTool.copyText(ScanPayOrderDetailActivity_user.this, mTvOrderId02.getText().toString());
        } else if (R.id.tv_refund_no_copy == v.getId()) {
            RxClipboardTool.copyText(ScanPayOrderDetailActivity_user.this, mTvRefundNo.getText().toString());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context, String orderNo) {
        Intent intent = new Intent(context, ScanPayOrderDetailActivity_user.class);
        intent.putExtra("orderNo", orderNo);
        context.startActivity(intent);
    }
}
