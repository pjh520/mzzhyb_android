package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.alibaba.fastjson.JSON;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.SearchActivity;
import com.jrdz.zhyb_android.utils.MMKVUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-09
 * 描    述：搜索页面
 * ================================================
 */
public class SearchActivity_user extends SearchActivity {

    @Override
    public void initData() {
        super.initData();

        mEtSearch.setHint("请输入药品名称或药房名称");
    }

    @Override
    protected void searchHistoryClick() {
        //历史记录 item点击事件
        searchHistoryAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                SearchResultActivity_user.newIntance(SearchActivity_user.this,searchHistoryAdapter.getItem(i),"");
            }
        });
    }

    @Override
    public void cleanHistory() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(SearchActivity_user.this, "提示", "确定清除所有历史搜索记录？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                MMKVUtils.delete("zhyf_search_history_user");
                searchHistoryAdapter.setNewData(new ArrayList<>());
            }
        });
    }

    //搜索
    @Override
    public void search() {
        if (EmptyUtils.isEmpty(mEtSearch.getText().toString())){
            showShortToast("输入药品名称或药房名称");
            return;
        }
        saveHistoryData(mEtSearch.getText().toString());

        SearchResultActivity_user.newIntance(SearchActivity_user.this,mEtSearch.getText().toString(),"");
    }

    //保存搜索数据
    private void saveHistoryData(String searchText){
        //获取历史搜索本地记录
        List<String> shopHistoryDatas=JSON.parseArray(MMKVUtils.getString("zhyf_search_history_user"),String.class);
        if (shopHistoryDatas==null){
            shopHistoryDatas=new ArrayList<>();
        }else {
            if (shopHistoryDatas.contains(searchText)){
                shopHistoryDatas.remove(searchText);
            }

            if (shopHistoryDatas.size()>=10){
                shopHistoryDatas.remove(9);
            }
        }
        shopHistoryDatas.add(0,searchText);
        MMKVUtils.putString("zhyf_search_history_user",JSON.toJSONString(shopHistoryDatas));
        getHistoryData();
    }

    //获取历史搜索记录
    @Override
    protected void getHistoryData() {
        //获取历史搜索本地记录
        List<String> shopHistoryDatas=JSON.parseArray(MMKVUtils.getString("zhyf_search_history_user"),String.class);
        searchHistoryAdapter.setNewData(shopHistoryDatas);
    }

    @Override
    public void scanOrgan(String barCode) {
        SearchResultActivity_user.newIntance(SearchActivity_user.this,"",barCode);
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, SearchActivity_user.class);
        context.startActivity(intent);
    }
}
