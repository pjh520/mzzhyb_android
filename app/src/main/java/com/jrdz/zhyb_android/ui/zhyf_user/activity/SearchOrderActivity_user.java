package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.alibaba.fastjson.JSON;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.SearchActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.SearchHistoryAdapter;
import com.jrdz.zhyb_android.utils.MMKVUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-09
 * 描    述：智慧药房-用户端-订单搜索-搜索页面
 * ================================================
 */
public class SearchOrderActivity_user extends SearchActivity {

    @Override
    public void initData() {
        mEtSearch.setHint("输入订单编号/药品名称/药房名称搜索");
        //初始化历史记录列表
        mCrvHistory.setHasFixedSize(true);
        mCrvHistory.setLayoutManager(getLayoutManager());
        searchHistoryAdapter = new SearchHistoryAdapter();
        mCrvHistory.setAdapter(searchHistoryAdapter);

        getHistoryData();
    }

    @Override
    protected void searchHistoryClick() {
        //历史记录 item点击事件
        searchHistoryAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                SearchOrderResultActivity_user.newIntance(SearchOrderActivity_user.this,searchHistoryAdapter.getItem(i),"");
            }
        });
    }

    //搜索
    @Override
    public void search() {
        if (EmptyUtils.isEmpty(mEtSearch.getText().toString())){
            showShortToast("输入订单编号/药品名称/药房名称搜索");
            return;
        }
        saveHistoryData(mEtSearch.getText().toString());

        SearchOrderResultActivity_user.newIntance(SearchOrderActivity_user.this,mEtSearch.getText().toString(),"");
    }

    //保存搜索数据
    private void saveHistoryData(String searchText){
        //获取历史搜索本地记录
        List<String> shopHistoryDatas=JSON.parseArray(MMKVUtils.getString("zhyf_search_history_order_user"),String.class);
        if (shopHistoryDatas==null){
            shopHistoryDatas=new ArrayList<>();
        }else {
            if (shopHistoryDatas.contains(searchText)){
                shopHistoryDatas.remove(searchText);
            }

            if (shopHistoryDatas.size()>=10){
                shopHistoryDatas.remove(10);
            }
        }
        shopHistoryDatas.add(0,searchText);
        MMKVUtils.putString("zhyf_search_history_order_user",JSON.toJSONString(shopHistoryDatas));
        getHistoryData();
    }

    //获取历史搜索记录
    @Override
    protected void getHistoryData() {
        //获取历史搜索本地记录
        List<String> shopHistoryDatas=JSON.parseArray(MMKVUtils.getString("zhyf_search_history_order_user"),String.class);
        searchHistoryAdapter.setNewData(shopHistoryDatas);
    }

    @Override
    public void scanOrgan(String barCode) {
        SearchOrderResultActivity_user.newIntance(SearchOrderActivity_user.this,mEtSearch.getText().toString(),barCode);
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, SearchOrderActivity_user.class);
        context.startActivity(intent);
    }
}
