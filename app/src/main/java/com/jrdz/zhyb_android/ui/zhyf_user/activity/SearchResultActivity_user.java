package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.CostomLoadMoreView;
import com.frame.compiler.widget.tabLayout.CommonTabLayout;
import com.frame.compiler.widget.tabLayout.listener.OnTabSelectListener;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.SearchResultGoodAdapter_user;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.SearchResultShopAdapter_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.SearchResultModel_user;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-09
 * 描    述：搜索结果页面
 * ================================================
 */
public class SearchResultActivity_user extends BaseRecyclerViewActivity {
    private LinearLayout mLlTitle;
    private FrameLayout mFlClose;
    private AppCompatEditText mEtSearch;
    private FrameLayout mFlClean;
    private TextView mTvSearch;
    private CommonTabLayout mStbTab;

    private String searchText, barCode;
    private String BySales = "", ByPrice = "", ByDistance = "";
    private String type;

    @Override
    public int getLayoutId() {
        return R.layout.activity_search_result_user;
    }

    @Override
    public void initView() {
        super.initView();
        mLlTitle = findViewById(R.id.ll_title);
        mFlClose = findViewById(R.id.fl_close);
        mEtSearch = findViewById(R.id.et_search);
        mFlClean = findViewById(R.id.fl_clean);
        mTvSearch = findViewById(R.id.tv_search);

        mStbTab = findViewById(R.id.stb_tab);
    }

    @Override
    public void initData() {
        searchText = getIntent().getStringExtra("searchText");
        barCode = getIntent().getStringExtra("barCode");
        mEtSearch.setText(searchText);
        if (EmptyUtils.isEmpty(searchText)) {
            mFlClean.setVisibility(View.GONE);
        } else {
            mFlClean.setVisibility(View.VISIBLE);
        }
        RxTool.setEditTextCursorLocation(mEtSearch);

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    protected void initImmersionBar() {
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .titleBar(mLlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initEvent() {
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                    return true;
                }
                return false;
            }
        });
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (EmptyUtils.isEmpty(s.toString())) {
                    mFlClean.setVisibility(View.GONE);
                    KeyboardUtils.hideSoftInput(mEtSearch);
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                } else {
                    mFlClean.setVisibility(View.VISIBLE);
                }
            }
        });
        mFlClose.setOnClickListener(this);
        mFlClean.setOnClickListener(this);
        mTvSearch.setOnClickListener(this);

        mStbTab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                switch (position) {
                    case 0://全部
                        BySales = "";
                        ByPrice = "";
                        ByDistance = "";
                        break;
                    case 1://位置
                        BySales = "";
                        ByPrice = "";
                        ByDistance = "2";
                        break;
                    case 2://销量
                        BySales = "1";
                        ByPrice = "";
                        ByDistance = "";
                        break;
                    case 3://价格
                        BySales = "";
                        ByPrice = "2";
                        ByDistance = "";
                        break;
                }

                showWaitDialog();
                onRefresh(mRefreshLayout);
            }

            @Override
            public void onTabReselect(int position) {
            }
        });
    }

    @Override
    public void getData() {
        //2022-10-09 获取数据
        SearchResultModel_user.sendSearchResultRequest(TAG, String.valueOf(mPageNum), "20", mEtSearch.getText().toString(), barCode, BySales,
                ByPrice, ByDistance, String.valueOf(Constants.AppStorage.APP_LATITUDE), String.valueOf(Constants.AppStorage.APP_LONGITUDE), new CustomerJsonCallBack<SearchResultModel_user>() {
                    @Override
                    public void onRequestError(SearchResultModel_user returnData, String msg) {
                        hideRefreshView();
                        setLoadMoreFail();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(SearchResultModel_user returnData) {
                        hideRefreshView();
                        if (isFinishing()) return;
                        List<SearchResultModel_user.DataBean> infos = returnData.getData();
                        if (infos != null) {
                            if (mPageNum == 0) {
                                // 2023-01-09 搜索到的是药品或者店铺
                                type = returnData.getType();
                                if ("1".equals(returnData.getType())) {//药品
                                    mStbTab.setTabData(CommonlyUsedDataUtils.getInstance().getSearchTabData("1"));
                                    mAdapter = new SearchResultGoodAdapter_user();
                                } else {//店铺
                                    mStbTab.setTabData(CommonlyUsedDataUtils.getInstance().getSearchTabData("2"));
                                    mAdapter = new SearchResultShopAdapter_user();
                                }
                                initRecyclerView();

                                mAdapter.setNewData(infos);
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 8) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    //初始化列表
    private void initRecyclerView() {
        mAdapter.setLoadMoreView(new CostomLoadMoreView());
        mRefreshLayout.setEnableRefresh(true);
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setEnableLoadMore(false);
        mAdapter.setEnableLoadMore(true);
        mAdapter.setOnLoadMoreListener(this, mRecyclerView);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        setEmptyView();
        isShowEmptyView();
        mRecyclerView.setAdapter(mAdapter);
        setRefreshInfo();

        mAdapter.setOnItemClickListener(this);
        mAdapter.setOnItemChildClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close://关闭
                goFinish();
                break;
            case R.id.fl_clean://清除搜索数据
                mEtSearch.setText("");
                break;
            case R.id.tv_search://搜索
                KeyboardUtils.hideSoftInput(mEtSearch);
                showWaitDialog();
                onRefresh(mRefreshLayout);
                break;
        }
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        if ("1".equals(type)) {
            SearchResultModel_user.DataBean itemData = ((SearchResultGoodAdapter_user) adapter).getItem(position);
            switch (view.getId()) {
                case R.id.rl_product://进入商品详情
                    GoodDetailActivity.newIntance(SearchResultActivity_user.this, itemData.getGoodsNo(),itemData.getFixmedins_code(),itemData.getGoodsName());
                    break;
                case R.id.rl_shop://进入店铺详情
                    ShopDetailActivity.newIntance(SearchResultActivity_user.this, itemData.getFixmedins_code());
                    break;
            }
        }
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        if ("2".equals(type)) {
            SearchResultModel_user.DataBean itemData = ((SearchResultShopAdapter_user) adapter).getItem(position);
            ShopDetailActivity.newIntance(SearchResultActivity_user.this, itemData.getFixmedins_code());
        }
    }

    @Override
    public void setEmptyView() {
        mAdapter.setEmptyView(R.layout.layout_search_result_empty_view, mRecyclerView);
    }

    public static void newIntance(Context context, String searchText, String barCode) {
        Intent intent = new Intent(context, SearchResultActivity_user.class);
        intent.putExtra("searchText", searchText);
        intent.putExtra("barCode", barCode);
        context.startActivity(intent);
    }
}
