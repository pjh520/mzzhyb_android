package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Intent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.frame.compiler.utils.StringUtils;
import com.hjq.shape.layout.ShapeRelativeLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.model.VisitorModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;

import java.util.Map;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-08
 * 描    述：选择就诊人
 * ================================================
 */
public class SelectVisitorActivity extends BaseActivity {
    private ShapeRelativeLayout mSrlPrescrInfo;
    private TextView mTvPrescrInfo;
    private ShapeTextView mStvPrescrOneself;
    private TextView mTvPrescrCertNo;
    private FrameLayout mFlPrescrEdit;
    private TextView mTvPrescrEdit;
    private ShapeRelativeLayout mSrlRelativesInfo;
    private TextView mTvRelativesInfo;
    private ShapeTextView mStvRelatives;
    private TextView mTvRelativesCertNo;
    private android.view.View mLine;
    private FrameLayout mFlEdit;
    private TextView mTvEdit;
    private FrameLayout mFlDel;
    private TextView mTvDel;
    private ImageView mIvAdd;

    @Override
    public int getLayoutId() {
        return R.layout.activity_select_visitor;
    }

    @Override
    public void initView() {
        super.initView();
        mSrlPrescrInfo = findViewById(R.id.srl_prescr_info);
        mTvPrescrInfo = findViewById(R.id.tv_prescr_info);
        mStvPrescrOneself = findViewById(R.id.stv_prescr_oneself);
        mTvPrescrCertNo = findViewById(R.id.tv_prescr_cert_no);
        mFlPrescrEdit = findViewById(R.id.fl_prescr_edit);
        mTvPrescrEdit = findViewById(R.id.tv_prescr_edit);

        mSrlRelativesInfo = findViewById(R.id.srl_relatives_info);
        mTvRelativesInfo = findViewById(R.id.tv_relatives_info);
        mStvRelatives = findViewById(R.id.stv_relatives);
        mTvRelativesCertNo = findViewById(R.id.tv_relatives_cert_no);
        mLine = findViewById(R.id.line);
        mFlEdit = findViewById(R.id.fl_edit);
        mTvEdit = findViewById(R.id.tv_edit);
        mFlDel = findViewById(R.id.fl_del);
        mTvDel = findViewById(R.id.tv_del);
        mIvAdd = findViewById(R.id.iv_add);
    }

    @Override
    public void initData() {
        super.initData();

        Map<String, String> certNoinfo = CommonlyUsedDataUtils.getInstance().getBirthdayAgeSex(InsuredLoginUtils.getIdCardNo());
        if (certNoinfo != null) {
            String sexText = "1".equals(String.valueOf(certNoinfo.get("sex"))) ? "男" : "女";
            mTvPrescrInfo.setText(InsuredLoginUtils.getName() + "   " + sexText + "    " + certNoinfo.get("age") + "岁");
        }

        mTvPrescrCertNo.setText(StringUtils.encryptionIDCard(InsuredLoginUtils.getIdCardNo()));
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mSrlPrescrInfo.setOnClickListener(this);
        mFlPrescrEdit.setOnClickListener(this);
        mTvPrescrEdit.setOnClickListener(this);
        mSrlRelativesInfo.setOnClickListener(this);
        mIvAdd.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.srl_prescr_info://选择本人数据
                onPrescrInfo();
                break;
            case R.id.fl_prescr_edit://编辑就诊人信息
            case R.id.tv_prescr_edit:
                Map<String, String> certNoinfo = CommonlyUsedDataUtils.getInstance().getBirthdayAgeSex(InsuredLoginUtils.getIdCardNo());
                String sexText = "1".equals(String.valueOf(certNoinfo.get("sex"))) ? "男" : "女";
                VisitorModel visitorModel = new VisitorModel(InsuredLoginUtils.getName(), InsuredLoginUtils.getIdCardNo(), String.valueOf(certNoinfo.get("sex")),
                        sexText, certNoinfo.get("age"), InsuredLoginUtils.getAccessoryId(), InsuredLoginUtils.getAccessoryUrl(), "1");
                visitorModel.setPhone(InsuredLoginUtils.getPhone());
                UpdateVisitorActivity.newIntance(SelectVisitorActivity.this,visitorModel);
                break;
            case R.id.srl_relatives_info://选择亲属数据
                onRelativesInfo();
                break;
            case R.id.iv_add://新增就诊人
                Intent intent = new Intent(SelectVisitorActivity.this, AddVisitorActivity.class);
                startActivityForResult(intent, new OnActivityCallback() {
                    @Override
                    public void onActivityResult(int resultCode, @Nullable Intent data) {
                        if (resultCode == RESULT_OK) {
//                            data.getParcelableExtra();
                            mSrlRelativesInfo.setVisibility(View.VISIBLE);
                            mIvAdd.setVisibility(View.GONE);
                        }
                    }
                });
                break;
        }
    }

    //选择本人数据
    private void onPrescrInfo() {
        Intent intent = new Intent();
        Map<String, String> certNoinfo = CommonlyUsedDataUtils.getInstance().getBirthdayAgeSex(InsuredLoginUtils.getIdCardNo());
        if (certNoinfo != null) {
            String sexText = "1".equals(String.valueOf(certNoinfo.get("sex"))) ? "男" : "女";
            VisitorModel visitorModel = new VisitorModel(InsuredLoginUtils.getName(), InsuredLoginUtils.getIdCardNo(), String.valueOf(certNoinfo.get("sex")),
                    sexText, certNoinfo.get("age"), InsuredLoginUtils.getAccessoryId(), InsuredLoginUtils.getAccessoryUrl(), "1");

            intent.putExtra("visitorModel", visitorModel);
            setResult(RESULT_OK, intent);
            goFinish();
        }
    }

    //选择亲属数据
    private void onRelativesInfo() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        goFinish();
    }
}
