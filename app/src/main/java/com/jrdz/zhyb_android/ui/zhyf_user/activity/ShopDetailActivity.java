package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.viewPage.BaseViewPagerAndTabsAdapter_new;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.banner.listener.OnBannerClickListener;
import com.frame.compiler.widget.banner.manager.GlideAppSimpleImageManager_round;
import com.frame.compiler.widget.banner.widget.BannerLayout;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.google.android.material.appbar.AppBarLayout;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.NewsDetailActivity;
import com.jrdz.zhyb_android.ui.home.model.BannerDataModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.PhaSortAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.ShopInfoFragment_user;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.ShopProductFragment;
import com.jrdz.zhyb_android.ui.zhyf_user.model.GoodsBannerModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarRefreshModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopDetailModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopDetailMoreModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginSmrzUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.widget.ShoppingCartAnimationView;
import com.jrdz.zhyb_android.widget.pop.ShopCarPop_user;
import com.jrdz.zhyb_android.widget.pop.ShopDetailMorePop;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-03
 * 描    述：店铺详情--用户端
 * ================================================
 */
public class ShopDetailActivity extends BaseActivity implements OnBannerClickListener {
    private LinearLayout mLlTitle;
    private FrameLayout mFlClose;
    private ShapeLinearLayout mSllSearch;
    private FrameLayout mFlCollect;
    private FrameLayout mFlMore;
    private AppBarLayout mAppBarLayout;
    private BannerLayout mBanner;
    private CustomeRecyclerView mSrlPhaSort;
    private ImageView mIvCollect, mIvMore, mIvPic;
    private TextView mTvTitle;
    private ImageView mIvLocation;
    private TextView mTvDistance;
    private TextView mTvEstimatePrice;
    private TextView mTvCollectNum;
    private SlidingTabLayout mStbTablayout;
    private ViewPager mViewpager;

    private View mLine;
    private LinearLayout mLlContactMerchants;
    private LinearLayout mLlShopcar;
    private FrameLayout mFlShopcarIcon;
    private ShapeTextView mSvRedPoint;
    private TextView mTvPrice;
    private ShapeTextView mTvApplyBuy;

    private String fixmedins_code;
    private ShopDetailModel.DataBean pagerData;
    ArrayList<GoodsBannerModel> bannerDatas = new ArrayList<>();//存储轮播图数据
    String[] titles = new String[]{"全部商品", "商家信息"};
    private ShopProductFragment shopProductFragment;
    private ViewGroup mDecorView;
    ArrayList<GoodsModel.DataBean> hasJoinedShopCarDatas = new ArrayList<>();//记录加入购物车的数据

    private PhaSortAdapter phaSortAdapter;//药品分类适配器
    private int requestTag = 0;
    private ShopDetailMorePop shopDetailMorePop;//更多弹框
    private ShopCarPop_user shopCarPop;//购物车弹框
    private String totalRedNum = "0";//选择的商品总数量
    private String totalPrice = "0";//选择的商品总价
    private CustomerDialogUtils customerDialogUtils;
    private InsuredLoginSmrzUtils insuredLoginSmrzUtils;//判断用户是否登录以及实名注册

    //用户下单 需要通知商品详情的购物车刷新 传值为2 1代表在商品详情里面操作的时候 通知外部放购物车刷新
    private ObserverWrapper<ShopCarRefreshModel> mShopCarObserver=new ObserverWrapper<ShopCarRefreshModel>() {
        @Override
        public void onChanged(@Nullable ShopCarRefreshModel value) {
            switch (value.getTag_value()){
                case "2":
                    if (!EmptyUtils.isEmpty(TAG)&&!TAG.equals(value.getTag())){
                        if (pagerData==null){
                            return;
                        }

                        shopProductFragment.onRefresh();
                        getShopCarData();
                    }
                    break;
            }
        }
    };

    //分类点击事件
    private BaseQuickAdapter.OnItemClickListener mOnPhaSortItemClickListener = new BaseQuickAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
            if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_1000)) {
                return;
            }

            PhaSortModel.DataBean itemData = phaSortAdapter.getItem(i);
            // 2022-09-30 点击item 进入药品专区
            PhaSortSpecialAreaActivity_user.newIntance(ShopDetailActivity.this, itemData.getCatalogueId(), itemData.getCatalogueName(), fixmedins_code);
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.activity_shop_detail;
    }

    @Override
    public void initView() {
        super.initView();
        mLlTitle = findViewById(R.id.ll_title);
        mFlClose = findViewById(R.id.fl_close);
        mSllSearch = findViewById(R.id.sll_search);
        mFlCollect = findViewById(R.id.fl_collect);
        mIvCollect = findViewById(R.id.iv_collect);
        mFlMore = findViewById(R.id.fl_more);
        mIvMore = findViewById(R.id.iv_more);
        mAppBarLayout = findViewById(R.id.app_bar_layout);
        mBanner = findViewById(R.id.banner);
        mSrlPhaSort = findViewById(R.id.srl_pha_sort);
        mIvPic = findViewById(R.id.iv_pic);
        mTvTitle = findViewById(R.id.tv_title);
        mIvLocation = findViewById(R.id.iv_location);
        mTvDistance = findViewById(R.id.tv_distance);
        mTvEstimatePrice = findViewById(R.id.tv_estimate_price);
        mTvCollectNum = findViewById(R.id.tv_collect_num);
        mStbTablayout = findViewById(R.id.stb_tablayout);
        mViewpager = findViewById(R.id.viewpager);

        mLine = findViewById(R.id.line);
        mLlContactMerchants = findViewById(R.id.ll_contact_merchants);
        mLlShopcar = findViewById(R.id.ll_shopcar);
        mFlShopcarIcon = findViewById(R.id.fl_shopcar_icon);
        mSvRedPoint = findViewById(R.id.sv_red_point);
        mTvPrice = findViewById(R.id.tv_price);
        mTvApplyBuy = findViewById(R.id.tv_apply_buy);

        mDecorView = (ViewGroup) getWindow().getDecorView();
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(false, 0.2f)
                .titleBar(mLlTitle);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        fixmedins_code = getIntent().getStringExtra("fixmedins_code");
        super.initData();
        MsgBus.sendShopCarRefresh().observe(this, mShopCarObserver);
        //药品分类
        mSrlPhaSort.setHasFixedSize(true);
        mSrlPhaSort.setLayoutManager(new GridLayoutManager(this, 4, GridLayoutManager.VERTICAL, false));
        phaSortAdapter = new PhaSortAdapter();
        mSrlPhaSort.setAdapter(phaSortAdapter);

        showWaitDialog();
        getPagerData();
        getPhaSortData();
        getShopCarData();
    }

    @Override
    public void initEvent() {
        super.initEvent();
        phaSortAdapter.setOnItemClickListener(mOnPhaSortItemClickListener);
        mFlClose.setOnClickListener(this);
        mSllSearch.setOnClickListener(this);
        mFlMore.setOnClickListener(this);
        mFlCollect.setOnClickListener(this);
        mLlContactMerchants.setOnClickListener(this);
        mLlShopcar.setOnClickListener(this);
        mTvApplyBuy.setOnClickListener(this);
    }

    //获取页面详情数据
    private void getPagerData() {
        ShopDetailModel.sendShopDetailRequest_user(TAG, fixmedins_code, String.valueOf(Constants.AppStorage.APP_LATITUDE),
                String.valueOf(Constants.AppStorage.APP_LONGITUDE), new CustomerJsonCallBack<ShopDetailModel>() {
                    @Override
                    public void onRequestError(ShopDetailModel returnData, String msg) {
                        dissWaitDailog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(ShopDetailModel returnData) {
                        dissWaitDailog();
                        pagerData = returnData.getData();
                        if (pagerData != null) {
                            //初始化 tablayout跟 viewpager
                            initTabLayout();
                            setPagerData();
                        }
                    }
                });
    }

    //初始化 tablayout跟 viewpager
    private void initTabLayout() {
        shopProductFragment = ShopProductFragment.newIntance(fixmedins_code);
        BaseViewPagerAndTabsAdapter_new baseViewPagerAndTabsAdapter_new = new BaseViewPagerAndTabsAdapter_new(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0://店铺风格
                        return shopProductFragment;
                    case 1://店铺信息
                        return ShopInfoFragment_user.newIntance(pagerData);
                }
                return shopProductFragment;
            }
        };

        baseViewPagerAndTabsAdapter_new.setData(Arrays.asList(titles));
        mViewpager.setAdapter(baseViewPagerAndTabsAdapter_new);
        mStbTablayout.setViewPager(mViewpager);
    }

    //设置页面详情数据
    private void setPagerData() {
        //设置收藏状态
        if ("1".equals(pagerData.getIsCollection())) {//已收藏
            mIvCollect.setImageResource(R.drawable.ic_collect_icon_pre);
        } else {//未收藏
            mIvCollect.setImageResource(R.drawable.ic_collect_icon_nor);
        }
        //设置轮播图
        setBannerData();
        //设置店铺信息
        GlideUtils.loadImg(pagerData.getStoreAccessoryUrl(), mIvPic, com.frame.compiler.R.drawable.ic_placeholder_bg,
                new RoundedCornersTransformation((int) getResources().getDimension(com.frame.compiler.R.dimen.dp_10), 0));
        mTvTitle.setText(EmptyUtils.strEmpty(pagerData.getStoreName()));
        mTvDistance.setText(pagerData.getDistance() + "km");
        mTvEstimatePrice.setText("起送 ¥" + pagerData.getStartingPrice());
        //是否展示店铺的月售数据
        if ("1".equals(pagerData.getIsShowSold())) {
            mTvCollectNum.setVisibility(View.VISIBLE);
            mTvCollectNum.setText("月售" + pagerData.getMonthlyStoreSales());
        } else {
            mTvCollectNum.setVisibility(View.GONE);
        }
    }

    //获取轮播图数据
    private void setBannerData() {
        bannerDatas.clear();
        if (!EmptyUtils.isEmpty(pagerData.getBannerAccessoryUrl1())) {
            bannerDatas.add(new GoodsBannerModel(pagerData.getBannerAccessoryUrl1()));
        }
        if (!EmptyUtils.isEmpty(pagerData.getBannerAccessoryUrl2())) {
            bannerDatas.add(new GoodsBannerModel(pagerData.getBannerAccessoryUrl2()));
        }
        if (!EmptyUtils.isEmpty(pagerData.getBannerAccessoryUrl3())) {
            bannerDatas.add(new GoodsBannerModel(pagerData.getBannerAccessoryUrl3()));
        }

        int banner_delay_time = new BigDecimal(pagerData.getIntervaltime()).multiply(new BigDecimal("1000")).intValue();

        if (bannerDatas != null && !bannerDatas.isEmpty()) {
            if (bannerDatas.size() == 1) {
                mBanner.initTips(false, false, false)
                        .setImageLoaderManager(new GlideAppSimpleImageManager_round())
                        .setPageNumViewMargin(12, 12, 12, 12)
                        .setViewPagerTouchMode(true)
                        .setDelayTime(banner_delay_time)
                        .initListResources(bannerDatas)
                        .switchBanner(false)
                        .setOnBannerClickListener(ShopDetailActivity.this);
            } else {
                mBanner.initTips(false, true, false)
                        .setImageLoaderManager(new GlideAppSimpleImageManager_round())
                        .setPageNumViewMargin(12, 12, 12, 12)
                        .setViewPagerTouchMode(false)
                        .setDelayTime(banner_delay_time)
                        .initListResources(bannerDatas)
                        .switchBanner(true)
                        .setOnBannerClickListener(ShopDetailActivity.this);
            }
        }
    }

    //获取药品分类数据
    private void getPhaSortData() {
        PhaSortModel.sendShopSortRequest_user(TAG, fixmedins_code, new CustomerJsonCallBack<PhaSortModel>() {
            @Override
            public void onRequestError(PhaSortModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(PhaSortModel returnData) {
                dissWaitDailog();
                if (phaSortAdapter!=null){
                    phaSortAdapter.setNewData(returnData.getData());
                }
            }
        });
    }

    //获取已加入购物车的数据
    protected void getShopCarData() {
        // 2022-11-05 请求接口 获取已加入购物车数据
        if (!InsuredLoginUtils.isLogin()) {
            dissWaitDailog();
            return;
        }
        ShopCarModel.sendShopCarRequest(TAG, fixmedins_code, "0", "1", new CustomerJsonCallBack<ShopCarModel>() {
            @Override
            public void onRequestError(ShopCarModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(ShopCarModel returnData) {
                dissWaitDailog();
                hasJoinedShopCarDatas.clear();
                List<ShopCarModel.DataBean> datas = returnData.getData();
                if (datas != null && !datas.isEmpty() && datas.get(0).getShoppingCartGoods() != null) {
                    //已加入购物车的数据
                    hasJoinedShopCarDatas.addAll(datas.get(0).getShoppingCartGoods());
                }
                calculatePrice(null, null);

                onGatherOrders();
            }
        });
    }

    //不够配送费时 需要凑单的时候调用
    protected void onGatherOrders() {

    }

    @Override
    public void onBannerClick(View view, int position, Object model) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            return;
        }

        if (model instanceof BannerDataModel.DataBean) {
            BannerDataModel.DataBean dataBean = (BannerDataModel.DataBean) model;
            if (dataBean != null) {
                //轮播图type 1代表纯广告 2跳外链 3跳公告        跳公告的id我放在url里面
                switch (dataBean.getTypeX()) {
                    case "1":
                        break;
                    case "2":
                        MyWebViewActivity.newIntance(ShopDetailActivity.this, dataBean.getTitle(), dataBean.getUrlX(), true, false);
                        break;
                    case "3":
                        NewsDetailActivity.newIntance(ShopDetailActivity.this, dataBean.getUrlX());
                        break;
                }
            }
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_close:
                goFinish();
                break;
            case R.id.sll_search:
                ShopSearchActivity.newIntance(ShopDetailActivity.this, pagerData.getFixmedins_code());
                break;
            case R.id.fl_more://更多
                onMorePop();
                break;
            case R.id.fl_collect://收藏
                if ("1".equals(pagerData.getIsCollection())) {//已收藏
                    onDelShopCollect();
                } else {//未收藏
                    onAddShopCollect();
                }
                break;
            case R.id.ll_contact_merchants:
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(ShopDetailActivity.this, "提示", "是否拨打电话" + pagerData.getTelephone() + "?", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        RxTool.takePhone(ShopDetailActivity.this, EmptyUtils.strEmpty(pagerData.getTelephone()));
                    }
                });
                break;
            case R.id.ll_shopcar://购物车弹框
                if (hasJoinedShopCarDatas.isEmpty()) {
                    showShortToast("请添加商品~");
                } else {
                    onShopcarPop();
                }
                break;
            case R.id.tv_apply_buy://立即购买
                if (insuredLoginSmrzUtils == null) {
                    insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
                }
                if (insuredLoginSmrzUtils.isLoginSmrz(ShopDetailActivity.this)) {
                    if (hasJoinedShopCarDatas.isEmpty()) {
                        showShortToast("请选择商品");
                        return;
                    }
                    if (shopCarPop != null && shopCarPop.isShowing()) {//结算购物车的数据
                        shopCarPop.dismiss();
                    }

                    OnlineBuyDrugActivity.newIntance(ShopDetailActivity.this, hasJoinedShopCarDatas, pagerData.getStoreName(),
                            pagerData.getStoreAccessoryUrl(), pagerData.getStartingPrice(), pagerData.getLatitude(), pagerData.getLongitude(),
                            pagerData.getFixmedins_type());

                    nextStep();
                }
                break;
        }
    }

    protected void nextStep() {

    }

    //添加店铺收藏
    private void onAddShopCollect() {
        showWaitDialog();
        BaseModel.sendAddStoreCollectionRequest(TAG, fixmedins_code, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("收藏成功");

                mIvCollect.setImageResource(R.drawable.ic_collect_icon_pre);
                pagerData.setIsCollection("1");
            }
        });
    }

    //删除店铺收藏
    private void onDelShopCollect() {
        showWaitDialog();
        BaseModel.sendDelStoreCollectionRequest(TAG, fixmedins_code, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("删除收藏成功");

                mIvCollect.setImageResource(R.drawable.ic_collect_icon_nor);
                pagerData.setIsCollection("0");
            }
        });
    }

    //更多弹框
    private void onMorePop() {
        if (shopDetailMorePop == null) {
            shopDetailMorePop = new ShopDetailMorePop(ShopDetailActivity.this, CommonlyUsedDataUtils.getInstance().getShopDetailMoreData());
        }

        shopDetailMorePop.setOnListener(new ShopDetailMorePop.IOptionListener() {
            @Override
            public void onItemClick(ShopDetailMoreModel item) {
                switch (item.getText()) {
                    case "首页":
                        SmartPhaMainActivity_user.newIntance(ShopDetailActivity.this, 0);
                        break;
                    case "投诉商家":
                        ComplaintStoreActivity.newIntance(ShopDetailActivity.this, pagerData.getFixmedins_code());
                        break;
                }
            }
        });

        shopDetailMorePop.showPopupWindow(mIvMore);
    }

    //购物车弹框
    protected void onShopcarPop() {
        if (shopCarPop == null) {
            shopCarPop = new ShopCarPop_user(ShopDetailActivity.this, hasJoinedShopCarDatas, totalRedNum, totalPrice);
        } else {
            shopCarPop.setData(hasJoinedShopCarDatas, totalRedNum, totalPrice);
        }

        shopCarPop.setOnListener(new ShopCarPop_user.IOptionListener() {
            @Override
            public void onCleanShopCar() {
                if (customerDialogUtils == null) {
                    customerDialogUtils = new CustomerDialogUtils();
                }
                customerDialogUtils.showDialog(ShopDetailActivity.this, "提示", "确定清空购物车商品？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        onDelAllShopCar();
                    }
                });
            }

            @Override
            public void onItemClick(GoodsModel.DataBean itemData) {
                // 2022-11-05 跳转商品详情页面
                GoodDetailActivity.newIntance(ShopDetailActivity.this, itemData.getGoodsNo(),itemData.getFixmedins_code(),itemData.getGoodsName());
            }

            @Override
            public void onPopAdd(GoodsModel.DataBean itemData, TextView tvNum, TextView tvPrice) {
                onAddShopcar(itemData, tvNum, tvPrice, "add");
            }

            @Override
            public void onPopReduce(GoodsModel.DataBean itemData, TextView tvNum, TextView tvPrice) {
                onAddShopcar(itemData, tvNum, tvPrice, "reduce");
            }
        });
        shopCarPop.showPopupWindow(mLine);
    }

    //清空购物车
    private void onDelAllShopCar() {
        showWaitDialog();
        BaseModel.sendDelAllShopCarRequest(TAG, pagerData.getFixmedins_code(), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("清空购物车成功");
                MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "1"));
                hasJoinedShopCarDatas.clear();
                // 2022-11-29 刷新全部商品 当前页的数据
                onRefreshShopProductFragment();
                calculatePrice(null, null);
            }
        });
    }

    //当购物车弹框 清空购物车时 需要更新全部商品列表
    public void onRefreshShopProductFragment() {
        shopProductFragment.onCleanShopCar();
    }

    //加入购物车--购物车列表
    private void onAddShopcar(GoodsModel.DataBean itemData, TextView tvNum, TextView tvPrice, String tag) {
        showWaitDialog();
        BaseModel.sendAddShopCarRequest(TAG, itemData.getFixmedins_code(), itemData.getGoodsNo(), itemData.getShoppingCartNum(),
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "1"));
                        switch (tag) {
                            case "add":
                                onAddCarList(itemData, tvNum, tvPrice);
                                shopProductFragment.onRefreshRightItem(itemData);
                                break;
                            case "reduce":
                                onReduceCarList(itemData, "2", tvNum, tvPrice);
                                shopProductFragment.onRefreshRightItem(itemData);
                                break;
                        }
                    }
                });
    }

    /**
     * ★★★★★把商品添加到购物车的动画效果★★★★★
     */
    public void addCart(View view, GoodsModel.DataBean item) {
        ShoppingCartAnimationView shoppingCartAnimationView = new ShoppingCartAnimationView(this);
        int position[] = new int[2];
        view.getLocationInWindow(position);
        shoppingCartAnimationView.setStartPosition(new Point(position[0], position[1]));
        mDecorView.addView(shoppingCartAnimationView);
        int endPosition[] = new int[2];
        mFlShopcarIcon.getLocationInWindow(endPosition);
        shoppingCartAnimationView.setEndPosition(new Point(endPosition[0], endPosition[1]));
        shoppingCartAnimationView.startBeizerAnimation();

        //2022-11-04  加入购物车的记录列表
        onAddCarList(item, null, null);
    }

    //加入购物车的记录列表
    protected void onAddCarList(GoodsModel.DataBean item, TextView tvNum, TextView tvPrice) {
        //判断当前添加的商品 是否商品数为1 为1说明是新添加的商品 若不是 则说明数量大于1 那么遍历列表 查询是同一个id的情况下 把数量设置进去
        if (item.getShoppingCartNum() == 1) {
            hasJoinedShopCarDatas.add(item);
        } else {
            for (GoodsModel.DataBean hasJoinedShopCarData : hasJoinedShopCarDatas) {
                if (hasJoinedShopCarData.getGoodsNo().equals(item.getGoodsNo())) {
                    hasJoinedShopCarData.setShoppingCartNum(item.getShoppingCartNum());
                    break;
                }
            }
        }

        Log.e("CarList", "Add_CarListsize=====" + hasJoinedShopCarDatas.size());
        // 2022-11-04 计算总价
        calculatePrice(tvNum, tvPrice);
    }

    //去除购物车的记录列表  from1:来自店铺详情页面列表 2:来自店铺详情 购物车弹框
    public void onReduceCarList(GoodsModel.DataBean item, String from, TextView tvNum, TextView tvPrice) {
        //判断当前添加的商品 是否商品数为0 为0说明是要删除的商品
        if ("1".equals(from)) {
            if (item.getShoppingCartNum() <= 0) {
                hasJoinedShopCarDatas.remove(item);
            } else {
                for (GoodsModel.DataBean hasJoinedShopCarData : hasJoinedShopCarDatas) {
                    if (hasJoinedShopCarData.getGoodsNo().equals(item.getGoodsNo())) {
                        hasJoinedShopCarData.setShoppingCartNum(item.getShoppingCartNum());
                        break;
                    }
                }
            }
        }

        Log.e("CarList", "Reduce_CarListsize=====" + hasJoinedShopCarDatas.size());
        // 2022-11-04 计算总价
        calculatePrice(tvNum, tvPrice);
    }

    //计算价格
    private void calculatePrice(TextView tvNum, TextView tvPrice) {
        totalRedNum = "0";
        totalPrice = "0";
        for (GoodsModel.DataBean rightLinkModel : hasJoinedShopCarDatas) {
            if ("1".equals(rightLinkModel.getIsPromotion())) {//有折扣
                totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(rightLinkModel.getPreferentialPrice())
                        .multiply(new BigDecimal(String.valueOf(rightLinkModel.getShoppingCartNum())))).toPlainString();
            } else {//没有折扣
                totalPrice = new BigDecimal(totalPrice).add(new BigDecimal(rightLinkModel.getPrice())
                        .multiply(new BigDecimal(String.valueOf(rightLinkModel.getShoppingCartNum())))).toPlainString();
            }
            totalRedNum = new BigDecimal(totalRedNum).add(new BigDecimal(String.valueOf(rightLinkModel.getShoppingCartNum()))).toPlainString();
        }

        if ("0".equals(totalRedNum)) {
            mSvRedPoint.setVisibility(View.GONE);
        } else {
            mSvRedPoint.setVisibility(View.VISIBLE);
            mSvRedPoint.setText(totalRedNum);
        }

        if (tvNum != null) {
            tvNum.setText("(共" + totalRedNum + "件商品)");
        }
        if (tvPrice != null) {
            tvPrice.setText(totalPrice);
        }

        mTvPrice.setText(totalPrice);
    }

    //隐藏加载框
    public void dissWaitDailog() {
        requestTag++;
        if (requestTag >= 3) {
            hideWaitDialog();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mBanner != null) {
            mBanner.start();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mBanner != null) {
            mBanner.stop();
        }
    }

    //获取实名认证实体类
    public InsuredLoginSmrzUtils getInsuredLoginSmrzUtils(){
        if (insuredLoginSmrzUtils == null) {
            insuredLoginSmrzUtils = new InsuredLoginSmrzUtils();
        }

        return insuredLoginSmrzUtils;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBanner != null) {
            mBanner.destroy();
            mBanner = null;
        }
        if (shopDetailMorePop != null) {
            shopDetailMorePop.onCleanListener();
            shopDetailMorePop.onDestroy();
        }
        if (shopCarPop != null) {
            shopCarPop.onCleanListener();
            shopCarPop.onDestroy();
        }
        if (insuredLoginSmrzUtils != null) {
            insuredLoginSmrzUtils.cleanData();
            insuredLoginSmrzUtils = null;
        }
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    public static void newIntance(Context context, String fixmedins_code) {
        Intent intent = new Intent(context, ShopDetailActivity.class);
        intent.putExtra("fixmedins_code", fixmedins_code);
        context.startActivity(intent);
    }
}
