package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.viewPage.BaseViewPagerAndTabsAdapter_new;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.banner.listener.OnBannerClickListener;
import com.frame.compiler.widget.banner.manager.GlideAppSimpleImageManager_round;
import com.frame.compiler.widget.banner.widget.BannerLayout;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.tabLayout.SlidingTabLayout;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.google.android.material.appbar.AppBarLayout;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.NewsDetailActivity;
import com.jrdz.zhyb_android.ui.home.model.BannerDataModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.PhaSortAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.ShopInfoFragment_user;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.ShopProductFragment;
import com.jrdz.zhyb_android.ui.zhyf_user.model.GoodsBannerModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarRefreshModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopDetailModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopDetailMoreModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginSmrzUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.ParcelUtils;
import com.jrdz.zhyb_android.widget.ShoppingCartAnimationView;
import com.jrdz.zhyb_android.widget.pop.ShopCarPop;
import com.jrdz.zhyb_android.widget.pop.ShopDetailMorePop;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-03
 * 描    述：店铺详情--用户端  用于不够配送费时 需要凑单的时候跳转
 * ================================================
 */
public class ShopDetailActivity2 extends ShopDetailActivity {

    @Override
    protected void onGatherOrders() {
        if (hasJoinedShopCarDatas!=null&&!hasJoinedShopCarDatas.isEmpty()){
            onShopcarPop();
        }
    }

    @Override
    protected void nextStep() {
        goFinish();
    }

    public static void newIntance(Context context, String fixmedins_code) {
        Intent intent = new Intent(context, ShopDetailActivity2.class);
        intent.putExtra("fixmedins_code", fixmedins_code);
        context.startActivity(intent);
    }
}
