package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.alibaba.fastjson.JSON;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.SearchActivity;
import com.jrdz.zhyb_android.utils.MMKVUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-03-27
 * 描    述：店铺搜索页面
 * ================================================
 */
public class ShopSearchActivity extends SearchActivity {
    private String fixmedins_code="";

    @Override
    public void initData() {
        fixmedins_code=getIntent().getStringExtra("fixmedins_code");
        super.initData();
    }

    @Override
    protected void searchHistoryClick() {
        //历史记录 item点击事件
        searchHistoryAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                ShopSearchResultActivity.newIntance(ShopSearchActivity.this,searchHistoryAdapter.getItem(i),"",fixmedins_code);
            }
        });
    }

    @Override
    public void cleanHistory() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(ShopSearchActivity.this, "提示", "确定清除所有历史搜索记录？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                MMKVUtils.delete("zhyf_shop_search_history_user");
                searchHistoryAdapter.setNewData(new ArrayList<>());
            }
        });
    }

    //搜索
    @Override
    public void search() {
        if (EmptyUtils.isEmpty(mEtSearch.getText().toString())){
            showShortToast("输入药品名称或药房名称");
            return;
        }
        saveHistoryData(mEtSearch.getText().toString());

        ShopSearchResultActivity.newIntance(ShopSearchActivity.this,mEtSearch.getText().toString(),"",fixmedins_code);
    }

    //保存搜索数据
    private void saveHistoryData(String searchText){
        //获取历史搜索本地记录
        List<String> shopHistoryDatas= JSON.parseArray(MMKVUtils.getString("zhyf_shop_search_history_user"),String.class);
        if (shopHistoryDatas==null){
            shopHistoryDatas=new ArrayList<>();
        }else {
            if (shopHistoryDatas.contains(searchText)){
                shopHistoryDatas.remove(searchText);
            }

            if (shopHistoryDatas.size()>=10){
                shopHistoryDatas.remove(9);
            }
        }
        shopHistoryDatas.add(0,searchText);
        MMKVUtils.putString("zhyf_shop_search_history_user",JSON.toJSONString(shopHistoryDatas));
        getHistoryData();
    }

    //获取历史搜索记录
    @Override
    protected void getHistoryData() {
        //获取历史搜索本地记录
        List<String> shopHistoryDatas=JSON.parseArray(MMKVUtils.getString("zhyf_shop_search_history_user"),String.class);
        searchHistoryAdapter.setNewData(shopHistoryDatas);
    }

    @Override
    public void scanOrgan(String barCode) {
        ShopSearchResultActivity.newIntance(ShopSearchActivity.this,"",barCode,fixmedins_code);
    }

    public static void newIntance(Context context,String fixmedins_code) {
        Intent intent = new Intent(context, ShopSearchActivity.class);
        intent.putExtra("fixmedins_code", fixmedins_code);
        context.startActivity(intent);
    }

}
