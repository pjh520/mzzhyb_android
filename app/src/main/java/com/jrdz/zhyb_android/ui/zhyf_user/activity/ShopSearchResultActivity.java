package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;

import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.SearchResultActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.SearchResultModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-03-27
 * 描    述：店铺搜索结果页面
 * ================================================
 */
public class ShopSearchResultActivity extends SearchResultActivity {

    private String fixmedins_code;

    @Override
    public void initData() {
        fixmedins_code=getIntent().getStringExtra("fixmedins_code");
        super.initData();
    }

    @Override
    public void getData() {
        // 2022-10-09获取数据
        SearchResultModel.sendShopSearchResultRequest(TAG, String.valueOf(mPageNum), "20", mEtSearch.getText().toString(),
                "0","0",fixmedins_code,barCode,new CustomerJsonCallBack<SearchResultModel>() {
            @Override
            public void onRequestError(SearchResultModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(SearchResultModel returnData) {
                hideRefreshView();
                List<SearchResultModel.DataBean> infos = returnData.getData();
                if (mAdapter!=null&&infos != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(infos);
                    } else {
                        mAdapter.addData(infos);
                        mAdapter.loadMoreComplete();
                    }

                    if (infos.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    public static void newIntance(Context context, String searchText, String barCode, String fixmedins_code) {
        Intent intent = new Intent(context, ShopSearchResultActivity.class);
        intent.putExtra("searchText", searchText);
        intent.putExtra("barCode", barCode);
        intent.putExtra("fixmedins_code", fixmedins_code);
        context.startActivity(intent);
    }
}
