package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.frame.compiler.utils.viewPage.BaseViewPagerAdapter;
import com.frame.compiler.widget.CustomViewPager;
import com.frame.compiler.widget.costomBottomTab.BottomTabModel;
import com.frame.compiler.widget.costomBottomTab.CustomBottomTabLayout;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.ShoppingCarFragment;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.SmartPhaHomeFragment_user;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.SmartPhaMineFragment_user;
import com.jrdz.zhyb_android.ui.zhyf_user.fragment.SortFragment_user;
import com.jrdz.zhyb_android.utils.InsuredLoginSmrzUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-27
 * 描    述：智慧药房--用户端-首页
 * ================================================
 */
public class SmartPhaMainActivity_user extends BaseActivity {
    private CustomViewPager mVpHome;
    private CustomBottomTabLayout mBottomTabLayout;

    private String[] mTabNames = {"首页", "分类", "购物车", "我的"};
    //未选择状态下的本地图片
    private int[] mUnSelectIcons = {
            R.drawable.ic_zhyf_home_nor,
            R.drawable.ic_zhyf_sort_nor,
            R.drawable.ic_shop_car_nor,
            R.drawable.ic_zhyf_mine_nor};
    //选择状态下的本地图片
    private int[] mSelectIcons = {
            R.drawable.ic_zhyf_home_pre,
            R.drawable.ic_zhyf_sort_pre,
            R.drawable.ic_shop_car_pre,
            R.drawable.ic_zhyf_mine_pre};

    //未选中的颜色
    private int mUnSelectColor = R.color.color_606265;
    //选中的颜色
    private int mSelectColor = R.color.color_4870e0;
    //tab数据
    private List<BottomTabModel> mBottomTabs = new ArrayList<>();
    private int currentTab;
    private InsuredLoginSmrzUtils insuredLoginSmrzUtils;

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initView() {
        super.initView();

        mVpHome = findViewById(R.id.vp_home);
        mBottomTabLayout = findViewById(R.id.bottomTabLayout);
    }

    @Override
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(false)
                .titleBar(mTitleBar);
        mImmersionBar.init();
    }

    @Override
    public void initData() {
        super.initData();

        currentTab = getIntent().getIntExtra("currentTab", 0);
        initViewPager();
        initBottomTabs();
        setCurrentTab(currentTab);
    }

    @Override
    public void initEvent() {
        super.initEvent();
    }

    //初始化ViewPager
    private void initViewPager() {
        BaseViewPagerAdapter baseViewPagerAndTabsAdapter_new = new BaseViewPagerAdapter(getSupportFragmentManager(), 4) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0://首页
                        return SmartPhaHomeFragment_user.newIntance();
                    case 1://分类
                        return SortFragment_user.newIntance();
                    case 2://购物车
                        return ShoppingCarFragment.newIntance();
                    case 3://我的
                        return SmartPhaMineFragment_user.newIntance();
                }
                return null;
            }
        };
        mVpHome.setOffscreenPageLimit(3);
        mVpHome.setAdapter(baseViewPagerAndTabsAdapter_new);
    }

    //初始化底部的tab栏目
    private void initBottomTabs() {
        mBottomTabs.clear();
        for (int i = 0; i < mUnSelectIcons.length; i++) {
            BottomTabModel mBottomTab = null;
            mBottomTab = new BottomTabModel(mTabNames[i], mUnSelectColor,
                    mSelectColor, mUnSelectIcons[i], mSelectIcons[i], null, null);
            mBottomTabs.add(mBottomTab);
        }
        mBottomTabLayout.setBottomTabData(mBottomTabs);

        mBottomTabLayout.setUpWithViewPager(mVpHome, new CustomBottomTabLayout.onUpWithViewPagerListener() {
            @Override
            public boolean onTabSelect(int position) {
                if (position == 2||position == 3) {
                    if (insuredLoginSmrzUtils==null){
                        insuredLoginSmrzUtils=new InsuredLoginSmrzUtils();
                    }
                    if (!insuredLoginSmrzUtils.isLoginSmrz(SmartPhaMainActivity_user.this)){
                        return false;
                    }
                }

                mVpHome.setCurrentItem(position, false);//设置当前显示标签页为第一页
                setTabImmersionBar(position);
                return true;
            }

            @Override
            public void onPageSelected(int position) {
                mBottomTabLayout.setCurrentTab(position);
            }
        });
    }

    //设置当前的currentTab
    public void setCurrentTab(int currentTab) {
        if (mBottomTabLayout != null) {
            mBottomTabLayout.setCurrentTab(currentTab);
        }
        if (mVpHome != null) {
            mVpHome.setCurrentItem(currentTab);
        }
    }

    //设置各个tab页面得沉浸式状态栏
    private void setTabImmersionBar(int currentTab) {
        switch (currentTab) {
            case 0:
                ImmersionBar.with(this).statusBarDarkFont(false).transparentStatusBar().init();
                break;
            case 1:
                ImmersionBar.with(this).statusBarDarkFont(true, 0.2f).transparentStatusBar().init();
                break;
            case 2:
                ImmersionBar.with(this).statusBarDarkFont(true, 0.2f).transparentStatusBar().init();
                break;
            case 3:
                ImmersionBar.with(this).statusBarDarkFont(false).transparentStatusBar().init();
                break;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        int currentTab = intent.getIntExtra("currentTab", 0);
        setCurrentTab(currentTab);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    //正常流程进来
    public static void newIntance(Context context, int currentTab) {
        Intent intent = new Intent(context, SmartPhaMainActivity_user.class);
        intent.putExtra("currentTab", currentTab);
        context.startActivity(intent);
    }
}
