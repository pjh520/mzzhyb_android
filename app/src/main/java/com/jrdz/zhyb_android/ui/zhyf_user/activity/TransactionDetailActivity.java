package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.wheel.TimeWheelUtils;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewActivity;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TransactionDetailsModel;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.TransactionDetailsAdapter_user;
import com.jrdz.zhyb_android.widget.pop.TransactionScreenPop;
import com.scwang.smart.refresh.layout.constant.RefreshState;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-06-14
 * 描    述：交易明细
 * ================================================
 */
public class TransactionDetailActivity extends BaseRecyclerViewActivity implements TransactionScreenPop.IOptionListener{
    private ShapeLinearLayout mSllStartTime;
    private TextView mTvStartTime;
    private ShapeLinearLayout mSllEndTime;
    private TextView mTvEndTime;
    private ShapeTextView mTvScreen;

    private TimeWheelUtils timeWheelUtils;
    private TransactionScreenPop transactionScreenPop;
    private List<PhaSortModel.DataBean> datas;
    private String payType="",purchasedType="";
    private int requestTag=0;

    @Override
    public int getLayoutId() {
        return R.layout.activity_transaction_details;
    }

    @Override
    public void initView() {
        super.initView();
        mSllStartTime = findViewById(R.id.sll_start_time);
        mTvStartTime = findViewById(R.id.tv_start_time);
        mSllEndTime = findViewById(R.id.sll_end_time);
        mTvEndTime = findViewById(R.id.tv_end_time);
        mTvScreen = findViewById(R.id.tv_screen);

    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        getPurchasedTypeData();
        onRefresh(mRefreshLayout);
//        mAdapter.setNewData(null);
    }

    @Override
    public void initAdapter() {
        mAdapter=new TransactionDetailsAdapter_user();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mSllStartTime.setOnClickListener(this);
        mSllEndTime.setOnClickListener(this);
        mTvScreen.setOnClickListener(this);

        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    //获取购药种类数据
    private void getPurchasedTypeData() {
        PhaSortModel.sendShopClassifyRequest_user(TAG,"",new CustomerJsonCallBack<PhaSortModel>() {
            @Override
            public void onRequestError(PhaSortModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(PhaSortModel returnData) {
                datas = returnData.getData();
                for (PhaSortModel.DataBean data : datas) {
                    data.setChoose("0");
                }

                dissWaitDailog();
            }
        });
    }

    @Override
    public void getData() {
        super.getData();
        String begindate="", enddate="";
        if (!EmptyUtils.isEmpty(mTvStartTime.getText().toString())&&!EmptyUtils.isEmpty(mTvEndTime.getText().toString())){
            begindate=mTvStartTime.getText().toString();
            enddate=mTvEndTime.getText().toString();
        }

        TransactionDetailsModel.sendInsuredTransactionDetailsRequest(TAG, String.valueOf(mPageNum), "20",
                begindate, enddate, payType, purchasedType, new CustomerJsonCallBack<TransactionDetailsModel>() {
                    @Override
                    public void onRequestError(TransactionDetailsModel returnData, String msg) {
                        dissWaitDailog();
                        setLoadMoreFail();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(TransactionDetailsModel returnData) {
                        dissWaitDailog();
                        List<TransactionDetailsModel.DataBean> infos = returnData.getData();
                        if (mAdapter!=null&&infos != null) {
                            if (mPageNum == 0) {
                                mAdapter.setNewData(infos);
                                if (infos.size() <= 0) {
                                    mAdapter.isUseEmpty(true);
                                }
                            } else {
                                mAdapter.addData(infos);
                                mAdapter.loadMoreComplete();
                            }

                            if (infos.isEmpty()) {
                                if (mAdapter.getData().size() < 6) {
                                    mAdapter.loadMoreEnd(true);
                                } else {
                                    mAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.sll_start_time:
                KeyboardUtils.hideSoftInput(mSllStartTime);
                if (timeWheelUtils == null) {
                    timeWheelUtils = new TimeWheelUtils();
                    timeWheelUtils.isShowDay(true, true, true, true, true, true);
                }
                timeWheelUtils.showTimeWheel(TransactionDetailActivity.this, "开始日期", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mTvStartTime.setText(EmptyUtils.strEmpty(dateInfo));

                        if (!EmptyUtils.isEmpty(mTvEndTime.getText().toString())){
                            showWaitDialog();
                            onRefresh(mRefreshLayout);
                        }
                    }
                });
                break;
            case R.id.sll_end_time:
                KeyboardUtils.hideSoftInput(mSllStartTime);
                if (timeWheelUtils == null) {
                    timeWheelUtils = new TimeWheelUtils();
                    timeWheelUtils.isShowDay(true, true, true, true, true, true);
                }
                timeWheelUtils.showTimeWheel(TransactionDetailActivity.this, "结束日期", new TimeWheelUtils.TimeWheelClickListener() {
                    @Override
                    public void onchooseDate(String dateInfo) {
                        mTvEndTime.setText(EmptyUtils.strEmpty(dateInfo));
                        if (!EmptyUtils.isEmpty(mTvStartTime.getText().toString())){
                            showWaitDialog();
                            onRefresh(mRefreshLayout);
                        }
                    }
                });
                break;
            case R.id.tv_screen://筛选
                if (transactionScreenPop == null) {
                    if (datas!=null){
                        transactionScreenPop = new TransactionScreenPop(TransactionDetailActivity.this, datas, this);
                    }else {
                        showShortToast("数据有误，请关闭app重新进入");
                        return;
                    }
                }

                transactionScreenPop.showPopupWindow();
                break;
        }
    }

    @Override
    public void onSure(String payType,String purchasedType) {
        this.payType=payType;
        this.purchasedType=purchasedType;

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    //隐藏加载框
    public void dissWaitDailog() {
        requestTag++;
        if (requestTag >= 2) {
            if (mRefreshLayout.getState() == RefreshState.Refreshing) {
                mRefreshLayout.finishRefresh();
            } else {
                hideWaitDialog();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timeWheelUtils != null) {
            timeWheelUtils.dissTimeWheel();
            timeWheelUtils = null;
        }

        if (transactionScreenPop != null) {
            transactionScreenPop.onClean();
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, TransactionDetailActivity.class);
        context.startActivity(intent);
    }
}
