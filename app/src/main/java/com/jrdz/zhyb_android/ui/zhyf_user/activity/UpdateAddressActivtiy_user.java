package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.app.Activity;
import android.content.Intent;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_user.model.AddressManageModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-01
 * 描    述：
 * ================================================
 */
public class UpdateAddressActivtiy_user extends AddAddressActivtiy_user {
    private AddressManageModel.DataBean addressManageModel;
    private CustomerDialogUtils customerDialogUtils;

    @Override
    public void initData() {
        addressManageModel = getIntent().getParcelableExtra("addressManageModel");
        super.initData();

        setRightTitleView("删除");

        mEtUsername.setText(EmptyUtils.strEmpty(addressManageModel.getFullName()));
        mEtPhone.setText(EmptyUtils.strEmpty(addressManageModel.getPhone()));
        mTvRegion.setText(addressManageModel.getLocation());
        mEtDetailAddress.setText(EmptyUtils.strEmpty(addressManageModel.getAddress()));
        mSbDefault.setCheckedImmediatelyNoEvent("1".equals(addressManageModel.getIsDefault()) ? true : false);

        province = addressManageModel.getAreaName();//省
        city = EmptyUtils.strEmpty(addressManageModel.getCity());//市
    }

    @Override
    public void rightTitleViewClick() {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }

        customerDialogUtils.showDialog(UpdateAddressActivtiy_user.this, "提示", "是否确认删除该收货地址?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        // 2022-11-01 请求接口 删除地址
                        showWaitDialog();
                        BaseModel.sendDelAddressRequest_user(TAG, addressManageModel.getShipToAddressId(), new CustomerJsonCallBack<BaseModel>() {
                            @Override
                            public void onRequestError(BaseModel returnData, String msg) {
                                hideWaitDialog();
                                showShortToast(msg);
                            }

                            @Override
                            public void onRequestSuccess(BaseModel returnData) {
                                hideWaitDialog();
                                //删除成功
                                showShortToast("删除成功");
                                //数据检测无误，则可以待会返回上一个页面
                                Intent intent = new Intent();
                                intent.putExtra("deleteData", addressManageModel.getShipToAddressId());
                                setResult(RESULT_OK, intent);
                                goFinish();
                            }
                        });
                    }
                });
    }

    @Override
    protected void requestSave(double latitude, double longitude) {
        String defalt = "0";
        if (mSbDefault.isChecked()) {
            defalt = "1";
        }

        showWaitDialog();
        BaseModel.sendUpdateAddressRequest_user(TAG, addressManageModel.getShipToAddressId(), mEtUsername.getText().toString(),
                mEtPhone.getText().toString(), mTvRegion.getText().toString(), mEtDetailAddress.getText().toString(),
                String.valueOf(latitude),String.valueOf(longitude),province,city,defalt, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();

                        showShortToast("修改成功");
                        //数据检测无误，则可以待会返回上一个页面
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        goFinish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
        addressManageModel = null;
    }

    public static void newIntance(Activity activity, AddressManageModel.DataBean addressManageModel, int requestCode) {
        Intent intent = new Intent(activity, UpdateAddressActivtiy_user.class);
        intent.putExtra("addressManageModel", addressManageModel);
        activity.startActivityForResult(intent, requestCode);
    }
}
