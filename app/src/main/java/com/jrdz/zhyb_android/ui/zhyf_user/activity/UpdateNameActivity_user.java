package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.StringUtils;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.PwdUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.insured.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-08-11
 * 描    述： 修改昵称
 * ================================================
 */
public class UpdateNameActivity_user extends BaseActivity {
    private EditText mEtName;
    private FrameLayout mFlDelete;
    private ShapeTextView mTvUpdate;

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_name_user;
    }

    @Override
    public void initView() {
        super.initView();
        mEtName = findViewById(R.id.et_name);
        mFlDelete = findViewById(R.id.fl_delete);
        mTvUpdate = findViewById(R.id.tv_update);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mFlDelete.setOnClickListener(this);
        mTvUpdate.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);

        switch (v.getId()){
            case R.id.fl_delete://清空输入框
                mEtName.setText("");
                break;
            case R.id.tv_update://更新姓名
                if (EmptyUtils.isEmpty(mEtName.getText().toString())) {
                    showShortToast("请输入新的昵称");
                    return;
                }

//                if (!PwdUtils.textFormat(mEtName.getText().toString())){
//                    showShortToast("4~12个字符,一个汉字为2个字符,以英文字母或汉字开头,由汉字,英文字母,数字任意两种组合组成");
//                    return;
//                }

                showWaitDialog();
                BaseModel.sendUpdateNickNameRequest(TAG, mEtName.getText().toString().trim(),new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("昵称修改成功");
                        //2022-11-03 更新参保人存储的本地信息 昵称
                        InsuredLoginUtils.setNickname(mEtName.getText().toString().trim());
                        MsgBus.updateInsuredInfo().post("11");//更新昵称
                        finish();
                    }
                });
                break;
        }
    }

    public static void newIntance(Context context) {
        Intent intent = new Intent(context, UpdateNameActivity_user.class);
        context.startActivity(intent);
    }
}
