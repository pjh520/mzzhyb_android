package com.jrdz.zhyb_android.ui.zhyf_user.activity;

import android.content.Context;
import android.content.Intent;

import com.alibaba.fastjson.JSON;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.StringUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_user.model.VisitorDiseaseHistoryModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.VisitorModel;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.activity
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-08
 * 描    述：编辑就诊人
 * ================================================
 */
public class UpdateVisitorActivity extends AddVisitorActivity {
    private VisitorModel visitorModel;

    @Override
    public int getLayoutId() {
        return R.layout.activity_edit_visitor;
    }

    @Override
    public void initData() {
        visitorModel = getIntent().getParcelableExtra("visitorModel");
        super.initData();
        mEtName.setEnabled(false);
        mEtName.setText(EmptyUtils.strEmpty(visitorModel.getName()));
        mEtPhone.setEnabled(false);
        mEtPhone.setText(StringUtils.encryptionPhone(visitorModel.getPhone()));
        mEtCertNo.setEnabled(false);
        mEtCertNo.setText(StringUtils.encryptionIDCard(visitorModel.getIdCardNo()));
        mTvSex.setEnabled(false);
        mTvSex.setText(EmptyUtils.strEmpty(visitorModel.getSexText()));
        mTvAge.setEnabled(false);
        mTvAge.setText(EmptyUtils.strEmpty(visitorModel.getAge()));

        String AssociatedDiseases = InsuredLoginUtils.getAssociatedDiseases();
        if (!EmptyUtils.isEmpty(AssociatedDiseases)) {
            VisitorDiseaseHistoryModel visitorDiseaseHistoryModel = JSON.parseObject(AssociatedDiseases, VisitorDiseaseHistoryModel.class);
            //过往病史
            selectPastMedical = visitorDiseaseHistoryModel.getSelectPastMedical();
            etPastMedical = visitorDiseaseHistoryModel.getEtPastMedical();
            if ("0".equals(visitorDiseaseHistoryModel.getIsSelectPastMedical())) {
                mStvPastMedical.setSelected(false);
                mStvPastMedical.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.bar_transparent))
                        .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1))
                        .setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();

                mStvPastMedical.setTextColor(getResources().getColor(R.color.color_333333));
            } else {
                mStvPastMedical.setSelected(true);
                mStvPastMedical.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0))
                        .setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent)).intoBackground();

                mStvPastMedical.setTextColor(getResources().getColor(R.color.white));
            }
            String pastMedicalShowText=getShowText(selectPastMedical,etPastMedical);
            mStvPastMedical.setText(EmptyUtils.isEmpty(pastMedicalShowText)?"过往病史":"过往病史:" + pastMedicalShowText);
            //过敏史弹框
            selectAllergy = visitorDiseaseHistoryModel.getSelectAllergy();
            etAllergy = visitorDiseaseHistoryModel.getEtAllergy();
            if ("0".equals(visitorDiseaseHistoryModel.getIsSelectAllergy())) {
                mStvAllergy.setSelected(false);
                mStvAllergy.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.bar_transparent))
                        .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1))
                        .setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();

                mStvAllergy.setTextColor(getResources().getColor(R.color.color_333333));
            } else {
                mStvAllergy.setSelected(true);
                mStvAllergy.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0))
                        .setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent)).intoBackground();

                mStvAllergy.setTextColor(getResources().getColor(R.color.white));
            }
            String allergyShowText=getShowText(selectAllergy,etAllergy);
            mStvAllergy.setText(EmptyUtils.isEmpty(allergyShowText)?"过敏史":"过敏史:" + allergyShowText);
            //家族病史弹框
            selectFamily = visitorDiseaseHistoryModel.getSelectFamily();
            etFamily = visitorDiseaseHistoryModel.getEtFamily();
            if ("0".equals(visitorDiseaseHistoryModel.getIsSelectFamily())) {
                mStvFamily.setSelected(false);
                mStvFamily.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.bar_transparent))
                        .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1))
                        .setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();

                mStvFamily.setTextColor(getResources().getColor(R.color.color_333333));
            } else {
                mStvFamily.setSelected(true);
                mStvFamily.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0))
                        .setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent)).intoBackground();

                mStvFamily.setTextColor(getResources().getColor(R.color.white));
            }
            String familyShowText=getShowText(selectFamily,etFamily);
            mStvFamily.setText(EmptyUtils.isEmpty(familyShowText)?"家族病史":"家族病史:" + familyShowText);
            //肝功能异常
            if ("0".equals(visitorDiseaseHistoryModel.getIsSelectLiver())) {
                mStvLiver.setSelected(false);
                mStvLiver.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.bar_transparent))
                        .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1))
                        .setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();

                mStvLiver.setTextColor(getResources().getColor(R.color.color_333333));
            } else {
                mStvLiver.setSelected(true);
                mStvLiver.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0))
                        .setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent)).intoBackground();

                mStvLiver.setTextColor(getResources().getColor(R.color.white));
            }
            //肾功能异常
            if ("0".equals(visitorDiseaseHistoryModel.getIsSelectKidney())) {
                mStvKidney.setSelected(false);
                mStvKidney.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.bar_transparent))
                        .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1))
                        .setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();

                mStvKidney.setTextColor(getResources().getColor(R.color.color_333333));
            } else {
                mStvKidney.setSelected(true);
                mStvKidney.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0))
                        .setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent)).intoBackground();

                mStvKidney.setTextColor(getResources().getColor(R.color.white));
            }
            //妊娠哺乳期
            if ("0".equals(visitorDiseaseHistoryModel.getIsSelectPregnancyLactation())) {
                mStvPregnancyLactation.setSelected(false);
                mStvPregnancyLactation.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.bar_transparent))
                        .setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.dp_1))
                        .setStrokeColor(getResources().getColor(R.color.txt_color_666)).intoBackground();

                mStvPregnancyLactation.setTextColor(getResources().getColor(R.color.color_333333));
            } else {
                mStvPregnancyLactation.setSelected(true);
                mStvPregnancyLactation.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0))
                        .setStrokeWidth(0).setStrokeColor(getResources().getColor(R.color.bar_transparent)).intoBackground();

                mStvPregnancyLactation.setTextColor(getResources().getColor(R.color.white));
            }
        }

        mTvSubmit.setText("保存");
    }

    @Override
    protected void onSubmit() {
        VisitorDiseaseHistoryModel visitorDiseaseHistoryModel = new VisitorDiseaseHistoryModel(mStvPastMedical.isSelected() ? "1" : "0", selectPastMedical, etPastMedical,
                mStvAllergy.isSelected() ? "1" : "0", selectAllergy, etAllergy,
                mStvFamily.isSelected() ? "1" : "0", selectFamily, etFamily,
                mStvLiver.isSelected() ? "1" : "0", mStvKidney.isSelected() ? "1" : "0", mStvPregnancyLactation.isSelected() ? "1" : "0");

        showWaitDialog();
        BaseModel.sendEditInsuredAssociatedDiseasesRequest(TAG, JSON.toJSONString(visitorDiseaseHistoryModel), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast(returnData.getMsg());
                InsuredLoginUtils.setAssociatedDiseases(JSON.toJSONString(visitorDiseaseHistoryModel));
                goFinish();
            }
        });

    }

    public static void newIntance(Context context, VisitorModel visitorModel) {
        Intent intent = new Intent(context, UpdateVisitorActivity.class);
        intent.putExtra("visitorModel", visitorModel);
        context.startActivity(intent);
    }
}
