package com.jrdz.zhyb_android.ui.zhyf_user.adapter;

import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.StringUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_user.model.AddressManageModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-31
 * 描    述：
 * ================================================
 */
public class AddressManageAdapter extends BaseQuickAdapter<AddressManageModel.DataBean, BaseViewHolder> {
    private AddressManageModel.DataBean currentItemData;
    private ImageView currentIvSelect;

    public AddressManageAdapter() {
        super(R.layout.layout_address_manage_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder = super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.fl_select,R.id.tv_default,R.id.fl_edit,R.id.tv_edit,R.id.fl_del,R.id.tv_del);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, AddressManageModel.DataBean item) {
        ImageView ivSelect=baseViewHolder.getView(R.id.iv_select);

        if ("1".equals(item.getIsDefault())){
            ivSelect.setImageResource(R.drawable.ic_product_select_pre);
            currentItemData=item;
            currentIvSelect=ivSelect;
        }else {
            ivSelect.setImageResource(R.drawable.ic_product_select_nor);
        }

        baseViewHolder.setText(R.id.tv_name_phone, item.getFullName()+"    "+ StringUtils.encryptionPhone(item.getPhone()));
        baseViewHolder.setText(R.id.tv_address, item.getLocation()+item.getAddress());
    }

    public AddressManageModel.DataBean getCurrentItemData() {
        return currentItemData;
    }

    public void setCurrentItemData(AddressManageModel.DataBean currentItemData) {
        this.currentItemData = currentItemData;
    }

    public ImageView getCurrentIvSelect() {
        return currentIvSelect;
    }

    public void setCurrentIvSelect(ImageView currentIvSelect) {
        this.currentIvSelect = currentIvSelect;
    }
}
