package com.jrdz.zhyb_android.ui.zhyf_user.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.widget.RatioImageView;
import com.frame.compiler.widget.glide.GlideUtils;
import com.jrdz.zhyb_android.R;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-19
 * 描    述：
 * ================================================
 */
public class CertPicAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    public CertPicAdapter() {
        super(R.layout.layout_cert_pic_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, String item) {
        RatioImageView ivCertPic=baseViewHolder.getView(R.id.iv_cert_pic);
        GlideUtils.loadImg(item, ivCertPic);
    }
}
