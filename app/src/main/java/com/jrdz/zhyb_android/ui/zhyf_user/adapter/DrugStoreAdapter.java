package com.jrdz.zhyb_android.ui.zhyf_user.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_user.model.DrugStoreModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-27
 * 描    述：
 * ================================================
 */
public class DrugStoreAdapter extends BaseQuickAdapter<DrugStoreModel.DataBean, BaseViewHolder> {
    public DrugStoreAdapter() {
        super(R.layout.layout_drugstore_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, DrugStoreModel.DataBean item) {
        ImageView ivStoreHead=baseViewHolder.getView(R.id.iv_store_head);
        TextView tvSaleNum=baseViewHolder.getView(R.id.tv_sale_num);

        baseViewHolder.setText(R.id.tv_store_name, EmptyUtils.strEmpty(item.getStoreName()));
        baseViewHolder.setText(R.id.tv_score, EmptyUtils.strEmpty(item.getStarLevel()));
        baseViewHolder.setText(R.id.tv_start_price, "起送 ¥"+item.getStartingPrice());

        //是否展示店铺的月售数据
        if ("1".equals(item.getIsShowSold())){
            tvSaleNum.setVisibility(View.VISIBLE);
            tvSaleNum.setText("已售" + item.getMonthlyStoreSales());
        }else {
            tvSaleNum.setVisibility(View.GONE);
        }

        baseViewHolder.setText(R.id.tv_store_address, "机构地址:"+item.getStoreAdress()+item.getDetailedAddress());
        baseViewHolder.setText(R.id.tv_store_distance, item.getDistance()+"km");
        GlideUtils.loadImg(item.getStoreAccessoryUrl(), ivStoreHead, com.frame.compiler.R.drawable.ic_placeholder_bg,
                new RoundedCornersTransformation((int) mContext.getResources().getDimension(com.frame.compiler.R.dimen.dp_10), 0));
    }
}
