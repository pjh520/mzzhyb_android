package com.jrdz.zhyb_android.ui.zhyf_user.adapter;

import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.widget.glide.GlideUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.FreeBackActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.model.FreeBackPicModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-03
 * 描    述：
 * ================================================
 */
public class FreeBackPicAdapter extends BaseMultiItemQuickAdapter<FreeBackPicModel, BaseViewHolder> {
    public FreeBackPicAdapter() {
        super(null);
        addItemType(FreeBackActivity.PIC_NOR, R.layout.layout_freeback_pic_item);
        addItemType(FreeBackActivity.PIC_ADDBTN, R.layout.layout_freeback_addpic_item);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.iv_delete);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, FreeBackPicModel freeBackModel) {
        switch (baseViewHolder.getItemViewType()) {
            case FreeBackActivity.PIC_NOR://新增图片
                ImageView ivPhoto=baseViewHolder.getView(R.id.iv_photo);
                GlideUtils.loadImg(freeBackModel.getImg(), ivPhoto);
                break;
            case FreeBackActivity.PIC_ADDBTN://新增按钮

                break;
        }
    }

}
