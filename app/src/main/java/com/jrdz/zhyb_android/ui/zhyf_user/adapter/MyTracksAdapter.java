package com.jrdz.zhyb_android.ui.zhyf_user.adapter;

import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.DecimalFormatUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TagTextBean;
import com.jrdz.zhyb_android.ui.zhyf_user.model.MyTracksModel;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.widget.TagTextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-02
 * 描    述：
 * ================================================
 */
public class MyTracksAdapter extends BaseMultiItemQuickAdapter<MyTracksModel.DataBean, BaseViewHolder> {
    private boolean isEditStatus;
    List<TagTextBean> tags = new ArrayList<>();

    public MyTracksAdapter() {
        super(null);
        addItemType(MyTracksModel.TITLE_TAG, R.layout.layout_mytracks_head_item);
        addItemType(MyTracksModel.CONTENT_TAG, R.layout.layout_mytracks_content_item);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.fl_date_all_select,R.id.fl_product_select);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, MyTracksModel.DataBean item) {
        switch (baseViewHolder.getItemViewType()) {
            case MyTracksModel.TITLE_TAG://头部
                onBindHead(baseViewHolder,item);
                break;
            case MyTracksModel.CONTENT_TAG://内容
                onBindContent(baseViewHolder,item);
                break;
        }
    }

    //绑定头部数据
    private void onBindHead(BaseViewHolder baseViewHolder, MyTracksModel.DataBean item) {
        FrameLayout flDateAllSelect=baseViewHolder.getView(R.id.fl_date_all_select);
        ImageView ivDateAllSelect=baseViewHolder.getView(R.id.iv_date_all_select);

        baseViewHolder.setText(R.id.tv_date, EmptyUtils.strEmpty(item.getGoodsName()));

        flDateAllSelect.setVisibility(isEditStatus? View.VISIBLE:View.GONE);
        ivDateAllSelect.setImageResource(item.isChoose()?R.drawable.ic_product_select_pre:R.drawable.ic_product_select_nor);
    }

    //绑定内容数据
    private void onBindContent(BaseViewHolder baseViewHolder, MyTracksModel.DataBean item) {
        FrameLayout flProductSelect=baseViewHolder.getView(R.id.fl_product_select);
        ImageView ivProductSelect=baseViewHolder.getView(R.id.iv_product_select);

        ImageView ivPic=baseViewHolder.getView(R.id.iv_pic);
        ShapeTextView stvOtc=baseViewHolder.getView(R.id.stv_otc);
        ShapeTextView stvEnterpriseTag= baseViewHolder.getView(R.id.stv_enterprise_tag);
        TagTextView tvTitle=baseViewHolder.getView(R.id.tv_title);
        TextView tvDescribe=baseViewHolder.getView(R.id.tv_describe);
        ShapeTextView stvDiscount=baseViewHolder.getView(R.id.stv_discount);
        TextView tvNum=baseViewHolder.getView(R.id.tv_num);
        TextView tvEstimatePrice=baseViewHolder.getView(R.id.tv_estimate_price);
        ShapeTextView stv02=baseViewHolder.getView(R.id.stv_02);
        TextView tvRealPrice=baseViewHolder.getView(R.id.tv_real_price);

        flProductSelect.setVisibility(isEditStatus? View.VISIBLE:View.GONE);
        ivProductSelect.setImageResource(item.isChoose()?R.drawable.ic_product_select_pre:R.drawable.ic_product_select_nor);

        GlideUtils.loadImg(item.getBannerAccessoryUrl1(),ivPic,R.drawable.ic_placeholder_top_corner_bg,
                new RoundedCornersTransformation(mContext.getResources().getDimensionPixelSize(R.dimen.dp_16),0,
                        RoundedCornersTransformation.CornerType.TOP));

        //判断是否是处方药
        if ("1".equals(item.getPresc_ItemType())){
            stvOtc.setText("OTC");
            stvOtc.setVisibility(View.VISIBLE);
        }else if ("2".equals(item.getPresc_ItemType())){
            stvOtc.setText("处方药");
            stvOtc.setVisibility(View.VISIBLE);
        }else {
            stvOtc.setVisibility(View.GONE);
        }
        //判断是否有折扣
        double promotionDiscountVlaue = new BigDecimal(item.getPromotionDiscount()).doubleValue();
        if ("1".equals(item.getIsPromotion())&&promotionDiscountVlaue<10){
            stv02.setVisibility(View.VISIBLE);
            tvRealPrice.setVisibility(View.VISIBLE);
            stvDiscount.setVisibility(View.VISIBLE);

            stvDiscount.setText(DecimalFormatUtils.noZero(item.getPromotionDiscount())+"折优惠");
            tvEstimatePrice.setText(EmptyUtils.strEmpty(item.getPreferentialPrice()));
            tvRealPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            tvRealPrice.getPaint().setAntiAlias(true);
            tvRealPrice.setText("¥"+DecimalFormatUtils.noZero(item.getPrice()));
        }else {
            stv02.setVisibility(View.GONE);
            tvRealPrice.setVisibility(View.GONE);
            stvDiscount.setVisibility(View.GONE);

            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(item.getPrice())));
        }

        //判断1.用户是否是企业基金用户 2.是否支持企业基金支付
        if ("1".equals(InsuredLoginUtils.getIsEnterpriseFundPay())&&"1".equals(item.getIsEnterpriseFundPay())){
            stvEnterpriseTag.setVisibility(View.VISIBLE);
        }else {
            stvEnterpriseTag.setVisibility(View.GONE);
        }
        //判断是否是平台药 若是 需要展示医保
        tags.clear();
        if ("1".equals(item.getIsPlatformDrug())){//是平台药
            tags.add(new TagTextBean("医保",R.color.color_ee8734));
            tvTitle.setContentAndTag(EmptyUtils.strEmpty(item.getGoodsName()), tags);
        }else {
            tvTitle.setContentAndTag(EmptyUtils.strEmpty(item.getGoodsName()), tags);
        }
        tvDescribe.setText(EmptyUtils.strEmpty(item.getEfccAtd()));
        //是否展示商品的月售数据
        if ("1".equals(item.getIsShowGoodsSold())){
            tvNum.setVisibility(View.VISIBLE);
            tvNum.setText("月售"+item.getMonthlySales());
        }else {
            tvNum.setVisibility(View.GONE);
        }
    }

    //设置是否处在编辑模式
    public void setIsEditStatus(boolean isEditStatus){
        this.isEditStatus=isEditStatus;
        notifyDataSetChanged();
    }
}
