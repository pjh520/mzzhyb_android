package com.jrdz.zhyb_android.ui.zhyf_user.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.GoodDetailActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.PhaSortSpecialAreaActivity2_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.PhaSortSpecialAreaModel;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-08-14
 * 描    述：
 * ================================================
 */
public class PhaSortSpecialAreaAdapter2_user extends BaseQuickAdapter<PhaSortSpecialAreaModel.DataBean, BaseViewHolder> {
    public PhaSortSpecialAreaAdapter2_user() {
        super(R.layout.layout_specicalarea_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.ll_title);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, PhaSortSpecialAreaModel.DataBean item) {
        CustomeRecyclerView crlProduct=baseViewHolder.getView(R.id.crl_product);

        baseViewHolder.setText(R.id.tv_title, EmptyUtils.strEmpty(item.getStoreName()));
        baseViewHolder.setText(R.id.tv_score, EmptyUtils.strEmpty(item.getStarLevel()));
        baseViewHolder.setText(R.id.tv_store_distance, item.getDistance()+"km");

        crlProduct.setLayoutManager(new GridLayoutManager(mContext,3, RecyclerView.VERTICAL,false));
        crlProduct.setHasFixedSize(true);
        SpecialAreaProductAdapter specialAreaProductAdapter=new SpecialAreaProductAdapter();
        crlProduct.setAdapter(specialAreaProductAdapter);
        specialAreaProductAdapter.setNewData(item.getStoreInformationGoods());

        specialAreaProductAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }

                GoodsModel.DataBean itemData = specialAreaProductAdapter.getItem(i);
                GoodDetailActivity.newIntance(mContext, itemData.getGoodsNo(),itemData.getFixmedins_code(),itemData.getGoodsName());
            }
        });
    }
}
