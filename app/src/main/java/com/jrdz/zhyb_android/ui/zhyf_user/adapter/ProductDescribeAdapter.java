package com.jrdz.zhyb_android.ui.zhyf_user.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ProductDescribeModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/11/6
 * 描    述：
 * ================================================
 */
public class ProductDescribeAdapter extends BaseQuickAdapter<ProductDescribeModel, BaseViewHolder> {
    public ProductDescribeAdapter() {
        super(R.layout.layout_product_describe_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ProductDescribeModel item) {
        ImageView iv=baseViewHolder.getView(R.id.iv);

        iv.setImageResource(item.getImg());
        baseViewHolder.setText(R.id.tv_title, EmptyUtils.strEmpty(item.getTitle()));
        baseViewHolder.setText(R.id.tv_describe, EmptyUtils.strEmpty(item.getDescribe()));
    }
}
