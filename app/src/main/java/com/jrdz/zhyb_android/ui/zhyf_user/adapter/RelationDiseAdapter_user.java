package com.jrdz.zhyb_android.ui.zhyf_user.adapter;

import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_user.model.RelationDiseModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-08
 * 描    述：
 * ================================================
 */
public class RelationDiseAdapter_user extends BaseQuickAdapter<RelationDiseModel, BaseViewHolder> {
    public RelationDiseAdapter_user() {
        super(R.layout.layout_relation_disetype_item_user, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, RelationDiseModel dataBean) {
        TextView tvWx=baseViewHolder.getView(R.id.tv_text);
        tvWx.setText(EmptyUtils.strEmpty(dataBean.getText()));

        tvWx.setSelected(dataBean.isChoose());
    }
}

