package com.jrdz.zhyb_android.ui.zhyf_user.adapter;

import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.DecimalFormatUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TagTextBean;
import com.jrdz.zhyb_android.ui.zhyf_user.model.SearchResultModel_user;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.widget.TagTextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-09
 * 描    述：
 * ================================================
 */
public class SearchResultGoodAdapter_user extends BaseQuickAdapter<SearchResultModel_user.DataBean, BaseViewHolder> {
    List<TagTextBean> tags = new ArrayList<>();

    public SearchResultGoodAdapter_user() {
        super(R.layout.layout_search_result_good_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder=super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.rl_product,R.id.rl_shop);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, SearchResultModel_user.DataBean item) {
        ImageView ivPic=baseViewHolder.getView(R.id.iv_pic);
        ShapeTextView stvOtc=baseViewHolder.getView(R.id.stv_otc);
        ShapeTextView stvEnterpriseTag= baseViewHolder.getView(R.id.stv_enterprise_tag);
        TagTextView tvTitle=baseViewHolder.getView(R.id.tv_title);
        ShapeTextView stvDiscount=baseViewHolder.getView(R.id.stv_discount);
        LinearLayout llNum=baseViewHolder.getView(R.id.ll_num);
        TextView tvNum=baseViewHolder.getView(R.id.tv_num);
        TextView tvOnlyhasNum=baseViewHolder.getView(R.id.tv_onlyhas_num);
        TextView tvEstimatePrice=baseViewHolder.getView(R.id.tv_estimate_price);
        ShapeTextView stv02=baseViewHolder.getView(R.id.stv_02);
        TextView tvRealPrice=baseViewHolder.getView(R.id.tv_real_price);

        GlideUtils.loadImg(item.getBannerAccessoryUrl1(),ivPic,R.drawable.ic_placeholder_top_corner_bg,
                new RoundedCornersTransformation(mContext.getResources().getDimensionPixelSize(R.dimen.dp_16),0,
                        RoundedCornersTransformation.CornerType.TOP));

        //判断是否是处方药
        if ("1".equals(item.getItemType())){
            stvOtc.setText("OTC");
            stvOtc.setVisibility(View.VISIBLE);
        }else if ("2".equals(item.getItemType())){
            stvOtc.setText("处方药");
            stvOtc.setVisibility(View.VISIBLE);
        }else {
            stvOtc.setVisibility(View.GONE);
        }
        //判断是否有折扣
        double promotionDiscountVlaue = new BigDecimal(item.getPromotionDiscount()).doubleValue();
        if ("1".equals(item.getIsPromotion())&&promotionDiscountVlaue<10){
            stv02.setVisibility(View.VISIBLE);
            tvRealPrice.setVisibility(View.VISIBLE);
            stvDiscount.setVisibility(View.VISIBLE);

            stvDiscount.setText(DecimalFormatUtils.noZero(item.getPromotionDiscount())+"折优惠");
            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(item.getPreferentialPrice())));
            tvRealPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            tvRealPrice.getPaint().setAntiAlias(true);
            tvRealPrice.setText("¥"+DecimalFormatUtils.noZero(item.getPrice()));

            RelativeLayout.LayoutParams rlp= (RelativeLayout.LayoutParams) llNum.getLayoutParams();
            rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);//addRule参数对应RelativeLayout XML布局的属性
            rlp.addRule(RelativeLayout.RIGHT_OF,0);
            llNum.setLayoutParams(rlp);
        }else {
            stv02.setVisibility(View.GONE);
            tvRealPrice.setVisibility(View.GONE);
            stvDiscount.setVisibility(View.GONE);

            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(item.getPrice())));

            RelativeLayout.LayoutParams rlp= (RelativeLayout.LayoutParams) llNum.getLayoutParams();
            rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,0);//addRule参数对应RelativeLayout XML布局的属性
            rlp.addRule(RelativeLayout.RIGHT_OF,R.id.iv_pic);
            llNum.setLayoutParams(rlp);
        }

        tags.clear();
        if ("1".equals(item.getIsPlatformDrug())){//是平台药
            tags.add(new TagTextBean("医保",R.color.color_ee8734));
            tvTitle.setContentAndTag(EmptyUtils.strEmpty(item.getGoodsName()), tags);
        }else {
            tvTitle.setContentAndTag(EmptyUtils.strEmpty(item.getGoodsName()), tags);
        }

        //判断1.用户是否是企业基金用户 2.是否支持企业基金支付
        if ("1".equals(InsuredLoginUtils.getIsEnterpriseFundPay())&&"1".equals(item.getIsEnterpriseFundPay())){
            stvEnterpriseTag.setVisibility(View.VISIBLE);
        }else {
            stvEnterpriseTag.setVisibility(View.GONE);
        }
        //是否展示商品的月售数据
        if ("1".equals(item.getIsShowGoodsSold())){
            tvNum.setVisibility(View.VISIBLE);
            tvNum.setText("月售"+item.getMonthlySales());
        }else {
            tvNum.setVisibility(View.GONE);
        }

        if ("1".equals(item.getIsShowMargin())&&item.getInventoryQuantity()<=5){
            tvOnlyhasNum.setVisibility(View.VISIBLE);
            tvOnlyhasNum.setText("仅剩"+item.getInventoryQuantity()+"件");
        }else {
            tvOnlyhasNum.setVisibility(View.GONE);
        }

        baseViewHolder.setText(R.id.tv_shop_name, EmptyUtils.strEmpty(item.getStoreName()));
        baseViewHolder.setText(R.id.tv_score, EmptyUtils.strEmpty(item.getStarLevel()));
        baseViewHolder.setText(R.id.tv_start_price, "起送 ¥"+item.getStartingPrice());
        baseViewHolder.setText(R.id.tv_store_distance, item.getDistance()+"km");
    }
}
