package com.jrdz.zhyb_android.ui.zhyf_user.adapter;

import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCollectionModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-01
 * 描    述：
 * ================================================
 */
public class ShopCollectionAdapter extends BaseQuickAdapter<ShopCollectionModel.DataBean, BaseViewHolder> {
    public ShopCollectionAdapter() {
        super(R.layout.layout_shop_collection_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder = super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.srl_item,R.id.ll_del);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ShopCollectionModel.DataBean item) {
        ImageView ivPic=baseViewHolder.getView(R.id.iv_pic);

        GlideUtils.loadImg(item.getStoreAccessoryUrl(), ivPic, com.frame.compiler.R.drawable.ic_placeholder_bg,
                new RoundedCornersTransformation((int) mContext.getResources().getDimension(com.frame.compiler.R.dimen.dp_10), 0));
        baseViewHolder.setText(R.id.tv_title, EmptyUtils.strEmpty(item.getStoreName()));
        baseViewHolder.setText(R.id.tv_estimate_price, "起送 ¥"+item.getStartingPrice());
        baseViewHolder.setText(R.id.tv_collect_num, "已售"+item.getAllSales());
        baseViewHolder.setText(R.id.tv_distance, item.getDistance()+"km");

    }
}
