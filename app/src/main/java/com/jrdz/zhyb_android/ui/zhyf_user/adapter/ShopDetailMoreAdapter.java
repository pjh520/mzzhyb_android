package com.jrdz.zhyb_android.ui.zhyf_user.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopDetailMoreModel;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.ui.live.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/2/24 0024
 * 描    述：
 * ================================================
 */
public class ShopDetailMoreAdapter extends BaseQuickAdapter<ShopDetailMoreModel, BaseViewHolder> {

    public ShopDetailMoreAdapter() {
        super(R.layout.layout_shopdetail_more_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, ShopDetailMoreModel item) {
        ImageView iv=helper.getView(R.id.iv);
        TextView tv=helper.getView(R.id.tv);

        if (item.getImg()==-1){
            iv.setVisibility(View.GONE);
        }else {
            iv.setVisibility(View.VISIBLE);
            iv.setImageResource(item.getImg());
        }

        tv.setText(EmptyUtils.strEmpty(item.getText()));
    }
}
