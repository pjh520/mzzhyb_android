package com.jrdz.zhyb_android.ui.zhyf_user.adapter;

import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.DecimalFormatUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TagTextBean;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.widget.TagTextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-21
 * 描    述：
 * ================================================
 */
public class ShopProductRightLinkAdapter_user extends BaseQuickAdapter<GoodsModel.DataBean, BaseViewHolder> {
    List<TagTextBean> tags = new ArrayList<>();

    public ShopProductRightLinkAdapter_user() {
        super(R.layout.layout_shopproduct_linkview_body_item, null);
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder = super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.fl_num_add, R.id.fl_num_reduce);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, GoodsModel.DataBean item) {
        ImageView ivPic = baseViewHolder.getView(R.id.iv_pic);
        ShapeTextView stvOtc = baseViewHolder.getView(R.id.stv_otc);
        ShapeTextView stvEnterpriseTag = baseViewHolder.getView(R.id.stv_enterprise_tag);
        TagTextView tvTitle = baseViewHolder.getView(R.id.tv_title);
        ShapeTextView stvDiscount = baseViewHolder.getView(R.id.stv_discount);
        LinearLayout llNum = baseViewHolder.getView(R.id.ll_num);
        TextView tvNum = baseViewHolder.getView(R.id.tv_num);
        TextView tvOnlyhasNum = baseViewHolder.getView(R.id.tv_onlyhas_num);
        TextView tvEstimatePrice = baseViewHolder.getView(R.id.tv_estimate_price);
        ShapeTextView stv_02 = baseViewHolder.getView(R.id.stv_02);
        TextView tvRealPrice = baseViewHolder.getView(R.id.tv_real_price);
        //控制商品数量view
        FrameLayout flNumReduce = baseViewHolder.getView(R.id.fl_num_reduce);
        TextView tvSelectNum = baseViewHolder.getView(R.id.tv_select_num);
        FrameLayout flNumAdd = baseViewHolder.getView(R.id.fl_num_add);

        //设置数据
        GlideUtils.loadImg(item.getBannerAccessoryUrl1(), ivPic, R.drawable.ic_good_placeholder,
                new RoundedCornersTransformation(mContext.getResources().getDimensionPixelSize(R.dimen.dp_16), 0));
        //判断是否是处方药
        if ("1".equals(item.getItemType())) {
            stvOtc.setText("OTC");
            stvOtc.setVisibility(View.VISIBLE);
        } else if ("2".equals(item.getItemType())) {
            stvOtc.setText("处方药");
            stvOtc.setVisibility(View.VISIBLE);
        } else {
            stvOtc.setVisibility(View.GONE);
        }
        //判断是否有折扣
        double promotionDiscountVlaue = new BigDecimal(item.getPromotionDiscount()).doubleValue();
        if ("1".equals(item.getIsPromotion()) && promotionDiscountVlaue < 10) {
            if (item.getShoppingCartNum() > 0) {
                stv_02.setVisibility(View.GONE);
                tvRealPrice.setVisibility(View.GONE);
            } else {
                stv_02.setVisibility(View.VISIBLE);
                tvRealPrice.setVisibility(View.VISIBLE);
            }

            stvDiscount.setText(DecimalFormatUtils.noZero(item.getPromotionDiscount()) + "折优惠");
            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(item.getPreferentialPrice())));

            tvRealPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            tvRealPrice.getPaint().setAntiAlias(true);
            tvRealPrice.setText("¥" + DecimalFormatUtils.noZero(item.getPrice()));

            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) llNum.getLayoutParams();
            rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);//addRule参数对应RelativeLayout XML布局的属性
            rlp.addRule(RelativeLayout.RIGHT_OF, 0);
            llNum.setLayoutParams(rlp);
        } else {
            stv_02.setVisibility(View.GONE);
            tvRealPrice.setVisibility(View.GONE);
            stvDiscount.setVisibility(View.GONE);

            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(item.getPrice())));

            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) llNum.getLayoutParams();
            rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);//addRule参数对应RelativeLayout XML布局的属性
            rlp.addRule(RelativeLayout.RIGHT_OF, R.id.iv_pic);
            llNum.setLayoutParams(rlp);
        }

        if (item.getShoppingCartNum() > 0) {
            flNumReduce.setVisibility(View.VISIBLE);
            tvSelectNum.setVisibility(View.VISIBLE);

            tvSelectNum.setText(String.valueOf(item.getShoppingCartNum()));
        } else {
            flNumReduce.setVisibility(View.GONE);
            tvSelectNum.setVisibility(View.GONE);
        }
        //判断是否是平台药 若是 需要展示医保
        tags.clear();
        if ("1".equals(item.getIsPlatformDrug())) {//是平台药
            tags.add(new TagTextBean("医保", R.color.color_ee8734));
            tvTitle.setContentAndTag(EmptyUtils.strEmpty(item.getGoodsName()), tags);
        } else {
            tvTitle.setContentAndTag(EmptyUtils.strEmpty(item.getGoodsName()), tags);
        }
        //是否展示商品的月售数据
        if ("1".equals(item.getIsShowGoodsSold())) {
            tvNum.setVisibility(View.VISIBLE);
            tvNum.setText("月售" + item.getMonthlySales());
        } else {
            tvNum.setVisibility(View.GONE);
        }
        if ("1".equals(item.getIsShowMargin()) && item.getInventoryQuantity() <= 5) {
            tvOnlyhasNum.setVisibility(View.VISIBLE);
            tvOnlyhasNum.setText("仅剩" + item.getInventoryQuantity() + "件");
        } else {
            tvOnlyhasNum.setVisibility(View.GONE);
        }

        tvSelectNum.setText(String.valueOf(item.getShoppingCartNum()));

        //判断1.用户是否是企业基金用户 2.是否支持企业基金支付
        if ("1".equals(InsuredLoginUtils.getIsEnterpriseFundPay())&&"1".equals(item.getIsEnterpriseFundPay())) {
            stvEnterpriseTag.setVisibility(View.VISIBLE);
        } else {
            stvEnterpriseTag.setVisibility(View.GONE);
        }
    }
}
