package com.jrdz.zhyb_android.ui.zhyf_user.adapter;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.DecimalFormatUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.swipeMenuLayout.SwipeMenuLayout;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.hjq.toast.ToastUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TagTextBean;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarModel;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.widget.TagTextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-27
 * 描    述：
 * ================================================
 */
public class ShoppingCarAdapter extends BaseQuickAdapter<ShopCarModel.DataBean, BaseViewHolder> {
    private final IOperation mIOperation;
    List<TagTextBean> tags = new ArrayList<>();

    private int currentPos = -1;//点击哪个商家的某个商品 默认-1 还没点击
    private ImageView currentIvHeadProductSelect;
    private LinearLayout currentRlProduct;//点击哪个商家的某个商品的父布局

    public ShoppingCarAdapter(IOperation iOperation) {
        super(R.layout.layout_shoppingcar_item, null);
        this.mIOperation = iOperation;
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder = super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.fl_head_product_select,R.id.ll_shop_head_info);
        return baseViewHolder;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ShopCarModel.DataBean item) {
        ImageView ivHeadProductSelect = baseViewHolder.getView(R.id.iv_head_product_select);
        LinearLayout rlProduct = baseViewHolder.getView(R.id.rl_product);
        TextView tvShopNam= baseViewHolder.getView(R.id.tv_shop_nam);
        //判断当前商家是否被选中
        if (item.isChoose()) {
            ivHeadProductSelect.setImageResource(R.drawable.ic_product_select_pre);
        } else {
            ivHeadProductSelect.setImageResource(R.drawable.ic_product_select_nor);
        }

        tvShopNam.setText(EmptyUtils.strEmpty(item.getStoreName()));

        rlProduct.removeAllViews();
        List<GoodsModel.DataBean> shopProductBeans = item.getShoppingCartGoods();
        for (int i = 0, size = shopProductBeans.size(); i < size; i++) {
            initView(ivHeadProductSelect, rlProduct, baseViewHolder.getAbsoluteAdapterPosition(), size, i, item, shopProductBeans.get(i));
        }
    }

    //externalPos 外部整个recyclerview的pos    pos：内部商品的pos
    private void initView(ImageView ivHeadProductSelect, LinearLayout rlProduct, int externalPos, int size, int pos,
                          ShopCarModel.DataBean shoppingCarModel, GoodsModel.DataBean shopProductBean) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_shoppingcar_product_item, rlProduct, false);
        RelativeLayout rlProductItem= view.findViewById(R.id.rl_product_item);
        FrameLayout flProductSelect = view.findViewById(R.id.fl_product_select);
        ImageView ivProductSelect = view.findViewById(R.id.iv_product_select);

        ImageView ivPic = view.findViewById(R.id.iv_pic);
        ShapeTextView stvOtc = view.findViewById(R.id.stv_otc);
        ShapeTextView stvEnterpriseTag= view.findViewById(R.id.stv_enterprise_tag);
        TagTextView tvTitle=view.findViewById(R.id.tv_title);
        ShapeTextView stvDiscount = view.findViewById(R.id.stv_discount);
        LinearLayout llNum=view.findViewById(R.id.ll_num);
        TextView tvNum=view.findViewById(R.id.tv_num);
        TextView tvOnlyhasNum=view.findViewById(R.id.tv_onlyhas_num);
        TextView tvEstimatePrice = view.findViewById(R.id.tv_estimate_price);
        ShapeTextView stv_02 = view.findViewById(R.id.stv_02);
        TextView tvRealPrice = view.findViewById(R.id.tv_real_price);
        //控制商品数量view
        FrameLayout flNumReduce = view.findViewById(R.id.fl_num_reduce);
        TextView tvSelectNum = view.findViewById(R.id.tv_select_num);
        FrameLayout flNumAdd = view.findViewById(R.id.fl_num_add);
        //商品分割线
        View line = view.findViewById(R.id.line);

        SwipeMenuLayout swipeDelete = view.findViewById(R.id.swipe_delete);
        ShapeLinearLayout llCollect = view.findViewById(R.id.ll_collect);
        ImageView ivCollect= view.findViewById(R.id.iv_collect);
        TextView tvCollect= view.findViewById(R.id.tv_collect);
        LinearLayout llDelete = view.findViewById(R.id.ll_delete);

        //设置数据
        GlideUtils.loadImg(shopProductBean.getBannerAccessoryUrl1(), ivPic, R.drawable.ic_placeholder_bg,
                new RoundedCornersTransformation(mContext.getResources().getDimensionPixelSize(R.dimen.dp_16), 0));
        //判断是否是处方药
        if ("1".equals(shopProductBean.getItemType())){
            stvOtc.setText("OTC");
            stvOtc.setVisibility(View.VISIBLE);
        }else if ("2".equals(shopProductBean.getItemType())){
            stvOtc.setText("处方药");
            stvOtc.setVisibility(View.VISIBLE);
        }else {
            stvOtc.setVisibility(View.GONE);
        }
        //判断是否有折扣
        double promotionDiscountVlaue = new BigDecimal(shopProductBean.getPromotionDiscount()).doubleValue();
        if ("1".equals(shopProductBean.getIsPromotion())&&promotionDiscountVlaue<10){
            stv_02.setVisibility(View.VISIBLE);
            tvRealPrice.setVisibility(View.VISIBLE);
            stvDiscount.setVisibility(View.VISIBLE);

            stvDiscount.setText(DecimalFormatUtils.noZero(shopProductBean.getPromotionDiscount())+"折优惠");
            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(shopProductBean.getPreferentialPrice())));
            tvRealPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            tvRealPrice.getPaint().setAntiAlias(true);
            tvRealPrice.setText("¥"+DecimalFormatUtils.noZero(shopProductBean.getPrice()));

            RelativeLayout.LayoutParams rlp= (RelativeLayout.LayoutParams) llNum.getLayoutParams();
            rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);//addRule参数对应RelativeLayout XML布局的属性
            rlp.addRule(RelativeLayout.RIGHT_OF,0);
            llNum.setLayoutParams(rlp);
        }else {
            stv_02.setVisibility(View.GONE);
            tvRealPrice.setVisibility(View.GONE);
            stvDiscount.setVisibility(View.GONE);

            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(shopProductBean.getPrice())));

            RelativeLayout.LayoutParams rlp= (RelativeLayout.LayoutParams) llNum.getLayoutParams();
            rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,0);//addRule参数对应RelativeLayout XML布局的属性
            rlp.addRule(RelativeLayout.RIGHT_OF,R.id.iv_pic);
            llNum.setLayoutParams(rlp);
        }
        //判断是否是平台药 若是 需要展示医保
        tags.clear();
        if ("1".equals(shopProductBean.getIsPlatformDrug())){//是平台药
            tags.add(new TagTextBean("医保",R.color.color_ee8734));
            tvTitle.setContentAndTag(EmptyUtils.strEmpty(shopProductBean.getGoodsName()), tags);
        }else {
            tvTitle.setContentAndTag(EmptyUtils.strEmpty(shopProductBean.getGoodsName()), tags);
        }

        //是否展示商品的月售数据
        if ("1".equals(shopProductBean.getIsShowGoodsSold())){
            tvNum.setVisibility(View.VISIBLE);
            tvNum.setText("月售"+shopProductBean.getMonthlySales());
        }else {
            tvNum.setVisibility(View.GONE);
        }

        if ("1".equals(shopProductBean.getIsShowMargin())&&shopProductBean.getInventoryQuantity()<=5){
            tvOnlyhasNum.setVisibility(View.VISIBLE);
            tvOnlyhasNum.setText("仅剩"+shopProductBean.getInventoryQuantity()+"件");
        }else {
            tvOnlyhasNum.setVisibility(View.GONE);
        }

        tvSelectNum.setText(String.valueOf(shopProductBean.getShoppingCartNum()));
        //最后一条的分割线需要隐藏
        if (pos == size - 1) {
            line.setVisibility(View.GONE);
        } else {
            line.setVisibility(View.VISIBLE);
        }

        //判断当前商品是否被选中
        if (shopProductBean.isChoose()) {
            ivProductSelect.setImageResource(R.drawable.ic_product_select_pre);
        } else {
            ivProductSelect.setImageResource(R.drawable.ic_product_select_nor);
        }
        //判断是否收藏
        if ("1".equals(shopProductBean.getIsCollection())){//已收藏
            ivCollect.setImageResource(R.drawable.ic_shopcar_collect_nor);
            tvCollect.setText("取消\n收藏");
            tvCollect.setTextColor(mContext.getResources().getColor(R.color.white));
            llCollect.getShapeDrawableBuilder().setSolidColor(mContext.getResources().getColor(R.color.color_4870e0)).intoBackground();
        }else {//未收藏
            ivCollect.setImageResource(R.drawable.ic_shopcar_collect_pre);
            tvCollect.setText("收藏");
            tvCollect.setTextColor(mContext.getResources().getColor(R.color.color_4870e0));
            llCollect.getShapeDrawableBuilder().setSolidColor(mContext.getResources().getColor(R.color.white)).intoBackground();
        }

        //判断1.用户是否是企业基金用户 2.是否支持企业基金支付
        if ("1".equals(InsuredLoginUtils.getIsEnterpriseFundPay())&&"1".equals(shopProductBean.getIsEnterpriseFundPay())){
            stvEnterpriseTag.setVisibility(View.VISIBLE);
        }else {
            stvEnterpriseTag.setVisibility(View.GONE);
        }

        //商品点击事件
        rlProductItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                if (mIOperation != null) {
                    mIOperation.onProductItem(shopProductBean.getGoodsNo(),shopProductBean.getFixmedins_code(),shopProductBean.getGoodsName());
                }
            }
        });
        //是否选中
        flProductSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                if ("2".equals(shopProductBean.getIsOnSale())) {
                    ToastUtils.show("该商品已下架");
                    return;
                }

                //点击的时候 判断该商品是否被选中
                if (shopProductBean.isChoose()) {
                    ivProductSelect.setImageResource(R.drawable.ic_product_select_nor);
                    shopProductBean.setChoose(false);
                } else {
                    ivProductSelect.setImageResource(R.drawable.ic_product_select_pre);
                    shopProductBean.setChoose(true);
                }
                //点击之后 还需要判断 选中该商品之后 该商家下的所有商品是否也被选中了（已下架的商品排除）
                if (productIsAllChecked(shoppingCarModel.getShoppingCartGoods())) {
                    ivHeadProductSelect.setImageResource(R.drawable.ic_product_select_pre);
                    shoppingCarModel.setChoose(true);
                } else {
                    ivHeadProductSelect.setImageResource(R.drawable.ic_product_select_nor);
                    shoppingCarModel.setChoose(false);
                }
                canclePreItem(ivHeadProductSelect, rlProduct, externalPos);
                // 2022-10-28 外部计算价格
                if (mIOperation != null) {
                    mIOperation.calPrice();
                }
            }
        });
        //数量减
        flNumReduce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                int num = shopProductBean.getShoppingCartNum();
                if (num > 1) {
                    if (mIOperation != null) {
                        mIOperation.numReduce(shopProductBean,tvSelectNum);
                    }
                } else {
                    ToastUtils.show("数量最少为1");
                    return;
                }
            }
        });
        //数量加
        flNumAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                int num = shopProductBean.getShoppingCartNum();
                if (num+1<=shopProductBean.getInventoryQuantity()){
                    if (mIOperation != null) {
                        mIOperation.numAdd(shopProductBean,tvSelectNum);
                    }
                }else {
                    ToastUtils.show("该商品库存不足,请联系商家");
                    return;
                }
            }
        });
        llCollect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                if (mIOperation != null) {
                    mIOperation.onCollect(shopProductBean,llCollect,ivCollect,tvCollect,swipeDelete);
                }
            }
        });
        //删除商品
        llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                if (mIOperation != null) {
                    mIOperation.onDelete(currentPos,externalPos,shoppingCarModel,shopProductBean,rlProduct,view,swipeDelete);
                }
            }
        });

        rlProduct.addView(view);
    }

    //如果当前选中的item 跟上一个不一样 取消上一个选中的item
    public void canclePreItem(ImageView ivHeadProductSelect, LinearLayout rlProduct, int choosePos) {
        if (currentPos != -1) {
            if (currentPos != choosePos) {
                ShopCarModel.DataBean shoppingCarModel = getData().get(currentPos);
                shoppingCarModel.setChoose(false);

                for (GoodsModel.DataBean shopProductBean : shoppingCarModel.getShoppingCartGoods()) {
                    shopProductBean.setChoose(false);
                }
                updatePreItemView(currentIvHeadProductSelect, currentRlProduct, shoppingCarModel.isChoose());
            }
        }

        currentIvHeadProductSelect = ivHeadProductSelect;
        currentRlProduct = rlProduct;
        currentPos = choosePos;
    }

    //商家选中或者取消选中后 若前面有选中的item 那么该item顶部商家标题的选中需要更新 以及商品列表也需要更新
    public void updatePreItemView(ImageView ivHeadProductSelect, LinearLayout rlProduct, boolean choose) {
        if (ivHeadProductSelect != null) {
            //判断当前商家是否被选中
            if (choose) {
                ivHeadProductSelect.setImageResource(R.drawable.ic_product_select_pre);
            } else {
                ivHeadProductSelect.setImageResource(R.drawable.ic_product_select_nor);
            }
        }

        if (rlProduct != null) {
            //判断当前商家是否被选中
            for (int i = 0, size = rlProduct.getChildCount(); i < size; i++) {
                View view = rlProduct.getChildAt(i);

                ImageView ivProductSelect = view.findViewById(R.id.iv_product_select);

                //判断当前商品是否被选中
                if (choose) {
                    ivProductSelect.setImageResource(R.drawable.ic_product_select_pre);
                } else {
                    ivProductSelect.setImageResource(R.drawable.ic_product_select_nor);
                }
            }
        }
    }

    //商家选中或者取消选中后 商品列表的选中需要更新  this.getRecyclerView()必须是绑定过recyclerview的 比如调用mAdapter.bindToRecyclerView(mRecyclerView);或者mAdapter.setOnLoadMoreListener(BaseRecyclerViewFragment.this, mRecyclerView);
    public void updateProductItemView(int position) {
        if (this.getRecyclerView() == null) {
            return;
        }

        BaseViewHolder viewHolder = (BaseViewHolder) this.getRecyclerView().findViewHolderForAdapterPosition(position);
        if (viewHolder != null) {
            List<GoodsModel.DataBean> shopProductBeans = getItem(position).getShoppingCartGoods();
            LinearLayout rlProduct = viewHolder.getView(R.id.rl_product);

            for (int i = 0, size = rlProduct.getChildCount(); i < size; i++) {
                GoodsModel.DataBean shopProductBean = shopProductBeans.get(i);

                View view = rlProduct.getChildAt(i);
                ImageView ivProductSelect = view.findViewById(R.id.iv_product_select);

                //判断当前商品是否被选中
                if (shopProductBean.isChoose()) {
                    ivProductSelect.setImageResource(R.drawable.ic_product_select_pre);
                } else {
                    ivProductSelect.setImageResource(R.drawable.ic_product_select_nor);
                }
            }
        }
    }

    //检查商家下面的 产品是否都已选中
    public boolean productIsAllChecked(List<GoodsModel.DataBean> shopProductBeans) {
        for (GoodsModel.DataBean shopProductBean : shopProductBeans) {
            if (!shopProductBean.isChoose() && "1".equals(shopProductBean.getIsOnSale())) {
                return false;
            }
        }

        return true;
    }

    //判断是否要移除商家
    public boolean isCanDeleteBusiness(int position) {
        return getItem(position).getShoppingCartGoods().size() == 1;
    }

    //重置记录的数据
    public void resetRecordData() {
        currentPos = -1;
        currentIvHeadProductSelect = null;
        currentRlProduct = null;
    }

    //获取当前选中的商家药品
    public int getCurrentPos() {
        return currentPos;
    }

    public ImageView getCurrentIvHeadProductSelect() {
        return currentIvHeadProductSelect;
    }

    public void setCurrentIvHeadProductSelect(ImageView currentIvHeadProductSelect) {
        this.currentIvHeadProductSelect = currentIvHeadProductSelect;
    }

    public LinearLayout getCurrentRlProduct() {
        return currentRlProduct;
    }

    public void setCurrentRlProduct(LinearLayout currentRlProduct) {
        this.currentRlProduct = currentRlProduct;
    }

    public interface IOperation {
        void onProductItem(String goodsNo,String fixmedins_code,String goodsName);//进入商品详情

        void calPrice();//计算价格

        void setPrice();//设置价格

        void numAdd(GoodsModel.DataBean dataBean,TextView tvSelectNum);//数量加

        void numReduce(GoodsModel.DataBean dataBean,TextView tvSelectNum);//数量减

        void onCollect(GoodsModel.DataBean shopProductBean,ShapeLinearLayout llCollect,ImageView ivCollect, TextView tvCollect, SwipeMenuLayout swipeDelete);//删除购物车的商品

        void onDelete(int currentPos, int externalPos, ShopCarModel.DataBean shoppingCarModel, GoodsModel.DataBean shopProductBean,
                      LinearLayout rlProduct, View view, SwipeMenuLayout swipeDelete);//删除购物车的商品
    }
}
