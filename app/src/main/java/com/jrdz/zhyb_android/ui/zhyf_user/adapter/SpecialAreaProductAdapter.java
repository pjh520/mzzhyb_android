package com.jrdz.zhyb_android.ui.zhyf_user.adapter;

import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.DecimalFormatUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.TagTextBean;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.widget.TagTextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-27
 * 描    述：
 * ================================================
 */
public class SpecialAreaProductAdapter extends BaseQuickAdapter<GoodsModel.DataBean, BaseViewHolder> {
    List<TagTextBean> tags = new ArrayList<>();

    public SpecialAreaProductAdapter() {
        super(R.layout.layout_specialarea_product_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, GoodsModel.DataBean item) {
        ImageView ivPic=baseViewHolder.getView(R.id.iv_pic);
        ShapeTextView stvOtc=baseViewHolder.getView(R.id.stv_otc);
        ShapeTextView stvEnterpriseTag= baseViewHolder.getView(R.id.stv_enterprise_tag);
        TagTextView tvTitle=baseViewHolder.getView(R.id.tv_title);
        TextView tvEstimatePrice=baseViewHolder.getView(R.id.tv_estimate_price);
        TextView tvRealPrice=baseViewHolder.getView(R.id.tv_real_price);

        GlideUtils.loadImg(item.getBannerAccessoryUrl1(),ivPic,R.drawable.ic_placeholder_top_corner_bg,
                new RoundedCornersTransformation(mContext.getResources().getDimensionPixelSize(R.dimen.dp_16),0, RoundedCornersTransformation.CornerType.TOP));

        //判断是否是处方药
        if ("1".equals(item.getItemType())){
            stvOtc.setText("OTC");
            stvOtc.setVisibility(View.VISIBLE);
        }else if ("2".equals(item.getItemType())){
            stvOtc.setText("处方药");
            stvOtc.setVisibility(View.VISIBLE);
        }else {
            stvOtc.setVisibility(View.GONE);
        }
        //判断是否有折扣
        double promotionDiscountVlaue = new BigDecimal(item.getPromotionDiscount()).doubleValue();
        if ("1".equals(item.getIsPromotion())&&promotionDiscountVlaue<10){
            tvRealPrice.setVisibility(View.VISIBLE);

            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(item.getPreferentialPrice())));
            tvRealPrice.setText("原价:"+item.getPrice());
        }else {
            tvRealPrice.setVisibility(View.GONE);
            tvEstimatePrice.setText(EmptyUtils.strEmpty(DecimalFormatUtils.noZero(item.getPrice())));
        }
        //判断是否是平台药 若是 需要展示医保
        tags.clear();
        if ("1".equals(item.getIsPlatformDrug())){//是平台药
            tags.add(new TagTextBean("医保",R.color.color_ee8734));
            tvTitle.setContentAndTag(EmptyUtils.strEmpty(item.getGoodsName()), tags);
        }else {
            tvTitle.setContentAndTag(EmptyUtils.strEmpty(item.getGoodsName()), tags);
        }

        //判断1.用户是否是企业基金用户 2.是否支持企业基金支付
        if ("1".equals(InsuredLoginUtils.getIsEnterpriseFundPay())&&"1".equals(item.getIsEnterpriseFundPay())){
            stvEnterpriseTag.setVisibility(View.VISIBLE);
        }else {
            stvEnterpriseTag.setVisibility(View.GONE);
        }

    }
}
