package com.jrdz.zhyb_android.ui.zhyf_user.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-01-08
 * 描    述：电子处方-图文
 * ================================================
 */
public class ElectPrescFragment_image extends BaseFragment {
    private ImageView mIvImg;
    private SubsamplingScaleImageView mIvLongImg;

    private String picPath;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_electpresc_image;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mIvImg = view.findViewById(R.id.iv_img);
        mIvLongImg = view.findViewById(R.id.iv_long_img);
    }

    @Override
    public void initData() {
        picPath = getArguments().getString("picPath");
        super.initData();
        if (!EmptyUtils.isEmpty(picPath)) {
            GlideUtils.loadLongImg(getContext(), picPath, mIvImg, mIvLongImg);
        } else {
            showTipDialog("数据有误,请重新进入页面");
        }
    }

    public static ElectPrescFragment_image newIntance(String picPath) {
        ElectPrescFragment_image electPrescFragment_image = new ElectPrescFragment_image();
        Bundle bundle = new Bundle();
        bundle.putString("picPath", picPath);
        electPrescFragment_image.setArguments(bundle);
        return electPrescFragment_image;
    }
}
