package com.jrdz.zhyb_android.ui.zhyf_user.fragment;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danikula.videocache.HttpProxyCacheServer;
import com.frame.compiler.utils.RxTool;
import com.frame.video_library.cache.ProxyVideoCacheManager;
import com.frame.video_library.controller.StandardVideoController2;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;

import xyz.doikki.videoplayer.player.VideoView;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-01-08
 * 描    述：电子处方-视频
 * ================================================
 */
public class ElectPrescFragment_video extends BaseFragment {
    private VideoView mVideoView;

    private String videoPath;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_videoplay_single;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mVideoView = view.findViewById(R.id.video_view);
    }

    @Override
    public void initData() {
        videoPath = getArguments().getString("videoPath");
        super.initData();
        HttpProxyCacheServer cacheServer = ProxyVideoCacheManager.getProxy(RxTool.getContext());
        String proxyUrl = cacheServer.getProxyUrl(videoPath);

        mVideoView.setUrl(proxyUrl);
        StandardVideoController2 controller = new StandardVideoController2(getContext());
        controller.addDefaultControlComponent("视频", false);
        mVideoView.setVideoController(controller);

        if (controller.getPrepareView() != null) {
            ImageView thumb = controller.getPrepareView().findViewById(R.id.thumb);//封面图
            Glide.with(this)
                    .setDefaultRequestOptions(
                            new RequestOptions().frame(1000000)
                                    .centerCrop()
                                    .error(com.frame.compiler.R.drawable.ic_placeholder_bg)//可以忽略
                                    .placeholder(com.frame.compiler.R.drawable.ic_placeholder_bg)//可以忽略
                    )
                    .load(videoPath)
                    .into(thumb);
        }

//        mVideoView.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mVideoView != null) {
            mVideoView.pause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mVideoView != null) {
            mVideoView.resume();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mVideoView != null) {
            mVideoView.release();
        }
    }

    public static ElectPrescFragment_video newIntance(String videoPath) {
        ElectPrescFragment_video electPrescFragment_image = new ElectPrescFragment_video();
        Bundle bundle = new Bundle();
        bundle.putString("videoPath", videoPath);
        electPrescFragment_image.setArguments(bundle);
        return electPrescFragment_image;
    }
}
