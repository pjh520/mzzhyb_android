package com.jrdz.zhyb_android.ui.zhyf_user.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.base.BaseFragment;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.LogisticsActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.ElectPrescActivity2_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.ElectPrescActivity_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.InquiryInfoActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.OrderDetailActivity_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.RefundProgressActivity_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.ScanPayOrderDetailActivity_user;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.OrderListAdapter_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OrderDetailModel_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.OrderListModel_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarRefreshModel;
import com.jrdz.zhyb_android.utils.WXUtils;

import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-09
 * 描    述：我的订单列表 fragment--用户端
 * ================================================
 */
public class OrderListFragment_user extends BaseRecyclerViewFragment {
    private String status;
    private CustomerDialogUtils customerDialogUtils;

    private boolean isOpenPreing = false;//当前操作是否是正在开处方

    //支付成功监听
    private ObserverWrapper<String> mPayStatusObserve = new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            if ("1".equals(status)) {
                showWaitDialog();
                onRefresh(mRefreshLayout);
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public void enableDataInitialized() {
        isDataInitialized = false;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_order_list;
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    protected void initAdapter() {
        mAdapter = new OrderListAdapter_user();
    }

    @Override
    public void initData() {
        status = getArguments().getString("status", "");
        super.initData();
        MsgBus.sendPayStatusRefresh_user().observe(this, mPayStatusObserve);

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    protected void getData() {
        super.getData();

        OrderListModel_user.sendOrderListRequest_user(TAG + status, status, String.valueOf(mPageNum), "10", "", new CustomerJsonCallBack<OrderListModel_user>() {
            @Override
            public void onRequestError(OrderListModel_user returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(OrderListModel_user returnData) {
                hideRefreshView();
                List<OrderListModel_user.DataBean> datas = returnData.getData();
                if (mAdapter != null && datas != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(datas);

                        if (datas == null || datas.isEmpty()) {
                            mAdapter.isUseEmpty(true);
                        }
                    } else {
                        mAdapter.addData(datas);
                        mAdapter.loadMoreComplete();
                    }

                    if (datas.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        OrderListModel_user.DataBean itemData = ((OrderListAdapter_user) adapter).getItem(position);
        //跳转订单详情页面
        if ("2".equals(itemData.getOrderType())) {
            ScanPayOrderDetailActivity_user.newIntance(getContext(), itemData.getOrderNo());
        } else {
            Intent intent = new Intent(getContext(), OrderDetailActivity_user.class);
            intent.putExtra("orderNo", itemData.getOrderNo());
            startActivityForResult(intent, new OnActivityCallback() {
                @Override
                public void onActivityResult(int resultCode, @Nullable Intent data) {
                    if (resultCode == -1) {
                        String OperationType = data.getStringExtra("OperationType");
                        String orderNo = data.getStringExtra("orderNo");
                        switch (OperationType) {
                            case "cancle"://取消订单
                            case "delete"://删除订单
                            case "confirm_receipt"://确认收货
                                deleteItem(orderNo);
                                break;
                            case "openPre"://开处方
                                showWaitDialog();
                                onRefresh(mRefreshLayout);
                                break;
                        }
                    }
                }
            });
        }
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        OrderListModel_user.DataBean itemData = ((OrderListAdapter_user) adapter).getItem(position);

        switch (((TextView) view).getText().toString()) {
            case "取消订单":
                onCancle(itemData.getOrderNo(), position);
                break;
            case "立即支付":
                onPay(itemData);
                break;
            case "联系商家":
                onTakePhone(itemData.getTelephone());
                break;
            case "查看物流":
                LogisticsActivity.newIntance(getContext(), itemData.getOrderNo());
                break;
            case "确认收货":
                confirmReceipt(itemData.getOrderNo(), position);
                break;
            case "加入购物车":
                onAddShopcar(itemData.getOrderNo(), 1);
                break;
            case "删除订单":
                onDelete(itemData.getOrderNo(), position);
                break;
            case "退款进度":
                RefundProgressActivity_user.newIntance(getContext(), itemData.getOrderNo(), itemData.getOnlineAmount());
                break;
            case "开处方":
                openPrescription(itemData);
                break;
            case "查看处方":
                if ("1".equals(itemData.getIsThirdPartyPres())) {
                    ElectPrescActivity2_user.newIntance(getContext(), itemData.getOrderNo());
                } else {
                    ElectPrescActivity_user.newIntance(getContext(), itemData.getOrderNo());
                }
                break;
        }
    }

    //取消订单
    private void onCancle(String OrderNo, int position) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(getContext(), "提示", "确定取消订单吗？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                // 2022-10-10 请求接口取消该订单
                showWaitDialog();
                BaseModel.sendCancleOrderRequest_user(TAG, OrderNo, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        showShortToast("取消成功");
                        //当订单取消成功之后 前端需要移除该订单的展示
                        mAdapter.remove(position);
                    }
                });
            }
        });
    }

    //开处方
    //1.app端提交问诊信息之后，开启120s定时器，提示用户正在分配医师，请稍等。然后app开启接口（这个接口，可以返回一个字段 里面有几个状态 1.正在分配医师 2.医师已分配完成，正在开处方 3.医师已开完处方 4.分配医师失败，跳转第三方开处方）轮询，根据返回的不同状态 展示不同的提示文字。
    //状态：1 就是用户提交问诊信息之后
    //     2 给筛选中的医师分配之后，医师进入开处方页面 （*进入这个状态时，后台应该把定时器关闭掉，防止医师开处方太久 导致超过120s，app端这时候，拿到这个状态，也把读秒定时器停止）
    //     3 医师把开好的处方提交
    //     4 当一直是状态1的时候，时间也超过120s
    //2.如果出现用户提交完问诊信息，然后又没在当前等待开处方页面，之后又通过重新进入订单详情页 《1》当时间还没超时，那么开处方按钮文字改为“查看开处方进度”，点击按钮，进入提交问诊信息页面 《2》如果医师已开完处方，那么按钮直接改为“去支付”《3》如果时间已超时（分配医师失败，跳转第三方开处方），那么按钮直接改为“开处方”，点击之后，跳转第三方开处方
    private void openPrescription(OrderListModel_user.DataBean itemData) {
        //[{"erpCode":"drp_00016","count":1}]
        if (itemData == null) return;
        // 2024-03-28 需要等后台确认 具体是使用哪个字段来判断 开处方的状态
        if (!"4".equals(itemData.getIsPrescription())) {//首次开处方 分配医师方案 跳转问诊信息填写页面
            JSONArray jsonArray = new JSONArray();
            List<OrderListModel_user.DataBean.OrderGoodsBean> orderGoods = itemData.getOrderGoods();
            if (orderGoods != null) {
                for (int i = 0, size = orderGoods.size(); i < size; i++) {
                    OrderListModel_user.DataBean.OrderGoodsBean orderGood = orderGoods.get(i);
                    JSONObject jsonObjectChild = new JSONObject();
                    jsonObjectChild.put("erpCode", EmptyUtils.strEmpty(orderGood.getGoodsNo()));//商品no
                    jsonObjectChild.put("count", orderGood.getGoodsNum());//商品数量

                    jsonArray.add(jsonObjectChild);
                }
            }
            Intent intent = new Intent(getContext(), InquiryInfoActivity.class);
            intent.putExtra("orderNo", itemData.getOrderNo());
            intent.putExtra("fixmedins_code", itemData.getFixmedins_code());
            intent.putExtra("associatedDiseases", EmptyUtils.strEmpty(getAssociatedDiseases(itemData.getOrderGoods())));
            intent.putExtra("drugListJson", jsonArray.toJSONString());
            startActivityForResult(intent, new BaseFragment.OnActivityCallback() {
                @Override
                public void onActivityResult(int resultCode, @Nullable Intent data) {
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                }
            });
        } else {//分配医师方案开处方失败 跳转第三方开处方页面
            if (Constants.Configure.IS_POS) {
                showShortToast("pos机不支持跳转微信小程序开处方");
                return;
            }
            //[{"erpCode":"drp_00016","count":1}]
            List<OrderListModel_user.DataBean.OrderGoodsBean> orderGoods = itemData.getOrderGoods();
            if (orderGoods != null) {
                JSONArray jsonArray = new JSONArray();
                for (int i = 0, size = orderGoods.size(); i < size; i++) {
                    OrderListModel_user.DataBean.OrderGoodsBean orderGood = orderGoods.get(i);
                    JSONObject jsonObjectChild = new JSONObject();
                    jsonObjectChild.put("erpCode", EmptyUtils.strEmpty(orderGood.getGoodsNo()));//商品no
                    jsonObjectChild.put("count", orderGood.getGoodsNum());//商品数量

                    jsonArray.add(jsonObjectChild);
                }
                isOpenPreing = true;
                WXUtils.goSmallRoutine(getContext(), Constants.Configure.WEZ_APPID,
                        Constants.Configure.WEZ_PATH + "&orderId=" + itemData.getOrderNo() + "&drugList=" + jsonArray.toJSONString());
            }
        }
    }

    private String getAssociatedDiseases(List<OrderListModel_user.DataBean.OrderGoodsBean> goods) {
        if (goods == null) return null;
        String associatedDiseases = "";
        for (OrderListModel_user.DataBean.OrderGoodsBean orderGood : goods) {
            String diseasesData = orderGood.getAssociatedDiseases();
            if (!EmptyUtils.isEmpty(diseasesData)) {
                associatedDiseases += diseasesData + "∞#";
            }
        }
        if (!EmptyUtils.isEmpty(associatedDiseases)) {
            associatedDiseases = associatedDiseases.substring(0, associatedDiseases.length() - 2);
        }
        return associatedDiseases;
    }

    //立即支付
    private void onPay(OrderListModel_user.DataBean itemData) {
        //跳转订单详情页面
        //跳转订单详情页面
        Intent intent = new Intent(getContext(), OrderDetailActivity_user.class);
        intent.putExtra("orderNo", itemData.getOrderNo());
        intent.putExtra("type", "pay");
        intent.putExtra("from", 0);
        startActivityForResult(intent, new OnActivityCallback() {
            @Override
            public void onActivityResult(int resultCode, @Nullable Intent data) {
                if (resultCode == -1) {
                    String OperationType = data.getStringExtra("OperationType");
                    String orderNo = data.getStringExtra("orderNo");
                    switch (OperationType) {
                        case "cancle"://取消订单
                        case "delete"://删除订单
                        case "confirm_receipt"://确认收货
                            deleteItem(orderNo);
                            break;
                    }
                }
            }
        });
//        OrderDetailActivity_user.newIntance(getContext(), itemData.getOrderNo(), "pay", 0);
    }

    //联系商家
    private void onTakePhone(String phone) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(getContext(), "提示", "是否拨打商家电话" + phone + "?", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                RxTool.takePhone(getContext(), phone);
            }
        });
    }

    //确认收货
    private void confirmReceipt(String OrderNo, int position) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(getContext(), "确认收货吗？", "注意:请确保已取货或已收到货物,确认收货后订单完成。", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                //2022-10-10 请求接口确认收货
                showWaitDialog();
                BaseModel.sendConfirmReceiptOrderRequest_user(TAG, OrderNo, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        //当订单取消成功之后 前端需要移除该订单的展示
                        mAdapter.remove(position);
                    }
                });
            }
        });
    }

    //加入购物车
    private void onAddShopcar(String orderNo, int shoppingCartNum) {
        showWaitDialog();
        BaseModel.sendAddOrderShopCarRequest(TAG, orderNo, shoppingCartNum, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("加入购物车成功");
                MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "1"));
            }
        });
    }

    //删除订单
    private void onDelete(String OrderNo, int position) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(getContext(), "确定删除此订单吗？", "注意:订单删除后不可恢复,订单评价也随之会自动删除,谨慎操作", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                //2022-10-10 请求接口删除该订单
                showWaitDialog();
                BaseModel.sendDelOrderRequest(TAG, OrderNo, new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        //当订单删除成功之后 前端需要移除该订单的展示
                        mAdapter.remove(position);
                    }
                });
            }
        });
    }

    //删除指定的列表item
    private void deleteItem(String orderNo) {
        for (int i = 0, size = mAdapter.getData().size(); i < size; i++) {
            OrderListModel_user.DataBean data = ((OrderListAdapter_user) mAdapter).getData().get(i);
            if (orderNo.equals(data.getOrderNo())) {
                mAdapter.remove(i);
                return;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if ("1".equals(status) && isOpenPreing) {
            isOpenPreing = false;
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public static OrderListFragment_user newIntance(String status) {
        OrderListFragment_user orderFragment = new OrderListFragment_user();
        Bundle bundle = new Bundle();
        bundle.putString("status", status);
        orderFragment.setArguments(bundle);
        return orderFragment;
    }
}
