package com.jrdz.zhyb_android.ui.zhyf_user.fragment;

import android.content.Context;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.ShopDetailActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.ShopCollectionAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCollectionModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-01
 * 描    述：店铺收藏
 * ================================================
 */
public class ShopCollectionFragment extends BaseRecyclerViewFragment {

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public void initView(View view) {
        super.initView(view);

        mTitleBar.setVisibility(View.GONE);
    }

    @Override
    protected void initAdapter() {
        mAdapter = new ShopCollectionAdapter();
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void initData() {
        super.initData();

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();
        setSmartHasRefreshOrLoadMore();
        setLoadMore();
    }

    @Override
    protected void getData() {
        super.getData();

        ShopCollectionModel.sendShopCollectionRequest(TAG, String.valueOf(mPageNum), "20", new CustomerJsonCallBack<ShopCollectionModel>() {
            @Override
            public void onRequestError(ShopCollectionModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(ShopCollectionModel returnData) {
                hideRefreshView();
                List<ShopCollectionModel.DataBean> datas = returnData.getData();
                if (mAdapter!=null&&datas != null) {
                    if (mPageNum == 0) {
                        mAdapter.setNewData(datas);
                        if (datas.size() <= 0) {
                            mAdapter.isUseEmpty(true);
                        }
                    } else {
                        mAdapter.addData(datas);
                        mAdapter.loadMoreComplete();
                    }

                    if (datas.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        ShopCollectionModel.DataBean itemData = ((ShopCollectionAdapter) adapter).getItem(position);

        switch (view.getId()){
            case R.id.srl_item://进入店铺详情页
                ShopDetailActivity.newIntance(getContext(), itemData.getFixmedins_code());
                break;
            case R.id.ll_del://删除
                onDelete(itemData.getFixmedins_code(),position);
                break;
        }
    }

    //删除
    private void onDelete(String fixmedins_code,int pos) {
        // 2022-11-01 请求接口删除收藏
        showWaitDialog();
        BaseModel.sendDelStoreCollectionRequest(TAG, fixmedins_code, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("删除收藏成功");
                //删除成功
                mAdapter.remove(pos);

                if (mAdapter.getData().isEmpty()){
                    mAdapter.isUseEmpty(true);
                }
            }
        });
    }

    public static ShopCollectionFragment newIntance() {
        ShopCollectionFragment shopCollectionFragment = new ShopCollectionFragment();
        return shopCollectionFragment;
    }
}
