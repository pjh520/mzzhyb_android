package com.jrdz.zhyb_android.ui.zhyf_user.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.route.BaiduMapRoutePlan;
import com.baidu.mapapi.utils.route.RouteParaOption;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.RxClipboardTool;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.lbs_library.utils.CustomLocationBean;
import com.frame.lbs_library.utils.ILocationListener;
import com.frame.lbs_library.utils.LBSUtil;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.MerchantQualiActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopDetailModel;
import com.jrdz.zhyb_android.utils.location.LocationTool;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-03
 * 描    述：用户端--店铺详情--商家信息页面
 * ================================================
 */
public class ShopInfoFragment_user extends BaseFragment {
    private LinearLayout mLlAddress;
    private TextView mTvAddress;
    private LinearLayout mLlPhone;
    private TextView mTvPhone;
    private FrameLayout mFlPhone;
    private LinearLayout mLlWx;
    private TextView mTvWx;
    private TextView mTvWxCopy;
    private LinearLayout mLlBusinessHours;
    private TextView mTvBusinessHours;
    private LinearLayout mLlDistributionScope;
    private TextView mTvDistributionScope;
    private LinearLayout mLlManagement;
    private TextView mTvManagement;
    private LinearLayout mLlFeaturedServices;
    private TextView mTvFeaturedServices;
    private LinearLayout mLlDeliveryService;
    private TextView mTvDeliveryService;
    private LinearLayout mLlNotice;
    private TextView mTvNotice;
    private LinearLayout mLlMerchantQualification;
    private TextView mTvMerchantQualification;

    private ShopDetailModel.DataBean pagerData;
    private CustomerDialogUtils customerDialogUtils;
    private double lat=0,lon=0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_shop_info_user;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mLlAddress = view.findViewById(R.id.ll_address);
        mTvAddress = view.findViewById(R.id.tv_address);
        mLlPhone = view.findViewById(R.id.ll_phone);
        mTvPhone = view.findViewById(R.id.tv_phone);
        mFlPhone = view.findViewById(R.id.fl_phone);
        mLlWx = view.findViewById(R.id.ll_wx);
        mTvWx = view.findViewById(R.id.tv_wx);
        mTvWxCopy = view.findViewById(R.id.tv_wx_copy);
        mLlBusinessHours = view.findViewById(R.id.ll_business_hours);
        mTvBusinessHours = view.findViewById(R.id.tv_business_hours);
        mLlDistributionScope = view.findViewById(R.id.ll_distribution_scope);
        mTvDistributionScope = view.findViewById(R.id.tv_distribution_scope);
        mLlManagement = view.findViewById(R.id.ll_management);
        mTvManagement = view.findViewById(R.id.tv_management);
        mLlFeaturedServices = view.findViewById(R.id.ll_featured_services);
        mTvFeaturedServices = view.findViewById(R.id.tv_featured_services);
        mLlDeliveryService = view.findViewById(R.id.ll_delivery_service);
        mTvDeliveryService = view.findViewById(R.id.tv_delivery_service);
        mLlNotice = view.findViewById(R.id.ll_notice);
        mTvNotice = view.findViewById(R.id.tv_notice);
        mLlMerchantQualification = view.findViewById(R.id.ll_merchant_qualification);
        mTvMerchantQualification = view.findViewById(R.id.tv_merchant_qualification);
    }

    @Override
    public void initData() {
        pagerData=getArguments().getParcelable("pagerData");
        super.initData();

        //获取自己当前的位置
        showWaitDialog();
        LocationTool.getInstance().registerLocation(this,new ILocationListener() {
                    @Override
                    public void onLocationSuccess(CustomLocationBean customLocationBean) {
                        hideWaitDialog();
                        lat=customLocationBean.getLatitude();
                        lon=customLocationBean.getLongitude();
                        Log.e("Location", "location.getLatitude()======="+customLocationBean.getLatitude()+"==location.getLongitude()=="+customLocationBean.getLongitude());
                    }

                    @Override
                    public void onLocationError(String errorText) {
                        hideWaitDialog();
                        showTipDialog(errorText);
                    }
                });

        //设置页面数据
        mTvAddress.setText("药店地址："+pagerData.getStoreAdress()+pagerData.getDetailedAddress());
        mTvPhone.setText("联系电话："+pagerData.getTelephone());
        mTvWx.setText("商家微信："+pagerData.getWechat());
        mTvBusinessHours.setText("营业时间："+pagerData.getOpeningStartTime()+"~"+pagerData.getOpeningEndTime());
        mTvDistributionScope.setText("配送范围："+pagerData.getDistributionScope());
        mTvManagement.setText("经营范围："+pagerData.getBusinessScope());
        mTvFeaturedServices.setText("特色服务："+(EmptyUtils.isEmpty(pagerData.getFeaturedServices())?"":pagerData.getFeaturedServices()+",")
                +("0".equals(pagerData.getIsEnterpriseFundPay())?"":"支持企业基金支付"));
        mTvNotice.setText("公告："+pagerData.getNotice());
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mLlAddress.setOnClickListener(this);
        mFlPhone.setOnClickListener(this);
        mTvWxCopy.setOnClickListener(this);
        mLlMerchantQualification.setOnClickListener(this);
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.ll_address://导航地址
                goBaiduMap();
                break;
            case R.id.fl_phone://联系电话
                showPhone(pagerData.getTelephone());
                break;
            case R.id.tv_wx_copy://复制微信
                RxClipboardTool.copyText(getContext(), pagerData.getWechat());
                break;
            case R.id.ll_merchant_qualification://查看商家资质
                MerchantQualiActivity.newIntance(getContext(),pagerData.getBusinessLicenseAccessoryUrl(),pagerData.getDrugBLAccessoryUrl(),pagerData.getMedicalDeviceBLAccessoryUrl());
                break;
        }
    }

    //跳转百度map
    private void goBaiduMap() {
        //定义起终点坐标（天安门和百度大厦）
        LatLng startPoint = new LatLng(lat, lon);
        LatLng endPoint = new LatLng(Double.valueOf(EmptyUtils.strEmptyToText(pagerData.getLatitude(),"0")), Double.valueOf(EmptyUtils.strEmptyToText(pagerData.getLongitude(),"0")));

        //构建RouteParaOption参数以及策略
        //也可以通过startName和endName来构造
        RouteParaOption paraOption = new RouteParaOption()
                .startPoint(startPoint)
                .endPoint(endPoint)
                .busStrategyType(RouteParaOption.EBusStrategyType.bus_recommend_way);
        //调起百度地图
        try {
            BaiduMapRoutePlan.openBaiduMapDrivingRoute(paraOption,getContext());
//            BaiduMapRoutePlan.openBaiduMapTransitRoute(paraOption, getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //显示电话
    private void showPhone(String phone) {
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(getContext(), "提示", "是否拨打电话" + phone + "?", "取消", "确定",
                new CustomerDialogUtils.IDialogListener() {

                    @Override
                    public void onBtn01Click() {
                    }

                    @Override
                    public void onBtn02Click() {
                        RxTool.takePhone(getContext(), phone);
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }

        //调起结束时及时调用finish方法以释放相关资源
        BaiduMapRoutePlan.finish(getContext());
        LocationTool.getInstance().unRegisterLocation();
    }

    public static ShopInfoFragment_user newIntance(ShopDetailModel.DataBean pagerData) {
        ShopInfoFragment_user shopInfoFragment_user = new ShopInfoFragment_user();
        Bundle bundle = new Bundle();
        bundle.putParcelable("pagerData", pagerData);
        shopInfoFragment_user.setArguments(bundle);
        return shopInfoFragment_user;
    }
}
