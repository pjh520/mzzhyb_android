package com.jrdz.zhyb_android.ui.zhyf_user.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.widget.CostomLoadMoreView;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.LeftLinkAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.GoodDetailActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.ShopDetailActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.ShopProductRightLinkAdapter_user;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarRefreshModel;
import com.jrdz.zhyb_android.utils.ParcelUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-21
 * 描    述：用户端--店铺详情--全部商品页面
 * ================================================
 */
public class ShopProductFragment extends BaseFragment implements BaseQuickAdapter.RequestLoadMoreListener {
    private RecyclerView mRlLeftList;
    private RecyclerView mRlRightList;
    private TextView mTvTitle;

    private String fixmedins_code;

    private LeftLinkAdapter leftLinkAdapter;
    private ShopProductRightLinkAdapter_user rightLinkAdapter;
    private int currentSelectPos = 0;
    private int mPageNum = 0;
    private String catalogueId = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_shop_product;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mRlLeftList = view.findViewById(R.id.rl_left_list);
        mRlRightList = view.findViewById(R.id.rl_right_list);
        mTvTitle = view.findViewById(R.id.tv_title);
    }

    @Override
    public void initData() {
        fixmedins_code = getArguments().getString("fixmedins_code", "");
        super.initData();

        initLeftView();
        initRightView();
//        showWaitDialog();
        getLeftData();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        //=========================左边列表===================================================
        //分类列表item点击事件
        leftLinkAdapter.setOnItemClickListener(new OnLeftItemClickListener());
        //=========================左边列表===================================================
    }

    //=========================左边列表===================================================
    //初始化左边分类view
    private void initLeftView() {
        mRlLeftList.setHasFixedSize(true);
        mRlLeftList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        leftLinkAdapter = new LeftLinkAdapter();
        mRlLeftList.setAdapter(leftLinkAdapter);
    }

    //获取左边列表的数据
    private void getLeftData() {
        PhaSortModel.sendShopClassifyRequest_user(TAG, fixmedins_code, new CustomerJsonCallBack<PhaSortModel>() {
            @Override
            public void onRequestError(PhaSortModel returnData, String msg) {
//                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(PhaSortModel returnData) {
//                hideWaitDialog();
                List<PhaSortModel.DataBean> datas = returnData.getData();
                if (datas == null) {
                    datas = new ArrayList<>();
                }
                datas.add(0,new PhaSortModel.DataBean("2","0","全部"));

                if (leftLinkAdapter!=null){
                    leftLinkAdapter.setNewData(datas);
                }
                //设置数据已经请求的id
                for (int i = 0; i < datas.size(); i++) {
                    PhaSortModel.DataBean data = datas.get(i);
                    if (i == 0) {
                        data.setChoose("1");
                        catalogueId = data.getCatalogueId();
                        mTvTitle.setText(data.getCatalogueName());
                    } else if (i == 1) {
                        data.setChoose("bottom01");
                    } else {
                        data.setChoose("0");
                    }
                }

//                showWaitDialog();
                onRefresh();
            }
        });
    }

    //左边列表item点击事件
    private class OnLeftItemClickListener implements BaseQuickAdapter.OnItemClickListener {

        @Override
        public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
            LeftLinkAdapter leftLinkAdapter = ((LeftLinkAdapter) baseQuickAdapter);
            PhaSortModel.DataBean leftLinkModel = leftLinkAdapter.getItem(i);

            if (!"1".equals(leftLinkModel.getChoose())) {//只响应未被选中的item
                moveToMiddle(mRlLeftList, i);

                leftLinkAdapter.setSelectedPosttion(baseQuickAdapter.getItemCount(), currentSelectPos, i);
                currentSelectPos = i;
                //设置数据已经请求的id
                catalogueId = leftLinkModel.getCatalogueId();
                mTvTitle.setText(leftLinkModel.getCatalogueName());

                showWaitDialog();
                onRefresh();
            }
        }
    }

    //将当前选中的item居中
    public void moveToMiddle(RecyclerView recyclerView, int position) {
        //先从RecyclerView的LayoutManager中获取当前第一项和最后一项的Position
        int firstItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
        int lastItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
        //中间位置
        int middle = (firstItem + lastItem) / 2;
        // 取绝对值，index下标是当前的位置和中间位置的差，下标为index的view的top就是需要滑动的距离
        int index = (position - middle) >= 0 ? position - middle : -(position - middle);
        //左侧列表一共有getChildCount个Item，如果>这个值会返回null，程序崩溃，如果>getChildCount直接滑到指定位置,或者,都一样啦
        if (index >= recyclerView.getChildCount()) {
            recyclerView.scrollToPosition(position);
        } else {
            //如果当前位置在中间位置上面，往下移动，这里为了防止越界
            if (position < middle) {
                recyclerView.scrollBy(0, -recyclerView.getChildAt(index).getTop());
                // 在中间位置的下面，往上移动
            } else {
                recyclerView.scrollBy(0, recyclerView.getChildAt(index).getTop());
            }
        }
    }
    //=========================左边列表===================================================

    //=========================右边列表===================================================
    //初始化右边分类详情列表view
    public void initRightView() {
        mRlRightList.setHasFixedSize(true);
        mRlRightList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rightLinkAdapter = new ShopProductRightLinkAdapter_user();
        mRlRightList.setAdapter(rightLinkAdapter);
        //设置开启上拉加载更多
        rightLinkAdapter.setEnableLoadMore(true);
        //设置上拉加载更多监听
        rightLinkAdapter.setOnLoadMoreListener(this, mRlRightList);
        rightLinkAdapter.setLoadMoreView(new CostomLoadMoreView());
        rightLinkAdapter.setEmptyView(R.layout.layout_empty_view, mRlRightList);

        rightLinkAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_1000)) {
                    return;
                }
                if (((ShopDetailActivity) getActivity()).getInsuredLoginSmrzUtils().isLoginSmrz(getContext())) {
                    GoodsModel.DataBean itemData = rightLinkAdapter.getItem(i);
                    switch (view.getId()) {
                        case R.id.fl_num_add://数量加
                            int num = itemData.getShoppingCartNum();
                            if (num+1<=itemData.getInventoryQuantity()){
                                onAddShopcar(itemData, baseQuickAdapter, view, i, "add");
                            }else {
                                showShortToast("该商品库存不足,请联系商家");
                            }
                            break;
                        case R.id.fl_num_reduce://数量减
                            onAddShopcar(itemData, baseQuickAdapter, view, i, "reduce");
                            break;
                    }
                }
            }
        });

        rightLinkAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_1000)) {
                    return;
                }
                GoodsModel.DataBean itemData = rightLinkAdapter.getItem(i);
                GoodDetailActivity.newIntance(getContext(), itemData.getGoodsNo(),itemData.getFixmedins_code(),itemData.getGoodsName());
            }
        });
    }

    //获取右边列表数据
    private void getRightData() {
        // 2022-10-21 获取数据
        GoodsModel.sendShopAllGoodsRequest_user(TAG, String.valueOf(mPageNum), "20", "",
                catalogueId, "0".equals(catalogueId)?"0":"", fixmedins_code, new CustomerJsonCallBack<GoodsModel>() {
                    @Override
                    public void onRequestError(GoodsModel returnData, String msg) {
                        hideWaitDialog();
                        setLoadMoreFail();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(GoodsModel returnData) {
                        hideWaitDialog();
                        List<GoodsModel.DataBean> datas = returnData.getData();
                        if (rightLinkAdapter != null && datas != null) {
                            //获取数据成功
                            if (mPageNum == 0) {
                                rightLinkAdapter.setNewData(datas);
                                if (mRlRightList!=null){
                                    mRlRightList.scrollToPosition(0);
                                }
                            } else {
                                rightLinkAdapter.addData(datas);
                                rightLinkAdapter.loadMoreComplete();
                            }

                            if (datas.isEmpty()) {
                                if (rightLinkAdapter.getData().size() < 8) {
                                    rightLinkAdapter.loadMoreEnd(true);
                                } else {
                                    rightLinkAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    //刷新
    public void onRefresh() {
        mPageNum = 0;
        getRightData();
    }

    @Override
    public void onLoadMoreRequested() {
        mPageNum++;
        getRightData();
    }

    //加入购物车
    private void onAddShopcar(GoodsModel.DataBean itemData, BaseQuickAdapter baseQuickAdapter, View view, int pos, String tag) {
        showWaitDialog();
        BaseModel.sendAddShopCarRequest(TAG, itemData.getFixmedins_code(), itemData.getGoodsNo(), "add".equals(tag) ? itemData.getShoppingCartNum() + 1 : itemData.getShoppingCartNum() - 1,
                new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        hideWaitDialog();
                        MsgBus.sendShopCarRefresh().post(new ShopCarRefreshModel(TAG, "1"));
                        switch (tag) {
                            case "add":
                                TextView tvSelectNum01 = (TextView) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.tv_select_num);

                                if (itemData.getShoppingCartNum() == 0) {
                                    FrameLayout flNumReduce01 = (FrameLayout) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.fl_num_reduce);
                                    ShapeTextView stv12 = (ShapeTextView) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.stv_02);
                                    TextView tvRealPrice = (TextView) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.tv_real_price);

                                    flNumReduce01.setVisibility(View.VISIBLE);
                                    tvSelectNum01.setVisibility(View.VISIBLE);
                                    stv12.setVisibility(View.GONE);
                                    tvRealPrice.setVisibility(View.GONE);

                                    itemData.setShoppingCartNum(1);
                                    tvSelectNum01.setText("1");
                                } else {
                                    itemData.setShoppingCartNum(itemData.getShoppingCartNum() + 1);
                                    tvSelectNum01.setText(String.valueOf(itemData.getShoppingCartNum()));
                                }

                                ((ShopDetailActivity) getActivity()).addCart(view, ParcelUtils.copy(itemData));
                                break;
                            case "reduce":
                                TextView tvSelectNum02 = (TextView) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.tv_select_num);

                                if (itemData.getShoppingCartNum() == 1) {
                                    FrameLayout flNumReduce02 = (FrameLayout) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.fl_num_reduce);
                                    ShapeTextView stv22 = (ShapeTextView) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.stv_02);
                                    TextView tvRealPrice = (TextView) baseQuickAdapter.getViewByPosition(mRlRightList, pos, R.id.tv_real_price);

                                    flNumReduce02.setVisibility(View.GONE);
                                    tvSelectNum02.setVisibility(View.GONE);
                                    double promotionDiscountVlaue = new BigDecimal(itemData.getPromotionDiscount()).doubleValue();
                                    if ("1".equals(itemData.getIsPromotion())&&promotionDiscountVlaue<10){
                                        stv22.setVisibility(View.VISIBLE);
                                        tvRealPrice.setVisibility(View.VISIBLE);
                                    }

                                    itemData.setShoppingCartNum(0);
                                    tvSelectNum02.setText("0");
                                } else {
                                    itemData.setShoppingCartNum(itemData.getShoppingCartNum() - 1);
                                    tvSelectNum02.setText(String.valueOf(itemData.getShoppingCartNum()));
                                }

                                ((ShopDetailActivity) getActivity()).onReduceCarList(ParcelUtils.copy(itemData), "1", null, null);
                                break;
                        }
                    }
                });
    }

    //当外部购物车弹框 改变选择的数量时 需要更新列表中对应的数量
    public void onRefreshRightItem(GoodsModel.DataBean item) {
        List<GoodsModel.DataBean> allDatas = rightLinkAdapter.getData();
        GoodsModel.DataBean itemData;
        for (int i = 0, size = allDatas.size(); i < size; i++) {
            itemData = allDatas.get(i);
            if (itemData.getGoodsNo().equals(item.getGoodsNo())) {
                itemData.setShoppingCartNum(item.getShoppingCartNum());
                rightLinkAdapter.notifyItemChanged(i);
                break;
            }
        }
    }

    //当外部购物车弹框 清空购物车时 需要更新列表
    public void onCleanShopCar() {
        for (GoodsModel.DataBean datum : rightLinkAdapter.getData()) {
            datum.setShoppingCartNum(0);
        }

        rightLinkAdapter.notifyDataSetChanged();
    }

    //加载更多时 遇到外围因素 失败时调用
    protected void setLoadMoreFail() {
        if (rightLinkAdapter!=null){
            rightLinkAdapter.loadMoreFail();
        }

        if (mPageNum > 1) {
            mPageNum -= 1;
        }
    }
    //=========================右边列表===================================================

    public static ShopProductFragment newIntance(String fixmedins_code) {
        ShopProductFragment shopProductFragment = new ShopProductFragment();
        Bundle bundle = new Bundle();
        bundle.putString("fixmedins_code", fixmedins_code);
        shopProductFragment.setArguments(bundle);
        return shopProductFragment;
    }
}
