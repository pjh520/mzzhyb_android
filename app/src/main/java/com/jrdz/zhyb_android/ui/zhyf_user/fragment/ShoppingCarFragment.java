package com.jrdz.zhyb_android.ui.zhyf_user.fragment;

import android.content.Context;
import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.swipeMenuLayout.SwipeMenuLayout;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.GoodDetailActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.OnlineBuyDrugActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.ShopDetailActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.ShoppingCarAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopCarRefreshModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-27
 * 描    述：购物车页面
 * ================================================
 */
public class ShoppingCarFragment extends BaseRecyclerViewFragment implements ShoppingCarAdapter.IOperation {
    private TextView mTvTotalPrice;
    private TextView mTvOriginTotalPrice;
    private TextView mTvFreight;
    private ShapeTextView mTvApplyBuy;

    //已选中的商品集合
    private ArrayList<GoodsModel.DataBean> chooseProductDatas = new ArrayList<>();
    private String storeName,storeAccessoryUrl,startingPrice,latitude,longitude,fixmedins_type;

    private CustomerDialogUtils customerDialogUtils;
    //登录成功信息
    private ObserverWrapper<Integer> mLoginObserve = new ObserverWrapper<Integer>() {
        @Override
        public void onChanged(@Nullable Integer value) {
            //设置机构、用户信息
            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        }
    };
    //操作商品监听
    private ObserverWrapper<ShopCarRefreshModel> mShopCarObserver=new ObserverWrapper<ShopCarRefreshModel>() {
        @Override
        public void onChanged(@Nullable ShopCarRefreshModel value) {
            switch (value.getTag_value()){
                case "1":
                case "2":
                    showWaitDialog();
                    onRefresh(mRefreshLayout);
                    break;
            }
        }
    };
    //商品收藏状态监听
    private ObserverWrapper<String> mCollectStatusObserver=new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            showWaitDialog();
            onRefresh(mRefreshLayout);
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.fragment_shopping_car;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mTvTotalPrice = view.findViewById(R.id.tv_total_price);
        mTvOriginTotalPrice = view.findViewById(R.id.tv_origin_total_price);
        mTvFreight = view.findViewById(R.id.tv_freight);
        mTvApplyBuy = view.findViewById(R.id.tv_apply_buy);

        hideLeftView();
        setTitle("需求清单");
        setRightIcon(getResources().getDrawable(R.drawable.ic_shopcar_delete));
        ImmersionBar.setTitleBar(this, mTitleBar);
    }

    @Override
    protected void initAdapter() {
        mAdapter = new ShoppingCarAdapter(this);
    }

    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    @Override
    public void initData() {
        super.initData();
        MsgBus.sendInsuredLoginStatus().observe(this, mLoginObserve);
        MsgBus.sendShopCarRefresh().observe(this, mShopCarObserver);
        MsgBus.sendCollectStatus_user().observe(this, mCollectStatusObserver);
        mTvOriginTotalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        mTvOriginTotalPrice.getPaint().setAntiAlias(true);

        showWaitDialog();
        onRefresh(mRefreshLayout);
    }

    @Override
    public void initEvent() {
        super.initEvent();

        setSmartHasRefreshOrLoadMore();
        setLoadMore();

        mTvApplyBuy.setOnClickListener(this);
    }

    @Override
    protected void getData() {
        super.getData();
        //2022-11-05 请求接口 获取已加入购物车数据
        ShopCarModel.sendShopCarRequest(TAG, "", String.valueOf(mPageNum), "3", new CustomerJsonCallBack<ShopCarModel>() {
            @Override
            public void onRequestError(ShopCarModel returnData, String msg) {
                hideRefreshView();
                setLoadMoreFail();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(ShopCarModel returnData) {
                hideRefreshView();
                List<ShopCarModel.DataBean> datas = returnData.getData();
                if (mAdapter!=null&&datas != null) {
                    if (mPageNum == 0) {
                        setPrice();
                        ((ShoppingCarAdapter) mAdapter).resetRecordData();

                        mAdapter.setNewData(datas);
                        if (datas.size() <= 0) {
                            mAdapter.isUseEmpty(true);
                        }
                    } else {
                        mAdapter.addData(datas);
                        mAdapter.loadMoreComplete();
                    }

                    if (datas.isEmpty()) {
                        if (mAdapter.getData().size() < 8) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreEnd();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void customItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemChildClick(adapter, view, position);
        ShopCarModel.DataBean itemData = ((ShoppingCarAdapter) adapter).getItem(position);
        switch (view.getId()) {
            case R.id.fl_head_product_select://选中商家
                ImageView ivHeadProductSelect = (ImageView) adapter.getViewByPosition(mRecyclerView, position, R.id.iv_head_product_select);
                LinearLayout rlProduct = (LinearLayout) adapter.getViewByPosition(mRecyclerView, position, R.id.rl_product);
                if (itemData.isChoose()) {
                    ivHeadProductSelect.setImageResource(R.drawable.ic_product_select_nor);
                    itemData.setChoose(false);

                    for (GoodsModel.DataBean shopProductBean : itemData.getShoppingCartGoods()) {
                        if ("1".equals(shopProductBean.getIsOnSale())) {
                            shopProductBean.setChoose(false);
                        }
                    }

                    //设置价格
                    setPrice();
                } else {
                    ivHeadProductSelect.setImageResource(R.drawable.ic_product_select_pre);
                    itemData.setChoose(true);

                    for (GoodsModel.DataBean shopProductBean : itemData.getShoppingCartGoods()) {
                        if ("1".equals(shopProductBean.getIsOnSale())) {
                            shopProductBean.setChoose(true);
                        }
                    }

                    ((ShoppingCarAdapter) adapter).canclePreItem(ivHeadProductSelect, rlProduct, position);

                    calPrice();
                }

                ((ShoppingCarAdapter) adapter).updateProductItemView(position);
                break;
            case R.id.ll_shop_head_info://进入店铺详情页面
                ShopDetailActivity.newIntance(getContext(),itemData.getFixmedins_code());
                break;
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.tv_apply_buy://申请购药
                onApplyBuy();
                break;
        }
    }

    @Override
    public void rightTitleViewClick() {
        if (mAdapter.getData().isEmpty()){
            showShortToast("购物车暂无商品");
            return;
        }
        if (customerDialogUtils == null) {
            customerDialogUtils = new CustomerDialogUtils();
        }
        customerDialogUtils.showDialog(getContext(), "提示", "确定清空购物车商品？", 2, "取消", "确定", R.color.txt_color_666, R.color.color_4970e0, new CustomerDialogUtils.IDialogListener() {
            @Override
            public void onBtn01Click() {
            }

            @Override
            public void onBtn02Click() {
                showWaitDialog();
                BaseModel.sendDelAllShopCarRequest(TAG, "", new CustomerJsonCallBack<BaseModel>() {
                    @Override
                    public void onRequestError(BaseModel returnData, String msg) {
                        hideWaitDialog();
                        showShortToast(msg);
                    }

                    @Override
                    public void onRequestSuccess(BaseModel returnData) {
                        onRefresh(mRefreshLayout);
                    }
                });
            }
        });
    }

    //============================== 列表item操作方法===============================================
    @Override
    public void onProductItem(String goodsNo,String fixmedins_code,String goodsName) {
        GoodDetailActivity.newIntance(getContext(), goodsNo,fixmedins_code,goodsName);
    }

    @Override
    public void calPrice() {
        double totalPrice = 0;//总价
        boolean isDiscount = false;//是否有打折商品
        double totalDiscountPrice = 0;//未打折的价格
        //清空数组 选中的数据
        chooseProductDatas.clear();
        //遍历列表 选出选中的数据列表
        ShoppingCarAdapter shoppingCarAdapter = (ShoppingCarAdapter) mAdapter;
        ShopCarModel.DataBean shopItemData = shoppingCarAdapter.getItem(shoppingCarAdapter.getCurrentPos());
        storeName=shopItemData.getStoreName();
        storeAccessoryUrl=shopItemData.getStoreAccessoryUrl();
        startingPrice=shopItemData.getStartingPrice();
        latitude=shopItemData.getLatitude();
        longitude=shopItemData.getLongitude();
        fixmedins_type=shopItemData.getFixmedins_type();
        for (GoodsModel.DataBean datum : shopItemData.getShoppingCartGoods()) {
            if (datum.isChoose()) {
                chooseProductDatas.add(datum);
                totalPrice = new BigDecimal(String.valueOf(totalPrice)).add(new BigDecimal(datum.getPrice())
                        .multiply(new BigDecimal(datum.getShoppingCartNum()))).doubleValue();
                double promotionDiscountVlaue = new BigDecimal(datum.getPromotionDiscount()).doubleValue();
                if ("1".equals(datum.getIsPromotion())&&promotionDiscountVlaue<10) {//有折扣
                    isDiscount = true;
                    totalDiscountPrice = new BigDecimal(totalDiscountPrice).add(new BigDecimal(datum.getPreferentialPrice())
                            .multiply(new BigDecimal(datum.getShoppingCartNum()))).doubleValue();
                } else {//没有折扣
                    totalDiscountPrice = new BigDecimal(totalDiscountPrice).add(new BigDecimal(datum.getPrice())
                            .multiply(new BigDecimal(datum.getShoppingCartNum()))).doubleValue();
                }
            }
        }

//        mTvTotalPrice.setText(DecimalFormatUtils.keepPlaces("0.00", totalPrice));
        mTvTotalPrice.setText(String.valueOf(totalDiscountPrice));

        if (isDiscount) {
            mTvOriginTotalPrice.setVisibility(View.VISIBLE);
            mTvOriginTotalPrice.setText("¥" + totalPrice);
        } else {
            mTvOriginTotalPrice.setVisibility(View.GONE);
        }
    }

    @Override
    public void setPrice() {
        if (chooseProductDatas!=null){
            chooseProductDatas.clear();
        }
        if (mTvTotalPrice!=null){
            mTvTotalPrice.setText("0");
        }
        if (mTvOriginTotalPrice!=null){
            mTvOriginTotalPrice.setVisibility(View.GONE);
        }
        if (mTvFreight!=null){
            mTvFreight.setText("不含运费");
        }
    }

    @Override
    public void numAdd(GoodsModel.DataBean dataBean,TextView tvSelectNum) {
        showWaitDialog();
        BaseModel.sendAddShopCarRequest(TAG, dataBean.getFixmedins_code(), dataBean.getGoodsNo(), dataBean.getShoppingCartNum() + 1, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                dataBean.setShoppingCartNum(dataBean.getShoppingCartNum() + 1);
                tvSelectNum.setText(String.valueOf(dataBean.getShoppingCartNum()));

                if (dataBean.isChoose()) {
                    calPrice();
                }
            }
        });
    }

    @Override
    public void numReduce(GoodsModel.DataBean dataBean,TextView tvSelectNum) {
        showWaitDialog();
        BaseModel.sendAddShopCarRequest(TAG, dataBean.getFixmedins_code(), dataBean.getGoodsNo(), dataBean.getShoppingCartNum() - 1, new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                dataBean.setShoppingCartNum(dataBean.getShoppingCartNum() - 1);
                tvSelectNum.setText(String.valueOf(dataBean.getShoppingCartNum()));

                if (dataBean.isChoose()) {
                    calPrice();
                }
            }
        });
    }

    @Override
    public void onCollect(GoodsModel.DataBean shopProductBean, ShapeLinearLayout llCollect, ImageView ivCollect, TextView tvCollect, SwipeMenuLayout swipeDelete) {
        if ("1".equals(shopProductBean.getIsCollection())){//已收藏
            onDelGoodCollect(shopProductBean,llCollect,ivCollect,tvCollect,swipeDelete);
        }else {//未收藏
            onAddGoodCollect(shopProductBean,llCollect,ivCollect,tvCollect,swipeDelete);
        }
    }
    //添加商品收藏
    private void onAddGoodCollect(GoodsModel.DataBean shopProductBean,ShapeLinearLayout llCollect,ImageView ivCollect, TextView tvCollect, SwipeMenuLayout swipeDelet) {
        showWaitDialog();
        BaseModel.sendAddGoodCollectionRequest(TAG,shopProductBean.getGoodsNo(), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("收藏成功");
                ivCollect.setImageResource(R.drawable.ic_shopcar_collect_nor);
                tvCollect.setText("取消\n收藏");
                tvCollect.setTextColor(getResources().getColor(R.color.white));
                shopProductBean.setIsCollection("1");
                llCollect.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.color_4870e0)).intoBackground();
            }
        });
    }
    //删除商品收藏
    private void onDelGoodCollect(GoodsModel.DataBean shopProductBean,ShapeLinearLayout llCollect,ImageView ivCollect, TextView tvCollect, SwipeMenuLayout swipeDelet) {
        showWaitDialog();
        BaseModel.sendDelGoodCollectionRequest(TAG, shopProductBean.getGoodsNo(), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("删除收藏成功");
                ivCollect.setImageResource(R.drawable.ic_shopcar_collect_pre);
                tvCollect.setText("收藏");
                tvCollect.setTextColor(getResources().getColor(R.color.color_4870e0));
                shopProductBean.setIsCollection("0");
                llCollect.getShapeDrawableBuilder().setSolidColor(getResources().getColor(R.color.white)).intoBackground();
            }
        });
    }

    @Override
    public void onDelete(int currentPos, int externalPos, ShopCarModel.DataBean shoppingCarModel, GoodsModel.DataBean shopProductBean, LinearLayout rlProduct, View view, SwipeMenuLayout swipeDelete) {
        showWaitDialog();
        BaseModel.sendDelShopCarRequest(TAG, shopProductBean.getGoodsNo(), new CustomerJsonCallBack<BaseModel>() {
            @Override
            public void onRequestError(BaseModel returnData, String msg) {
                hideWaitDialog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BaseModel returnData) {
                hideWaitDialog();
                showShortToast("删除成功");
                if (((ShoppingCarAdapter)mAdapter).isCanDeleteBusiness(externalPos)) {
                    mAdapter.remove(externalPos);

                    if (currentPos == externalPos) {
                        //移除商家之后 需要判断移除的item是否是当前选中的item 若是 那么需要重置原来记录的数据
                        ((ShoppingCarAdapter)mAdapter).resetRecordData();
                        //设置外部的总价
                        setPrice();
                    }
                } else {
                    shoppingCarModel.getShoppingCartGoods().remove(shopProductBean);
                    rlProduct.removeView(view);
                    swipeDelete.quickClose();

                    if (currentPos == externalPos && shopProductBean.isChoose()) {
                        calPrice();
                    }
                }
            }
        });
    }
    //============================== 列表item操作方法===============================================

    //申请购药
    private void onApplyBuy() {
        if (chooseProductDatas.isEmpty()) {
            showShortToast("请选择商品");
            return;
        }
        // 2022-10-28 请求接口 下单 跳转订单页面
        OnlineBuyDrugActivity.newIntance(getContext(),chooseProductDatas, EmptyUtils.strEmpty(storeName),EmptyUtils.strEmpty(storeAccessoryUrl),
                EmptyUtils.strEmpty(startingPrice),EmptyUtils.strEmpty(latitude),EmptyUtils.strEmpty(longitude),fixmedins_type);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ShoppingCarAdapter adapter = ((ShoppingCarAdapter) mAdapter);
        if (adapter != null && adapter.getCurrentIvHeadProductSelect() != null) {
            adapter.setCurrentIvHeadProductSelect(null);
        }
        if (adapter != null && adapter.getCurrentRlProduct() != null) {
            adapter.setCurrentRlProduct(null);
        }
        if (customerDialogUtils != null) {
            customerDialogUtils.onclean();
            customerDialogUtils = null;
        }

    }

    public static ShoppingCarFragment newIntance() {
        ShoppingCarFragment shoppingCarFragment = new ShoppingCarFragment();
        return shoppingCarFragment;
    }
}
