package com.jrdz.zhyb_android.ui.zhyf_user.fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.baidu.location.BDLocation;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.banner.listener.OnBannerClickListener;
import com.frame.compiler.widget.banner.manager.GlideAppSimpleImageManager_round;
import com.frame.compiler.widget.banner.widget.BannerLayout;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.lbs_library.utils.CustomLocationBean;
import com.frame.lbs_library.utils.ILocationListener;
import com.frame.lbs_library.utils.LBSUtil;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.base.baseRecyclerView.BaseRecyclerViewFragment;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.NewsDetailActivity;
import com.jrdz.zhyb_android.ui.home.model.BannerDataModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.MsgListActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.PhaSortAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.GoodDetailActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.MsgListActivity_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.PhaSortSpecialAreaActivity2_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.PhaSortSpecialAreaActivity3_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.PhaSortSpecialAreaActivity_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.SearchActivity_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.ShopDetailActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.DrugRecommendAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.DrugStoreAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.model.DrugStoreModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.UserIsReadMessageCountModel;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;
import com.jrdz.zhyb_android.utils.location.LocationTool;
import com.jrdz.zhyb_android.widget.pagemenu.PageMenuLayout;
import com.jrdz.zhyb_android.widget.pagemenu.holder.AbstractHolder;
import com.jrdz.zhyb_android.widget.pagemenu.holder.PageMenuViewHolderCreator;
import com.jrdz.zhyb_android.widget.pagemenu.indicator.CircleIndicator;
import com.scwang.smart.refresh.layout.constant.RefreshState;

import java.util.List;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-27
 * 描    述：智慧药房 用户端首页
 * ================================================
 */
public class SmartPhaHomeFragment_user extends BaseRecyclerViewFragment implements OnBannerClickListener {
    private LinearLayout mLlTitle;
    private FrameLayout mFlClose;
    private LinearLayout mLlLocationInfo;
    private TextView mTvLocationAddress;
    private FrameLayout mFlMsg;
    private ShapeView mSvRedPoint;

    private ShapeLinearLayout mSllSearch;
    private BannerLayout mBanner;
    private PageMenuLayout mPagemenu;
    private CircleIndicator indicator;
    private CustomeRecyclerView mSrlRecommend;
    private TextView mTvNearbyPharmacyTag;

    private DrugRecommendAdapter drugRecommendAdapter;//推荐列表适配器
    private int requestTag=0;

    //登录成功信息
    private ObserverWrapper<Integer> mLoginObserve = new ObserverWrapper<Integer>() {
        @Override
        public void onChanged(@Nullable Integer value) {
            //设置机构、用户信息
            if (drugRecommendAdapter != null) {
                drugRecommendAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_smartpha_home_user;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mLlTitle = view.findViewById(R.id.ll_title);
        mFlClose = view.findViewById(R.id.fl_close);

        mLlLocationInfo = view.findViewById(R.id.ll_location_info);
        mTvLocationAddress = view.findViewById(R.id.tv_location_address);

        mFlMsg = view.findViewById(R.id.fl_msg);
        mSvRedPoint = view.findViewById(R.id.sv_red_point);

        ImmersionBar.setTitleBar(this, mLlTitle);
    }

    @Override
    protected void initAdapter() {
        mAdapter = new DrugStoreAdapter();
        mAdapter.setHeaderAndEmpty(true);
    }

    @Override
    public void initData() {
        MsgBus.sendInsuredLoginStatus().observe(this, mLoginObserve);
        super.initData();
        onLocation();
        initHeadView();

        showWaitDialog();
        getBannerData();
        getPhaSortData();
        getDrugRecommendData();
    }

    @Override
    public void initEvent() {
        super.initEvent();

        mFlClose.setOnClickListener(this);
        mLlLocationInfo.setOnClickListener(this);
        mFlMsg.setOnClickListener(this);
        mSllSearch.setOnClickListener(this);
    }

    //是否一进入页面就加载空布局 默认为true
    @Override
    public void isShowEmptyView() {
        mAdapter.isUseEmpty(false);
    }

    //初始化头部view
    private void initHeadView() {
        View headView = LayoutInflater.from(getContext()).inflate(R.layout.layout_smartpha_home_headview_user, mRecyclerView, false);
        mSllSearch = headView.findViewById(R.id.sll_search);
        mBanner = headView.findViewById(R.id.banner);
        mPagemenu = headView.findViewById(R.id.pagemenu);
        indicator = headView.findViewById(R.id.indicator);
        mSrlRecommend = headView.findViewById(R.id.srl_recommend);
        mTvNearbyPharmacyTag= headView.findViewById(R.id.tv_nearby_pharmacy_tag);
        //推荐
        mSrlRecommend.setHasFixedSize(true);
        mSrlRecommend.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
        drugRecommendAdapter = new DrugRecommendAdapter();
        mSrlRecommend.setAdapter(drugRecommendAdapter);

        drugRecommendAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                GoodsModel.DataBean itemData = drugRecommendAdapter.getItem(i);
                GoodDetailActivity.newIntance(getContext(),itemData.getGoodsNo(),itemData.getFixmedins_code(),itemData.getGoodsName());
            }
        });

        mAdapter.addHeaderView(headView);
    }

    //获取轮播图数据
    private void getBannerData() {
        BannerDataModel.sendInsuredBannerDataRequest(TAG, new CustomerJsonCallBack<BannerDataModel>() {
            @Override
            public void onRequestError(BannerDataModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(BannerDataModel returnData) {
                dissWaitDailog();
                List<BannerDataModel.DataBean> bannerDatas = returnData.getData();
                if (bannerDatas != null&&!bannerDatas.isEmpty()) {
                    //测试替换图片标记 为了给米脂先看效果图 直接替换图片
                    for (BannerDataModel.DataBean bannerData : bannerDatas) {
                        bannerData.setImgurl("https://up.enterdesk.com/edpic/cc/4c/f9/cc4cf93461658f9caf395bd244170b54.jpg");
                    }
                    if (bannerDatas.size() == 1) {
                        mBanner.initTips(false, false, false)
                                .setImageLoaderManager(new GlideAppSimpleImageManager_round())
                                .setPageNumViewMargin(12, 12, 12, 12)
                                .setViewPagerTouchMode(true)
                                .initListResources(bannerDatas)
                                .switchBanner(false)
                                .setOnBannerClickListener(SmartPhaHomeFragment_user.this);
                    } else {
                        mBanner.initTips(false, true, false)
                                .setImageLoaderManager(new GlideAppSimpleImageManager_round())
                                .setPageNumViewMargin(12, 12, 12, 12)
                                .setViewPagerTouchMode(false)
                                .initListResources(bannerDatas)
                                .switchBanner(true)
                                .setOnBannerClickListener(SmartPhaHomeFragment_user.this);
                    }
                }
            }
        });
    }

    //获取药品分类数据
    private void getPhaSortData() {
        PhaSortModel.sendShopClassifyRequest_user(TAG,"",new CustomerJsonCallBack<PhaSortModel>() {
            @Override
            public void onRequestError(PhaSortModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(PhaSortModel returnData) {
                dissWaitDailog();
                List<PhaSortModel.DataBean> datas = returnData.getData();
//                datas.add(new PhaSortModel.DataBean("2","1","常备药2"));
//                datas.add(new PhaSortModel.DataBean("2","1","男性专区2"));
//                datas.add(new PhaSortModel.DataBean("2","1","女性专区2"));
//
//                datas.add(new PhaSortModel.DataBean("2","1","常备药3"));
//                datas.add(new PhaSortModel.DataBean("2","1","男性专区3"));
//                datas.add(new PhaSortModel.DataBean("2","1","女性专区3"));
//
//                datas.add(new PhaSortModel.DataBean("2","1","常备药4"));
//                datas.add(new PhaSortModel.DataBean("2","1","男性专区4"));
//                datas.add(new PhaSortModel.DataBean("2","1","女性专区4"));
//
//                datas.add(new PhaSortModel.DataBean("2","1","常备药5"));
//                datas.add(new PhaSortModel.DataBean("2","1","男性专区5"));
//                datas.add(new PhaSortModel.DataBean("2","1","女性专区5"));
//
//                datas.add(new PhaSortModel.DataBean("2","1","常备药6"));
//                datas.add(new PhaSortModel.DataBean("2","1","男性专区6"));
//                datas.add(new PhaSortModel.DataBean("2","1","女性专区6"));
//
//                datas.add(new PhaSortModel.DataBean("2","1","常备药7"));
//                datas.add(new PhaSortModel.DataBean("2","1","男性专区7"));
//                datas.add(new PhaSortModel.DataBean("2","1","女性专区7"));
//
//                datas.add(new PhaSortModel.DataBean("2","1","常备药8"));
//                datas.add(new PhaSortModel.DataBean("2","1","男性专区8"));
//                datas.add(new PhaSortModel.DataBean("2","1","女性专区8"));
//
//                datas.add(new PhaSortModel.DataBean("2","1","常备药9"));
//                datas.add(new PhaSortModel.DataBean("2","1","男性专区9"));
//                datas.add(new PhaSortModel.DataBean("2","1","女性专区9"));

                if (datas!=null){
                    initPagemenu(datas);
                }
            }
        });
    }

    //获取推荐数据
    private void getDrugRecommendData() {
        GoodsModel.sendHomeRecommendListRequest_user(TAG, new CustomerJsonCallBack<GoodsModel>() {
            @Override
            public void onRequestError(GoodsModel returnData, String msg) {
                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(GoodsModel returnData) {
                dissWaitDailog();
                List<GoodsModel.DataBean> datas = returnData.getData();
                if (drugRecommendAdapter!=null&&datas!=null){
                    drugRecommendAdapter.setNewData(datas);
                }
            }
        });
    }

    @Override
    protected void getData() {
        super.getData();

        DrugStoreModel.sendDrugStoreRequest(TAG, String.valueOf(Constants.AppStorage.APP_LATITUDE),  String.valueOf(Constants.AppStorage.APP_LONGITUDE), new CustomerJsonCallBack<DrugStoreModel>() {
            @Override
            public void onRequestError(DrugStoreModel returnData, String msg) {
//                dissWaitDailog();
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(DrugStoreModel returnData) {
//                dissWaitDailog();
                List<DrugStoreModel.DataBean> datas = returnData.getData();

                if (mAdapter!=null&&datas!=null){
                    mAdapter.setNewData(datas);
                }

                if (datas==null||datas.isEmpty()) {
                    mAdapter.isUseEmpty(true);
                }
            }
        });
    }

    //初始化药品分类
    private void initPagemenu(List<PhaSortModel.DataBean> datas) {
        mPagemenu.setPageDatas(datas, new PageMenuViewHolderCreator() {
            @Override
            public int getLayoutId() {
                return R.layout.layout_phasort_item;
            }

            @Override
            public AbstractHolder createHolder(View itemView) {
                return new AbstractHolder<PhaSortModel.DataBean>(itemView) {
                    private ImageView ivMall;
                    private TextView tvmall;

                    @Override
                    protected void initView(View itemView) {
                        ivMall = itemView.findViewById(R.id.iv_mall);
                        tvmall = itemView.findViewById(R.id.tv_mall);
                    }

                    @Override
                    public void bindView(RecyclerView.ViewHolder holder, final PhaSortModel.DataBean data, int pos) {
                        GlideUtils.loadImg(data.getAccessoryUrl(), ivMall,R.drawable.ic_add_dir_bg,new CircleCrop());
                        tvmall.setText(EmptyUtils.strEmpty(data.getCatalogueName()));
                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_1000)) {
                                    return;
                                }
                                // 2022-09-30 点击item 进入药品专区
                                if ("1".equals(data.getCatalogueId())){
                                    PhaSortSpecialAreaActivity2_user.newIntance(getContext(),data.getCatalogueId(),data.getCatalogueName());
                                }else {
                                    PhaSortSpecialAreaActivity3_user.newIntance(getContext(), data.getCatalogueId(), data.getCatalogueName(), "");
                                }
                            }
                        });
                    }
                };
            }
        });

        if (mPagemenu.getPageCount() > 1) {
            indicator.setVisibility(View.VISIBLE);
            indicator.setViewPager(mPagemenu);
        } else {
            indicator.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBannerClick(View view, int position, Object model) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            return;
        }

        if (model instanceof BannerDataModel.DataBean) {
            BannerDataModel.DataBean dataBean = (BannerDataModel.DataBean) model;
            if (dataBean != null) {
                //轮播图type 1代表纯广告 2跳外链 3跳公告        跳公告的id我放在url里面
                switch (dataBean.getTypeX()) {
                    case "1":
                        break;
                    case "2":
                        MyWebViewActivity.newIntance(getContext(), dataBean.getTitle(), dataBean.getUrlX(), true, false);
                        break;
                    case "3":
                        NewsDetailActivity.newIntance(getContext(), dataBean.getUrlX());
                        break;
                }
            }
        }
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()){
            case R.id.fl_close:
                goFinish();
                break;
            case R.id.ll_location_info://定位
                onLocation();
                break;
            case R.id.fl_msg://消息
                MsgListActivity_user.newIntance(getContext());
                break;
            case R.id.sll_search://跳转搜索页面
                SearchActivity_user.newIntance(getContext());
                break;
        }
    }

    @Override
    public void customItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.customItemClick(adapter, view, position);
        DrugStoreModel.DataBean itemData = ((DrugStoreAdapter) adapter).getItem(position);

        ShopDetailActivity.newIntance(getContext(),itemData.getFixmedins_code());
    }

    //获取定位信息
    private void onLocation() {
        LocationTool.getInstance().registerLocation(this,new ILocationListener() {
                    @Override
                    public void onLocationSuccess(CustomLocationBean customLocationBean) {
                        Constants.AppStorage.APP_LATITUDE = customLocationBean.getLatitude();
                        Constants.AppStorage.APP_LONGITUDE = customLocationBean.getLongitude();
                        mLlLocationInfo.setEnabled(false);
                        mTvLocationAddress.setText(customLocationBean.getCity() + "·" + customLocationBean.getDetail_address());

                        onRefresh(mRefreshLayout);
                    }

                    @Override
                    public void onLocationError(String errorText) {
                        dissWaitDailog();
                        mLlLocationInfo.setEnabled(true);
                        mTvLocationAddress.setText("定位失败");
                        showTipDialog(errorText);
                    }
                }
        );
    }

    //隐藏加载框
    public void dissWaitDailog() {
        requestTag++;
        if (requestTag >= 3) {
            if (mRefreshLayout.getState() == RefreshState.Refreshing) {
                mRefreshLayout.finishRefresh();
            } else {
                hideWaitDialog();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisibleToUser) {
            getUserIsReadMessageCount();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            getUserIsReadMessageCount();
        }
    }

    //获取用户端 未读消息
    private void getUserIsReadMessageCount() {
        UserIsReadMessageCountModel.sendUserIsReadMessageCountRequest(TAG, new CustomerJsonCallBack<UserIsReadMessageCountModel>() {
            @Override
            public void onRequestError(UserIsReadMessageCountModel returnData, String msg) {
                showShortToast(msg);
            }

            @Override
            public void onRequestSuccess(UserIsReadMessageCountModel returnData) {
                if (returnData.getData()!=null){
                    if ("0".equals(returnData.getData().getMessageCount())){
                        mSvRedPoint.setVisibility(View.GONE);
                    }else {
                        mSvRedPoint.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        LocationTool.getInstance().unRegisterLocation();
    }

    public static SmartPhaHomeFragment_user newIntance() {
        SmartPhaHomeFragment_user smartPhaHomeFragment_user = new SmartPhaHomeFragment_user();
        return smartPhaHomeFragment_user;
    }
}
