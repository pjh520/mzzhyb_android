package com.jrdz.zhyb_android.ui.zhyf_user.fragment;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.FileUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.StringUtils;
import com.frame.compiler.utils.storage.FileStorageUtils;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeRelativeLayout;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseFragment;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.AboutUsActivity_ZhyfManage;
import com.jrdz.zhyb_android.ui.zhyf_manage.activity.MsgListActivity;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.PhaSortMineAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortMineModel;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.AddressManageActivtiy;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.FreeBackActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.InsuredAccountActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.MyCollectionActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.MyTracksActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.OrderListActivity_user;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.PersonalActivity_user;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.jrdz.zhyb_android.utils.InsuredLoginUtils;

import java.util.ArrayList;

import cody.bus.ObserverWrapper;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.home.fragment
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/20
 * 描    述：智慧药房--我的页面
 * ================================================
 */
public class SmartPhaMineFragment_user extends BaseFragment {
    private FrameLayout mFlMsg;
    private FrameLayout mFlSetting;
    private ShapeRelativeLayout mRlPersonal;
    private ImageView mIvAccount;
    private TextView mTvPhone;
    private TextView mTvNickname;
    private CustomeRecyclerView mSrlOrderSort;
    private CustomeRecyclerView mSrlOtherSort;

    private PhaSortMineAdapter orderSortAdapter;//我的订单 适配器
    private PhaSortMineAdapter otherSortAdapter;//其他服务 适配器

    private ObserverWrapper<String> mUpdateInsuredInfoObserve=new ObserverWrapper<String>() {
        @Override
        public void onChanged(@Nullable String value) {
            switch (value){
                case "11"://更新昵称
                    mTvNickname.setText(InsuredLoginUtils.getNickname());
                    break;
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableLazyLoad();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_smartpha_mine;
    }

    @Override
    public void initView(View view) {
        super.initView(view);
        mFlMsg = view.findViewById(R.id.fl_msg);
        mFlSetting = view.findViewById(R.id.fl_setting);
        mRlPersonal = view.findViewById(R.id.rl_personal);
        mIvAccount = view.findViewById(R.id.iv_account);
        mTvPhone = view.findViewById(R.id.tv_phone);
        mTvNickname = view.findViewById(R.id.tv_nickname);
        mSrlOrderSort = view.findViewById(R.id.srl_order_sort);
        mSrlOtherSort = view.findViewById(R.id.srl_other_sort);

        mFlSetting.setVisibility(View.GONE);
    }

    @Override
    public void initData() {
        super.initData();
        MsgBus.updateInsuredInfo().observe(this, mUpdateInsuredInfoObserve);
        // 2022-10-09 新版本需要增加用户头像
        GlideUtils.loadImg(InsuredLoginUtils.getAccessoryUrl(), mIvAccount,R.drawable.ic_mine_head,new CircleCrop());
        mTvPhone.setText(StringUtils.encryptionPhone(InsuredLoginUtils.getPhone()));
        mTvNickname.setText(EmptyUtils.strEmpty(InsuredLoginUtils.getNickname()));

        //我的订单
        mSrlOrderSort.setHasFixedSize(true);
        mSrlOrderSort.setLayoutManager(new GridLayoutManager(getContext(), 4, GridLayoutManager.VERTICAL, false));
        orderSortAdapter = new PhaSortMineAdapter();
        mSrlOrderSort.setAdapter(orderSortAdapter);
        orderSortAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getOrderSortData());
        //其他服务
        mSrlOtherSort.setHasFixedSize(true);
        mSrlOtherSort.setLayoutManager(new GridLayoutManager(getContext(), 4, GridLayoutManager.VERTICAL, false));
        otherSortAdapter = new PhaSortMineAdapter();
        mSrlOtherSort.setAdapter(otherSortAdapter);
        otherSortAdapter.setNewData(CommonlyUsedDataUtils.getInstance().getOtherServiceData_user());
    }

    @Override
    public void initEvent() {
        super.initEvent();
        mFlMsg.setOnClickListener(this);
        mRlPersonal.setOnClickListener(this);

        //我的订单 item点击事件
        orderSortAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                PhaSortMineModel itemData = orderSortAdapter.getItem(i);
                switch (itemData.getId()) {
                    case "4001"://全部
                        OrderListActivity_user.newIntance(getContext(), 0);
                        break;
                    case "4002"://待付款
                        OrderListActivity_user.newIntance(getContext(), 1);
                        break;
                    case "4003"://待发货
                        OrderListActivity_user.newIntance(getContext(), 2);
                        break;
                    case "4004"://待收货
                        OrderListActivity_user.newIntance(getContext(), 3);
                        break;
                    case "4005"://已完成
                        OrderListActivity_user.newIntance(getContext(), 4);
                        break;
                    case "4006"://已取消
                        OrderListActivity_user.newIntance(getContext(), 5);
                        break;
                }
            }
        });

        //其他服务 item点击事件
        otherSortAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                PhaSortMineModel itemData = otherSortAdapter.getItem(i);
                switch (itemData.getId()) {
                    case "5101"://我的地址
                        AddressManageActivtiy.newIntance(getContext());
                        break;
                    case "5102"://我的收藏
                        MyCollectionActivity.newIntance(getContext());
                        break;
                    case "5103"://我的足迹
                        MyTracksActivity.newIntance(getContext());
                        break;
                    case "5104"://意见反馈
                        FreeBackActivity.newIntance(getContext());
                        break;
                    case "5105"://关于我们
                        AboutUsActivity_ZhyfManage.newIntance(getContext(),1);
                        break;
                    case "5106"://我的账户
                        InsuredAccountActivity.newIntance(getContext());
//                        ArrayList<String> datas= FileUtils.readStringFromTxtFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()+"/test.txt");
//                        Log.e("666666", "===="+datas.toString());
                        break;
                }
            }
        });
    }

    @Override
    public void customClick(View v) {
        super.customClick(v);
        switch (v.getId()) {
            case R.id.fl_msg://消息提醒
                MsgListActivity.newIntance(getContext());
                break;
            case R.id.rl_personal://个人信息
                PersonalActivity_user.newIntance(getContext());
                break;

        }
    }

    public static SmartPhaMineFragment_user newIntance() {
        SmartPhaMineFragment_user smartPhaMineFragment_user = new SmartPhaMineFragment_user();
        return smartPhaMineFragment_user;
    }
}
