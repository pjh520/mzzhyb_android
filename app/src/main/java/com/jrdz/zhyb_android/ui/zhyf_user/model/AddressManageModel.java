package com.jrdz.zhyb_android.ui.zhyf_user.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-31
 * 描    述：
 * ================================================
 */
public class AddressManageModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-24 11:20:32
     * data : [{"ShipToAddressId":1,"FullName":"彭俊鸿","Phone":"15060338985","Address":"福建省莆田市涵江区涵西街道湖园路在世纪名苑-东北门附近","IsDefault":1,"UserId":"2fe33bde-60de-4c41-8f3c-86f0720832a2","UserName":"15060338985"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * ShipToAddressId : 1
         * FullName : 彭俊鸿
         * Phone : 15060338985
         * Address : 福建省莆田市涵江区涵西街道湖园路在世纪名苑-东北门附近
         * IsDefault : 1
         * UserId : 2fe33bde-60de-4c41-8f3c-86f0720832a2
         * UserName : 15060338985
         */

        private String ShipToAddressId;
        private String FullName;
        private String Phone;
        private String Location;
        private String Address;
        private String IsDefault;
        private String UserId;
        private String UserName;
        private String Latitude;
        private String Longitude;
        private String AreaName;
        private String City;

        public String getShipToAddressId() {
            return ShipToAddressId;
        }

        public void setShipToAddressId(String ShipToAddressId) {
            this.ShipToAddressId = ShipToAddressId;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String Phone) {
            this.Phone = Phone;
        }

        public String getLocation() {
            return Location;
        }

        public void setLocation(String location) {
            Location = location;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getIsDefault() {
            return IsDefault;
        }

        public void setIsDefault(String IsDefault) {
            this.IsDefault = IsDefault;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String latitude) {
            Latitude = latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String longitude) {
            Longitude = longitude;
        }

        public String getAreaName() {
            return AreaName;
        }

        public void setAreaName(String areaName) {
            AreaName = areaName;
        }

        public String getCity() {
            return City;
        }

        public void setCity(String city) {
            City = city;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.ShipToAddressId);
            dest.writeString(this.FullName);
            dest.writeString(this.Phone);
            dest.writeString(this.Location);
            dest.writeString(this.Address);
            dest.writeString(this.IsDefault);
            dest.writeString(this.UserId);
            dest.writeString(this.UserName);
            dest.writeString(this.Latitude);
            dest.writeString(this.Longitude);
            dest.writeString(this.AreaName);
            dest.writeString(this.City);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.ShipToAddressId = in.readString();
            this.FullName = in.readString();
            this.Phone = in.readString();
            this.Location= in.readString();
            this.Address = in.readString();
            this.IsDefault = in.readString();
            this.UserId = in.readString();
            this.UserName = in.readString();
            this.Latitude = in.readString();
            this.Longitude = in.readString();
            this.AreaName = in.readString();
            this.City = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //收货地址列表-用户端
    public static void sendAddAddressRequest_user(final String TAG, final CustomerJsonCallBack<AddressManageModel> callback) {
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ADDRESS_LIST_URL, "", callback);
    }
}
