package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-25
 * 描    述：
 * ================================================
 */
public class DefaultAddressModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-25 11:49:14
     * data : {"ShipToAddressId":1,"FullName":"彭俊鸿","Phone":"150****8985","Address":"涵西街道湖园路在世纪名苑-东北门附近","IsDefault":1,"UserId":"ffc2fefe-d250-425c-a40c-eb67b746bb73","UserName":"15060338985","Location":"福建省莆田市涵江区"}
     */

    private String code;
    private String msg;
    private String server_time;
    private AddressManageModel.DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public AddressManageModel.DataBean getData() {
        return data;
    }

    public void setData(AddressManageModel.DataBean data) {
        this.data = data;
    }

    //获取附近药房
    public static void sendDefaultAddressRequest(final String TAG,final CustomerJsonCallBack<DefaultAddressModel> callback) {
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_DEFAULTADDRESS_URL, "", callback);
    }
}
