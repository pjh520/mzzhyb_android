package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-01-10
 * 描    述：
 * ================================================
 */
public class DetailCatalogueModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-01-10 19:04:06
     * data : {"AccessoryUrl":"","SubAccessoryUrl":"","CatalogueId":1,"CatalogueName":"常备药","AccessoryId":"00000000-0000-0000-0000-000000000000","SubAccessoryId":"00000000-0000-0000-0000-000000000000","fixmedins_code":"","IsHomePage":1,"IsClassification":0,"Sort":1,"IsSystem":1}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * AccessoryUrl :
         * SubAccessoryUrl :
         * CatalogueId : 1
         * CatalogueName : 常备药
         * AccessoryId : 00000000-0000-0000-0000-000000000000
         * SubAccessoryId : 00000000-0000-0000-0000-000000000000
         * fixmedins_code :
         * IsHomePage : 1
         * IsClassification : 0
         * Sort : 1
         * IsSystem : 1
         */

        private String SubAccessoryUrl;

        public String getSubAccessoryUrl() {
            return SubAccessoryUrl;
        }

        public void setSubAccessoryUrl(String SubAccessoryUrl) {
            this.SubAccessoryUrl = SubAccessoryUrl;
        }
    }

    //目录详情
    public static void sendDetailCatalogueRequest_user(final String TAG, String CatalogueId,
                                                       final CustomerJsonCallBack<DetailCatalogueModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CatalogueId", CatalogueId);
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_DETAILCATALOGUE_URL, jsonObject.toJSONString(), callback);
    }
}
