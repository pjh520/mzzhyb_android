package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_manage.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-20
 * 描    述：
 * ================================================
 */
public class ElectPrescModel2 {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2024-01-11 12:54:39
     * data : {"ElectronicPrescriptions":{"electronicPrescriptionNo":"503154","CreateDT":"2024-01-10 14:08:39","OrderNo":"20240110121459100745","UserName":"15060338985","psn_name":"彭俊鸿","sex":"S00030001","age":"32岁","diag_name":"高血压","MedicareAmount":1.08,"AccessoryUrl":"https://fttest.oss-cn-hangzhou.aliyuncs.com/wxConsultDoc/wxPresImg/2024/1/503154_1704866976914.jpg","video":"","record":"http://fttest.oss-cn-hangzhou.aliyuncs.com/gzz_video/2024/01/10/503154-24-1-10-14-8-46.mp3"},"OrderGoods":[{"GoodsName":"金匮肾气丸","GoodsNum":1,"Price":1.08,"fixmedins_code":"H61080300942","ItemType":2,"BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/crm_product/284f16bf-98a1-4ac9-8c72-0794149a5cc4/ddbf63d2-f134-40c9-8829-73064e1f2a73.jfif","IsPlatformDrug":1,"AssociatedDiseases":"水肿","Spec":"5g","UsageDosage":"","Remark":""}],"otherinfo":{"dr_name":"王医生","doctorurl":""}}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * ElectronicPrescriptions : {"electronicPrescriptionNo":"503154","CreateDT":"2024-01-10 14:08:39","OrderNo":"20240110121459100745","UserName":"15060338985","psn_name":"彭俊鸿","sex":"S00030001","age":"32岁","diag_name":"高血压","MedicareAmount":1.08,"AccessoryUrl":"https://fttest.oss-cn-hangzhou.aliyuncs.com/wxConsultDoc/wxPresImg/2024/1/503154_1704866976914.jpg","video":"","record":"http://fttest.oss-cn-hangzhou.aliyuncs.com/gzz_video/2024/01/10/503154-24-1-10-14-8-46.mp3"}
         * OrderGoods : [{"GoodsName":"金匮肾气丸","GoodsNum":1,"Price":1.08,"fixmedins_code":"H61080300942","ItemType":2,"BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/crm_product/284f16bf-98a1-4ac9-8c72-0794149a5cc4/ddbf63d2-f134-40c9-8829-73064e1f2a73.jfif","IsPlatformDrug":1,"AssociatedDiseases":"水肿","Spec":"5g","UsageDosage":"","Remark":""}]
         * otherinfo : {"dr_name":"王医生","doctorurl":""}
         */

        private ElectronicPrescriptionsBean ElectronicPrescriptions;
        private OtherinfoBean otherinfo;
        private List<OrderGoodsBean> OrderGoods;

        public ElectronicPrescriptionsBean getElectronicPrescriptions() {
            return ElectronicPrescriptions;
        }

        public void setElectronicPrescriptions(ElectronicPrescriptionsBean ElectronicPrescriptions) {
            this.ElectronicPrescriptions = ElectronicPrescriptions;
        }

        public OtherinfoBean getOtherinfo() {
            return otherinfo;
        }

        public void setOtherinfo(OtherinfoBean otherinfo) {
            this.otherinfo = otherinfo;
        }

        public List<OrderGoodsBean> getOrderGoods() {
            return OrderGoods;
        }

        public void setOrderGoods(List<OrderGoodsBean> OrderGoods) {
            this.OrderGoods = OrderGoods;
        }

        public static class ElectronicPrescriptionsBean {
            /**
             * electronicPrescriptionNo : 503154
             * CreateDT : 2024-01-10 14:08:39
             * OrderNo : 20240110121459100745
             * UserName : 15060338985
             * psn_name : 彭俊鸿
             * sex : S00030001
             * age : 32岁
             * diag_name : 高血压
             * MedicareAmount : 1.08
             * AccessoryUrl : https://fttest.oss-cn-hangzhou.aliyuncs.com/wxConsultDoc/wxPresImg/2024/1/503154_1704866976914.jpg
             * video :
             * record : http://fttest.oss-cn-hangzhou.aliyuncs.com/gzz_video/2024/01/10/503154-24-1-10-14-8-46.mp3
             */

            private String electronicPrescriptionNo;
            private String CreateDT;
            private String OrderNo;
            private String UserName;
            private String psn_name;
            private String sex;
            private String age;
            private String diag_name;
            private String MedicareAmount;
            private String AccessoryUrl;
            private String video;
            private String record;

            public String getElectronicPrescriptionNo() {
                return electronicPrescriptionNo;
            }

            public void setElectronicPrescriptionNo(String electronicPrescriptionNo) {
                this.electronicPrescriptionNo = electronicPrescriptionNo;
            }

            public String getCreateDT() {
                return CreateDT;
            }

            public void setCreateDT(String CreateDT) {
                this.CreateDT = CreateDT;
            }

            public String getOrderNo() {
                return OrderNo;
            }

            public void setOrderNo(String OrderNo) {
                this.OrderNo = OrderNo;
            }

            public String getUserName() {
                return UserName;
            }

            public void setUserName(String UserName) {
                this.UserName = UserName;
            }

            public String getPsn_name() {
                return psn_name;
            }

            public void setPsn_name(String psn_name) {
                this.psn_name = psn_name;
            }

            public String getSex() {
                return sex;
            }

            public void setSex(String sex) {
                this.sex = sex;
            }

            public String getAge() {
                return age;
            }

            public void setAge(String age) {
                this.age = age;
            }

            public String getDiag_name() {
                return diag_name;
            }

            public void setDiag_name(String diag_name) {
                this.diag_name = diag_name;
            }

            public String getMedicareAmount() {
                return MedicareAmount;
            }

            public void setMedicareAmount(String MedicareAmount) {
                this.MedicareAmount = MedicareAmount;
            }

            public String getAccessoryUrl() {
                return AccessoryUrl;
            }

            public void setAccessoryUrl(String AccessoryUrl) {
                this.AccessoryUrl = AccessoryUrl;
            }

            public String getVideo() {
                return video;
            }

            public void setVideo(String video) {
                this.video = video;
            }

            public String getRecord() {
                return record;
            }

            public void setRecord(String record) {
                this.record = record;
            }
        }

        public static class OtherinfoBean {
            /**
             * dr_name : 王医生
             * doctorurl :
             */

            private String dr_name;
            private String doctorurl;

            public String getDr_name() {
                return dr_name;
            }

            public void setDr_name(String dr_name) {
                this.dr_name = dr_name;
            }

            public String getDoctorurl() {
                return doctorurl;
            }

            public void setDoctorurl(String doctorurl) {
                this.doctorurl = doctorurl;
            }
        }

        public static class OrderGoodsBean {
            /**
             * GoodsName : 金匮肾气丸
             * GoodsNum : 1
             * Price : 1.08
             * fixmedins_code : H61080300942
             * ItemType : 2
             * BannerAccessoryUrl1 : http://113.135.194.23:3080/App_Upload/Storage/crm_product/284f16bf-98a1-4ac9-8c72-0794149a5cc4/ddbf63d2-f134-40c9-8829-73064e1f2a73.jfif
             * IsPlatformDrug : 1
             * AssociatedDiseases : 水肿
             * Spec : 5g
             * UsageDosage :
             * Remark :
             */

            private String GoodsName;
            private String GoodsNum;
            private String Price;
            private String fixmedins_code;
            private String ItemType;
            private String BannerAccessoryUrl1;
            private String IsPlatformDrug;
            private String AssociatedDiseases;
            private String Spec;
            private String UsageDosage;
            private String Remark;

            public String getGoodsName() {
                return GoodsName;
            }

            public void setGoodsName(String GoodsName) {
                this.GoodsName = GoodsName;
            }

            public String getGoodsNum() {
                return GoodsNum;
            }

            public void setGoodsNum(String GoodsNum) {
                this.GoodsNum = GoodsNum;
            }

            public String getPrice() {
                return Price;
            }

            public void setPrice(String Price) {
                this.Price = Price;
            }

            public String getFixmedins_code() {
                return fixmedins_code;
            }

            public void setFixmedins_code(String fixmedins_code) {
                this.fixmedins_code = fixmedins_code;
            }

            public String getItemType() {
                return ItemType;
            }

            public void setItemType(String ItemType) {
                this.ItemType = ItemType;
            }

            public String getBannerAccessoryUrl1() {
                return BannerAccessoryUrl1;
            }

            public void setBannerAccessoryUrl1(String BannerAccessoryUrl1) {
                this.BannerAccessoryUrl1 = BannerAccessoryUrl1;
            }

            public String getIsPlatformDrug() {
                return IsPlatformDrug;
            }

            public void setIsPlatformDrug(String IsPlatformDrug) {
                this.IsPlatformDrug = IsPlatformDrug;
            }

            public String getAssociatedDiseases() {
                return AssociatedDiseases;
            }

            public void setAssociatedDiseases(String AssociatedDiseases) {
                this.AssociatedDiseases = AssociatedDiseases;
            }

            public String getSpec() {
                return Spec;
            }

            public void setSpec(String Spec) {
                this.Spec = Spec;
            }

            public String getUsageDosage() {
                return UsageDosage;
            }

            public void setUsageDosage(String UsageDosage) {
                this.UsageDosage = UsageDosage;
            }

            public String getRemark() {
                return Remark;
            }

            public void setRemark(String Remark) {
                this.Remark = Remark;
            }
        }
    }

    //查看订单处方--用户端
    public static void sendElectPrescModelRequest_user(final String TAG, String OrderNo, final CustomerJsonCallBack<ElectPrescModel2> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_USERORDERELECTRONICPRESCRIPTIONDETAIL_URL, jsonObject.toJSONString(), callback);
    }

    //查看订单处方--商户端
    public static void sendElectPrescModelRequest(final String TAG, String OrderNo, final CustomerJsonCallBack<ElectPrescModel2> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_ORDERELECTRONICPRESCRIPTIONDET_URL, jsonObject.toJSONString(), callback);
    }
}
