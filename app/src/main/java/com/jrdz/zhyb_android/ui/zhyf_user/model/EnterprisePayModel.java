package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-06-09
 * 描    述：
 * ================================================
 */
public class EnterprisePayModel {

    /**
     * code : 1
     * msg : 企业基金支付成功
     * server_time : 2023-06-09 18:26:24
     * data : {"OrderNo":"20230609182530100283","EnterpriseFundAmount":"100","EnterpriseFundPay":"11.79","OnlineAmount":"0"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * OrderNo : 20230609182530100283
         * EnterpriseFundAmount : 100
         * EnterpriseFundPay : 11.79
         * OnlineAmount : 0
         */

        private String OrderNo;
        private String EnterpriseFundAmount;
        private String EnterpriseFundPay;
        private String OnlineAmount;

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getEnterpriseFundAmount() {
            return EnterpriseFundAmount;
        }

        public void setEnterpriseFundAmount(String EnterpriseFundAmount) {
            this.EnterpriseFundAmount = EnterpriseFundAmount;
        }

        public String getEnterpriseFundPay() {
            return EnterpriseFundPay;
        }

        public void setEnterpriseFundPay(String EnterpriseFundPay) {
            this.EnterpriseFundPay = EnterpriseFundPay;
        }

        public String getOnlineAmount() {
            return OnlineAmount;
        }

        public void setOnlineAmount(String OnlineAmount) {
            this.OnlineAmount = OnlineAmount;
        }
    }

    //企业基金支付
    public static void sendEnterprisePayRequest(final String TAG,String OrderNo,String PaymentPwd,final CustomerJsonCallBack<EnterprisePayModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);
        jsonObject.put("PaymentPwd", PaymentPwd);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ORDERENTERPRISEFUNDPAY_URL, jsonObject.toJSONString(), callback);
    }
    //企业基金支付--扫码支付
    public static void sendScanPayEnterprisePayRequest(final String TAG,String OrderNo,String PaymentPwd,final CustomerJsonCallBack<EnterprisePayModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);
        jsonObject.put("PaymentPwd", PaymentPwd);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_SCAN_ORDERENTERPRISEFUNDPAY_URL, jsonObject.toJSONString(), callback);
    }

}
