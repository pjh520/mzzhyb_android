package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-03
 * 描    述：
 * ================================================
 */
public class FreeBackPicModel implements MultiItemEntity {
    private String id;
    private String img;
    private int itemType;

    public FreeBackPicModel(String id, String img, int itemType) {
        this.id = id;
        this.img = img;
        this.itemType = itemType;
    }

    public String getId() {
        return id;
    }

    public String getImg() {
        return img;
    }

    @Override
    public int getItemType() {
        return itemType;
    }
}
