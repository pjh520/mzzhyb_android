package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-22
 * 描    述：
 * ================================================
 */
public class GoodDetailModel_user {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-22 16:42:23
     * data : {"InstructionsAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/983086cc-b8c9-4df3-a3de-5ba3fa9ed9d0/367f346e-6218-444e-a1c1-5130364c55b1.jpg","DetailAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/836c15c9-01e0-4533-bdd7-23da113abc5d/9777a79f-d274-4f83-964c-ce0995e1f8e9.jpg","DetailAccessoryUrl2":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/8a14a79b-1367-46bc-9d58-df95151b6fd8/6013b381-b80c-48e4-a563-698b3231fc12.jpg","DetailAccessoryUrl3":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/28b62992-72cb-4ed2-803e-7e4af24b938d/f18df8a9-dadd-4b87-a12d-1fad94d409cc.jpg","DetailAccessoryUrl4":"","DetailAccessoryUrl5":"","BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/cc9dae19-400f-4790-a302-22e69b867388/c9061f61-88b6-4df3-aba9-21d5b8b64546.jpg","BannerAccessoryUrl2":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/90d01f6b-7517-4308-a05d-d52103a34e34/4d671b41-f7d8-4538-9406-57d07848bd69.jpg","BannerAccessoryUrl3":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/9578035c-b24d-43b6-9e31-ad536f6fc55d/9da0ae8a-9672-4bb9-bc30-30b0cd06fba7.jpg","MonthlySales":100,"CatalogueName":"常备药","StoreName":"测试店铺","StoreAccessoryUrl":"~/App_Upload/Storage/Medicare_Ticket/4cf4e4f1-4d0c-486f-920a-baf79a13d9f3/25fb5b5e-057f-4cf2-a97a-7c86744c2d3d.jpg","Distance":0,"GoodsId":2,"MIC_Code":"XA01ABD075A002010100483","ItemCode":"XA01ABD075A002010100483","ItemName":"地喹氯铵含片","ItemType":1,"ApprovalNumber":"国药准字H20193280-1111","RegDosform":"胶囊剂","Spec":"100mg","EnterpriseName":"通药制药集团股份有限公司","EfccAtd":"功能主治1111","UsualWay":"常见用法1111","StorageConditions":"贮存条件1111","ExpiryDateCount":12,"InventoryQuantity":5,"Price":1,"InstructionsAccessoryId":"983086cc-b8c9-4df3-a3de-5ba3fa9ed9d0","DetailAccessoryId1":"836c15c9-01e0-4533-bdd7-23da113abc5d","DetailAccessoryId2":"8a14a79b-1367-46bc-9d58-df95151b6fd8","DetailAccessoryId3":"28b62992-72cb-4ed2-803e-7e4af24b938d","DetailAccessoryId4":"00000000-0000-0000-0000-000000000000","DetailAccessoryId5":"00000000-0000-0000-0000-000000000000","RegistrationCertificateNo":"","ProductionLicenseNo":"","Brand":"","Packaging":"","ApplicableScope":"","UsageDosage":"","Precautions":"","Usagemethod":"","IsPlatformDrug":1,"CreateDT":"2022-11-18 19:53:16","UpdateDT":"2022-11-18 19:53:16","CreateUser":"99","UpdateUser":"99","fixmedins_code":"H61080200145","GoodsLocation":1,"CatalogueId":1,"BannerAccessoryId1":"cc9dae19-400f-4790-a302-22e69b867388","BannerAccessoryId2":"90d01f6b-7517-4308-a05d-d52103a34e34","BannerAccessoryId3":"9578035c-b24d-43b6-9e31-ad536f6fc55d","Intervaltime":5,"DrugsClassification":1,"DrugsClassificationName":"西药中成药","IsPromotion":1,"GoodsName":"地喹氯铵含片","PromotionDiscount":9,"PreferentialPrice":0.9,"StoreSimilarity":"","IsOnSale":1,"AssociatedDiseases":"盗汗咳血肺结核，经证实的,","AssociatedDiagnostics":"[{\"choose\":true,\"code\":\"BF06900\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"脑损害和功能障碍及躯体疾病引起的精神障碍\"},{\"choose\":true,\"code\":\"BF20000\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"偏执型精神分裂症\"},{\"choose\":true,\"code\":\"BF20900\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"精神分裂症\"},{\"choose\":true,\"code\":\"BF25904\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"分裂情感性障碍(治疗期(第1-28天)稳定期(第29-90天)康复期(≥=91天))\"},{\"choose\":true,\"code\":\"BF31902\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"双相情感障碍(治疗期(第1-28天)稳定期(第29-90天)康复期(≥91天))\"}]","GoodsNo":"300000"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * InstructionsAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/983086cc-b8c9-4df3-a3de-5ba3fa9ed9d0/367f346e-6218-444e-a1c1-5130364c55b1.jpg
         * DetailAccessoryUrl1 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/836c15c9-01e0-4533-bdd7-23da113abc5d/9777a79f-d274-4f83-964c-ce0995e1f8e9.jpg
         * DetailAccessoryUrl2 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/8a14a79b-1367-46bc-9d58-df95151b6fd8/6013b381-b80c-48e4-a563-698b3231fc12.jpg
         * DetailAccessoryUrl3 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/28b62992-72cb-4ed2-803e-7e4af24b938d/f18df8a9-dadd-4b87-a12d-1fad94d409cc.jpg
         * DetailAccessoryUrl4 :
         * DetailAccessoryUrl5 :
         * BannerAccessoryUrl1 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/cc9dae19-400f-4790-a302-22e69b867388/c9061f61-88b6-4df3-aba9-21d5b8b64546.jpg
         * BannerAccessoryUrl2 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/90d01f6b-7517-4308-a05d-d52103a34e34/4d671b41-f7d8-4538-9406-57d07848bd69.jpg
         * BannerAccessoryUrl3 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/9578035c-b24d-43b6-9e31-ad536f6fc55d/9da0ae8a-9672-4bb9-bc30-30b0cd06fba7.jpg
         * MonthlySales : 100
         * CatalogueName : 常备药
         * StoreName : 测试店铺
         * StoreAccessoryUrl : ~/App_Upload/Storage/Medicare_Ticket/4cf4e4f1-4d0c-486f-920a-baf79a13d9f3/25fb5b5e-057f-4cf2-a97a-7c86744c2d3d.jpg
         * Distance : 0
         * GoodsId : 2
         * MIC_Code : XA01ABD075A002010100483
         * ItemCode : XA01ABD075A002010100483
         * ItemName : 地喹氯铵含片
         * ItemType : 1
         * ApprovalNumber : 国药准字H20193280-1111
         * RegDosform : 胶囊剂
         * Spec : 100mg
         * EnterpriseName : 通药制药集团股份有限公司
         * EfccAtd : 功能主治1111
         * UsualWay : 常见用法1111
         * StorageConditions : 贮存条件1111
         * ExpiryDateCount : 12
         * InventoryQuantity : 5
         * Price : 1
         * InstructionsAccessoryId : 983086cc-b8c9-4df3-a3de-5ba3fa9ed9d0
         * DetailAccessoryId1 : 836c15c9-01e0-4533-bdd7-23da113abc5d
         * DetailAccessoryId2 : 8a14a79b-1367-46bc-9d58-df95151b6fd8
         * DetailAccessoryId3 : 28b62992-72cb-4ed2-803e-7e4af24b938d
         * DetailAccessoryId4 : 00000000-0000-0000-0000-000000000000
         * DetailAccessoryId5 : 00000000-0000-0000-0000-000000000000
         * RegistrationCertificateNo :
         * ProductionLicenseNo :
         * Brand :
         * Packaging :
         * ApplicableScope :
         * UsageDosage :
         * Precautions :
         * Usagemethod :
         * IsPlatformDrug : 1
         * CreateDT : 2022-11-18 19:53:16
         * UpdateDT : 2022-11-18 19:53:16
         * CreateUser : 99
         * UpdateUser : 99
         * fixmedins_code : H61080200145
         * GoodsLocation : 1
         * CatalogueId : 1
         * BannerAccessoryId1 : cc9dae19-400f-4790-a302-22e69b867388
         * BannerAccessoryId2 : 90d01f6b-7517-4308-a05d-d52103a34e34
         * BannerAccessoryId3 : 9578035c-b24d-43b6-9e31-ad536f6fc55d
         * Intervaltime : 5
         * DrugsClassification : 1
         * DrugsClassificationName : 西药中成药
         * IsPromotion : 1
         * GoodsName : 地喹氯铵含片
         * PromotionDiscount : 9
         * PreferentialPrice : 0.9
         * StoreSimilarity :
         * IsOnSale : 1
         * AssociatedDiseases : 盗汗咳血肺结核，经证实的,
         * AssociatedDiagnostics : [{"choose":true,"code":"BF06900","diag_type":"1","diag_typename":"西医主要诊断","name":"脑损害和功能障碍及躯体疾病引起的精神障碍"},{"choose":true,"code":"BF20000","diag_type":"1","diag_typename":"西医主要诊断","name":"偏执型精神分裂症"},{"choose":true,"code":"BF20900","diag_type":"1","diag_typename":"西医主要诊断","name":"精神分裂症"},{"choose":true,"code":"BF25904","diag_type":"1","diag_typename":"西医主要诊断","name":"分裂情感性障碍(治疗期(第1-28天)稳定期(第29-90天)康复期(≥=91天))"},{"choose":true,"code":"BF31902","diag_type":"1","diag_typename":"西医主要诊断","name":"双相情感障碍(治疗期(第1-28天)稳定期(第29-90天)康复期(≥91天))"}]
         * GoodsNo : 300000
         */

        private String InstructionsAccessoryUrl;
        private String DetailAccessoryUrl1;
        private String DetailAccessoryUrl2;
        private String DetailAccessoryUrl3;
        private String DetailAccessoryUrl4;
        private String DetailAccessoryUrl5;
        private String BannerAccessoryUrl1;
        private String BannerAccessoryUrl2;
        private String BannerAccessoryUrl3;
        private String MonthlySales;
        private String CatalogueName;
        private String StoreName;
        private String StoreAccessoryUrl;
        private String Distance;
        private String GoodsId;
        private String MIC_Code;
        private String ItemCode;
        private String ItemName;
        private String ItemType;
        private String ApprovalNumber;
        private String RegDosform;
        private String Spec;
        private String EnterpriseName;
        private String EfccAtd;
        private String UsualWay;
        private String StorageConditions;
        private String ExpiryDateCount;
        private int InventoryQuantity;
        private String Price;
        private String InstructionsAccessoryId;
        private String DetailAccessoryId1;
        private String DetailAccessoryId2;
        private String DetailAccessoryId3;
        private String DetailAccessoryId4;
        private String DetailAccessoryId5;
        private String RegistrationCertificateNo;
        private String ProductionLicenseNo;
        private String Brand;
        private String Packaging;
        private String ApplicableScope;
        private String UsageDosage;
        private String Precautions;
        private String Usagemethod;
        private String IsPlatformDrug;
        private String CreateDT;
        private String UpdateDT;
        private String CreateUser;
        private String UpdateUser;
        private String fixmedins_code;
        private String GoodsLocation;
        private String CatalogueId;
        private String BannerAccessoryId1;
        private String BannerAccessoryId2;
        private String BannerAccessoryId3;
        private String Intervaltime;
        private String DrugsClassification;
        private String DrugsClassificationName;
        private String IsPromotion;
        private String GoodsName;
        private String PromotionDiscount;
        private String PreferentialPrice;
        private String StoreSimilarity;
        private String IsOnSale;
        private String AssociatedDiseases;
        private String AssociatedDiagnostics;
        private String GoodsNo;
        private String MonthlyStoreSales;
        private int ShoppingCartNum;
        private String Telephone;
        private String StartingPrice;
        private String IsCollection;
        private String Latitude;
        private String Longitude;
        private String fixmedins_type;
        private String IsShowGoodsSold;
        private String IsShowSold;
        private String IsShowMargin;

        public String getInstructionsAccessoryUrl() {
            return InstructionsAccessoryUrl;
        }

        public void setInstructionsAccessoryUrl(String InstructionsAccessoryUrl) {
            this.InstructionsAccessoryUrl = InstructionsAccessoryUrl;
        }

        public String getDetailAccessoryUrl1() {
            return DetailAccessoryUrl1;
        }

        public void setDetailAccessoryUrl1(String DetailAccessoryUrl1) {
            this.DetailAccessoryUrl1 = DetailAccessoryUrl1;
        }

        public String getDetailAccessoryUrl2() {
            return DetailAccessoryUrl2;
        }

        public void setDetailAccessoryUrl2(String DetailAccessoryUrl2) {
            this.DetailAccessoryUrl2 = DetailAccessoryUrl2;
        }

        public String getDetailAccessoryUrl3() {
            return DetailAccessoryUrl3;
        }

        public void setDetailAccessoryUrl3(String DetailAccessoryUrl3) {
            this.DetailAccessoryUrl3 = DetailAccessoryUrl3;
        }

        public String getDetailAccessoryUrl4() {
            return DetailAccessoryUrl4;
        }

        public void setDetailAccessoryUrl4(String DetailAccessoryUrl4) {
            this.DetailAccessoryUrl4 = DetailAccessoryUrl4;
        }

        public String getDetailAccessoryUrl5() {
            return DetailAccessoryUrl5;
        }

        public void setDetailAccessoryUrl5(String DetailAccessoryUrl5) {
            this.DetailAccessoryUrl5 = DetailAccessoryUrl5;
        }

        public String getBannerAccessoryUrl1() {
            return BannerAccessoryUrl1;
        }

        public void setBannerAccessoryUrl1(String BannerAccessoryUrl1) {
            this.BannerAccessoryUrl1 = BannerAccessoryUrl1;
        }

        public String getBannerAccessoryUrl2() {
            return BannerAccessoryUrl2;
        }

        public void setBannerAccessoryUrl2(String BannerAccessoryUrl2) {
            this.BannerAccessoryUrl2 = BannerAccessoryUrl2;
        }

        public String getBannerAccessoryUrl3() {
            return BannerAccessoryUrl3;
        }

        public void setBannerAccessoryUrl3(String BannerAccessoryUrl3) {
            this.BannerAccessoryUrl3 = BannerAccessoryUrl3;
        }

        public String getMonthlySales() {
            return MonthlySales;
        }

        public void setMonthlySales(String MonthlySales) {
            this.MonthlySales = MonthlySales;
        }

        public String getCatalogueName() {
            return CatalogueName;
        }

        public void setCatalogueName(String CatalogueName) {
            this.CatalogueName = CatalogueName;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public String getStoreAccessoryUrl() {
            return StoreAccessoryUrl;
        }

        public void setStoreAccessoryUrl(String StoreAccessoryUrl) {
            this.StoreAccessoryUrl = StoreAccessoryUrl;
        }

        public String getDistance() {
            return Distance;
        }

        public void setDistance(String Distance) {
            this.Distance = Distance;
        }

        public String getGoodsId() {
            return GoodsId;
        }

        public void setGoodsId(String GoodsId) {
            this.GoodsId = GoodsId;
        }

        public String getMIC_Code() {
            return MIC_Code;
        }

        public void setMIC_Code(String MIC_Code) {
            this.MIC_Code = MIC_Code;
        }

        public String getItemCode() {
            return ItemCode;
        }

        public void setItemCode(String ItemCode) {
            this.ItemCode = ItemCode;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String ItemName) {
            this.ItemName = ItemName;
        }

        public String getItemType() {
            return ItemType;
        }

        public void setItemType(String ItemType) {
            this.ItemType = ItemType;
        }

        public String getApprovalNumber() {
            return ApprovalNumber;
        }

        public void setApprovalNumber(String ApprovalNumber) {
            this.ApprovalNumber = ApprovalNumber;
        }

        public String getRegDosform() {
            return RegDosform;
        }

        public void setRegDosform(String RegDosform) {
            this.RegDosform = RegDosform;
        }

        public String getSpec() {
            return Spec;
        }

        public void setSpec(String Spec) {
            this.Spec = Spec;
        }

        public String getEnterpriseName() {
            return EnterpriseName;
        }

        public void setEnterpriseName(String EnterpriseName) {
            this.EnterpriseName = EnterpriseName;
        }

        public String getEfccAtd() {
            return EfccAtd;
        }

        public void setEfccAtd(String EfccAtd) {
            this.EfccAtd = EfccAtd;
        }

        public String getUsualWay() {
            return UsualWay;
        }

        public void setUsualWay(String UsualWay) {
            this.UsualWay = UsualWay;
        }

        public String getStorageConditions() {
            return StorageConditions;
        }

        public void setStorageConditions(String StorageConditions) {
            this.StorageConditions = StorageConditions;
        }

        public String getExpiryDateCount() {
            return ExpiryDateCount;
        }

        public void setExpiryDateCount(String ExpiryDateCount) {
            this.ExpiryDateCount = ExpiryDateCount;
        }

        public int getInventoryQuantity() {
            return InventoryQuantity;
        }

        public void setInventoryQuantity(int InventoryQuantity) {
            this.InventoryQuantity = InventoryQuantity;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String Price) {
            this.Price = Price;
        }

        public String getInstructionsAccessoryId() {
            return InstructionsAccessoryId;
        }

        public void setInstructionsAccessoryId(String InstructionsAccessoryId) {
            this.InstructionsAccessoryId = InstructionsAccessoryId;
        }

        public String getDetailAccessoryId1() {
            return DetailAccessoryId1;
        }

        public void setDetailAccessoryId1(String DetailAccessoryId1) {
            this.DetailAccessoryId1 = DetailAccessoryId1;
        }

        public String getDetailAccessoryId2() {
            return DetailAccessoryId2;
        }

        public void setDetailAccessoryId2(String DetailAccessoryId2) {
            this.DetailAccessoryId2 = DetailAccessoryId2;
        }

        public String getDetailAccessoryId3() {
            return DetailAccessoryId3;
        }

        public void setDetailAccessoryId3(String DetailAccessoryId3) {
            this.DetailAccessoryId3 = DetailAccessoryId3;
        }

        public String getDetailAccessoryId4() {
            return DetailAccessoryId4;
        }

        public void setDetailAccessoryId4(String DetailAccessoryId4) {
            this.DetailAccessoryId4 = DetailAccessoryId4;
        }

        public String getDetailAccessoryId5() {
            return DetailAccessoryId5;
        }

        public void setDetailAccessoryId5(String DetailAccessoryId5) {
            this.DetailAccessoryId5 = DetailAccessoryId5;
        }

        public String getRegistrationCertificateNo() {
            return RegistrationCertificateNo;
        }

        public void setRegistrationCertificateNo(String RegistrationCertificateNo) {
            this.RegistrationCertificateNo = RegistrationCertificateNo;
        }

        public String getProductionLicenseNo() {
            return ProductionLicenseNo;
        }

        public void setProductionLicenseNo(String ProductionLicenseNo) {
            this.ProductionLicenseNo = ProductionLicenseNo;
        }

        public String getBrand() {
            return Brand;
        }

        public void setBrand(String Brand) {
            this.Brand = Brand;
        }

        public String getPackaging() {
            return Packaging;
        }

        public void setPackaging(String Packaging) {
            this.Packaging = Packaging;
        }

        public String getApplicableScope() {
            return ApplicableScope;
        }

        public void setApplicableScope(String ApplicableScope) {
            this.ApplicableScope = ApplicableScope;
        }

        public String getUsageDosage() {
            return UsageDosage;
        }

        public void setUsageDosage(String UsageDosage) {
            this.UsageDosage = UsageDosage;
        }

        public String getPrecautions() {
            return Precautions;
        }

        public void setPrecautions(String Precautions) {
            this.Precautions = Precautions;
        }

        public String getUsagemethod() {
            return Usagemethod;
        }

        public void setUsagemethod(String Usagemethod) {
            this.Usagemethod = Usagemethod;
        }

        public String getIsPlatformDrug() {
            return IsPlatformDrug;
        }

        public void setIsPlatformDrug(String IsPlatformDrug) {
            this.IsPlatformDrug = IsPlatformDrug;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getUpdateDT() {
            return UpdateDT;
        }

        public void setUpdateDT(String UpdateDT) {
            this.UpdateDT = UpdateDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String CreateUser) {
            this.CreateUser = CreateUser;
        }

        public String getUpdateUser() {
            return UpdateUser;
        }

        public void setUpdateUser(String UpdateUser) {
            this.UpdateUser = UpdateUser;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getGoodsLocation() {
            return GoodsLocation;
        }

        public void setGoodsLocation(String GoodsLocation) {
            this.GoodsLocation = GoodsLocation;
        }

        public String getCatalogueId() {
            return CatalogueId;
        }

        public void setCatalogueId(String CatalogueId) {
            this.CatalogueId = CatalogueId;
        }

        public String getBannerAccessoryId1() {
            return BannerAccessoryId1;
        }

        public void setBannerAccessoryId1(String BannerAccessoryId1) {
            this.BannerAccessoryId1 = BannerAccessoryId1;
        }

        public String getBannerAccessoryId2() {
            return BannerAccessoryId2;
        }

        public void setBannerAccessoryId2(String BannerAccessoryId2) {
            this.BannerAccessoryId2 = BannerAccessoryId2;
        }

        public String getBannerAccessoryId3() {
            return BannerAccessoryId3;
        }

        public void setBannerAccessoryId3(String BannerAccessoryId3) {
            this.BannerAccessoryId3 = BannerAccessoryId3;
        }

        public String getIntervaltime() {
            return Intervaltime;
        }

        public void setIntervaltime(String Intervaltime) {
            this.Intervaltime = Intervaltime;
        }

        public String getDrugsClassification() {
            return DrugsClassification;
        }

        public void setDrugsClassification(String DrugsClassification) {
            this.DrugsClassification = DrugsClassification;
        }

        public String getDrugsClassificationName() {
            return DrugsClassificationName;
        }

        public void setDrugsClassificationName(String DrugsClassificationName) {
            this.DrugsClassificationName = DrugsClassificationName;
        }

        public String getIsPromotion() {
            return IsPromotion;
        }

        public void setIsPromotion(String IsPromotion) {
            this.IsPromotion = IsPromotion;
        }

        public String getGoodsName() {
            return GoodsName;
        }

        public void setGoodsName(String GoodsName) {
            this.GoodsName = GoodsName;
        }

        public String getPromotionDiscount() {
            return PromotionDiscount;
        }

        public void setPromotionDiscount(String PromotionDiscount) {
            this.PromotionDiscount = PromotionDiscount;
        }

        public String getPreferentialPrice() {
            return PreferentialPrice;
        }

        public void setPreferentialPrice(String PreferentialPrice) {
            this.PreferentialPrice = PreferentialPrice;
        }

        public String getStoreSimilarity() {
            return StoreSimilarity;
        }

        public void setStoreSimilarity(String StoreSimilarity) {
            this.StoreSimilarity = StoreSimilarity;
        }

        public String getIsOnSale() {
            return IsOnSale;
        }

        public void setIsOnSale(String IsOnSale) {
            this.IsOnSale = IsOnSale;
        }

        public String getAssociatedDiseases() {
            return AssociatedDiseases;
        }

        public void setAssociatedDiseases(String AssociatedDiseases) {
            this.AssociatedDiseases = AssociatedDiseases;
        }

        public String getAssociatedDiagnostics() {
            return AssociatedDiagnostics;
        }

        public void setAssociatedDiagnostics(String AssociatedDiagnostics) {
            this.AssociatedDiagnostics = AssociatedDiagnostics;
        }

        public String getGoodsNo() {
            return GoodsNo;
        }

        public void setGoodsNo(String GoodsNo) {
            this.GoodsNo = GoodsNo;
        }

        public String getMonthlyStoreSales() {
            return MonthlyStoreSales;
        }

        public void setMonthlyStoreSales(String monthlyStoreSales) {
            MonthlyStoreSales = monthlyStoreSales;
        }

        public int getShoppingCartNum() {
            return ShoppingCartNum;
        }

        public void setShoppingCartNum(int shoppingCartNum) {
            ShoppingCartNum = shoppingCartNum;
        }

        public String getTelephone() {
            return Telephone;
        }

        public void setTelephone(String telephone) {
            Telephone = telephone;
        }

        public String getStartingPrice() {
            return StartingPrice;
        }

        public void setStartingPrice(String startingPrice) {
            StartingPrice = startingPrice;
        }

        public String getIsCollection() {
            return IsCollection;
        }

        public void setIsCollection(String isCollection) {
            IsCollection = isCollection;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String latitude) {
            Latitude = latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String longitude) {
            Longitude = longitude;
        }

        public String getFixmedins_type() {
            return fixmedins_type;
        }

        public void setFixmedins_type(String fixmedins_type) {
            this.fixmedins_type = fixmedins_type;
        }

        public String getIsShowGoodsSold() {
            return IsShowGoodsSold;
        }

        public void setIsShowGoodsSold(String isShowGoodsSold) {
            IsShowGoodsSold = isShowGoodsSold;
        }

        public String getIsShowSold() {
            return IsShowSold;
        }

        public void setIsShowSold(String isShowSold) {
            IsShowSold = isShowSold;
        }

        public String getIsShowMargin() {
            return IsShowMargin;
        }

        public void setIsShowMargin(String isShowMargin) {
            IsShowMargin = isShowMargin;
        }
    }

    //获取商品详情
    public static void sendGoodDetailRequest_user(final String TAG, String GoodsNo, String Longitude, String Latitude,
                                                  final CustomerJsonCallBack<GoodDetailModel_user> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("GoodsNo", GoodsNo);//商品编码
        jsonObject.put("Longitude", Longitude);//经度
        jsonObject.put("Latitude", Latitude);//纬度
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_STOREGOODSDETAIL_URL, jsonObject.toJSONString(), callback);
    }
}
