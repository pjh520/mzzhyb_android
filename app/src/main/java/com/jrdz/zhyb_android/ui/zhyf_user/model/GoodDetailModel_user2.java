package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-22
 * 描    述：
 * ================================================
 */
public class GoodDetailModel_user2 {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-22 16:42:23
     * data : {"InstructionsAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/983086cc-b8c9-4df3-a3de-5ba3fa9ed9d0/367f346e-6218-444e-a1c1-5130364c55b1.jpg","DetailAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/836c15c9-01e0-4533-bdd7-23da113abc5d/9777a79f-d274-4f83-964c-ce0995e1f8e9.jpg","DetailAccessoryUrl2":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/8a14a79b-1367-46bc-9d58-df95151b6fd8/6013b381-b80c-48e4-a563-698b3231fc12.jpg","DetailAccessoryUrl3":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/28b62992-72cb-4ed2-803e-7e4af24b938d/f18df8a9-dadd-4b87-a12d-1fad94d409cc.jpg","DetailAccessoryUrl4":"","DetailAccessoryUrl5":"","BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/cc9dae19-400f-4790-a302-22e69b867388/c9061f61-88b6-4df3-aba9-21d5b8b64546.jpg","BannerAccessoryUrl2":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/90d01f6b-7517-4308-a05d-d52103a34e34/4d671b41-f7d8-4538-9406-57d07848bd69.jpg","BannerAccessoryUrl3":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/9578035c-b24d-43b6-9e31-ad536f6fc55d/9da0ae8a-9672-4bb9-bc30-30b0cd06fba7.jpg","MonthlySales":100,"CatalogueName":"常备药","StoreName":"测试店铺","StoreAccessoryUrl":"~/App_Upload/Storage/Medicare_Ticket/4cf4e4f1-4d0c-486f-920a-baf79a13d9f3/25fb5b5e-057f-4cf2-a97a-7c86744c2d3d.jpg","Distance":0,"GoodsId":2,"MIC_Code":"XA01ABD075A002010100483","ItemCode":"XA01ABD075A002010100483","ItemName":"地喹氯铵含片","ItemType":1,"ApprovalNumber":"国药准字H20193280-1111","RegDosform":"胶囊剂","Spec":"100mg","EnterpriseName":"通药制药集团股份有限公司","EfccAtd":"功能主治1111","UsualWay":"常见用法1111","StorageConditions":"贮存条件1111","ExpiryDateCount":12,"InventoryQuantity":5,"Price":1,"InstructionsAccessoryId":"983086cc-b8c9-4df3-a3de-5ba3fa9ed9d0","DetailAccessoryId1":"836c15c9-01e0-4533-bdd7-23da113abc5d","DetailAccessoryId2":"8a14a79b-1367-46bc-9d58-df95151b6fd8","DetailAccessoryId3":"28b62992-72cb-4ed2-803e-7e4af24b938d","DetailAccessoryId4":"00000000-0000-0000-0000-000000000000","DetailAccessoryId5":"00000000-0000-0000-0000-000000000000","RegistrationCertificateNo":"","ProductionLicenseNo":"","Brand":"","Packaging":"","ApplicableScope":"","UsageDosage":"","Precautions":"","Usagemethod":"","IsPlatformDrug":1,"CreateDT":"2022-11-18 19:53:16","UpdateDT":"2022-11-18 19:53:16","CreateUser":"99","UpdateUser":"99","fixmedins_code":"H61080200145","GoodsLocation":1,"CatalogueId":1,"BannerAccessoryId1":"cc9dae19-400f-4790-a302-22e69b867388","BannerAccessoryId2":"90d01f6b-7517-4308-a05d-d52103a34e34","BannerAccessoryId3":"9578035c-b24d-43b6-9e31-ad536f6fc55d","Intervaltime":5,"DrugsClassification":1,"DrugsClassificationName":"西药中成药","IsPromotion":1,"GoodsName":"地喹氯铵含片","PromotionDiscount":9,"PreferentialPrice":0.9,"StoreSimilarity":"","IsOnSale":1,"AssociatedDiseases":"盗汗咳血肺结核，经证实的,","AssociatedDiagnostics":"[{\"choose\":true,\"code\":\"BF06900\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"脑损害和功能障碍及躯体疾病引起的精神障碍\"},{\"choose\":true,\"code\":\"BF20000\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"偏执型精神分裂症\"},{\"choose\":true,\"code\":\"BF20900\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"精神分裂症\"},{\"choose\":true,\"code\":\"BF25904\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"分裂情感性障碍(治疗期(第1-28天)稳定期(第29-90天)康复期(≥=91天))\"},{\"choose\":true,\"code\":\"BF31902\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"双相情感障碍(治疗期(第1-28天)稳定期(第29-90天)康复期(≥91天))\"}]","GoodsNo":"300000"}
     */

    private String code;
    private String msg;
    private String server_time;
    private GoodDetailModel_user.DataBean data;
    private ArrayList<DataBean1> data1;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public GoodDetailModel_user.DataBean getData() {
        return data;
    }

    public void setData(GoodDetailModel_user.DataBean data) {
        this.data = data;
    }

    public ArrayList<DataBean1> getData1() {
        return data1;
    }

    public void setData1(ArrayList<DataBean1> data1) {
        this.data1 = data1;
    }

    public static class DataBean1{
        /**
         * GoodsId : 39
         * MIC_Code : ZD02AAF0188010101951
         * ItemCode : ZD02AAF0188010101951
         * ItemType : 1
         * EfccAtd : 健脾舒肝，除湿止带。用于脾虚湿盛，白带量多，腰腿酸痛。
         * InventoryQuantity : 10
         * Price : 1
         * IsPlatformDrug : 1
         * fixmedins_code : H61080300942
         * GoodsLocation : 1
         * CatalogueId : 3
         * DrugsClassification : 1
         * DrugsClassificationName : 西药中成药
         * IsPromotion : 1
         * GoodsName : 妇科白带膏
         * PromotionDiscount : 9
         * PreferentialPrice : 0.9
         * IsOnSale : 1
         * GoodsNo : 300084
         * MonthlySales : 0
         * CatalogueName : 防护药品
         * Distance : 0
         * ShoppingCartNum : 0
         * MonthlyStoreSales : 0
         * Spec : 120g
         * StoreName : 横山区塔湾镇小豆湾村卫生所
         * StartingPrice : 0.01
         * StarLevel : 5
         * IsOline : 1
         * IsEnterpriseFundPay : 1
         * IsShowSold : 2
         * IsShowGoodsSold : 1
         * IsShowMargin : 1
         */

        private int GoodsId;
        private String MIC_Code;
        private String ItemCode;
        private String ItemType;
        private String EfccAtd;
        private int InventoryQuantity;
        private String Price;
        private String IsPlatformDrug;
        private String fixmedins_code;
        private String GoodsLocation;
        private String CatalogueId;
        private String DrugsClassification;
        private String DrugsClassificationName;
        private String IsPromotion;
        private String GoodsName;
        private String PromotionDiscount;
        private String PreferentialPrice;
        private String IsOnSale;
        private String GoodsNo;
        private String MonthlySales;
        private String CatalogueName;
        private String Distance;
        private int ShoppingCartNum;
        private String MonthlyStoreSales;
        private String Spec;
        private String StoreName;
        private String StartingPrice;
        private String StarLevel;
        private String IsOline;
        private String IsEnterpriseFundPay;
        private String IsShowSold;
        private String IsShowGoodsSold;
        private String IsShowMargin;
        private String AssociatedDiseases;
        private String AssociatedDiagnostics;
        private String fixmedins_type;
        private String Latitude;
        private String Longitude;
        private String StoreAccessoryUrl;
        private String DistributionMethod;
        private String BannerAccessoryId1;
        private String BannerAccessoryUrl1;

        private boolean isChoose=false;

        public int getGoodsId() {
            return GoodsId;
        }

        public void setGoodsId(int GoodsId) {
            this.GoodsId = GoodsId;
        }

        public String getMIC_Code() {
            return MIC_Code;
        }

        public void setMIC_Code(String MIC_Code) {
            this.MIC_Code = MIC_Code;
        }

        public String getItemCode() {
            return ItemCode;
        }

        public void setItemCode(String ItemCode) {
            this.ItemCode = ItemCode;
        }

        public String getItemType() {
            return ItemType;
        }

        public void setItemType(String ItemType) {
            this.ItemType = ItemType;
        }

        public String getEfccAtd() {
            return EfccAtd;
        }

        public void setEfccAtd(String EfccAtd) {
            this.EfccAtd = EfccAtd;
        }

        public int getInventoryQuantity() {
            return InventoryQuantity;
        }

        public void setInventoryQuantity(int InventoryQuantity) {
            this.InventoryQuantity = InventoryQuantity;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String Price) {
            this.Price = Price;
        }

        public String getIsPlatformDrug() {
            return IsPlatformDrug;
        }

        public void setIsPlatformDrug(String IsPlatformDrug) {
            this.IsPlatformDrug = IsPlatformDrug;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getGoodsLocation() {
            return GoodsLocation;
        }

        public void setGoodsLocation(String GoodsLocation) {
            this.GoodsLocation = GoodsLocation;
        }

        public String getCatalogueId() {
            return CatalogueId;
        }

        public void setCatalogueId(String CatalogueId) {
            this.CatalogueId = CatalogueId;
        }

        public String getDrugsClassification() {
            return DrugsClassification;
        }

        public void setDrugsClassification(String DrugsClassification) {
            this.DrugsClassification = DrugsClassification;
        }

        public String getDrugsClassificationName() {
            return DrugsClassificationName;
        }

        public void setDrugsClassificationName(String DrugsClassificationName) {
            this.DrugsClassificationName = DrugsClassificationName;
        }

        public String getIsPromotion() {
            return IsPromotion;
        }

        public void setIsPromotion(String IsPromotion) {
            this.IsPromotion = IsPromotion;
        }

        public String getGoodsName() {
            return GoodsName;
        }

        public void setGoodsName(String GoodsName) {
            this.GoodsName = GoodsName;
        }

        public String getPromotionDiscount() {
            return PromotionDiscount;
        }

        public void setPromotionDiscount(String PromotionDiscount) {
            this.PromotionDiscount = PromotionDiscount;
        }

        public String getPreferentialPrice() {
            return PreferentialPrice;
        }

        public void setPreferentialPrice(String PreferentialPrice) {
            this.PreferentialPrice = PreferentialPrice;
        }

        public String getIsOnSale() {
            return IsOnSale;
        }

        public void setIsOnSale(String IsOnSale) {
            this.IsOnSale = IsOnSale;
        }

        public String getGoodsNo() {
            return GoodsNo;
        }

        public void setGoodsNo(String GoodsNo) {
            this.GoodsNo = GoodsNo;
        }

        public String getMonthlySales() {
            return MonthlySales;
        }

        public void setMonthlySales(String MonthlySales) {
            this.MonthlySales = MonthlySales;
        }

        public String getCatalogueName() {
            return CatalogueName;
        }

        public void setCatalogueName(String CatalogueName) {
            this.CatalogueName = CatalogueName;
        }

        public String getDistance() {
            return Distance;
        }

        public void setDistance(String Distance) {
            this.Distance = Distance;
        }

        public int getShoppingCartNum() {
            return ShoppingCartNum;
        }

        public void setShoppingCartNum(int ShoppingCartNum) {
            this.ShoppingCartNum = ShoppingCartNum;
        }

        public String getMonthlyStoreSales() {
            return MonthlyStoreSales;
        }

        public void setMonthlyStoreSales(String MonthlyStoreSales) {
            this.MonthlyStoreSales = MonthlyStoreSales;
        }

        public String getSpec() {
            return Spec;
        }

        public void setSpec(String Spec) {
            this.Spec = Spec;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public String getStartingPrice() {
            return StartingPrice;
        }

        public void setStartingPrice(String StartingPrice) {
            this.StartingPrice = StartingPrice;
        }

        public String getStarLevel() {
            return StarLevel;
        }

        public void setStarLevel(String StarLevel) {
            this.StarLevel = StarLevel;
        }

        public String getIsOline() {
            return IsOline;
        }

        public void setIsOline(String IsOline) {
            this.IsOline = IsOline;
        }

        public String getIsEnterpriseFundPay() {
            return IsEnterpriseFundPay;
        }

        public void setIsEnterpriseFundPay(String IsEnterpriseFundPay) {
            this.IsEnterpriseFundPay = IsEnterpriseFundPay;
        }

        public String getIsShowSold() {
            return IsShowSold;
        }

        public void setIsShowSold(String IsShowSold) {
            this.IsShowSold = IsShowSold;
        }

        public String getIsShowGoodsSold() {
            return IsShowGoodsSold;
        }

        public void setIsShowGoodsSold(String IsShowGoodsSold) {
            this.IsShowGoodsSold = IsShowGoodsSold;
        }

        public String getIsShowMargin() {
            return IsShowMargin;
        }

        public void setIsShowMargin(String IsShowMargin) {
            this.IsShowMargin = IsShowMargin;
        }

        public String getAssociatedDiseases() {
            return AssociatedDiseases;
        }

        public void setAssociatedDiseases(String associatedDiseases) {
            AssociatedDiseases = associatedDiseases;
        }

        public String getAssociatedDiagnostics() {
            return AssociatedDiagnostics;
        }

        public void setAssociatedDiagnostics(String associatedDiagnostics) {
            AssociatedDiagnostics = associatedDiagnostics;
        }

        public String getFixmedins_type() {
            return fixmedins_type;
        }

        public void setFixmedins_type(String fixmedins_type) {
            this.fixmedins_type = fixmedins_type;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String latitude) {
            Latitude = latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String longitude) {
            Longitude = longitude;
        }

        public String getStoreAccessoryUrl() {
            return StoreAccessoryUrl;
        }

        public void setStoreAccessoryUrl(String storeAccessoryUrl) {
            StoreAccessoryUrl = storeAccessoryUrl;
        }

        public String getDistributionMethod() {
            return DistributionMethod;
        }

        public void setDistributionMethod(String distributionMethod) {
            DistributionMethod = distributionMethod;
        }

        public String getBannerAccessoryId1() {
            return BannerAccessoryId1;
        }

        public void setBannerAccessoryId1(String bannerAccessoryId1) {
            BannerAccessoryId1 = bannerAccessoryId1;
        }

        public String getBannerAccessoryUrl1() {
            return BannerAccessoryUrl1;
        }

        public void setBannerAccessoryUrl1(String bannerAccessoryUrl1) {
            BannerAccessoryUrl1 = bannerAccessoryUrl1;
        }

        public boolean isChoose() {
            return isChoose;
        }

        public void setChoose(boolean choose) {
            isChoose = choose;
        }


    }

    //获取商品详情（带商家药品列表）
    public static void sendGoodDetailRequest_user2(final String TAG, String GoodsNo, String Longitude, String Latitude,String name,
                                                  final CustomerJsonCallBack<GoodDetailModel_user2> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("GoodsNo", GoodsNo);//商品编码
        jsonObject.put("Longitude", Longitude);//经度
        jsonObject.put("Latitude", Latitude);//纬度
        jsonObject.put("name", name);//搜索
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_USERGOODSDETAIL_URL, jsonObject.toJSONString(), callback);
    }

}
