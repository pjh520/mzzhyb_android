package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.flyjingfish.openimagelib.beans.OpenImageUrl;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.widget.banner.listener.BannerModelCallBack;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-22
 * 描    述：
 * ================================================
 */
public class GoodsBannerModel implements BannerModelCallBack<String>,OpenImageUrl {
    private String imgurl;

    public GoodsBannerModel(String imgurl) {
        this.imgurl = imgurl;
    }

    @Override
    public String getBannerUrl() {
        return imgurl;
    }

    @Override
    public String getBannerTitle() {
        return "";
    }

    /* =================== 实现大图预览     ==================== */
    @Override
    public MediaType getType() {
        return MediaType.IMAGE;
    }

    @Override
    public String getImageUrl() {
        return imgurl;
    }

    @Override
    public String getVideoUrl() {
        return "";
    }

    @Override
    public String getCoverImageUrl() {
        return imgurl;
    }
}
