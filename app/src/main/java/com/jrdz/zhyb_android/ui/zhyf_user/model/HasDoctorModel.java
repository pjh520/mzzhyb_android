package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-19
 * 描    述：
 * ================================================
 */
public class HasDoctorModel {

    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-12-19 13:42:49
     * data : {"IsDoctor":"1"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * IsDoctor : 1
         */

        private String IsDoctor;//是否有执业医师(0没有1有）

        public String getIsDoctor() {
            return IsDoctor;
        }

        public void setIsDoctor(String IsDoctor) {
            this.IsDoctor = IsDoctor;
        }
    }

    //判断机构是否有医师执业执照
    public static void sendHasDoctorRequest(final String TAG,String fixmedins_code,final CustomerJsonCallBack<HasDoctorModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fixmedins_code", fixmedins_code);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ISDOCTOR_URL, jsonObject.toJSONString(), callback);
    }
}
