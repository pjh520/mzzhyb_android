package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-04-01
 * 描    述：
 * ================================================
 */
public class InquiryInfoModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2024-04-01 16:32:12
     * data : {"AssociatedDiseases1":"{\"etFamily\":\"\",\"selectFamily\":\"\",\"isSelectPastMedical\":\"1\",\"isSelectAllergy\":\"1\",\"selectPastMedical\":\"冠心病;慢性阻塞性肺病;寻麻疹\",\"selectAllergy\":\"\",\"etAllergy\":\"\",\"isSelectKidney\":\"0\",\"isSelectLiver\":\"1\",\"isSelectFamily\":\"0\",\"etPastMedical\":\"\",\"isSelectPregnancyLactation\":\"0\"}","MedicalRecordAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/80f164e0-5153-4c44-9d89-803e06787d35/dc218529-5fe2-45d1-80d1-17331b384af1.jpg","MedicalRecordAccessoryUrl2":"","MedicalRecordAccessoryUrl3":"","MedicalRecordAccessoryUrl4":"","MedicalRecordAccessoryUrl5":"","OnlineInquiryId":25,"psn_name":"宋怀春","mdtrt_cert_no":"612726196609210011","sex":"1","age":"57","AssociatedDiseases":"感冒","IsTake":1,"IsAllergy":0,"IsUntowardReaction":0,"Symptom":"测试过","CreateDT":"2024-04-01 16:31:49","UpdateDT":"2024-04-01 16:31:49","CreateUser":"15060338985","UpdateUser":"15060338985","UserId":"dc6b1e1a-c23b-46a1-b2bc-61ad068a42fd","UserName":"15060338985","fixmedins_code":"","OrderNo":"20240401163114101131","MedicalRecordAccessory1":"80f164e0-5153-4c44-9d89-803e06787d35","MedicalRecordAccessory2":"00000000-0000-0000-0000-000000000000","MedicalRecordAccessory3":"00000000-0000-0000-0000-000000000000","MedicalRecordAccessory4":"00000000-0000-0000-0000-000000000000","MedicalRecordAccessory5":"00000000-0000-0000-0000-000000000000","IsDoctor":0,"IsPrescription":3,"dr_code":"D610803001453","dr_name":"陈子平","IsThirdPartyPres":0}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * AssociatedDiseases1 : {"etFamily":"","selectFamily":"","isSelectPastMedical":"1","isSelectAllergy":"1","selectPastMedical":"冠心病;慢性阻塞性肺病;寻麻疹","selectAllergy":"","etAllergy":"","isSelectKidney":"0","isSelectLiver":"1","isSelectFamily":"0","etPastMedical":"","isSelectPregnancyLactation":"0"}
         * MedicalRecordAccessoryUrl1 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/80f164e0-5153-4c44-9d89-803e06787d35/dc218529-5fe2-45d1-80d1-17331b384af1.jpg
         * MedicalRecordAccessoryUrl2 :
         * MedicalRecordAccessoryUrl3 :
         * MedicalRecordAccessoryUrl4 :
         * MedicalRecordAccessoryUrl5 :
         * OnlineInquiryId : 25
         * psn_name : 宋怀春
         * mdtrt_cert_no : 612726196609210011
         * sex : 1
         * age : 57
         * AssociatedDiseases : 感冒
         * IsTake : 1
         * IsAllergy : 0
         * IsUntowardReaction : 0
         * Symptom : 测试过
         * CreateDT : 2024-04-01 16:31:49
         * UpdateDT : 2024-04-01 16:31:49
         * CreateUser : 15060338985
         * UpdateUser : 15060338985
         * UserId : dc6b1e1a-c23b-46a1-b2bc-61ad068a42fd
         * UserName : 15060338985
         * fixmedins_code :
         * OrderNo : 20240401163114101131
         * MedicalRecordAccessory1 : 80f164e0-5153-4c44-9d89-803e06787d35
         * MedicalRecordAccessory2 : 00000000-0000-0000-0000-000000000000
         * MedicalRecordAccessory3 : 00000000-0000-0000-0000-000000000000
         * MedicalRecordAccessory4 : 00000000-0000-0000-0000-000000000000
         * MedicalRecordAccessory5 : 00000000-0000-0000-0000-000000000000
         * IsDoctor : 0
         * IsPrescription : 3
         * dr_code : D610803001453
         * dr_name : 陈子平
         * IsThirdPartyPres : 0
         */

        private String AssociatedDiseases1;
        private String MedicalRecordAccessoryUrl1;
        private String MedicalRecordAccessoryUrl2;
        private String MedicalRecordAccessoryUrl3;
        private String MedicalRecordAccessoryUrl4;
        private String MedicalRecordAccessoryUrl5;
        private int OnlineInquiryId;
        private String psn_name;
        private String mdtrt_cert_no;
        private String sex;
        private String age;
        private String AssociatedDiseases;
        private String IsTake;
        private String IsAllergy;
        private String IsUntowardReaction;
        private String Symptom;
        private String CreateDT;
        private String UpdateDT;
        private String CreateUser;
        private String UpdateUser;
        private String UserId;
        private String UserName;
        private String fixmedins_code;
        private String OrderNo;
        private String MedicalRecordAccessory1;
        private String MedicalRecordAccessory2;
        private String MedicalRecordAccessory3;
        private String MedicalRecordAccessory4;
        private String MedicalRecordAccessory5;
        private int IsDoctor;
        private int IsPrescription;
        private String dr_code;
        private String dr_name;
        private int IsThirdPartyPres;

        public String getAssociatedDiseases1() {
            return AssociatedDiseases1;
        }

        public void setAssociatedDiseases1(String AssociatedDiseases1) {
            this.AssociatedDiseases1 = AssociatedDiseases1;
        }

        public String getMedicalRecordAccessoryUrl1() {
            return MedicalRecordAccessoryUrl1;
        }

        public void setMedicalRecordAccessoryUrl1(String MedicalRecordAccessoryUrl1) {
            this.MedicalRecordAccessoryUrl1 = MedicalRecordAccessoryUrl1;
        }

        public String getMedicalRecordAccessoryUrl2() {
            return MedicalRecordAccessoryUrl2;
        }

        public void setMedicalRecordAccessoryUrl2(String MedicalRecordAccessoryUrl2) {
            this.MedicalRecordAccessoryUrl2 = MedicalRecordAccessoryUrl2;
        }

        public String getMedicalRecordAccessoryUrl3() {
            return MedicalRecordAccessoryUrl3;
        }

        public void setMedicalRecordAccessoryUrl3(String MedicalRecordAccessoryUrl3) {
            this.MedicalRecordAccessoryUrl3 = MedicalRecordAccessoryUrl3;
        }

        public String getMedicalRecordAccessoryUrl4() {
            return MedicalRecordAccessoryUrl4;
        }

        public void setMedicalRecordAccessoryUrl4(String MedicalRecordAccessoryUrl4) {
            this.MedicalRecordAccessoryUrl4 = MedicalRecordAccessoryUrl4;
        }

        public String getMedicalRecordAccessoryUrl5() {
            return MedicalRecordAccessoryUrl5;
        }

        public void setMedicalRecordAccessoryUrl5(String MedicalRecordAccessoryUrl5) {
            this.MedicalRecordAccessoryUrl5 = MedicalRecordAccessoryUrl5;
        }

        public int getOnlineInquiryId() {
            return OnlineInquiryId;
        }

        public void setOnlineInquiryId(int OnlineInquiryId) {
            this.OnlineInquiryId = OnlineInquiryId;
        }

        public String getPsn_name() {
            return psn_name;
        }

        public void setPsn_name(String psn_name) {
            this.psn_name = psn_name;
        }

        public String getMdtrt_cert_no() {
            return mdtrt_cert_no;
        }

        public void setMdtrt_cert_no(String mdtrt_cert_no) {
            this.mdtrt_cert_no = mdtrt_cert_no;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getAssociatedDiseases() {
            return AssociatedDiseases;
        }

        public void setAssociatedDiseases(String AssociatedDiseases) {
            this.AssociatedDiseases = AssociatedDiseases;
        }

        public String getIsTake() {
            return IsTake;
        }

        public void setIsTake(String IsTake) {
            this.IsTake = IsTake;
        }

        public String getIsAllergy() {
            return IsAllergy;
        }

        public void setIsAllergy(String IsAllergy) {
            this.IsAllergy = IsAllergy;
        }

        public String getIsUntowardReaction() {
            return IsUntowardReaction;
        }

        public void setIsUntowardReaction(String IsUntowardReaction) {
            this.IsUntowardReaction = IsUntowardReaction;
        }

        public String getSymptom() {
            return Symptom;
        }

        public void setSymptom(String Symptom) {
            this.Symptom = Symptom;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getUpdateDT() {
            return UpdateDT;
        }

        public void setUpdateDT(String UpdateDT) {
            this.UpdateDT = UpdateDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String CreateUser) {
            this.CreateUser = CreateUser;
        }

        public String getUpdateUser() {
            return UpdateUser;
        }

        public void setUpdateUser(String UpdateUser) {
            this.UpdateUser = UpdateUser;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getMedicalRecordAccessory1() {
            return MedicalRecordAccessory1;
        }

        public void setMedicalRecordAccessory1(String MedicalRecordAccessory1) {
            this.MedicalRecordAccessory1 = MedicalRecordAccessory1;
        }

        public String getMedicalRecordAccessory2() {
            return MedicalRecordAccessory2;
        }

        public void setMedicalRecordAccessory2(String MedicalRecordAccessory2) {
            this.MedicalRecordAccessory2 = MedicalRecordAccessory2;
        }

        public String getMedicalRecordAccessory3() {
            return MedicalRecordAccessory3;
        }

        public void setMedicalRecordAccessory3(String MedicalRecordAccessory3) {
            this.MedicalRecordAccessory3 = MedicalRecordAccessory3;
        }

        public String getMedicalRecordAccessory4() {
            return MedicalRecordAccessory4;
        }

        public void setMedicalRecordAccessory4(String MedicalRecordAccessory4) {
            this.MedicalRecordAccessory4 = MedicalRecordAccessory4;
        }

        public String getMedicalRecordAccessory5() {
            return MedicalRecordAccessory5;
        }

        public void setMedicalRecordAccessory5(String MedicalRecordAccessory5) {
            this.MedicalRecordAccessory5 = MedicalRecordAccessory5;
        }

        public int getIsDoctor() {
            return IsDoctor;
        }

        public void setIsDoctor(int IsDoctor) {
            this.IsDoctor = IsDoctor;
        }

        public int getIsPrescription() {
            return IsPrescription;
        }

        public void setIsPrescription(int IsPrescription) {
            this.IsPrescription = IsPrescription;
        }

        public String getDr_code() {
            return dr_code;
        }

        public void setDr_code(String dr_code) {
            this.dr_code = dr_code;
        }

        public String getDr_name() {
            return dr_name;
        }

        public void setDr_name(String dr_name) {
            this.dr_name = dr_name;
        }

        public int getIsThirdPartyPres() {
            return IsThirdPartyPres;
        }

        public void setIsThirdPartyPres(int IsThirdPartyPres) {
            this.IsThirdPartyPres = IsThirdPartyPres;
        }
    }

    //判断机构是否有医师执业执照
    public static void sendInquiryInfoRequest(final String TAG,String OrderNo,final CustomerJsonCallBack<InquiryInfoModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_QUERYONLINEINQUIRY_URL, jsonObject.toJSONString(), callback);
    }
}
