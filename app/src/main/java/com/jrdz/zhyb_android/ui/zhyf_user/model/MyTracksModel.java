package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.google.gson.annotations.SerializedName;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-02
 * 描    述：
 * ================================================
 */
public class MyTracksModel{
    public static final int TITLE_TAG=1;
    public static final int CONTENT_TAG=2;
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-12-02 09:20:58
     * data : [{"Price":1,"GoodsName":"苯丙酮尿症1003","BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/6faa7154-c810-4b94-8930-fcc09d9090d1/975a3467-2185-4046-b77a-32ee59ce4877.jpg","MonthlySales":100,"ItemType":1,"IsPromotion":1,"PromotionDiscount":8,"PreferentialPrice":0.8,"EfccAtd":"功能主治1111","TrackId":1,"UserId":"e34c4bdb-55c1-43da-9e04-8bb5ff591b3b","UserName":"15060338985","GoodsNo":"300006","CreateDT":"2022-12-02 09:10:54","CreateUser":"15060338985"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements MultiItemEntity {
        /**
         * Price : 1
         * GoodsName : 苯丙酮尿症1003
         * BannerAccessoryUrl1 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/6faa7154-c810-4b94-8930-fcc09d9090d1/975a3467-2185-4046-b77a-32ee59ce4877.jpg
         * MonthlySales : 100
         * ItemType : 1
         * IsPromotion : 1
         * PromotionDiscount : 8
         * PreferentialPrice : 0.8
         * EfccAtd : 功能主治1111
         * TrackId : 1
         * UserId : e34c4bdb-55c1-43da-9e04-8bb5ff591b3b
         * UserName : 15060338985
         * GoodsNo : 300006
         * CreateDT : 2022-12-02 09:10:54
         * CreateUser : 15060338985
         */

        private String Price;
        private String GoodsName;
        private String BannerAccessoryUrl1;
        private String MonthlySales;
        @SerializedName("ItemType")
        private String presc_ItemType;
        private String IsPromotion;
        private String PromotionDiscount;
        private String PreferentialPrice;
        private String EfccAtd;
        private String TrackId;
        private String GoodsNo;
        private String TrackTime;
        private String IsPlatformDrug;
        private String IsShowGoodsSold;
        private String fixmedins_code;
        //是否允许企业基金支付（0不允许 1允许）
        private String IsEnterpriseFundPay;
        //程序自用
        private boolean choose;//是否选中
        private int parentPos = -1;//记录当前商品对应的标题pos
        private int spanSize;//时间标题 跟 内容占宽度的比例
        private int itemType;//区分标题跟 内容

        //展示标题
        public DataBean(String goodsName, String trackTime, boolean choose, int spanSize, int itemType) {
            GoodsName = goodsName;
            TrackTime = trackTime;
            this.choose = choose;
            this.spanSize = spanSize;
            this.itemType = itemType;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String Price) {
            this.Price = Price;
        }

        public String getGoodsName() {
            return GoodsName;
        }

        public void setGoodsName(String GoodsName) {
            this.GoodsName = GoodsName;
        }

        public String getBannerAccessoryUrl1() {
            return BannerAccessoryUrl1;
        }

        public void setBannerAccessoryUrl1(String BannerAccessoryUrl1) {
            this.BannerAccessoryUrl1 = BannerAccessoryUrl1;
        }

        public String getMonthlySales() {
            return MonthlySales;
        }

        public void setMonthlySales(String MonthlySales) {
            this.MonthlySales = MonthlySales;
        }

        public String getPresc_ItemType() {
            return presc_ItemType;
        }

        public void setPresc_ItemType(String presc_ItemType) {
            this.presc_ItemType = presc_ItemType;
        }

        public String getIsPromotion() {
            return IsPromotion;
        }

        public void setIsPromotion(String IsPromotion) {
            this.IsPromotion = IsPromotion;
        }

        public String getPromotionDiscount() {
            return PromotionDiscount;
        }

        public void setPromotionDiscount(String PromotionDiscount) {
            this.PromotionDiscount = PromotionDiscount;
        }

        public String getPreferentialPrice() {
            return PreferentialPrice;
        }

        public void setPreferentialPrice(String PreferentialPrice) {
            this.PreferentialPrice = PreferentialPrice;
        }

        public String getEfccAtd() {
            return EfccAtd;
        }

        public void setEfccAtd(String EfccAtd) {
            this.EfccAtd = EfccAtd;
        }

        public String getTrackId() {
            return TrackId;
        }

        public void setTrackId(String TrackId) {
            this.TrackId = TrackId;
        }

        public String getGoodsNo() {
            return GoodsNo;
        }

        public void setGoodsNo(String GoodsNo) {
            this.GoodsNo = GoodsNo;
        }

        public String getTrackTime() {
            return TrackTime;
        }

        public void setTrackTime(String trackTime) {
            TrackTime = trackTime;
        }

        public boolean isChoose() {
            return choose;
        }

        public void setChoose(boolean choose) {
            this.choose = choose;
        }

        public int getParentPos() {
            return parentPos;
        }

        public void setParentPos(int parentPos) {
            this.parentPos = parentPos;
        }

        public int getSpanSize() {
            return spanSize;
        }

        public void setSpanSize(int spanSize) {
            this.spanSize = spanSize;
        }

        public void setItemType(int itemType) {
            this.itemType = itemType;
        }

        @Override
        public int getItemType() {
            return itemType;
        }

        public String getIsPlatformDrug() {
            return IsPlatformDrug;
        }

        public void setIsPlatformDrug(String isPlatformDrug) {
            IsPlatformDrug = isPlatformDrug;
        }

        public String getIsShowGoodsSold() {
            return IsShowGoodsSold;
        }

        public void setIsShowGoodsSold(String isShowGoodsSold) {
            IsShowGoodsSold = isShowGoodsSold;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getIsEnterpriseFundPay() {
            return IsEnterpriseFundPay;
        }

        public void setIsEnterpriseFundPay(String isEnterpriseFundPay) {
            IsEnterpriseFundPay = isEnterpriseFundPay;
        }
    }

    //订单详情
    public static void sendMyTracksRequest_user(final String TAG,String pageindex,String pagesize, final CustomerJsonCallBack<MyTracksModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_TRACKLIST_URL, jsonObject.toJSONString(), callback);
    }
}
