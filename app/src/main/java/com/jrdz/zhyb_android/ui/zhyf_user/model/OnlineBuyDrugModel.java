package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.base.BaseModel;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;
import com.jrdz.zhyb_android.net.RequestData;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-25
 * 描    述：
 * ================================================
 */
public class OnlineBuyDrugModel {
    /**
     * code : 1
     * msg : 订单提交成功
     * server_time : 2022-12-05 14:41:14
     * data : {"OrderNo":"20221205144113100009"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * OrderNo : 20221205144113100009
         */

        private String OrderNo;

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }
    }

    //新增订单
    public static void sendOnlineBuyDrugRequest(final String TAG, String ShippingMethod, String ShippingMethodName, String FullName,
                                                String Phone,String Location, String Address, String PurchasingDrugsMethod, String PurchasingDrugsMethodName,
                                                String diag_code, String diag_name, String Buyer, String IDNumber, String GoodsNum,
                                                String TotalAmount, String BuyerWeChat, String BuyerMessage,
                                                String fixmedins_code, ArrayList<GoodsModel.DataBean> goodBeans,String AreaName,String CityName,
                                                String distance,String insutype,
                                                final CustomerJsonCallBack<OnlineBuyDrugModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ShippingMethod", ShippingMethod);//配送方式（1、自提2、商家配送）
        jsonObject.put("ShippingMethodName", ShippingMethodName);//配送方式名称
        jsonObject.put("FullName", FullName);//姓名
        jsonObject.put("Phone", Phone);//手机号
        jsonObject.put("Location", Location);//省市区
        jsonObject.put("Address", Address);//详细地址
        jsonObject.put("PurchasingDrugsMethod", PurchasingDrugsMethod);//购药方式（1、自助购药（自费结算）2、城乡居民医保/职工医保购药（医保结算）3、慢特病购药（医保结算）4、两病购药（医保结算）5、门诊统筹（医保结算））
        jsonObject.put("PurchasingDrugsMethodName", PurchasingDrugsMethodName);//购药方式名称
        jsonObject.put("diag_code", diag_code);//病种编码
        jsonObject.put("diag_name", diag_name);//病种名称
        jsonObject.put("Buyer", Buyer);//购药人
        jsonObject.put("IDNumber", IDNumber);//购药人身份证
        jsonObject.put("GoodsNum", GoodsNum);//商品数量
        jsonObject.put("TotalAmount", TotalAmount);//商品价格
        jsonObject.put("BuyerWeChat", BuyerWeChat);//买家微信
        jsonObject.put("BuyerMessage", BuyerMessage);//买家留言
        jsonObject.put("fixmedins_code", fixmedins_code);//机构编码

        JSONArray jsonArray=new JSONArray();
        for (GoodsModel.DataBean goodBean : goodBeans) {
            JSONObject jsonObjectChild = new JSONObject();
            jsonObjectChild.put("GoodsName", goodBean.getGoodsName());//商品名称
            jsonObjectChild.put("GoodsNum", goodBean.getShoppingCartNum());//数量
            jsonObjectChild.put("Price", "1".equals(goodBean.getIsPromotion())?goodBean.getPreferentialPrice():goodBean.getPrice());//商品价格
            jsonObjectChild.put("GoodsNo", goodBean.getGoodsNo());//商品编码
            jsonObjectChild.put("BannerAccessoryId1", goodBean.getBannerAccessoryId1()); //商品图标
            jsonObjectChild.put("ItemType", goodBean.getItemType());//目录类型（1、OTC 2、处方药）

            jsonArray.add(jsonObjectChild);
        }
        jsonObject.put("OrderGoods", jsonArray);//订单商品表
        jsonObject.put("AreaName", AreaName);//省份
        jsonObject.put("CityName", CityName);//市
        jsonObject.put("distance", distance);//距离
        jsonObject.put("insutype", insutype);//险种类型

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ORDER_URL, jsonObject.toJSONString(), callback);
    }

    //新增O2O订单--扫码下单
    public static void sendScanOrderRequest(final String TAG, String fixmedins_code, String TotalAmount, String GoodsNum,
                                            ArrayList<GoodsModel.DataBean> goodBeans, final CustomerJsonCallBack<OnlineBuyDrugModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fixmedins_code", fixmedins_code);//机构编码
        jsonObject.put("TotalAmount", TotalAmount);
        jsonObject.put("GoodsNum", GoodsNum);
        JSONArray jsonArray=new JSONArray();
        for (GoodsModel.DataBean goodBean : goodBeans) {
            JSONObject jsonObjectChild = new JSONObject();
            jsonObjectChild.put("GoodsName", goodBean.getGoodsName());//商品名称
            jsonObjectChild.put("GoodsNum", goodBean.getShoppingCartNum());//数量
            jsonObjectChild.put("Price", "1".equals(goodBean.getIsPromotion())?goodBean.getPreferentialPrice():goodBean.getPrice());//商品价格
            jsonObjectChild.put("GoodsNo", goodBean.getGoodsNo());//商品编码
            jsonArray.add(jsonObjectChild);
        }
        jsonObject.put("OrderGoods", jsonArray);//订单商品表
        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MERCHANT_SCAN_ORDER_URL, jsonObject.toJSONString(), callback);
    }
}
