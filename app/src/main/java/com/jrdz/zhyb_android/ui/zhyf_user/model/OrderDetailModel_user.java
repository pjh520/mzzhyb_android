package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-25
 * 描    述：
 * ================================================
 */
public class OrderDetailModel_user {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-25 19:04:17
     * data : {"OrderNo":"20221125182221100001","ShippingMethod":1,"ShippingMethodName":"自提","FullName":"","Phone":"","Address":"","PurchasingDrugsMethod":1,"PurchasingDrugsMethodName":"自助购药","GoodsNum":2,"TotalAmount":1.8,"OrderStatus":1,"fixmedins_code":"H61080200145","StoreName":"测试店铺","Telephone":"15060338986","Wechat":"15060338986","setl_id":"","LogisticsFee":0,"CreateDT":"2022-11-25 18:22:21","OrderGoods":[{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1}]}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * OrderNo : 20221125182221100001
         * ShippingMethod : 1
         * ShippingMethodName : 自提
         * FullName :
         * Phone :
         * Address :
         * PurchasingDrugsMethod : 1
         * PurchasingDrugsMethodName : 自助购药
         * GoodsNum : 2
         * TotalAmount : 1.8
         * OrderStatus : 1
         * fixmedins_code : H61080200145
         * StoreName : 测试店铺
         * Telephone : 15060338986
         * Wechat : 15060338986
         * setl_id :
         * LogisticsFee : 0
         * CreateDT : 2022-11-25 18:22:21
         * OrderGoods : [{"GoodsName":"地喹氯铵含片","GoodsNum":2,"Price":1,"fixmedins_code":"H61080200145","ItemType":1}]
         */

        private String OrderNo;
        private String ShippingMethod;
        private String ShippingMethodName;
        private String FullName;
        private String Phone;
        private String Location;
        private String Address;
        private String PurchasingDrugsMethod;
        private String PurchasingDrugsMethodName;
        private String GoodsNum;
        private String TotalAmount;
        private String OrderStatus;
        private String fixmedins_code;
        private String StoreName;
        private String Telephone;
        private String Wechat;
        private String setl_id;
        private String LogisticsFee;
        private String CreateDT;
        private List<OrderGoodsBean> OrderGoods;
        private String BuyerWeChat;
        private String BuyerMessage;
        private String StoreAccessoryUrl;
        private String OnlineAmount;

        private String psn_no;
        private String mdtrt_cert_type;
        private String mdtrt_cert_no;
        private String medfee_sumamt;
        private String insutype;
        private String fund_pay_sumamt;
        private String psn_name;
        private String psn_cash_pay;
        private String balc;
        private String acct_pay;
        private String ipt_otp_no;
        private String psn_type;
        private String IsPrescription;//是否开处方(0 不开处方 1、待开处方 2、已开处方
        private String LastPaytTime;
        //订单取消时间
        private String CancelTime;
        //订单退款编号
        private String OrderRefundNo;
        //订单退款流水号
        private String OrderRefundSerialNo;
        //医保支付金额
        private String MedicareAmount;
        //医保结算状态(0、 未结算 1、结算成功）
        private String SettlementStatus;
        //订单发货时间
        private String DeliveryTime;
        //订单确认时间
        private String ConfirmTime;
        //是否已退款(0、 未退款 1、已退款）
        private String IsRefunded;
        //是否允许企业基金支付（0不允许 1允许）
        private String IsEnterpriseFundPay;
        //企业基金余额
        private String EnterpriseFundAmount;
        //企业基金支付金额
        private String EnterpriseFundPay;
        //企业基金支付状态
        private String EnterpriseFundPayStatus;//(0未支付 1已支付)
        //医保基金支付总额(预结算）
        private String pre_fund_pay_sumamt;
        //医保个人账户余额(预结算）
        private String pre_balc;
        //取消订单是否查看物流（0、可以1、不可以）
        private String IsViewLogistics;

        private String sex;
        private String age;
        private String Buyer;
        private String AccessoryUrl;
        private String StartingPrice;
        private String DiscountAmount;//优惠金额
        private String Discount;//优惠折扣
        private String OriginalTotalAmount;//原始订单总额
        private String IsThirdPartyPres;//是否第三方开处方（1是，0否）

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getShippingMethod() {
            return ShippingMethod;
        }

        public void setShippingMethod(String ShippingMethod) {
            this.ShippingMethod = ShippingMethod;
        }

        public String getShippingMethodName() {
            return ShippingMethodName;
        }

        public void setShippingMethodName(String ShippingMethodName) {
            this.ShippingMethodName = ShippingMethodName;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String Phone) {
            this.Phone = Phone;
        }

        public String getLocation() {
            return Location;
        }

        public void setLocation(String location) {
            Location = location;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getPurchasingDrugsMethod() {
            return PurchasingDrugsMethod;
        }

        public void setPurchasingDrugsMethod(String PurchasingDrugsMethod) {
            this.PurchasingDrugsMethod = PurchasingDrugsMethod;
        }

        public String getPurchasingDrugsMethodName() {
            return PurchasingDrugsMethodName;
        }

        public void setPurchasingDrugsMethodName(String PurchasingDrugsMethodName) {
            this.PurchasingDrugsMethodName = PurchasingDrugsMethodName;
        }

        public String getGoodsNum() {
            return GoodsNum;
        }

        public void setGoodsNum(String GoodsNum) {
            this.GoodsNum = GoodsNum;
        }

        public String getTotalAmount() {
            return TotalAmount;
        }

        public void setTotalAmount(String TotalAmount) {
            this.TotalAmount = TotalAmount;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public String getTelephone() {
            return Telephone;
        }

        public void setTelephone(String Telephone) {
            this.Telephone = Telephone;
        }

        public String getWechat() {
            return Wechat;
        }

        public void setWechat(String Wechat) {
            this.Wechat = Wechat;
        }

        public String getSetl_id() {
            return setl_id;
        }

        public void setSetl_id(String setl_id) {
            this.setl_id = setl_id;
        }

        public String getLogisticsFee() {
            return LogisticsFee;
        }

        public void setLogisticsFee(String LogisticsFee) {
            this.LogisticsFee = LogisticsFee;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getBuyerWeChat() {
            return BuyerWeChat;
        }

        public void setBuyerWeChat(String buyerWeChat) {
            BuyerWeChat = buyerWeChat;
        }

        public String getBuyerMessage() {
            return BuyerMessage;
        }

        public void setBuyerMessage(String buyerMessage) {
            BuyerMessage = buyerMessage;
        }

        public String getStoreAccessoryUrl() {
            return StoreAccessoryUrl;
        }

        public void setStoreAccessoryUrl(String storeAccessoryUrl) {
            StoreAccessoryUrl = storeAccessoryUrl;
        }

        public String getOnlineAmount() {
            return OnlineAmount;
        }

        public void setOnlineAmount(String onlineAmount) {
            OnlineAmount = onlineAmount;
        }

        public String getPsn_no() {
            return psn_no;
        }

        public void setPsn_no(String psn_no) {
            this.psn_no = psn_no;
        }

        public String getMdtrt_cert_type() {
            return mdtrt_cert_type;
        }

        public void setMdtrt_cert_type(String mdtrt_cert_type) {
            this.mdtrt_cert_type = mdtrt_cert_type;
        }

        public String getMdtrt_cert_no() {
            return mdtrt_cert_no;
        }

        public void setMdtrt_cert_no(String mdtrt_cert_no) {
            this.mdtrt_cert_no = mdtrt_cert_no;
        }

        public String getMedfee_sumamt() {
            return medfee_sumamt;
        }

        public void setMedfee_sumamt(String medfee_sumamt) {
            this.medfee_sumamt = medfee_sumamt;
        }

        public String getInsutype() {
            return insutype;
        }

        public void setInsutype(String insutype) {
            this.insutype = insutype;
        }

        public String getFund_pay_sumamt() {
            return fund_pay_sumamt;
        }

        public void setFund_pay_sumamt(String fund_pay_sumamt) {
            this.fund_pay_sumamt = fund_pay_sumamt;
        }

        public String getPsn_name() {
            return psn_name;
        }

        public void setPsn_name(String psn_name) {
            this.psn_name = psn_name;
        }

        public String getPsn_cash_pay() {
            return psn_cash_pay;
        }

        public void setPsn_cash_pay(String psn_cash_pay) {
            this.psn_cash_pay = psn_cash_pay;
        }

        public String getBalc() {
            return balc;
        }

        public void setBalc(String balc) {
            this.balc = balc;
        }

        public String getAcct_pay() {
            return acct_pay;
        }

        public void setAcct_pay(String acct_pay) {
            this.acct_pay = acct_pay;
        }

        public String getIpt_otp_no() {
            return ipt_otp_no;
        }

        public void setIpt_otp_no(String ipt_otp_no) {
            this.ipt_otp_no = ipt_otp_no;
        }

        public String getPsn_type() {
            return psn_type;
        }

        public void setPsn_type(String psn_type) {
            this.psn_type = psn_type;
        }

        public String getIsPrescription() {
            return IsPrescription;
        }

        public void setIsPrescription(String isPrescription) {
            IsPrescription = isPrescription;
        }

        public String getLastPaytTime() {
            return LastPaytTime;
        }

        public void setLastPaytTime(String lastPaytTime) {
            LastPaytTime = lastPaytTime;
        }

        public String getCancelTime() {
            return CancelTime;
        }

        public void setCancelTime(String cancelTime) {
            CancelTime = cancelTime;
        }

        public String getOrderRefundNo() {
            return OrderRefundNo;
        }

        public void setOrderRefundNo(String orderRefundNo) {
            OrderRefundNo = orderRefundNo;
        }

        public String getOrderRefundSerialNo() {
            return OrderRefundSerialNo;
        }

        public void setOrderRefundSerialNo(String orderRefundSerialNo) {
            OrderRefundSerialNo = orderRefundSerialNo;
        }

        public String getMedicareAmount() {
            return MedicareAmount;
        }

        public void setMedicareAmount(String medicareAmount) {
            MedicareAmount = medicareAmount;
        }

        public String getSettlementStatus() {
            return SettlementStatus;
        }

        public void setSettlementStatus(String settlementStatus) {
            SettlementStatus = settlementStatus;
        }

        public String getDeliveryTime() {
            return DeliveryTime;
        }

        public void setDeliveryTime(String deliveryTime) {
            DeliveryTime = deliveryTime;
        }

        public String getConfirmTime() {
            return ConfirmTime;
        }

        public void setConfirmTime(String confirmTime) {
            ConfirmTime = confirmTime;
        }

        public String getIsRefunded() {
            return IsRefunded;
        }

        public void setIsRefunded(String isRefunded) {
            IsRefunded = isRefunded;
        }

        public String getIsEnterpriseFundPay() {
            return IsEnterpriseFundPay;
        }

        public void setIsEnterpriseFundPay(String isEnterpriseFundPay) {
            IsEnterpriseFundPay = isEnterpriseFundPay;
        }

        public String getEnterpriseFundAmount() {
            return EnterpriseFundAmount;
        }

        public void setEnterpriseFundAmount(String enterpriseFundAmount) {
            EnterpriseFundAmount = enterpriseFundAmount;
        }

        public String getEnterpriseFundPay() {
            return EnterpriseFundPay;
        }

        public void setEnterpriseFundPay(String enterpriseFundPay) {
            EnterpriseFundPay = enterpriseFundPay;
        }

        public String getEnterpriseFundPayStatus() {
            return EnterpriseFundPayStatus;
        }

        public void setEnterpriseFundPayStatus(String enterpriseFundPayStatus) {
            EnterpriseFundPayStatus = enterpriseFundPayStatus;
        }

        public String getPre_fund_pay_sumamt() {
            return pre_fund_pay_sumamt;
        }

        public void setPre_fund_pay_sumamt(String pre_fund_pay_sumamt) {
            this.pre_fund_pay_sumamt = pre_fund_pay_sumamt;
        }

        public String getPre_balc() {
            return pre_balc;
        }

        public void setPre_balc(String pre_balc) {
            this.pre_balc = pre_balc;
        }

        public String getIsViewLogistics() {
            return IsViewLogistics;
        }

        public void setIsViewLogistics(String isViewLogistics) {
            IsViewLogistics = isViewLogistics;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getBuyer() {
            return Buyer;
        }

        public void setBuyer(String buyer) {
            Buyer = buyer;
        }

        public String getAccessoryUrl() {
            return AccessoryUrl;
        }

        public void setAccessoryUrl(String accessoryUrl) {
            AccessoryUrl = accessoryUrl;
        }

        public String getStartingPrice() {
            return StartingPrice;
        }

        public void setStartingPrice(String startingPrice) {
            StartingPrice = startingPrice;
        }

        public String getDiscountAmount() {
            return DiscountAmount;
        }

        public void setDiscountAmount(String discountAmount) {
            DiscountAmount = discountAmount;
        }

        public String getDiscount() {
            return Discount;
        }

        public void setDiscount(String discount) {
            Discount = discount;
        }

        public String getOriginalTotalAmount() {
            return OriginalTotalAmount;
        }

        public void setOriginalTotalAmount(String originalTotalAmount) {
            OriginalTotalAmount = originalTotalAmount;
        }

        public String getIsThirdPartyPres() {
            return IsThirdPartyPres;
        }

        public void setIsThirdPartyPres(String isThirdPartyPres) {
            IsThirdPartyPres = isThirdPartyPres;
        }

        public List<OrderGoodsBean> getOrderGoods() {
            return OrderGoods;
        }

        public void setOrderGoods(List<OrderGoodsBean> OrderGoods) {
            this.OrderGoods = OrderGoods;
        }

        public static class OrderGoodsBean {
            /**
             * GoodsName : 地喹氯铵含片
             * GoodsNum : 2
             * Price : 1
             * fixmedins_code : H61080200145
             * ItemType : 1
             */
            private String GoodsNo;
            private String GoodsName;
            private int GoodsNum;
            private String Price;
            private String fixmedins_code;
            private String ItemType;
            private String BannerAccessoryUrl1;
            //是否允许企业基金支付（0不允许 1允许）
            private String IsEnterpriseFundPay;
            //关联疾病
            private String AssociatedDiseases;
            private String Spec;
            private String UsageDosage;
            private String Remark;
            private String IsPlatformDrug;

            public String getGoodsNo() {
                return GoodsNo;
            }

            public void setGoodsNo(String goodsNo) {
                GoodsNo = goodsNo;
            }

            public String getGoodsName() {
                return GoodsName;
            }

            public void setGoodsName(String GoodsName) {
                this.GoodsName = GoodsName;
            }

            public int getGoodsNum() {
                return GoodsNum;
            }

            public void setGoodsNum(int GoodsNum) {
                this.GoodsNum = GoodsNum;
            }

            public String getPrice() {
                return Price;
            }

            public void setPrice(String Price) {
                this.Price = Price;
            }

            public String getFixmedins_code() {
                return fixmedins_code;
            }

            public void setFixmedins_code(String fixmedins_code) {
                this.fixmedins_code = fixmedins_code;
            }

            public String getItemType() {
                return ItemType;
            }

            public void setItemType(String ItemType) {
                this.ItemType = ItemType;
            }

            public String getBannerAccessoryUrl1() {
                return BannerAccessoryUrl1;
            }

            public void setBannerAccessoryUrl1(String bannerAccessoryUrl1) {
                BannerAccessoryUrl1 = bannerAccessoryUrl1;
            }

            public String getIsEnterpriseFundPay() {
                return IsEnterpriseFundPay;
            }

            public void setIsEnterpriseFundPay(String isEnterpriseFundPay) {
                IsEnterpriseFundPay = isEnterpriseFundPay;
            }

            public String getAssociatedDiseases() {
                return AssociatedDiseases;
            }

            public void setAssociatedDiseases(String associatedDiseases) {
                AssociatedDiseases = associatedDiseases;
            }

            public String getSpec() {
                return Spec;
            }

            public void setSpec(String spec) {
                Spec = spec;
            }

            public String getUsageDosage() {
                return UsageDosage;
            }

            public void setUsageDosage(String usageDosage) {
                UsageDosage = usageDosage;
            }

            public String getRemark() {
                return Remark;
            }

            public void setRemark(String remark) {
                Remark = remark;
            }

            public String getIsPlatformDrug() {
                return IsPlatformDrug;
            }

            public void setIsPlatformDrug(String isPlatformDrug) {
                IsPlatformDrug = isPlatformDrug;
            }
        }
    }

    //订单详情
    public static void sendOrderDetailRequest_user(final String TAG, String OrderNo,final CustomerJsonCallBack<OrderDetailModel_user> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);//订单号

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ORDERDETAIL_URL, jsonObject.toJSONString(), callback);
    }
}
