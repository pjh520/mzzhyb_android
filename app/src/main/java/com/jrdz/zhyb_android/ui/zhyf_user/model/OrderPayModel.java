package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-20
 * 描    述：
 * ================================================
 */
public class OrderPayModel {
    /**
     * code : 1
     * msg : 结算成功
     * server_time : 2022-12-22 15:10:57
     * data : {"OrderNo":"20221221105446100030","SettlementStatus":"1","setl_id":"696500014","OnlineAmount":"0"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * OrderNo : 20221221105446100030
         * SettlementStatus : 1
         * setl_id : 696500014
         * OnlineAmount : 0
         */

        private String OrderNo;
        private String SettlementStatus;
        private String setl_id;
        private String OnlineAmount;
        private String acct_pay;

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getSettlementStatus() {
            return SettlementStatus;
        }

        public void setSettlementStatus(String SettlementStatus) {
            this.SettlementStatus = SettlementStatus;
        }

        public String getSetl_id() {
            return setl_id;
        }

        public void setSetl_id(String setl_id) {
            this.setl_id = setl_id;
        }

        public String getOnlineAmount() {
            return OnlineAmount;
        }

        public void setOnlineAmount(String OnlineAmount) {
            this.OnlineAmount = OnlineAmount;
        }

        public String getAcct_pay() {
            return acct_pay;
        }

        public void setAcct_pay(String acct_pay) {
            this.acct_pay = acct_pay;
        }
    }

    //订单立即支付（医保结算）
    public static void sendOrderPayRequest(final String TAG,String OrderNo, String verificationCode,String name,String pwd,String IsAcct_pay,
                                           final CustomerJsonCallBack<OrderPayModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);//订单号
        jsonObject.put("verificationCode", verificationCode);//短信验证码
        jsonObject.put("name", name);//姓名
        jsonObject.put("pwd", pwd);//医保密码
        jsonObject.put("IsAcct_pay", IsAcct_pay);//是否优先使用个人账户余额（0否 1是）

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_ORDERPAY_URL, jsonObject.toJSONString(), callback);
    }
}
