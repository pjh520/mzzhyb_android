package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-08-14
 * 描    述：
 * ================================================
 */
public class PhaSortSpecialAreaModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-08-25 09:43:08
     * totalItems : 0
     * data : [{"fixmedins_code":"H61080200145","StoreName":"榆林市第一医院","StoreAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/70dc3860-5019-43bb-a22b-4389d320e261/ced1b995-2b05-4a4f-a679-f0eaceab1f0a.jpg","StartingPrice":0,"StoreAdress":"福建省莆田市荔城区","DetailedAddress":"黄石镇西洪桥在清华建材附近","StarLevel":5,"MonthlySales":0,"Distance":7,"MonthlyStoreSales":0,"StoreInformationGoods":[{"GoodsId":0,"ItemCode":"ZA04CAL0115010102767","ItemType":0,"InventoryQuantity":0,"Price":25,"IsPlatformDrug":0,"GoodsLocation":0,"CatalogueId":0,"BannerAccessoryId1":"00000000-0000-0000-0000-000000000000","DrugsClassification":0,"IsPromotion":0,"GoodsName":"连花清瘟胶囊","PromotionDiscount":0,"PreferentialPrice":22.5,"IsOnSale":0,"BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/crm_product/7d07e195-985d-473f-a440-6e4d27103eb4/d66b8e0c-9dea-450a-8d48-4fb59d479ce9.jpg","GoodsNo":"300110","MonthlySales":0,"Distance":0,"ShoppingCartNum":0,"MonthlyStoreSales":0,"AssociatedDiseases":"感冒∞#伤寒","AssociatedDiagnostics":"[{\"choose\":true,\"code\":\"J00.x00\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"急性鼻咽炎［感冒］\"}]","StartingPrice":0,"StarLevel":0,"IsOline":0,"IsEnterpriseFundPay":0,"IsShowSold":0,"IsShowGoodsSold":0,"IsShowMargin":0}]}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * fixmedins_code : H61080200145
         * StoreName : 榆林市第一医院
         * StoreAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/70dc3860-5019-43bb-a22b-4389d320e261/ced1b995-2b05-4a4f-a679-f0eaceab1f0a.jpg
         * StartingPrice : 0
         * StoreAdress : 福建省莆田市荔城区
         * DetailedAddress : 黄石镇西洪桥在清华建材附近
         * StarLevel : 5
         * MonthlySales : 0
         * Distance : 7
         * MonthlyStoreSales : 0
         * StoreInformationGoods : [{"GoodsId":0,"ItemCode":"ZA04CAL0115010102767","ItemType":0,"InventoryQuantity":0,"Price":25,"IsPlatformDrug":0,"GoodsLocation":0,"CatalogueId":0,"BannerAccessoryId1":"00000000-0000-0000-0000-000000000000","DrugsClassification":0,"IsPromotion":0,"GoodsName":"连花清瘟胶囊","PromotionDiscount":0,"PreferentialPrice":22.5,"IsOnSale":0,"BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/crm_product/7d07e195-985d-473f-a440-6e4d27103eb4/d66b8e0c-9dea-450a-8d48-4fb59d479ce9.jpg","GoodsNo":"300110","MonthlySales":0,"Distance":0,"ShoppingCartNum":0,"MonthlyStoreSales":0,"AssociatedDiseases":"感冒∞#伤寒","AssociatedDiagnostics":"[{\"choose\":true,\"code\":\"J00.x00\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"急性鼻咽炎［感冒］\"}]","StartingPrice":0,"StarLevel":0,"IsOline":0,"IsEnterpriseFundPay":0,"IsShowSold":0,"IsShowGoodsSold":0,"IsShowMargin":0}]
         */

        private String fixmedins_code;
        private String StoreName;
        private String StoreAccessoryUrl;
        private String StartingPrice;
        private String StoreAdress;
        private String DetailedAddress;
        private String StarLevel;
        private String MonthlySales;
        private String Distance;
        private String MonthlyStoreSales;
        private List<GoodsModel.DataBean> StoreInformationGoods;

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public String getStoreAccessoryUrl() {
            return StoreAccessoryUrl;
        }

        public void setStoreAccessoryUrl(String StoreAccessoryUrl) {
            this.StoreAccessoryUrl = StoreAccessoryUrl;
        }

        public String getStartingPrice() {
            return StartingPrice;
        }

        public void setStartingPrice(String StartingPrice) {
            this.StartingPrice = StartingPrice;
        }

        public String getStoreAdress() {
            return StoreAdress;
        }

        public void setStoreAdress(String StoreAdress) {
            this.StoreAdress = StoreAdress;
        }

        public String getDetailedAddress() {
            return DetailedAddress;
        }

        public void setDetailedAddress(String DetailedAddress) {
            this.DetailedAddress = DetailedAddress;
        }

        public String getStarLevel() {
            return StarLevel;
        }

        public void setStarLevel(String StarLevel) {
            this.StarLevel = StarLevel;
        }

        public String getMonthlySales() {
            return MonthlySales;
        }

        public void setMonthlySales(String MonthlySales) {
            this.MonthlySales = MonthlySales;
        }

        public String getDistance() {
            return Distance;
        }

        public void setDistance(String Distance) {
            this.Distance = Distance;
        }

        public String getMonthlyStoreSales() {
            return MonthlyStoreSales;
        }

        public void setMonthlyStoreSales(String MonthlyStoreSales) {
            this.MonthlyStoreSales = MonthlyStoreSales;
        }

        public List<GoodsModel.DataBean> getStoreInformationGoods() {
            return StoreInformationGoods;
        }

        public void setStoreInformationGoods(List<GoodsModel.DataBean> StoreInformationGoods) {
            this.StoreInformationGoods = StoreInformationGoods;
        }
    }

    //获取专区列表--用户端
    public static void sendPhaSortSpecialAreaRequest_user(final String TAG, String pageindex, String pagesize,
                                                          String CatalogueId, String DrugsClassification, String Longitude,String Latitude,
                                                          final CustomerJsonCallBack<PhaSortSpecialAreaModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);//
        jsonObject.put("pagesize", pagesize);//
        jsonObject.put("CatalogueId", CatalogueId);//目录唯一标识
        jsonObject.put("DrugsClassification", DrugsClassification);//药品分类
        jsonObject.put("Longitude", Longitude);
        jsonObject.put("Latitude", Latitude);
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_STOREINFORMATIONGOODSLIST_URL, jsonObject.toJSONString(), callback);
    }
}
