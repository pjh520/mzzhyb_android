package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-02
 * 描    述：
 * ================================================
 */
public class ProductCollectionModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-12-02 18:13:13
     * data : [{"Price":1,"GoodsName":"苯丙酮尿症1002","BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/a7e199f9-045c-433b-84bd-7e969bb96eb4/f5d1c506-b214-4e24-addb-dfa0bc42c1e4.jpg","CollectionCount":0,"GoodsCollectionId":2,"UserId":"5f4f5865-b1c2-4c6b-8641-7545e15f56e4","UserName":"15060338985","GoodsNo":"299999","CreateDT":"2022-12-02 18:07:25","CreateUser":"15060338985"},{"Price":1,"GoodsName":"地喹氯铵含片","BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/cc9dae19-400f-4790-a302-22e69b867388/c9061f61-88b6-4df3-aba9-21d5b8b64546.jpg","CollectionCount":0,"GoodsCollectionId":3,"UserId":"719129fc-7e72-477b-b1ef-41001fc20f82","UserName":"15060338985","GoodsNo":"300000","CreateDT":"2022-12-02 18:07:39","CreateUser":"15060338985"},{"Price":1,"GoodsName":"苯丙酮尿症1003","BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/6faa7154-c810-4b94-8930-fcc09d9090d1/975a3467-2185-4046-b77a-32ee59ce4877.jpg","CollectionCount":0,"GoodsCollectionId":4,"UserId":"cf751d9b-41b0-41cf-94c8-19e0ba7039e0","UserName":"15060338985","GoodsNo":"300006","CreateDT":"2022-12-02 18:07:45","CreateUser":"15060338985"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * Price : 1
         * GoodsName : 苯丙酮尿症1002
         * BannerAccessoryUrl1 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/a7e199f9-045c-433b-84bd-7e969bb96eb4/f5d1c506-b214-4e24-addb-dfa0bc42c1e4.jpg
         * CollectionCount : 0
         * GoodsCollectionId : 2
         * UserId : 5f4f5865-b1c2-4c6b-8641-7545e15f56e4
         * UserName : 15060338985
         * GoodsNo : 299999
         * CreateDT : 2022-12-02 18:07:25
         * CreateUser : 15060338985
         */

        private String Price;
        private String GoodsName;
        private String BannerAccessoryUrl1;
        private String CollectionCount;
        private String GoodsCollectionId;
        private String UserId;
        private String UserName;
        private String GoodsNo;
        private String CreateDT;
        private String CreateUser;
        private String IsPlatformDrug;
        private String fixmedins_code;
        //是否允许企业基金支付（0不允许 1允许）
        private String IsEnterpriseFundPay;


        public String getPrice() {
            return Price;
        }

        public void setPrice(String Price) {
            this.Price = Price;
        }

        public String getGoodsName() {
            return GoodsName;
        }

        public void setGoodsName(String GoodsName) {
            this.GoodsName = GoodsName;
        }

        public String getBannerAccessoryUrl1() {
            return BannerAccessoryUrl1;
        }

        public void setBannerAccessoryUrl1(String BannerAccessoryUrl1) {
            this.BannerAccessoryUrl1 = BannerAccessoryUrl1;
        }

        public String getCollectionCount() {
            return CollectionCount;
        }

        public void setCollectionCount(String CollectionCount) {
            this.CollectionCount = CollectionCount;
        }

        public String getGoodsCollectionId() {
            return GoodsCollectionId;
        }

        public void setGoodsCollectionId(String GoodsCollectionId) {
            this.GoodsCollectionId = GoodsCollectionId;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getGoodsNo() {
            return GoodsNo;
        }

        public void setGoodsNo(String GoodsNo) {
            this.GoodsNo = GoodsNo;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String CreateUser) {
            this.CreateUser = CreateUser;
        }

        public String getIsPlatformDrug() {
            return IsPlatformDrug;
        }

        public void setIsPlatformDrug(String isPlatformDrug) {
            IsPlatformDrug = isPlatformDrug;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getIsEnterpriseFundPay() {
            return IsEnterpriseFundPay;
        }

        public void setIsEnterpriseFundPay(String isEnterpriseFundPay) {
            IsEnterpriseFundPay = isEnterpriseFundPay;
        }
    }

    //商品收藏列表
    public static void sendProductCollectionRequest(final String TAG, String pageindex, String pagesize, final CustomerJsonCallBack<ProductCollectionModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_GOODSCOLLECTIONLIST_URL, jsonObject.toJSONString(), callback);
    }
}
