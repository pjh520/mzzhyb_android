package com.jrdz.zhyb_android.ui.zhyf_user.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/11/6
 * 描    述：
 * ================================================
 */
public class ProductDescribeModel {
    private int img;
    private String title;
    private String describe;

    public ProductDescribeModel(int img, String title, String describe) {
        this.img = img;
        this.title = title;
        this.describe = describe;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    @Override
    public String toString() {
        return "ProductDescribeModel{" +
                "title='" + title + '\'' +
                ", describe='" + describe + '\'' +
                '}';
    }
}
