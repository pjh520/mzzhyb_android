package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.RequestData;
import com.jrdz.zhyb_android.ui.home.model.ScanPayStatusModel;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-03-29
 * 描    述：
 * ================================================
 */
public class QueryPresStatusModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2024-03-29 17:29:28
     * data : {"OrderNo":"20240329172226101120","IsPrescription":"4"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * OrderNo : 20240329172226101120
         * IsPrescription : 4
         */

        private String OrderNo;
        private String IsPrescription;
        private String CreateDT;

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getIsPrescription() {
            return IsPrescription;
        }

        public void setIsPrescription(String IsPrescription) {
            this.IsPrescription = IsPrescription;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String createDT) {
            CreateDT = createDT;
        }
    }

    //查询处方问诊状态
    public static void sendQueryPresStatusRequest(final String TAG, String OrderNo, final CustomerJsonCallBack<QueryPresStatusModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderNo", OrderNo);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_QUERYPRESSTATUS_URL, jsonObject.toJSONString(), callback);
    }
}
