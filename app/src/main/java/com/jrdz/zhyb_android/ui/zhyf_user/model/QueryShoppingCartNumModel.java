package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-12-25
 * 描    述：
 * ================================================
 */
public class QueryShoppingCartNumModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-12-25 17:52:56
     * data : {"ShoppingCartNum":"0"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * ShoppingCartNum : 0
         */

        private String ShoppingCartNum;

        public String getShoppingCartNum() {
            return ShoppingCartNum;
        }

        public void setShoppingCartNum(String ShoppingCartNum) {
            this.ShoppingCartNum = ShoppingCartNum;
        }
    }

    //用户端获取购物车商品数
    public static void sendQueryShoppingCartNumRequest(final String TAG,String GoodsNo, String fixmedins_code,
                                                       final CustomerJsonCallBack<QueryShoppingCartNumModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("GoodsNo", GoodsNo);//机构编码
        jsonObject.put("fixmedins_code", fixmedins_code);//机构编码

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_QUERYSHOPPINGCARTNUM_URL, jsonObject.toJSONString(), callback);
    }
}
