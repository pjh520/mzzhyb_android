package com.jrdz.zhyb_android.ui.zhyf_user.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023/1/6
 * 描    述：
 * ================================================
 */
public class RefundProgressDataModel {
    private String status;
    private String content;
    private String date;

    public RefundProgressDataModel(String status, String content, String date) {
        this.status = status;
        this.content = content;
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
