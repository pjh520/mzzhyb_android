package com.jrdz.zhyb_android.ui.zhyf_user.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023/6/11
 * 描    述：
 * ================================================
 */
public class RefundProgressTopItemModel {

    private String describe;
    private String price;

    public RefundProgressTopItemModel(String describe, String price) {
        this.describe = describe;
        this.price = price;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
