package com.jrdz.zhyb_android.ui.zhyf_user.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/12/17
 * 描    述：
 * ================================================
 */
public class RelationDiseModel {
    private String text;
    private boolean choose;

    public RelationDiseModel(String text, boolean choose) {
        this.text = text;
        this.choose = choose;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isChoose() {
        return choose;
    }

    public void setChoose(boolean choose) {
        this.choose = choose;
    }

    @Override
    public String toString() {
        return "RelationDiseModel{" +
                "text='" + text + '\'' +
                ", choose=" + choose +
                '}';
    }
}
