package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-10-09
 * 描    述：
 * ================================================
 */
public class SearchResultModel_user {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-01-11 09:54:04
     * totalItems : 2
     * type : 1
     * data : [{"GoodsId":24,"MIC_Code":"XC09AAY074A001010101747","ItemCode":"XC09AAY074A001010101747","ItemType":2,"EfccAtd":"功能主治1111","InventoryQuantity":8,"Price":1,"IsPlatformDrug":1,"fixmedins_code":"H61080200145","GoodsLocation":1,"CatalogueId":1,"BannerAccessoryId1":"f8af59bc-aef8-4c62-9b2e-893b31c83a33","DrugsClassification":1,"DrugsClassificationName":"西药中成药","IsPromotion":1,"GoodsName":"马来酸依那普利片","PromotionDiscount":9,"PreferentialPrice":0.9,"IsOnSale":1,"BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/f8af59bc-aef8-4c62-9b2e-893b31c83a33/9ff2f3bb-6705-4888-9119-60555387c7f5.jpg","GoodsNo":"300022","MonthlySales":100,"CatalogueName":"常备药","Distance":0,"ShoppingCartNum":0,"MonthlyStoreSales":100,"Spec":"100mg","AssociatedDiseases":"盗汗咳血肺结核，经证实的,∞#咳嗽","AssociatedDiagnostics":"[{\"choose\":true,\"code\":\"A00.000\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"霍乱，由于O1群霍乱弧菌，霍乱生物型所致\"},{\"choose\":true,\"code\":\"A00.000x001\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"古典生物型霍乱\"},{\"choose\":true,\"code\":\"A00.100\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"霍乱，由于O1群霍乱弧菌，埃尔托生物型所致\"}]","StoreName":"测试店铺","StartingPrice":1,"StarLevel":0},{"GoodsId":26,"MIC_Code":"XC09AAY074A001010101747","ItemCode":"XC09AAY074A001010101747","ItemType":2,"EfccAtd":"功能主治1111","InventoryQuantity":5,"Price":1,"IsPlatformDrug":1,"fixmedins_code":"H61080200145","GoodsLocation":1,"CatalogueId":2,"BannerAccessoryId1":"3445380c-68a1-42f8-9849-5a5eccf91710","DrugsClassification":1,"DrugsClassificationName":"西药中成药","IsPromotion":1,"GoodsName":"马来酸依那普利片1001","PromotionDiscount":9,"PreferentialPrice":0.9,"IsOnSale":1,"BannerAccessoryUrl1":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/3445380c-68a1-42f8-9849-5a5eccf91710/f6f2f74b-2db3-49ab-adb0-27147dc9b598.jpg","GoodsNo":"300024","MonthlySales":100,"CatalogueName":"男性专区","Distance":0,"ShoppingCartNum":0,"MonthlyStoreSales":100,"Spec":"100mg","AssociatedDiseases":"盗汗咳血肺结核，经证实的,","AssociatedDiagnostics":"[{\"choose\":true,\"code\":\"A01.000\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"伤寒\"},{\"choose\":true,\"code\":\"A01.000x017\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"伤寒轻型\"},{\"choose\":true,\"code\":\"A04.803\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"厌氧菌肠炎\"},{\"choose\":true,\"code\":\"A04.901\",\"diag_type\":\"1\",\"diag_typename\":\"西医主要诊断\",\"name\":\"细菌性结肠炎\"}]","StoreName":"测试店铺","StartingPrice":1,"StarLevel":0}]
     */

    private String code;
    private String msg;
    private String server_time;
    private String totalItems;
    private String type;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * GoodsId : 24
         * MIC_Code : XC09AAY074A001010101747
         * ItemCode : XC09AAY074A001010101747
         * ItemType : 2
         * EfccAtd : 功能主治1111
         * InventoryQuantity : 8
         * Price : 1
         * IsPlatformDrug : 1
         * fixmedins_code : H61080200145
         * GoodsLocation : 1
         * CatalogueId : 1
         * BannerAccessoryId1 : f8af59bc-aef8-4c62-9b2e-893b31c83a33
         * DrugsClassification : 1
         * DrugsClassificationName : 西药中成药
         * IsPromotion : 1
         * GoodsName : 马来酸依那普利片
         * PromotionDiscount : 9
         * PreferentialPrice : 0.9
         * IsOnSale : 1
         * BannerAccessoryUrl1 : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/f8af59bc-aef8-4c62-9b2e-893b31c83a33/9ff2f3bb-6705-4888-9119-60555387c7f5.jpg
         * GoodsNo : 300022
         * MonthlySales : 100
         * CatalogueName : 常备药
         * Distance : 0
         * ShoppingCartNum : 0
         * MonthlyStoreSales : 100
         * Spec : 100mg
         * AssociatedDiseases : 盗汗咳血肺结核，经证实的,∞#咳嗽
         * AssociatedDiagnostics : [{"choose":true,"code":"A00.000","diag_type":"1","diag_typename":"西医主要诊断","name":"霍乱，由于O1群霍乱弧菌，霍乱生物型所致"},{"choose":true,"code":"A00.000x001","diag_type":"1","diag_typename":"西医主要诊断","name":"古典生物型霍乱"},{"choose":true,"code":"A00.100","diag_type":"1","diag_typename":"西医主要诊断","name":"霍乱，由于O1群霍乱弧菌，埃尔托生物型所致"}]
         * StoreName : 测试店铺
         * StartingPrice : 1
         * StarLevel : 0
         */

        private String GoodsId;
        private String MIC_Code;
        private String ItemCode;
        private String ItemType;
        private String EfccAtd;
        private int InventoryQuantity;
        private String Price;
        private String IsPlatformDrug;
        private String fixmedins_code;
        private String GoodsLocation;
        private String CatalogueId;
        private String BannerAccessoryId1;
        private String DrugsClassification;
        private String DrugsClassificationName;
        private String IsPromotion;
        private String GoodsName;
        private String PromotionDiscount;
        private String PreferentialPrice;
        private String IsOnSale;
        private String BannerAccessoryUrl1;
        private String GoodsNo;
        private String MonthlySales;
        private String CatalogueName;
        private String Distance;
        private String ShoppingCartNum;
        private String MonthlyStoreSales;
        private String Spec;
        private String AssociatedDiseases;
        private String AssociatedDiagnostics;
        private String StoreName;
        private String StartingPrice;
        private String StarLevel;

        private String StoreAccessoryUrl;
        private String StoreAdress;
        private String DetailedAddress;
        private String IsShowGoodsSold;
        private String IsShowSold;
        private String IsShowMargin;
        //是否允许企业基金支付（0不允许 1允许）
        private String IsEnterpriseFundPay;

        public String getGoodsId() {
            return GoodsId;
        }

        public void setGoodsId(String GoodsId) {
            this.GoodsId = GoodsId;
        }

        public String getMIC_Code() {
            return MIC_Code;
        }

        public void setMIC_Code(String MIC_Code) {
            this.MIC_Code = MIC_Code;
        }

        public String getItemCode() {
            return ItemCode;
        }

        public void setItemCode(String ItemCode) {
            this.ItemCode = ItemCode;
        }

        public String getItemType() {
            return ItemType;
        }

        public void setItemType(String ItemType) {
            this.ItemType = ItemType;
        }

        public String getEfccAtd() {
            return EfccAtd;
        }

        public void setEfccAtd(String EfccAtd) {
            this.EfccAtd = EfccAtd;
        }

        public int getInventoryQuantity() {
            return InventoryQuantity;
        }

        public void setInventoryQuantity(int InventoryQuantity) {
            this.InventoryQuantity = InventoryQuantity;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String Price) {
            this.Price = Price;
        }

        public String getIsPlatformDrug() {
            return IsPlatformDrug;
        }

        public void setIsPlatformDrug(String IsPlatformDrug) {
            this.IsPlatformDrug = IsPlatformDrug;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getGoodsLocation() {
            return GoodsLocation;
        }

        public void setGoodsLocation(String GoodsLocation) {
            this.GoodsLocation = GoodsLocation;
        }

        public String getCatalogueId() {
            return CatalogueId;
        }

        public void setCatalogueId(String CatalogueId) {
            this.CatalogueId = CatalogueId;
        }

        public String getBannerAccessoryId1() {
            return BannerAccessoryId1;
        }

        public void setBannerAccessoryId1(String BannerAccessoryId1) {
            this.BannerAccessoryId1 = BannerAccessoryId1;
        }

        public String getDrugsClassification() {
            return DrugsClassification;
        }

        public void setDrugsClassification(String DrugsClassification) {
            this.DrugsClassification = DrugsClassification;
        }

        public String getDrugsClassificationName() {
            return DrugsClassificationName;
        }

        public void setDrugsClassificationName(String DrugsClassificationName) {
            this.DrugsClassificationName = DrugsClassificationName;
        }

        public String getIsPromotion() {
            return IsPromotion;
        }

        public void setIsPromotion(String IsPromotion) {
            this.IsPromotion = IsPromotion;
        }

        public String getGoodsName() {
            return GoodsName;
        }

        public void setGoodsName(String GoodsName) {
            this.GoodsName = GoodsName;
        }

        public String getPromotionDiscount() {
            return PromotionDiscount;
        }

        public void setPromotionDiscount(String PromotionDiscount) {
            this.PromotionDiscount = PromotionDiscount;
        }

        public String getPreferentialPrice() {
            return PreferentialPrice;
        }

        public void setPreferentialPrice(String PreferentialPrice) {
            this.PreferentialPrice = PreferentialPrice;
        }

        public String getIsOnSale() {
            return IsOnSale;
        }

        public void setIsOnSale(String IsOnSale) {
            this.IsOnSale = IsOnSale;
        }

        public String getBannerAccessoryUrl1() {
            return BannerAccessoryUrl1;
        }

        public void setBannerAccessoryUrl1(String BannerAccessoryUrl1) {
            this.BannerAccessoryUrl1 = BannerAccessoryUrl1;
        }

        public String getGoodsNo() {
            return GoodsNo;
        }

        public void setGoodsNo(String GoodsNo) {
            this.GoodsNo = GoodsNo;
        }

        public String getMonthlySales() {
            return MonthlySales;
        }

        public void setMonthlySales(String MonthlySales) {
            this.MonthlySales = MonthlySales;
        }

        public String getCatalogueName() {
            return CatalogueName;
        }

        public void setCatalogueName(String CatalogueName) {
            this.CatalogueName = CatalogueName;
        }

        public String getDistance() {
            return Distance;
        }

        public void setDistance(String Distance) {
            this.Distance = Distance;
        }

        public String getShoppingCartNum() {
            return ShoppingCartNum;
        }

        public void setShoppingCartNum(String ShoppingCartNum) {
            this.ShoppingCartNum = ShoppingCartNum;
        }

        public String getMonthlyStoreSales() {
            return MonthlyStoreSales;
        }

        public void setMonthlyStoreSales(String MonthlyStoreSales) {
            this.MonthlyStoreSales = MonthlyStoreSales;
        }

        public String getSpec() {
            return Spec;
        }

        public void setSpec(String Spec) {
            this.Spec = Spec;
        }

        public String getAssociatedDiseases() {
            return AssociatedDiseases;
        }

        public void setAssociatedDiseases(String AssociatedDiseases) {
            this.AssociatedDiseases = AssociatedDiseases;
        }

        public String getAssociatedDiagnostics() {
            return AssociatedDiagnostics;
        }

        public void setAssociatedDiagnostics(String AssociatedDiagnostics) {
            this.AssociatedDiagnostics = AssociatedDiagnostics;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public String getStartingPrice() {
            return StartingPrice;
        }

        public void setStartingPrice(String StartingPrice) {
            this.StartingPrice = StartingPrice;
        }

        public String getStarLevel() {
            return StarLevel;
        }

        public void setStarLevel(String StarLevel) {
            this.StarLevel = StarLevel;
        }

        public String getStoreAccessoryUrl() {
            return StoreAccessoryUrl;
        }

        public void setStoreAccessoryUrl(String storeAccessoryUrl) {
            StoreAccessoryUrl = storeAccessoryUrl;
        }

        public String getStoreAdress() {
            return StoreAdress;
        }

        public void setStoreAdress(String storeAdress) {
            StoreAdress = storeAdress;
        }

        public String getDetailedAddress() {
            return DetailedAddress;
        }

        public void setDetailedAddress(String detailedAddress) {
            DetailedAddress = detailedAddress;
        }

        public String getIsShowGoodsSold() {
            return IsShowGoodsSold;
        }

        public void setIsShowGoodsSold(String isShowGoodsSold) {
            IsShowGoodsSold = isShowGoodsSold;
        }

        public String getIsShowSold() {
            return IsShowSold;
        }

        public void setIsShowSold(String isShowSold) {
            IsShowSold = isShowSold;
        }

        public String getIsShowMargin() {
            return IsShowMargin;
        }

        public void setIsShowMargin(String isShowMargin) {
            IsShowMargin = isShowMargin;
        }

        public String getIsEnterpriseFundPay() {
            return IsEnterpriseFundPay;
        }

        public void setIsEnterpriseFundPay(String isEnterpriseFundPay) {
            IsEnterpriseFundPay = isEnterpriseFundPay;
        }
    }

    //用户搜索接口
    public static void sendSearchResultRequest(final String TAG, String pageindex, String pagesize, String name,String barCode,String BySales, String ByPrice,
                                               String ByDistance,String Latitude,String Longitude, final CustomerJsonCallBack<SearchResultModel_user> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);
        jsonObject.put("name", name);
        jsonObject.put("barCode", barCode);
        jsonObject.put("BySales", BySales);//按销量（1降序，2升序）
        jsonObject.put("ByPrice", ByPrice);//按价格（1降序，2升序）
        jsonObject.put("ByDistance", ByDistance);//按距离（1降序，2升序）
        jsonObject.put("Latitude", Latitude);//纬度
        jsonObject.put("Longitude", Longitude);//经度

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_USERSEARCHLIST_URL, jsonObject.toJSONString(), callback);
    }
}
