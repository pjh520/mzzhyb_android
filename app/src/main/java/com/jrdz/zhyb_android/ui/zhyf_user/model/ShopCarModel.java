package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;
import com.jrdz.zhyb_android.net.RequestData;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-23
 * 描    述：
 * ================================================
 */
public class ShopCarModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-23 18:17:12
     * data : [{"ShoppingCartId":1,"fixmedins_code":"H61080200145","StoreName":"测试店铺","ShoppingCartGoods":[{"GoodsName":"苯丙酮尿症1002","ShoppingCartNum":0,"Price":1,"GoodsNo":"299999","ItemType":1,"BannerAccessoryUrl1":"~/App_Upload/Storage/Medicare_Ticket/a7e199f9-045c-433b-84bd-7e969bb96eb4/f5d1c506-b214-4e24-addb-dfa0bc42c1e4.jpg","IsPromotion":1,"PromotionDiscount":8,"PreferentialPrice":0.8}]}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * ShoppingCartId : 1
         * fixmedins_code : H61080200145
         * StoreName : 测试店铺
         * ShoppingCartGoods : [{"GoodsName":"苯丙酮尿症1002","ShoppingCartNum":0,"Price":1,"GoodsNo":"299999","ItemType":1,"BannerAccessoryUrl1":"~/App_Upload/Storage/Medicare_Ticket/a7e199f9-045c-433b-84bd-7e969bb96eb4/f5d1c506-b214-4e24-addb-dfa0bc42c1e4.jpg","IsPromotion":1,"PromotionDiscount":8,"PreferentialPrice":0.8}]
         */

        private int ShoppingCartId;
        private String fixmedins_code;
        private String StoreName;
        private String StartingPrice;
        private String StoreAccessoryUrl;
        private String Latitude;
        private String Longitude;
        private String fixmedins_type;
        private List<GoodsModel.DataBean> ShoppingCartGoods;
        //程序自用（外部商家）
        private boolean isChoose;

        public int getShoppingCartId() {
            return ShoppingCartId;
        }

        public void setShoppingCartId(int ShoppingCartId) {
            this.ShoppingCartId = ShoppingCartId;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public String getStartingPrice() {
            return StartingPrice;
        }

        public void setStartingPrice(String startingPrice) {
            StartingPrice = startingPrice;
        }

        public String getStoreAccessoryUrl() {
            return StoreAccessoryUrl;
        }

        public void setStoreAccessoryUrl(String storeAccessoryUrl) {
            StoreAccessoryUrl = storeAccessoryUrl;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String latitude) {
            Latitude = latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String longitude) {
            Longitude = longitude;
        }

        public String getFixmedins_type() {
            return fixmedins_type;
        }

        public void setFixmedins_type(String fixmedins_type) {
            this.fixmedins_type = fixmedins_type;
        }

        public List<GoodsModel.DataBean> getShoppingCartGoods() {
            return ShoppingCartGoods;
        }

        public void setShoppingCartGoods(List<GoodsModel.DataBean> ShoppingCartGoods) {
            this.ShoppingCartGoods = ShoppingCartGoods;
        }

        public boolean isChoose() {
            return isChoose;
        }

        public void setChoose(boolean choose) {
            isChoose = choose;
        }
    }

    //购物车商品列表
    public static void sendShopCarRequest(final String TAG, String fixmedins_code,String pageindex,String pagesize,final CustomerJsonCallBack<ShopCarModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fixmedins_code", fixmedins_code);
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_SHOPPINGCARTLIST_URL, jsonObject.toJSONString(), callback);
    }

    //购物车商品列表
    public static void sendShopCarRequest_mana(final String TAG, String fixmedins_code,String pageindex,String pagesize,final CustomerJsonCallBack<ShopCarModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fixmedins_code", fixmedins_code);
        jsonObject.put("pageindex", pageindex);
        jsonObject.put("pagesize", pagesize);

        RequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MERCHANT_SHOPPINGCARTLIST_URL, jsonObject.toJSONString(), callback);
    }
}
