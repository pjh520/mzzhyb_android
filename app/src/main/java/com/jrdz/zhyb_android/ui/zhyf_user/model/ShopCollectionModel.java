package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

import java.util.List;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-02
 * 描    述：
 * ================================================
 */
public class ShopCollectionModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-12-02 17:37:46
     * data : [{"StoreName":"测试店铺","StoreAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/4cf4e4f1-4d0c-486f-920a-baf79a13d9f3/25fb5b5e-057f-4cf2-a97a-7c86744c2d3d.jpg","StartingPrice":1,"AllSales":0,"Distance":0,"StoreCollectionId":2,"UserId":"34c576a1-87e0-44eb-8138-323f3394f2ac","UserName":"15060338985","fixmedins_code":"H61080200145","CreateDT":"2022-12-02 17:23:52","CreateUser":"15060338985"}]
     */

    private String code;
    private String msg;
    private String server_time;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * StoreName : 测试店铺
         * StoreAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/4cf4e4f1-4d0c-486f-920a-baf79a13d9f3/25fb5b5e-057f-4cf2-a97a-7c86744c2d3d.jpg
         * StartingPrice : 1
         * AllSales : 0
         * Distance : 0
         * StoreCollectionId : 2
         * UserId : 34c576a1-87e0-44eb-8138-323f3394f2ac
         * UserName : 15060338985
         * fixmedins_code : H61080200145
         * CreateDT : 2022-12-02 17:23:52
         * CreateUser : 15060338985
         */

        private String StoreName;
        private String StoreAccessoryUrl;
        private String StartingPrice;
        private String AllSales;
        private String Distance;
        private String StoreCollectionId;
        private String UserId;
        private String UserName;
        private String fixmedins_code;
        private String CreateDT;
        private String CreateUser;

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public String getStoreAccessoryUrl() {
            return StoreAccessoryUrl;
        }

        public void setStoreAccessoryUrl(String StoreAccessoryUrl) {
            this.StoreAccessoryUrl = StoreAccessoryUrl;
        }

        public String getStartingPrice() {
            return StartingPrice;
        }

        public void setStartingPrice(String StartingPrice) {
            this.StartingPrice = StartingPrice;
        }

        public String getAllSales() {
            return AllSales;
        }

        public void setAllSales(String AllSales) {
            this.AllSales = AllSales;
        }

        public String getDistance() {
            return Distance;
        }

        public void setDistance(String Distance) {
            this.Distance = Distance;
        }

        public String getStoreCollectionId() {
            return StoreCollectionId;
        }

        public void setStoreCollectionId(String StoreCollectionId) {
            this.StoreCollectionId = StoreCollectionId;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getCreateDT() {
            return CreateDT;
        }

        public void setCreateDT(String CreateDT) {
            this.CreateDT = CreateDT;
        }

        public String getCreateUser() {
            return CreateUser;
        }

        public void setCreateUser(String CreateUser) {
            this.CreateUser = CreateUser;
        }
    }

    //店铺收藏列表
    public static void sendShopCollectionRequest(final String TAG,String pageindex,String pagesize,final CustomerJsonCallBack<ShopCollectionModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageindex", pageindex);//经度
        jsonObject.put("pagesize", pagesize);//纬度

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_STORECOLLECTIONLIST_URL, jsonObject.toJSONString(), callback);
    }
}
