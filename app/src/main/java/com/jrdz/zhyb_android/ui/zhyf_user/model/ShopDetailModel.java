package com.jrdz.zhyb_android.ui.zhyf_user.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-29
 * 描    述：
 * ================================================
 */
public class ShopDetailModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2022-11-29 11:01:53
     * data : {"StoreAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/4cf4e4f1-4d0c-486f-920a-baf79a13d9f3/25fb5b5e-057f-4cf2-a97a-7c86744c2d3d.jpg","BannerAccessoryUrl1":"~/App_Upload/Storage/Medicare_Ticket/a70aa962-107a-4f98-8dc1-636b810fc19b/78234837-bc22-4d96-a655-2c7b031267f7.jpg","BannerAccessoryUrl2":"~/App_Upload/Storage/Medicare_Ticket/e906618b-c19f-40d3-b097-b86b5b0953c5/5d130579-bff6-4056-8629-9a733dbd61da.jpg","BannerAccessoryUrl3":"~/App_Upload/Storage/Medicare_Ticket/7c662da3-27e2-4f0a-8a2a-b8d94bc74f33/e6ae6a87-321d-42a1-9f87-3bb46879d3fe.jpg","BusinessLicenseAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/709c7f4b-c79c-4f6f-8353-fa6721be8e64/4315404c-c9a4-4919-9245-6edb8069902c.jpg","DrugBLAccessoryUrl":"http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/5957486e-bcf6-4ad7-bc8f-73606e45937c/fe50d056-93c0-4a04-87ae-75197fc4671b.jpg","MedicalDeviceBLAccessoryUrl":"","GSCertificateAccessoryUrl":"","StarLevel":0,"MonthlySales":0,"Distance":0,"StoreInformationId":1,"StoreName":"测试店铺","StoreAccessoryId":"4cf4e4f1-4d0c-486f-920a-baf79a13d9f3","IsAutomatic":2,"StartTime":"08:00","EndTime":"22:00","IsShowSold":1,"IsShowGoodsSold":1,"IsShowMargin":1,"StartingPrice":1,"StoreAdress":"黑龙江省哈尔滨市市辖区","DetailedAddress":"涵西街道湖园路在世纪名苑-东北门附近","Telephone":"15060338986","Wechat":"15060338986","DistributionScope":"商家自配5km","BusinessScope":"经营党委","FeaturedServices":"医保","Notice":"这是店铺公告","fixmedins_code":"H61080200145","BannerAccessoryId1":"a70aa962-107a-4f98-8dc1-636b810fc19b","BannerAccessoryId2":"e906618b-c19f-40d3-b097-b86b5b0953c5","BannerAccessoryId3":"7c662da3-27e2-4f0a-8a2a-b8d94bc74f33","Intervaltime":3,"IsOline":2,"OpeningStartTime":"08:00","OpeningEndTime":"21:00","BusinessLicenseAccessoryId":"709c7f4b-c79c-4f6f-8353-fa6721be8e64","DrugBLAccessoryId":"5957486e-bcf6-4ad7-bc8f-73606e45937c","MedicalDeviceBLAccessoryId":"00000000-0000-0000-0000-000000000000","GSCertificateAccessoryId":"00000000-0000-0000-0000-000000000000"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * StoreAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/4cf4e4f1-4d0c-486f-920a-baf79a13d9f3/25fb5b5e-057f-4cf2-a97a-7c86744c2d3d.jpg
         * BannerAccessoryUrl1 : ~/App_Upload/Storage/Medicare_Ticket/a70aa962-107a-4f98-8dc1-636b810fc19b/78234837-bc22-4d96-a655-2c7b031267f7.jpg
         * BannerAccessoryUrl2 : ~/App_Upload/Storage/Medicare_Ticket/e906618b-c19f-40d3-b097-b86b5b0953c5/5d130579-bff6-4056-8629-9a733dbd61da.jpg
         * BannerAccessoryUrl3 : ~/App_Upload/Storage/Medicare_Ticket/7c662da3-27e2-4f0a-8a2a-b8d94bc74f33/e6ae6a87-321d-42a1-9f87-3bb46879d3fe.jpg
         * BusinessLicenseAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/709c7f4b-c79c-4f6f-8353-fa6721be8e64/4315404c-c9a4-4919-9245-6edb8069902c.jpg
         * DrugBLAccessoryUrl : http://113.135.194.23:3080/App_Upload/Storage/Medicare_Ticket/5957486e-bcf6-4ad7-bc8f-73606e45937c/fe50d056-93c0-4a04-87ae-75197fc4671b.jpg
         * MedicalDeviceBLAccessoryUrl :
         * GSCertificateAccessoryUrl :
         * StarLevel : 0
         * MonthlySales : 0
         * Distance : 0
         * StoreInformationId : 1
         * StoreName : 测试店铺
         * StoreAccessoryId : 4cf4e4f1-4d0c-486f-920a-baf79a13d9f3
         * IsAutomatic : 2
         * StartTime : 08:00
         * EndTime : 22:00
         * IsShowSold : 1
         * IsShowGoodsSold : 1
         * IsShowMargin : 1
         * StartingPrice : 1
         * StoreAdress : 黑龙江省哈尔滨市市辖区
         * DetailedAddress : 涵西街道湖园路在世纪名苑-东北门附近
         * Telephone : 15060338986
         * Wechat : 15060338986
         * DistributionScope : 商家自配5km
         * BusinessScope : 经营党委
         * FeaturedServices : 医保
         * Notice : 这是店铺公告
         * fixmedins_code : H61080200145
         * BannerAccessoryId1 : a70aa962-107a-4f98-8dc1-636b810fc19b
         * BannerAccessoryId2 : e906618b-c19f-40d3-b097-b86b5b0953c5
         * BannerAccessoryId3 : 7c662da3-27e2-4f0a-8a2a-b8d94bc74f33
         * Intervaltime : 3
         * IsOline : 2
         * OpeningStartTime : 08:00
         * OpeningEndTime : 21:00
         * BusinessLicenseAccessoryId : 709c7f4b-c79c-4f6f-8353-fa6721be8e64
         * DrugBLAccessoryId : 5957486e-bcf6-4ad7-bc8f-73606e45937c
         * MedicalDeviceBLAccessoryId : 00000000-0000-0000-0000-000000000000
         * GSCertificateAccessoryId : 00000000-0000-0000-0000-000000000000
         */

        private String StoreAccessoryUrl;
        private String BannerAccessoryUrl1;
        private String BannerAccessoryUrl2;
        private String BannerAccessoryUrl3;
        private String BusinessLicenseAccessoryUrl;
        private String DrugBLAccessoryUrl;
        private String MedicalDeviceBLAccessoryUrl;
        private String GSCertificateAccessoryUrl;
        private String StarLevel;
        private String MonthlySales;
        private String Distance;
        private String StoreName;
        private String StartingPrice;
        private String StoreAdress;
        private String DetailedAddress;
        private String Telephone;
        private String Wechat;
        private String DistributionScope;
        private String BusinessScope;
        private String FeaturedServices;
        private String Notice;
        private String fixmedins_code;
        private String Intervaltime;
        private String OpeningStartTime;
        private String OpeningEndTime;
        private String MonthlyStoreSales;
        private String IsCollection;
        private String Latitude;
        private String Longitude;
        private String fixmedins_type;
        private String IsShowSold;
        //是否允许企业基金支付（0不允许 1允许）
        private String IsEnterpriseFundPay;

        public String getStoreAccessoryUrl() {
            return StoreAccessoryUrl;
        }

        public void setStoreAccessoryUrl(String StoreAccessoryUrl) {
            this.StoreAccessoryUrl = StoreAccessoryUrl;
        }

        public String getBannerAccessoryUrl1() {
            return BannerAccessoryUrl1;
        }

        public void setBannerAccessoryUrl1(String BannerAccessoryUrl1) {
            this.BannerAccessoryUrl1 = BannerAccessoryUrl1;
        }

        public String getBannerAccessoryUrl2() {
            return BannerAccessoryUrl2;
        }

        public void setBannerAccessoryUrl2(String BannerAccessoryUrl2) {
            this.BannerAccessoryUrl2 = BannerAccessoryUrl2;
        }

        public String getBannerAccessoryUrl3() {
            return BannerAccessoryUrl3;
        }

        public void setBannerAccessoryUrl3(String BannerAccessoryUrl3) {
            this.BannerAccessoryUrl3 = BannerAccessoryUrl3;
        }

        public String getBusinessLicenseAccessoryUrl() {
            return BusinessLicenseAccessoryUrl;
        }

        public void setBusinessLicenseAccessoryUrl(String BusinessLicenseAccessoryUrl) {
            this.BusinessLicenseAccessoryUrl = BusinessLicenseAccessoryUrl;
        }

        public String getDrugBLAccessoryUrl() {
            return DrugBLAccessoryUrl;
        }

        public void setDrugBLAccessoryUrl(String DrugBLAccessoryUrl) {
            this.DrugBLAccessoryUrl = DrugBLAccessoryUrl;
        }

        public String getMedicalDeviceBLAccessoryUrl() {
            return MedicalDeviceBLAccessoryUrl;
        }

        public void setMedicalDeviceBLAccessoryUrl(String MedicalDeviceBLAccessoryUrl) {
            this.MedicalDeviceBLAccessoryUrl = MedicalDeviceBLAccessoryUrl;
        }

        public String getGSCertificateAccessoryUrl() {
            return GSCertificateAccessoryUrl;
        }

        public void setGSCertificateAccessoryUrl(String GSCertificateAccessoryUrl) {
            this.GSCertificateAccessoryUrl = GSCertificateAccessoryUrl;
        }

        public String getStarLevel() {
            return StarLevel;
        }

        public void setStarLevel(String StarLevel) {
            this.StarLevel = StarLevel;
        }

        public String getMonthlySales() {
            return MonthlySales;
        }

        public void setMonthlySales(String MonthlySales) {
            this.MonthlySales = MonthlySales;
        }

        public String getDistance() {
            return Distance;
        }

        public void setDistance(String Distance) {
            this.Distance = Distance;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public String getStartingPrice() {
            return StartingPrice;
        }

        public void setStartingPrice(String StartingPrice) {
            this.StartingPrice = StartingPrice;
        }

        public String getStoreAdress() {
            return StoreAdress;
        }

        public void setStoreAdress(String StoreAdress) {
            this.StoreAdress = StoreAdress;
        }

        public String getDetailedAddress() {
            return DetailedAddress;
        }

        public void setDetailedAddress(String DetailedAddress) {
            this.DetailedAddress = DetailedAddress;
        }

        public String getTelephone() {
            return Telephone;
        }

        public void setTelephone(String Telephone) {
            this.Telephone = Telephone;
        }

        public String getWechat() {
            return Wechat;
        }

        public void setWechat(String Wechat) {
            this.Wechat = Wechat;
        }

        public String getDistributionScope() {
            return DistributionScope;
        }

        public void setDistributionScope(String DistributionScope) {
            this.DistributionScope = DistributionScope;
        }

        public String getBusinessScope() {
            return BusinessScope;
        }

        public void setBusinessScope(String BusinessScope) {
            this.BusinessScope = BusinessScope;
        }

        public String getFeaturedServices() {
            return FeaturedServices;
        }

        public void setFeaturedServices(String FeaturedServices) {
            this.FeaturedServices = FeaturedServices;
        }

        public String getNotice() {
            return Notice;
        }

        public void setNotice(String Notice) {
            this.Notice = Notice;
        }

        public String getFixmedins_code() {
            return fixmedins_code;
        }

        public void setFixmedins_code(String fixmedins_code) {
            this.fixmedins_code = fixmedins_code;
        }

        public String getIntervaltime() {
            return Intervaltime;
        }

        public void setIntervaltime(String Intervaltime) {
            this.Intervaltime = Intervaltime;
        }

        public String getOpeningStartTime() {
            return OpeningStartTime;
        }

        public void setOpeningStartTime(String OpeningStartTime) {
            this.OpeningStartTime = OpeningStartTime;
        }

        public String getOpeningEndTime() {
            return OpeningEndTime;
        }

        public void setOpeningEndTime(String OpeningEndTime) {
            this.OpeningEndTime = OpeningEndTime;
        }

        public String getMonthlyStoreSales() {
            return MonthlyStoreSales;
        }

        public void setMonthlyStoreSales(String monthlyStoreSales) {
            MonthlyStoreSales = monthlyStoreSales;
        }

        public String getIsCollection() {
            return IsCollection;
        }

        public void setIsCollection(String isCollection) {
            IsCollection = isCollection;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String latitude) {
            Latitude = latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String longitude) {
            Longitude = longitude;
        }

        public String getFixmedins_type() {
            return fixmedins_type;
        }

        public void setFixmedins_type(String fixmedins_type) {
            this.fixmedins_type = fixmedins_type;
        }

        public String getIsShowSold() {
            return IsShowSold;
        }

        public void setIsShowSold(String isShowSold) {
            IsShowSold = isShowSold;
        }

        public String getIsEnterpriseFundPay() {
            return IsEnterpriseFundPay;
        }

        public void setIsEnterpriseFundPay(String isEnterpriseFundPay) {
            IsEnterpriseFundPay = isEnterpriseFundPay;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.StoreAccessoryUrl);
            dest.writeString(this.BannerAccessoryUrl1);
            dest.writeString(this.BannerAccessoryUrl2);
            dest.writeString(this.BannerAccessoryUrl3);
            dest.writeString(this.BusinessLicenseAccessoryUrl);
            dest.writeString(this.DrugBLAccessoryUrl);
            dest.writeString(this.MedicalDeviceBLAccessoryUrl);
            dest.writeString(this.GSCertificateAccessoryUrl);
            dest.writeString(this.StarLevel);
            dest.writeString(this.MonthlySales);
            dest.writeString(this.Distance);
            dest.writeString(this.StoreName);
            dest.writeString(this.StartingPrice);
            dest.writeString(this.StoreAdress);
            dest.writeString(this.DetailedAddress);
            dest.writeString(this.Telephone);
            dest.writeString(this.Wechat);
            dest.writeString(this.DistributionScope);
            dest.writeString(this.BusinessScope);
            dest.writeString(this.FeaturedServices);
            dest.writeString(this.Notice);
            dest.writeString(this.fixmedins_code);
            dest.writeString(this.Intervaltime);
            dest.writeString(this.OpeningStartTime);
            dest.writeString(this.OpeningEndTime);
            dest.writeString(this.MonthlyStoreSales);
            dest.writeString(this.IsCollection);
            dest.writeString(this.Latitude);
            dest.writeString(this.Longitude);
            dest.writeString(this.fixmedins_type);
            dest.writeString(this.IsShowSold);
            dest.writeString(this.IsEnterpriseFundPay);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.StoreAccessoryUrl = in.readString();
            this.BannerAccessoryUrl1 = in.readString();
            this.BannerAccessoryUrl2 = in.readString();
            this.BannerAccessoryUrl3 = in.readString();
            this.BusinessLicenseAccessoryUrl = in.readString();
            this.DrugBLAccessoryUrl = in.readString();
            this.MedicalDeviceBLAccessoryUrl = in.readString();
            this.GSCertificateAccessoryUrl = in.readString();
            this.StarLevel = in.readString();
            this.MonthlySales = in.readString();
            this.Distance = in.readString();
            this.StoreName = in.readString();
            this.StartingPrice = in.readString();
            this.StoreAdress = in.readString();
            this.DetailedAddress = in.readString();
            this.Telephone = in.readString();
            this.Wechat = in.readString();
            this.DistributionScope = in.readString();
            this.BusinessScope = in.readString();
            this.FeaturedServices = in.readString();
            this.Notice = in.readString();
            this.fixmedins_code = in.readString();
            this.Intervaltime = in.readString();
            this.OpeningStartTime = in.readString();
            this.OpeningEndTime = in.readString();
            this.MonthlyStoreSales = in.readString();
            this.IsCollection = in.readString();
            this.Latitude = in.readString();
            this.Longitude = in.readString();
            this.fixmedins_type = in.readString();
            this.IsShowSold = in.readString();
            this.IsEnterpriseFundPay = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    //店铺详情
    public static void sendShopDetailRequest_user(final String TAG, String fixmedins_code,String Latitude,String Longitude,
                                                  final CustomerJsonCallBack<ShopDetailModel> callback) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fixmedins_code", fixmedins_code);//机构编码
        jsonObject.put("Latitude", Latitude);//机构编码
        jsonObject.put("Longitude", Longitude);//机构编码

        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_USER_QUERYSTOREINFO_URL, jsonObject.toJSONString(), callback);
    }
}
