package com.jrdz.zhyb_android.ui.zhyf_user.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.frame.compiler.widget.wheel.entity.IWheelEntity;

import java.util.List;

/**
 * ================================================
 * 项目名称：SuperWarehouse_Android
 * 包    名：com.tjl.super_warehouse.ui.mine.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2019/11/10 0010
 * 描    述：
 * 1.资质问题：资质不全，资质不符，盗用资质，资质失效，资质造假
 * 2.超出经营范围：日用品，医疗器械，保健品
 * 3.违规商品：涉毒，假货，涉黄，涉暴，涉政，野生动物，香烟/电子烟，易燃易爆品
 * 4.品牌侵权：LOGO图侵权，店铺名称侵权
 * 5.虚假门店：无实体店，地址不一致，商家倒闭转让
 * ================================================
 */
public class TwoLinkWheelModel implements IWheelEntity, Parcelable {
    private String text;

    public TwoLinkWheelModel(String text) {
        this.text = text;
    }

    private List<ChildBean> childBean;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<ChildBean> getChildBean() {
        return childBean;
    }

    public void setChildBean(List<ChildBean> childBean) {
        this.childBean = childBean;
    }

    @Override
    public String getWheelText() {
        return text;
    }

    public static class ChildBean implements IWheelEntity, Parcelable {
        private String text;

        public ChildBean(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        @Override
        public String getWheelText() {
            return text;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.text);
        }

        public ChildBean() {
        }

        protected ChildBean(Parcel in) {
            this.text = in.readString();
        }

        public static final Parcelable.Creator<ChildBean> CREATOR = new Parcelable.Creator<ChildBean>() {
            @Override
            public ChildBean createFromParcel(Parcel source) {
                return new ChildBean(source);
            }

            @Override
            public ChildBean[] newArray(int size) {
                return new ChildBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.text);
        dest.writeTypedList(this.childBean);
    }

    public TwoLinkWheelModel() {
    }

    protected TwoLinkWheelModel(Parcel in) {
        this.text = in.readString();
        this.childBean = in.createTypedArrayList(ChildBean.CREATOR);
    }

    public static final Parcelable.Creator<TwoLinkWheelModel> CREATOR = new Parcelable.Creator<TwoLinkWheelModel>() {
        @Override
        public TwoLinkWheelModel createFromParcel(Parcel source) {
            return new TwoLinkWheelModel(source);
        }

        @Override
        public TwoLinkWheelModel[] newArray(int size) {
            return new TwoLinkWheelModel[size];
        }
    };
}
