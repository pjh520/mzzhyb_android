package com.jrdz.zhyb_android.ui.zhyf_user.model;

import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.net.InsuredRequestData;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-01-11
 * 描    述：
 * ================================================
 */
public class UserIsReadMessageCountModel {
    /**
     * code : 1
     * msg : 成功
     * server_time : 2023-01-31 09:42:14
     * data : {"MessageCount":"0"}
     */

    private String code;
    private String msg;
    private String server_time;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * MessageCount : 0
         */

        private String MessageCount;

        public String getMessageCount() {
            return MessageCount;
        }

        public void setMessageCount(String MessageCount) {
            this.MessageCount = MessageCount;
        }
    }

    //店铺收藏列表
    public static void sendUserIsReadMessageCountRequest(final String TAG,final CustomerJsonCallBack<UserIsReadMessageCountModel> callback) {
        InsuredRequestData.requesNetWork_Json(TAG, Constants.BASE_URL + Constants.Api.GET_MANA_USERISREADMESSAGECOUNT_URL,"", callback);
    }
}
