package com.jrdz.zhyb_android.ui.zhyf_user.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-20
 * 描    述：
 * ================================================
 */
public class VisitorDiseaseHistoryModel {
    protected String isSelectPastMedical;//是否选中 0未选中 1选中
    protected String selectPastMedical;
    protected String etPastMedical;

    protected String isSelectAllergy;//是否选中 0未选中 1选中
    protected String selectAllergy;
    protected String etAllergy;

    protected String isSelectFamily;//是否选中 0未选中 1选中
    protected String selectFamily;
    protected String etFamily;

    protected String isSelectLiver;//是否选中 0未选中 1选中

    protected String isSelectKidney;//是否选中 0未选中 1选中

    protected String isSelectPregnancyLactation;//是否选中 0未选中 1选中

    public VisitorDiseaseHistoryModel() {}

    public VisitorDiseaseHistoryModel(String isSelectPastMedical, String selectPastMedical, String etPastMedical,
                                      String isSelectAllergy, String selectAllergy, String etAllergy,
                                      String isSelectFamily, String selectFamily, String etFamily,
                                      String isSelectLiver, String isSelectKidney, String isSelectPregnancyLactation) {
        this.isSelectPastMedical = isSelectPastMedical;
        this.selectPastMedical = selectPastMedical;
        this.etPastMedical = etPastMedical;
        this.isSelectAllergy = isSelectAllergy;
        this.selectAllergy = selectAllergy;
        this.etAllergy = etAllergy;
        this.isSelectFamily = isSelectFamily;
        this.selectFamily = selectFamily;
        this.etFamily = etFamily;
        this.isSelectLiver = isSelectLiver;
        this.isSelectKidney = isSelectKidney;
        this.isSelectPregnancyLactation = isSelectPregnancyLactation;
    }

    public String getIsSelectPastMedical() {
        return isSelectPastMedical;
    }

    public void setIsSelectPastMedical(String isSelectPastMedical) {
        this.isSelectPastMedical = isSelectPastMedical;
    }

    public String getSelectPastMedical() {
        return selectPastMedical;
    }

    public void setSelectPastMedical(String selectPastMedical) {
        this.selectPastMedical = selectPastMedical;
    }

    public String getEtPastMedical() {
        return etPastMedical;
    }

    public void setEtPastMedical(String etPastMedical) {
        this.etPastMedical = etPastMedical;
    }

    public String getIsSelectAllergy() {
        return isSelectAllergy;
    }

    public void setIsSelectAllergy(String isSelectAllergy) {
        this.isSelectAllergy = isSelectAllergy;
    }

    public String getSelectAllergy() {
        return selectAllergy;
    }

    public void setSelectAllergy(String selectAllergy) {
        this.selectAllergy = selectAllergy;
    }

    public String getEtAllergy() {
        return etAllergy;
    }

    public void setEtAllergy(String etAllergy) {
        this.etAllergy = etAllergy;
    }

    public String getIsSelectFamily() {
        return isSelectFamily;
    }

    public void setIsSelectFamily(String isSelectFamily) {
        this.isSelectFamily = isSelectFamily;
    }

    public String getSelectFamily() {
        return selectFamily;
    }

    public void setSelectFamily(String selectFamily) {
        this.selectFamily = selectFamily;
    }

    public String getEtFamily() {
        return etFamily;
    }

    public void setEtFamily(String etFamily) {
        this.etFamily = etFamily;
    }

    public String getIsSelectLiver() {
        return isSelectLiver;
    }

    public void setIsSelectLiver(String isSelectLiver) {
        this.isSelectLiver = isSelectLiver;
    }

    public String getIsSelectKidney() {
        return isSelectKidney;
    }

    public void setIsSelectKidney(String isSelectKidney) {
        this.isSelectKidney = isSelectKidney;
    }

    public String getIsSelectPregnancyLactation() {
        return isSelectPregnancyLactation;
    }

    public void setIsSelectPregnancyLactation(String isSelectPregnancyLactation) {
        this.isSelectPregnancyLactation = isSelectPregnancyLactation;
    }
}
