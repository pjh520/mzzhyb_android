package com.jrdz.zhyb_android.ui.zhyf_user.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.ui.zhyf_user.model
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-19
 * 描    述：
 * ================================================
 */
public class VisitorModel implements Parcelable {
    private String name;
    private String idCardNo;
    private String sex;
    private String sexText;
    private String age;
    private String accessoryId;
    private String accessoryUrl;
    private String isOneself;
    private String phone;

    public VisitorModel(String name, String idCardNo, String sex, String sexText, String age, String accessoryId, String accessoryUrl, String isOneself) {
        this.name = name;
        this.idCardNo = idCardNo;
        this.sex = sex;
        this.sexText = sexText;
        this.age = age;
        this.accessoryId = accessoryId;
        this.accessoryUrl = accessoryUrl;
        this.isOneself = isOneself;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public String getSexText() {
        return sexText;
    }

    public void setSexText(String sexText) {
        this.sexText = sexText;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAccessoryId() {
        return accessoryId;
    }

    public void setAccessoryId(String accessoryId) {
        this.accessoryId = accessoryId;
    }

    public String getAccessoryUrl() {
        return accessoryUrl;
    }

    public void setAccessoryUrl(String accessoryUrl) {
        this.accessoryUrl = accessoryUrl;
    }

    public String getIsOneself() {
        return isOneself;
    }

    public void setIsOneself(String isOneself) {
        this.isOneself = isOneself;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.idCardNo);
        dest.writeString(this.sex);
        dest.writeString(this.sexText);
        dest.writeString(this.age);
        dest.writeString(this.accessoryId);
        dest.writeString(this.accessoryUrl);
        dest.writeString(this.isOneself);
        dest.writeString(this.phone);
    }

    protected VisitorModel(Parcel in) {
        this.name = in.readString();
        this.idCardNo = in.readString();
        this.sex = in.readString();
        this.sexText = in.readString();
        this.age = in.readString();
        this.accessoryId = in.readString();
        this.accessoryUrl = in.readString();
        this.isOneself = in.readString();
        this.phone = in.readString();
    }

    public static final Parcelable.Creator<VisitorModel> CREATOR = new Parcelable.Creator<VisitorModel>() {
        @Override
        public VisitorModel createFromParcel(Parcel source) {
            return new VisitorModel(source);
        }

        @Override
        public VisitorModel[] newArray(int size) {
            return new VisitorModel[size];
        }
    };
}
