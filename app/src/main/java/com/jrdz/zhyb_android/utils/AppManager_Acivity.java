package com.jrdz.zhyb_android.utils;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.frame.compiler.utils.LogUtils;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;

import java.util.Stack;

/**
 * ================================================
 * 项目名称：SuperWarehouse_Android
 * 包    名：com.tjl.super_warehouse.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2019/12/27
 * 描    述：activity 管理
 * ================================================
 */
public class AppManager_Acivity implements Application.ActivityLifecycleCallbacks{
    private int appCount = 0;
    private boolean isBackground=false;

    /**
     * 内部类实现单例模式
     * 延迟加载，减少内存开销
     */
    private static class SingletonHolder {
        private static AppManager_Acivity instance = new AppManager_Acivity();
    }

    /**
     * 私有的构造函数
     */
    private AppManager_Acivity() {}

    public static AppManager_Acivity getInstance() {
        return AppManager_Acivity.SingletonHolder.instance;
    }

    // Activity栈
    private static Stack<Activity> activityStack;

    /**
     * 添加Activity到堆栈
     */
    public void addActivity(Activity activity) {
        if (activityStack == null) {
            activityStack = new Stack<Activity>();
        }
        activityStack.add(activity);
    }

    /**
     * 获取当前Activity（堆栈中最后一个压入的）
     */
    public Activity currentActivity() {
        if (activityStack==null)return null;
        Activity activity = activityStack.lastElement();
        return activity;
    }

    /**
     * 结束当前Activity（堆栈中最后一个压入的）
     */
    public void finishActivity() {
        Activity activity = activityStack.lastElement();
        finishActivity(activity);
    }

    /**
     * 结束指定的Activity
     */
    public void finishActivity(Activity activity) {
        if (activity != null) {
            activityStack.remove(activity);
            activity.finish();
            activity = null;
        }
    }

    /**
     * 结束指定类名的Activity
     */
    public void finishActivity(Class<?> cls) {
        for (Activity activity : activityStack) {
            if (activity.getClass().equals(cls)) {
                activity.finish();
                activity=null;
            }
        }
    }

    /**
     * 结束所有Activity
     */
    public void finishAllActivity() {
        for (int i = 0; i < activityStack.size(); i++) {
            if (null != activityStack.get(i)) {
                activityStack.get(i).finish();
            }
        }
        activityStack.clear();
    }

    /**
     * 是否有指定类名的Activity
     */
    public boolean hasActivity(Class<?> cls) {
        for (Activity activity : activityStack) {
            if (activity.getClass().equals(cls)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 退出应用程序
     */
    public void AppExit() {
        try {
            finishAllActivity();
            // 退出JVM(java虚拟机),释放所占内存资源,0表示正常退出(非0的都为异常退出) （注意：调用此 API 可能导致当前 Activity onDestroy 方法无法正常回调）
            System.exit(0);
            // 从操作系统中结束掉当前程序的进程
//            android.os.Process.killProcess(android.os.Process.myPid());
        } catch (Exception e) {
        }
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        addActivity(activity);
        LogUtils.e("activityNum","AppManager_Acivity+activityNum="+activityStack.size());
    }

    @Override
    public void onActivityStarted(Activity activity) {
        appCount++;
        if (isBackground&&appCount==1){
            isBackground=false;
            MsgBus.sendIsBackground().post(true);
        }
        //关键代码,这五行也可以放在BaseActivity里也行 app灰白化
//        Paint paint = new Paint();
//        ColorMatrix cm = new ColorMatrix();
//        cm.setSaturation(0);
//        paint.setColorFilter(new ColorMatrixColorFilter(cm));
//        activity.getWindow().getDecorView().setLayerType(View.LAYER_TYPE_HARDWARE, paint);
    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {
        appCount--;
        if (appCount<=0){
            isBackground= true;
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        finishActivity(activity);
        LogUtils.e("activityNum","AppManager_Acivity+activityNum="+activityStack.size());
    }
}
