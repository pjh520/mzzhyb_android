package com.jrdz.zhyb_android.utils;

import android.app.Activity;

import com.jrdz.zhyb_android.ui.home.activity.ScanPayGoodDetailActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.GoodDetailActivity;

import java.util.Stack;

/**
 * ================================================
 * 项目名称：SuperWarehouse_Android
 * 包    名：com.tjl.super_warehouse.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2019/12/27
 * 描    述：activity 管理
 * ================================================
 */
public class AppManager_GoodsDetailsAcivity {
    /**
     * 内部类实现单例模式
     * 延迟加载，减少内存开销
     */
    private static class SingletonHolder {
        private static AppManager_GoodsDetailsAcivity instance = new AppManager_GoodsDetailsAcivity();
    }

    /**
     * 私有的构造函数
     */
    private AppManager_GoodsDetailsAcivity() {}

    public static AppManager_GoodsDetailsAcivity getInstance() {
        return AppManager_GoodsDetailsAcivity.SingletonHolder.instance;
    }

    // Activity栈(存放商品详情页面)
    private static Stack<GoodDetailActivity> productDdetailActivityStack=new Stack<>();
    private static Stack<ScanPayGoodDetailActivity> productDdetailActivityStack2=new Stack<>();
    //商品详情页最多个数
    public static final int PRODUCT_DETAIL_ACTIVITY_MAX_NUM = 5;

    /**
     * 添加Activity到堆栈
     */
    public void addActivity(GoodDetailActivity activity) {
        productDdetailActivityStack.add(activity);
    }
    /**
     * 添加Activity到堆栈
     */
    public void addActivity(ScanPayGoodDetailActivity activity) {
        productDdetailActivityStack2.add(activity);
    }
    /**
     * 获取当前Activity（堆栈中最后一个压入的）
     */
    public Activity currentActivity() {
        Activity activity = productDdetailActivityStack.lastElement();
        return activity;
    }

    /**
     * 结束当前Activity（堆栈中最后一个压入的）
     */
    public void finishActivity() {
        Activity activity = productDdetailActivityStack.lastElement();
        finishActivity(activity);
    }

    /**
     * 结束当前Activity（堆栈中第一个压入的）
     */
    public void finishFirstActivity() {
        Activity activity = productDdetailActivityStack.peek();
        finishActivity(activity);
    }
    /**
     * 结束当前Activity（堆栈中第一个压入的）
     */
    public void finishFirstActivity2() {
        Activity activity = productDdetailActivityStack2.peek();
        finishActivity2(activity);
    }
    /**
     * 结束指定的Activity
     */
    public void finishActivity(Activity activity) {
        if (activity != null) {
            productDdetailActivityStack.remove(activity);
            activity.finish();
            activity = null;
        }
    }
    /**
     * 结束指定的Activity
     */
    public void finishActivity2(Activity activity) {
        if (activity != null) {
            productDdetailActivityStack2.remove(activity);
            activity.finish();
            activity = null;
        }
    }
    /**
     * 结束所有Activity
     */
    public void finishAllActivity() {
        for (int i = 0; i < productDdetailActivityStack.size(); i++) {
            if (null != productDdetailActivityStack.get(i)) {
                productDdetailActivityStack.get(i).finish();
            }
        }
        productDdetailActivityStack.clear();
    }

    //获取栈的activity数量
    public int getSize(){
        return productDdetailActivityStack.size();
    }

    public int getSize2(){
        return productDdetailActivityStack2.size();
    }
}
