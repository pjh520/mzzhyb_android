package com.jrdz.zhyb_android.utils;

import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-09-30
 * 描    述：
 * ================================================
 */
public class ChineseFilter implements InputFilter {
    /**
     * 偏旁部首
     */
    public static final String CHINESE_RADICAL_DIGISTS = "[犭凵巛冖氵廴纟讠礻亻钅宀亠忄辶弋饣刂阝冫卩疒艹疋豸冂匸扌丬屮衤勹彳彡]";

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        if (TextUtils.isEmpty(source)){
            return "";
        }

        for (int i = start; i < end; i++) {
            if (stringFilterChinese(source)||CHINESE_RADICAL_DIGISTS.contains(source)) {
                return "";
            }
        }
        return null;
    }

    /**
     * 限制只能输入汉字，过滤非汉字
     *
     * @param str 输入值
     * @return true 非汉字；false 汉字
     */
    public boolean stringFilterChinese(CharSequence str) {
        //只允许汉字，正则表达式匹配出所有非汉字
        String regEx = "[^\u4E00-\u9FA5]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        if (m.find()) {
            return true;
        } else {
            return false;
        }
    }
}
