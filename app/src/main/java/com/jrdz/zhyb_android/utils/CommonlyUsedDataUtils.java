package com.jrdz.zhyb_android.utils;

import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.tabLayout.listener.CustomTabEntity;
import com.frame.compiler.widget.toast.ToastUtil;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.database.DataDicModel;
import com.jrdz.zhyb_android.ui.home.model.HomeSortModel;
import com.jrdz.zhyb_android.ui.home.model.KVModel;
import com.jrdz.zhyb_android.ui.home.model.LargeVersionModel;
import com.jrdz.zhyb_android.ui.home.model.MonthSettModel;
import com.jrdz.zhyb_android.ui.home.model.MonthSettTypeModel;
import com.jrdz.zhyb_android.ui.home.model.QueryComFunctModel;
import com.jrdz.zhyb_android.ui.mine.model.UnionsModel;
import com.jrdz.zhyb_android.ui.settlement.model.QueryPersonalInfoModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.OtherSortModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PayTypeModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortMineModel;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.StoreStatusModel;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopDetailMoreModel;

import org.litepal.LitePal;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * ================================================
 * 项目名称：dgonline-android
 * 包    名：com.aten.dgonline_android.ui.Home
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2018/11/13
 * 描    述：常用的数据
 * ================================================
 */
public class CommonlyUsedDataUtils {
    /**
     * 内部类实现单例模式
     * 延迟加载，减少内存开销
     */
    private static class SingletonHolder {
        private static CommonlyUsedDataUtils instance = new CommonlyUsedDataUtils();
    }

    /**
     * 私有的构造函数
     */
    private CommonlyUsedDataUtils() {
    }

    public static CommonlyUsedDataUtils getInstance() {
        return CommonlyUsedDataUtils.SingletonHolder.instance;
    }

    //获取科室全部数据
    public List<DataDicModel> getCatyData() {
        return LitePal.select("value", "label").where("type=?", "DEPT").order("sort asc").find(DataDicModel.class);
//        List<DataDicModel> dataDicModels=new ArrayList<>();
//
//        DataDicModel dataDicModel01=new DataDicModel("预防保健科",1,"dept","A01","1");
//        DataDicModel dataDicModel02=new DataDicModel("全科医疗科",2,"dept","A02","1");
//        DataDicModel dataDicModel03=new DataDicModel("内科",3,"dept","A03","1");
//        DataDicModel dataDicModel04=new DataDicModel("呼吸内科专业",4,"dept","A03.01","1");
//        DataDicModel dataDicModel05=new DataDicModel("消化内科专业",5,"dept","A03.02","1");
//        DataDicModel dataDicModel06=new DataDicModel("神经内科专业",6,"dept","A03.03","1");
//        DataDicModel dataDicModel07=new DataDicModel("心血管内科专业",7,"dept","A03.04","1");
//        DataDicModel dataDicModel08=new DataDicModel("血液内科专业",8,"dept","A03.05","1");
//        DataDicModel dataDicModel09=new DataDicModel("肾病学专业",8,"dept","A03.06","1");
//        DataDicModel dataDicModel10=new DataDicModel("内分泌专业",8,"dept","A03.07","1");
//
//        dataDicModels.add(dataDicModel01);dataDicModels.add(dataDicModel02);dataDicModels.add(dataDicModel03);
//        dataDicModels.add(dataDicModel04);dataDicModels.add(dataDicModel05);dataDicModels.add(dataDicModel06);
//        dataDicModels.add(dataDicModel07);dataDicModels.add(dataDicModel08);dataDicModels.add(dataDicModel09);
//        dataDicModels.add(dataDicModel10);
//
//        return dataDicModels;
    }

    //获取诊断类别全部数据
    public List<DataDicModel> getDiagTypeData() {
        return LitePal.select("value", "label").where("type=?", "DIAG_TYPE").order("sort asc").find(DataDicModel.class);
    }

    //获取性别数据
    public List<DataDicModel> getGendData() {
        return LitePal.select("value", "label").where("type=?", "GEND").order("sort asc").find(DataDicModel.class);
    }

    //获取民族数据
    public List<DataDicModel> getNatyData() {
        return LitePal.select("value", "label").where("type=?", "NATY").order("sort asc").find(DataDicModel.class);
    }

    //获取人员证件类型数据
    public List<DataDicModel> getPsnCertTypeData() {
        return LitePal.select("value", "label").where("type=?", "PSN_CERT_TYPE").order("sort asc").find(DataDicModel.class);
    }

    //获取人员类别数据
    public List<DataDicModel> getPsnTypeData() {
        return LitePal.select("value", "label").where("type=?", "PSN_TYPE").order("sort asc").find(DataDicModel.class);
    }

    //获取险种类别全部数据
    public List<DataDicModel> getInsutypeData() {
        return LitePal.select("value", "label").where("type=?", "INSUTYPE").order("sort asc").find(DataDicModel.class);
    }

    //获取公务员标志数据
    public List<DataDicModel> getCvlservFlagData() {
        return LitePal.select("value", "label").where("type=?", "CVLSERV_FLAG").order("sort asc").find(DataDicModel.class);
    }

    //获取参保状态数据
    public List<DataDicModel> getPsnInsuStasData() {
        return LitePal.select("value", "label").where("type=?", "PSN_INSU_STAS").order("sort asc").find(DataDicModel.class);
    }

    //获取个人结算方式数据
    public List<DataDicModel> getPsnSetlwayData() {
        return LitePal.select("value", "label").where("type=?", "PSN_SETLWAY").order("sort asc").find(DataDicModel.class);
    }

    //获取疾病分类数据
    public List<DataDicModel> getDiseTypeData() {
        return LitePal.select("value", "label").where("type=?", "DISE_TYPE").order("sort asc").find(DataDicModel.class);
    }

    //获取基金支付类型数据
    public List<DataDicModel> getFundPayTypeData() {
        return LitePal.select("value", "label").where("type=?", "FUND_PAY_TYPE").order("sort asc").find(DataDicModel.class);
    }

    //获取基金款项待遇享受标志数据
    public List<DataDicModel> getTrtEnjymntFlagData() {
        return LitePal.select("value", "label").where("type=?", "TRT_ENJYMNT_FLAG").order("sort asc").find(DataDicModel.class);
    }

    //获取清算类别数据
    public List<DataDicModel> getClrTypeData() {
        return LitePal.select("value", "label").where("type=?", "CLR_TYPE").order("sort asc").find(DataDicModel.class);
    }

    //获取退费结算标志数据
    public List<DataDicModel> getRefdSetlFlagData() {
        return LitePal.select("value", "label").where("type=?", "REFD_SETL_FLAG").order("sort asc").find(DataDicModel.class);
    }

    //获取对账结果数据
    public List<DataDicModel> getStmtRsltData() {
        return LitePal.select("value", "label").where("type=?", "STMT_RSLT").order("sort asc").find(DataDicModel.class);
    }

    //获取目录类型数据
    public List<DataDicModel> getListTypeData() {
        List<DataDicModel> datas = LitePal.select("value", "label").where("type=?", "LIST_TYPE").order("sort asc").find(DataDicModel.class);

        if ("2".equals(MechanismInfoUtils.getFixmedinsType())) {//药店的时候 去除医疗服务项目
            Iterator<DataDicModel> iterator = datas.iterator();
            while (iterator.hasNext()) {
                DataDicModel data = iterator.next();
                if ("医疗服务项目".equals(data.getLabel())) {
                    iterator.remove();
                }
            }
        }

        return datas;
    }

    //获取目录类型数据
    public List<DataDicModel> getListTypeData_user() {
        List<DataDicModel> datas = LitePal.select("value", "label").where("type=?", "LIST_TYPE").order("sort asc").find(DataDicModel.class);

        return datas;
    }

    //获取字典表label
    public String getDataDicLabel(String type, String value) {
        List<DataDicModel> dicModels = LitePal.select("label").where("type=? and value=?", EmptyUtils.strEmpty(type), EmptyUtils.strEmpty(value)).order("sort asc").find(DataDicModel.class);

        if (dicModels != null && !dicModels.isEmpty()) {
            return dicModels.get(0).getLabel();
        } else {
            return value;
        }
    }

    //获取字典表label
    public String getDataDicLabel2(List<DataDicModel> dataDicModels, String value) {
        //获取性别
        String label = "";
        if (dataDicModels != null) {
            for (DataDicModel dataDicModel : dataDicModels) {
                if (dataDicModel.getValue().equals(value)) {
                    label = dataDicModel.getLabel();
                    break;
                }
            }
        }

        return EmptyUtils.isEmpty(label) ? value : label;
    }

    //获取添加医师类型--医院
    public ArrayList<KVModel> getDrType() {
        ArrayList<KVModel> arrayList = new ArrayList<>();

        KVModel kvModel01 = new KVModel("医师", "1");
        KVModel kvModel02 = new KVModel("药师", "2");
        KVModel kvModel03 = new KVModel("护士", "3");

        arrayList.add(kvModel01);
        arrayList.add(kvModel02);
        arrayList.add(kvModel03);

        //药店的时候 多了普通店员身份
        if ("2".equals(MechanismInfoUtils.getFixmedinsType())) {
            KVModel kvModel04 = new KVModel("普通店员", "4");
            arrayList.add(kvModel04);
        }
        return arrayList;
    }

    //获取物流时效（天）
    public ArrayList<KVModel> getLogisTime() {
        ArrayList<KVModel> arrayList = new ArrayList<>();

        KVModel kvModel00 = new KVModel("0（当日达）", "0");
        KVModel kvModel01 = new KVModel("1（次日达）", "1");
        KVModel kvModel02 = new KVModel("2（隔日达）", "2");
        KVModel kvModel03 = new KVModel("3", "3");
        KVModel kvModel04 = new KVModel("4", "4");
        KVModel kvModel05 = new KVModel("5", "5");
        KVModel kvModel06 = new KVModel("6", "6");
        KVModel kvModel07 = new KVModel("7", "7");

        arrayList.add(kvModel00);
        arrayList.add(kvModel01);
        arrayList.add(kvModel02);
        arrayList.add(kvModel03);
        arrayList.add(kvModel04);
        arrayList.add(kvModel05);
        arrayList.add(kvModel06);
        arrayList.add(kvModel07);
        return arrayList;
    }


    //获取亲属关系
    public ArrayList<KVModel> getRelationship() {
        ArrayList<KVModel> arrayList = new ArrayList<>();

        KVModel kvModel01 = new KVModel("父母", "1");
        KVModel kvModel02 = new KVModel("子女", "2");
        KVModel kvModel03 = new KVModel("配偶", "3");

        arrayList.add(kvModel01);
        arrayList.add(kvModel02);
        arrayList.add(kvModel03);

        return arrayList;
    }

    //获取自动开启或者关闭店铺时间
    public ArrayList<KVModel> getAutoOpenOrCloseShopTime() {
        ArrayList<KVModel> arrayList = new ArrayList<>();
        KVModel kvModel;
        for (int i=0;i<25;i++){
            if (i<10){
                kvModel= new KVModel("0"+i+":00", "0"+i+":00");
            }else if (i==24){
                kvModel= new KVModel("23:59", "23:59");
            }else {
                kvModel = new KVModel(i+":00", i+":00");
            }

            arrayList.add(kvModel);
        }
        return arrayList;
    }

    //获取排序数据
    public ArrayList<KVModel> getFixmedinsSort() {
        ArrayList<KVModel> arrayList = new ArrayList<>();

        KVModel kvModel01 = new KVModel("距离排序", "1");
        KVModel kvModel02 = new KVModel("机构编码排序", "2");

        arrayList.add(kvModel01);
        arrayList.add(kvModel02);

        return arrayList;
    }

    //获取机构等级
    public ArrayList<KVModel> getFixmedinsGrade() {
        ArrayList<KVModel> arrayList = new ArrayList<>();

        KVModel kvModel01 = new KVModel("全部", "");
        KVModel kvModel02 = new KVModel("一级", "1");
        KVModel kvModel03 = new KVModel("二级", "2");
        KVModel kvModel04 = new KVModel("三级", "3");
        KVModel kvModel05 = new KVModel("未定级", "4");

        arrayList.add(kvModel01);
        arrayList.add(kvModel02);
        arrayList.add(kvModel03);
        arrayList.add(kvModel04);
        arrayList.add(kvModel05);

        return arrayList;
    }

    //获取区划数据
    public ArrayList<KVModel> getFixmedinsZoning() {
        ArrayList<KVModel> arrayList = new ArrayList<>();

        KVModel kvModel14 = new KVModel("榆林市", "610800");
        KVModel kvModel01 = new KVModel("榆阳区", "610802");
        KVModel kvModel02 = new KVModel("神木市", "610881");
        KVModel kvModel03 = new KVModel("府谷县", "610822");
        KVModel kvModel04 = new KVModel("横山区", "610803");
        KVModel kvModel05 = new KVModel("靖边县", "610824");
        KVModel kvModel06 = new KVModel("定边县", "610825");
        KVModel kvModel07 = new KVModel("绥德县", "610826");
        KVModel kvModel08 = new KVModel("米脂县", "610827");
        KVModel kvModel09 = new KVModel("佳县", "610828");
        KVModel kvModel10 = new KVModel("吴堡县", "610829");
        KVModel kvModel11 = new KVModel("清涧县", "610830");
        KVModel kvModel12 = new KVModel("子洲县", "610831");

        arrayList.add(kvModel14);
        arrayList.add(kvModel01);arrayList.add(kvModel02);arrayList.add(kvModel03);
        arrayList.add(kvModel04);arrayList.add(kvModel05);arrayList.add(kvModel06);
        arrayList.add(kvModel07);arrayList.add(kvModel08);arrayList.add(kvModel09);
        arrayList.add(kvModel10);arrayList.add(kvModel11);arrayList.add(kvModel12);
        return arrayList;
    }

    //获取排序数据
    public ArrayList<KVModel> getFixmedinsSort02() {
        ArrayList<KVModel> arrayList = new ArrayList<>();
        KVModel kvModel01 = new KVModel("距离排序", "");
        KVModel kvModel02 = new KVModel("由远到近", "1");
        KVModel kvModel03 = new KVModel("由近到远", "2");

        arrayList.add(kvModel01);
        arrayList.add(kvModel02);
        arrayList.add(kvModel03);
        return arrayList;
    }

    //获取机构定点批次
    public ArrayList<KVModel> getFixmedinsBatch() {
        ArrayList<KVModel> arrayList = new ArrayList<>();
        KVModel kvModel01 = new KVModel("定点批次", "");
        KVModel kvModel02 = new KVModel("第一批次", "第一批");
        KVModel kvModel03 = new KVModel("第二批次", "第二批");
        KVModel kvModel04 = new KVModel("第三批次", "第三批");
        KVModel kvModel05 = new KVModel("第四批次", "第四批");

        arrayList.add(kvModel01);
        arrayList.add(kvModel02);
        arrayList.add(kvModel03);
        arrayList.add(kvModel04);
        arrayList.add(kvModel05);
        return arrayList;
    }

    //获取区划数据
    public ArrayList<KVModel> getFixmedinsZoning02() {
        ArrayList<KVModel> arrayList = new ArrayList<>();
        KVModel kvModel1 = new KVModel("统筹区划", "");
        KVModel kvModel2 = new KVModel("市管", "市管");
        KVModel kvModel3 = new KVModel("米脂县", "米脂县");

        arrayList.add(kvModel1);arrayList.add(kvModel2);arrayList.add(kvModel3);
        return arrayList;
    }

    //获取管理员身份首页分类的数据
    public ArrayList<HomeSortModel> getHomeSort_Manage(String fixmedins_type) {
        ArrayList<HomeSortModel> arrayList = new ArrayList<>();
        //“绑定关系”，“科室管理”，“人员管理”，“电子处方”，“门诊日志”，“月结查询”，“月结申报”，“结算对账”，“数据版本”。------  医院
        //“绑定关系”，“人员管理”，“电子处方”，“月结查询”，“月结申报”，“结算对账”，“数据版本”。-----------------  药店
        HomeSortModel homeSort01 = new HomeSortModel("1001", R.drawable.ic_bind_relat, "绑定关系");
        HomeSortModel homeSort03 = new HomeSortModel("1003", R.drawable.ic_physician_manage, "人员管理");
        HomeSortModel homeSort04 = new HomeSortModel("1004", R.drawable.ic_elect_prescr, "电子处方");

        HomeSortModel homeSort06 = new HomeSortModel("1006", R.drawable.ic_month_sett_query, "月结查询");
        HomeSortModel homeSort07 = new HomeSortModel("1007", R.drawable.ic_month_sett_declaration, "月结申报");
        HomeSortModel homeSort08 = new HomeSortModel("1008", R.drawable.ic_sett_reconciliation, "结算对账");
        HomeSortModel homeSort09 = new HomeSortModel("1009", R.drawable.ic_data_version, "数据版本");

        arrayList.add(homeSort01);
        if ("1".equals(fixmedins_type)) {
            HomeSortModel homeSort02 = new HomeSortModel("1002", R.drawable.ic_depart_manage, "科室管理");
            HomeSortModel homeSort05 = new HomeSortModel("1005", R.drawable.ic_outpatient_log, "门诊日志");
            arrayList.add(homeSort02);
            arrayList.add(homeSort03);
            arrayList.add(homeSort04);
            arrayList.add(homeSort05);
        } else {
            arrayList.add(homeSort03);
            arrayList.add(homeSort04);
        }

        arrayList.add(homeSort06);
        arrayList.add(homeSort07);
        arrayList.add(homeSort08);
        arrayList.add(homeSort09);
        return arrayList;
    }

    //获取医师身份首页分类的数据
    public ArrayList<HomeSortModel> getHomeSort_Physician(String fixmedins_type) {
        ArrayList<HomeSortModel> arrayList = new ArrayList<>();
        //“电子处方”，“门诊日志”，“月结查询”，“月结申报”，“结算对账”-----  医院
        //“电子处方”，“月结查询”，“月结申报”，“结算对账”----------------  药店

        HomeSortModel homeSort01 = new HomeSortModel("1004", R.drawable.ic_elect_prescr, "电子处方");
        HomeSortModel homeSort03 = new HomeSortModel("1006", R.drawable.ic_month_sett_query, "月结查询");
        HomeSortModel homeSort04 = new HomeSortModel("1007", R.drawable.ic_month_sett_declaration, "月结申报");
        HomeSortModel homeSort05 = new HomeSortModel("1008", R.drawable.ic_sett_reconciliation, "结算对账");

        arrayList.add(homeSort01);
        if ("1".equals(fixmedins_type)) {
            HomeSortModel homeSort02 = new HomeSortModel("1005", R.drawable.ic_outpatient_log, "门诊日志");
            arrayList.add(homeSort02);
        }
        HomeSortModel homeSort10 = new HomeSortModel("1010", R.drawable.ic_insurance_payment, "城乡居民\n医保缴费");

        arrayList.add(homeSort03);
        arrayList.add(homeSort04);
        arrayList.add(homeSort05);
        arrayList.add(homeSort10);
        return arrayList;
    }

    //获取首页常用功能的数据
    public ArrayList<QueryComFunctModel> getHomeCommFunct() {
        ArrayList<QueryComFunctModel> arrayList = new ArrayList<>();

        QueryComFunctModel comFunct01 = new QueryComFunctModel("1101", R.drawable.ic_medical_insurance_catalog_query, "医保目录查询");
        QueryComFunctModel comFunct02 = new QueryComFunctModel("1102", R.drawable.ic_disease_category_directory_query, "病种目录查询");
        QueryComFunctModel comFunct03 = new QueryComFunctModel("1103", R.drawable.ic_employee_benefits_query, "人员待遇查询");

        arrayList.add(comFunct01);
        arrayList.add(comFunct02);
        arrayList.add(comFunct03);
        return arrayList;
    }

    //获取首页其他功能的数据
    public ArrayList<QueryComFunctModel> getHomeOtherCommFunct() {
        ArrayList<QueryComFunctModel> arrayList = new ArrayList<>();

        QueryComFunctModel comFunct01 = new QueryComFunctModel("1201", R.drawable.ic_medinsu_catainfo_query, "医保目录信息查询");
        QueryComFunctModel comFunct02 = new QueryComFunctModel("1202", R.drawable.ic_match_cata, "医疗目录与医保目录匹配信息查询");
        QueryComFunctModel comFunct03 = new QueryComFunctModel("1203", R.drawable.ic_compared_cata, "医药机构目录匹配信息查询");
        QueryComFunctModel comFunct04 = new QueryComFunctModel("1204", R.drawable.ic_pricelimit_info_query, "医保目录限价信息查询");
        QueryComFunctModel comFunct05 = new QueryComFunctModel("1205", R.drawable.ic_firstpay_prop_info_query, "医保目录先自付比例信息查询");

        arrayList.add(comFunct01);
        arrayList.add(comFunct02);
        arrayList.add(comFunct03);
        arrayList.add(comFunct04);
        arrayList.add(comFunct05);
        return arrayList;
    }

    //获取定点药店购药医疗类别全部数据
    public ArrayList<DataDicModel> getPharmacyMedTypeData() {
        ArrayList<DataDicModel> dataDicModels = new ArrayList<>();
        DataDicModel dataDicModel01 = new DataDicModel("定点药店购药", 0, "", "41", "1");
        DataDicModel dataDicModel02 = new DataDicModel("药店购慢特病药", 0, "", "9929", "1");
//        DataDicModel dataDicModel03 = new DataDicModel("新冠门诊", 0, "", "1102", "1");

        dataDicModels.add(dataDicModel01);
        dataDicModels.add(dataDicModel02);
//        dataDicModels.add(dataDicModel03);
        return dataDicModels;
    }

    //获取门诊医疗类别全部数据
    public ArrayList<DataDicModel> getOutpatMedTypeData() {
        ArrayList<DataDicModel> dataDicModels = new ArrayList<>();
        DataDicModel dataDicModel01 = new DataDicModel("门诊统筹", 0, "", "110104", "1");
        DataDicModel dataDicModel02 = new DataDicModel("普通门诊", 0, "", "11", "1");
        DataDicModel dataDicModel03 = new DataDicModel("门诊挂号", 0, "", "12", "1");
        DataDicModel dataDicModel04 = new DataDicModel("门诊两病", 0, "", "9901", "1");
        DataDicModel dataDicModel05 = new DataDicModel("门诊慢特病", 0, "", "14", "1");
//        DataDicModel dataDicModel06 = new DataDicModel("新冠门诊", 0, "", "1102", "1");

        dataDicModels.add(dataDicModel01);
        dataDicModels.add(dataDicModel02);
        dataDicModels.add(dataDicModel03);
        dataDicModels.add(dataDicModel04);
        dataDicModels.add(dataDicModel05);
//        dataDicModels.add(dataDicModel06);
        return dataDicModels;
    }

    //获取医疗类别全部数据
    public ArrayList<DataDicModel> getMedTypeData() {
        ArrayList<DataDicModel> dataDicModels = new ArrayList<>();
        DataDicModel dataDicModel01 = new DataDicModel("定点药店购药", 0, "", "41", "1");
        DataDicModel dataDicModel02 = new DataDicModel("药店购慢特病药", 0, "", "9929", "1");
        DataDicModel dataDicModel03 = new DataDicModel("门诊统筹", 0, "", "110104", "1");
        DataDicModel dataDicModel04 = new DataDicModel("普通门诊", 0, "", "11", "1");
        DataDicModel dataDicModel05 = new DataDicModel("门诊挂号", 0, "", "12", "1");
        DataDicModel dataDicModel06 = new DataDicModel("门诊两病", 0, "", "9901", "1");
        DataDicModel dataDicModel07 = new DataDicModel("门诊慢特病", 0, "", "14", "1");
//        DataDicModel dataDicModel08 = new DataDicModel("新冠门诊", 0, "", "1102", "1");

        dataDicModels.add(dataDicModel01);
        dataDicModels.add(dataDicModel02);
        dataDicModels.add(dataDicModel03);
        dataDicModels.add(dataDicModel04);
        dataDicModels.add(dataDicModel05);
        dataDicModels.add(dataDicModel06);
        dataDicModels.add(dataDicModel07);
//        dataDicModels.add(dataDicModel08);
        return dataDicModels;
    }

    //获取管理员身份首页分类的数据(大字版)
    public ArrayList<LargeVersionModel> getLargever_Manage(String fixmedins_type) {
        ArrayList<LargeVersionModel> arrayList = new ArrayList<>();
        //“绑定关系”，“科室管理”，“医师管理”，“挂号记录”，“月结查询”，“月结申报”，“结算对账”，“数据版本”。------  医院
        //“绑定关系”，“医师管理”，“挂号记录”，“月结查询”，“月结申报”，“结算对账”，“数据版本”。-----------------  药店
        LargeVersionModel largeVersion01 = new LargeVersionModel("1001", R.drawable.ic_bind_relat_large);//绑定关系
        LargeVersionModel largeVersion02 = new LargeVersionModel("1002", R.drawable.ic_data_version_large);//数据版本
        LargeVersionModel largeVersion04 = new LargeVersionModel("1004", R.drawable.ic_physician_manage_large);//医师管理
        LargeVersionModel largeVersion05 = new LargeVersionModel("1005", R.drawable.ic_dir_manage_large);//目录管理
        LargeVersionModel largeVersion06 = new LargeVersionModel("1006", R.drawable.ic_reg_record_large);//挂号记录
        LargeVersionModel largeVersion07 = new LargeVersionModel("1007", R.drawable.ic_settle_reco_large);//结算对账
        LargeVersionModel largeVersion08 = new LargeVersionModel("1008", R.drawable.ic_month_settle_query_large);//月结查询

        arrayList.add(largeVersion01);
        arrayList.add(largeVersion02);
        if ("1".equals(fixmedins_type)) {
            LargeVersionModel largeVersion03 = new LargeVersionModel("1003", R.drawable.ic_depart_manage_large);//科室管理
            arrayList.add(largeVersion03);
            arrayList.add(largeVersion04);
            arrayList.add(largeVersion05);
            arrayList.add(largeVersion06);
        } else {
            arrayList.add(largeVersion04);
            arrayList.add(largeVersion05);
        }

        arrayList.add(largeVersion08);
        arrayList.add(largeVersion07);
        return arrayList;
    }

    //获取医师身份首页分类的数据(大字版)
    public ArrayList<LargeVersionModel> getLargever_Physician(String fixmedins_type) {
        ArrayList<LargeVersionModel> arrayList = new ArrayList<>();
        //“挂号记录”，“月结申报”，“结算对账”-----  医院
        //“月结申报”，“结算对账”----------------  药店
        LargeVersionModel largeVersion02 = new LargeVersionModel("1005", R.drawable.ic_dir_manage_large);//目录管理
        LargeVersionModel largeVersion03 = new LargeVersionModel("1011", R.drawable.ic_settle_record_large);//结算记录
        LargeVersionModel largeVersion05 = new LargeVersionModel("1007", R.drawable.ic_settle_reco_large);//结算对账
        LargeVersionModel largeVersion06 = new LargeVersionModel("1008", R.drawable.ic_month_settle_query_large);//月结查询

        LargeVersionModel largeVersion01;
        if ("1".equals(fixmedins_type)) {
            largeVersion01 = new LargeVersionModel("1009", R.drawable.ic_outpat_settle_large);//门诊结算

            arrayList.add(largeVersion01);
            arrayList.add(largeVersion02);
            arrayList.add(largeVersion03);

            LargeVersionModel largeVersion04 = new LargeVersionModel("1006", R.drawable.ic_reg_record_large);//挂号记录

            arrayList.add(largeVersion04);
        } else {
            largeVersion01 = new LargeVersionModel("1010", R.drawable.ic_pharmacy_settle_large);//药店结算
            arrayList.add(largeVersion01);
            arrayList.add(largeVersion02);
            arrayList.add(largeVersion03);
        }

        arrayList.add(largeVersion06);
        arrayList.add(largeVersion05);
        return arrayList;
    }

    //获取6个月数据
    public ArrayList<MonthSettModel> getSixDateData() {
        ArrayList<MonthSettModel> dates = new ArrayList<>();
        SimpleDateFormat sf01 = new SimpleDateFormat("yyyy年MM月");
        SimpleDateFormat sf02 = new SimpleDateFormat("yyyyMM");
        Calendar ca = Calendar.getInstance();
        ca.setTime(new Date());
        dates.add(new MonthSettModel(sf02.format(ca.getTime()), sf01.format(ca.getTime())));
        for (int i = 0; i < 11; i++) {
            ca.add(Calendar.MONTH, -1);
            dates.add(new MonthSettModel(sf02.format(ca.getTime()), sf01.format(ca.getTime())));
        }

        return dates;
    }

    //获取12个月数据+全部
    public ArrayList<MonthSettModel> getDateData() {
        ArrayList<MonthSettModel> dates = new ArrayList<>();
        SimpleDateFormat sf01 = new SimpleDateFormat("yyyy年MM月");
        SimpleDateFormat sf02 = new SimpleDateFormat("yyyy-MM");
        Calendar ca = Calendar.getInstance();
        ca.setTime(new Date());


        MonthSettModel modelAll = new MonthSettModel("", "全部");
        modelAll.setChoose(true);
        dates.add(modelAll);
        dates.add(new MonthSettModel(sf02.format(ca.getTime()), sf01.format(ca.getTime())));
        for (int i = 0; i < 11; i++) {
            ca.add(Calendar.MONTH, -1);
            dates.add(new MonthSettModel(sf02.format(ca.getTime()), sf01.format(ca.getTime())));
        }

        return dates;
    }

    //获取门诊两病全部数据
    public ArrayList<QueryPersonalInfoModel.PsnOpspRegBean> getTwoDiseData() {
        ArrayList<QueryPersonalInfoModel.PsnOpspRegBean> dataDicModels = new ArrayList<>();
        QueryPersonalInfoModel.PsnOpspRegBean dataDicModel01 = new QueryPersonalInfoModel.PsnOpspRegBean("M03900", "高血压");
        QueryPersonalInfoModel.PsnOpspRegBean dataDicModel02 = new QueryPersonalInfoModel.PsnOpspRegBean("M01604", "糖尿病合并高血压");

        dataDicModels.add(dataDicModel01);
        dataDicModels.add(dataDicModel02);
        return dataDicModels;
    }

    //获取选取照片方式数据
    public ArrayList<String> getSelectPhotoData() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("拍照");
        arrayList.add("从相册选择");
        return arrayList;
    }

    //获取保存到相册
    public ArrayList<String> getSavePhotoData() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("保存到相册");
        return arrayList;
    }

    //获取职业全部数据
    public ArrayList<String> getOccupationData() {
        ArrayList<String> dataDicModels = new ArrayList<>();

        dataDicModels.add("农民");
        dataDicModels.add("学生");
        dataDicModels.add("工人");
        dataDicModels.add("职员");
        dataDicModels.add("国家公务员");
        dataDicModels.add("自由职业者");
        dataDicModels.add("个体经营者");
        dataDicModels.add("无业人员");
        dataDicModels.add("退（离）休人员");
        dataDicModels.add("现役军人");
        dataDicModels.add("专业技术人员");
        dataDicModels.add("企业管理人员");
        dataDicModels.add("其他");
        return dataDicModels;
    }

    //获取职业全部数据
    public ArrayList<CustomTabEntity> getMonthSettTypeData() {
        ArrayList<CustomTabEntity> dataDicModels = new ArrayList<>();
        MonthSettTypeModel monthSettTypeModel01 = new MonthSettTypeModel("", "全部");
        MonthSettTypeModel monthSettTypeModel02 = new MonthSettTypeModel("1", "医保结算");
        MonthSettTypeModel monthSettTypeModel03 = new MonthSettTypeModel("2", "自费结算");

        dataDicModels.add(monthSettTypeModel01);
        dataDicModels.add(monthSettTypeModel02);
        dataDicModels.add(monthSettTypeModel03);
        return dataDicModels;
    }

    //获取职业全部数据
    public ArrayList<CustomTabEntity> getGoodDetailTabData() {
        ArrayList<CustomTabEntity> dataDicModels = new ArrayList<>();
        MonthSettTypeModel monthSettTypeModel01 = new MonthSettTypeModel("1", "商品");
        MonthSettTypeModel monthSettTypeModel02 = new MonthSettTypeModel("2", "详情");

        dataDicModels.add(monthSettTypeModel01);
        dataDicModels.add(monthSettTypeModel02);
        return dataDicModels;
    }

    //获取登录的人员数据
    public ArrayList<CustomTabEntity> getLoginTabData() {
        ArrayList<CustomTabEntity> dataDicModels = new ArrayList<>();
        MonthSettTypeModel monthSettTypeModel01 = new MonthSettTypeModel("1", "管理员");
        MonthSettTypeModel monthSettTypeModel02 = new MonthSettTypeModel("2", "其他人员");

        dataDicModels.add(monthSettTypeModel01);
        dataDicModels.add(monthSettTypeModel02);
        return dataDicModels;
    }

    //用户端-搜索结果-tab数据
    public ArrayList<CustomTabEntity> getSearchTabData(String type) {
        ArrayList<CustomTabEntity> dataDicModels = new ArrayList<>();
        dataDicModels.add(new MonthSettTypeModel("1", "全部"));
        dataDicModels.add(new MonthSettTypeModel("2", "位置"));
        dataDicModels.add(new MonthSettTypeModel("3", "销量"));
        if ("1".equals(type)){
            dataDicModels.add(new MonthSettTypeModel("4", "价格"));
        }


        return dataDicModels;
    }

    //获取每次用量单位数据
    public ArrayList<String> getConsumpData() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("片");
        arrayList.add("丸");
        arrayList.add("颗");
        arrayList.add("粒");
        arrayList.add("包");
        arrayList.add("袋");
        arrayList.add("瓶");
        arrayList.add("ml");
        arrayList.add("贴");
        arrayList.add("支");
        return arrayList;
    }

    //获取药品数量单位数据
    public ArrayList<String> getCataNumData() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("片");
        arrayList.add("丸");
        arrayList.add("颗");
        arrayList.add("粒");
        arrayList.add("包");
        arrayList.add("袋");
        arrayList.add("瓶");
        arrayList.add("盒");
        arrayList.add("贴");
        arrayList.add("支");
        return arrayList;
    }

    //获取药品频率数据
    public ArrayList<String> getFrequencyData() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("每日一次");
        arrayList.add("每日两次");
        arrayList.add("每日三次");
        arrayList.add("每日四次");
        arrayList.add("每日五次");
        arrayList.add("每日六次");
        arrayList.add("每周一次");
        arrayList.add("每周两次");
        arrayList.add("每周三次");
        arrayList.add("每小时一次");
        arrayList.add("每2小时一次");
        arrayList.add("每5小时一次");
        arrayList.add("每8小时一次");
        arrayList.add("每晚一次");
        arrayList.add("隔天一次");
        arrayList.add("五天一次");
        arrayList.add("十天一次");
        arrayList.add("两周一次");
        arrayList.add("三周一次");
        arrayList.add("四周一次");
        arrayList.add("早晚各一次");
        arrayList.add("发热时服用");
        return arrayList;
    }

    //获取药品用法数据(西药中成药处方)
    public ArrayList<String> getUsageData() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("饭后  口服");
        arrayList.add("饭前  口服");
        arrayList.add("饭后  冲服");
        arrayList.add("饭前  冲服");
        arrayList.add("含化");
        arrayList.add("喷涂  外用");
        arrayList.add("涂抹  外用");
        arrayList.add("填塞");
        arrayList.add("注射");
        arrayList.add("雾化");
        arrayList.add("灌注");
        return arrayList;
    }

    //获取重量单位数据
    public ArrayList<String> getWeightData() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("g");
        arrayList.add("条");
        return arrayList;
    }

    //获取药品用法数据（中药处方）
    public ArrayList<String> getUsageData_china() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("每日一剂  水煎服  每日两次");
        arrayList.add("每日一剂  水煎服  每日一次");
        arrayList.add("每日一剂  水煎服  每日三次");
        arrayList.add("每日一剂  水煎服  每日四次");
        arrayList.add("冲服  每天一次");
        arrayList.add("每日一剂  水煎1次分多次服用");
        arrayList.add("每日一剂  水煎2次分多次服用");
        arrayList.add("每日一剂  水煎2次分两次服用");
        arrayList.add("煲粥吃");
        arrayList.add("每日一剂  水煎次分次服服天停天服");
        return arrayList;
    }

    //获取性别
    public ArrayList<String> getGend() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("男");
        arrayList.add("女");
        return arrayList;
    }

    //通过身份证号计算年龄、性别、出生日期
    public Map<String, String> getBirthdayAgeSex(String idCardNo) {
        if (EmptyUtils.isEmpty(idCardNo)){
            ToastUtil.show("身份证号码有误");
            return null;
        }
        String birthday = "";
        int age = 0;
        String sexCode = "";

        char[] number = idCardNo.toCharArray();
        boolean flag = true;
        if (number.length == 15) {
            for (int x = 0; x < number.length; x++) {
                if (!flag) {
                    return null;
                }
                flag = Character.isDigit(number[x]);
            }
        } else if (number.length == 18) {
            for (int x = 0; x < number.length - 1; x++) {
                if (!flag) {
                    return null;
                }
                flag = Character.isDigit(number[x]);
            }
        }

        Calendar cal = Calendar.getInstance();
        int yearNow = cal.get(Calendar.YEAR);
        int monthNow = cal.get(Calendar.MONTH);
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);

        if (flag && idCardNo.length() == 15) {
            birthday = "19" + idCardNo.substring(6, 8) + "-" + idCardNo.substring(8, 10) + "-" + idCardNo.substring(10, 12);
            sexCode = Integer.parseInt(idCardNo.substring(14, 15)) % 2 == 0 ? "2" : "1";

            int yearBirth = Integer.parseInt("19" + idCardNo.substring(6, 8));
            int monthBirth = Integer.parseInt(idCardNo.substring(10, 12));
            int dayOfMonthBirth = Integer.parseInt(idCardNo.substring(12, 14));

            age = yearNow - yearBirth;
            if (monthNow < monthBirth || (monthNow == monthBirth && dayOfMonthNow < dayOfMonthBirth)) {
                age--;
            }
        } else if (flag && idCardNo.length() == 18) {
            birthday = idCardNo.substring(6, 10) + "-" + idCardNo.substring(10, 12) + "-" + idCardNo.substring(12, 14);
            sexCode = Integer.parseInt(idCardNo.substring(16, 17)) % 2 == 0 ? "2" : "1";

            int yearBirth = Integer.parseInt(idCardNo.substring(6, 10));
            int monthBirth = Integer.parseInt(idCardNo.substring(10, 12));
            int dayOfMonthBirth = Integer.parseInt(idCardNo.substring(12, 14));

            age = yearNow - yearBirth;
            if (monthNow < monthBirth || (monthNow == monthBirth && dayOfMonthNow < dayOfMonthBirth)) {
                age--;
            }
        }
        Map<String, String> map = new HashMap<String, String>();
        map.put("birthday", birthday);
        map.put("age", String.valueOf(age));
        map.put("sex", sexCode);
        return map;
    }

    //门诊日志文件夹
    public String getOutpatLogDir() {
        return BaseGlobal.suitBaseDir() + MMKVUtils.getString("OutpatLogDir", "/门诊日志/");
    }

    //电子处方文件夹
    public String getElecPrescrDir() {
        return BaseGlobal.suitBaseDir() + MMKVUtils.getString("ElecPrescrDir", "/电子处方/");
    }

    //获取参保人首页业务办理的数据
    public ArrayList<QueryComFunctModel> getBusinessProcessingData() {
        ArrayList<QueryComFunctModel> arrayList = new ArrayList<>();

        QueryComFunctModel comFunct01 = new QueryComFunctModel("1301", R.drawable.ic_medical_insurance_payment, "居民参保缴费");
        QueryComFunctModel comFunct02 = new QueryComFunctModel("1302", R.drawable.ic_chronic_diseases_handle, "门诊慢病申报");
        QueryComFunctModel comFunct03 = new QueryComFunctModel("1303", R.drawable.ic_off_location_handle, "异地就医备案");
        QueryComFunctModel comFunct04 = new QueryComFunctModel("1304", R.drawable.ic_special_drug_handle, "特殊药品备案");
        QueryComFunctModel comFunct05 = new QueryComFunctModel("1305", R.drawable.ic_insured_outpatient_settlement, "医药机构结算");
        QueryComFunctModel comFunct06 = new QueryComFunctModel("1306", R.drawable.ic_trauma_info_reg, "外伤信息登记");
        QueryComFunctModel comFunct07 = new QueryComFunctModel("1307", R.drawable.ic_family_assistance, "家庭共济绑定");
        QueryComFunctModel comFunct08 = new QueryComFunctModel("1308", R.drawable.ic_insurance_transfer, "医保转移申请");

        arrayList.add(comFunct01);arrayList.add(comFunct02);arrayList.add(comFunct03);arrayList.add(comFunct04);
        arrayList.add(comFunct05);arrayList.add(comFunct06);arrayList.add(comFunct07);arrayList.add(comFunct08);
        return arrayList;
    }
    //获取参保人首页业务查询的数据
    public ArrayList<QueryComFunctModel> getBusinessQueryData() {
        ArrayList<QueryComFunctModel> arrayList = new ArrayList<>();

        QueryComFunctModel comFunct01 = new QueryComFunctModel("1201", R.drawable.ic_personal_account_query, "个人账户查询");
        QueryComFunctModel comFunct02 = new QueryComFunctModel("1202", R.drawable.ic_medical_insurance_catalog_query, "医保目录查询");
        QueryComFunctModel comFunct03 = new QueryComFunctModel("1203", R.drawable.ic_insurance_info_query, "参保信息查询");
        QueryComFunctModel comFunct04 = new QueryComFunctModel("1204", R.drawable.ic_fixmedins_query, "定点医药机构\n查询");
        QueryComFunctModel comFunct05 = new QueryComFunctModel("1205", R.drawable.ic_slowcare_pharmacy, "定点慢保药店\n查询");
        QueryComFunctModel comFunct06 = new QueryComFunctModel("1206", R.drawable.ic_employee_clinic, "职工门诊统筹定点医药机构查询");

        arrayList.add(comFunct01);arrayList.add(comFunct02);arrayList.add(comFunct03);arrayList.add(comFunct04);
        arrayList.add(comFunct05);arrayList.add(comFunct06);
        return arrayList;
    }
    //获取参保人首页公共服务的数据
    public ArrayList<QueryComFunctModel> getPublicServiceData() {
        ArrayList<QueryComFunctModel> arrayList = new ArrayList<>();
        QueryComFunctModel comFunct01 = new QueryComFunctModel("1101", R.drawable.ic_policy_promotion, "政策宣传");
        QueryComFunctModel comFunct02 = new QueryComFunctModel("1102", R.drawable.ic_msg_home, "最新通知");
        QueryComFunctModel comFunct03 = new QueryComFunctModel("1103", R.drawable.ic_insured_freedback, "投诉建议");
        QueryComFunctModel comFunct04 = new QueryComFunctModel("1104", R.drawable.ic_mizhi_news, "大美米脂");

        arrayList.add(comFunct04);arrayList.add(comFunct01);arrayList.add(comFunct02);arrayList.add(comFunct03);
        return arrayList;
    }

    //获取打印联数
    public ArrayList<UnionsModel> getUnionsData(String selectData) {
        ArrayList<UnionsModel> dates = new ArrayList<>();
        UnionsModel unionsModel01 = new UnionsModel("1", "1".equals(selectData) ? true : false);
        UnionsModel unionsModel02 = new UnionsModel("2", "2".equals(selectData) ? true : false);
        UnionsModel unionsModel03 = new UnionsModel("3", "3".equals(selectData) ? true : false);

        dates.add(unionsModel01);
        dates.add(unionsModel02);
        dates.add(unionsModel03);
        return dates;
    }

    //获取智慧医保首页 其他分类数据
    public ArrayList<OtherSortModel> getOtherSortData() {
        ArrayList<OtherSortModel> arrayList = new ArrayList<>();

        OtherSortModel comFunct01 = new OtherSortModel("3001", R.drawable.ic_product_pool, "产品池", 0);
        OtherSortModel comFunct02 = new OtherSortModel("3002", R.drawable.ic_drafts, "草稿箱", 0);
        OtherSortModel comFunct03 = new OtherSortModel("3003", R.drawable.ic_drug_storehouse, "药品库", 0);
        OtherSortModel comFunct04 = new OtherSortModel("3004", R.drawable.ic_shipped_order, "待发货订单", 0);

        arrayList.add(comFunct01);
        arrayList.add(comFunct02);
        arrayList.add(comFunct03);
        arrayList.add(comFunct04);

        if ("1".equals(LoginUtils.getType())) {//管理员身份
            arrayList.add(new OtherSortModel("3005", R.drawable.ic_shop_decoration, "店铺装修", 0));
        } else if ("2".equals(LoginUtils.getType())) {//医师身份
            arrayList.add(new OtherSortModel("3006", R.drawable.ic_prescr_icon, "开处方", 0));
        }

        return arrayList;
    }

    //获取选取照片方式数据
    public ArrayList<StoreStatusModel> getStoreStatusData() {
        ArrayList<StoreStatusModel> arrayList = new ArrayList<>();

        StoreStatusModel storeStatusModel01 = new StoreStatusModel("1", "上线");
        StoreStatusModel storeStatusModel02 = new StoreStatusModel("2", "下线");
        arrayList.add(storeStatusModel01);
        arrayList.add(storeStatusModel02);
        return arrayList;
    }

    //获取轮播间隔时间数据
    public ArrayList<StoreStatusModel> getBannerTimeData() {
        ArrayList<StoreStatusModel> arrayList = new ArrayList<>();

        StoreStatusModel storeStatusModel01 = new StoreStatusModel("60", "60S");
        StoreStatusModel storeStatusModel02 = new StoreStatusModel("30", "30S");
        StoreStatusModel storeStatusModel03 = new StoreStatusModel("15", "15S");
        StoreStatusModel storeStatusModel04 = new StoreStatusModel("10", "10S");
        StoreStatusModel storeStatusModel05 = new StoreStatusModel("5", "5S");
        StoreStatusModel storeStatusModel06 = new StoreStatusModel("3", "3S");

        arrayList.add(storeStatusModel01);
        arrayList.add(storeStatusModel02);
        arrayList.add(storeStatusModel03);
        arrayList.add(storeStatusModel04);
        arrayList.add(storeStatusModel05);
        arrayList.add(storeStatusModel06);

        return arrayList;
    }

    //获取药品类型数据
    public ArrayList<StoreStatusModel> getDrugTypeData() {
        ArrayList<StoreStatusModel> arrayList = new ArrayList<>();
        StoreStatusModel storeStatusModel01 = new StoreStatusModel("1", "非处方药(OTC)");
        StoreStatusModel storeStatusModel02 = new StoreStatusModel("2", "处方药");

        arrayList.add(storeStatusModel01);
        arrayList.add(storeStatusModel02);

        return arrayList;
    }

    //获取是否促销数据
    public ArrayList<StoreStatusModel> getPromotionData() {
        ArrayList<StoreStatusModel> arrayList = new ArrayList<>();
        StoreStatusModel storeStatusModel01 = new StoreStatusModel("1", "是");
        StoreStatusModel storeStatusModel02 = new StoreStatusModel("2", "否");

        arrayList.add(storeStatusModel01);
        arrayList.add(storeStatusModel02);

        return arrayList;
    }

    //获取编辑-商品-位置数据
    public ArrayList<StoreStatusModel> getGoosPostionData() {
        ArrayList<StoreStatusModel> arrayList = new ArrayList<>();

        StoreStatusModel storeStatusModel01 = new StoreStatusModel("1", "首页");
        StoreStatusModel storeStatusModel02 = new StoreStatusModel("2", "分类");

        arrayList.add(storeStatusModel01);
        arrayList.add(storeStatusModel02);

        return arrayList;
    }

    //获取编辑-商品-位置数据
    public ArrayList<StoreStatusModel> getUserSexData() {
        ArrayList<StoreStatusModel> arrayList = new ArrayList<>();

        StoreStatusModel storeStatusModel01 = new StoreStatusModel("1", "男");
        StoreStatusModel storeStatusModel02 = new StoreStatusModel("2", "女");

        arrayList.add(storeStatusModel01);
        arrayList.add(storeStatusModel02);

        return arrayList;
    }

    //获取智慧药房-我的-订单状态分类数据
    public ArrayList<PhaSortMineModel> getOrderSortData() {
        ArrayList<PhaSortMineModel> arrayList = new ArrayList<>();
        PhaSortMineModel comFunct01 = new PhaSortMineModel("4001", R.drawable.ic_order_all, "全部");
        PhaSortMineModel comFunct02 = new PhaSortMineModel("4002", R.drawable.ic_order_stay_pay, "待付款");
        PhaSortMineModel comFunct03 = new PhaSortMineModel("4003", R.drawable.ic_order_sendout, "待发货");
        PhaSortMineModel comFunct04 = new PhaSortMineModel("4004", R.drawable.ic_order_receipt, "待收货");
        PhaSortMineModel comFunct05 = new PhaSortMineModel("4005", R.drawable.ic_order_complete, "已完成");
        PhaSortMineModel comFunct06 = new PhaSortMineModel("4006", R.drawable.ic_order_cancle, "已取消");
        arrayList.add(comFunct01);
        arrayList.add(comFunct02);
        arrayList.add(comFunct03);
        arrayList.add(comFunct04);
        arrayList.add(comFunct05);
        arrayList.add(comFunct06);

        return arrayList;
    }

    //获取获取智慧药房-我的-其他服务分类数据
    public ArrayList<PhaSortMineModel> getOtherServiceData() {
        ArrayList<PhaSortMineModel> arrayList = new ArrayList<>();

        if ("1".equals(LoginUtils.getType())) {//管理员身份
            PhaSortMineModel comFunct01 = new PhaSortMineModel("4101", R.drawable.ic_zhyf_my_account, "我的账户");
            PhaSortMineModel comFunct02 = new PhaSortMineModel("4102", R.drawable.ic_zhyf_my_flair, "我的资质");
//            PhaSortMineModel comFunct03 = new PhaSortMineModel("4103", R.drawable.ic_zhyf_my_address, "我的地址");
            PhaSortMineModel comFunct04 = new PhaSortMineModel("4104", R.drawable.ic_zhyf_my_shipping, "我的配送");

            arrayList.add(comFunct01);
            arrayList.add(comFunct02);
//            arrayList.add(comFunct03);
            arrayList.add(comFunct04);
        } else if ("2".equals(LoginUtils.getType())) {//医师身份
            PhaSortMineModel comFunct01 = new PhaSortMineModel("4101", R.drawable.ic_zhyf_my_account, "我的账户");
//            PhaSortMineModel comFunct03 = new PhaSortMineModel("4103", R.drawable.ic_zhyf_my_address, "我的地址");
//            PhaSortMineModel comFunct05 = new PhaSortMineModel("4105", R.drawable.ic_issued_prescr, "我开具的处方");

            arrayList.add(comFunct01);
//            arrayList.add(comFunct03);
//            arrayList.add(comFunct05);
        }
//        else {
//            PhaSortMineModel comFunct03 = new PhaSortMineModel("4103", R.drawable.ic_zhyf_my_address, "我的地址");
//
//            arrayList.add(comFunct03);
//        }
        return arrayList;
    }

    //获取获取智慧药房-我的-其他服务分类数据--用户端
    public ArrayList<PhaSortMineModel> getOtherServiceData_user() {
        ArrayList<PhaSortMineModel> arrayList = new ArrayList<>();
        PhaSortMineModel comFunct01 = new PhaSortMineModel("5101", R.drawable.ic_zhyf_my_address, "我的地址");
        PhaSortMineModel comFunct02 = new PhaSortMineModel("5102", R.drawable.ic_my_collection, "我的收藏");
        PhaSortMineModel comFunct03 = new PhaSortMineModel("5103", R.drawable.ic_my_tracks, "我的足迹");
        PhaSortMineModel comFunct04 = new PhaSortMineModel("5104", R.drawable.ic_feedback_icon, "意见反馈");
        PhaSortMineModel comFunct05 = new PhaSortMineModel("5105", R.drawable.ic_about_us_icon, "关于我们");
        PhaSortMineModel comFunct06 = new PhaSortMineModel("5106", R.drawable.ic_feedback_icon, "我的交易");

        arrayList.add(comFunct01);arrayList.add(comFunct02);arrayList.add(comFunct03);
        arrayList.add(comFunct04);arrayList.add(comFunct06);arrayList.add(comFunct05);

        return arrayList;
    }

    //获取获取智慧药房-我的账户-充值-选择支付方式数据
    public ArrayList<PayTypeModel> getSelectPayTypeData() {
        ArrayList<PayTypeModel> payTypeModels = new ArrayList<>();
        PayTypeModel payTypeModel01 = new PayTypeModel("2", "支付宝", R.drawable.ic_zfb_icon);
        PayTypeModel payTypeModel02 = new PayTypeModel("1", "微信", R.drawable.ic_wx_icon);
        PayTypeModel payTypeModel03 = new PayTypeModel("3", "云闪付", R.drawable.ic_ysf_icon);
        payTypeModels.add(payTypeModel01);
        payTypeModels.add(payTypeModel02);
        payTypeModels.add(payTypeModel03);

        return payTypeModels;
    }

    //获取获取智慧药房-我的账户-提现-选择收款方式数据
    public ArrayList<PayTypeModel> getSelectCollectionTypeData() {
        ArrayList<PayTypeModel> payTypeModels = new ArrayList<>();
        PayTypeModel payTypeModel01 = new PayTypeModel("2", "支付宝", R.drawable.ic_zfb_icon);
        PayTypeModel payTypeModel02 = new PayTypeModel("1", "微信", R.drawable.ic_wx_icon);
        payTypeModels.add(payTypeModel01);
        payTypeModels.add(payTypeModel02);

        return payTypeModels;
    }

    //获取获取智慧药房-订单-选择支付方式数据
    public ArrayList<PayTypeModel> getSelectOrderPayTypeData() {
        ArrayList<PayTypeModel> payTypeModels = new ArrayList<>();
        PayTypeModel payTypeModel01 = new PayTypeModel("A2", "支付宝", R.drawable.ic_zfb_icon);
        PayTypeModel payTypeModel02 = new PayTypeModel("W2", "微信", R.drawable.ic_wx_icon);
        PayTypeModel payTypeModel03 = new PayTypeModel("Y", "云闪付", R.drawable.ic_ysf_icon);
        payTypeModels.add(payTypeModel01);
        payTypeModels.add(payTypeModel02);
        payTypeModels.add(payTypeModel03);

        return payTypeModels;
    }

    //获取店铺详情更多数据
    public ArrayList<ShopDetailMoreModel> getShopDetailMoreData() {
        ArrayList<ShopDetailMoreModel> arrayList = new ArrayList<>();
        arrayList.add(new ShopDetailMoreModel(R.drawable.shouye,"首页"));
        arrayList.add(new ShopDetailMoreModel(R.drawable.tousu,"投诉商家"));
        return arrayList;
    }

    //获取商品详情更多数据
    public ArrayList<ShopDetailMoreModel> getGoodDetailMoreData() {
        ArrayList<ShopDetailMoreModel> arrayList = new ArrayList<>();
        arrayList.add(new ShopDetailMoreModel(R.drawable.shouye,"首页"));
        return arrayList;
    }

    //获取药品目录更多数据
    public ArrayList<ShopDetailMoreModel> getSelectCataManageMoreData() {
        ArrayList<ShopDetailMoreModel> arrayList = new ArrayList<>();
        arrayList.add(new ShopDetailMoreModel(-1,"药品目录同步"));
        arrayList.add(new ShopDetailMoreModel(-1,"新增"));
        return arrayList;
    }

    //获取过往病史的数据
    public ArrayList<String> getPastMedicalDatas() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("高血压");
        arrayList.add("糖尿病");
        arrayList.add("冠心病");
        arrayList.add("乙肝");
        arrayList.add("支气管炎");
        arrayList.add("慢性阻塞性肺病");
        arrayList.add("荨麻疹");
        arrayList.add("肾功能衰");
        return arrayList;
    }

    //获取过敏史的数据
    public ArrayList<String> getAllergyDatas() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("青霉素");
        arrayList.add("阿司匹林");
        arrayList.add("螨尘");
        arrayList.add("花粉");
        arrayList.add("海鲜");
        arrayList.add("蛋白质（牛奶鸡蛋）");
        return arrayList;
    }

    //获取家族病史的数据
    public ArrayList<String> getFamilyDatas() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("高血压");
        arrayList.add("冠心病");
        arrayList.add("乙肝");
        arrayList.add("精神病");
        arrayList.add("哮喘");
        arrayList.add("白血病");
        return arrayList;
    }

    //获取登录的人员数据
    public ArrayList<CustomTabEntity> getInsuredLoginTabData() {
        ArrayList<CustomTabEntity> dataDicModels = new ArrayList<>();
        MonthSettTypeModel monthSettTypeModel01 = new MonthSettTypeModel("1", "密码登录");
        MonthSettTypeModel monthSettTypeModel02 = new MonthSettTypeModel("2", "验证码登录");

        dataDicModels.add(monthSettTypeModel01);
        dataDicModels.add(monthSettTypeModel02);
        return dataDicModels;
    }
}
