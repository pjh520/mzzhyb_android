package com.jrdz.zhyb_android.utils;

import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author 拜雨
 * @date 2020-02
 * @desc
 */
public class DES3 {

    private final static String Algorithm = "DESede";
    private final static String Transformation = "DESede/ECB/ZeroBytePadding";

    public static byte[] encrypt(byte[] data, byte[] key) {
        SecretKey secretKey = new SecretKeySpec(key, Algorithm);
        try {
            Cipher cipher = Cipher.getInstance(Transformation);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return cipher.doFinal(data);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static byte[] decrypt(byte[] data, byte[] key) {
        SecretKey secretKey = new SecretKeySpec(key, Algorithm);
        try {
            Cipher cipher = Cipher.getInstance(Transformation);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return cipher.doFinal(data);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] str2Bytes(String value) {
        return value.getBytes(Charset.forName("GBK"));
    }

    /**
     * Bytes 2 Hex
     *
     * @param data
     * @return
     */
    public static String bytes2HexString(byte data[]) {
        StringBuilder buffer = new StringBuilder();
        byte[] byteTemp;
        int j = (byteTemp = data).length;
        for (int i = 0; i < j; i++) {
            byte b = byteTemp[i];
            String hex = Integer.toHexString(b & 255);
            if (hex.length() == 1)
                buffer.append('0');
            buffer.append(hex);
        }

        return buffer.toString().toUpperCase();
    }
}
