package com.jrdz.zhyb_android.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Environment;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.FileUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.storage.FileStorageUtils;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.mmkv.MMKV;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/10
 * 描    述：唯一设备标识
 * ================================================
 */
public class DeviceID {
    public static final String GUID_KEY = "guid";
    private static final String uuidFileName = ".zhybguid";

    private static final String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "zhyb_android" + File.separator;

    //获取Android唯一表示
    public static String getAndroidUniId() {
//        String androidId=getAndroidID();
//        if (EmptyUtils.isEmpty(androidId)){
//            try {
//                androidId=createGUID();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
        String androidId = "";
        try {
            androidId = createGUID();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return androidId;
    }

    /**
     * 获取AndroidID
     *
     * @return AndroidID，可能为空
     */
    @SuppressLint("HardwareIds")
    private static String getAndroidID() {
        String id = Settings.Secure.getString(RxTool.getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        if (id == null || "9774d56d682e549c".equals(id)) {
            id = "";
        }
        return id;
    }

    //获取guid
    private static String createGUID() throws Exception {
        // 读取顺序，mmkv内部存储 file
        String guid = getFromMMKV();
        if (guid != null) {
            Log.e("getPermission", "getFromMMKV===guid===" + guid);
            return guid;
        }
        guid = getFromFile();
        if (guid != null) {
            Log.e("getPermission", "getFromFile==guid===" + guid);
            // 如果能够从文件中获取，表示应用卸载了，需要重新给mmkv赋值
            setToMMKV(guid);
            return guid;
        }
        // 前面三个都没有数据，表示第一次安装，需要生成一个UUID
        guid = UUID.randomUUID().toString().replace("-", "");
        // 将UUID保存到MMKV、外部存储目录
        setToMMKV(guid);
        setToFile(guid);
        return guid;
    }

    /**
     * 从MMKV中读取UUID
     */
    public static String getFromMMKV() throws Exception {
        String guid = MMKVUtils.getString(GUID_KEY);
        if (!TextUtils.isEmpty(guid)) {
            //MMKV中有值，更新一下系统数据库和外置存储文件
            updateFile(guid);
            return guid;
        }
        return null;
    }


    /**
     * 从文件读取UUID
     */
    private static String getFromFile() throws Exception {
        File file = new File(filePath,uuidFileName);
        if (!file.exists()) {
            return null;
        }
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        // 读取一行即可
        return bufferedReader.readLine();
    }


    /**
     * 检测外部存储目录是否有该UUID，没有则更新，没有表示文件被用户清除了
     */
    private static void updateFile(String guid) throws Exception {
        File file = new File(filePath,uuidFileName);
        // 如果文件不存在，则表示被清理了，
        if (!file.exists()) {
            File fileParent = new File(filePath);
            if (!fileParent.exists()){
                fileParent.mkdir();
            }

            file.createNewFile();
            writeData(guid);
        } else {
            // 如果文件存在，则需要对比一下sp中和文件中的是否一致
            String fileUUID = getFromFile();
            // 如果不相等，则表示被篡改了，需要更新
            if (!guid.equals(fileUUID)) {
                setToFile(guid);
            }
        }
    }

    /**
     * 保存到MMKV
     */
    private static void setToMMKV(String guid) {
        MMKVUtils.putString(GUID_KEY, guid);
    }

    /**
     * 保存到文件
     */
    private static void setToFile(String guid) throws Exception {
        File file = new File(filePath,uuidFileName);
        if (file.exists()) {
            file.delete();
        }

        File fileParent = new File(filePath);
        if (!fileParent.exists()){
            fileParent.mkdir();
        }

        file.createNewFile();
        writeData(guid);
    }

    /**
     * 将UUID写入文件
     */
    private static void writeData(String guid) throws Exception {
        FileOutputStream fos = new FileOutputStream(filePath+uuidFileName);
        fos.write(guid.getBytes());
        fos.close();
    }

    //给外部获取唯一值
    public static String getDeviceIDByMMKV(){
        return MMKVUtils.getString(GUID_KEY);
    }
}
