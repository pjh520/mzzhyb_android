package com.jrdz.zhyb_android.utils;

import android.content.Context;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.toast.ToastUtil;
import com.jrdz.zhyb_android.base.MyWebViewActivity;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.net.H5JsonCallBack;
import com.jrdz.zhyb_android.ui.insured.activity.PersonalAccountQueryListActivity;
import com.jrdz.zhyb_android.ui.insured.model.WechatForYuLinLoginDataModel;
import com.jrdz.zhyb_android.ui.insured.model.WechatForYuLinLoginModel;
import com.library.constantStorage.ConstantStorage;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024-07-03
 * 描    述：
 * ================================================
 */
public class H5RequestUtils {

    public void goPager(Context context,String TAG,String title,String pageUrl,IRequestListener iRequestListener){
//        String json = "{\"mobile\":\"18966873288\",\"certNo\":\"610102196812180625\",\"name\":\"王军利\"}";
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("mobile",InsuredLoginUtils.getPhone());
        jsonObject.put("certNo",InsuredLoginUtils.getIdCardNo());
        jsonObject.put("name",InsuredLoginUtils.getName());

        String encrypt = AESUtils.encrypt(ConstantStorage.getH5AESKey(context), jsonObject.toJSONString());
        iRequestListener.onShowWaitDialog();
        WechatForYuLinLoginModel.sendWechatForYuLinLoginRequest(TAG, encrypt, new H5JsonCallBack<WechatForYuLinLoginModel>() {
            @Override
            public void onRequestError(WechatForYuLinLoginModel returnData, String msg) {
                if (iRequestListener!=null){
                    iRequestListener.onHideWaitDialog();
                    iRequestListener.onFail(msg+":"+(returnData!=null?returnData.getData():"接口返回异常"));
                }
            }

            @Override
            public void onRequestSuccess(WechatForYuLinLoginModel returnData) {
                if (iRequestListener!=null){
                    iRequestListener.onHideWaitDialog();
                }

                String data = returnData.getData();
                if (!EmptyUtils.isEmpty(data)) {
                    String decrypt = AESUtils.decrypt(ConstantStorage.getH5AESKey(context), data);
                    WechatForYuLinLoginDataModel wechatForYuLinLoginDataModel = JSON.parseObject(decrypt, WechatForYuLinLoginDataModel.class);
                    if (wechatForYuLinLoginDataModel != null) {
                        String url = pageUrl + "?accessToken=" + wechatForYuLinLoginDataModel.getAccessToken();
                        MyWebViewActivity.newIntance(context, title, url, true, false);
                    }
                }
            }
        });
    }

    public interface IRequestListener{
        void onShowWaitDialog();
        void onHideWaitDialog();
        void onFail(String msg);
    }
}
