package com.jrdz.zhyb_android.utils;

import android.content.Context;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.alibaba.fastjson.JSON;
import com.frame.compiler.widget.MNPasswordEditText;
import com.frame.compiler.widget.customPop.BottomPop;
import com.hjq.shape.view.ShapeEditText;
import com.jrdz.zhyb_android.R;
import com.frame.compiler.widget.safeKeyBoard.MyKeyboardView;
import com.jrdz.zhyb_android.constant.Constants;
import com.library.constantStorage.ConstantStorage;

import java.lang.reflect.Method;

import razerdp.basepopup.BasePopupWindow;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.widget.pop
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/11
 * 描    述： 结算输入密码
 * ================================================
 */
public class InputPwdUtils {
    private InputPwdListener mInputPwdListener;
    private BottomPop bottomDialog;
    private View pwdView;

    public void showPwdDialog(Context context,InputPwdListener inputPwdListener){
        this.mInputPwdListener=inputPwdListener;
        if (bottomDialog==null||pwdView==null){
            pwdView = View.inflate(context, R.layout.layout_input_pwd, null);
            bottomDialog = new BottomPop(context, pwdView);
        }

        FrameLayout flClose=pwdView.findViewById(R.id.fl_close);
        MNPasswordEditText etPwd=pwdView.findViewById(R.id.pwd_inputview);
        MyKeyboardView keyboardView=pwdView.findViewById(R.id.keyboard_view);
        //初始化键盘
        if (keyboardView.getEditText() != etPwd){
            keyboardView.init(etPwd, bottomDialog, MyKeyboardView.KEYBOARDTYPE_Only_Num_Pwd);
        }else {
            keyboardView.randomKey(keyboardView.getKeyboardOnlyNumPwd());
        }

        flClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });

        etPwd.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String text, boolean isComplete) {
                if (isComplete){
                    mInputPwdListener.onComplete(Base64.encodeToString(DES3.encrypt(DES3.str2Bytes(text),DES3.str2Bytes(ConstantStorage.getPay3DESKey(context))),Base64.NO_WRAP));
                    bottomDialog.dismiss();
                }
            }
        });
        bottomDialog.setOnDismissListener(new BasePopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                etPwd.setText("");
            }
        });

        bottomDialog.showPopupWindow();
    }

    //关闭dialog
    public void dismissDialog() {
        if (bottomDialog != null) {
            bottomDialog.dismiss();
            bottomDialog = null;
        }

        pwdView=null;
    }

    public void disableShowInput(EditText editText) {
        if (android.os.Build.VERSION.SDK_INT <= 10) {
            editText.setInputType(InputType.TYPE_NULL);
        } else {
            Class<EditText> cls = EditText.class;
            Method method;
            try {
                method = cls.getMethod("setShowSoftInputOnFocus", boolean.class);
                method.setAccessible(true);
                method.invoke(editText, false);
            } catch (Exception e) {//TODO: handle exception
            }
            try {
                method = cls.getMethod("setSoftInputShownOnFocus", boolean.class);
                method.setAccessible(true);
                method.invoke(editText, false);
            } catch (Exception e) {//TODO: handle exception
            }
        }
    }

    public interface InputPwdListener {
        void onComplete(String pwd);
    }
}
