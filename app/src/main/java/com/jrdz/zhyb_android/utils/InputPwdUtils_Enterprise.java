package com.jrdz.zhyb_android.utils;

import android.content.Context;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.frame.compiler.utils.MD5Util;
import com.frame.compiler.widget.MNPasswordEditText;
import com.frame.compiler.widget.customPop.BottomPop;
import com.frame.compiler.widget.safeKeyBoard.MyKeyboardView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.constant.Constants;

import java.lang.reflect.Method;

import razerdp.basepopup.BasePopupWindow;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.widget.pop
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/2/11
 * 描    述： 结算输入密码--企业基金支付
 * ================================================
 */
public class InputPwdUtils_Enterprise {
    private InputPwdListener_Enterprise mInputPwdListener;
    private BottomPop bottomDialog;
    private View pwdView;

    public void showPwdDialog(Context context,String enterpriseBalance,String hasPayMoney,InputPwdListener_Enterprise inputPwdListener){
        this.mInputPwdListener=inputPwdListener;
        if (bottomDialog==null||pwdView==null){
            pwdView = View.inflate(context, R.layout.layout_input_pwd_enterprise, null);
            bottomDialog = new BottomPop(context, pwdView);
        }

        FrameLayout flClose=pwdView.findViewById(R.id.fl_close);
        TextView tvEnterpriseBalance=pwdView.findViewById(R.id.tv_enterprise_balance);
        TextView tvHasPayMoney=pwdView.findViewById(R.id.tv_has_pay_money);
        MNPasswordEditText etPwd=pwdView.findViewById(R.id.pwd_inputview);
        MyKeyboardView keyboardView=pwdView.findViewById(R.id.keyboard_view);

        tvEnterpriseBalance.setText("¥"+enterpriseBalance+"元");
        tvHasPayMoney.setText("¥"+hasPayMoney+"元");
        //初始化键盘
        if (keyboardView.getEditText() != etPwd){
            keyboardView.init(etPwd, bottomDialog, MyKeyboardView.KEYBOARDTYPE_Only_Num_Pwd);
        }else {
            keyboardView.randomKey(keyboardView.getKeyboardOnlyNumPwd());
        }

        flClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });

        etPwd.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String text, boolean isComplete) {
                if (isComplete){
                    mInputPwdListener.onComplete_Enterprise(MD5Util.up32(text+ Constants.Configure.ENTERPRISE_PWD_TAG));
                    bottomDialog.dismiss();
                }
            }
        });
        bottomDialog.setOnDismissListener(new BasePopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                etPwd.setText("");
            }
        });

        bottomDialog.showPopupWindow();
    }

    //关闭dialog
    public void dismissDialog() {
        if (bottomDialog != null) {
            bottomDialog.dismiss();
            bottomDialog = null;
        }

        pwdView=null;
    }

    public void disableShowInput(EditText editText) {
        if (android.os.Build.VERSION.SDK_INT <= 10) {
            editText.setInputType(InputType.TYPE_NULL);
        } else {
            Class<EditText> cls = EditText.class;
            Method method;
            try {
                method = cls.getMethod("setShowSoftInputOnFocus", boolean.class);
                method.setAccessible(true);
                method.invoke(editText, false);
            } catch (Exception e) {//TODO: handle exception
            }
            try {
                method = cls.getMethod("setSoftInputShownOnFocus", boolean.class);
                method.setAccessible(true);
                method.invoke(editText, false);
            } catch (Exception e) {//TODO: handle exception
            }
        }
    }

    public interface InputPwdListener_Enterprise {
        void onComplete_Enterprise(String pwd);
    }
}
