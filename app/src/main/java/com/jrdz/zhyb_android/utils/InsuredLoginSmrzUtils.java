package com.jrdz.zhyb_android.utils;

import android.content.Context;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.CustomerDialogUtils;
import com.frame.compiler.widget.toast.ToastUtil;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.insured.activity.FingerprintloginActivity;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredLoginActivity;
import com.jrdz.zhyb_android.ui.insured.activity.InsuredPersonalActivity;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-08-26
 * 描    述：
 * ================================================
 */
public class InsuredLoginSmrzUtils {
    private CustomerDialogUtils customerDialogUtils;

    //判断是否登录 以及实名认证
    public boolean isLoginSmrz(Context context){
        if (!InsuredLoginUtils.isLogin()){
            ToastUtil.show("请先登录");
            if (EmptyUtils.isEmpty(MMKVUtils.getString("finger_login_key", ""))) {//关闭指纹登录
                InsuredLoginActivity.newIntance(context,0);
            }else {
                FingerprintloginActivity.newIntance(context,0);
            }
            return false;
        }

        if ("0".equals(InsuredLoginUtils.getIsFace())){//未实名认证
            if (customerDialogUtils==null){
                customerDialogUtils = new CustomerDialogUtils();
            }
            customerDialogUtils.showDialog(context, "提示", "您还未实名认证，是否去实名认证？", 2,"关闭", "去实名", R.color.txt_color_666, R.color.color_f16b6f,
                    new CustomerDialogUtils.IDialogListener() {

                        @Override
                        public void onBtn01Click() {}

                        @Override
                        public void onBtn02Click() {
                            InsuredPersonalActivity.newIntance(context);
                        }
                    });
            return false;
        }

        return true;
    }

    //清楚引用 以及弹框
    public void cleanData(){
        if (customerDialogUtils!=null){
            customerDialogUtils.onclean();
            customerDialogUtils=null;
        }
    }
}
