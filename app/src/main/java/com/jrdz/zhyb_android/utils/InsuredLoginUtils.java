package com.jrdz.zhyb_android.utils;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.toast.ToastUtil;
import com.jrdz.zhyb_android.database.InsuredUserInfoModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;

import org.litepal.LitePal;
//参保人登录信息封装
public class InsuredLoginUtils {

    //判断用户是否登陆
    public static boolean isLogin() {
        InsuredUserInfoModel insuredUserInfoModel = LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel == null) {
            return false;
        } else {
            return true;
        }
    }

    //获取用户信息
    public static InsuredUserInfoModel getInsuredUserinfo() {
        return LitePal.findFirst(InsuredUserInfoModel.class);
    }

    //清楚账号信息 needCallBack 是否需要发送退出信息
    public static void clearAccount(boolean needCallBack) {
        //1.清空数据库存储的用户信息
        LitePal.deleteAll(InsuredUserInfoModel.class);
        if (needCallBack){
            MsgBus.sendInsuredLoginStatus().post(0);
        }
    }

    //获取用户手机号
    public static String getPhone(){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(insuredUserInfoModel.getPhone());
    }

    //设置用户手机号
    public static void setPhone(String phone){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel!=null){
            insuredUserInfoModel.setPhone(phone);
            insuredUserInfoModel.update(insuredUserInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取用户密码
    public static String getPwd(){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(insuredUserInfoModel.getPwd());
    }

    //获取用户名
    public static String getName(){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(insuredUserInfoModel.getName());
    }

    //设置用户名
    public static void setName(String name){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel!=null){
            insuredUserInfoModel.setName(name);
            insuredUserInfoModel.update(insuredUserInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取用户是否实名
    public static String getIsFace(){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(insuredUserInfoModel.getIsFace());
    }

    //设置用户是否实名
    public static void setIsFace(String isFace){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel!=null){
            insuredUserInfoModel.setIsFace(isFace);
            insuredUserInfoModel.update(insuredUserInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取用户身份证号码
    public static String getIdCardNo(){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(insuredUserInfoModel.getId_card_no());
    }
    //设置用户身份证号码
    public static void setIdCardNo(String idCardNo){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel!=null){
            insuredUserInfoModel.setId_card_no(idCardNo);
            insuredUserInfoModel.update(insuredUserInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取用户图片id
    public static String getAccessoryId(){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(insuredUserInfoModel.getAccessoryId());
    }

    //设置用户图片accessoryId
    public static void setAccessoryId(String accessoryId){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel!=null){
            insuredUserInfoModel.setAccessoryId(accessoryId);
            insuredUserInfoModel.update(insuredUserInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取用户图片url
    public static String getAccessoryUrl(){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(insuredUserInfoModel.getAccessoryUrl());
    }

    //设置用户手机号
    public static void setAccessoryUrl(String accessoryUrl){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel!=null){
            insuredUserInfoModel.setAccessoryUrl(accessoryUrl);
            insuredUserInfoModel.update(insuredUserInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取用户的昵称
    public static String getNickname(){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(insuredUserInfoModel.getNickname());
    }

    //设置用户的昵称
    public static void setNickname(String nickname){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel!=null){
            insuredUserInfoModel.setNickname(nickname);
            insuredUserInfoModel.update(insuredUserInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }


    //获取用户的疾病史
    public static String getAssociatedDiseases(){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(insuredUserInfoModel.getAssociatedDiseases());
    }

    //设置用户的疾病史
    public static void setAssociatedDiseases(String associatedDiseases){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel!=null){
            insuredUserInfoModel.setAssociatedDiseases(associatedDiseases);
            insuredUserInfoModel.update(insuredUserInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取用户的性别
    public static String getSex(){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(insuredUserInfoModel.getSex());
    }

    //设置用户的性别
    public static void setSex(String sex){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel!=null){
            insuredUserInfoModel.setSex(sex);
            insuredUserInfoModel.update(insuredUserInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }


    //获取是否设置支付密码
    public static String getIsSetPaymentPwd(){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(insuredUserInfoModel.getIsSetPaymentPwd());
    }

    //设置是否设置支付密码
    public static void setIsSetPaymentPwd(String isSetPaymentPwd){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel!=null){
            insuredUserInfoModel.setIsSetPaymentPwd(isSetPaymentPwd);
            insuredUserInfoModel.update(insuredUserInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //是否支持企业基金支付
    public static String getIsEnterpriseFundPay(){
        InsuredUserInfoModel insuredUserInfoModel= LitePal.findFirst(InsuredUserInfoModel.class);
        if (insuredUserInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(insuredUserInfoModel.getIsEnterpriseFundPay());
    }
}
