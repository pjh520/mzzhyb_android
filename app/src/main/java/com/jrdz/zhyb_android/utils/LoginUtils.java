package com.jrdz.zhyb_android.utils;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.toast.ToastUtil;
import com.jrdz.zhyb_android.database.UserInfoModel;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;

import org.litepal.LitePal;

public class LoginUtils {

    //判断用户是否登陆
    public static boolean isLogin() {
        UserInfoModel userInfoModel = LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel == null) {
            return false;
        } else {
            return true;
        }
    }

    //获取用户信息
    public static UserInfoModel getUserinfo() {
        return LitePal.findFirst(UserInfoModel.class);
    }

    //清楚账号信息 needCallBack 是否需要发送退出信息
    public static void clearAccount(boolean needCallBack) {
        //1.清空数据库存储的用户信息
        LitePal.deleteAll(UserInfoModel.class);
        if (needCallBack){
            MsgBus.sendLoginStatus().post(0);
        }
    }

    //获取登录用户id
    public static String getUserId(){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(userInfoModel.getUserId());
    }

    //获取登录用户名
    public static String getUserName(){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(userInfoModel.getUserName());
    }

    //设置用户名
    public static void setUserName(String userName){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel!=null){
            userInfoModel.setUserName(userName);
            userInfoModel.update(userInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取登录用户名
    public static String getPwd(){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(userInfoModel.getPwd());
    }

    //获取登录用户医师类型
    public static String getDrTypeName(){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(userInfoModel.getDrTypeName());
    }
    //获取登录用户医师科室id
    public static String getDeptId(){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(userInfoModel.getDeptId());
    }

    //获取登录用户医师科室名称
    public static String getDeptIdName(){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(userInfoModel.getDeptIdName());
    }

    //获取登录用户医师科室科别
    public static String getCaty(){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(userInfoModel.getCaty());
    }

    //获取登录用户医师电话
    public static String getDr_phone(){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(userInfoModel.getDr_phone());
    }

    //设置登录用户医师电话
    public static void setDr_phone(String dr_phone){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel!=null){
            userInfoModel.setDr_phone(dr_phone);
            userInfoModel.update(userInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //判断是否是管理员
    public static boolean isManage(){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel==null){
            return false;
        }
        return "1".equals(userInfoModel.getType());
    }

    //获取登录人员的身份
    public static String getType(){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel==null){
            return "";
        }
        return userInfoModel.getType();
    }

    //获取医师资格证图片id
    public static String getAccessoryId(){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(userInfoModel.getAccessoryId());
    }

    //获取医师资格证图片
    public static String getAccessoryUrl(){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(userInfoModel.getAccessoryUrl());
    }

    //设置医师资格证图片id
    public static void setAccessoryId(String accessoryId){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel!=null){
            userInfoModel.setAccessoryId(accessoryId);
            userInfoModel.update(userInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //设置医师资格证图片
    public static void setAccessoryUrl(String accessoryUrl){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel!=null){
            userInfoModel.setAccessoryUrl(accessoryUrl);
            userInfoModel.update(userInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取用户身份证号
    public static String getIDNumber(){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel==null){
            return "";
        }

        return userInfoModel.getIDNumber();
    }

    //设置用户身份证号
    public static void setIDNumber(String iDNumber){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel!=null){
            userInfoModel.setApproveStatus(iDNumber);
            userInfoModel.update(userInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取管理员入驻审核状态
    public static String getApproveStatus(){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel==null){
            return "";
        }

        return userInfoModel.getApproveStatus();
    }

    //设置管理员入驻审核状态
    public static void setApproveStatus(String approveStatus){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel!=null){
            userInfoModel.setApproveStatus(approveStatus);
            userInfoModel.update(userInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取智慧医保登录人员的头像
    public static String getThrAccessoryUrl(){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel==null){
            return "";
        }

        return userInfoModel.getThrAccessoryUrl();
    }

    //设置智慧医保登录人员的头像
    public static void setThrAccessoryUrl(String thrAccessoryUrl){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel!=null){
            userInfoModel.setThrAccessoryUrl(thrAccessoryUrl);
            userInfoModel.update(userInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //设置智慧医保登录人员的头像id
    public static void setThrAccessoryId(String thrAccessoryId){
        UserInfoModel userInfoModel= LitePal.findFirst(UserInfoModel.class);
        if (userInfoModel!=null){
            userInfoModel.setThrAccessoryId(thrAccessoryId);
            userInfoModel.update(userInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

}
