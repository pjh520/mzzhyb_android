package com.jrdz.zhyb_android.utils;

import androidx.annotation.Nullable;

import com.tencent.mmkv.MMKV;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/16
 * 描    述： MMKV工具类
 * ================================================
 */
public class MMKVUtils {
    private static final String mmapID="zhyb";

    //------------------------存储字符串-----------------------------------------
    public static void putString(String key, @Nullable String value){
        MMKV mmkv = MMKV.mmkvWithID(mmapID);
        mmkv.putString(key,value);
    }

    public static String getString(String key){
        return getString(key,"");
    }

    public static String getString(String key,String defValue){
        MMKV mmkv = MMKV.mmkvWithID(mmapID);
        return mmkv.getString(key,defValue);
    }
    //------------------------存储布尔型-----------------------------------------
    public static void putBoolean(String key, boolean value){
        MMKV mmkv = MMKV.mmkvWithID(mmapID);
        mmkv.putBoolean(key,value);
    }

    public static boolean getBoolean(String key){
        MMKV mmkv = MMKV.mmkvWithID(mmapID);
        return mmkv.getBoolean(key,false);
    }

    public static boolean getBoolean_true(String key){
        MMKV mmkv = MMKV.mmkvWithID(mmapID);
        return mmkv.getBoolean(key,true);
    }

    //删除指定key的mmkv存储数据
    public static void delete(String key){
        MMKV mmkv = MMKV.mmkvWithID(mmapID);
        mmkv.removeValueForKey(key);
    }
}
