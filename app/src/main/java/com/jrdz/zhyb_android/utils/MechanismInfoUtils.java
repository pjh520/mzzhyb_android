package com.jrdz.zhyb_android.utils;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.toast.ToastUtil;
import com.jrdz.zhyb_android.database.MechanismInfoModel;

import org.litepal.LitePal;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/1/16
 * 描    述：智慧医保--机构信息工具类
 * ================================================
 */
public class MechanismInfoUtils {
    //获取定点医药机构编号
    public static String getFixmedinsCode(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(mechanismInfoModel.getFixmedins_code());
    }

    //定点医药机构名称
    public static String getFixmedinsName(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(mechanismInfoModel.getFixmedins_name());
    }
    //定点医药机构类型
    public static String getFixmedinsType(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(mechanismInfoModel.getFixmedins_type());
    }
    //就医地医保区划
    public static String getAdmdvs(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(mechanismInfoModel.getAdmdvs());
    }
    //统一社会信用代码
    public static String getUscc(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(mechanismInfoModel.getUscc());
    }
    //医院等级
    public static String getHospLv(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }
        return EmptyUtils.strEmpty(mechanismInfoModel.getHosp_lv());
    }
    //判断是否激活设备
    public static boolean isActivate(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return false;
        }

        return !EmptyUtils.isEmpty(mechanismInfoModel.getFixmedins_code());
    }

    //获取联数
    public static String getUnions(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }

        return mechanismInfoModel.getUnions();
    }
    //设置联数
    public static void setUnions(String Unions){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel!=null){
            mechanismInfoModel.setUnions(Unions);
            mechanismInfoModel.update(mechanismInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取手机号
    public static String getPhone(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }

        return mechanismInfoModel.getPhone();
    }
    //设置手机号
    public static void setPhone(String phone){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel!=null){
            mechanismInfoModel.setPhone(phone);
            mechanismInfoModel.update(mechanismInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取是否开启电子处方
    public static String getElectronicPrescription(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }

        return mechanismInfoModel.getElectronicPrescription();
    }
    //设置是否开启电子处方
    public static void setElectronicPrescription(String electronicPrescription){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel!=null){
            mechanismInfoModel.setElectronicPrescription(electronicPrescription);
            mechanismInfoModel.update(mechanismInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取是否开启门诊日志
    public static String getOutpatientMdtrtinfo(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }

        return mechanismInfoModel.getOutpatientMdtrtinfo();
    }
    //设置是否开启门诊日志
    public static void setOutpatientMdtrtinfo(String outpatientMdtrtinfo){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel!=null){
            mechanismInfoModel.setOutpatientMdtrtinfo(outpatientMdtrtinfo);
            mechanismInfoModel.update(mechanismInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取营业执照id
    public static String getAccessoryId(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }

        return mechanismInfoModel.getAccessoryId();
    }
    //设置营业执照id
    public static void setAccessoryId(String accessoryId){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel!=null){
            mechanismInfoModel.setAccessoryId(accessoryId);
            mechanismInfoModel.update(mechanismInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取营业执照url
    public static String getAccessoryUrl(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }

        return mechanismInfoModel.getAccessoryUrl();
    }
    //设置营业执照url
    public static void setAccessoryUrl(String accessoryUrl){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel!=null){
            mechanismInfoModel.setAccessoryUrl(accessoryUrl);
            mechanismInfoModel.update(mechanismInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取管理员入驻审核状态
    public static String getApproveStatus(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }

        return mechanismInfoModel.getApproveStatus();
    }

    //设置管理员入驻审核状态
    public static void setApproveStatus(String approveStatus){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel!=null){
            mechanismInfoModel.setApproveStatus(approveStatus);
            mechanismInfoModel.update(mechanismInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }

    //获取订单语音提醒状态
    public static String getVoiceNotification(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }

        return mechanismInfoModel.getVoiceNotification();
    }

    //设置订单语音提醒状态
    public static void setVoiceNotification(String voiceNotification){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel!=null){
            mechanismInfoModel.setVoiceNotification(voiceNotification);
            mechanismInfoModel.update(mechanismInfoModel.getId());
        }else {
            ToastUtil.show("数据有误，请重新登录app");
        }
    }


    //获取机构是否审核通过（申请认证）
    public static boolean isApplySuccess(){
        if ("3".equals(MechanismInfoUtils.getApproveStatus())||"5".equals(MechanismInfoUtils.getApproveStatus())){//机构审核通过
            return true;
        }else {
            return false;
        }
    }

    //获取管理员入驻模式
    public static String getEntryMode(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }

        return mechanismInfoModel.getEntryMode();
    }

    //获取是否允许企业基金支付
    public static String getIsEnterpriseFundPay(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }

        return mechanismInfoModel.getIsEnterpriseFundPay();
    }

    //是否是O2Ｏ商家
    public static String getIsOTO(){
        MechanismInfoModel mechanismInfoModel= LitePal.findFirst(MechanismInfoModel.class);
        if (mechanismInfoModel==null){
            return "";
        }

        return mechanismInfoModel.getIsOTO();
    }

    //清楚定点机构信息
    public static void clearAccount() {
        //1.清空数据库存储的用户信息
        LitePal.deleteAll(MechanismInfoModel.class);
    }
}
