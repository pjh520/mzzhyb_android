package com.jrdz.zhyb_android.utils;

import android.util.Log;

import com.frame.compiler.utils.EmptyUtils;

import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-08-18
 * 描    述：
 * ================================================
 */
public class PwdUtils {

    /**
     * 规则2：至少包含大小写字母及数字中的两种
     * 是否包含
     *
     * @param str
     * @return
     */
    public static boolean isLetterDigit(String str) {
        boolean isDigit = false;//定义一个boolean值，用来表示是否包含数字
        boolean isLetter = false;//定义一个boolean值，用来表示是否包含字母
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {   //用char包装类中的判断数字的方法判断每一个字符
                isDigit = true;
            } else if (Character.isLetter(str.charAt(i))) {  //用char包装类中的判断字母的方法判断每一个字符
                isLetter = true;
            }
        }
        String regex = "^[a-zA-Z0-9]+$";
        boolean isRight = isDigit && isLetter && str.matches(regex);
        return isRight;
    }

    /**
     * 规则3：必须同时包含大小写字母及数字
     * 是否包含
     *
     * @param str
     * @return
     */
    public static boolean isContainAll(String str) {
        if (EmptyUtils.isEmpty(str))return false;
        boolean isDigit = false;//定义一个boolean值，用来表示是否包含数字
        boolean isLowerCase = false;//定义一个boolean值，用来表示是否包含字母
        boolean isUpperCase = false;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {//用char包装类中的判断数字的方法判断每一个字符
                isDigit = true;
            } else if (Character.isLowerCase(str.charAt(i))) {//用char包装类中的判断字母的方法判断每一个字符
                isLowerCase = true;
            } else if (Character.isUpperCase(str.charAt(i))) {
                isUpperCase = true;
            }
        }
        String regex = "^[a-zA-Z0-9]+$";
        boolean isRight = isDigit && isLowerCase && isUpperCase && str.matches(regex);
        return isRight;
    }

    //验证密码是否满足正则 true:代表满足数字,大写字母,小写字母,特殊符,至少其中三种组成密码 false反之
    public static boolean pwdFormat(String pwd) {
        Pattern p = Pattern.compile("^(?![A-Za-z]+$)(?![A-Z\\d]+$)(?![A-Z\\W]+$)(?![a-z\\d]+$)(?![a-z\\W]+$)(?![\\d\\W]+$)\\S{8,100}$");
        Matcher m = p.matcher(pwd);
        if (m.find()) {
            return true;
        }
        return false;
    }

    //4~12个字符,一个汉字为2个字符,以英文字母或汉字开头,由汉字,英文字母,数字任意两种组合组成
    public static boolean textFormat(String pwd) {
        if (EmptyUtils.isEmpty(pwd)){
            return false;
        }
        int totalLength=0,hz=0,yw=0,sz=0;
        for (int i = 0,size=pwd.length(); i < size; i++) {
            char text = pwd.charAt(i);
            // 汉字
            if (Character.getType(text) == Character.OTHER_LETTER) {
                totalLength+=2;
                hz=1;
                Log.e("666666", "textFormat: "+text+"====totalLength===="+totalLength+"====hz===="+hz);
            }
            // 字母
            else if (Character.isLetter(text)) {
                totalLength+=1;
                yw=1;
                Log.e("666666", "textFormat: "+text+"====totalLength===="+totalLength+"====yw===="+yw);
            }
            // 数字
            else if (Character.isDigit(text)) {
                totalLength+=1;
                sz=1;
                Log.e("666666", "textFormat: "+text+"====totalLength===="+totalLength+"====sz===="+sz);
            }
        }

        if (totalLength<4||totalLength>12){
            return false;
        }

        if ((hz+yw+sz)<2){
            return false;
        }
        return true;
    }
}
