package com.jrdz.zhyb_android.utils;

import static android.content.Context.MEDIA_PROJECTION_SERVICE;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.ScrollView;

import androidx.annotation.ColorInt;
import androidx.annotation.RequiresApi;
import androidx.collection.LruCache;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.ImageUtils;
import com.jrdz.zhyb_android.R;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.components
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/4/17 0017
 * 描    述：scrollview截图
 * ================================================
 */
public class ScreenshotUtil {
    //截图全屏（头部+scrollView）
    public static Bitmap ScreenShot(View view,ScrollView scrollView) {
        Bitmap titleBarBitmap = view2Bitmap2(view);
        Bitmap bitmap = scrollViewScreenShot(scrollView,Color.TRANSPARENT);

        return combineBitmapsIntoOnlyOne(titleBarBitmap,bitmap);
    }

    public static Bitmap ScreenShot(View view, NestedScrollView scrollView) {
        Bitmap titleBarBitmap = view2Bitmap2(view);
        Bitmap bitmap = nestedScrollViewScreenShot(scrollView,Color.TRANSPARENT);

        return combineBitmapsIntoOnlyOne(titleBarBitmap,bitmap);
    }

    //截图scrollView
    public static Bitmap scrollViewScreenShot(ScrollView scrollView,@ColorInt int bgColor) {
        int h = 0;
        Bitmap bitmap = null;
        for (int i = 0; i < scrollView.getChildCount(); i++) {
            h += scrollView.getChildAt(i).getHeight();
        }
        bitmap = Bitmap.createBitmap(scrollView.getWidth(), h, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(bgColor);
        scrollView.draw(canvas);
        return bitmap;
    }

    //截图scrollView
    public static Bitmap nestedScrollViewScreenShot(NestedScrollView nestedScrollView,@ColorInt int bgColor) {
        int h = 0;
        Bitmap bitmap = null;
        for (int i = 0; i < nestedScrollView.getChildCount(); i++) {
            h += nestedScrollView.getChildAt(i).getHeight();
        }
        bitmap = Bitmap.createBitmap(nestedScrollView.getWidth(), h, Bitmap.Config.ARGB_4444);
        final Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(bgColor);
        nestedScrollView.draw(canvas);
        return bitmap;
    }

//    public class MyAdapter extends BaseQuickAdapter<T> {
//
//        public MyAdapter() {
//            super(getItemLayoutResId(), datas);
//        }
//
//        /**
//         * 用于对外暴露convert方法,构造缓存视图(截屏用)
//         * @param viewHolder
//         * @param t
//         */
//        public void startConvert(BaseViewHolder viewHolder, T t){
//            convert(viewHolder,t);
//        }
//
//        @Override
//        protected void convert(BaseViewHolder viewHolder, T t) {
//            bindView(viewHolder, t);
//        }
//    }
//    /**https://juejin.cn/post/6844903533968359438
//     * 截取recycler view
//     */
//    public static Bitmap getRecyclerViewScreenshot(RecyclerView view) {
//        BaseQuickAdapter adapter = (BaseQuickAdapter)view.getAdapter();
//        Bitmap bigBitmap = null;
//        if (adapter != null) {
//            int size = adapter.getData().size();
//            int height = 0;
//            Paint paint = new Paint();
//            int iHeight = 0;
//            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
//
//            // Use 1/8th of the available memory for this memory cache.
//            final int cacheSize = maxMemory / 8;
//            LruCache<String, Bitmap> bitmaCache = new LruCache<>(cacheSize);
//            for (int i = 0; i < size; i++) {
//                BaseViewHolder holder = (BaseViewHolder) adapter.createViewHolder(view, adapter.getItemViewType(i));
//                //此处需要调用convert方法，否则绘制出来的都是空的item
//                adapter.startConvert(holder, adapter.getData().get(i));
//                holder.itemView.measure(
//                        View.MeasureSpec.makeMeasureSpec(view.getWidth(), View.MeasureSpec.EXACTLY),
//                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
//                holder.itemView.layout(0, 0, holder.itemView.getMeasuredWidth(),
//                        holder.itemView.getMeasuredHeight());
//                holder.itemView.setDrawingCacheEnabled(true);
//                holder.itemView.buildDrawingCache();
//                Bitmap drawingCache = holder.itemView.getDrawingCache();
//                if (drawingCache != null) {
//
//                    bitmaCache.put(String.valueOf(i), drawingCache);
//                }
//                height += holder.itemView.getMeasuredHeight();
//            }
//
//            bigBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), height, Bitmap.Config.ARGB_8888);
//            Canvas bigCanvas = new Canvas(bigBitmap);
//            Drawable lBackground = view.getBackground();
//            if (lBackground instanceof ColorDrawable) {
//                ColorDrawable lColorDrawable = (ColorDrawable) lBackground;
//                int lColor = lColorDrawable.getColor();
//                bigCanvas.drawColor(lColor);
//            }
//
//            for (int i = 0; i < size; i++) {
//                Bitmap bitmap = bitmaCache.get(String.valueOf(i));
//                bigCanvas.drawBitmap(bitmap, 0f, iHeight, paint);
//                iHeight += bitmap.getHeight();
//                bitmap.recycle();
//            }
//        }
//        return bigBitmap;
//    }

    /**
     * View to bitmap.
     *
     * @param view The view.
     * @return bitmap
     */
    public static Bitmap view2Bitmap(final View view) {
        if (view == null) return null;
        Bitmap ret = Bitmap.createBitmap(view.getWidth(),
                view.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(ret);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) {
            bgDrawable.draw(canvas);
        } else {
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);
        return ret;
    }

    /**
     * View to bitmap.
     *
     * @param view The view.
     * @return bitmap
     */
    public static Bitmap view2Bitmap2(final View view) {
        if (view == null) return null;
        boolean drawingCacheEnabled = view.isDrawingCacheEnabled();
        boolean willNotCacheDrawing = view.willNotCacheDrawing();
        view.setDrawingCacheEnabled(true);
        view.setWillNotCacheDrawing(false);
        Bitmap drawingCache = view.getDrawingCache();
        Bitmap bitmap;
        if (null == drawingCache || drawingCache.isRecycled()) {
            view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
            view.buildDrawingCache();
            drawingCache = view.getDrawingCache();
            if (null == drawingCache || drawingCache.isRecycled()) {
                bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.RGB_565);
                Canvas canvas = new Canvas(bitmap);
                view.draw(canvas);
            } else {
                bitmap = Bitmap.createBitmap(drawingCache);
            }
        } else {
            bitmap = Bitmap.createBitmap(drawingCache);
        }
        view.setWillNotCacheDrawing(willNotCacheDrawing);
        view.setDrawingCacheEnabled(drawingCacheEnabled);
        return bitmap;
    }

    /**
     * 将四张图拼成一张
     *
     * @param pic1 图一
     * @param pic2 图二
     * @return only_bitmap
     */
    public static Bitmap combineBitmapsIntoOnlyOne(Bitmap pic1, Bitmap pic2) {
        int w_total = pic2.getWidth();
        int h_total = pic1.getHeight() + pic2.getHeight();
        int h_pic1 = pic1.getHeight();

        Bitmap only_bitmap = Bitmap.createBitmap(w_total, h_total, Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(only_bitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(pic1, 0, 0, null);
        canvas.drawBitmap(pic2, 0, h_pic1, null);

        return only_bitmap;
    }

    /**
     * 压缩图片
     * @param image
     * @return
     */
    public static Bitmap compressImage(Bitmap image) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            // 质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
            image.compress(Bitmap.CompressFormat.JPEG, 90, baos);
            // 把压缩后的数据baos存放到ByteArrayInputStream中
            ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
            // 把ByteArrayInputStream数据生成图片
            return BitmapFactory.decodeStream(isBm, null, null);
        }catch (Exception e){
            e.printStackTrace();
            return image;
        }
    }
}
