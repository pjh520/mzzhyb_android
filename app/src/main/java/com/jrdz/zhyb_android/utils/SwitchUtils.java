package com.jrdz.zhyb_android.utils;

import android.annotation.SuppressLint;
import android.app.admin.DevicePolicyManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.PowerManager;
import android.util.Log;

import java.lang.reflect.Method;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022/11/29
 * 描    述：系统功能操作类（WiFi、热点、屏幕等开关操作）
 * ================================================
 */
public class SwitchUtils {
    private static final String TAG = "SwitchUtils类";

    /**
     * 热点开关
     */
    public static boolean apSwitch(Context context, boolean flag){
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }
        try{
            WifiConfiguration apConfig = new WifiConfiguration();
            apConfig.SSID ="ap";   //配置热点的名称
            apConfig.preSharedKey = "12345678";  //配置热点的密码(至少8位)
            apConfig.allowedKeyManagement.set(4); //配置密码加密方式
            //通过反射调用设置热点
            Method method = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, Boolean.TYPE);
            Boolean rs = (Boolean) method.invoke(wifiManager, apConfig, flag);//true开启热点 false关闭热点
            Log.d(TAG, "开启是否成功:" + rs);
            return rs;
        } catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * WiFi开关
     */
    public static void wifiSwitch(Context context, boolean flag){
        //获取wifi管理服务
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(flag);
    }

    /**
     * 蓝牙开关
     */
    public static void blueSwitch(boolean flag){
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (flag) {
            bluetoothAdapter.enable();
        }else{
            bluetoothAdapter.disable();
        }
    }

    /**
     * 熄屏
     */
    public static void ScreenOff(Context context) {
        DevicePolicyManager policyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        policyManager.lockNow();
    }

    /**
     * 亮屏
     */
    public static void ScreenOn(Context context){
        //获取电源管理器对象
        PowerManager pm=(PowerManager) context.getSystemService(Context.POWER_SERVICE);
        //获取PowerManager.WakeLock对象,后面的参数|表示同时传入两个值,最后的是LogCat里用的Tag
        @SuppressLint("InvalidWakeLockTag")
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP |
                PowerManager.SCREEN_DIM_WAKE_LOCK,"bright");
        //点亮屏幕
        wl.acquire();
        //释放
        wl.release();
    }
}
