package com.jrdz.zhyb_android.utils;

import android.content.Context;
import android.graphics.Color;
import android.view.View;

import com.frame.compiler.widget.customPop.BottomDialogUtils;
import com.frame.compiler.widget.toast.ToastUtil;
import com.frame.compiler.widget.wheel.WheelView;
import com.jrdz.zhyb_android.R;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：dgonline-android
 * 包    名：com.aten.compiler.widget.customerDialog
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2018/10/29
 * 描    述：两列联动wheel
 * ================================================
 */
public class TwoLinkWheelUtils<T1,T2> {
    private BottomDialogUtils bottomDialogUtils;
    private String cityInfo = "";
    private WheelView<T1> wv01;
    private WheelView<T2> wv02;

    //显示城市选择框
    public void showCityWheel(Context context, final ITwoLinkWheelUtils cityWheelClickListener) {
        View cityWheelView = View.inflate(context, R.layout.layout_two_link_wheel, null);
        ArrayList<WheelView> wvs = new ArrayList<>();
        wv01 = (WheelView) cityWheelView.findViewById(R.id.wv_01);
        wv02 = (WheelView) cityWheelView.findViewById(R.id.wv_02);

        wvs.add(wv01);
        wvs.add(wv02);

        setWvOption(context, wvs);

        wv01.setCurvedArcDirection(WheelView.CURVED_ARC_DIRECTION_LEFT);
        wv01.setCurvedArcDirectionFactor(0.65f);


        wv01.setOnItemSelectedListener(new WheelView.OnItemSelectedListener<T1>() {
            @Override
            public void onItemSelected(WheelView<T1> wheelView, T1 data, int position) {
                cityWheelClickListener.onCity(data);
            }
        });

        bottomDialogUtils = new BottomDialogUtils(context);
        bottomDialogUtils.showBottomDialogDialog(cityWheelView, "选择投诉类型", new BottomDialogUtils.BottomClickListener() {
            @Override
            public void onSure() {
                cityWheelClickListener.onchooseCity(wv01 == null ? null : wv01.getSelectedItemPosition(), wv01 == null ? null : wv01.getSelectedItemData(),
                        wv02 == null ? null : wv02.getSelectedItemPosition(), wv02 == null ? null : wv02.getSelectedItemData());
            }

            @Override
            public void onCancle() {
            }
        });
    }

    //设置wheelview的属性
    private void setWvOption(Context context, ArrayList<WheelView> wvs) {
        for (WheelView wv : wvs) {
            wv.setVisibleItems(7);
            wv.setAutoFitTextSize(true);
            wv.setSelectedRectColor(Color.parseColor("#1e1e1e"));
            wv.setNormalItemTextColor(Color.parseColor("#808080"));
            wv.setTextSize(18f, true);
            wv.setShowDivider(true);
            wv.setDividerType(WheelView.DIVIDER_TYPE_FILL);
            wv.setDividerColor(context.getResources().getColor(R.color.line02));
            wv.setDividerPaddingForWrap(10, true);
            wv.setDividerHeight(0.5f, true);
            wv.setResetSelectedPosition(true);

            wv.setLineSpacing(15, true);
        }
    }

    //设置省的数据
    public void setWv01Data(List<T1> provinceDatas) {
        if (wv01 == null) {
            ToastUtil.show("数据有误,请重新打开页面！");
        } else {
            wv01.setData(provinceDatas);
        }
    }

    //设置市的数据
    public void setWv02Data(List<T2> cityDatas) {
        if (wv02 == null) {
            ToastUtil.show("数据有误,请重新打开页面！");
        } else {
            wv02.setData(cityDatas);
        }
    }

    public void setWv01SelectItem(int position) {
        if (wv01 != null) {
            wv01.setSelectedItemPosition(position, true, 1000);
        }
    }

    public void setWv02SelectItem(int position) {
        if (wv02 != null) {
            wv02.setSelectedItemPosition(position, true, 1000);
        }
    }

    public interface ITwoLinkWheelUtils<T1,T2> {
        void onCity(T1 provinceData);//获取城市以及区的数据

        void onchooseCity(int provincePos, T1 provinceItemData,
                          int cityPos, T2 cityItemData);
    }

    //清理缓存
    public void onCleanData(){
        if (bottomDialogUtils!=null){
            bottomDialogUtils.dismissBottomDialogDialog();
        }
    }
}