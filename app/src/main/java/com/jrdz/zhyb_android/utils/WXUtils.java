package com.jrdz.zhyb_android.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.IsInstallUtils;
import com.frame.compiler.utils.LogUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.toast.ToastUtil;
import com.jrdz.zhyb_android.constant.Constants;
import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

/**
 * ================================================
 * 项目名称：Pumeng_master
 * 包    名：com.demo.pay_library.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/7/16 0016
 * 描    述：
 * ================================================
 */
public class WXUtils {

    //跳转小程序
    public static void goSmallRoutine(Context context,String parma,String path){
        LogUtils.e("goSmallRoutine", "goSmallRoutine: path==="+path);
        try {
            //这个是打开微信  防止华为手机 微信没有设置浮动权限 打不开小程序
            Intent lan = context.getPackageManager().getLaunchIntentForPackage("com.tencent.mm");
            Intent t2 = new Intent(Intent.ACTION_MAIN);
            t2.addCategory(Intent.CATEGORY_LAUNCHER);
            t2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            t2.setComponent(lan.getComponent());
            context.startActivity(t2);

            IWXAPI api = WXAPIFactory.createWXAPI(context, Constants.Configure.WX_APPID);
            WXLaunchMiniProgram.Req req = new WXLaunchMiniProgram.Req();
            req.userName = parma; // 填小程序原始id
            if (!EmptyUtils.isEmpty(path)){
                req.path = path;                  //拉起小程序页面的可带参路径，不填默认拉起小程序首页，对于小游戏，可以只传入 query 部分，来实现传参效果，如：传入 "?foo=bar"。
            }
//            req.miniprogramType = WXLaunchMiniProgram.Req.MINIPROGRAM_TYPE_PREVIEW;// 可选打开 开发版，体验版和正式版
            req.miniprogramType = WXLaunchMiniProgram.Req.MINIPTOGRAM_TYPE_RELEASE;// 可选打开 开发版，体验版和正式版
            api.sendReq(req);
        } catch (Exception e) {
            ToastUtil.show("打开小程序失败，请先检查微信是否安装");
            e.printStackTrace();
        }
    }

}
