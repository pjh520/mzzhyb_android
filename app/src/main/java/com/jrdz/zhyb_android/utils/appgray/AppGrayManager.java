package com.jrdz.zhyb_android.utils.appgray;

import android.app.Activity;
import android.app.Application;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-14
 * 描    述：activity 全局置灰管理--popwindow 没效果  可以使用 GlobalGray.hook替换
 * ================================================
 */
public class AppGrayManager implements Application.ActivityLifecycleCallbacks{

    private Paint mPaint;

    public AppGrayManager(Paint paint) {
        this.mPaint=paint;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        //关键代码,app灰白化
        activity.getWindow().getDecorView().setLayerType(View.LAYER_TYPE_HARDWARE, mPaint);
    }

    @Override
    public void onActivityStarted(Activity activity) {}

    @Override
    public void onActivityResumed(Activity activity) {}

    @Override
    public void onActivityPaused(Activity activity) {}

    @Override
    public void onActivityStopped(Activity activity) {}

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {}

    @Override
    public void onActivityDestroyed(Activity activity) {}
}
