package com.jrdz.zhyb_android.utils.appgray;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.view.View;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.utils.appgray
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-14
 * 描    述：
 * ================================================
 */
public class GlobalGray {

    public static void hook(){
        try {
            //一、获取 WindowManagerGlobal 对象
            Class windowManagerGlobalClass = Class.forName("android.view.WindowManagerGlobal");
            Method getInstanceStaticMethod = windowManagerGlobalClass.getDeclaredMethod("getInstance");
            Object windowManagerGlobal = getInstanceStaticMethod.invoke(windowManagerGlobalClass);
            //二、获取 WindowManagerGlobal 中的 mViews
            Field mViewsField = windowManagerGlobalClass.getDeclaredField("mViews");
            mViewsField.setAccessible(true);
            ArrayList<View> mViews = (ArrayList<View>)mViewsField.get(windowManagerGlobal);
            //三、创建代理类对象
            //创建饱和度为 0 的画笔
            Paint mPaint =new Paint();
            ColorMatrix mColorMatrix = new ColorMatrix();
            mColorMatrix.setSaturation(0f);
            mPaint.setColorFilter(new ColorMatrixColorFilter(mColorMatrix));

            ObservableArrayList proxyArrayList =new ObservableArrayList(new ObservableArrayList.OnListAddListener() {
                @Override
                public void add(ArrayList list, int index) {
                    View view = (View) list.get(index);
                    view.setLayerType(View.LAYER_TYPE_HARDWARE,mPaint);
                }
            });

            //将原有的数据添加到代理 ArrayList
            proxyArrayList.addAll(mViews);
            //四、使用代理对象替换原始对象
            mViewsField.set(windowManagerGlobal,proxyArrayList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
