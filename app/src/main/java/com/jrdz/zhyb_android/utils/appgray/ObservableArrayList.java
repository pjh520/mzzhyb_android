package com.jrdz.zhyb_android.utils.appgray;

import java.util.ArrayList;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.utils.appgray
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-12-14
 * 描    述：
 * ================================================
 */
public class ObservableArrayList<T> extends ArrayList<T> {
    private OnListAddListener onListAddListener;

    public ObservableArrayList(OnListAddListener onListAddListener) {
        this.onListAddListener=onListAddListener;
    }

    @Override
    public boolean add(T o) {
        boolean isAdd = super.add(o);
        if (onListAddListener!=null){
            onListAddListener.add(this, size() - 1);
        }
        return isAdd;
    }

    public interface OnListAddListener<T> {
        void add(ArrayList<T> list, int index);
    }
}
