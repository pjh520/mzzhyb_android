package com.jrdz.zhyb_android.utils.location;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.provider.Settings;

import androidx.fragment.app.Fragment;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.LogUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.permission.PermissionHelper;
import com.frame.lbs_library.utils.CustomLocationBean;
import com.frame.lbs_library.utils.ILocationListener;
import com.frame.lbs_library.utils.LBSUtil;
import com.hjq.permissions.Permission;
import com.jrdz.zhyb_android.constant.Constants;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-11-28
 * 描    述：原生获取经纬度
 * //                Location location1=LocationTool.getLocation(getContext());
 * //                Log.e("666666", "location1==Latitude==="+location1.getLatitude()+"location==Longitude==="+location1.getLongitude());
 * //                Gps gps=LocationTool.GPS84ToBD09(location1.getLongitude(),location1.getLatitude());
 * //                Log.e("666666", "Gps===location1==Latitude==="+gps.getLongitude()+"location==Longitude==="+gps.getLatitude());
 * //                Log.e("666666", "location1==Latitude==="+LocationTool.getAddress(getContext(),location1.getLatitude(),location1.getLongitude()).toString());
 * ================================================
 */
public class LocationTool {
    private ILocationListener mListener;
    private MyLocationListener myLocationListener;
    private LocationManager mLocationManager;
    private PermissionHelper permissionHelper;

    /**
     * 内部类实现单例模式
     * 延迟加载，减少内存开销
     */
    private static class SingletonHolder {
        private static LocationTool instance = new LocationTool();
    }

    /**
     * 私有的构造函数
     */
    private LocationTool() {
    }

    public static LocationTool getInstance() {
        return SingletonHolder.instance;
    }

    /**
     * 判断Gps是否可用
     *
     * @return {@code true}: 是<br>{@code false}: 否
     */
    public boolean isGpsEnabled(Context context) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /**
     * 判断定位是否可用
     *
     * @return {@code true}: 是<br>{@code false}: 否
     */
    public boolean isLocationEnabled(Context context) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER) || lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /**
     * 打开Gps设置界面
     */
    public void openGpsSettings(Context context) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * 注册
     * <p>使用完记得调用{@link #unRegisterLocation()}</p>
     * <p>需添加权限 {@code <uses-permission android:name="android.permission.INTERNET"/>}</p>
     * <p>需添加权限 {@code <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>}</p>
     * <p>需添加权限 {@code <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>}</p>
     * <p>如果{@code minDistance}为0，则通过{@code minTime}来定时更新；</p>
     * <p>{@code minDistance}不为0，则以{@code minDistance}为准；</p>
     * <p>两者都为0，则随时刷新。</p>
     *
//     * @param minTime     位置信息更新周期（单位：毫秒）
//     * @param minDistance 位置变化最小距离：当位置距离变化超过此值时，将更新位置信息（单位：米）
     * @param listener    位置刷新的回调接口
     * @return {@code true}: 初始化成功<br>{@code false}: 初始化失败
     */
    public void registerLocation(final Fragment fragment,ILocationListener listener){
        registerLocation(fragment.getActivity(),listener);
    }
    public void registerLocation(final Activity activity,ILocationListener listener) {
        if (listener == null) {
            listener.onLocationError("监听器不能为空");
            return;
        }
        if (permissionHelper == null) {
            permissionHelper = new PermissionHelper();
        }
        permissionHelper.requestPermission(activity, new PermissionHelper.onPermissionListener() {
            @Override
            public void onSuccess() {
                //如果是pos机 那么只能使用第三方定位
                openThirdPartyLocation(listener);
//                if (Constants.Configure.IS_POS){
//                    openThirdPartyLocation(listener);
//                }else {
//                    openRegisterLocation(activity, listener);
//                }
            }

            @Override
            public void onNoAllSuccess(String noAllSuccessText) {}

            @Override
            public void onFail(String failText) {
                listener.onLocationError(failText);
            }
        },"定位权限:获取当前的经纬度,以便计算距离以及显示当前的位置信息",Permission.ACCESS_FINE_LOCATION);
    }

    //POS只能使用第三方定位
    private void openThirdPartyLocation(ILocationListener listener) {
        LBSUtil.getInstance().onStartLocation(listener);
    }

    //打开联系监听的定位
    @SuppressLint("MissingPermission")
    private void openRegisterLocation(Activity activity,ILocationListener listener) {
        mLocationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        mListener = listener;
        if (!isLocationEnabled(activity)) {
            listener.onLocationError("无法定位，请打开定位服务");
            return;
        }
//        String bestprovider=mLocationManager.getBestProvider(getCriteria(),true);
//        if (EmptyUtils.isEmpty(bestprovider)){
//            bestprovider = LocationManager.NETWORK_PROVIDER;
//        }
        String bestprovider = LocationManager.NETWORK_PROVIDER;
//        Log.e("666666", "bestprovider==="+bestprovider);
        if (myLocationListener == null) {
            myLocationListener = new MyLocationListener();
        }
        mLocationManager.requestLocationUpdates(bestprovider, 0, 0, myLocationListener);
    }

    /**
     * 设置定位参数
     *
     * @return {@link Criteria}
     */
    private static Criteria getCriteria() {
        Criteria criteria = new Criteria();
        //设置定位精确度 Criteria.ACCURACY_COARSE比较粗略，Criteria.ACCURACY_FINE则比较精细
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        //设置是否要求速度
        criteria.setSpeedRequired(true);
        // 设置是否允许运营商收费
        criteria.setCostAllowed(true);
        //设置是否需要方位信息
        criteria.setBearingRequired(true);
        //设置是否需要海拔信息
        criteria.setAltitudeRequired(true);
        // 设置对电源的需求
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        return criteria;
    }

    /**
     * 根据经纬度获取地理位置
     *
     * @param latitude  纬度
     * @param longitude 经度
     * @return {@link Address}
     */
    public Address getAddress(double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(RxTool.getContext(), Locale.getDefault());

        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                return addresses.get(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //根据地理位置获取经纬度
    public Address getLocalTion(Context context, String addressText) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());

        try {
            List<Address> addresses = geocoder.getFromLocationName(addressText, 1);
            if (addresses.size() > 0) {
                return addresses.get(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据经纬度获取所在国家
     *
     * @param context   上下文
     * @param latitude  纬度
     * @param longitude 经度
     * @return 所在国家
     */
    public String getCountryName(Context context, double latitude, double longitude) {
        Address address = getAddress(latitude, longitude);
        return address == null ? "unknown" : address.getCountryName();
    }

    /**
     * 根据经纬度获取所在地
     *
     * @param context   上下文
     * @param latitude  纬度
     * @param longitude 经度
     * @return 所在地
     */
    public String getLocality(Context context, double latitude, double longitude) {
        Address address = getAddress(latitude, longitude);
        return address == null ? "unknown" : address.getLocality();
    }

    /**
     * 根据经纬度获取所在街道
     *
     * @param context   上下文
     * @param latitude  纬度
     * @param longitude 经度
     * @return 所在街道
     */
    private String getStreet(Context context, double latitude, double longitude) {
        Address address = getAddress(latitude, longitude);
        return address == null ? "unknown" : address.getAddressLine(0);
    }

    private boolean isMove(Location location, Location preLocation) {
        boolean isMove;
        if (preLocation != null) {
            double speed = location.getSpeed() * 3.6;
            double distance = location.distanceTo(preLocation);
            double compass = Math.abs(preLocation.getBearing() - location.getBearing());
            double angle;
            if (compass > 180) {
                angle = 360 - compass;
            } else {
                angle = compass;
            }
            if (speed != 0) {
                if (speed < 35 && (distance > 3 && distance < 10)) {
                    isMove = angle > 10;
                } else {
                    isMove = (speed < 40 && distance > 10 && distance < 100) ||
                            (speed < 50 && distance > 10 && distance < 100) ||
                            (speed < 60 && distance > 10 && distance < 100) ||
                            (speed < 9999 && distance > 100);
                }
            } else {
                isMove = false;
            }
        } else {
            isMove = true;
        }
        return isMove;
    }

    //停止定位
    public void unRegisterLocation() {
        if (mLocationManager != null) {
            if (myLocationListener != null) {
                mLocationManager.removeUpdates(myLocationListener);
                myLocationListener = null;
            }
            mLocationManager = null;
        }

        if (mListener!=null){
            mListener=null;
        }
        LBSUtil.getInstance().onDestoryLocation();
    }

    private class MyLocationListener implements LocationListener {
        /**
         * 当坐标改变时触发此函数，如果Provider传进相同的坐标，它就不会被触发
         *
         * @param location 坐标
         */
        @Override
        public void onLocationChanged(Location location) {
            if (location==null&&mListener!=null){
                mListener.onLocationError("定位失败");
                return;
            }
            if (myLocationListener != null) {
                mLocationManager.removeUpdates(myLocationListener);
            }
            if (mListener != null) {
                //Address[addressLines=[0:"福建省莆田市涵江区塘北街255-55号西坡东侧安置区"],feature=塘北街255-55号西坡东侧安置区,admin=福建省,sub-admin=涵西街道,locality=莆田市,thoroughfare=塘北街,postalCode=null,countryCode=CN,countryName=中国,hasLatitude=true,latitude=25.466108,hasLongitude=true,longitude=119.105247,phone=null,url=null,extras=Bundle[mParcelledData.dataSize=92]]
                CustomLocationBean customLocationBean = CoordinateUtils.GPS84ToBD09(location.getLongitude(), location.getLatitude());
                Address address=getAddress(location.getLatitude(), location.getLongitude());
                if (address!=null){
                    customLocationBean.setProvince(address.getAdminArea());
                    customLocationBean.setCity(address.getLocality());
                    customLocationBean.setDistrict(address.getSubLocality());
                    customLocationBean.setStreet(address.getThoroughfare());
                    customLocationBean.setDetail_address(address.getFeatureName());
                }

                mListener.onLocationSuccess(customLocationBean);
            }
        }

        /**
         * provider的在可用、暂时不可用和无服务三个状态直接切换时触发此函数
         *
         * @param provider 提供者
         * @param status   状态
         * @param extras   provider可选包
         */
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    LogUtils.e("当前GPS状态为可见状态");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    LogUtils.e("onStatusChanged", "当前GPS状态为服务区外状态");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    LogUtils.e("onStatusChanged", "当前GPS状态为暂停服务状态");
                    break;
                default:
                    break;
            }
        }

        /**
         * provider被enable时触发此函数，比如GPS被打开
         */
        @Override
        public void onProviderEnabled(String provider) {}

        /**
         * provider被disable时触发此函数，比如GPS被关闭
         */
        @Override
        public void onProviderDisabled(String provider) {}
    }
}
