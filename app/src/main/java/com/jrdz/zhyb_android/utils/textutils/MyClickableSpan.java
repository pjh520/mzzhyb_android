package com.jrdz.zhyb_android.utils.textutils;

import android.text.NoCopySpan;
import android.text.style.ClickableSpan;

/**
 * ================================================
 * 项目名称：Pumeng_master
 * 包    名：com.languo.pumeng_master.utils.textutils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/8/3 0003
 * 描    述：(暂时不能用 在某些机型上面 开启无障碍 会崩溃)
 * IndexOutOfBoundsException: setSpan (-1 ... -1) starts before 0
 * 1、常见的可能是下标计算有误，导致start和end负值；
 * 2、网上有人说开启无障碍模式的TalkBack功能，如果start==end也会，已证实；
 * 3、还有一种本人遇到的，实现ClickableSpan时为了解决内存泄漏问题，按照网上的方法实现了NoCopySpan。部分机型可能不支持NoCopySpan的实现，导致异常报错，去除后恢复正常。
 * ================================================
 */
@Deprecated
public abstract class MyClickableSpan extends ClickableSpan  {

}
//implements NoCopySpan