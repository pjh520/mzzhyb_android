package com.jrdz.zhyb_android.utils.upload;

import android.app.Activity;
import android.util.Log;

import androidx.fragment.app.Fragment;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.pic.GlideEngine;
import com.frame.compiler.utils.pic.ImageFileCompressEngine;
import com.frame.compiler.utils.pic.ImageFileCropEngine;
import com.frame.compiler.utils.pic.ImageLoaderUtils;
import com.frame.compiler.utils.pic.MeSandboxFileEngine;
import com.frame.compiler.utils.pic.PicStyleUtils;
import com.frame.compiler.widget.customPop.BottomListPop;
import com.frame.compiler.widget.permission.PermissionDescriptionConvert;
import com.frame.compiler.widget.permission.PermissionHelper;
import com.frame.compiler.widget.toast.ToastUtil;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.mine.model.FileUploadModel;
import com.jrdz.zhyb_android.utils.CommonlyUsedDataUtils;
import com.luck.picture.lib.basic.PictureSelector;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.config.SelectMimeType;
import com.luck.picture.lib.config.SelectModeConfig;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.entity.MediaExtraInfo;
import com.luck.picture.lib.interfaces.OnPermissionsInterceptListener;
import com.luck.picture.lib.interfaces.OnRequestPermissionListener;
import com.luck.picture.lib.interfaces.OnResultCallbackListener;
import com.luck.picture.lib.language.LanguageConfig;
import com.luck.picture.lib.style.PictureSelectorStyle;
import com.luck.picture.lib.utils.MediaUtils;
import com.luck.picture.lib.utils.PictureFileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * ================================================
 * 项目名称：locustszhubo
 * 包    名：com.applications.locustszhubo.live.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/11/3 0003
 * 描    述：压缩上传图片一条龙(单图上传)
 * <p>
 * 串行上传图片 即图片压缩完 上传一张 后才会继续第二张图片的操作
 * ================================================
 */
public class CompressUploadSinglePicUtils {
    private final static String TAG = CompressUploadSinglePicUtils.class.getSimpleName();

    public final static String PIC_ACTIVATEDEVICE_TAG = "1";//注册激活页面
    public final static String PIC_ADDDOCTOR_TAG = "2";//添加医师页面
    public final static String PIC_DOCTOR_SIGN_TAG = "3";//医师签名
    public final static String PIC_PRESCR_TAG = "4";//电子处方临时图片

    private Activity activity;
    private BottomListPop bottomListPop; //图片类型选择弹框
    private IChoosePic iChoosePic;
    private boolean isOriginal,isUcrop;

    private PictureSelectorStyle selectorStyle;
    private ImageFileCompressEngine imageFileCompressEngine;
    private ImageFileCropEngine imageFileCropEngine;
    private int language = LanguageConfig.CHINESE;
    private String mType;
    private PermissionHelper permissionHelper;

    //初始化选择图片
    public void initChoosePic(Activity activity, boolean isOriginal, IChoosePic iChoosePic) {
        initChoosePic(activity,isOriginal,false,iChoosePic);
    }
    public void initChoosePic(Activity activity, boolean isOriginal,boolean isUcrop, IChoosePic iChoosePic) {
        this.activity = activity;
        this.isOriginal = isOriginal;
        this.isUcrop = isUcrop;
        this.iChoosePic = iChoosePic;
        imageFileCompressEngine=new ImageFileCompressEngine();
        selectorStyle = new PictureSelectorStyle();
        if (isUcrop){
            imageFileCropEngine=new ImageFileCropEngine(selectorStyle);
        }
    }

    //显示选择获取的图片的方式
    public void showChoosePicTypeDialog(String type) {
        this.mType=type;
        if (bottomListPop==null){
            bottomListPop = new BottomListPop(activity, "", CommonlyUsedDataUtils.getInstance().getSelectPhotoData(),
                    null, 0, new BottomListPop.IOptionListener() {
                @Override
                public void onItemClick(String str, int positoion, Object otherData) {
                    switch (positoion){
                        case 0://拍照
                            goCamera();
                            break;
                        case 1://从相册选择
                            goAlbum(activity,new MyResultCallback());
                            break;
                    }
                }
            });
        }

        bottomListPop.showPopupWindow();
    }

    //直接拍照获取照片
    public void goCamera() {
        PictureSelector.create(activity)
                .openCamera(SelectMimeType.ofImage())// 单独拍照，也可录像或也可音频 看你传入的类型是图片or视频
                .setCompressEngine(imageFileCompressEngine)//设置相册压缩引擎
                .setSandboxFileEngine(new MeSandboxFileEngine()) //设置相册沙盒目录拷贝引擎
                .setOutputCameraDir(ImageLoaderUtils.getSandboxPath()) //使用相机输出路径
                .setPermissionsInterceptListener(new OnPermissionsInterceptListener() {
                    @Override
                    public void requestPermission(Fragment fragment, String[] permissionArray, OnRequestPermissionListener call) {
                        if (permissionHelper==null){
                            permissionHelper = new PermissionHelper();
                        }

                        permissionHelper.requestPermission(fragment, new PermissionHelper.onPermissionListener() {
                            @Override
                            public void onSuccess() {
                                call.onCall(permissionArray,true);
                            }

                            @Override
                            public void onNoAllSuccess(String noAllSuccessText) {}

                            @Override
                            public void onFail(String failText) {
                                ToastUtil.show(failText);
                            }
                        }, PermissionDescriptionConvert.getPermissionDescription(fragment.getContext(),new ArrayList<>(Arrays.asList(permissionArray))),null,permissionArray);
                    }

                    @Override
                    public boolean hasPermissions(Fragment fragment, String[] permissionArray) {
                        return XXPermissions.isGranted(fragment.getContext(), permissionArray);
                    }
                }) //拦截相册权限处理事件，实现自定义权限
//                .setCameraInterceptListener(getCustomCameraEvent())//拦截相机事件，实现自定义相机
                .forResult(new MyResultCallback());
    }

    //进入相册选取图片
    public void goAlbum(Activity activity,OnResultCallbackListener listener) {
        //进入相册以下是例子：不需要的api可以不写
        PictureSelector.create(activity)
                .openGallery(SelectMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .setImageEngine(GlideEngine.createGlideEngine())// 设置相册图片加载引擎
                .setSelectorUIStyle(PicStyleUtils.getWeChatStyle(selectorStyle)) //设置相册主题
                .setLanguage(language) //设置相册语言
                .setCompressEngine(imageFileCompressEngine) //设置相册压缩引擎
//                .setCropEngine(imageFileCropEngine) //设置相册裁剪引擎
                .setSandboxFileEngine(new MeSandboxFileEngine()) //设置相册沙盒目录拷贝引擎
//                .setExtendLoaderEngine(new MeExtendLoaderEngine()) //设置相册数据源加载引擎
//                .setCameraInterceptListener(new MeOnCameraInterceptListener()) //拦截相机事件，实现自定义相机  不需则设置为null
//                .setEditMediaInterceptListener(new MeOnMediaEditInterceptListener(selectorStyle)) //拦截资源编辑事件，实现自定义编辑
                .setPermissionsInterceptListener(new OnPermissionsInterceptListener() {
                    @Override
                    public void requestPermission(Fragment fragment, String[] permissionArray, OnRequestPermissionListener call) {
                        if (permissionHelper==null){
                            permissionHelper = new PermissionHelper();
                        }
                        if (CompressUploadSinglePicUtils_insured.containsPermission(permissionArray, Permission.READ_EXTERNAL_STORAGE)){
                            permissionArray=new String[]{Permission.READ_MEDIA_IMAGES};
                        }
                        String[] finalPermissionArray = permissionArray;
                        permissionHelper.requestPermission(fragment, new PermissionHelper.onPermissionListener() {
                            @Override
                            public void onSuccess() {
                                call.onCall(finalPermissionArray,true);
                            }

                            @Override
                            public void onNoAllSuccess(String noAllSuccessText) {}

                            @Override
                            public void onFail(String failText) {
                                ToastUtil.show(failText);
                            }
                        }, PermissionDescriptionConvert.getPermissionDescription(fragment.getContext(),new ArrayList<>(Arrays.asList(permissionArray))),null,permissionArray);
                    }

                    @Override
                    public boolean hasPermissions(Fragment fragment, String[] permissionArray) {
                        return XXPermissions.isGranted(fragment.getContext(), permissionArray);
                    }
                }) //拦截相册权限处理事件，实现自定义权限
//                .setSelectLimitTipsListener(new MeOnSelectLimitTipsListener())//拦截选择限制事件，可实现自定义提示
//                .setSelectFilterListener()//拦截不支持的选择项   **
//                .isCameraForegroundService(false) //拍照时是否开启一个前台服务
//                .setRequestedOrientation() //设置屏幕旋转方向   **
//                .setSelectedData() //相册已选数据
//                .setRecyclerAnimationMode(AnimationType.DEFAULT_ANIMATION) //相册列表动画效果
//                .setImageSpanCount(4) //相册列表每行显示个数
                .isOriginalControl(true)
                .isDisplayCamera(true) //是否显示相机入口
                .isPageStrategy(true) //是否开启分页模式
                .setSelectionMode(SelectModeConfig.SINGLE) //单选或是多选
//                .isDirectReturnSingle(false) //单选时是否立即返回
//                .setMaxSelectNum(9) //图片最大选择数量
//                .setMinSelectNum(1) //图片最小选择数量
//                .setMaxVideoSelectNum(1) //视频最大选择数量
//                .setMinVideoSelectNum(1) //视频最小选择数量
//                .setRecordVideoMaxSecond(15) //视频录制最大时长
//                .setRecordVideoMinSecond(1) //视频录制最小时长
//                .setFilterVideoMaxSecond(15) //过滤视频最大时长
//                .setFilterVideoMinSecond(1) //过滤视频最小时长
//                .setSelectMaxDurationSecond(15) //选择最大时长视频或音频
//                .setSelectMinDurationSecond(1) //选择最小时长视频或音频
//                .setVideoQuality() //系统相机录制视频质量
//                .isQuickCapture(false) //使用系统摄像机录制后，是否支持使用系统播放器立即播放视频
//                .isPreviewAudio(false) //是否支持音频预览
//                .isPreviewImage(true) //是否支持预览图片
//                .isPreviewVideo(true) //是否支持预览视频
//                .isPreviewFullScreenMode(true) //预览点击全屏效果
//                .isEmptyResultReturn(true) //支持未选择返回
                .isWithSelectVideoImage(false) //是否支持视频图片同选
                .isSelectZoomAnim(true) //选择缩略图缩放效果
                .isOpenClickSound(false) //是否开启点击音效
//                .isCameraAroundState(false) //是否开启前置摄像头；系统相机 只支持部分机型
//                .isCameraRotateImage(false) //拍照是否纠正旋转图片
//                .isGif(false) //是否显示gif文件
//                .isWebp(false) //是否显示webp文件
//                .isBmp(false) //是否显示bmp文件
//                .setOfAllCameraType(SelectMimeType.ofImage()) //isWithSelectVideoImage模式下相机优先使用权
                .isMaxSelectEnabledMask(true) //达到最大选择数是否开启禁选蒙层
//                .isSyncCover(true) //isPageModel模式下是否强制同步封面，默认false
                .isAutomaticTitleRecyclerTop(true) //点击相册标题是否快速回到第一项
                .isFastSlidingSelect(true) //快速滑动选择
//                .setCameraImageFormat() //拍照图片输出格式
//                .setCameraImageFormatForQ() //拍照图片输出格式，Android Q以上
//                .setCameraVideoFormat() //拍照视频输出格式
//                .setCameraVideoFormatForQ() //拍照视频输出格式，Android Q以上
//                .setOutputCameraDir(ImageLoaderUtils.getSandboxPath()) //使用相机输出路径
//                .setOutputAudioDir(ImageLoaderUtils.getSandboxPath())//使用录音输出路径
//                .setQuerySandboxDir(ImageLoaderUtils.getSandboxPath()) //查询指定目录下的资源
//                .setOutputCameraImageFileName("luck.jpeg") //图片输出文件名
//                .setOutputCameraVideoFileName("luck.mp4") //视频输出文件名
//                .setOutputAudioFileName("luck.mp3") //录音输出文件名
//                .isOnlyObtainSandboxDir(false) //是否只查询指定目录下的资源
//                .setFilterMaxFileSize(30) //过滤最大文件
//                .setFilterMinFileSize(1) //过滤最小文件
//                .setSelectMaxFileSize(30) //最大可选文件大小
//                .setSelectMinFileSize(1) //最小可选文件大小
//                .setQueryOnlyMimeType(PictureMimeType.ofGIF()) //查询指定文件类型
                .setSkipCropMimeType(new String[]{PictureMimeType.ofGIF(), PictureMimeType.ofWEBP()}) //跳过不需要裁剪的类型
                .forResult(listener);
    }

    /**
     * 返回结果回调
     */
    private class MyResultCallback implements OnResultCallbackListener<LocalMedia> {

        @Override
        public void onResult(ArrayList<LocalMedia> result) {
            for (LocalMedia media : result) {
                if (media.getWidth() == 0 || media.getHeight() == 0) {
                    if (PictureMimeType.isHasImage(media.getMimeType())) {
                        MediaExtraInfo imageExtraInfo = MediaUtils.getImageSize(activity, media.getPath());
                        media.setWidth(imageExtraInfo.getWidth());
                        media.setHeight(imageExtraInfo.getHeight());
                    } else if (PictureMimeType.isHasVideo(media.getMimeType())) {
                        MediaExtraInfo videoExtraInfo = MediaUtils.getVideoSize(activity, media.getPath());
                        media.setWidth(videoExtraInfo.getWidth());
                        media.setHeight(videoExtraInfo.getHeight());
                    }
                }
                Log.i(TAG, "文件名: " + media.getFileName());
                Log.i(TAG, "是否压缩:" + media.isCompressed());
                Log.i(TAG, "压缩:" + media.getCompressPath());
                Log.i(TAG, "初始路径:" + media.getPath());
                Log.i(TAG, "绝对路径:" + media.getRealPath());
                Log.i(TAG, "是否裁剪:" + media.isCut());
                Log.i(TAG, "裁剪:" + media.getCutPath());
                Log.i(TAG, "是否开启原图:" + media.isOriginal());
                Log.i(TAG, "原图路径:" + media.getOriginalPath());
                Log.i(TAG, "沙盒路径:" + media.getSandboxPath());
                Log.i(TAG, "水印路径:" + media.getWatermarkPath());
                Log.i(TAG, "视频缩略图:" + media.getVideoThumbnailPath());
                Log.i(TAG, "原始宽高: " + media.getWidth() + "x" + media.getHeight());
                Log.i(TAG, "裁剪宽高: " + media.getCropImageWidth() + "x" + media.getCropImageHeight());
                Log.i(TAG, "文件大小: " + media.getSize());
                Log.i(TAG, "media.getAvailablePath()===="+media.getAvailablePath());
            }

            //开始上传图片
            if (result.size() > 0) {
                iChoosePic.onShowWait();
                ossUpImageDate(result.get(0));
            }
        }

        @Override
        public void onCancel() {
            Log.i(TAG, "PictureSelector Cancel");
        }
    }

    //Object realPath=PictureMimeType.isContent(media.getAvailablePath()) && !media.isCut() && !media.isCompressed() ? Uri.parse(media.getAvailablePath()) : media.getAvailablePath();
    //上传商品图片到oss
    private void ossUpImageDate(LocalMedia media) {
        String availablePath=media.getAvailablePath();
        if (!EmptyUtils.isEmpty(availablePath)){
            upFile(PictureMimeType.isContent(availablePath) && !media.isCut() && !media.isCompressed(),availablePath,media.getWidth(),media.getHeight());
        }else {
            ToastUtil.show("上传失败");
            iChoosePic.onOssUpResult("","",1,1);
            iChoosePic.onHideWait();
        }
    }

    // String imgUrl=isUri?Uri.parse(path):path;
    private void upFile(boolean isUri,String path,int width,int height) {
        // 2022-04-23 模拟上传一张照片成功
        FileUploadModel.sendFileUploadRequest(TAG, mType, new File(path), new CustomerJsonCallBack<FileUploadModel>() {
            @Override
            public void onRequestError(FileUploadModel returnData, String msg) {
                iChoosePic.onHideWait();
                ToastUtil.show(msg);
            }

            @Override
            public void onRequestSuccess(FileUploadModel returnData) {
                iChoosePic.onHideWait();
                FileUploadModel.DataBean data = returnData.getData();
                if (data!=null&&!EmptyUtils.isEmpty(data.getAccessoryId())&&!EmptyUtils.isEmpty(data.getAccessoryUrl())){
                    ToastUtil.show("上传成功");
                    iChoosePic.onOssUpResult(data.getAccessoryId(),data.getAccessoryUrl(),width,height);
                }else {
                    ToastUtil.show("上传失败");
                    iChoosePic.onOssUpResult("","",width,height);
                }
            }
        });
    }

    //销毁工具类
    public void onDeatoryUtils() {
        if (bottomListPop != null && bottomListPop.isShowing()) {
            bottomListPop.dismiss();
            bottomListPop = null;
        }
        if (activity!=null&&!activity.isFinishing()){
            PictureFileUtils.deleteAllCacheDirFile(activity);
        }
    }

    public interface IChoosePic {
        void onOssUpResult(String accessoryId,String url,int width,int height);//上传完

        void onShowWait();//显示加载框

        void onHideWait();//隐藏加载框
    }
}
