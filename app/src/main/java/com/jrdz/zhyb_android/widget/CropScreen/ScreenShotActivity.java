package com.jrdz.zhyb_android.widget.CropScreen;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Window;

import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.ImageUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.base.BaseActivity;
import com.jrdz.zhyb_android.pos.CPayDevice;
import com.jrdz.zhyb_android.pos.CPayPrint;
import com.jrdz.zhyb_android.utils.ScreenshotUtil;

import java.io.FileNotFoundException;

/**
 * Created by wei on 16-9-18.
 * <p>
 * There is totally transparent activity,only has a record permission dialog.
 * If you want to screenshot on other applications,might you need to use this activity to take screenshot.
 */
public class ScreenShotActivity extends BaseActivity {
    public static final String KEY_PATH = "path";
    public static final String KEY_DELAY = "delay_time";
    public static final int REQUEST_MEDIA_PROJECTION = 0x2304;
    private String savedPath;
    private long delay;

    @Override
    public int getLayoutId() {
        return 0;
    }

    @Override
    public void initData() {
        super.initData();
        savedPath = getIntent().getStringExtra(KEY_PATH);
        delay = getIntent().getLongExtra(KEY_DELAY, 0);
        requestScreenShotPermission();
    }

    @Override
    protected void initImmersionBar() {
        mImmersionBar = ImmersionBar.with(this).statusBarDarkFont(true, 0.2f)
                .statusBarColor(R.color.bar_transparent);
        mImmersionBar.init();
    }

    public void requestScreenShotPermission() {
        if (Build.VERSION.SDK_INT >= 21) {
            startActivityForResult(createScreenCaptureIntent(), REQUEST_MEDIA_PROJECTION);
        }
    }

    private Intent createScreenCaptureIntent() {
        //here used media_projection instead of Context.MEDIA_PROJECTION_SERVICE to  make it successfully build on low api.
        return ((MediaProjectionManager) getSystemService("media_projection")).createScreenCaptureIntent();
    }

    @Override
    public void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_MEDIA_PROJECTION: {
                if (resultCode == RESULT_OK && data != null) {
                    getWindow().getDecorView().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Shooter shooter = new Shooter(ScreenShotActivity.this, resultCode, data);
                            shooter.startScreenShot(savedPath, new Shooter.OnShotListener() {
                                @Override
                                public void onFinish(Bitmap bitmap, String savedPath) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            CPayPrint printer = CPayDevice.getCPayPrint();

                                            try {
                                                printer.print(bitmap, new CPayPrint.PrintCallback() {
                                                    @Override
                                                    public void onCall(int code, String msg) {
                                                        if (code != 9999) {
                                                            showShortToast(msg);
                                                        }
                                                        if (bitmap != null && !bitmap.isRecycled()) {
                                                            bitmap.recycle();
                                                        }
                                                    }
                                                });
                                            } catch (Exception e) {
                                                hideWaitDialog();
                                                showTipDialog("打印异常了:"+e.getMessage());
                                                e.printStackTrace();
                                            }

                                            finish(); // don't forget finish activity
                                        }
                                    });
                                }

                                @Override
                                public void onError() {
                                    finish();
                                }
                            });
                        }
                    }, delay);
                } else if (resultCode == RESULT_CANCELED) {
                    finish();
                } else {
                    finish();
                }
            }
        }
    }

    @Override
    protected boolean isSetContentView() {
        return false;
    }

    public static Intent createIntent(Context context, String path, long delay) {
        Intent intent = new Intent(context, ScreenShotActivity.class);
        intent.putExtra(KEY_PATH, path);
        intent.putExtra(KEY_DELAY, delay);
        return intent;
    }
}