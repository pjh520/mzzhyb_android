package com.jrdz.zhyb_android.widget.pagemenu;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.widget.pagemenu.adapter.EntranceAdapter;
import com.jrdz.zhyb_android.widget.pagemenu.adapter.PageViewPagerAdapter;
import com.jrdz.zhyb_android.widget.pagemenu.holder.PageMenuViewHolderCreator;

import java.util.ArrayList;
import java.util.List;

//自定义viewpager，自适应高度
public class PageMenuLayout<T> extends ViewPager {
    private static final int DEFAULT_ROW_COUNT = 2;
    private static final int DEFAULT_SPAN_COUNT = 5;
    /**
     * 行数
     */
    private int mRowCount = DEFAULT_ROW_COUNT;
    /**
     * 列数
     */
    private int mSpanCount = DEFAULT_SPAN_COUNT;
    int lastX = -1;
    int lastY = -1;
    private int pageCount=0;

    public PageMenuLayout(@NonNull Context context) {
        super(context, null);
    }

    public PageMenuLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PageMenuLayout);
        if (typedArray != null) {
            mRowCount = typedArray.getInteger(R.styleable.PageMenuLayout_pagemenu_row_count, DEFAULT_ROW_COUNT);
            mSpanCount = typedArray.getInteger(R.styleable.PageMenuLayout_pagemenu_span_count, DEFAULT_SPAN_COUNT);
            typedArray.recycle();
        }
    }

    /**
     * 设置行数
     *
     * @param rowCount
     */
    public void setRowCount(int rowCount) {
        mRowCount = rowCount;
    }

    /**
     * 设置列数
     *
     * @param spanCount
     */
    public void setSpanCount(int spanCount) {
        mSpanCount = spanCount;
    }

    public void setPageDatas(@NonNull List<T> datas, @NonNull PageMenuViewHolderCreator creator) {
        setPageDatas(mRowCount, mSpanCount, datas, creator);
    }

    public void setPageDatas(int rowCount, int spanCount, @NonNull List<T> datas, @NonNull PageMenuViewHolderCreator creator) {
        if (datas == null) {
            datas = new ArrayList<>();
        }
        mRowCount = rowCount;
        mSpanCount = spanCount;
        if (mRowCount == 0 || mSpanCount == 0) {
            return;
        }
        int pageSize = mRowCount * mSpanCount;
        pageCount = (int) Math.ceil(datas.size() * 1.0 / pageSize);
        List<View> viewList = new ArrayList<>();
        for (int index = 0; index < pageCount; index++) {
            RecyclerView recyclerView = new RecyclerView(this.getContext());
            recyclerView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            recyclerView.setLayoutManager(new GridLayoutManager(this.getContext(), mSpanCount, GridLayoutManager.VERTICAL, false));
            EntranceAdapter<T> entranceAdapter = new EntranceAdapter<>(creator, datas, index, pageSize);
            recyclerView.setAdapter(entranceAdapter);
            viewList.add(recyclerView);
        }
        PageViewPagerAdapter adapter = new PageViewPagerAdapter(viewList);
        setAdapter(adapter);
    }

    //菜单总页数
    public int getPageCount() {
        return pageCount;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int height = 0;
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            int h = child.getMeasuredHeight();
            if (h > height) {
                height = h;
            }
        }
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        int x = (int) ev.getRawX();
        int y = (int) ev.getRawY();
        int dealtX = 0;
        int dealtY = 0;

        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                dealtX = 0;
                dealtY = 0;
                // 保证子View能够接收到Action_move事件
                getParent().requestDisallowInterceptTouchEvent(true);
                break;
            case MotionEvent.ACTION_MOVE:
                dealtX += Math.abs(x - lastX);
                dealtY += Math.abs(y - lastY);

                // 这里是否拦截的判断依据是左右滑动，读者可根据自己的逻辑进行是否拦截
                if (dealtX >= dealtY) { // 左右滑动请求父 View 不要拦截
                    getParent().requestDisallowInterceptTouchEvent(true);
                } else {
                    getParent().requestDisallowInterceptTouchEvent(false);
                }
                lastX = x;
                lastY = y;
                break;
            case MotionEvent.ACTION_CANCEL:
                break;
            case MotionEvent.ACTION_UP:
                break;

        }
        return super.dispatchTouchEvent(ev);
    }
}
