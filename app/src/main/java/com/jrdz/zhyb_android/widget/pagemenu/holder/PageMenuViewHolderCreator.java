package com.jrdz.zhyb_android.widget.pagemenu.holder;

import android.view.View;

public interface PageMenuViewHolderCreator {

    AbstractHolder createHolder(View itemView);

    int getLayoutId();
}
