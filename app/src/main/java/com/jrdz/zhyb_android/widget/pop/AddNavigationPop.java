package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.text.InputFilter;
import android.view.View;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.toast.ToastUtil;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.ShopStyleModel;
import com.jrdz.zhyb_android.utils.ChineseFilter;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AlphaConfig;
import razerdp.util.animation.AnimationHelper;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/11/15 0015
 * 描    述：新增，修改，删除 导航弹框
 * ================================================
 */
public class AddNavigationPop extends BasePopupWindow {
    private String mTag;
    private ShopStyleModel.DataBean.ClassificationBean mShopDecoSortModel;
    private IOptionListener iOptionListener;
    private EditText etContent;
    private TextView tvTitle,tvDecribe01,tvCancle,tvSure,tvDecribe02;

    public AddNavigationPop(Context context, ShopStyleModel.DataBean.ClassificationBean shopDecoSortModel, String tag) {
        super(context);
        this.mTag=tag;
        this.mShopDecoSortModel=shopDecoSortModel;
        setContentView(R.layout.layout_add_navigation_pop);
    }

    public void setOnListener(IOptionListener iOptionListener){
        this.iOptionListener=iOptionListener;
    }

    public void setInputType(int inputType){
        if (etContent!=null){
            etContent.setInputType(inputType);
        }
    }

    //清除输入框的数据
    public void cleanData(){
        if (etContent!=null){
            etContent.setText("");
            etContent.requestFocus();
        }
    }

    public void setFocus(){
        if (etContent!=null){
            etContent.requestFocus();
        }
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        tvTitle=contentView.findViewById(R.id.tv_title);
        etContent=contentView.findViewById(R.id.et_content);
        tvDecribe01=contentView.findViewById(R.id.tv_decribe01);
        tvCancle=contentView.findViewById(R.id.tv_cancle);
        tvSure=contentView.findViewById(R.id.tv_sure);
        tvDecribe02=contentView.findViewById(R.id.tv_decribe02);

        setData(mShopDecoSortModel);
        setPopData(mTag);

        etContent.requestFocus();
        etContent.setFilters(new InputFilter[]{new ChineseFilter(), new InputFilter.LengthFilter(5)});

        tvSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (EmptyUtils.isEmpty(etContent.getText().toString())||etContent.getText().toString().length()<2) {
                    ToastUtil.show("请输入目录名称，2~5个汉字");
                    return;
                }
                KeyboardUtils.hideSoftInput(etContent);
                dismiss();
                if (iOptionListener!=null){
                    switch (tvSure.getText().toString()){
                        case "添加":
                            iOptionListener.onAdd(etContent.getText().toString());
                            break;
                        case "修改":
                            iOptionListener.onUpdate(mShopDecoSortModel.getCatalogueId(),etContent.getText().toString());
                            break;
                    }
                }
            }
        });

        tvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (iOptionListener!=null){
                    switch (tvCancle.getText().toString()){
                        case "取消":
                            iOptionListener.onCancle();
                            break;
                        case "删除":
                            iOptionListener.onDelete(mShopDecoSortModel.getCatalogueId());
                            break;
                    }
                }
            }
        });
    }

    //设置页面删除 更新时 需要携带的展示数据
    public void setData(ShopStyleModel.DataBean.ClassificationBean shopDecoSortModel){
        mShopDecoSortModel=shopDecoSortModel;
        if (etContent!=null&&mShopDecoSortModel!=null){
            etContent.setText(EmptyUtils.strEmpty(shopDecoSortModel.getCatalogueName()));
            etContent.requestFocus();
            RxTool.setEditTextCursorLocation(etContent);
        }
    }

    //设置页面展示数据
    public void setPopData(String tag){
        if (tvTitle==null||tvDecribe01==null||tvCancle==null||tvSure==null||tvDecribe02==null){
            return;
        }
        if ("1".equals(tag)){//新增
            tvTitle.setText("新增导航");
            tvDecribe01.setText("注意:目录个数最多20个，且在首页和分类所有名称不能重复");

            tvCancle.setText("取消");
            tvSure.setText("添加");

            tvDecribe02.setVisibility(View.GONE);
        }else {//删除或者修改
            tvTitle.setText("编辑导航");
            tvDecribe01.setText("提示:店铺所有导航目录名称不能重复");

            tvCancle.setText("删除");
            tvSure.setText("修改");

            tvDecribe02.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withAlpha(AlphaConfig.IN)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withAlpha(AlphaConfig.OUT)
                .toDismiss();
    }

    //清除监听
    public void onCleanListener(){
        if (iOptionListener!=null){
            iOptionListener=null;
        }
    }

    public interface IOptionListener{
        void onAdd(String etContent);
        void onCancle();

        void onDelete(String catalogueId);
        void onUpdate(String catalogueId,String etContent);
    }
}
