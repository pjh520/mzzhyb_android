package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.jrdz.zhyb_android.R;

import java.util.List;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/11/15 0015
 * 描    述：下拉列表弹框
 * ================================================
 */
public class DropDownDataPop extends BasePopupWindow implements BaseQuickAdapter.OnItemClickListener {
    private List<String> datas;
    private IOptionListener iOptionListener;
    private DropDownDataAdapter dropDownDataAdapter;
    private RecyclerView rlProductList;

    public DropDownDataPop(Context context, List<String> datas) {
        super(context);
        this.datas=datas;

        setContentView(R.layout.layout_dropdown_data_pop);
        setWidthAsAnchorView(true);
        setAutoMirrorEnable(true);
        setBackgroundColor(context.getResources().getColor(R.color.bar_transparent));
    }

    //设置数据
    public void setData(List<String> datas){
        this.datas=datas;
        if (dropDownDataAdapter!=null){
            dropDownDataAdapter.setNewData(datas);
        }
//        if (rlProductList!=null){
//            rlProductList.scrollToPosition(0);
//        }
    }

    //设置监听
    public void setOnListener(IOptionListener iOptionListener){
        this.iOptionListener=iOptionListener;
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        rlProductList=contentView.findViewById(R.id.rl_product_list);

        //初始化列表
        dropDownDataAdapter=new DropDownDataAdapter();
        rlProductList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false));
        rlProductList.setHasFixedSize(true);
        rlProductList.setAdapter(dropDownDataAdapter);

        dropDownDataAdapter.setOnItemClickListener(this);
        dropDownDataAdapter.setNewData(datas);
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            return;
        }
        String itemData = ((DropDownDataAdapter) baseQuickAdapter).getItem(i);

        if (iOptionListener!=null){
            iOptionListener.onItemClick(itemData);
        }

        dismiss();
    }

    @Override
    public void onPopupLayout(@NonNull Rect popupRect, @NonNull Rect anchorRect) {
        //计算basepopup中心与anchorview中心方位
        //e.g：算出gravity == Gravity.Left，意味着Popup显示在anchorView的左侧
        int gravity = computeGravity(popupRect, anchorRect);
        //计算垂直位置
        switch (gravity & Gravity.VERTICAL_GRAVITY_MASK) {
            case Gravity.TOP:
                setShowAnimation(AnimationHelper.asAnimation()
                        .withTranslation(TranslationConfig.FROM_BOTTOM)
                        .toShow());
                setDismissAnimation(AnimationHelper.asAnimation()
                        .withTranslation(TranslationConfig.TO_BOTTOM)
                        .toDismiss());
                break;
            case Gravity.BOTTOM:
                setShowAnimation(AnimationHelper.asAnimation()
                        .withTranslation(TranslationConfig.FROM_TOP)
                        .toShow());
               setDismissAnimation(AnimationHelper.asAnimation()
                       .withTranslation(TranslationConfig.TO_TOP)
                       .toDismiss());
                break;
        }
    }

    //清除监听
    public void onCleanListener(){
        if (iOptionListener!=null){
            iOptionListener=null;
        }
    }

    public interface IOptionListener{
        void onItemClick(String item);
    }
}
