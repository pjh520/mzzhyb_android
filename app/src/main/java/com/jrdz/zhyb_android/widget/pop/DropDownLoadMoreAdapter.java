package com.jrdz.zhyb_android.widget.pop;

import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.model.DoctorManageModel;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.ui.live.adapter
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/2/24 0024
 * 描    述：
 * ================================================
 */
public class DropDownLoadMoreAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public DropDownLoadMoreAdapter() {
        super(R.layout.layout_dropdown_data_item, null);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, String item) {
        TextView tv=helper.getView(R.id.tv);

        tv.setText(EmptyUtils.strEmpty(item));
    }
}
