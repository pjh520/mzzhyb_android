package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.flyjingfish.openimagelib.OpenImage;
import com.flyjingfish.openimagelib.enums.MediaType;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.glide.GlideUtils;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_user.activity.GoodDetailActivity;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ProductDescribeModel;

import java.util.List;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.widget.pop
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-07
 * 描    述：药品说明书弹框
 * ================================================
 */
public class DrugManualPop extends BasePopupWindow {
    private List<ProductDescribeModel> data;
    private String picUrl;

    public DrugManualPop(Context context, List<ProductDescribeModel> data, String picUrl) {
        super(context);
        this.data = data;
        this.picUrl = picUrl;
        setContentView(R.layout.layout_drug_manual_pop);
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        ShapeLinearLayout llContain= contentView.findViewById(R.id.ll_contain);
        ImageView ivImg = contentView.findViewById(R.id.iv_img);
        SubsamplingScaleImageView ivLongImg = contentView.findViewById(R.id.iv_long_img);
        ShapeTextView tvSure = contentView.findViewById(R.id.tv_sure);

        GlideUtils.loadLongImg(getContext(),picUrl,ivImg,ivLongImg);

        llContain.removeAllViews();
        if (data!=null){
            for (ProductDescribeModel datum : data) {
                View view= LayoutInflater.from(getContext()).inflate(R.layout.layout_drug_manual_item, llContain, false);
                TextView tvTitle=view.findViewById(R.id.tv_title);
                TextView tvValue=view.findViewById(R.id.tv_value);

                tvTitle.setText(EmptyUtils.strEmptyToText(datum.getTitle(), "--"));
                tvValue.setText(EmptyUtils.strEmptyToText(datum.getDescribe(), "--"));

                llContain.addView(view);
            }
        }

        ivImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenImage.with(getContext())
                        //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                        .setClickImageView(ivImg)
                        //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                        .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                        //RecyclerView的数据
                        .setImageUrl(picUrl, MediaType.IMAGE)
                        //点击的ImageView所在数据的位置
                        .setClickPosition(0)
                        //开始展示大图
                        .show();
            }
        });

        ivLongImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenImage.with(getContext())
                        //点击ImageView所在的RecyclerView（也支持设置setClickViewPager2，setClickViewPager，setClickGridView，setClickListView，setClickImageView，setClickWebView，setNoneClickView）
                        .setNoneClickView()
                        //点击的ImageView的ScaleType类型（如果设置不对，打开的动画效果将是错误的）
                        .setSrcImageViewScaleType(ImageView.ScaleType.CENTER_CROP,true)
                        //RecyclerView的数据
                        .setImageUrl(picUrl, MediaType.IMAGE)
                        //点击的ImageView所在数据的位置
                        .setClickPosition(0)
                        //开始展示大图
                        .show();
            }
        });
        tvSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.FROM_BOTTOM)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.TO_BOTTOM)
                .toDismiss();
    }
}
