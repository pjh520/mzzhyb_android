package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.widget.HandWrite;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/12/11 0011
 * 描    述：手写签名
 * ================================================
 */
public class HandWritePop extends BasePopupWindow {
    private IOptionListener mIOptionListener;
    private HandWrite mHandWrite;

    public HandWritePop(Context context,IOptionListener iOptionListener) {
        super(context);
        this.mIOptionListener=iOptionListener;
        setContentView(R.layout.layout_handwrite_pop);
        setPopupGravity(Gravity.BOTTOM);
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        FrameLayout mFlClose = contentView.findViewById(R.id.fl_close);
        mHandWrite = contentView.findViewById(R.id.handWrite);
        ShapeTextView mTvRewrite = contentView.findViewById(R.id.tv_rewrite);
        ShapeTextView mTvCommit = contentView.findViewById(R.id.tv_commit);

        mFlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mTvRewrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandWrite.reset();
            }
        });

        mTvCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIOptionListener!=null){
                    mIOptionListener.commit(mHandWrite.buildAreaBitmap(true));
                    dismiss();
                }
            }
        });
    }

    //清除信息
    public void onClean(){
        mIOptionListener=null;
        if (mHandWrite!=null){
            mHandWrite.release();
            mHandWrite=null;
        }

        dismiss();
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.FROM_BOTTOM)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.TO_BOTTOM)
                .toDismiss();
    }

    public interface IOptionListener{
        void commit(Bitmap mBitmap);
    }
}
