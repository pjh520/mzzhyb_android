package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frame.compiler.utils.EmptyUtils;
import com.google.android.flexbox.FlexboxLayout;
import com.hjq.shape.view.ShapeEditText;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;

import java.util.ArrayList;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.widget.pop
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-08
 * 描    述：疾病史 弹框
 * ================================================
 */
public class MedicalHistoryPop extends BasePopupWindow {
    private TextView tvTitle;
    private FlexboxLayout fblContain;
    private ShapeEditText etContent;
    private ShapeTextView tvSubmit;

    private ArrayList<String> datas;
    private String title, selectData, etContentStr;
    private IOptionListener iOptionListener;

    public MedicalHistoryPop(Context context, String title, ArrayList<String> datas, String selectData, String etContentStr) {
        super(context);
        this.title = title;
        this.datas = datas;
        this.selectData = selectData;
        this.etContentStr = etContentStr;

        setKeyboardAdaptive(true);
        setContentView(R.layout.layout_medical_history_pop);
    }

    //设置数据
    public void setData(String title, ArrayList<String> datas, String selectData, String etContentStr) {
        this.title = title;
        this.datas = datas;
        this.selectData = selectData;
        this.etContentStr = etContentStr;
        setPopView();
    }

    //设置监听
    public void setOnListener(IOptionListener iOptionListener) {
        this.iOptionListener = iOptionListener;
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        tvTitle = contentView.findViewById(R.id.tv_title);
        fblContain = contentView.findViewById(R.id.fbl_contain);
        etContent = contentView.findViewById(R.id.et_content);
        tvSubmit = contentView.findViewById(R.id.tv_submit);

        setPopView();
    }

    //设置pop view 数据
    private void setPopView() {
        tvTitle.setText(title);
        etContent.setText(EmptyUtils.strEmpty(etContentStr));

        fblContain.removeAllViews();
        for (String data : datas) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_medical_history_item, fblContain, false);
            ShapeTextView stvItem = view.findViewById(R.id.stv_item);

            if (selectData.contains(data)) {
                stvItem.setSelected(true);
                stvItem.getShapeDrawableBuilder().setSolidColor(getContext().getResources().getColor(R.color.color_4870e0))
                        .setStrokeWidth(0).setStrokeColor(getContext().getResources().getColor(R.color.bar_transparent)).intoBackground();

                stvItem.setTextColor(getContext().getResources().getColor(R.color.white));
            } else {
                stvItem.setSelected(false);
                stvItem.getShapeDrawableBuilder().setSolidColor(getContext().getResources().getColor(R.color.bar_transparent))
                        .setStrokeWidth(getContext().getResources().getDimensionPixelSize(R.dimen.dp_1))
                        .setStrokeColor(getContext().getResources().getColor(R.color.txt_color_666)).intoBackground();

                stvItem.setTextColor(getContext().getResources().getColor(R.color.color_333333));
            }
            stvItem.setText(data);

            stvItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (stvItem.isSelected()) {
                        stvItem.setSelected(false);
                        stvItem.getShapeDrawableBuilder().setSolidColor(getContext().getResources().getColor(R.color.bar_transparent))
                                .setStrokeWidth(getContext().getResources().getDimensionPixelSize(R.dimen.dp_1))
                                .setStrokeColor(getContext().getResources().getColor(R.color.txt_color_666)).intoBackground();

                        stvItem.setTextColor(getContext().getResources().getColor(R.color.color_333333));
                    } else {
                        stvItem.setSelected(true);
                        stvItem.getShapeDrawableBuilder().setSolidColor(getContext().getResources().getColor(R.color.color_4870e0))
                                .setStrokeWidth(0).setStrokeColor(getContext().getResources().getColor(R.color.bar_transparent)).intoBackground();

                        stvItem.setTextColor(getContext().getResources().getColor(R.color.white));
                    }
                }
            });

            fblContain.addView(view);
        }

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectDatasStr = "";
                for (int i = 0, size = fblContain.getChildCount(); i < size; i++) {
                    ShapeTextView itemView = (ShapeTextView) fblContain.getChildAt(i);

                    if (itemView.isSelected()) {
                        selectDatasStr += itemView.getText().toString().trim() + ",";
                    }
                }

                if (!EmptyUtils.isEmpty(selectDatasStr)) {
                    selectDatasStr = selectDatasStr.substring(0, selectDatasStr.length() - 1);
                }

                dismiss();
                if (iOptionListener != null) {
                    iOptionListener.sure(selectDatasStr, etContent.getText().toString());
                }
            }
        });
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.FROM_BOTTOM)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.TO_BOTTOM)
                .toDismiss();
    }

    //清除监听
    public void onCleanListener() {
        if (iOptionListener != null) {
            iOptionListener = null;
        }
    }

    public interface IOptionListener {
        void sure(String selectDatas, String etContent);
    }
}
