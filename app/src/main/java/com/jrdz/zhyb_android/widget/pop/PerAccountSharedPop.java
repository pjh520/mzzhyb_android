package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ThreadUtils;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.hjq.shape.layout.ShapeRecyclerView;
import com.huawei.hms.hmsscankit.ScanUtil;
import com.huawei.hms.hmsscankit.WriterException;
import com.huawei.hms.ml.scan.HmsBuildBitmapOption;
import com.huawei.hms.ml.scan.HmsScan;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.home.adapter.MonthSettQueryAdapter;
import com.jrdz.zhyb_android.ui.home.model.MonthSettModel;
import com.jrdz.zhyb_android.ui.insured.adapter.CrlBtnAdapter;
import com.jrdz.zhyb_android.ui.insured.model.CrlBtnModel;

import java.util.ArrayList;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.widget.pop
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-07-26
 * 描    述：
 * ================================================
 */
public class PerAccountSharedPop extends BasePopupWindow {
    private ImageView ivQrCode;

    private String insuranceType,insuredStatus;
    private IOptionListener mIOptionListener;

    public PerAccountSharedPop(Context context,String insuranceType,String insuredStatus,IOptionListener iOptionListener) {
        super(context);
        this.insuranceType=insuranceType;
        this.insuredStatus=insuredStatus;
        this.mIOptionListener=iOptionListener;
        setContentView(R.layout.layout_shared_per_account_pop);
        setPopupGravity(Gravity.BOTTOM);
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        LinearLayout llRoot = contentView.findViewById(R.id.ll_root);
        ShapeLinearLayout sllSaveView= contentView.findViewById(R.id.sll_save_view);
        ShapeRecyclerView srlBottomBtn = contentView.findViewById(R.id.srl_bottom_btn);
        ivQrCode = contentView.findViewById(R.id.iv_qr_code);
        TextView tvInsuranceType= contentView.findViewById(R.id.tv_insurance_type);
        TextView tvInsuredStatus= contentView.findViewById(R.id.tv_insured_status);

        tvInsuranceType.setText(EmptyUtils.strEmptyToText(insuranceType,"--"));
        tvInsuredStatus.setText(EmptyUtils.strEmptyToText(insuredStatus,"--"));

        llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        //初始化按钮的数据
        ArrayList<CrlBtnModel> crlBtnModels = new ArrayList<>();
        CrlBtnModel crlBtnModel01 = new CrlBtnModel(1, R.drawable.ic_download, "保存图片");

        crlBtnModels.add(crlBtnModel01);

        srlBottomBtn.setHasFixedSize(true);
        srlBottomBtn.setLayoutManager(new GridLayoutManager(getContext(),4, RecyclerView.VERTICAL,false));
        //设置recycleview的适配器
        CrlBtnAdapter crlBtnAdapter = new CrlBtnAdapter(crlBtnModels);
        srlBottomBtn.setAdapter(crlBtnAdapter);
        crlBtnAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    return;
                }
                CrlBtnModel item = ((CrlBtnAdapter) baseQuickAdapter).getItem(i);
                if (mIOptionListener!=null&&item!=null){
                    mIOptionListener.onItemClick(item.getId(),sllSaveView);
                }
            }
        });

        setQrCode();
    }

    private void setQrCode() {
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                String content = "http://113.135.194.23:3080/appinstall/zhyb_sign.apk";
                int type = HmsScan.QRCODE_SCAN_TYPE;
                int width = getContext().getResources().getDimensionPixelSize(R.dimen.dp_168);
                int height = width;
                HmsBuildBitmapOption options = new HmsBuildBitmapOption.Creator().setBitmapBackgroundColor(Color.WHITE).setBitmapColor(Color.BLACK).setBitmapMargin(0).create();

                try {
                    // 如果未设置HmsBuildBitmapOption对象，生成二维码参数options置null。
                    Bitmap qrBitmap = ScanUtil.buildBitmap(content, type, width, height, options);
                    getContext().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ivQrCode.setImageBitmap(qrBitmap);
                        }
                    });

                } catch (WriterException e) {
                    Log.w("buildBitmap", e);
                }
            }
        });
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.FROM_BOTTOM)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.TO_BOTTOM)
                .toDismiss();
    }


    //清除信息
    public void onClean(){
        mIOptionListener=null;
        dismiss();
    }

    public interface IOptionListener{
        void onItemClick(int tag,View view);
    }
}
