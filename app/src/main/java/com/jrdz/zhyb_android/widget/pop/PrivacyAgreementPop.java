package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.utils.MMKVUtils;

import razerdp.basepopup.BasePopupWindow;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/11/15 0015
 * 描    述：隐私协议弹框
 * ================================================
 */
public class PrivacyAgreementPop extends BasePopupWindow {
    private IOptionListener iOptionListener;

    public PrivacyAgreementPop(Context context, IOptionListener iOptionListener) {
        super(context);
        this.iOptionListener=iOptionListener;

        setContentView(R.layout.layout_privacy_agreement_pop);
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        ShapeTextView sbtnSure=contentView.findViewById(R.id.sbtn_sure);
        TextView tvCancle=contentView.findViewById(R.id.tv_cancle);
        TextView tvUserAgreement=contentView.findViewById(R.id.tv_user_agreement);
        TextView tvPrivacyPolicy=contentView.findViewById(R.id.tv_privacy_policy);
        sbtnSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                MMKVUtils.putBoolean("privacyAgreemen",true);
                iOptionListener.onAgree();
            }
        });

        tvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                iOptionListener.onCancle();
            }
        });

        tvUserAgreement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iOptionListener.goUserAgreement();
            }
        });

        tvPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iOptionListener.goPrivacyPolicy();
            }
        });
    }

    //清除监听
    public void onCleanListener(){
        if (iOptionListener!=null){
            iOptionListener=null;
        }
    }

    public interface IOptionListener{
        void onAgree();
        void onCancle();
        void goUserAgreement();
        void goPrivacyPolicy();
    }
}
