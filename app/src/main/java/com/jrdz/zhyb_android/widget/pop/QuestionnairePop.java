package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;

import razerdp.basepopup.BasePopupWindow;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/11/15 0015
 * 描    述：调查问卷询问弹框
 * ================================================
 */
public class QuestionnairePop extends BasePopupWindow {
    private IOptionListener iOptionListener;
    private boolean isForce;
    private String questionnaireCode;
    private TextView tvBtn01;

    public QuestionnairePop(Context context,boolean isForce, String questionnaireCode,IOptionListener iOptionListener) {
        super(context);
        this.iOptionListener = iOptionListener;
        this.isForce=isForce;
        this.questionnaireCode = questionnaireCode;
        setContentView(R.layout.layout_customer_dialog02);
    }

    public void setIsForce(boolean isForce,String questionnaireCode){
        this.isForce=isForce;
        this.questionnaireCode = questionnaireCode;
        if (tvBtn01==null)return;
        if (isForce) {//强制更新
            tvBtn01.setVisibility(View.GONE);
            setBackPressEnable(false);
            setOutSideDismiss(false);
        } else {//普通更新
            tvBtn01.setVisibility(View.VISIBLE);
            setBackPressEnable(true);
            setOutSideDismiss(true);
        }
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        TextView tvTitle=contentView.findViewById(com.frame.compiler.R.id.tv_title);
        TextView tvContent=contentView.findViewById(com.frame.compiler.R.id.tv_content);
        tvBtn01=contentView.findViewById(com.frame.compiler.R.id.tv_btn01);
        TextView tvBtn02=contentView.findViewById(com.frame.compiler.R.id.tv_btn02);

        tvBtn01.setText("关闭");
        tvBtn02.setText("去填写");
        //设置标题
        tvTitle.setText("提示");
        //设置更新内容 已经是否强制更新
        tvContent.setText("您有一份调查问卷，请填写！");

        if (isForce) {//强制更新
            tvBtn01.setVisibility(View.GONE);
            setBackPressEnable(false);
            setOutSideDismiss(false);
        } else {//普通更新
            tvBtn01.setVisibility(View.VISIBLE);
            setBackPressEnable(true);
            setOutSideDismiss(true);
        }

        tvBtn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (iOptionListener!=null){
                    iOptionListener.onQuestionnaireCancle();
                }
            }
        });

        tvBtn02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isForce){
                    dismiss();
                }
                if (iOptionListener!=null){
                    iOptionListener.onQuestionnaireAgree(questionnaireCode);
                }
            }
        });
    }

    //清除监听
    public void onCleanListener() {
        if (iOptionListener != null) {
            iOptionListener = null;
        }
    }

    public interface IOptionListener {
        void onQuestionnaireAgree(String code);
        void onQuestionnaireCancle();
    }
}
