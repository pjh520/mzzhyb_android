package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.widget.CostomLoadMoreView;
import com.frame.compiler.widget.CustRefreshLayout;
import com.frame.compiler.widget.toast.ToastUtil;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.net.CustomerJsonCallBack;
import com.jrdz.zhyb_android.ui.home.activity.ScanPayGoodDetailActivity;
import com.jrdz.zhyb_android.ui.home.adapter.ShopProductRightLinkAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.utils.MechanismInfoUtils;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.constant.RefreshState;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.List;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.widget.pop
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-12-26
 * 描    述：在线支付-搜索药品弹框
 * ================================================
 */
public class ScanPaySelectDrugPop extends BasePopupWindow implements View.OnClickListener, BaseQuickAdapter.OnItemClickListener, BaseQuickAdapter.OnItemChildClickListener, BaseQuickAdapter.RequestLoadMoreListener, OnRefreshListener {
    public final String TAG = this.getClass().getSimpleName();

    private FrameLayout flView;
    private TextView tvClose;
    private CustRefreshLayout mRefreshLayout;
    private RecyclerView rlList;

    private String searchText, barCode;
    private IOptionListener iOptionListener;
    private ShopProductRightLinkAdapter shopProductRightLinkAdapter;
    private int mPageNum = 0;


    public ScanPaySelectDrugPop(Context context, String searchText, String barCode) {
        super(context);
        this.searchText = searchText;
        this.barCode = barCode;

        setAlignBackground(true);
        setAlignBackgroundGravity(Gravity.BOTTOM);
        setPopupGravity(GravityMode.RELATIVE_TO_ANCHOR, Gravity.TOP);
        setOutSideTouchable(false);
        setOutSideDismiss(false);
        setContentView(R.layout.layout_scanpay_selectdrug_pop);
    }

    //设置数据
    public void setData(String searchText, String barCode) {
        this.searchText = searchText;
        this.barCode = barCode;

        //请求接口 获取搜索数据
        if (iOptionListener != null) {
            iOptionListener.onShowWait();
        }
        if (mRefreshLayout!=null){
            onRefresh(mRefreshLayout);
        }
    }

    //设置监听
    public void setOnListener(IOptionListener iOptionListener) {
        this.iOptionListener = iOptionListener;
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        flView= contentView.findViewById(R.id.fl_view);
        tvClose = contentView.findViewById(R.id.tv_close);
        mRefreshLayout = contentView.findViewById(R.id.refreshLayout);
        rlList = contentView.findViewById(R.id.rl_list);

        //初始化列表
        setRefreshInfo();
        shopProductRightLinkAdapter = new ShopProductRightLinkAdapter();
        rlList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rlList.setAdapter(shopProductRightLinkAdapter);
        //设置开启上拉加载更多
        shopProductRightLinkAdapter.setEnableLoadMore(true);
        //设置上拉加载更多监听
        shopProductRightLinkAdapter.setOnLoadMoreListener(this, rlList);
        shopProductRightLinkAdapter.setLoadMoreView(new CostomLoadMoreView());
        shopProductRightLinkAdapter.setEmptyView(R.layout.layout_empty_view, rlList);

        shopProductRightLinkAdapter.setOnItemClickListener(this);
        shopProductRightLinkAdapter.setOnItemChildClickListener(this);

        //请求接口 获取搜索数据
        if (iOptionListener != null) {
            iOptionListener.onShowWait();
        }
        if (mRefreshLayout!=null){
            onRefresh(mRefreshLayout);
        }

        tvClose.setOnClickListener(this);
    }

    //设置SmartRefreshLayout的刷新 加载样式
    protected void setRefreshInfo() {
        mRefreshLayout.setEnableRefresh(true);
        mRefreshLayout.setEnableLoadMore(false);
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setPrimaryColorsId(R.color.white, R.color.txt_color_666);
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
        mRefreshLayout.setHeaderMaxDragRate(5);//最大显示下拉高度/Header标准高度  头部下拉高度的比例

        mRefreshLayout.setDisableContentWhenRefresh(true);
    }

    //获取右边列表数据
    private void getRightData() {
        GoodsModel.sendSortGoodsListRequest_mana(TAG, String.valueOf(mPageNum), "20", searchText,
                "", "", MechanismInfoUtils.getFixmedinsCode(), "", "2", barCode,
                new CustomerJsonCallBack<GoodsModel>() {
                    @Override
                    public void onRequestError(GoodsModel returnData, String msg) {
                        dissWaitDailog();
                        setLoadMoreFail();
                        ToastUtil.show(msg);
                    }

                    @Override
                    public void onRequestSuccess(GoodsModel returnData) {
                        dissWaitDailog();
                        List<GoodsModel.DataBean> datas = returnData.getData();
                        if (shopProductRightLinkAdapter != null && datas != null) {
                            //获取数据成功
                            if (mPageNum == 0) {
                                shopProductRightLinkAdapter.setNewData(datas);
                                if (rlList != null) {
                                    rlList.scrollToPosition(0);
                                }
                            } else {
                                shopProductRightLinkAdapter.addData(datas);
                                shopProductRightLinkAdapter.loadMoreComplete();
                            }

                            if (datas.isEmpty()) {
                                if (shopProductRightLinkAdapter.getData().size() < 8) {
                                    shopProductRightLinkAdapter.loadMoreEnd(true);
                                } else {
                                    shopProductRightLinkAdapter.loadMoreEnd();
                                }
                            }
                        }
                    }
                });
    }

    protected void setLoadMoreFail() {
        if (shopProductRightLinkAdapter != null) {
            shopProductRightLinkAdapter.loadMoreFail();
        }

        if (mPageNum > 1) {
            mPageNum -= 1;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_close://关闭
                dismiss();
                break;
        }

    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_1000)) {
            return;
        }
        GoodsModel.DataBean itemData = shopProductRightLinkAdapter.getItem(i);
        ScanPayGoodDetailActivity.newIntance(getContext(), itemData.getGoodsNo());
    }

    @Override
    public void onItemChildClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_1000)) {
            return;
        }
        GoodsModel.DataBean itemData = shopProductRightLinkAdapter.getItem(i);
        switch (view.getId()) {
            case R.id.fl_num_add://数量加
                int num = itemData.getShoppingCartNum();
                if (num + 1 <= itemData.getInventoryQuantity()) {
                    if (iOptionListener != null) {
                        iOptionListener.onSearchResultAddShopcar(itemData,flView,rlList, baseQuickAdapter, view, i, "add");
                    }
                } else {
                    ToastUtil.show("该商品库存不足,请联系商家");
                }
                break;
            case R.id.fl_num_reduce://数量减
                if (iOptionListener != null) {
                    iOptionListener.onSearchResultAddShopcar(itemData,flView,rlList, baseQuickAdapter, view, i, "reduce");
                }
                break;
        }
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        mPageNum = 0;
        getRightData();
    }

    @Override
    public void onLoadMoreRequested() {
        mPageNum++;
        getRightData();
    }

    //隐藏加载框
    public void dissWaitDailog() {
        if (mRefreshLayout != null && mRefreshLayout.getState() == RefreshState.Refreshing) {
            mRefreshLayout.finishRefresh();
        } else {
            if (iOptionListener != null) {
                iOptionListener.onHideWait();
            }
        }
    }

    //清除监听
    public void onCleanListener() {
        if (iOptionListener != null) {
            iOptionListener = null;
        }
    }

    public interface IOptionListener {
        void onShowWait();

        void onHideWait();

        void onSearchResultAddShopcar(GoodsModel.DataBean itemData,ViewGroup viewGroup,RecyclerView recyclerView, BaseQuickAdapter baseQuickAdapter, View view, int pos, String tag);
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.FROM_BOTTOM)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.TO_BOTTOM)
                .toDismiss();
    }
}
