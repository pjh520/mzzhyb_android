package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.PayTypeAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PayTypeModel;

import java.util.ArrayList;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/12/11 0011
 * 描    述：选择支付方式 弹框
 * ================================================
 */
public class SelectPayTypePop extends BasePopupWindow {
    private ArrayList<PayTypeModel> payTypeModels;
    private IOptionListener mIOptionListener;

    public SelectPayTypePop(Context context,ArrayList<PayTypeModel> payTypeModels, IOptionListener iOptionListener) {
        super(context);
        this.mIOptionListener=iOptionListener;
        this.payTypeModels=payTypeModels;
        setContentView(R.layout.layout_select_pay_type_pop);
        setPopupGravity(Gravity.BOTTOM);
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        FrameLayout mFlClose = contentView.findViewById(R.id.fl_close);
        CustomeRecyclerView crvPayType = contentView.findViewById(R.id.crv_pay_type);

        //初始支付方式
        crvPayType.setHasFixedSize(true);
        crvPayType.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false));
        PayTypeAdapter payTypeAdapter=new PayTypeAdapter();
        crvPayType.setAdapter(payTypeAdapter);
        //支付方式数据
        payTypeAdapter.setNewData(payTypeModels);

        mFlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        payTypeAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                dismiss();
                if (mIOptionListener!=null){
                    mIOptionListener.onItemClick(payTypeAdapter.getItem(i));
                }
            }
        });
    }

    //清除信息
    public void onClean(){
        mIOptionListener=null;
        dismiss();
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.FROM_BOTTOM)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.TO_BOTTOM)
                .toDismiss();
    }

    public interface IOptionListener{
        void onItemClick(PayTypeModel payTypeModel);
    }
}
