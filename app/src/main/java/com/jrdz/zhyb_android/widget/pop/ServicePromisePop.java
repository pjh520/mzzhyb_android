package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.view.animation.Animation;

import com.jrdz.zhyb_android.R;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.widget.pop
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-22
 * 描    述：服务承诺弹框
 * ================================================
 */
public class ServicePromisePop extends BasePopupWindow {
    public ServicePromisePop(Context context) {
        super(context);
        setContentView(R.layout.layout_service_promise_pop);
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.FROM_BOTTOM)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.TO_BOTTOM)
                .toDismiss();
    }
}
