package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.widget.toast.ToastUtil;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.GoodsModel;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.ShopCarPopAdapter_user;

import java.util.ArrayList;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.widget.pop
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-05
 * 描    述：店铺详情 以及商品详情 购物车弹框
 * ================================================
 */
public class ShopCarPop_user extends BasePopupWindow implements BaseQuickAdapter.OnItemClickListener, BaseQuickAdapter.OnItemChildClickListener, View.OnClickListener {
    private RecyclerView rlList;
    private TextView tvNum, tvDel, tvPrice;
    private ImageView ivDel;

    private ArrayList<GoodsModel.DataBean> datas;
    private String totalRedNum, totalPrice;
    private IOptionListener iOptionListener;

    private ShopCarPopAdapter_user shopCarPopAdapter;

    public ShopCarPop_user(Context context, ArrayList<GoodsModel.DataBean> datas, String totalRedNum, String totalPrice) {
        super(context);
        this.datas = datas;
        this.totalRedNum = totalRedNum;
        this.totalPrice = totalPrice;

        setMaxHeight(context.getResources().getDimensionPixelSize(R.dimen.dp_750));
        setAlignBackground(true);
        setAlignBackgroundGravity(Gravity.BOTTOM);
        setPopupGravity(GravityMode.RELATIVE_TO_ANCHOR, Gravity.TOP);
        setOutSideTouchable(true);
        setOutSideDismiss(true);
        setContentView(R.layout.layout_shopcar_pop);
    }

    //设置数据
    public void setData(ArrayList<GoodsModel.DataBean> datas, String totalRedNum, String totalPrice) {
        this.datas = datas;
        if (shopCarPopAdapter != null) {
            shopCarPopAdapter.setNewData(datas);
        }
        if (tvNum != null) {
            tvNum.setText("(共" + totalRedNum + "件商品)");
        }
        if (tvPrice != null) {
            tvPrice.setText(totalPrice);
        }
    }

    //设置监听
    public void setOnListener(IOptionListener iOptionListener) {
        this.iOptionListener = iOptionListener;
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        tvNum = contentView.findViewById(R.id.tv_num);
        ivDel = contentView.findViewById(R.id.iv_del);
        tvDel = contentView.findViewById(R.id.tv_del);
        rlList = contentView.findViewById(R.id.rl_list);

        tvNum.setText("(共" + totalRedNum + "件商品)");

        //初始化列表
        shopCarPopAdapter = new ShopCarPopAdapter_user();
        rlList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rlList.setAdapter(shopCarPopAdapter);

        shopCarPopAdapter.setOnItemClickListener(this);
        shopCarPopAdapter.setOnItemChildClickListener(this);
        shopCarPopAdapter.setNewData(datas);

        //底部金额view
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_shopcarpop_foot, null, false);
        tvPrice = view.findViewById(R.id.tv_price);
        tvPrice.setText(totalPrice);
        shopCarPopAdapter.addFooterView(view);

        ivDel.setOnClickListener(this);
        tvDel.setOnClickListener(this);
    }

    @Override
    public boolean onOutSideTouch(MotionEvent event, boolean touchInBackground, boolean isPressed) {
        if (touchInBackground) {
            if (event.getAction() == 1) {
                this.dismiss();
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        if (ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_1000)) {
            GoodsModel.DataBean itemData = ((ShopCarPopAdapter_user) baseQuickAdapter).getItem(i);

            if (iOptionListener != null) {
                iOptionListener.onItemClick(itemData);
            }
            dismiss();
        }
    }

    @Override
    public void onItemChildClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        if (ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            GoodsModel.DataBean itemData = ((ShopCarPopAdapter_user) baseQuickAdapter).getItem(i);

            switch (view.getId()) {
                case R.id.fl_num_add://数量加
                    TextView tvSelectNum01 = (TextView) baseQuickAdapter.getViewByPosition(rlList, i, R.id.tv_select_num);
                    int num = itemData.getShoppingCartNum();
                    if (num+1<=itemData.getInventoryQuantity()){
                        itemData.setShoppingCartNum(num + 1);
                        tvSelectNum01.setText(String.valueOf(itemData.getShoppingCartNum()));

                        if (iOptionListener != null) {
                            iOptionListener.onPopAdd(itemData, tvNum, tvPrice);
                        }
                    }else {
                        ToastUtil.show("该商品库存不足,请联系商家");
                    }
                    break;
                case R.id.fl_num_reduce://数量减
                    TextView tvSelectNum02 = (TextView) baseQuickAdapter.getViewByPosition(rlList, i, R.id.tv_select_num);

                    if (itemData.getShoppingCartNum() == 1) {
                        itemData.setShoppingCartNum(0);
                        datas.remove(i);
                        baseQuickAdapter.notifyDataSetChanged();
                        if (datas.size() == 0) {
                            dismiss();
                        }
                    } else {
                        itemData.setShoppingCartNum(itemData.getShoppingCartNum() - 1);
                        tvSelectNum02.setText(String.valueOf(itemData.getShoppingCartNum()));
                    }

                    if (iOptionListener != null) {
                        iOptionListener.onPopReduce(itemData, tvNum, tvPrice);
                    }
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_del:
            case R.id.tv_del:
                if (ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                    if (iOptionListener != null) {
                        dismiss();
                        iOptionListener.onCleanShopCar();
                    }
                }
                break;
        }
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.FROM_BOTTOM)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.TO_BOTTOM)
                .toDismiss();
    }

    //清除监听
    public void onCleanListener() {
        if (iOptionListener != null) {
            iOptionListener = null;
        }
    }

    public interface IOptionListener {
        void onCleanShopCar();

        void onItemClick(GoodsModel.DataBean item);

        void onPopAdd(GoodsModel.DataBean itemData, TextView tvNum, TextView tvPrice);

        void onPopReduce(GoodsModel.DataBean item, TextView tvNum, TextView tvPrice);
    }
}
