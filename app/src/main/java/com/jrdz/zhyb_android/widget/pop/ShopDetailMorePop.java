package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.utils.ClickUtils;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_user.adapter.ShopDetailMoreAdapter;
import com.jrdz.zhyb_android.ui.zhyf_user.model.ShopDetailMoreModel;

import java.util.ArrayList;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/11/15 0015
 * 描    述：商家首页more弹框
 * ================================================
 */
public class ShopDetailMorePop extends BasePopupWindow implements BaseQuickAdapter.OnItemClickListener {
    private ArrayList<ShopDetailMoreModel> datas;
    private IOptionListener iOptionListener;
    private ShopDetailMoreAdapter shopDetailMoreAdapter;
    private RecyclerView rlProductList;

    public ShopDetailMorePop(Context context, ArrayList<ShopDetailMoreModel> datas) {
        super(context);
        this.datas=datas;

        setContentView(R.layout.layout_shop_detail_more_pop);
//        setBackgroundColor(context.getResources().getColor(R.color.bar_transparent));
    }

    //设置数据
    public void setData(ArrayList<ShopDetailMoreModel> datas){
        this.datas=datas;
        if (shopDetailMoreAdapter!=null){
            shopDetailMoreAdapter.setNewData(datas);
        }
    }

    //设置监听
    public void setOnListener(IOptionListener iOptionListener){
        this.iOptionListener=iOptionListener;
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        rlProductList=contentView.findViewById(R.id.rl_product_list);

        //初始化列表
        shopDetailMoreAdapter=new ShopDetailMoreAdapter();
        rlProductList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false));
        rlProductList.setHasFixedSize(true);
        rlProductList.setAdapter(shopDetailMoreAdapter);

        shopDetailMoreAdapter.setOnItemClickListener(this);
        shopDetailMoreAdapter.setNewData(datas);
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        if (!ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
            return;
        }
        ShopDetailMoreModel itemData = ((ShopDetailMoreAdapter) baseQuickAdapter).getItem(i);

        if (iOptionListener!=null){
            iOptionListener.onItemClick(itemData);
        }

        dismiss();
    }

    //清除监听
    public void onCleanListener(){
        if (iOptionListener!=null){
            iOptionListener=null;
        }
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.FROM_TOP)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.TO_TOP)
                .toDismiss();
    }

    public interface IOptionListener{
        void onItemClick(ShopDetailMoreModel item);
    }
}
