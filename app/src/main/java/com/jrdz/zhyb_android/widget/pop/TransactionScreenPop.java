package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.widget.CustomeRecyclerView;
import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;
import com.jrdz.zhyb_android.ui.zhyf_manage.adapter.TransactionScreenPayTypeAdapter;
import com.jrdz.zhyb_android.ui.zhyf_manage.model.PhaSortModel;

import java.util.ArrayList;
import java.util.List;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/12/11 0011
 * 描    述：选择支付方式 弹框
 * ================================================
 */
public class TransactionScreenPop extends BasePopupWindow {
    private List<PhaSortModel.DataBean> phaSortModels;
    private IOptionListener mIOptionListener;

    public TransactionScreenPop(Context context, List<PhaSortModel.DataBean> phaSortModels, IOptionListener iOptionListener) {
        super(context);
        this.phaSortModels = phaSortModels;
        this.mIOptionListener = iOptionListener;
        setContentView(R.layout.layout_transaction_screen_pop);
        setPopupGravity(Gravity.BOTTOM);
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        FrameLayout flClose = contentView.findViewById(R.id.fl_close);
        CustomeRecyclerView crvPayType = contentView.findViewById(R.id.crv_pay_type);
        CustomeRecyclerView crvPurchasedType = contentView.findViewById(R.id.crv_purchased_type);
        ShapeTextView tvCancle = contentView.findViewById(R.id.tv_cancle);
        ShapeTextView tvSure = contentView.findViewById(R.id.tv_sure);
        //初始支付方式
        crvPayType.setHasFixedSize(true);
        crvPayType.setLayoutManager(new GridLayoutManager(getContext(), 3, RecyclerView.VERTICAL, false));
        TransactionScreenPayTypeAdapter payTypeAdapter = new TransactionScreenPayTypeAdapter();
        crvPayType.setAdapter(payTypeAdapter);
        //支付方式数据
        ArrayList<PhaSortModel.DataBean> payTypeModels = new ArrayList<>();
        payTypeModels.add(new PhaSortModel.DataBean("2", "1", "医保"));
        payTypeModels.add(new PhaSortModel.DataBean("2", "2", "支付宝"));
        payTypeModels.add(new PhaSortModel.DataBean("2", "3", "微信"));
        payTypeModels.add(new PhaSortModel.DataBean("2", "4", "企业基金"));
        payTypeModels.add(new PhaSortModel.DataBean("2", "5", "其他"));

        payTypeAdapter.setNewData(payTypeModels);

        //初始购药种类
        crvPurchasedType.setHasFixedSize(true);
        crvPurchasedType.setLayoutManager(new GridLayoutManager(getContext(), 4, RecyclerView.VERTICAL, false));
        TransactionScreenPayTypeAdapter purchasedTypeAdapter = new TransactionScreenPayTypeAdapter();
        crvPurchasedType.setAdapter(purchasedTypeAdapter);
        //购药种类数据
        purchasedTypeAdapter.setNewData(phaSortModels);
        payTypeAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                PhaSortModel.DataBean itemData = payTypeAdapter.getItem(i);
                ShapeTextView stvText = view.findViewById(R.id.stv_text);

                if ("0".equals(itemData.getChoose())) {//未选中  那么点击之后 要选中
                    ShapeTextView currentFlItem = payTypeAdapter.getCurrentFlItem();
                    //判断是否已经有选中的 有的话 直接设置为未选中
                    if (currentFlItem != null) {
                        currentFlItem.getShapeDrawableBuilder().setSolidColor(Color.parseColor("#F7F6FB")).intoBackground();
                        currentFlItem.setTextColor(getContext().getResources().getColor(R.color.color_333333));
                        itemData.setChoose("0");
                    }
                    PhaSortModel.DataBean currentData = payTypeAdapter.getCurrentData();
                    if (currentData != null) {
                        currentData.setChoose("0");
                    }

                    stvText.getShapeDrawableBuilder().setSolidColor(Color.parseColor("#ECF0FC")).intoBackground();
                    stvText.setTextColor(getContext().getResources().getColor(R.color.color_4870e0));
                    itemData.setChoose("1");

                    payTypeAdapter.setCurrentFlItem(stvText);
                    payTypeAdapter.setCurrentData(itemData);
                } else {//已选中  那么点击之后 要未选中
                    stvText.getShapeDrawableBuilder().setSolidColor(Color.parseColor("#F7F6FB")).intoBackground();
                    stvText.setTextColor(getContext().getResources().getColor(R.color.color_333333));
                    itemData.setChoose("0");

                    payTypeAdapter.setCurrentFlItem(null);
                    payTypeAdapter.setCurrentData(null);
                }
            }
        });

        purchasedTypeAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                PhaSortModel.DataBean itemData = purchasedTypeAdapter.getItem(i);
                ShapeTextView stvText = view.findViewById(R.id.stv_text);

                if ("0".equals(itemData.getChoose())) {//未选中  那么点击之后 要选中
                    ShapeTextView currentFlItem = purchasedTypeAdapter.getCurrentFlItem();
                    //判断是否已经有选中的 有的话 直接设置为未选中
                    if (currentFlItem != null) {
                        currentFlItem.getShapeDrawableBuilder().setSolidColor(Color.parseColor("#F7F6FB")).intoBackground();
                        currentFlItem.setTextColor(getContext().getResources().getColor(R.color.color_333333));
                    }
                    PhaSortModel.DataBean currentData = purchasedTypeAdapter.getCurrentData();
                    if (currentData != null) {
                        currentData.setChoose("0");
                    }

                    stvText.getShapeDrawableBuilder().setSolidColor(Color.parseColor("#ECF0FC")).intoBackground();
                    stvText.setTextColor(getContext().getResources().getColor(R.color.color_4870e0));
                    itemData.setChoose("1");

                    purchasedTypeAdapter.setCurrentFlItem(stvText);
                    purchasedTypeAdapter.setCurrentData(itemData);
                } else {//已选中  那么点击之后 要未选中
                    stvText.getShapeDrawableBuilder().setSolidColor(Color.parseColor("#F7F6FB")).intoBackground();
                    stvText.setTextColor(getContext().getResources().getColor(R.color.color_333333));

                    itemData.setChoose("0");
                    purchasedTypeAdapter.setCurrentFlItem(null);
                    purchasedTypeAdapter.setCurrentData(null);
                }
            }
        });

        flClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        tvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        tvSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                PhaSortModel.DataBean payTypeDatas = payTypeAdapter.getCurrentData();
                PhaSortModel.DataBean purchasedTypeDatas = purchasedTypeAdapter.getCurrentData();

                if (mIOptionListener != null) {
                    mIOptionListener.onSure(payTypeDatas != null ? payTypeDatas.getCatalogueId() : "",
                            purchasedTypeDatas != null ? purchasedTypeDatas.getCatalogueId() : "");
                }
            }
        });
    }

    //清除信息
    public void onClean() {
        mIOptionListener = null;
        dismiss();
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.FROM_BOTTOM)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.TO_BOTTOM)
                .toDismiss();
    }

    public interface IOptionListener {
        void onSure(String payType, String purchasedType);
    }
}
