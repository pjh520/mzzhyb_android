package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.text.InputFilter;
import android.view.View;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.text.MoneyValueFilter;
import com.jrdz.zhyb_android.R;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AlphaConfig;
import razerdp.util.animation.AnimationHelper;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/11/15 0015
 * 描    述：修改数量弹框
 * ================================================
 */
public class UpdateNumPop extends BasePopupWindow {
    private IOptionListener iOptionListener;
    private EditText etContent;
    private MoneyValueFilter moneyValueFilter;
    private int digits=2;

    public UpdateNumPop(Context context) {
        super(context);
        setContentView(R.layout.layout_update_num_pop);
    }

    public void setOnListener(IOptionListener iOptionListener){
        this.iOptionListener=iOptionListener;
    }

    public void setContent(String content){
        if (etContent!=null){
            etContent.setText(EmptyUtils.strEmpty(content));
            etContent.requestFocus();
            RxTool.setEditTextCursorLocation(etContent);
        }
    }

    public void setInputType(int inputType){
        if (etContent!=null){
            etContent.setInputType(inputType);
        }
    }

    //设置输入金额的限制
    public void setFilters(int d) {
        digits=d;
        if (moneyValueFilter==null){
            moneyValueFilter=new MoneyValueFilter();
        }
        moneyValueFilter.setDigits(d);
        if (etContent!=null){
            etContent.setFilters(new InputFilter[]{moneyValueFilter});
        }
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        etContent=contentView.findViewById(R.id.et_content);
        TextView tvCancle=contentView.findViewById(R.id.tv_cancle);
        TextView tvSure=contentView.findViewById(R.id.tv_sure);
        etContent.requestFocus();
        //设置输入金额的限制
        if (moneyValueFilter==null){
            moneyValueFilter=new MoneyValueFilter();
        }
        moneyValueFilter.setDigits(digits);
        etContent.setFilters(new InputFilter[]{moneyValueFilter});

        tvSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyboardUtils.hideSoftInput(etContent);
                dismiss();

                if (iOptionListener!=null){
                    iOptionListener.onAgree(EmptyUtils.isEmpty(etContent.getText().toString())?"0":etContent.getText().toString());
                }
            }
        });

        tvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (iOptionListener!=null){
                    iOptionListener.onCancle();
                }
            }
        });
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withAlpha(AlphaConfig.IN)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withAlpha(AlphaConfig.OUT)
                .toDismiss();
    }

    //清除监听
    public void onCleanListener(){
        if (iOptionListener!=null){
            iOptionListener=null;
        }
    }

    public interface IOptionListener{
        void onAgree(String data);
        void onCancle();
    }
}
