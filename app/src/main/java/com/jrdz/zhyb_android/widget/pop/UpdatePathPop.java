package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.KeyboardUtils;
import com.frame.compiler.utils.RxTool;
import com.jrdz.zhyb_android.R;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AlphaConfig;
import razerdp.util.animation.AnimationHelper;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/11/15 0015
 * 描    述：修改地址弹框
 * ================================================
 */
public class UpdatePathPop extends BasePopupWindow {
    private IOptionListener iOptionListener;
    private TextView tvParentPath;
    private EditText etContent;


    public UpdatePathPop(Context context) {
        super(context);
        setContentView(R.layout.layout_update_path_pop);
    }

    public void setOnListener(IOptionListener iOptionListener){
        this.iOptionListener=iOptionListener;
    }

    public void setParentPath(String parentPath){
        if (tvParentPath!=null){
            tvParentPath.setText(EmptyUtils.strEmpty(parentPath));
        }
    }

    public void setContent(String content){
        if (etContent!=null){
            etContent.setText(EmptyUtils.strEmpty(content));
            etContent.requestFocus();
            RxTool.setEditTextCursorLocation(etContent);
        }
    }

    public void setInputType(int inputType){
        if (etContent!=null){
            etContent.setInputType(inputType);
        }
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        tvParentPath=contentView.findViewById(R.id.tv_parent_path);
        etContent=contentView.findViewById(R.id.et_content);
        TextView tvCancle=contentView.findViewById(R.id.tv_cancle);
        TextView tvSure=contentView.findViewById(R.id.tv_sure);
        etContent.requestFocus();
        tvSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyboardUtils.hideSoftInput(etContent);
                dismiss();

                if (iOptionListener!=null){
                    String content="";
                    if (EmptyUtils.isEmpty(etContent.getText().toString())){
                        content="/";
                    }else if (!etContent.getText().toString().startsWith("/")){
                        content="/"+etContent.getText().toString();
                    }else {
                        content=etContent.getText().toString();
                    }

                    iOptionListener.onAgree(content);
                }
            }
        });

        tvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (iOptionListener!=null){
                    iOptionListener.onCancle();
                }
            }
        });
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withAlpha(AlphaConfig.IN)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withAlpha(AlphaConfig.OUT)
                .toDismiss();
    }

    //清除监听
    public void onCleanListener(){
        if (iOptionListener!=null){
            iOptionListener=null;
        }
    }

    public interface IOptionListener{
        void onAgree(String data);
        void onCancle();
    }
}
