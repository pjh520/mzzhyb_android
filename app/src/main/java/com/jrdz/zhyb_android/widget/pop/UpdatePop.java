package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.frame.compiler.utils.EmptyUtils;
import com.jrdz.zhyb_android.R;

import razerdp.basepopup.BasePopupWindow;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/11/15 0015
 * 描    述：更新弹框
 * ================================================
 */
public class UpdatePop extends BasePopupWindow {
    private IOptionListener iOptionListener;
    private String content;
    private boolean isForce;

    public UpdatePop(Context context, String content, boolean isForce, IOptionListener iOptionListener) {
        super(context);
        this.iOptionListener = iOptionListener;
        this.content=content;
        this.isForce=isForce;
        setContentView(R.layout.layout_update_agreement_pop);
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        TextView contentText = contentView.findViewById(R.id.contentText);
        TextView mCancelButton = contentView.findViewById(R.id.cancel_button);
        TextView mSureButton = contentView.findViewById(R.id.sure_button);
        //设置更新内容 已经是否强制更新
        contentText.setText(EmptyUtils.strEmpty(content));

        if (isForce) {//强制更新
            mCancelButton.setVisibility(View.GONE);
            setBackPressEnable(false);
            setOutSideDismiss(false);
        } else {//普通更新
            mCancelButton.setVisibility(View.VISIBLE);
            setBackPressEnable(true);
            setOutSideDismiss(true);
        }

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                iOptionListener.onCancle();
            }
        });

        mSureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isForce){
                    dismiss();
                }

                iOptionListener.onAgree();
            }
        });
    }

    //清除监听
    public void onCleanListener() {
        if (iOptionListener != null) {
            iOptionListener = null;
        }
    }

    public interface IOptionListener {
        void onAgree();
        void onCancle();
    }
}
