package com.jrdz.zhyb_android.widget.pop;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.hjq.shape.view.ShapeTextView;
import com.jrdz.zhyb_android.R;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.widget.pop
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-11-07
 * 描    述：获取验证码弹框
 * ================================================
 */
public class VerificationCodePop extends BasePopupWindow {
    private IOptionListener iOptionListener;

    public VerificationCodePop(Context context, IOptionListener iOptionListener) {
        super(context);
        this.iOptionListener=iOptionListener;
        setKeyboardAdaptive(true);
        setContentView(R.layout.layout_verification_code_pop);
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        EditText etVerifCode = contentView.findViewById(R.id.et_verif_code);
        TextView tvTime = contentView.findViewById(R.id.tv_time);
        ShapeTextView tvNext = contentView.findViewById(R.id.tv_next);

        tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iOptionListener.sendSms(tvTime);
            }
        });

        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iOptionListener.next(etVerifCode.getText().toString(),tvTime);
            }
        });
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.FROM_BOTTOM)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.TO_BOTTOM)
                .toDismiss();
    }

    //清除监听
    public void onCleanListener(){
        if (iOptionListener!=null){
            iOptionListener=null;
        }
    }

    public interface IOptionListener{
        void sendSms(TextView tvTime);
        void next(String verifCode,TextView tvTime);
    }
}
