package com.jrdz.zhyb_android.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.frame.compiler.utils.LogUtils;
import com.jrdz.zhyb_android.constant.Constants;
import com.jrdz.zhyb_android.msgEvent.cody.MsgBus;
import com.kuaiqian.fusedpay.entity.WeChatMiniProgramResultStatus;
import com.kuaiqian.fusedpay.utils.a;
import com.kuaiqian.fusedpay.utils.LogUtil;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

/**
 * 参考类
 * 微信支付结果回调页面
 * （当集成方的app已经集成了微信支付，并且依然想要使用我们的微信支付时【kqFusedApplicationId为com.bill99】，
 * 集成方的IWXAPIEventHandler 的实现类中加入聚合SDK微信支付返回的结果的接收代码）
 * Created by wangzhe
 */
public class WechatPayEntryActivity extends Activity implements IWXAPIEventHandler {
    private IWXAPI mWechatApi;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtil.i("WechatPayEntryActivity onCreate");
        try {
            //示例445557788888：集成方在微信开放平台申请的AppId
            mWechatApi = WXAPIFactory.createWXAPI(this, Constants.Configure.WX_APPID);
            mWechatApi.handleIntent(getIntent(), this);
        } catch (Exception e) {
            LogUtil.e("handle wechat intent error", e);
            finish();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtil.i("WechatPayEntryActivity onNewIntent");
        try {
            setIntent(intent);
            mWechatApi.handleIntent(intent, this);
        } catch (Exception e) {
            LogUtil.e("handle wechat intent error", e);
            finish();
        }
    }

    @Override
    public void onReq(BaseReq baseReq) {

    }

    @Override
    public void onResp(BaseResp baseResp) {
        LogUtils.e("onResponse", "baseResp.errCode==" + baseResp.errCode + " errMsg:" + baseResp.errStr);

        try {
            switch (baseResp.getType()){
                case ConstantsAPI.COMMAND_LAUNCH_WX_MINIPROGRAM://从小程序回来
                    WXLaunchMiniProgram.Resp launchMiniProResp = (WXLaunchMiniProgram.Resp) baseResp;
                    //对应小程序组件 <button open-type="launchApp"> 中的 app-parameter 属性
                    String extraData = launchMiniProResp.extMsg;
                    LogUtils.e("onResponse", "mini program extMsg = " + extraData);

                    if (!TextUtils.isEmpty(a.a().e())) {//快钱支付的情况
                        Intent intent = new Intent();
                        intent.setClassName(this, a.a().e());
                        Bundle bundle = new Bundle();
                        bundle.putString("resultStatus", WeChatMiniProgramResultStatus.getCodeByResultStatus(extraData));
                        bundle.putString("resultMessage", WeChatMiniProgramResultStatus.getMsgByResultStatus(extraData));

                        intent.putExtras(bundle);
                        startActivity(intent);
                    } else {//视频处方的情况
                        MsgBus.sendAppletData_user().post(extraData);
                    }
                    break;
            }
        } catch (Exception e) {
            LogUtil.e("handle wechat result error", e);
        } finally {
            finish();
        }
    }
}
