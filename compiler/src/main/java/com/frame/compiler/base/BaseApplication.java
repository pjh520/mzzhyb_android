package com.frame.compiler.base;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;
import com.bumptech.glide.Glide;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.ThreadUtils;
import com.hjq.toast.ToastUtils;
import com.tencent.mmkv.MMKV;
import org.litepal.LitePal;
import java.util.List;

/**
 * project:PJHAndroidFrame
 * package:com.frame.pjhandroidframe.base
 * Created by 彭俊鸿 on 2018/6/22.
 * e-mail : 1031028399@qq.com
 * Application基类
 */

public class BaseApplication extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MMKV.initialize(this);
        RxTool.init(this);
        String curProcessName =getProcessName(this, android.os.Process.myPid());
        if (curProcessName != null && curProcessName.equalsIgnoreCase(this.getPackageName())) {
            mainThreadInit();
        }
    }

    //在主进程中初始化
    public void mainThreadInit(){
        ThreadUtils.getSinglePool().execute(new Runnable() {
            @Override
            public void run() {
                asyncMethod();
            }
        });
        // 初始化 Toast 框架
        ToastUtils.init(this);
        initLitePal();
    }

    //异步方法进行初始化
    public void asyncMethod() {

    }

    //初始化数据库
    private void initLitePal() {
        LitePal.initialize(this);
    }

    // 获取进程名
    public String getProcessName(Context cxt, int pid) {
        android.app.ActivityManager am = (android.app.ActivityManager) cxt.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningApps = am.getRunningAppProcesses();
        if (runningApps == null) {
            return null;
        }
        for (android.app.ActivityManager.RunningAppProcessInfo procInfo : runningApps) {
            if (procInfo.pid == pid) {
                return procInfo.processName;
            }
        }
        return null;
    }

    /**
     * 低内存的时候执行
     */
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        // 清理所有图片内存缓存
        Glide.get(this).clearMemory();
    }

    /**
     * HOME键退出应用程序
     * 程序在内存清理的时候执行
     */
    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (level == TRIM_MEMORY_UI_HIDDEN) {
            Glide.get(this).clearMemory();
        }
        // 根据手机内存剩余情况清理图片内存缓存
        Glide.get(this).trimMemory(level);
    }
}
