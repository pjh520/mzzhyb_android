package com.frame.compiler.base;

import android.os.Environment;

import com.frame.compiler.utils.FileUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.utils.storage.FileStorageUtils;

import java.io.File;

/**
 * <p>project:yunpay_v2</p>
 * <p>package:com.justep.yunpay.comlibrary.config</p>
 * <p>Created by kqh on 2017/7/14.</p>
 * <p>e-mail : 752498932@qq.com </p>
 * 各种文件类 存储位置
 */

public class BaseGlobal {
    private final static String IMAGE_DIR = "/image/";//图片文件夹
    private final static String PHOTO_CACHE_DIR = "/photo_cache_dir/";//拍照缓存文件夹
    private final static String IMAGE_GLIDE_DIR = "/glidecache/";//glide缓存
    private final static String IMAGE_COMPRESSED_TEMP_DIR = "compresscache/.nomedia/";//图片压缩文件夹
    private final static String IMAGE_CROP = "crop/.nomedia/";//图片裁剪文件夹
    private final static String VIDEO_DIR = "/video/";//视频文件夹
    private final static String COMPRESSED_VIDEO_DIR = "compressedVideo/.nomedia/";//视频压缩文件夹
    private final static String AUDIO_DIR = "/audio/";//音频文件夹
    private final static String APK_DIR = "/apk/";//apk文件夹
    private final static String FILE_DIR = "/file/";//文件文件夹
    private final static String CRASH_DIR = "/crash/";//崩溃日志文件夹
    private final static String PIC_SHOT_DIR = "/picshot/";//图片截图文件夹
    private final static String PDF_DIR = "/pdf/";//pdf文件夹

    public static String suitBaseDir() {
        String baseDir = "";
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            File baseDirFile = RxTool.getContext().getExternalFilesDir(FileStorageUtils.EXTERNAL_FILE_DIRECTORY);
            if (baseDirFile == null) {
                baseDir = RxTool.getContext().getFilesDir().getAbsolutePath();
            } else {
                baseDir = baseDirFile.getAbsolutePath();
            }
        } else {
            baseDir = RxTool.getContext().getFilesDir().getAbsolutePath();
        }
        //判断文件夹是否存在 不存在创建
        FileUtils.fileDirExis(baseDir);
        return baseDir;
    }

    public static String getCache() {
        String baseDir = "";
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            File baseDirFile = RxTool.getContext().getExternalCacheDir();
            if (baseDirFile == null) {
                baseDir = RxTool.getContext().getCacheDir().getAbsolutePath();
            } else {
                baseDir = baseDirFile.getAbsolutePath();
            }
        } else {
            baseDir = RxTool.getContext().getCacheDir().getAbsolutePath();
        }
        //判断文件夹是否存在 不存在创建
        FileUtils.fileDirExis(baseDir);
        return baseDir;
    }

    //存储图片文件夹
    public static String getImageDir() {
        return suitBaseDir() + IMAGE_DIR;
    }
    //拍照缓存文件夹
    public static String getPhotoCacheDir() {
        return RxTool.getContext().getFilesDir().getAbsolutePath() + PHOTO_CACHE_DIR;
    }
    //glide图片缓存文件夹
    public static String getImageGlideDir() {
        return getCache() + IMAGE_GLIDE_DIR;
    }

    //图片压缩文件夹
    public static String getImageCompressedTempDir() {
        return getImageDir() + IMAGE_COMPRESSED_TEMP_DIR;
    }

    //图片裁剪
    public static String getImageCrop() {
        return getImageDir() + IMAGE_CROP;
    }

    //存储视频文件夹
    public static String getVideoDir() {
        return suitBaseDir() + VIDEO_DIR;
    }

    //存储视频压缩文件夹
    public static String getVideoCompressedDir() {
        return suitBaseDir() + COMPRESSED_VIDEO_DIR;
    }

    //存储音频文件夹
    public static String getAudioDir() {
        return suitBaseDir() + AUDIO_DIR;
    }

    //存储下载的apk文件夹
    public static String getApkDir() {
        return suitBaseDir() + APK_DIR;
    }

    //Crash日志文件夹
    public static String getCrashDir() {
        return suitBaseDir() + CRASH_DIR;
    }

    //存储文件的文件夹
    public static String getFileDir() {
        return suitBaseDir() + FILE_DIR;
    }

    //图片截图文件夹
    public static String getPicShotDir() {
        return suitBaseDir() + PIC_SHOT_DIR;
    }

    //PDF文件夹
    public static String getPdfDir() {
        return suitBaseDir() + PDF_DIR;
    }
}
