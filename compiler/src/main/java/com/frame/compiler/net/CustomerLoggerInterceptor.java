package com.frame.compiler.net;

import android.text.TextUtils;

import com.frame.compiler.utils.LogUtils;

import java.io.IOException;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.frame.compiler.net
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-03-30
 * 描    述：
 * ================================================
 */
public class CustomerLoggerInterceptor implements Interceptor {
    public static final String TAG = "LogUtils";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        logForRequest(request);
        Response response = chain.proceed(request);
        return logForResponse(response);
    }

    //打印请求
    private void logForRequest(Request request) {
        try {
            String url = request.url().toString();
            Headers headers = request.headers();
            LogUtils.e(TAG, "========request'log=======");
            LogUtils.e(TAG, "method : " + request.method());
            LogUtils.e(TAG, "url : " + url);
            if (headers != null && headers.size() > 0) {
                LogUtils.e(TAG, "headers : " + headers.toString());
            }
            RequestBody requestBody = request.body();
            if (requestBody != null) {
                MediaType mediaType = requestBody.contentType();
                if (mediaType != null) {
                    LogUtils.e(TAG, "requestBody's contentType : " + mediaType.toString());
                    if (isText(mediaType)) {
                        LogUtils.e(TAG, "requestBody's content : " + bodyToString(request));
                    } else {
                        LogUtils.e(TAG, "requestBody's content : " + " maybe [file part] , too large too print , ignored!");
                    }
                }
            }
            LogUtils.e(TAG, "========request'log=======end");
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    //打印返回的数据
    private Response logForResponse(Response response) {
        try {
            //===>response log
            LogUtils.e(TAG, "========response'log=======");
            Response.Builder builder = response.newBuilder();
            Response clone = builder.build();
            LogUtils.e(TAG, "url : " + clone.request().url());
            LogUtils.e(TAG, "code : " + clone.code());
            LogUtils.e(TAG, "protocol : " + clone.protocol());
            if (!TextUtils.isEmpty(clone.message())){
                LogUtils.e(TAG, "message : " + clone.message());
            }

            ResponseBody body = clone.body();
            if (body != null) {
                MediaType mediaType = body.contentType();
                if (mediaType != null) {
                    LogUtils.e(TAG, "responseBody's contentType : " + mediaType.toString());
                    if (isText(mediaType)) {
                        String resp = body.string();
                        LogUtils.json(TAG, resp);

                        body = ResponseBody.create(mediaType, resp);
                        return response.newBuilder().body(body).build();
                    } else {
                        LogUtils.e(TAG, "responseBody's content : " + " maybe [file part] , too large too print , ignored!");
                    }
                }
            }

            LogUtils.e(TAG, "========response'log=======end");
        } catch (Exception e) {
//            e.printStackTrace();
        }

        return response;
    }

    private boolean isText(MediaType mediaType) {
        if (mediaType.type() != null && mediaType.type().equals("text")) {
            return true;
        }
        if (mediaType.subtype() != null) {
            if (mediaType.subtype().equals("json") || mediaType.subtype().equals("xml")
                    || mediaType.subtype().equals("html") || mediaType.subtype().equals("webviewhtml")) {
                return true;
            }
        }
        return false;
    }

    private String bodyToString(final Request request) {
        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "something error when show requestBody.";
        }
    }
}
