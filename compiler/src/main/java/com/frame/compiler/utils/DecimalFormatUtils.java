package com.frame.compiler.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * ================================================
 * 项目名称：SuperWarehouse_Android
 * 包    名：com.aten.compiler.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2019/10/25 0025
 * 描    述：
 * ================================================
 */
public class DecimalFormatUtils {
    //保留小数的格式
    public static String keepPlaces(String pattern, double o){
        DecimalFormat df = new DecimalFormat(pattern);
        return df.format(o);
    }
    //保留小数的格式(去除小数点后面的0)
    public static String noZero(String s){
        if(!EmptyUtils.isEmpty(s)&&s.indexOf(".") > 0){
            s = s.replaceAll("0+?$", "");//去掉多余的0
            s = s.replaceAll("[.]$", "");//如最后一位是.则去掉
        }
        return s;
    }

    //保留小数的格式(保留小数点后面的两位)
    public static String twoZero(double o){
        DecimalFormat df = new DecimalFormat("#.00");
        return df.format(o);
    }
}
