package com.frame.compiler.utils;

import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class LogUtils {
    private static boolean mIsShowLog=false;

    public static void init(boolean isShowLog){
        mIsShowLog=isShowLog;
    }

    //打印日志e
    public static void e(String msg) {
        if (mIsShowLog) {
            e("",msg);
        }
    }

    //打印日志e
    public static void e(String tag, String msg) {
        if (mIsShowLog) {
            Log.e(EmptyUtils.isEmpty(tag) ? "LogUtils" : tag, msg);
        }
    }

    //打印日志d
    public static void d(String tag, String msg) {
        if (mIsShowLog) {
            Log.d(EmptyUtils.isEmpty(tag) ? "LogUtils" : tag, msg);
        }
    }

    //打印日志json
    public static void json(String tag, String msg) {
        if (mIsShowLog) {
            String text = formatJson(msg);
            if (TextUtils.isEmpty(text)) {
                return;
            }

            // 打印 Json 数据最好换一行再打印会好看一点
            text = " \n" + text;

            int segmentSize = 3 * 1024;
            long length = text.length();
            if (length <= segmentSize) {
                // 长度小于等于限制直接打印
                e(tag, text);
                return;
            }

            // 循环分段打印日志
            while (text.length() > segmentSize) {
                String logContent = text.substring(0, segmentSize);
                text = text.replace(logContent, "");
                e(tag, logContent);
            }

            // 打印剩余日志
            e(tag, text);
        }
    }

    /**
     * 格式化 Json 字符串
     */
    @SuppressWarnings("AlibabaUndefineMagicConstant")
    private static String formatJson(String json) {
        if (json == null) {
            return "";
        }

        try {
            if (json.startsWith("{")) {
                return unescapeJson(new JSONObject(json).toString(4));
            } else if (json.startsWith("[")) {
                return unescapeJson(new JSONArray(json).toString(4));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * 去除 Json 中非必要的字符转义
     */
    @NonNull
    private static String unescapeJson(String json) {
        if (TextUtils.isEmpty(json)) {
            return "";
        }
        return json.replace("\\/", "/");
    }
}