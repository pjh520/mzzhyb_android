package com.frame.compiler.utils;

import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;

/**
 * ================================================
 * 项目名称：SuperWarehouse_Android
 * 包    名：com.tjl.super_warehouse.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/1/9 0009
 * 描    述：RecyclerView列表优化
 * ================================================
 */
public class RecyclerViewUtils {

    //加载图片 或者停止加载
    public static void resumeOrPausePic(RecyclerView recyclerView) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        //当屏幕停止滚动，加载图片
                        Glide.with(recyclerView.getContext()).resumeRequests();//恢复Glide加载图片
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        //当屏幕滚动且用户使用的触碰或手指还在屏幕上，停止加载图片
                        Glide.with(recyclerView.getContext()).pauseRequests();//禁止Glide加载图片
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        //由于用户的操作，屏幕产生惯性滑动，停止加载图片
                        Glide.with(recyclerView.getContext()).pauseRequests();//禁止Glide加载图片
                        break;
                }
            }
        });
    }
}
