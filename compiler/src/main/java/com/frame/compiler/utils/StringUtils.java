package com.frame.compiler.utils;

import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ================================================
 * 项目名称：dgonline-android
 * 包   名：com.aten.compiler.utils
 * 作   者：彭俊鸿
 * 邮   箱：1031028399@qq.com
 * 版   本：1.0
 * 创建日期：2018/12/25
 * 描   述：字符串操作工具类
 * ================================================
 */
public class StringUtils {

    /*
     * 字符串补位操作
     * indexStr 首位显示字符 如果没有 设置""
     * seatStr 占位字符
     * strNum 字符串的字数 =总的字符串长度-indexStr的长度
     * patchStr 原始字符串
     */
    public static String strPatchPos(String indexStr, String seatStr, String strNum, long patchStr) {
        return String.format(indexStr + "%" + seatStr + strNum + "d", patchStr);
    }

    //身份证前三后三 中间*
    public static String encryptionIDCard(String IDCard) {
        try {
            if (IDCard.length() == 15) {
                return IDCard.substring(0, 3) + "*********" + IDCard.substring(12);
            } else if (IDCard.length() == 18) {
                return IDCard.substring(0, 3) + "************" + IDCard.substring(15);
            } else {
                return IDCard;
            }
        }catch (Exception e){
            return IDCard;
        }
    }

    //手机号码前三后三 中间*
    public static String encryptionPhone(String phone) {
        try {
            if (!EmptyUtils.isEmpty(phone) && phone.length() >= 11) {
                return phone.substring(0, 3) + "****" + phone.substring(7);
            } else {
                return phone;
            }
        }catch (Exception e){
            return phone;
        }
    }

    //名字加*
    public static String encryptionName(String name) {
        StringBuilder stringBuilder = new StringBuilder();
        if (!EmptyUtils.isEmpty(name)){
            for (int i=0,size=name.length();i<size;i++){
                char c=name.charAt(i);
                if (i>=1){
                    stringBuilder.append("*");
                }else {
                    stringBuilder.append(c);
                }
            }
        }

        return stringBuilder.toString();
    }

    public static String generateFileSuffix(String fileName) {
        String suffix = fileName.substring(fileName.lastIndexOf("."), fileName.length());
        return suffix;
    }

    //----------------------------字符串拼接---------------------------------------------------------
    /*
     * 1、对于字符串连接操作较少的建议使用String
     * 2、对于字符串连接操作较频繁，并且是多线程操作，使用StringBuffer
     * 3、对于字符串连接操作比较频繁，但是是单线程操作的，建议使用StringBuilder。
     * StringBuffer和StringBuilder的区别就在于StringBuffer的操作使用synchronized关键字加了锁，是线程安全的。
     */
    //StringBuffer拼接字符串  ---线程安全
    public static String appendBuffer(Object... objects) {
        StringBuffer stringBuffer = new StringBuffer();
        if (objects != null) {
            for (Object object : objects) {
                stringBuffer.append(object);
            }
        }

        return stringBuffer.toString();
    }

    //StringBuilder拼接字符串  ---线程不安全
    public static String appendBuilder(Object... objects) {
        StringBuilder stringBuilder = new StringBuilder();
        if (objects != null) {
            for (Object object : objects) {
                stringBuilder.append(object);
            }
        }

        return stringBuilder.toString();
    }
    //----------------------------字符串拼接---------------------------------------------------------

    public static String replaceBlank(String src) {
        String dest = "";
        if (src!=null){
            Pattern pattern=Pattern.compile("\t|\r|\n|\\s");
            Matcher matcher=pattern.matcher(src);
            dest=matcher.replaceAll("");
        }
        return dest;
    }
}
