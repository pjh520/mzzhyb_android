package com.frame.compiler.utils.listScroll;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.frame.compiler.utils.listScroll
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-04-03
 * 描    述：
 * ================================================
 */
public class RVScrollUtils {
//    //缓慢滚动
//    public static void rvSmoothScrollToPosition(RecyclerView recyclerView,LinearLayoutManager layoutManager,int position) {
//        boolean smoothScrolling = true;
//        int firstPos= layoutManager.findFirstVisibleItemPosition();
//        int lastPos= layoutManager.findLastVisibleItemPosition();
//
//        if (position in(firstPos + 1) until lastPos){
//            val childAt:View ? = layoutManager.findViewByPosition(position)
//            var top = childAt ?.top ?:0
//            recyclerView.smoothScrollBy(0, top)
//        } else{
//
//            recyclerView.addOnScrollListener(object :RecyclerView.OnScrollListener() {
//
//                override fun onScrollStateChanged(recyclerView:RecyclerView, newState:Int){
//
//                    if (smoothScrolling || newState == RecyclerView.SCROLL_STATE_IDLE) {
//
//                        if (position in layoutManager.
//                        findFirstVisibleItemPosition() + 1..layoutManager.findLastVisibleItemPosition())
//                        {
//
//                            val childAt:View ? = layoutManager.findViewByPosition(position)
//                            val top = childAt ?.top ?:0
//                            recyclerView.scrollBy(0, top)
//
//                            recyclerView.removeOnScrollListener(this)
//                        }
//                        smoothScrolling = false
//                    }
//                }
//
//                override fun onScrolled(recyclerView:RecyclerView, dx:Int, dy:Int){
//                }
//            })
//
//            recyclerView.smoothScrollToPosition(position)
//        }
//
//    }
//
//
//    /**
//     * 直接跳转刷新Layout
//     */
//    fun rvScrollToPosition(rv:RecyclerView, layoutManager:LinearLayoutManager, position:Int) {
//
//        val firstPos = layoutManager.findFirstVisibleItemPosition()
//        val lastPos:Int = layoutManager.findLastVisibleItemPosition()
//
//        if (position <= firstPos) {
//            //当要置顶的项在当前显示的第一个项的前面时
//            rv.scrollToPosition(position)
//
//        } else if (position <= lastPos) {
//            //当要置顶的项已经在屏幕上显示时,通过LayoutManager
//            val childAt:View ? = layoutManager.findViewByPosition(position)
//            var top = childAt ?.top ?:0
//            rv.scrollBy(0, top)
//
//        } else {
//            //当要置顶的项在当前显示的最后一项之后
//            layoutManager.scrollToPositionWithOffset(position, 0)
//        }
//    }
}
