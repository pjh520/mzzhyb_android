package com.frame.compiler.utils.pic;

import android.content.Context;

import com.luck.picture.lib.engine.ExtendLoaderEngine;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.entity.LocalMediaFolder;
import com.luck.picture.lib.interfaces.OnQueryAlbumListener;
import com.luck.picture.lib.interfaces.OnQueryAllAlbumListener;
import com.luck.picture.lib.interfaces.OnQueryDataResultListener;
import com.luck.picture.lib.loader.SandboxFileLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * ================================================
 * 项目名称：AndroidFrame
 * 包    名：com.frame.compiler.utils.pic
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-04-22
 * 描    述：自定义数据加载器
 * ================================================
 */
public class MeExtendLoaderEngine implements ExtendLoaderEngine {

    @Override
    public void loadAllAlbumData(Context context,
                                 OnQueryAllAlbumListener<LocalMediaFolder> query) {
        LocalMediaFolder folder = SandboxFileLoader
                .loadInAppSandboxFolderFile(context, ImageLoaderUtils.getSandboxPath());
        List<LocalMediaFolder> folders = new ArrayList<>();
        folders.add(folder);
        query.onComplete(folders);
    }

    @Override
    public void loadOnlyInAppDirAllMediaData(Context context,
                                             OnQueryAlbumListener<LocalMediaFolder> query) {
        LocalMediaFolder folder = SandboxFileLoader
                .loadInAppSandboxFolderFile(context, ImageLoaderUtils.getSandboxPath());
        query.onComplete(folder);
    }

    @Override
    public void loadFirstPageMediaData(Context context, long bucketId, int page, int pageSize, OnQueryDataResultListener<LocalMedia> query) {
        LocalMediaFolder folder = SandboxFileLoader
                .loadInAppSandboxFolderFile(context, ImageLoaderUtils.getSandboxPath());
        query.onComplete(folder.getData(), false);
    }

    @Override
    public void loadMoreMediaData(Context context, long bucketId, int page, int limit, int pageSize, OnQueryDataResultListener<LocalMedia> query) {

    }
}