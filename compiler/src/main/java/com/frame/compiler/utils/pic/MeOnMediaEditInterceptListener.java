package com.frame.compiler.utils.pic;

import android.net.Uri;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.frame.compiler.R;
import com.frame.compiler.utils.RxTool;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.interfaces.OnMediaEditInterceptListener;
import com.luck.picture.lib.style.PictureSelectorStyle;
import com.luck.picture.lib.style.SelectMainStyle;
import com.luck.picture.lib.style.TitleBarStyle;
import com.luck.picture.lib.utils.DateUtils;
import com.luck.picture.lib.utils.StyleUtils;
import com.yalantis.ucrop.UCrop;

import java.io.File;

/**
 * ================================================
 * 项目名称：AndroidFrame
 * 包    名：com.frame.compiler.utils.pic
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-04-23
 * 描    述：自定义编辑
 * ================================================
 */
public class MeOnMediaEditInterceptListener implements OnMediaEditInterceptListener {
    private final UCrop.Options options;
    private PictureSelectorStyle selectorStyle;
    private int aspect_ratio_x = -1, aspect_ratio_y = -1;//-1 代表默认

    public MeOnMediaEditInterceptListener(PictureSelectorStyle selectorStyle){
        this.selectorStyle=selectorStyle;
        options=buildOptions();
    }

    @Override
    public void onStartMediaEdit(Fragment fragment, LocalMedia currentLocalMedia, int requestCode) {
        String currentEditPath = currentLocalMedia.getAvailablePath();
        Uri inputUri = PictureMimeType.isContent(currentEditPath) ? Uri.parse(currentEditPath) : Uri.fromFile(new File(currentEditPath));
        Uri destinationUri = Uri.fromFile(new File(ImageLoaderUtils.getSandboxPath(), DateUtils.getCreateFileName("CROP_") + ".jpeg"));
        UCrop uCrop = UCrop.of(inputUri, destinationUri);
        options.setHideBottomControls(false);
        uCrop.withOptions(options);
        uCrop.startEdit(fragment.getActivity(), fragment, requestCode);
    }

    /**
     * 配制UCrop，可根据需求自我扩展
     *
     * @return
     */
    private UCrop.Options buildOptions() {
        UCrop.Options options = new UCrop.Options();
        options.setHideBottomControls(false);//是否显示裁剪菜单栏
        options.setFreeStyleCropEnabled(false);//裁剪框or图片拖动
        options.setShowCropFrame(true);//是否显示裁剪边框
        options.setShowCropGrid(true);//是否显示裁剪框网格
        options.setCircleDimmedLayer(true);//圆形头像裁剪模式
        options.withAspectRatio(aspect_ratio_x, aspect_ratio_y);//设置裁剪比例
        options.setCropOutputPathDir(ImageLoaderUtils.getSandboxPath());
        options.isCropDragSmoothToCenter(false);
        options.isUseCustomLoaderBitmap(true);//自定义Loader Bitmap
        options.setSkipCropMimeType(getNotSupportCrop());
        options.isForbidCropGifWebp(true);
        options.isForbidSkipMultipleCrop(false);
        options.setMaxScaleMultiplier(100);
        if (selectorStyle != null && selectorStyle.getSelectMainStyle().getStatusBarColor() != 0) {
            SelectMainStyle mainStyle = selectorStyle.getSelectMainStyle();
            boolean isDarkStatusBarBlack = mainStyle.isDarkStatusBarBlack();
            int statusBarColor = mainStyle.getStatusBarColor();
            options.isDarkStatusBarBlack(isDarkStatusBarBlack);
            if (StyleUtils.checkStyleValidity(statusBarColor)) {
                options.setStatusBarColor(statusBarColor);
                options.setToolbarColor(statusBarColor);
            } else {
                options.setStatusBarColor(ContextCompat.getColor(RxTool.getContext(), R.color.ps_color_grey));
                options.setToolbarColor(ContextCompat.getColor(RxTool.getContext(), R.color.ps_color_grey));
            }
            TitleBarStyle titleBarStyle = selectorStyle.getTitleBarStyle();
            if (StyleUtils.checkStyleValidity(titleBarStyle.getTitleTextColor())) {
                options.setToolbarWidgetColor(titleBarStyle.getTitleTextColor());
            } else {
                options.setToolbarWidgetColor(ContextCompat.getColor(RxTool.getContext(), R.color.ps_color_white));
            }
        } else {
            options.setStatusBarColor(ContextCompat.getColor(RxTool.getContext(), R.color.ps_color_grey));
            options.setToolbarColor(ContextCompat.getColor(RxTool.getContext(), R.color.ps_color_grey));
            options.setToolbarWidgetColor(ContextCompat.getColor(RxTool.getContext(), R.color.ps_color_white));
        }
        return options;
    }

    private String[] getNotSupportCrop() {
        return new String[]{PictureMimeType.ofGIF(), PictureMimeType.ofWEBP()};
    }
}