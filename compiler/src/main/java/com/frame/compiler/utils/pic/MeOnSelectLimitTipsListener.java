package com.frame.compiler.utils.pic;

import android.content.Context;

import androidx.annotation.Nullable;

import com.luck.picture.lib.config.PictureSelectionConfig;
import com.luck.picture.lib.config.SelectLimitType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.interfaces.OnSelectLimitTipsListener;
import com.luck.picture.lib.utils.ToastUtils;

/**
 * ================================================
 * 项目名称：AndroidFrame
 * 包    名：com.frame.compiler.utils.pic
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2022-04-23
 * 描    述：拦截自定义提示
 * ================================================
 */
public class MeOnSelectLimitTipsListener implements OnSelectLimitTipsListener {
    @Override
    public boolean onSelectLimitTips(Context context, @Nullable LocalMedia media, PictureSelectionConfig config, int limitType) {
        if (limitType == SelectLimitType.SELECT_NOT_SUPPORT_SELECT_LIMIT) {
            ToastUtils.showToast(context, "暂不支持的选择类型");
            return true;
        }
        return false;
    }
}