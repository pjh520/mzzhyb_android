package com.frame.compiler.utils.storage;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.webkit.MimeTypeMap;

import com.frame.compiler.base.BaseGlobal;
import com.frame.compiler.utils.CloseUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.LogUtils;
import com.frame.compiler.utils.RxTool;

import java.io.File;

/**
 * ================================================
 * 项目名称：My Application
 * 包    名：com.example.myapplication.utils.storage
 * 版    本：1.0
 * 创建日期：2021/10/20 0020
 * 描    述：androidQ,R分区存储工具类
 * ================================================
 */
public class FileStorageUtils {
    //未启用分区存储的时候，媒体文件存储的位置
    public static final String EXTERNAL_FILE_DIRECTORY = "zhyb";
    //卷名
    public static final String VOLUME_EXTERNAL = "external";
    //外部存储共享存储空间非媒体文件数据库的uri，其保存在/Download目录下 文件 URI
    public static final Uri DOCUMENT_EXTERNAL_URI = MediaStore.Files.getContentUri(VOLUME_EXTERNAL);

    /**
     * 根据媒体文件类型推断其合理存储的目录，比如（/storage/emulated/0/Pictures/）
     *
     * @param mimeType 媒体文件类型，比如(text/plain)
     * @return 媒体文件合理存储的目录
     */
    public static File guessExternalFileDirectory(String mimeType) {
        String relativePath = guessExternalFileRelativeDirectory(mimeType);
        return Environment.getExternalStoragePublicDirectory(relativePath);
    }

    /**
     * 根据媒体文件类型推断其合理存储的目录
     *
     * @param mimeType 媒体文件类型，比如(text/plain)
     * @return 媒体文件合理存储的目录
     */
    public static String guessExternalFileRelativeDirectory(String mimeType) {
        String dir = Environment.DIRECTORY_DOWNLOADS;
        if (EmptyUtils.isEmpty(mimeType)||!isMediaMimeType(mimeType)) {
            dir= Environment.DIRECTORY_DOWNLOADS;
        }else if (mimeType.startsWith("image/")) {
            dir = Environment.DIRECTORY_PICTURES;
        } else if (mimeType.startsWith("video/")) {
            dir = Environment.DIRECTORY_MOVIES;
        } else if (mimeType.startsWith("audio/")) {
            dir = Environment.DIRECTORY_PODCASTS;
        }
        return dir;
    }

    /**
     * 根据媒体文件类型推断其合理存储的uri
     *
     * @param mimeType 媒体文件类型，比如(image/png)
     * @return 媒体文件合理存储的uri
     */
    public static Uri guessExternalMediaUri(String mimeType) {
        Uri uri =null;
        //判断是否存在外部存储卡
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            if (EmptyUtils.isEmpty(mimeType)||!isMediaMimeType(mimeType)) {
                //在搭载 Android 10（API 级别 29）及更高版本的设备上，这些文件存储在 MediaStore.Downloads 表格中。
                //此表格在 Android 9（API 级别 28）及更低版本中不可用
                uri = DOCUMENT_EXTERNAL_URI;
            }else if (mimeType.startsWith("image/")) {//图片
                uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            } else if (mimeType.startsWith("video/")) {//视频
                uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            } else if (mimeType.startsWith("audio/")) {//音频
                uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            }
        } else {
            if (EmptyUtils.isEmpty(mimeType)||!isMediaMimeType(mimeType)) {
                uri = DOCUMENT_EXTERNAL_URI;
            }else if (mimeType.startsWith("image/")) {//图片
                uri = MediaStore.Images.Media.INTERNAL_CONTENT_URI;
            } else if (mimeType.startsWith("video/")) {//视频
                uri = MediaStore.Video.Media.INTERNAL_CONTENT_URI;
            } else if (mimeType.startsWith("audio/")) {//音频
                uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
            }
        }

        return uri;
    }

    /**
     * 根据文件的名称(hello.txt)推断文件的类型(application/pdf)
     *
     * @param fileName (hello.txt)
     * @return (application / pdf)
     */
    public static String guessExternalFileMimeType(String fileName) {
        if (!fileName.contains(".")) {
            throw new IllegalArgumentException("parameter " + fileName + " is invalidate, Must have And Only one '.'") ;
        }

        //        String[] ss = fileName.split("[.]");
//        if (ss!=null){
//            for (String s : ss) {
//                Log.e("HomeFragment","s==="+s);
//            }
//        }

        int indexOfDot = fileName.lastIndexOf(".");
        String extension = fileName.substring(indexOfDot + 1);
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
    }

    /**
     * 根据文件的名称(hello.txt)推断文件的存储的内部路径
     *
     * @param type 存储文件类型 1：图片 2：音频 3.视频 4.文件 5.崩溃文件
     */
    public static String getInternalFilePath(String type) {
        String internalFilePath=BaseGlobal.getFileDir();
        switch (type){
            case "1":
                internalFilePath= BaseGlobal.getImageDir();
                break;
            case "2":
                internalFilePath= BaseGlobal.getAudioDir();
                break;
            case "3":
                internalFilePath= BaseGlobal.getVideoDir();
                break;
            case "4":
                internalFilePath= BaseGlobal.getFileDir();
                break;
            case "5":
                internalFilePath= BaseGlobal.getCrashDir();
                break;
        }

        return internalFilePath;
    }

    /**
     * 判断一个文件是否不是媒体文件类型
     *
     * @param mimeType
     * @return
     */
    public static Boolean isMediaMimeType(String mimeType) {
        if (mimeType != null && (mimeType.startsWith("image/") || mimeType.startsWith("video/") || mimeType.startsWith("audio/"))) {
            return true;
        }
        return false;
    }

    /**
     * 根据uri 查询它的mimeType
     *
     * @param context
     * @param uri     比如：content://media/external/file/10086
     * @return 比如:application/pdf
     */
    public static String queryMimeTypeFromUri(Context context, Uri uri) {
        long id = ContentUris.parseId(uri);
        String[] projection = {MediaStore.MediaColumns.MIME_TYPE};
        String selection = MediaStore.MediaColumns._ID + "= ?";
        String[] selectionArgs = {String.valueOf(id)};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null) {
                int columnIndex = cursor.getColumnIndex(MediaStore.MediaColumns.MIME_TYPE);
                if (cursor.moveToFirst()) {
                    return cursor.getString(columnIndex);
                }
            }
        } catch (Exception e) {
            LogUtils.e(e.getMessage());
        } finally {
            CloseUtils.closeIOQuietly(cursor);
        }
        return null;
    }

    /**
     * 使用SAF 访问文件时，构建一个默认打开的目录，我们默认打开在download/目录
     * 如果imooc目录存在，那么默认打开的就是download/imooc目录
     * <p>
     * URI 格式：content://com.android.externalstorage.documents/document/primary: + 相对路径的 urlencode
     * 例如：
     * 路径 /sdcard=content://com.android.externalstorage.documents/document/primary:
     * 路径 /sdcard/Download=content://com.android.externalstorage.documents/document/primary:Download
     * 路径 /sdcard/Download/Image=content://com.android.externalstorage.documents/document/primary:Download%2fImage
     *
     * @return
     */
    public static Uri getExtraInitUri() {
        // 这个值没有定义成常量，只能谢在这里
        String baseUri = "content://com.android.externalstorage.documents/document/primary:";
        try {
            File file =new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), EXTERNAL_FILE_DIRECTORY);
            if (!file.exists()) {
                file.mkdirs();
            }
            return Uri.parse(baseUri + Uri.encode(Environment.DIRECTORY_DOWNLOADS + "/" + EXTERNAL_FILE_DIRECTORY));
        } catch (Exception ex){
            // 如果文件创建失败了，则默认打开download目录
        }

        return Uri.parse(baseUri + Uri.encode(Environment.DIRECTORY_DOWNLOADS));
    }

    /**
     * 获取自己应用的包名
     */
    public static String getPackageName() {
        return RxTool.getContext().getPackageName();
    }

    /**
     * Android 10规定了APP有两种外部存储空间视图模式：Legacy View、Filtered View。
     *
     * Filtered View:App可以直接访问App-specific目录，但不能直接访问App-specific外的文件。
     *               访问公共目录或其他APP的App-specific目录，只能通过MediaStore、SAF、或者其他APP提供的ContentProvider、FileProvider等访问。
     * Legacy View:兼容模式。与Android 10以前一样，申请权限后App可访问外部存储，拥有完整的访问权限。
     *采用分区存储只能读写外部共享目录。
     * 采用非分区存储是可以读写外部存储任何目录的。
     * 注意：android10在分区存储模式下不能用文件路径读写外部共享文件，但在android11在分区存储下又可以用文件路径读写外部共享文件，所以如果您的项目存在通过文件路径来读写外部共享文件，要在配置文件中设置android:requestLegacyExternalStorage="true"来适配android10
     *
     * 1、运行在android 11上
     * 当targetSdkVersion<=28：
     * 结果 Environment.isExternalStorageLegacy() 为 true，采用的是非分区存储方法。
     *
     * 当targetSdkVersion=29：
     * 不设置android:requestLegacyExternalStorage="true"
     * a 应用从非分区存储更新，结果Environment.isExternalStorageLegacy()为 true，采用的是非分区存储方法。
     * b 正常卸载安装 结果Environment.isExternalStorageLegacy()为 flase ，采用的是分区存储方法。
     *
     * 设置android:requestLegacyExternalStorage="true"
     * 结果Environment.isExternalStorageLegacy()为 true，采用的是非分区存储方法。
     *
     * 当targetSdkVersion=30：
     * 不设置android:preserveLegacyExternalStorage="true"
     * 结果Environment.isExternalStorageLegacy()为 false，采用的是分区存储方法。
     *
     * 设置android:preserveLegacyExternalStorage="true"
     * a、应用从非分区存储更新，结果Environment.isExternalStorageLegacy()为 true，采用的是非分区存储方法。
     * b、应用正常卸载安装或者从分区存储更新，结果Environment.isExternalStorageLegacy()为 flase，采用的是分区存储方法。
     *
     * 2、运行在android 10上
     * 当targetSdkVersion<=28：
     * 结果Environment.isExternalStorageLegacy()为 true，采用的是非分区存储方法。
     *
     * 当targetSdkVersion>=29：
     * 不设置android:requestLegacyExternalStorage="true"
     * a 应用从非分区存储更新，结果Environment.isExternalStorageLegacy()为 true，采用的是非分区存储方法。
     * b 正常卸载安装 结果Environment.isExternalStorageLegacy()为 flase ，采用的是分区存储方法。
     *
     * 设置android:requestLegacyExternalStorage="true"
     * 结果Environment.isExternalStorageLegacy()为 true，采用的是非分区存储方法。
     *
     * 3、运行在android 9及以下
     * 采用的是非分区存储方法
     * @return true: 分区存储未启用
     */
    public static Boolean isExternalStorageLegacy(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            // return true:未开启 false:开启分区存储
            return Environment.isExternalStorageLegacy();
        }

        return true;
    }
}
