package com.frame.compiler.utils.storage;

import android.net.Uri;

/**
 * ================================================
 * 项目名称：My Application
 * 包    名：com.example.myapplication.utils.storage
 * 版    本：1.0
 * 创建日期：2021/10/21 0021
 * 描    述：
 * ================================================
 */
public class MediaInfo {
    private Uri uri;
    private String displayName;
    private String mineType;
    private int width;
    private int height;
    private String path;

    public MediaInfo(Uri uri, String displayName, String mineType, int width, int height, String path) {
        this.uri = uri;
        this.displayName = displayName;
        this.mineType = mineType;
        this.width = width;
        this.height = height;
        this.path = path;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getMineType() {
        return mineType;
    }

    public void setMineType(String mineType) {
        this.mineType = mineType;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "MediaInfo{" +
                "uri=" + uri +
                ", displayName='" + displayName + '\'' +
                ", mineType='" + mineType + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", path='" + path + '\'' +
                '}';
    }
}
