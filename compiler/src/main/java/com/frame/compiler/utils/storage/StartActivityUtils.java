package com.frame.compiler.utils.storage;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Parcelable;

import androidx.activity.result.ActivityResultCallback;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.hjq.permissions.PermissionFragment;

import static com.frame.compiler.utils.storage.ScopStorageUtils.TYPE_CREATE_MEDIA_REQUEST;

/**
 * ================================================
 * 项目名称：My Application
 * 包    名：com.frame.compiler.utils.storage
 * 版    本：1.0
 * 创建日期：2021/10/28 0028
 * 描    述：
 * ================================================
 */
public class StartActivityUtils {
    public static final String EXTRA = "extra";
    public static final String TYPE = "type";

    public static void startIntentSenderForResult(FragmentActivity activity, IntentSender intentSender, int requestCode, final SimpleCallback callback) {
        Bundle bundle = new Bundle();
        bundle.putInt(TYPE, requestCode);
        bundle.putParcelable(EXTRA, intentSender);
        PermissionFragment.start(activity, bundle, new ActivityResultCallback() {
            @Override
            public void onActivityResult(int requestCode, int resultCode, Intent data) {
                if (resultCode == Activity.RESULT_OK) {
                    callback.onResult(true);
                } else {
                    callback.onResult(false);
                }
            }
        });
    }

    public static class PermissionFragment extends Fragment {
        private static final String TAG = "PermissionFragment";

        private static ActivityResultCallback mActivityCallback;

        public static void start(FragmentActivity activity, Bundle arguments, ActivityResultCallback activityCallback) {
            mActivityCallback = activityCallback;
            FragmentManager fm = activity.getSupportFragmentManager();
            if (!fm.isStateSaved()) {
                Fragment fragment = fm.findFragmentByTag(TAG);
                if (fragment == null) {
                    fragment = new PermissionFragment();
                    fm.beginTransaction().add(android.R.id.content, fragment, TAG)
                            .hide(fragment)
                            .commitNowAllowingStateLoss();
                }

                IntentSender intentSender = arguments.getParcelable(EXTRA);
                try {
                    activity.startIntentSenderForResult(intentSender, arguments.getInt(TYPE), null, 0, 0,
                            0, Bundle.EMPTY);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (mActivityCallback!=null){
                mActivityCallback.onActivityResult(requestCode, resultCode, data);
                mActivityCallback = null;
            }
        }
    }

    public interface SimpleCallback {
        void onResult(Boolean result);
    }

    interface ActivityResultCallback {
        void onActivityResult(int requestCode,int resultCode,Intent data);
    }
}
