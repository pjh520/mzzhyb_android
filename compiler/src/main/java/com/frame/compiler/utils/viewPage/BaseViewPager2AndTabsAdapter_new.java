package com.frame.compiler.utils.viewPage;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

/**
 * ================================================
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2018/5/23
 * 描    述：ViewPager2适配器基类(防止内存泄漏)
 * ================================================
 */
public abstract class BaseViewPager2AndTabsAdapter_new extends FragmentStateAdapter {
    private int mData;
    public BaseViewPager2AndTabsAdapter_new(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    public BaseViewPager2AndTabsAdapter_new(@NonNull Fragment fragment) {
        super(fragment);
    }

    public BaseViewPager2AndTabsAdapter_new(@NonNull FragmentManager fragmentManager,
                                            @NonNull Lifecycle lifecycle) {
        super(fragmentManager,lifecycle);
    }

    public void setData(int data) {
        this.mData = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public abstract Fragment createFragment(int position);

    @Override
    public int getItemCount() {
        return mData;
    }
}
