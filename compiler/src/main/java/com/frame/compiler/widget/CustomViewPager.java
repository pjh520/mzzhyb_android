package com.frame.compiler.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

/**
 * Created by Administrator on 2016/08/30 030.
 * 复写viewpager 使其不能根据手势滑动
 */
public class CustomViewPager extends ViewPager {
    private boolean isCanScroll;//是否可以滑动
    private boolean isCanLeftScroll;//是否可以左滑
    private boolean isCanRightScroll;//是否可以右滑
    private float beforeX;//上一次x坐标
    private float beforeY;

    public CustomViewPager(Context context, AttributeSet attrs) {
        this(context, attrs,false);
    }

    public CustomViewPager(@NonNull Context context, @Nullable AttributeSet attrs, boolean enabled) {
        super(context, attrs);
        this.isCanScroll = enabled;
    }

    // 触摸没有反应就可以了
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.isCanLeftScroll||this.isCanRightScroll||this.isCanScroll) {
            return super.onTouchEvent(event);
        }

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.isCanLeftScroll||this.isCanRightScroll||this.isCanScroll) {
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (isCanRightScroll) {//禁止左滑 可以右滑
            switch (ev.getAction()) {
                case MotionEvent.ACTION_DOWN://按下如果‘仅’作为‘上次坐标’，不妥，因为可能存在左滑，motionValue大于0的情况（来回滑，只要停止坐标在按下坐标的右边，左滑仍然能滑过去）
                    beforeX = ev.getX();
                    beforeY=ev.getY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    float motionValue = ev.getX() - beforeX;
                    float motionValueY =ev.getY() - beforeY;

                    if (Math.abs(motionValueY)<Math.abs(motionValue)&&motionValue <0) {//禁止左滑
                        return true;
                    }
                    beforeX = ev.getX();//手指移动时，再把当前的坐标作为下一次的‘上次坐标’，解决上述问题
                    break;
                default:
                    break;
            }
        }else if (isCanLeftScroll){//禁止右滑 可以左滑
            switch (ev.getAction()) {
                case MotionEvent.ACTION_DOWN://按下如果‘仅’作为‘上次坐标’，不妥，因为可能存在左滑，motionValue大于0的情况（来回滑，只要停止坐标在按下坐标的右边，左滑仍然能滑过去）
                    beforeX = ev.getX();
                    beforeY=ev.getY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    float motionValue = ev.getX() - beforeX;
                    float motionValueY =ev.getY() - beforeY;
                    if (Math.abs(motionValueY)<Math.abs(motionValue)&&motionValue >0) {//禁止右滑
                        return true;
                    }
                    beforeX = ev.getX();//手指移动时，再把当前的坐标作为下一次的‘上次坐标’，解决上述问题

                    break;
                default:
                    break;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

    //设置是否可以滑动
    public void setScrollble(boolean isCanScroll) {
        this.isCanScroll = isCanScroll;
    }

    //设置是否可以左滑
    public void setLeftScrollble(boolean isCanLeftScroll) {
        this.isCanLeftScroll = isCanLeftScroll;
    }

    //设置是否可以右滑
    public void setRightScrollble(boolean isCanRightScroll) {
        this.isCanRightScroll = isCanRightScroll;
    }
}