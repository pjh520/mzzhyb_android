package com.frame.compiler.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.frame.compiler.R;

/**
 * ================================================
 * 项目名称：My Application
 * 包    名：com.frame.compiler.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/6/19 0019
 * 描    述：渐变效果RecyclerView
 * 需要添加下列数据
 * app:fading_direction="bottom"
 * android:requiresFadingEdge="vertical"
 * android:fadingEdgeLength="@dimen/dp_60"
 * ================================================
 */
public class FadingEdgeRecyclerView extends RecyclerView {
    private int fadingDirection;

    public FadingEdgeRecyclerView(@NonNull Context context) {
        this(context, null);
    }

    public FadingEdgeRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FadingEdgeRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(context,attrs, defStyleAttr);
    }

    private void initAttrs(@NonNull Context context,@Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.FadingEdgeRecyclerView, defStyleAttr, 0);
        //背景色
        fadingDirection = array.getInt(R.styleable.FadingEdgeRecyclerView_fading_direction, 3);
        //回收
        array.recycle();
    }

    @Override
    protected float getTopFadingEdgeStrength() {
        return fadingDirection==2?0f:super.getTopFadingEdgeStrength();
    }

    @Override
    protected float getBottomFadingEdgeStrength() {
        return fadingDirection==1?0f:super.getBottomFadingEdgeStrength();
    }
}
