package com.frame.compiler.widget.banner.manager;

import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.frame.compiler.R;
import com.frame.compiler.widget.banner.listener.BannerModelCallBack;
import com.frame.compiler.widget.banner.listener.ImageLoaderManager;
import com.frame.compiler.widget.glide.GlideApp;
import com.frame.compiler.widget.glide.GlideUtils;

public class GlideAppSimpleImageManager<T extends BannerModelCallBack> implements ImageLoaderManager<T> {
    public GlideAppSimpleImageManager() {}

    @NonNull
    @Override
    public ImageView display(@NonNull ViewGroup container, T model) {
        ImageView imageView = new ImageView(container.getContext());
        GlideUtils.loadImg(model.getBannerUrl(),imageView);
        return imageView;
    }
}
