package com.frame.compiler.widget.banner.manager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.frame.compiler.R;
import com.frame.compiler.widget.banner.listener.BannerModelCallBack;
import com.frame.compiler.widget.banner.listener.ImageLoaderManager;
import com.frame.compiler.widget.glide.GlideUtils;
import com.frame.compiler.widget.transformation.RoundedCornersTransformation;

public class GlideAppSimpleImageManager_round<T extends BannerModelCallBack> implements ImageLoaderManager<T> {
    public GlideAppSimpleImageManager_round() {}

    @NonNull
    @Override
    public View display(@NonNull ViewGroup container, T model) {
        View view= LayoutInflater.from(container.getContext()).inflate(R.layout.layout_banner_round_item,container,false);
        ImageView imageView = view.findViewById(R.id.iv);
        GlideUtils.loadImg(model.getBannerUrl(),imageView,R.drawable.ic_placeholder_bg,
                new RoundedCornersTransformation((int) container.getResources().getDimension(R.dimen.dp_16),0));
        return imageView;
    }
}
