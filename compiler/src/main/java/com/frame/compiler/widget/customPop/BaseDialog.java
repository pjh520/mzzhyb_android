package com.frame.compiler.widget.customPop;

import android.content.Context;
import android.view.animation.Animation;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AlphaConfig;
import razerdp.util.animation.AnimationHelper;

/**
 * ================================================
 * 项目名称：My Application
 * 包    名：com.frame.compiler.widget.customPop
 * 版    本：1.0
 * 创建日期：2021/9/17 0017
 * 描    述：基础弹框
 * ================================================
 */
public class BaseDialog extends BasePopupWindow {
    public BaseDialog(Context context) {
        super(context);
//        setContentView(contentView);
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withAlpha(AlphaConfig.IN)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withAlpha(AlphaConfig.OUT)
                .toDismiss();
    }
}
