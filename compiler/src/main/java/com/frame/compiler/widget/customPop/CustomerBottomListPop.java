package com.frame.compiler.widget.customPop;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.frame.compiler.R;
import com.frame.compiler.utils.ClickUtils;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.MaxHeightRecyclerView;
import com.frame.compiler.widget.customPop.adapter.CustomBottomChooseAdapter;
import com.frame.compiler.widget.customPop.model.BottomChooseBean;
import com.frame.compiler.widget.toast.ToastUtil;

import java.util.List;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/9
 * 描    述：底部弹框-带列表
 * ================================================
 */
public class CustomerBottomListPop<T> extends BasePopupWindow implements BaseQuickAdapter.OnItemChildClickListener {
    private String tag,title;
    private List<T> datas;
    private IBottomChooseListener iOptionListener;
    private CustomBottomChooseAdapter adapter;
    private TextView tvDialogTitle;
    private View titleLine;
    private String chooseText;

    public CustomerBottomListPop(Context context,IBottomChooseListener iOptionListener) {
        super(context);
        this.iOptionListener=iOptionListener;
        setContentView(R.layout.layout_bottom_choose_list_pop);
    }

    public void setDatas(String tag, String title, String chooseText,List<T> datas) {
        this.tag=tag;
        this.title=title;
        this.datas = datas;
        this.chooseText = chooseText;
        if (adapter!=null&&tvDialogTitle!=null){
            if (EmptyUtils.isEmpty(title)){
                tvDialogTitle.setVisibility(View.GONE);
                titleLine.setVisibility(View.GONE);
            }else {
                tvDialogTitle.setVisibility(View.VISIBLE);
                titleLine.setVisibility(View.VISIBLE);
                tvDialogTitle.setText(title);
            }
            adapter.setChooseText(chooseText);
            adapter.setNewData(datas);
        }else {
            ToastUtil.show("页面初始化未完成，请重新点击选择");
        }
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);

        tvDialogTitle=contentView.findViewById(R.id.tv_dialog_title);
        titleLine=contentView.findViewById(R.id.title_line);
        MaxHeightRecyclerView rlBottomChoose=contentView.findViewById(R.id.rl_bottom_choose);
        TextView stvDialogCancle=contentView.findViewById(R.id.tv_dialog_cancle);
        //设置标题
        if (EmptyUtils.isEmpty(title)){
            tvDialogTitle.setVisibility(View.GONE);
            titleLine.setVisibility(View.GONE);
        }else {
            tvDialogTitle.setVisibility(View.VISIBLE);
            titleLine.setVisibility(View.VISIBLE);
            tvDialogTitle.setText(title);
        }
        //初始化列表数据
        rlBottomChoose.setHasFixedSize(true);
        rlBottomChoose.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        adapter = new CustomBottomChooseAdapter(iOptionListener);
        rlBottomChoose.setAdapter(adapter);
        adapter.setNewData(datas);
        adapter.setOnItemChildClickListener(this);
        //设置按钮点击事件
        stvDialogCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        Object itemData = ((CustomBottomChooseAdapter) adapter).getItem(position);
        if (R.id.tv_choose==view.getId()){
            if (ClickUtils.isFastClick(ClickUtils.MIN_CLICK_DELAY_TIME_500)) {
                dismiss();
                String text="";
                if (itemData instanceof String){
                    text= (String) itemData;
                }else if (itemData instanceof BottomChooseBean){
                    text=((BottomChooseBean)itemData).getText();
                }
                if (EmptyUtils.isEmpty(chooseText)||!chooseText.equals(text)){
                    iOptionListener.onItemClick(tag, itemData);
                }
            }
        }
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.FROM_BOTTOM)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.TO_BOTTOM)
                .toDismiss();
    }

    public interface IBottomChooseListener<T>{
        void onItemClick(String tag,T str);
    }
}

