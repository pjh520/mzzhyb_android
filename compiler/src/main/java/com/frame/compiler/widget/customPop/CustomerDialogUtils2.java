package com.frame.compiler.widget.customPop;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.frame.compiler.R;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.utils.ScreenUtils;
import com.hjq.shape.view.ShapeTextView;

/**
 * ================================================
 * 项目名称：My Application
 * 包    名：com.frame.compiler.widget.customPop
 * 版    本：1.0
 * 创建日期：2021/9/17 0017
 * 描    述：dialog 工具类（点击外部不能取消）
 * ================================================
 */
public class CustomerDialogUtils2 {
    private static int btn01DefaultColor=R.color.txt_color_666;
    private static int btn02DefaultColor=R.color.color_4970e0;
    private static int mask_color=R.color.color_black20;
    public BaseDialog baseDialog;
    private View view;

    public void showDialog(Context context,String title,String content,String btn01,String btn02,final IDialogListener iDialogListener){
        showDialog(context,title,content,2,btn01,btn02,iDialogListener);
    }

    public void showDialog(Context context,String title,String content,int btnNum,String btn01,String btn02,final IDialogListener iDialogListener){
        showDialog(context,title,content,btnNum,btn01,btn02,btn01DefaultColor,btn02DefaultColor,iDialogListener);
    }

    public void showDialog(Context context, String title, String content, int btnNum, String btn01, String btn02,
                                  int btn01Color, int btn02Color, final IDialogListener iDialogListener){
        if (baseDialog==null){
            baseDialog=new BaseDialog(context);
            baseDialog.setOutSideDismiss(false);
            baseDialog.setBackgroundColor(context.getResources().getColor(mask_color));
        }
        if (view==null){
            view=baseDialog.createPopupById(R.layout.layout_customer_dialog02);
            baseDialog.setContentView(view);
        }
        //初始化控件
        TextView tvTitle=view.findViewById(R.id.tv_title);
        TextView tvContent=view.findViewById(R.id.tv_content);
        LinearLayout llBtn=view.findViewById(R.id.ll_btn);
        ShapeTextView tvBtn01=view.findViewById(R.id.tv_btn01);
        ShapeTextView tvBtn02=view.findViewById(R.id.tv_btn02);

        //设置标题
        tvTitle.setText(EmptyUtils.isEmpty(title)?"提示":title);
        //设置内容
        tvContent.setText(EmptyUtils.strEmpty(content));

        //设置按钮的显示
        if (btnNum==1){
            llBtn.setVisibility(View.VISIBLE);

            tvBtn01.setVisibility(View.VISIBLE);
            tvBtn02.setVisibility(View.GONE);

            tvBtn01.setText(EmptyUtils.strEmpty(btn01));
            tvBtn01.setTextColor(context.getResources().getColor(btn01Color));
        }else if (btnNum==2){
            llBtn.setVisibility(View.VISIBLE);

            tvBtn01.setVisibility(View.VISIBLE);
            tvBtn02.setVisibility(View.VISIBLE);

            tvBtn01.setText(EmptyUtils.strEmpty(btn01));
            tvBtn02.setText(EmptyUtils.strEmpty(btn02));
            tvBtn01.setTextColor(context.getResources().getColor(btn01Color));
            tvBtn02.setTextColor(context.getResources().getColor(btn02Color));
        }else {
            llBtn.setVisibility(View.GONE);
        }

        tvBtn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                baseDialog.dismiss();
                iDialogListener.onBtn01Click();
            }
        });

        tvBtn02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                baseDialog.dismiss();
                iDialogListener.onBtn02Click();
            }
        });
        //显示弹框
        baseDialog.showPopupWindow();
    }

    public interface IDialogListener{
        void onBtn01Click();
        void onBtn02Click();
    }

    //清空数据
    public void onclean(){
        if (view!=null){
            view=null;
        }
    }
}
