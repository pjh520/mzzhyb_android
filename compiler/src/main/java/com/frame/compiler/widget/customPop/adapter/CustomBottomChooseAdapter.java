package com.frame.compiler.widget.customPop.adapter;

import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.frame.compiler.R;
import com.frame.compiler.utils.EmptyUtils;
import com.frame.compiler.widget.customPop.CustomerBottomListPop;
import com.frame.compiler.widget.customPop.model.BottomChooseBean;

/**
 * project:YunPiao-master
 * package:com.aiten.yunticketing.ui.TrainTickets.adapter
 * Created by 彭俊鸿 on 2018/1/4.
 * e-mail : 1031028399@qq.com
 */

public class CustomBottomChooseAdapter<T> extends BaseQuickAdapter<T, BaseViewHolder> {
    private String chooseText;//选中的position的文字
    private CustomerBottomListPop.IBottomChooseListener mOnBottomChooseItemListener;

    public CustomBottomChooseAdapter(CustomerBottomListPop.IBottomChooseListener iBottomChooseListener) {
        super(R.layout.layout_bottom_choose_item2, null);
        this.mOnBottomChooseItemListener = iBottomChooseListener;
    }

    //设置点击事件时的优化写法 防止在onBindViewHolder中多次初始化onclick
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder = super.onCreateViewHolder(parent, viewType);
        baseViewHolder.addOnClickListener(R.id.tv_choose);
        return baseViewHolder;
    }

    public void setChooseText(String chooseText) {
        this.chooseText = chooseText;
    }

    @Override
    protected void convert(BaseViewHolder helper, final T item) {
        TextView tvChoose=(TextView)helper.getView(R.id.tv_choose);
        String text="";
        if (item instanceof String){
            text= (String) item;
        }else if (item instanceof BottomChooseBean){
            text=((BottomChooseBean)item).getText();
        }
        tvChoose.setText(EmptyUtils.strEmpty(text));
        if (!EmptyUtils.isEmpty(chooseText)&&chooseText.equals(text)){
            tvChoose.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
        }else {
            tvChoose.setTextColor(mContext.getResources().getColor(R.color.txt_color_1e1e1e));
        }
    }
}
