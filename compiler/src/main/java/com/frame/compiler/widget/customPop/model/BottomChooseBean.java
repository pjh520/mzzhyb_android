package com.frame.compiler.widget.customPop.model;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.widget.bottomPop
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2021/12/9
 * 描    述：
 * ================================================
 */
public interface BottomChooseBean {
    String getText();
}
