package com.frame.compiler.widget.customWebview;

/**
 * @author Rex on 2019/6/20.
 */
public class CommonJs {

    /**
     * 为全局图片加上点击事件 回调 imageOnclick.openImage imageOnclick.openImage需要客户端对应实现
     * 时机为window.onload
     */
    public final static String IMG_CLICK_JS = "<script type='text/javascript'>window.onload = function(){" +
            "var $img = document.getElementsByTagName('img');" +
            "for(var p in  $img){" +
            "    if (typeof $img[p] === 'object') {" +
            "        $img[p].style.width = '100%';" +
            "        $img[p].style.height ='auto';" +
            "        $img[p].onclick = function(e){" +
            "            ImgClick(e.srcElement.src);" +
            "        };" +
            "    }" +
            "}" +
            "};" +
            "function ImgClick(src) {" +
            "    var message = {" +
            "        'imgUrl' : src," +
            "    };" +
            "   window.imageOnclick.openImage(src);" +
            "};" +
            "</script>";

    /**
     * 为全局图片加上点击事件 回调 imageOnclick.openImage imageOnclick.openImage需要客户端对应实现
     * 时机为window.onload
     */

    public final static String VIDEO_CLICK_JS = "<script type='text/javascript'>window.onload = function(){" +
            "var $videos = document.getElementsByTagName('video');" +
            "for(var pos in  $videos){" +
            "    if (typeof $videos[pos] === 'object') {" +
            "        $videos[pos].style.width = '100%';" +
            "        $videos[pos].style.height ='50%';" +
            "        $videos[pos].controlslist ='nodownload';" +
            "        $videos[pos].poster=$videos[pos].src+'?x-oss-process=video/snapshot,t_1,f_jpg,m_fast';"+
            "    }" +
            "}" +
            "};" +
            "</script>";

    public final static String headTop = "<html><head>" +
            "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\"> " +
            "<style>img{max-width: 100%; width:auto; height:auto;display:block;overflow:hidden;margin-bottom: 10px;}video{margin-bottom: 10px;}</style>" +
            "</head><body>";

    public final static String headTop2 = "<html><head>" +
            "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\"> " +
            "<style>img{max-width: 100%; width:auto; height:auto;}</style>" +
            "</head><body>";

    public final static String headBottom = "</body></html>";
}
