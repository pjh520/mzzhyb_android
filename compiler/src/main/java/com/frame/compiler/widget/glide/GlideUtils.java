package com.frame.compiler.widget.glide;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.signature.ObjectKey;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.ImageViewState;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.frame.compiler.R;
import com.hjq.shape.layout.ShapeLinearLayout;
import com.luck.picture.lib.utils.MediaUtils;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * project:PJHAndroidFrame
 * package:com.frame.pjh_core.widget.glide
 * Created by 彭俊鸿 on 2018/6/4.
 * e-mail : 1031028399@qq.com
 */

public class GlideUtils {

    //加载网络图片
    public static void loadImg(Object path, ImageView mImageView) {
        loadImg(path, mImageView, R.drawable.ic_placeholder_bg, R.color.color_f0f0f0);
    }

    public static void loadImg(Object path, ImageView mImageView, int lodingImage, int errorImageView) {
        loadImg(path, mImageView, lodingImage, errorImageView, DiskCacheStrategy.AUTOMATIC, false, null);
    }

    public static void loadImg(Object path, ImageView mImageView, int lodingImage, int errorImageView, int imgWidth) {
        loadImg(path, mImageView, lodingImage, errorImageView, imgWidth, DiskCacheStrategy.AUTOMATIC, false, null);
    }

    public static void loadImg(Object path, ImageView mImageView, int lodingImage, Transformation requestOptions) {
        loadImg(path, mImageView, lodingImage, lodingImage, DiskCacheStrategy.AUTOMATIC, false, requestOptions);
    }

    public static void loadImg(Object path, ImageView mImageView, int lodingImage, int errorImageView, DiskCacheStrategy strategy, Transformation requestOptions) {
        loadImg(path, mImageView, lodingImage, errorImageView, strategy, false, requestOptions);
    }

    //needRealtime 是否需要实时获取
    public static void loadImg(Object path, ImageView mImageView, int lodingImage, int errorImageView, DiskCacheStrategy strategy, boolean needRealtime, Transformation requestOptions) {
        loadImg(path, mImageView, lodingImage, errorImageView, 320, strategy, needRealtime, requestOptions);
    }

    //needRealtime 是否需要实时获取
    public static void loadImg(Object path, ImageView mImageView, int lodingImage, int errorImageView, int imgWidth, DiskCacheStrategy strategy, boolean needRealtime, Transformation requestOptions) {
//        if (path instanceof String && (((String) path).contains("http") || ((String) path).contains("https"))) {
//            if (((String) path).contains("?")) {
//                path = String.valueOf(path) + "&x-oss-process=image/resize,s_"+(imgWidth==0?320:imgWidth);
//            } else {
//                path = String.valueOf(path) + "?x-oss-process=image/resize,s_"+(imgWidth==0?320:imgWidth);
//            }
//        }

        if (mImageView == null || mImageView.getContext() == null) {
            return;
        }

        if (mImageView.getContext() instanceof FragmentActivity) {
            if (isDestroy((FragmentActivity) mImageView.getContext())) {
                return;
            }
        }
        if (needRealtime) {
            if (requestOptions == null) {
                GlideApp.with(mImageView.getContext()).load(path).centerCrop().signature(new ObjectKey(UUID.randomUUID())).diskCacheStrategy(strategy).
                        placeholder(lodingImage).error(errorImageView).into(mImageView);
            } else {
                GlideApp.with(mImageView.getContext()).load(path).centerCrop().signature(new ObjectKey(UUID.randomUUID())).diskCacheStrategy(strategy).
                        placeholder(lodingImage).error(errorImageView).transform(new CenterCrop(), requestOptions).into(mImageView);
            }
        } else {
            if (requestOptions == null) {
                GlideApp.with(mImageView.getContext()).load(path).centerCrop().diskCacheStrategy(strategy).placeholder(lodingImage).
                        error(errorImageView).into(mImageView);
            } else {
                GlideApp.with(mImageView.getContext()).load(path).centerCrop().diskCacheStrategy(strategy).placeholder(lodingImage).
                        error(errorImageView).transform(new CenterCrop(), requestOptions).into(mImageView);
            }
        }
    }

    //图片加载监听
    public static void loadImg_listener(Object path, ImageView mImageView, int lodingImage, RequestListener<Drawable> listener) {
        if (path instanceof String) {
            if (((String) path).contains("?")) {
                path = String.valueOf(path) + "&x-oss-process=image/resize,s_320";
            } else {
                path = String.valueOf(path) + "?x-oss-process=image/resize,s_320";
            }
        }

        if (mImageView == null || mImageView.getContext() == null) {
            return;
        }

        if (mImageView.getContext() instanceof FragmentActivity) {
            if (isDestroy((FragmentActivity) mImageView.getContext())) {
                return;
            }
        }

        GlideApp.with(mImageView.getContext()).load(path).centerCrop().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).placeholder(lodingImage).
                error(lodingImage).listener(listener).into(mImageView);
    }

    public static void getImageWidHeig(Context context, String pathUrl, final IGetImageData iGetImageData) {
        if (context == null) {
            return;
        }

        if (context instanceof FragmentActivity) {
            if (isDestroy((FragmentActivity) context)) {
                return;
            }
        }

        //获取图片真正的宽高
        GlideApp.with(context)
                .asBitmap()//Glide返回一个Bitmap对象
                .load(pathUrl)
                .skipMemoryCache(true)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        double proportionScreen = new BigDecimal(String.valueOf(resource.getWidth())).divide(new BigDecimal(resource.getHeight()), 2, BigDecimal.ROUND_HALF_UP).doubleValue();//屏幕的宽高比
                        iGetImageData.sendData(resource, resource.getWidth(), resource.getHeight(), proportionScreen);
                    }
                });
    }

    //判断是否是长图 区分加载的view
    public static void loadLongImg(final Context context, String pathUrl, final ImageView imageView, final SubsamplingScaleImageView subsamplingScaleImageView) {
        if (context == null) {
            return;
        }

        if (context instanceof FragmentActivity) {
            if (isDestroy((FragmentActivity) context)) {
                return;
            }
        }

        //获取图片真正的宽高
        GlideApp.with(context)
                .asBitmap()//Glide返回一个Bitmap对象
                .load(pathUrl)
                .skipMemoryCache(true)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        double proportionScreen = new BigDecimal(String.valueOf(resource.getWidth())).divide(new BigDecimal(resource.getHeight()), 2, BigDecimal.ROUND_HALF_UP).doubleValue();//屏幕的宽高比

                        if (MediaUtils.isLongImage(resource.getWidth(), resource.getHeight())) {
                            imageView.setVisibility(View.GONE);
                            subsamplingScaleImageView.setVisibility(View.VISIBLE);
                            float scale = context.getResources().getDimensionPixelSize(R.dimen.dp_750) / (float) resource.getWidth();

                            subsamplingScaleImageView.setQuickScaleEnabled(false);
                            subsamplingScaleImageView.setZoomEnabled(false);
                            subsamplingScaleImageView.setPanEnabled(false);
                            subsamplingScaleImageView.setImage(ImageSource.cachedBitmap(resource),
                                    new ImageViewState(scale, new PointF(0, 0), 0));
                        } else {
                            imageView.setVisibility(View.VISIBLE);
                            subsamplingScaleImageView.setVisibility(View.GONE);

                            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) imageView.getLayoutParams();
                            layoutParams.width = context.getResources().getDimensionPixelSize(R.dimen.dp_750);
                            layoutParams.height = new BigDecimal(layoutParams.width).divide(new BigDecimal(proportionScreen), 1, BigDecimal.ROUND_HALF_UP).intValue();
                            imageView.setLayoutParams(layoutParams);
                            imageView.setImageBitmap(resource);
                        }
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {

                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {

                    }
                });
    }

    public interface IGetImageData {
        void sendData(@NonNull Bitmap resource, int width, int height, double radio);
    }

    /**
     * 判断Activity是否Destroy
     *
     * @param mActivity
     * @return
     */
    public static boolean isDestroy(Activity mActivity) {
        if (mActivity == null || mActivity.isFinishing() || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && mActivity.isDestroyed())) {
            return true;
        } else {
            return false;
        }
    }
}
