package com.frame.compiler.widget.permission;

import android.content.Context;

import androidx.annotation.NonNull;

import com.frame.compiler.R;

import java.util.List;

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/XXPermissions
 *    time   : 2023/01/02
 *    desc   : 权限描述转换器
 */
public final class PermissionDescriptionConvert {

    /**
     * 获取权限描述
     */
   public static String getPermissionDescription(Context context, List<String> permissions) {
       StringBuilder stringBuilder = new StringBuilder();
       List<String> permissionNames = PermissionNameConvert.permissionsToNames(context, permissions);
       for (String permissionName : permissionNames) {
           stringBuilder.append(permissionName)
               .append(context.getString(R.string.common_permission_colon))
               .append(permissionsToDescription(context, permissionName))
               .append("\n");
       }
       return stringBuilder.toString().trim();
   }

   /**
    * 将权限名称列表转换成对应权限描述
    */
   @NonNull
   public static String permissionsToDescription(Context context, String permissionName) {
       // 请根据权限名称转换成对应权限说明
       switch (permissionName){
           case "存储权限":
               return "申请存储权限,主要在手机本地存放,查看一些重要文件（包括唯一码,图片,apk,以及一些其他下载文件）";
           case "照片和视频权限":
               return "访问相册时，必须申请的权限,不然看不到内容";
           case "音乐和音频权限":
               return "申请音乐和音频权限,主要是播放音乐";
           case "相机权限":
               return "使用摄像头拍照时,必须的权限,不然无法使用摄像头拍照";
           case "麦克风权限":
               return "在使用麦克风时，必须的权限,不然语音时,其他人听不到你的声音";
       }
       return "用于 xxx 业务";
   }
}