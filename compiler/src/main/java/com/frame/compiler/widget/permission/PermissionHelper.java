package com.frame.compiler.widget.permission;

import android.app.Activity;
import android.content.Context;

import androidx.fragment.app.Fragment;

import com.hjq.permissions.OnPermissionCallback;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;

import java.util.List;

//权限申请帮助类
public class PermissionHelper {

    //申请单个权限的时候用到
    public void requestPermission(final Activity activity, final onPermissionListener onPermissionListener,String permissionTip, String permission) {
        XXPermissions.with(activity)
                .permission(permission)
                .interceptor(new PermissionInterceptor(permissionTip))
                .request(new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        if (all) {
                            onPermissionListener.onSuccess();
                        } else {
                            onPermissionListener.onNoAllSuccess("获取部分权限成功，但部分权限未正常授予");
                        }
                    }

                    @Override
                    public void onDenied(List<String> permissions, boolean never) {
                        if (never) {
                            onPermissionListener.onFail("被永久拒绝授权，请到系统设置页面手动授予权限");
                            // 如果是被永久拒绝就跳转到应用权限系统设置页面
                            XXPermissions.startPermissionActivity(activity, permissions);
                        } else {
                            if (permissions.contains(Permission.ACCESS_BACKGROUND_LOCATION)) {
                                onPermissionListener.onFail("没有授予后台定位权限，请您选择\"始终允许\"");
                            } else {
                                onPermissionListener.onFail("获取权限失败");
                            }
                        }
                    }
                });
    }

    //申请多个权限的时候用到
    public void requestPermission(final Activity activity, final onPermissionListener onPermissionListener,String permissionTip,List<String> permissions, String[]... permission) {
        XXPermissions xxPermissions = XXPermissions.with(activity);
        if (permissions != null && !permissions.isEmpty()) {
            //申请单个权限
            xxPermissions.permission(permissions);
        }
        if (permission!=null&&permission.length != 0) {
            //申请权限组
            xxPermissions.permission(permission);
        }

        xxPermissions.interceptor(new PermissionInterceptor(permissionTip))
                .request(new OnPermissionCallback() {
            @Override
            public void onGranted(List<String> permissions, boolean all) {
                if (all) {
                    onPermissionListener.onSuccess();
                } else {
                    onPermissionListener.onNoAllSuccess("获取部分权限成功，但部分权限未正常授予");
                }
            }

            @Override
            public void onDenied(List<String> permissions, boolean never) {
                if (never) {
                    onPermissionListener.onFail("被永久拒绝授权，请到系统设置页面手动授予权限");
                    // 如果是被永久拒绝就跳转到应用权限系统设置页面
                    XXPermissions.startPermissionActivity(activity, permissions);
                } else {
                    if (permissions.contains(Permission.ACCESS_BACKGROUND_LOCATION)) {
                        onPermissionListener.onFail("没有授予后台定位权限，请您选择\"始终允许\"");
                    } else {
                        onPermissionListener.onFail("获取权限失败");
                    }
                }
            }
        });
    }

    //申请单个权限的时候用到
    public void requestPermission(final Fragment fragment, final onPermissionListener onPermissionListener,String permissionTip,String permission) {
        XXPermissions.with(fragment)
                .permission(permission)
                .interceptor(new PermissionInterceptor(permissionTip))
                .request(new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        if (all) {
                            onPermissionListener.onSuccess();
                        } else {
                            onPermissionListener.onNoAllSuccess("获取部分权限成功，但部分权限未正常授予");
                        }
                    }

                    @Override
                    public void onDenied(List<String> permissions, boolean never) {
                        if (never) {
                            onPermissionListener.onFail("被永久拒绝授权，请到系统设置页面手动授予权限");
                            // 如果是被永久拒绝就跳转到应用权限系统设置页面
                            XXPermissions.startPermissionActivity(fragment, permissions);
                        } else {
                            if (permissions.contains(Permission.ACCESS_BACKGROUND_LOCATION)) {
                                onPermissionListener.onFail("没有授予后台定位权限，请您选择\"始终允许\"");
                            } else {
                                onPermissionListener.onFail("获取权限失败");
                            }
                        }
                    }
                });
    }

    //申请多个权限的时候用到
    public void requestPermission(final Fragment fragment, final onPermissionListener onPermissionListener,String permissionTip, List<String> permissions, String[]... permission) {
        XXPermissions xxPermissions = XXPermissions.with(fragment);
        if (permissions != null && !permissions.isEmpty()) {
            //申请单个权限
            xxPermissions.permission(permissions);
        }
        if (permission!=null&&permission.length != 0) {
            //申请权限组
            xxPermissions.permission(permission);
        }

        xxPermissions.interceptor(new PermissionInterceptor(permissionTip))
                .request(new OnPermissionCallback() {
            @Override
            public void onGranted(List<String> permissions, boolean all) {
                if (all) {
                    onPermissionListener.onSuccess();
                } else {
                    onPermissionListener.onNoAllSuccess("获取部分权限成功，但部分权限未正常授予");
                }
            }

            @Override
            public void onDenied(List<String> permissions, boolean never) {
                if (never) {
                    onPermissionListener.onFail("被永久拒绝授权，请到系统设置页面手动授予权限");
//                            ToastUtil.show("被永久拒绝授权，请到系统设置页面手动授予权限");
                    // 如果是被永久拒绝就跳转到应用权限系统设置页面
                    XXPermissions.startPermissionActivity(fragment, permissions);
                } else {
                    if (permissions.contains(Permission.ACCESS_BACKGROUND_LOCATION)) {
                        onPermissionListener.onFail("没有授予后台定位权限，请您选择\"始终允许\"");
//                                ToastUtil.show("没有授予后台定位权限，请您选择\"始终允许\"");
                    } else {
                        onPermissionListener.onFail("获取权限失败");
//                                ToastUtil.show("获取权限失败");
                    }
                }
            }
        });
    }

    //判断是否具有定位的权限
    public boolean hasHomePermission(Context context) {
        return XXPermissions.isGranted(context, Permission.ACCESS_COARSE_LOCATION, Permission.ACCESS_FINE_LOCATION);
    }

    public interface onPermissionListener {
        void onSuccess();

        void onNoAllSuccess(String noAllSuccessText);

        void onFail(String failText);
    }
}
//----------- activity需要重新该方法 那样子当权限是从设置页面回来的时候 判断需要的权限已申请
//----------- 那么这时候就可以继续接下去 权限申请成功时的流程---------------------------------
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == XXPermissions.REQUEST_CODE) {
//            toast("检测到你刚刚从权限设置界面返回回来");
//        }
//    }