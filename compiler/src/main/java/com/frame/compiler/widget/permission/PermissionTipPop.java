package com.frame.compiler.widget.permission;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frame.compiler.R;
import com.frame.compiler.utils.EmptyUtils;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.TranslationConfig;

/**
 * ================================================
 * 项目名称：Locusts
 * 包    名：com.applications.locusts.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2024/08/28 0015
 * 描    述：权限说明弹框
 * ================================================
 */
public class PermissionTipPop extends BasePopupWindow {
    private String message;
    private TextView tvPermissionDescriptionMessage;

    public PermissionTipPop(Context context) {
        super(context);

        setContentView(R.layout.permission_description_popup);
        setBackgroundColor(context.getResources().getColor(R.color.bar_transparent));
    }

    public void setMessage(String message) {
        this.message=message;
        if (tvPermissionDescriptionMessage!=null){
            tvPermissionDescriptionMessage.setText(EmptyUtils.strEmpty(message));
        }
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        tvPermissionDescriptionMessage=contentView.findViewById(R.id.tv_permission_description_message);
        tvPermissionDescriptionMessage.setText(EmptyUtils.strEmpty(message));
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.FROM_TOP)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(TranslationConfig.TO_TOP)
                .toDismiss();
    }
}
