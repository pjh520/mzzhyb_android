package com.frame.compiler.widget.tabLayout.listener;

public interface OnTabSelectListener {
    void onTabSelect(int position);
    void onTabReselect(int position);
}