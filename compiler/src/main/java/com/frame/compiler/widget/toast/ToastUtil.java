package com.frame.compiler.widget.toast;

import android.annotation.SuppressLint;
import android.app.AppOpsManager;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import com.frame.compiler.utils.RxTool;
import com.hjq.toast.ToastUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * ================================================
 * 项目名称：AndroidFrameNew
 * 包    名：com.frame.compiler.widget
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/5/5
 * 描    述：
 * ================================================
 */
public class ToastUtil {
    //判断是否有通知权限
    public static boolean isNotificationEnabled() {
        return areNotificationsEnabled(RxTool.getContext());
    }

    //显示短时提示
    public static void show(String tip) {
//        ToastUtils.setGravity(Gravity.BOTTOM);//设置toast显示的位置
        ToastUtils.showLong(tip);
    }

    @SuppressWarnings("ConstantConditions")
    @SuppressLint("PrivateApi")
    protected static boolean areNotificationsEnabled(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return context.getSystemService(NotificationManager.class).areNotificationsEnabled();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // 参考 Support 库中的方法： NotificationManagerCompat.from(context).areNotificationsEnabled()
            AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            try {
                Method method = appOps.getClass().getMethod("checkOpNoThrow",
                        Integer.TYPE, Integer.TYPE, String.class);
                Field field = appOps.getClass().getDeclaredField("OP_POST_NOTIFICATION");
                int value = (int) field.get(Integer.class);
                return ((int) method.invoke(appOps, value, context.getApplicationInfo().uid,
                        context.getPackageName())) == AppOpsManager.MODE_ALLOWED;
            } catch (NoSuchMethodException | NoSuchFieldException | InvocationTargetException |
                    IllegalAccessException | RuntimeException e) {
                e.printStackTrace();
                return true;
            }
        }
        return true;
    }
}
