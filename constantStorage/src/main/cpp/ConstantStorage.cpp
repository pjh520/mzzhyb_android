//
// Created by Administrator on 2021/4/22 0022.
//

#include <jni.h>
#include <string.h>
#include <stdio.h>

/* litepal 数据库加密key*/
const char* LITE_AES_KEY = "JAKKW12J239N381929SSDVM982SAL9AK";
/*字符密码加密key */
const char* PAY_3DES_KEY = "ylzhybs@2022ylzhybs@2022";
/*省医保局H5页面对接AES加密key */
const char* H5_AES_KEY = "ylz@irokf^*iorn!";


/**
 * 发布的app 签名,只有和本签名一致的app 才会返回合法的 授权 key
 */
const char* RELEASE_SIGN ="308203423082022a020101300d06092a864886f70d01010b05003066310f300d06035504030c06313131313131310f300d060355040b0c06313131313131310f300d060355040a0c06313131313131310f300d06035504070c06313131313131310f300d06035504080c06313131313131310f300d060355040613063131313131313020170d3234303731333039313033335a180f32303534303730363039313033335a3066310f300d06035504030c06313131313131310f300d060355040b0c06313131313131310f300d060355040a0c06313131313131310f300d06035504070c06313131313131310f300d06035504080c06313131313131310f300d0603550406130631313131313130820122300d06092a864886f70d01010105000382010f003082010a02820101008785f7afbf134137b2463d0d966f38c27cfb479f98cde17fed750b874bd8f891d4b46b732d9851458ca1e78047e25c5567ca48cebac17965fb420d56ef6976aa6bba5c7f96dda8681342d6d99a0186d4e1fa1edadb1e509cee240453369a00e0170a3407e0838c5b445a67e6b69f482d59faa1906011d2d6113dce80cdfab05718e04c8869f63c515fa26453dd162290d0430715c626556a6a00d34eb3a3492d0efe84589833c000471a680fff368c13737029e30b4d5c2155e8cdd77ff0337ea310aa6e58c5c6cae4ebe73ae8d204bbb0f048d39d1a73f3ce1ed9f5ce704cfd70ca31edb23db511f8ea5d39ae0c9103e29f0be05bd7d0488bb7b79b8aac601d0203010001300d06092a864886f70d01010b050003820101004a5d5bfa868335d3e086bd557d709a6da4e98b08535b2991ec7dbf38e28d03cc6d7bec1f6ee91c0ca95826c16f9ea1f05fff481557de64e944d2485de74d88e4bbf22118522aa737403e29af10b1a79364e7a9765685670934f80733d72afe71144a657d8be3937c26b0f225ceb647944a507e9e2b8e3b208ecd72ec6fb2632de1478b241be5dc07c87c873079e6b9539ab1c46e8921171b80fcab07bfa5de4310d75c79e8a093a7ba4f72662c00921ea0ce3004fa6eb0dc606a4424a1fc71687f8388ec718497679838f967f985ea7399b3711c4ede458b422840cf9ab0036bd778e61542417cad1875971db7dbe8b80df18f910356e110c3b32175e0b49507";

/**
 * 拿到传入的app  的 签名信息，对比合法的app 签名，防止so文件被未知应用盗用
 */
static jclass contextClass;
static jclass signatureClass;
static jclass packageNameClass;
static jclass packageInfoClass;

extern "C" JNIEXPORT jstring JNICALL Java_com_library_constantStorage_ConstantStorage_getLiteAesKey(
		JNIEnv * env, jobject obj, jobject contextObject) {

	jmethodID getPackageManagerId = (env)->GetMethodID(contextClass, "getPackageManager","()Landroid/content/pm/PackageManager;");
	jmethodID getPackageNameId = (env)->GetMethodID(contextClass, "getPackageName","()Ljava/lang/String;");
	jmethodID signToStringId = (env)->GetMethodID(signatureClass, "toCharsString","()Ljava/lang/String;");
	jmethodID getPackageInfoId = (env)->GetMethodID(packageNameClass, "getPackageInfo","(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;");
	jobject packageManagerObject =  (env)->CallObjectMethod(contextObject, getPackageManagerId);
	jstring packNameString =  (jstring)(env)->CallObjectMethod(contextObject, getPackageNameId);
	jobject packageInfoObject = (env)->CallObjectMethod(packageManagerObject, getPackageInfoId,packNameString, 64);
	jfieldID signaturefieldID =(env)->GetFieldID(packageInfoClass,"signatures", "[Landroid/content/pm/Signature;");
	jobjectArray signatureArray = (jobjectArray)(env)->GetObjectField(packageInfoObject, signaturefieldID);
	jobject signatureObject =  (env)->GetObjectArrayElement(signatureArray,0);

	const char* signStrng =  (env)->GetStringUTFChars((jstring)(env)->CallObjectMethod(signatureObject, signToStringId),0);
	if(strcmp(signStrng,RELEASE_SIGN)==0)//签名一致  返回合法的 api key，否则返回错误
	{
	   return (env)->NewStringUTF(LITE_AES_KEY);
	}else
	{
	   return (env)->NewStringUTF("error");
	}
}

extern "C" JNIEXPORT jstring JNICALL Java_com_library_constantStorage_ConstantStorage_getPay3DESKey(
		JNIEnv * env, jobject obj, jobject contextObject) {

	jmethodID getPackageManagerId = (env)->GetMethodID(contextClass, "getPackageManager","()Landroid/content/pm/PackageManager;");
	jmethodID getPackageNameId = (env)->GetMethodID(contextClass, "getPackageName","()Ljava/lang/String;");
	jmethodID signToStringId = (env)->GetMethodID(signatureClass, "toCharsString","()Ljava/lang/String;");
	jmethodID getPackageInfoId = (env)->GetMethodID(packageNameClass, "getPackageInfo","(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;");
	jobject packageManagerObject =  (env)->CallObjectMethod(contextObject, getPackageManagerId);
	jstring packNameString =  (jstring)(env)->CallObjectMethod(contextObject, getPackageNameId);
	jobject packageInfoObject = (env)->CallObjectMethod(packageManagerObject, getPackageInfoId,packNameString, 64);
	jfieldID signaturefieldID =(env)->GetFieldID(packageInfoClass,"signatures", "[Landroid/content/pm/Signature;");
	jobjectArray signatureArray = (jobjectArray)(env)->GetObjectField(packageInfoObject, signaturefieldID);
	jobject signatureObject =  (env)->GetObjectArrayElement(signatureArray,0);

	const char* signStrng =  (env)->GetStringUTFChars((jstring)(env)->CallObjectMethod(signatureObject, signToStringId),0);
	if(strcmp(signStrng,RELEASE_SIGN)==0)//签名一致  返回合法的 api key，否则返回错误
	{
	   return (env)->NewStringUTF(PAY_3DES_KEY);
	}else
	{
	   return (env)->NewStringUTF("error");
	}
}

extern "C" JNIEXPORT jstring JNICALL Java_com_library_constantStorage_ConstantStorage_getH5AESKey(
		JNIEnv * env, jobject obj, jobject contextObject) {

	jmethodID getPackageManagerId = (env)->GetMethodID(contextClass, "getPackageManager","()Landroid/content/pm/PackageManager;");
	jmethodID getPackageNameId = (env)->GetMethodID(contextClass, "getPackageName","()Ljava/lang/String;");
	jmethodID signToStringId = (env)->GetMethodID(signatureClass, "toCharsString","()Ljava/lang/String;");
	jmethodID getPackageInfoId = (env)->GetMethodID(packageNameClass, "getPackageInfo","(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;");
	jobject packageManagerObject =  (env)->CallObjectMethod(contextObject, getPackageManagerId);
	jstring packNameString =  (jstring)(env)->CallObjectMethod(contextObject, getPackageNameId);
	jobject packageInfoObject = (env)->CallObjectMethod(packageManagerObject, getPackageInfoId,packNameString, 64);
	jfieldID signaturefieldID =(env)->GetFieldID(packageInfoClass,"signatures", "[Landroid/content/pm/Signature;");
	jobjectArray signatureArray = (jobjectArray)(env)->GetObjectField(packageInfoObject, signaturefieldID);
	jobject signatureObject =  (env)->GetObjectArrayElement(signatureArray,0);

	const char* signStrng =  (env)->GetStringUTFChars((jstring)(env)->CallObjectMethod(signatureObject, signToStringId),0);
	if(strcmp(signStrng,RELEASE_SIGN)==0)//签名一致  返回合法的 api key，否则返回错误
	{
		return (env)->NewStringUTF(H5_AES_KEY);
	}else
	{
		return (env)->NewStringUTF("error");
	}
}

JNIEXPORT jint JNICALL JNI_OnLoad (JavaVM* vm,void* reserved){

	 JNIEnv* env = NULL;
	 jint result=-1;
	 if(vm->GetEnv((void**)&env, JNI_VERSION_1_4) != JNI_OK)
	   return result;

	 contextClass = (jclass)env->NewGlobalRef((env)->FindClass("android/content/Context"));
	 signatureClass = (jclass)env->NewGlobalRef((env)->FindClass("android/content/pm/Signature"));
	 packageNameClass = (jclass)env->NewGlobalRef((env)->FindClass("android/content/pm/PackageManager"));
	 packageInfoClass = (jclass)env->NewGlobalRef((env)->FindClass("android/content/pm/PackageInfo"));

	 return JNI_VERSION_1_4;
 }