package com.library.constantStorage;

import android.content.Context;

public class ConstantStorage {

    static {
        System.loadLibrary("ConstantStorage");
    }

    public native static String getLiteAesKey(Context context);
    public native static String getPay3DESKey(Context context);
    public native static String getH5AESKey(Context context);
}