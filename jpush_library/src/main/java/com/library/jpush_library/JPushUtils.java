package com.library.jpush_library;

import android.content.Context;

import cn.jiguang.api.utils.JCollectionAuth;
import cn.jpush.android.api.JPushInterface;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.library.jpush_library
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-02-24
 * 描    述：
 * ================================================
 */
public class JPushUtils {

    //初始化极光推送
    public static void init(Context context){
        JCollectionAuth.setAuth(context,true);
        JPushInterface.setDebugMode(false);
        JPushInterface.init(context);
    }

}
