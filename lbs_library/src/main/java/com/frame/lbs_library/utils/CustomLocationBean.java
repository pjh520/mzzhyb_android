package com.frame.lbs_library.utils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.jrdz.zhyb_android.utils.location
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-11-28
 * 描    述：
 * ================================================
 */
public class CustomLocationBean {
    private String province;//省
    private String city;//市
    private String district;//区
    private String Street;//街道
    private String detail_address;//详细地址
    private double mLatitude;
    private double mLongitude;

    public CustomLocationBean() {
    }

    public CustomLocationBean(double longitude, double mLatitude) {
        setLatitude(mLatitude);
        setLongitude(longitude);
    }

    public CustomLocationBean(String province, String city, String district, String street, String detail_address, double mLatitude, double mLongitude) {
        this.province = province;
        this.city = city;
        this.district = district;
        Street = street;
        this.detail_address = detail_address;
        this.mLatitude = mLatitude;
        this.mLongitude = mLongitude;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getDetail_address() {
        return detail_address;
    }

    public void setDetail_address(String detail_address) {
        this.detail_address = detail_address;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        this.mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        this.mLongitude = longitude;
    }

    @Override
    public String toString() {
        return "CustomLocationBean{" +
                "province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", district='" + district + '\'' +
                ", Street='" + Street + '\'' +
                ", detail_address='" + detail_address + '\'' +
                ", mLatitude=" + mLatitude +
                ", mLongitude=" + mLongitude +
                '}';
    }
}
