package com.frame.lbs_library.utils;

/**
 * ================================================
 * 项目名称：zhyb_android
 * 包    名：com.frame.lbs_library.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2023-12-01
 * 描    述：
 * ================================================
 */
public interface ILocationListener {
    void onLocationSuccess(CustomLocationBean customLocationBean);
    void onLocationError(String errorText);
}
