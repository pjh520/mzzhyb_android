package com.frame.lbs_library.utils;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.baidu.location.BDAbstractLocationListener;
import com.baidu.location.BDLocation;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.geocode.GeoCodeOption;
import com.baidu.mapapi.search.geocode.GeoCodeResult;
import com.baidu.mapapi.search.geocode.GeoCoder;
import com.baidu.mapapi.search.geocode.OnGetGeoCoderResultListener;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeResult;
import com.frame.compiler.utils.LogUtils;
import com.frame.compiler.utils.NetworkUtils;
import com.frame.compiler.utils.RxTool;
import com.frame.compiler.widget.permission.PermissionHelper;
import com.frame.compiler.widget.toast.ToastUtil;
import com.hjq.permissions.Permission;

/**
 * ================================================
 * 项目名称：AndroidFrameNew
 * 包    名：com.frame.lbs_library.utils
 * 作    者：彭俊鸿
 * 邮    箱：1031028399@qq.com
 * 版    本：1.0
 * 创建日期：2020/5/21 0021
 * 描    述：百度地位工具类
 * ================================================
 */
public class LBSUtil {
    private LocationClient mLocationClient;
    private MyLocationListener myListener = new MyLocationListener();
    private LocationClientOption mOption;
    private ILocationListener ilbsLocationListener;

    private PermissionHelper permissionHelper;
    private GeoCoder mCoder;
    private IGeoCoderListener mIGeoCoderListener;

    /**
     * 内部类实现单例模式
     * 延迟加载，减少内存开销
     */
    private static class SingletonHolder {
        private static LBSUtil instance = new LBSUtil();
    }

    /**
     * 私有的构造函数
     */
    private LBSUtil() {}

    public static LBSUtil getInstance() {
        return LBSUtil.SingletonHolder.instance;
    }

    //初始化百度定位
    public void initLBS(final Activity activity,final ILocationListener ilbsLocation){
        if (permissionHelper==null){
            permissionHelper=new PermissionHelper();
        }
        permissionHelper.requestPermission(activity, new PermissionHelper.onPermissionListener() {
            @Override
            public void onSuccess() {
                onStartLocation(ilbsLocation);
            }

            @Override
            public void onNoAllSuccess(String noAllSuccessText) {}

            @Override
            public void onFail(String failText) {
                ilbsLocationListener=ilbsLocation;
                ilbsLocationListener.onLocationError("权限申请失败，app将不能获取位置信息");
            }
        },"定位权限:获取当前的经纬度,以便计算距离以及显示当前的位置信息", Permission.ACCESS_FINE_LOCATION);
    }
    //初始化百度定位
    public void initLBS(final Fragment fragment, final ILocationListener ilbsLocation){
        if (permissionHelper==null){
            permissionHelper=new PermissionHelper();
        }
        permissionHelper.requestPermission(fragment, new PermissionHelper.onPermissionListener() {
            @Override
            public void onSuccess() {
                onStartLocation(ilbsLocation);
            }

            @Override
            public void onNoAllSuccess(String noAllSuccessText) {

            }

            @Override
            public void onFail(String failText) {
                ilbsLocationListener=ilbsLocation;
                ilbsLocationListener.onLocationError("权限申请失败，app将不能获取位置信息");
            }
        },"定位权限:获取当前的经纬度,以便计算距离以及显示当前的位置信息", Permission.ACCESS_FINE_LOCATION);
    }

    //开启定位
    public void onStartLocation(final ILocationListener ilbsLocation){
        ilbsLocationListener=ilbsLocation;
        //声明LocationClient类
        //定位服务
        LocationClient.setAgreePrivacy(true);
        try {
            if (mLocationClient==null){
                mLocationClient = new LocationClient(RxTool.getContext());
                //注册监听函数
                mLocationClient.registerLocationListener(myListener);
                mLocationClient.setLocOption(getDefaultLocationClientOption());
            }
            mLocationClient.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     *
     * @return DefaultLocationClientOption  默认O设置
     */
    public LocationClientOption getDefaultLocationClientOption() {
        if (mOption == null) {
            mOption = new LocationClientOption();
            mOption.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy); // 可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
            mOption.setCoorType("bd09ll"); // 可选，默认gcj02，设置返回的定位结果坐标系，如果配合百度地图使用，建议设置为bd09ll;
            mOption.setOnceLocation(true);
            mOption.setFirstLocType(LocationClientOption.FirstLocType.SPEED_IN_FIRST_LOC);
            mOption.setScanSpan(0); // 可选，默认0，即仅定位一次，设置发起连续定位请求的间隔需要大于等于1000ms才是有效的
            mOption.setIsNeedAddress(true); // 可选，设置是否需要地址信息，默认不需要
            mOption.setIsNeedLocationDescribe(true); // 可选，设置是否需要地址描述
            mOption.setNeedDeviceDirect(false); // 可选，设置是否需要设备方向结果
            mOption.setLocationNotify(false); // 可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
            mOption.setIgnoreKillProcess(true); // 可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop
            mOption.setIsNeedLocationDescribe(true); // 可选，默认false，设置是否需要位置语义化结果，可以在BDLocation
            mOption.setIsNeedLocationPoiList(true); // 可选，默认false，设置是否需要POI结果，可以在BDLocation
            mOption.SetIgnoreCacheException(false); // 可选，默认false，设置是否收集CRASH信息，默认收集
            mOption.setOpenGps(true); // 可选，默认false，设置是否开启Gps定位
            mOption.setIsNeedAltitude(false); // 可选，默认false，设置定位时是否需要海拔信息，默认不需要，除基础定位版本都可用
        }
        return mOption;
    }

    public class MyLocationListener extends BDAbstractLocationListener {
        @Override
        public void onReceiveLocation(BDLocation location){
            //此处的BDLocation为定位结果信息类，通过它的各种get方法可获取定位相关的全部结果
            //以下只列举部分获取地址相关的结果信息
            //更多结果信息获取说明，请参照类参考中BDLocation类中的说明
            if (null != location && location.getLocType() != BDLocation.TypeServerError) {
//                StringBuffer sb = new StringBuffer(256);
//                sb.append("time : ");
//                /**
//                 * 时间也可以使用systemClock.elapsedRealtime()方法 获取的是自从开机以来，每次回调的时间；
//                 * location.getTime() 是指服务端出本次结果的时间，如果位置不发生变化，则时间不变
//                 */
//                sb.append(location.getTime());
//                sb.append("\nlocType : ");// 定位类型
//                sb.append(location.getLocType());
//                sb.append("\nlocType description : ");// *****对应的定位类型说明*****
//                sb.append(location.getLocTypeDescription());
//                sb.append("\nlatitude : ");// 纬度
//                sb.append(location.getLatitude());
//                sb.append("\nlongtitude : ");// 经度
//                sb.append(location.getLongitude());
//                sb.append("\nradius : ");// 半径
//                sb.append(location.getRadius());
//                sb.append("\nCountryCode : ");// 国家码
//                sb.append(location.getCountryCode());
//                sb.append("\nProvince : ");// 获取省份
//                sb.append(location.getProvince());
//                sb.append("\nCountry : ");// 国家名称
//                sb.append(location.getCountry());
//                sb.append("\ncitycode : ");// 城市编码
//                sb.append(location.getCityCode());
//                sb.append("\ncity : ");// 城市
//                sb.append(location.getCity());
//                sb.append("\nDistrict : ");// 区
//                sb.append(location.getDistrict());
//                sb.append("\nTown : ");// 获取镇信息
//                sb.append(location.getTown());
//                sb.append("\nStreet : ");// 街道
//                sb.append(location.getStreet());
//                sb.append("\naddr : ");// 地址信息
//                sb.append(location.getAddrStr());
//                sb.append("\nStreetNumber : ");// 获取街道号码
//                sb.append(location.getStreetNumber());
//                sb.append("\nUserIndoorState: ");// *****返回用户室内外判断结果*****
//                sb.append(location.getUserIndoorState());
//                sb.append("\nDirection(not all devices have value): ");
//                sb.append(location.getDirection());// 方向
//                sb.append("\nlocationdescribe: ");
//                sb.append(location.getLocationDescribe());// 位置语义化信息
//                sb.append("\nPoi: ");// POI信息
//                if (location.getPoiList() != null && !location.getPoiList().isEmpty()) {
//                    for (int i = 0; i < location.getPoiList().size(); i++) {
//                        Poi poi = (Poi) location.getPoiList().get(i);
//                        sb.append("poiName:");
//                        sb.append(poi.getName() + ", ");
//                        sb.append("poiTag:");
//                        sb.append(poi.getTags() + "\n");
//                    }
//                }
//                if (location.getPoiRegion() != null) {
//                    sb.append("PoiRegion: ");// 返回定位位置相对poi的位置关系，仅在开发者设置需要POI信息时才会返回，在网络不通或无法获取时有可能返回null
//                    PoiRegion poiRegion = location.getPoiRegion();
//                    sb.append("DerectionDesc:"); // 获取POIREGION的位置关系，ex:"内"
//                    sb.append(poiRegion.getDerectionDesc() + "; ");
//                    sb.append("Name:"); // 获取POIREGION的名字字符串
//                    sb.append(poiRegion.getName() + "; ");
//                    sb.append("Tags:"); // 获取POIREGION的类型
//                    sb.append(poiRegion.getTags() + "; ");
//                    sb.append("\nSDK版本: ");
//                }
//                if (location.getLocType() == BDLocation.TypeGpsLocation) {// GPS定位结果
//                    sb.append("\nspeed : ");
//                    sb.append(location.getSpeed());// 速度 单位：km/h
//                    sb.append("\nsatellite : ");
//                    sb.append(location.getSatelliteNumber());// 卫星数目
//                    sb.append("\nheight : ");
//                    sb.append(location.getAltitude());// 海拔高度 单位：米
//                    sb.append("\ngps status : ");
//                    sb.append(location.getGpsAccuracyStatus());// *****gps质量判断*****
//                    sb.append("\ndescribe : ");
//                    sb.append("gps定位成功");
//                } else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {// 网络定位结果
//                    // 运营商信息
//                    if (location.hasAltitude()) {// *****如果有海拔高度*****
//                        sb.append("\nheight : ");
//                        sb.append(location.getAltitude());// 单位：米
//                    }
//                    sb.append("\noperationers : ");// 运营商信息
//                    sb.append(location.getOperators());
//                    sb.append("\ndescribe : ");
//                    sb.append("网络定位成功");
//                } else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
//                    sb.append("\ndescribe : ");
//                    sb.append("离线定位成功，离线定位结果也是有效的");
//                } else if (location.getLocType() == BDLocation.TypeServerError) {
//                    sb.append("\ndescribe : ");
//                    sb.append("服务端网络定位失败，可以反馈IMEI号和大体定位时间到loc-bugs@baidu.com，会有人追查原因");
//                } else if (location.getLocType() == BDLocation.TypeNetWorkException) {
//                    sb.append("\ndescribe : ");
//                    sb.append("网络不同导致定位失败，请检查网络是否通畅");
//                } else if (location.getLocType() == BDLocation.TypeCriteriaException) {
//                    sb.append("\ndescribe : ");
//                    sb.append("无法获取有效定位依据导致定位失败，一般是由于手机的原因，处于飞行模式下一般会造成这种结果，可以试着重启手机");
//                }
//                LogUtils.e(sb.toString());
                switch (location.getLocType()){
                    case 61:
                    case 161:
                        ilbsLocationListener.onLocationSuccess(new CustomLocationBean(location.getProvince(),location.getCity(),
                                location.getDistrict(),location.getStreet(),location.getLocationDescribe(),location.getLatitude(),location.getLongitude()));
                        break;
                    case 62:
                        ilbsLocationListener.onLocationError("无法获取有效定位依据，请检查运营商网络或者WiFi网络是否正常开启，尝试重新请求定位");
                        break;
                    case 63:
                        ilbsLocationListener.onLocationError("没有成功向服务器发起请求，请确认当前测试手机网络是否通畅，尝试重新请求定位");
                        break;
                    case 69:
                        ilbsLocationListener.onLocationError("定位失败，请检查定位服务开关是否打开");
                        break;
                    case 70:
                        ilbsLocationListener.onLocationError("定位失败，请检查是否授予定位权限");
                        break;
                    case 71:
                        ilbsLocationListener.onLocationError("定位失败，请检查定位服务开关是否打开，以及是否授予定位权限");
                        break;
                }

            }else {
                //定位失败
                StringBuffer sb = new StringBuffer();
                sb.append("定位失败" + "\n");
                sb.append("错误码:" + location.getLocType() + "\n");
                LogUtils.e(sb.toString());
                ilbsLocationListener.onLocationError(sb.toString());
            }
        }

        /**
         * 回调定位诊断信息，开发者可以根据相关信息解决定位遇到的一些问题
         * @param locType 当前定位类型
         * @param diagnosticType 诊断类型（1~9）
         * @param diagnosticMessage 具体的诊断信息释义
         */
        @Override
        public void onLocDiagnosticMessage(int locType, int diagnosticType, String diagnosticMessage) {
            super.onLocDiagnosticMessage(locType, diagnosticType, diagnosticMessage);
            StringBuffer sb = new StringBuffer(256);
            sb.append("诊断结果: ");
            if (locType == BDLocation.TypeNetWorkLocation) {
                if (diagnosticType == 1) {
                    sb.append("网络定位成功，没有开启GPS，建议打开GPS会更好");
                    sb.append("\n" + diagnosticMessage);
                } else if (diagnosticType == 2) {
                    sb.append("网络定位成功，没有开启Wi-Fi，建议打开Wi-Fi会更好");
                    sb.append("\n" + diagnosticMessage);
                }
            } else if (locType == BDLocation.TypeOffLineLocationFail) {
                if (diagnosticType == 3) {
                    sb.append("定位失败，请您检查您的网络状态");
                    sb.append("\n" + diagnosticMessage);
                }
            } else if (locType == BDLocation.TypeCriteriaException) {
                if (diagnosticType == 4) {
                    sb.append("定位失败，无法获取任何有效定位依据");
                    sb.append("\n" + diagnosticMessage);
                } else if (diagnosticType == 5) {
                    sb.append("定位失败，无法获取有效定位依据，请检查运营商网络或者Wi-Fi网络是否正常开启，尝试重新请求定位");
                    sb.append(diagnosticMessage);
                } else if (diagnosticType == 6) {
                    sb.append("定位失败，无法获取有效定位依据，请尝试插入一张sim卡或打开Wi-Fi重试");
                    sb.append("\n" + diagnosticMessage);
                } else if (diagnosticType == 7) {
                    sb.append("定位失败，飞行模式下无法获取有效定位依据，请关闭飞行模式重试");
                    sb.append("\n" + diagnosticMessage);
                } else if (diagnosticType == 9) {
                    sb.append("定位失败，无法获取任何有效定位依据");
                    sb.append("\n" + diagnosticMessage);
                }
            } else if (locType == BDLocation.TypeServerError) {
                if (diagnosticType == 8) {
                    sb.append("定位失败，请确认您定位的开关打开状态，是否赋予APP定位权限");
                    sb.append("\n" + diagnosticMessage);
                }
            }
            LogUtils.e(sb.toString());
        }
    }

    //由地址解析经纬度
    public void GeoCoder(String city, String address, final IGeoCoderListener iGeoCoderListener){
        mIGeoCoderListener=iGeoCoderListener;
        if (mCoder==null){
            mCoder = GeoCoder.newInstance();
        }

        mCoder.setOnGetGeoCodeResultListener(new OnGetGeoCoderResultListener() {
            @Override
            public void onGetGeoCodeResult(GeoCodeResult geoCodeResult) {
                if (null != geoCodeResult && null != geoCodeResult.getLocation()) {
                    if (geoCodeResult == null || geoCodeResult.error != SearchResult.ERRORNO.NO_ERROR) {
                        //没有检索到结果
                        iGeoCoderListener.onGeoCoderError("该地址不合法，暂未检测到地址经纬度");
                    } else {
                        iGeoCoderListener.onGeoCoderSuccess(geoCodeResult.getLocation().latitude, geoCodeResult.getLocation().longitude);
                    }
                }
            }

            @Override
            public void onGetReverseGeoCodeResult(ReverseGeoCodeResult reverseGeoCodeResult) {}
        });

        //city 和 address是必填项
        mCoder.geocode(new GeoCodeOption()
                .city(city)
                .address(address));
    }
    public void stopGeoCoder(){
        if (mCoder != null) {
            mCoder.destroy();
            mCoder=null;
        }
        if (mIGeoCoderListener!= null){
            mIGeoCoderListener=null;
        }
    }

    //停止定位
    public void onStopLocation(){
        if (mLocationClient==null)return;
        mLocationClient.stop();
    }

    //停止定位
    public void onDestoryLocation(){
        if (null != mLocationClient) {
            /**
             * 如果AMapLocationClient是在当前Activity实例化的，
             * 在Activity的onDestroy中一定要执行AMapLocationClient的onDestroy
             */
            mLocationClient.stop();
            mLocationClient = null;
        }
    }
}
