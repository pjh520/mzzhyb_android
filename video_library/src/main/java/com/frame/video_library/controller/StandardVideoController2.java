package com.frame.video_library.controller;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.frame.video_library.R;
import com.frame.video_library.component.CompleteView2;
import xyz.doikki.videocontroller.StandardVideoController;
import xyz.doikki.videocontroller.component.ErrorView;
import xyz.doikki.videocontroller.component.GestureView;
import xyz.doikki.videocontroller.component.LiveControlView;
import xyz.doikki.videocontroller.component.PrepareView;
import xyz.doikki.videocontroller.component.TitleView;
import xyz.doikki.videocontroller.component.VodControlView;

/**
 * 直播/点播控制器
 * 注意：此控制器仅做一个参考，如果想定制ui，你可以直接继承GestureVideoController或者BaseVideoController实现
 * 你自己的控制器
 * Created by dueeeke on 2017/4/7.
 */

public class StandardVideoController2 extends StandardVideoController {
    private Animation anim;
    private PrepareView prepareView;

    public StandardVideoController2(@NonNull Context context) {
        super(context);
    }

    public StandardVideoController2(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public StandardVideoController2(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dkplayer_layout_standard_controller2;
    }

    @Override
    public void addDefaultControlComponent(String title, boolean isLive) {
        CompleteView2 completeView = new CompleteView2(getContext());
        ErrorView errorView = new ErrorView(getContext());
        prepareView = new PrepareView(getContext());
        prepareView.setClickStart();
        TitleView titleView = new TitleView(getContext());
        titleView.setTitle(title);
        addControlComponent(completeView, errorView, prepareView, titleView);
        if (isLive) {
            addControlComponent(new LiveControlView(getContext()));
        } else {
            addControlComponent(new VodControlView(getContext()));
        }
        addControlComponent(new GestureView(getContext()));
        setCanChangePosition(!isLive);
    }

    public PrepareView getPrepareView() {
        return prepareView;
    }

    @Override
    protected void onVisibilityChanged(boolean isVisible, Animation anim) {
        this.anim=anim;
        super.onVisibilityChanged(isVisible, anim);
    }

    public void onCancleAnim(){
        if (anim!=null){
            anim.reset();
            anim=null;
        }
    }
}
